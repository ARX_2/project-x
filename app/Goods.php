<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class Goods extends Model
{



  protected $fillable = ['published', 'main', 'slug', 'title', 'short_description', 'description', 'coast', 'type_coast', 'min_coast', 'type_min_coast', 'articuldate', 'presence', 'delivery', 'payment', 'category', 'created_by', 'modified_by', 'siteID', 'seoname', 'seotitle', 'seodescription', 'seokeywords', 'deleted', 'youtube', 'checkseo', 'sort'];


  public function setSlugAttribute($value){
    //asdsadd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function categories(){
      return $this->hasOne('App\Category', 'id', 'category');
    }
    public function subcategory(){
      return $this->hasOne('App\Category', 'id', 'category')->with('categor');
    }
    public function images(){
      return $this->hasOne('App\Image', 'goods', 'articul')->orderBy('sort', 'asc');
    }
    public function imageses(){
      return $this->hasMany('App\Image', 'goods', 'articul')->orderBy('sort', 'asc');
    }
    public function properties(){
      return $this->belongsToMany('App\Property', 'goods_property', 'goods', 'property' );
    }

    public function CategoryGoods(){
      return $this->hasOne('App\CategoryGoods', 'goods', 'id');
    }
    public function Goods_object(){
      return $this->hasOne('App\Goods_object', 'p_g_id', 'id');
    }
    public function goods_salon(){
      return $this->hasOne('App\Goods_salon', 'parent_id', 'id');
    }
    public function service_goods(){
      return $this->hasMany('App\Service_goods', 'service', 'id')->with('products');
    }
    public function options(){
      return $this->hasMany('App\Service_option', 'service', 'id');
    }
    public function inventory(){
      return $this->hasMany('App\Inventory', 'goods', 'id');
    }
    public function type_costs(){
      return $this->hasOne('App\Type_cost', 'id', 'type_cost');
    }
    public function feedbacks(){
      return $this->hasmany('App\Feedback', 'goods', 'id');
    }
    public function doma(){
      return $this->hasOne('App\Model\Goods_doma', 'p_g_id', 'id');
    }
    // public function images(){
    //   return $this->hasOne('App\Image', 'goods', 'id')->with('imageResize');
    // }
}
