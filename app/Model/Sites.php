<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 09.04.2019
 * Time: 11:33
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Sites extends Model
{
    protected $table = 'sites';

    //связка для footer
    public function information(){
        return $this->hasOne('App\Model\Information', 'siteID', 'id')->select('siteID','timeJob','address','city','logo2','metrika','analytics', 'vkontakte', 'odnoklassniki', 'instagram', 'youtube','color_menu')->with('cities');
    }
    public function sections(){
        return $this->hasMany('App\Model\Sections', 'siteID', 'id')->where('published', 1)->select('siteID','title','slug','published');
    }
}
