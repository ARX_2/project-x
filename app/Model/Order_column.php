<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order_column extends Model
{
    protected $table = 'order_column';

    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $softDelete = true;

    public function orders(){
        return $this->hasMany('App\Orders', 'column_id', 'id')->with('order_customer')->orderBy('position', 'asc')->orderBy('id', 'desc');
    }
}