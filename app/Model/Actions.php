<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 18.03.2019
 * Time: 17:28
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Actions extends Model
{
    protected $table = 'actions';
}