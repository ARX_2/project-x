<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 14.03.2019
 * Time: 12:46
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';
}