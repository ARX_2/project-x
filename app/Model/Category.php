<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 14.03.2019
 * Time: 8:09
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    public function images(){
        return $this->hasOne('App\Image', 'category', 'articul')->orderBy('sort', 'ASC');
    }
    /// Используется только на главной странице сайта - IndexController
    public function video(){
        return $this->hasMany('App\Model\Video', 'article', 'youtube')->where('published',1)->orderBy('sort', 'ASC')->limit(4);
    }
    public function sub_category(){
        return $this->hasMany('App\Model\Category', 'parent_id', 'id')->where('published',1)->select('id', 'seoname', 'title', 'slug', 'parent_id')->orderBy('sort', 'ASC');
    }
    /// Страницы товары, услуги
    public function bread_cat(){
        return $this->hasOne('App\Model\Category', 'id', 'parent_id')->select('id', 'title', 'slug', 'parent_id');
    }
    public function sub_category_nav(){
        return $this->hasMany('App\Model\Category', 'parent_id', 'id')->where('published',1)->orderBy('sort', 'asc');
    }
    public function goods_products(){
        return $this->hasMany('App\Modegoods_servicesl\Goods', 'category', 'id')->where('published', 1)->limit(24)->with('images');
    }
    /// используется для услуг вывод товаров через категорию - надо удалить
//    public function goods_services(){
//        return $this->hasMany('App\Model\Goods', 'category', 'id')->where('published', 1)->limit(40)->with('images');
//    }
    /// В портфолио выводим объекты, привязанные к категориям
    public function objects(){
        return $this->hasOne('App\Model\Objects', 'parent_id', 'id');
    }
    /// &&&&
    public function subcategory(){
        return $this->hasMany('App\Model\Category', 'parent_id', 'id')->orderBy('sort', 'asc');
    }
    public function section(){
      return $this->hasOne('App\Section', 'type', 'type');
    }

    public function reviews(){
        return $this->hasMany('App\Model\Feedback', 'category', 'id');
    }

}
