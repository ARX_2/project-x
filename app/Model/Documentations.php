<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 18.03.2019
 * Time: 20:10
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Documentations extends Model
{
    protected $table = 'documentations';
}