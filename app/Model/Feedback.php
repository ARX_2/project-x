<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedback';

    public function imageses(){
        return $this->hasMany('App\Image', 'feedback', 'id');
    }
    /// для страницы all-reviews достаю отзывы на категории и считаю их количество
    public function count_category(){
        return $this->hasOne('App\Category', 'id', 'category');
    }
}
