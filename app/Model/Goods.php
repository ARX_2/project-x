<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 13.03.2019
 * Time: 17:00
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Goods extends Model
{
    protected $table = 'goods';

    public function images(){
        return $this->hasOne('App\Image', 'goods', 'articul')->orderBy('sort', 'ASC');
    }

    public function imageses(){
        return $this->hasMany('App\Image', 'goods', 'articul')->orderBy('sort', 'ASC');
    }

    public function goods_category(){
        return $this->hasOne('App\Model\Category', 'id', 'category')->select('id','slug','published','type','parent_id');
    }
    /// Для товаров
    /// Главная страница товаров
    /// Получаем url категории для товаров
    public function product_url_cat(){
        return $this->hasOne('App\Model\Category', 'id', 'category')->select('id','title','slug','published','type','parent_id','seoname');
    }
    /// Получаем url для подкатегории на странице просмотра категории
    public function product_url_scat(){
        return $this->hasOne('App\Model\Category', 'id', 'category')->select('id','slug','published','type','parent_id');
    }
    /// Получаем характеристики проекта
    public function goods_doma(){
        return $this->hasOne('App\Model\Goods_doma', 'p_g_id', 'id');
    }
    public function feedbacks(){
      return $this->hasmany('App\Feedback', 'goods', 'id');
    }
    public function video(){
        return $this->hasMany('App\Model\Video', 'article', 'youtube')->where('published',1)->orderBy('sort', 'ASC')->limit(4);
    }
    /// Для услуг
    ///
}
