<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Order_message extends Model
{
    protected $table = 'order_message';

    public function user(){
        return $this->hasOne('App\Model\Users', 'id', 'user_id');
    }
}