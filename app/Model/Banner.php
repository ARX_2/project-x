<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 29.03.2019
 * Time: 18:08
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners';
}