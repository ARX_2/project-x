<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 18.03.2019
 * Time: 20:53
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Emails extends Model
{
    protected $table = 'emails';
}