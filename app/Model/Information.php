<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 08.04.2019
 * Time: 11:53
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    protected $table = 'information';

    public function sites(){
        return $this->hasOne('App\Model\Sites', 'id', 'siteID')->select('id', 'name');
    }
    public function cities(){
    return $this->hasOne('App\City', 'id', 'city');
  }
}