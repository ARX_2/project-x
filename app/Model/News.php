<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 18.03.2019
 * Time: 18:39
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public function image_one(){
        return $this->hasOne('App\Image', 'object', 'id');
    }

    protected $table = 'news';
}