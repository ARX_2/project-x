<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 18.03.2019
 * Time: 17:48
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Sections extends Model
{
    protected $table = 'sections';
}