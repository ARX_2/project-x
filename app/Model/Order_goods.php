<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Order_goods extends Model
{
    protected $table = 'order_goods';

    public function goods(){
        return $this->hasOne('App\Model\Goods', 'id', 'goods_id');
    }
}