<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 13.03.2019
 * Time: 18:06
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'texts';

    public function image_one(){
        return $this->hasOne('App\Image', 'text', 'id');
    }

    public function section_image_one(){
        return $this->hasOne('App\Image', 'text', 'id');
    }

    public function sections(){
        return $this->hasOne('App\Model\Sections')->select('id','type','slug','siteID');
    }
    public function video(){
        return $this->hasMany('App\Model\Video', 'article', 'youtube')->where('published',1)->orderBy('sort', 'ASC')->limit(4);
    }
}