<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 25.03.2019
 * Time: 16:52
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Pricelist_category extends Model
{
    protected $table = 'category_price';

    public function pricelist_item(){
        return $this->hasMany('App\Model\Pricelist', 'category', 'id');
    }
}