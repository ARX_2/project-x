<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 25.03.2019
 * Time: 16:34
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Pricelist extends Model
{
    protected $table = 'pricelists';
}