<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 18.03.2019
 * Time: 16:26
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $table = 'objects';

    public function section_image_one(){
        return $this->hasOne('App\Image', 'object', 'id');
    }
    public function image_one(){
        return $this->hasOne('App\Image', 'object', 'articul');
    }
    public function imageses(){
        return $this->hasMany('App\Image', 'object', 'articul');
    }
    public function portfolio_one_category(){
        return $this->hasOne('App\Model\Category', 'id', 'parent_id')->select('id','slug','published');
    }

    public function portfolio_category(){
        return $this->hasMany('App\Model\Services', 'id', 'parent_id')->where('published', 1)->select('id', 'parent_id', 'published', 'title', 'sort', 'slug')->orderBy('sort', 'asc');
    }
    public function category_parent(){
        return $this->hasOne('App\Model\Category', 'id', 'parent_id')->select('id','parent_id','slug');
    }

}
