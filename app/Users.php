<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
  protected $fillable = ['name', 'image', 'tel', 'published', 'title', 'description', 'email', 'password', 'description', 'created_at', 'updated_at', 'userRolle', 'siteID', 'job', 'sex', 'lastname', 'onSite', 'timeJob'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function users(){
      return $this->has();
    }
    public function imageResize(){
      return $this->hasOne('App\ImageResize', 'image', 'image');
    }
    public function userrolle(){
      return $this->hasOne('App\UserRolle', 'id', 'userRolle');
    }
    public function users_salon(){
      return $this->hasOne('App\Users_salon', 'user', 'id')->orderBy('id', 'desc');
    }
    public function calendar(){
      return $this->hasMany('App\Calendar', 'userid', 'id');
    }

    public function january(){
      return $this->hasOne('App\Calendar', 'userid', 'id')->where('month', 1);
    }
    public function february(){
      return $this->hasOne('App\Calendar', 'userid', 'id')->where('month', 2);
    }
    public function mart(){
      return $this->hasOne('App\Calendar', 'userid', 'id')->where('month', 3);
    }
    public function april(){
      return $this->hasOne('App\Calendar', 'userid', 'id')->where('month', 4);
    }
    public function may(){
      return $this->hasOne('App\Calendar', 'userid', 'id')->where('month', 5);
    }
    public function june(){
      return $this->hasOne('App\Calendar', 'userid', 'id')->where('month', 6);
    }
    public function jule(){
      return $this->hasOne('App\Calendar', 'userid', 'id')->where('month', 7);
    }
    public function august(){
      return $this->hasOne('App\Calendar', 'userid', 'id')->where('month', 8);
    }
    public function september(){
      return $this->hasOne('App\Calendar', 'userid', 'id')->where('month', 9);
    }
    public function october(){
      return $this->hasOne('App\Calendar', 'userid', 'id')->where('month', 10);
    }
    public function november(){
      return $this->hasOne('App\Calendar', 'userid', 'id')->where('month', 11);
    }
    public function december(){
      return $this->hasOne('App\Calendar', 'userid', 'id')->where('month', 12);
    }
}
