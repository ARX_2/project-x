<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category_price extends Model
{
    protected $table = 'category_price';

    public function price(){
      return $this->hasMany('App\Pricelist', 'category', 'id');
    }
}
