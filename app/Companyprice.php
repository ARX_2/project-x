<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companyprice extends Model
{
  protected $fillable = ['title', 'subtitle', 'description', 'image', 'siteID', 'created_by', 'modified_by'];

public function setSlugAttribute($value){
  //dd($this->title);
  $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
}

public function contacts(){
  return $this->has();
}
}
