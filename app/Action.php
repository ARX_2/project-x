<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
  public function imageResize(){
    return $this->hasOne('App\ImageResize', 'image', 'image');
  }
}
