<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ruka extends Model
{
  protected $connection = 'rukabludka';
  protected $table = 'users';
  public function imageResize(){
    return $this->hasOne('App\ImageResize', 'image', 'image');
  }
}
