<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    public function site(){
      return $this->has();
    }

    public function companies(){
      return $this->hasOne('App\Company', 'siteID', 'id')->with('infoImage')->with('goods')
      ->with('management')->with('contactUser');
    }
    public function categories(){
      return $this->hasMany('App\Category', 'siteID', 'id');
    }
    public function about(){
      return $this->hasOne('App\Company', 'siteID', 'id')->with('images')->with('infoImage')->with('template')->with('sections');
    }

    public function templates(){
      return $this->hasOne('App\Template', 'siteID', 'id')
      ->with('seos')->with('companies')->with('goods')->with('categories')->with('infos')
      ->with('companyprices')->with('objects')->with('menues')->with('banners')->with('menu')->with('includes')->with('jobs')->with('texts');
    }
    public function template(){
      return $this->hasOne('App\Template', 'siteID', 'id');
    }
    public function product(){
      return $this->hasOne('App\Goods', 'siteID', 'id')->with('categories')->with('properties');
    }
    public function user(){
      return $this->hasOne('App\Users', 'siteID', 'id')->with('imageResize')->where('userRolle', 3);
    }
    public function category(){
      return $this->hasOne('App\Category', 'siteID', 'id');
    }
    public function company(){
      return $this->hasOne('App\Company', 'siteID', 'id')->with('images')->with('infoImage');
    }
    public function info(){
      return $this->hasOne('App\Information', 'siteID', 'id')->with('sections')->with('cities');
    }
    public function contacts(){
      return $this->hasOne('App\Contact', 'siteID', 'id');
    }
    public function price(){
      return $this->hasMany('App\Pricelist', 'siteID', 'id')->with('category');
    }
    public function category_price(){
      return $this->hasMany('App\Category_price', 'siteID', 'id')->with('price');
    }
    public function sections(){
        return $this->hasMany('App\Section', 'siteID', 'id');
    }
    public function cities(){
        return $this->hasOne('App\City', 'id', 'city');
    }
}
