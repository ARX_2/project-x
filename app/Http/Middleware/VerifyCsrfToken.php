<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'admin/admin/sort/goods', 'admin/admin/git/push', 'create/order', 'admin/admin/support/comment',
        'admin/goods', 'admin/image/upload', 'admin/goods/update/description', 'admin/image/category/upload',
        'admin/category/update/description', 'admin/service/update/description', 'admin/image/banner/upload','admin/image/tizer/upload',
        'admin-salon/supplier/create', 'admin-salon/goods/update/description', '/admin-salon/service/update/description',
        '/admin-salon/image/materials/upload', '/admin-salon/materials/update/description', '/admin-salon/invenory/entrance',
        '/admin-salon/invenory/inventory', '/admin-salon/invenory/charge','admin/stroitelstvo/update/description','admin/proyekty/update/description',
        'admin/izgotovleniye/update/description','admin/bani/update/description', '/feedback/store', 'admin/change/title/sites',
        '/admin/image/category/cover/upload',

    ];
}
