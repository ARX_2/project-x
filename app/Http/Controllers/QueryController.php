<?php

namespace App\Http\Controllers;

use DB;
use App\Offer;
use Request;
use App\Template;
use App\Work;
use ImageResize;
use App\Forumsubcategory;
use App\ForumMessage;
use App\Information;
use App\Category;
use App\ImageResize as Img;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
class QueryController extends Controller
{
  public static function siteid($sitename){
      $site =  DB::table('sites')->where('title', $sitename)->first();
      return $site;
  }
    public static function template($tempid){

      $site = self::siteid(Request::getHttpHost());
      //dd($site);
      $templatename = Template::where('siteID', $site->id)->where('template', $tempid)->value('templatename');

      if($site->title == 'povishev.ru'){
        $templatename = 'povishev';
      }
      if($site->type == 1){
        $type = 'site';
      }
      else{
        $type = 'salon';
      }
      return ['type' => $type, 'templatename' => $templatename];
    }

    static function categories(){
      //dd(Request::getHttpHost());
      $site = self::siteid(Request::getHttpHost());
      $categories = DB::table('categories')
          ->select('categories.id', 'categories.title', 'categories.slug', 'categories.main', 'categories.parent_id', 'categories.published',
              'images.images', 'categories.type', 'i.imagesize', 'i.size', 'i.mele' , 'fullimagesize')
          ->leftJoin('images', 'categories.id', '=', 'images.category')
          ->leftJoin('image as i', 'images.images', '=', 'i.image')
          ->where('categories.published', 1)
          ->where('categories.siteID', $site->id)
          ->where('categories.deleted', '!=', '1')
          ->get();
        return $categories;
    }

  public static function seos($locate){
      $site = self::siteid(Request::getHttpHost());
    return DB::table('seos')->where('siteID', $site->id)->where('locate', $locate)->get();
    }
    static function banner(){
      $site = self::siteid(Request::getHttpHost());
    return  DB::table('banners')->where('siteID', $site->id)->where('published', '1')->get();
    }
    static function objects(){
      $site = self::siteid(Request::getHttpHost());
      return DB::table('objects')
      ->select('objects.title', 'objects.text', 'objects.image as images', 'objects.published', 'i.imagesize', 'i.image', 'i.fullimagesize', 'i.size', 'i.mele')
      ->leftJoin('image as i', 'objects.image', '=', 'i.image')
      ->where('siteID', $site->id)->where('published', '1')->get();
    }
    static function company(){
      $site = self::siteid(Request::getHttpHost());
    return  DB::table('companies')
          ->leftJoin('images', 'companies.id', '=', 'images.company')
          ->leftJoin('image as i', 'images.images', '=', 'i.image')
              ->where('companies.siteID', $site->id)
              ->where('companies.published', 1)->get();
    }
    static function goods(){
      $site = self::siteid(Request::getHttpHost());
      //dd($site);
      return DB::table('goods')
            ->select('goods.id', 'goods.coast', 'cg.category', 'goods.slug', 'goods.title', 'images.images', 'goods.main', 'goods.type', 'i.imagesize', 'i.size', 'i.mele' , 'fullimagesize')
            ->leftJoin('images', 'goods.id', '=', 'images.goods')
            ->leftJoin('category_goods as cg', 'goods.id', '=', 'cg.goods')
            ->leftJoin('categories as c', 'cg.category', '=', 'c.id')
            ->leftJoin('image as i', 'images.images', '=', 'i.image')
            ->where('goods.published', 1)
            ->where('goods.siteID', $site->id)
            ->where('c.deleted', '!=', '1')
            ->where('goods.deleted', '!=', '1')
            ->get();
    }
    static function info(){
      $site = self::siteid(Request::getHttpHost());
    return  DB::table('information')
            ->leftJoin('background as b', 'information.siteID', '=', 'b.siteID')
            ->leftJoin('menus as m', 'information.siteID', '=', 'm.siteID')
            ->where('information.siteID', $site->id)
            ->orWhere('b.siteID', $site->id)
            ->orWhere('m.siteID', $site->id)
            ->first();
    }
    static function companyprices(){
        $site = self::siteid(Request::getHttpHost());
      return  DB::table('companyprices')->where('siteID', $site->id)->get();
    }

    static function contacts(){
      $site = self::siteid(Request::getHttpHost());
    return  DB::table('contacts')->where('siteID', $site->id)->get();
    }

    public static function product($slug){
    return  DB::table('goods as g')
          ->select('g.id', 'g.seo', 'g.slug', 'g.title', 'cg.category', 'g.short_description', 'g.description', 'g.coast', 'g.min_coast', 'p.property', 'c.type as ctype', 'p.title as title_p', 'tp.type', 'images.images'
          , 'i.imagesize', 'i.size', 'i.mele' , 'fullimagesize')
          ->leftJoin('category_goods as cg', 'g.id', '=', 'cg.goods')
          ->leftJoin('goods_property as gp', 'g.id', '=', 'gp.goods')
          ->leftJoin('property as p', 'gp.property', '=', 'p.id')
          ->leftJoin('type_property as tp', 'p.type', '=', 'tp.id')
          ->leftJoin('images', 'g.id', '=', 'images.goods')
          ->leftJoin('categories as c', 'cg.category', '=', 'c.id')
          ->leftJoin('image as i', 'images.images', '=', 'i.image')
          ->where('g.slug', $slug)
          ->where('g.deleted', '!=', '1')
          ->get();
    }
    static function active_category(){
      $site = self::siteid(Request::getHttpHost());
    return  DB::table('categories')->where('categories.published', 1)
          ->leftJoin('category_goods as cg', 'categories.id', '=', 'cg.category')
          ->where('categories.siteID', $site->id)
          ->get();
    }

    static function User(){
      $site = self::siteid(Request::getHttpHost());
    return  DB::table('users')->select('users.id', 'users.name', 'users.email', 'users.tel', 'users.title', 'users.description', 'users.image as images', 'users.published', 'users.userRolle', 'i.imagesize', 'i.size', 'i.mele' , 'fullimagesize')
      ->leftJoin('image as i', 'users.image', '=', 'i.image')
      ->where('published', 1)->where('siteID', $site->id)->first();
    }

    public static function actionCategory($slug){
      $site = self::siteid(Request::getHttpHost());
    return  DB::table('categories')
          ->select('categories.id', 'categories.title', 'categories.slug', 'categories.main', 'categories.parent_id', 'categories.published',
              'images.images', 'categories.type')
          ->leftJoin('images', 'categories.id', '=', 'images.category')
          ->where('categories.published', 1)
          ->where('categories.siteID', $site->id)
          ->where('categories.slug', $slug)
          ->first();
    }
  public static function active_goods($slug){
    $site = self::siteid(Request::getHttpHost());
      return DB::table('goods as g')
      ->select('g.title', 'g.coast', 'g.slug', 'g.id', 'images.images'
      , 'i.imagesize', 'i.size', 'i.mele' , 'fullimagesize')
      ->leftJoin('category_goods as cg', 'cg.goods', '=', 'g.id')
      ->leftJoin('categories as c', 'cg.category', '=', 'c.id')
      ->leftJoin('images', 'g.id', '=', 'images.goods')
      ->leftJoin('image as i', 'images.images', '=', 'i.image')
      ->where('c.published', 1)
      ->where('c.siteID', $site->id)
      ->where('c.slug', $slug)
      ->where('g.published', 1)
      ->where('g.deleted', '!=', '1')
      ->get();
    }

    static function ResizeImage($image, $size){

      if(explode('/', $image)[0] == 'public'){
        $imageName = $image;
    }
    else{
        if(isset($image->images)){
      $imageName = $image->images;
    }
    elseif(isset($image->image)){
      if($image->image == null){
        return 'public/no-image-170x170.jpg';
      }
      else{
        $imageName = $image->image;
      }
    }
    else{
      return 'public/no-image-170x170.jpg';
    }
    }
      $imageOrigin = $imageName;
      //dd($image);
      if($image->imageResize !== null){
        try {
              Storage::getVisibility($image->imageResize->fullimagesize);
              if($image->imageResize->size == $size){
                return $image->imageResize->fullimagesize;
              }
              else{
                $imagename = explode('.', $imageName);
                $sizearray = explode('x', $size);
                //dd($image);



                ImageResize::make('storage/app/'.$imageName)
                ->resize($sizearray[0], $sizearray[1],function ($constraint) {
                      $constraint->aspectRatio();
                      $constraint->upsize();
                  })
                ->save('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);

                //dd($imageName);
                $optimizerChain = app(\Spatie\ImageOptimizer\OptimizerChain::class)->optimize('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);
                  DB::table('image')->insert(['image' => $imageOrigin,'imagesize' => $imagename[0], 'fullimagesize' => $imagename[0].'_'.$size.'.'.$imagename[1], 'size' => $size, 'mele' => $imagename[1]]);
                return $imagename[0].'_'.$size.'.'.$imagename[1];
              }
              //dd($image->imageResize->fullimagesize);

          } catch (\Exception $ex) {
              DB::table('image')->where('image', $imageName)->delete();
              $imageName = 'public/5c0fca9089a27.png';
              $imagename = explode('.', $imageName);
              $sizearray = explode('x', $size);
              //dd($image);



              ImageResize::make('storage/app/'.$imageName)
              ->resize($sizearray[0], $sizearray[1],function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })
              ->save('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);

              //dd($imageName);
              $optimizerChain = app(\Spatie\ImageOptimizer\OptimizerChain::class)->optimize('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);
                DB::table('image')->insert(['image' => $imageOrigin,'imagesize' => $imagename[0], 'fullimagesize' => $imagename[0].'_'.$size.'.'.$imagename[1], 'size' => $size, 'mele' => $imagename[1]]);
              return $imagename[0].'_'.$size.'.'.$imagename[1];
          }
      }
      else{
        //dd($imageName);
        try {
              Storage::getVisibility($imageName);
          } catch (\Exception $ex) {
                $imageName = 'public/5c0fca9089a27.png';
      }
      $imageName = $imageOrigin;
      //dd($imageOrigin);
      $imagename = explode('.', $imageName);
      $sizearray = explode('x', $size);
      //dd($image);



      ImageResize::make('storage/app/'.$imageName)
      ->resize($sizearray[0], $sizearray[1],function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })
      ->save('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);

      //dd($imageName);
      $optimizerChain = app(\Spatie\ImageOptimizer\OptimizerChain::class)->optimize('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);
        DB::table('image')->insert(['image' => $imageOrigin,'imagesize' => $imagename[0], 'fullimagesize' => $imagename[0].'_'.$size.'.'.$imagename[1], 'size' => $size, 'mele' => $imagename[1]]);
      return $imagename[0].'_'.$size.'.'.$imagename[1];
      }

    }


    static function date($date){
      $datemonth = ['01' => 'января', '02' => 'февраля', '03' => 'марта',
      '04' => 'апреля', '05' => 'мая', '06' => 'июня', '07' => 'июля', '08' => 'августа',
      '09' => 'сентября', '10' => 'октября', '11' => 'ноября', '12' => 'декабря'];
      $month = date("m", strtotime($date));
      $day = date("d", strtotime($date));
      $year = date("Y", strtotime($date));
      return $day.' '.$datemonth[$month].' '.$year;
    }
}
