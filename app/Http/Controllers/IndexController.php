<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 29.03.2019
 * Time: 17:48
 */

namespace App\Http\Controllers;
use App\Email;
use App\Model\Banner;
use App\Model\Category;
use App\Model\Goods;
use App\Model\Information;
use App\Model\News;
use App\Model\Portfolio;
use App\Model\Sections;
use App\Model\Settings;
use App\Model\Tizers;
use App\Model\Url_request;
use App\Model\Users;
use App\Model\Visual;
use App\Site;
use App\Tel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Caching;
use Resize;

class IndexController extends Controller
{
    public function index(request $request){

        if (Cache::has('/' . $request->getHttpHost())) {
           // Cache::forget('/' . $request->getHttpHost());
        }
        else {$site = Site::with('templates')->where('title', $request->getHttpHost())->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $tel = Tel::where('siteID', $siteID)->first();
        $email = Email::where('siteID', $siteID)->first();
        $settings = Settings::where('siteID', $siteID)->where('type', 1)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo', 'metrika_short')->first();
        /// Получаем товары для блока "Популярные товары"
        $tizers = Tizers::where('siteID', $siteID)->limit(4)->get();
        /// Получаем категории для блока "Каталог"
        $banner = Banner::where('siteID', $siteID)->where('published', 1)->first();
        $url_request = Url_request::where('siteID', $siteID)->where('published', 1)->get();
        /// Получаем категории для блока "Каталог"
        $category = Category::with('sub_category')->with('section')->where('siteID', $siteID)->where('parent_id', null)->where('published', 1)->where('main', 1)->limit(6)->get();
        $categories = Category::where('siteID', $siteID)->where('parent_id', null)->where('published', 1)->where('type', 7)->where('main', 1)->orderby('sort', 'asc')->get();
        $info = Information::with('cities')->where('siteID', $siteID)->first();
        /// Получаем новости для блока "Новости"
        $news = News::with('image_one')->where('siteID', $siteID)->where('published', 1)->where('main', 1)->limit(3)->get();
        $visual = Visual::where('siteID', $siteID)->first();
        /// Получаем портфолио для "Наши работы"
        $portfolio = Portfolio::with('portfolio_one_category')->with('image_one')->where('siteID', $siteID)->where('parent_id', '!=', null)->where('published', 1)->where('main', 1)->limit(18)->get();
        /// DOCTYPE
        if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Главная страница сайта';}
        if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
        if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
        $canonical = $request->getSchemeAndHttpHost();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);

        /// header
        if($visual->menu == 3){
            $view .= view('2019.layouts.menu.menu_3i', ['categories' => $categories, 'tel' => $tel, 'email' => $email, 'info' => $info]);
        }
        else{
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        }
        /// content
        $view .= view('2019.index.index_1', ['siteID' => $siteID, 'url_request'=>$url_request, 'banner' => $banner, 'tizers' => $tizers, 'visual'=>$visual, 'category' => $category]);
        $visual = Visual::where('siteID', $siteID)->first();

        if($visual->index_goods == null && $visual->index_goods == 0){
        $goods = Goods::with(['goods_category' => function($query){$query->with('section');}])->whereHas('goods_category', function($query){$query->where('published', 1);})
            ->where('siteiD', $siteID)->where('published', 1)->where('main', 1)->limit(8)->get();
        $view .= view('2019.index.goods_all', ['siteID' => $siteID,'goods' => $goods]);
        }

        if($visual->index_goods == 7) {
            $goods = Goods::with('product_url_cat')->whereHas('product_url_cat', function ($query) {$query->where('published', 1)->where('type', 7);})->where('siteiD', $siteID)->where('published', 1)->where('main', 1)->limit(8)->get();
            $view .= view('2019.index.goods_izgotovleniye', ['siteID' => $siteID,'goods' => $goods]);
        }

        $view .= view('2019.index.bottom', ['siteID' => $siteID, 'news' => $news, 'portfolio' => $portfolio, 'settings' => $settings, 'favicon' => $favicon]);
        if($visual->menu == 2 || $visual->menu == 3){$view .= view('2019.layouts.menu.menu_2_style');}
        /// footer
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }
}
