<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 12.03.2019
 * Time: 13:13
 */

namespace App\Http\Controllers;

use App\Model\Actions;
use App\Model\Documentations;
use App\Model\Emails;
use App\Model\News;
use App\Model\Information;
use App\Model\Pricelist_category;
use App\Model\Sections;
use App\Model\Settings;
use App\Model\Tels;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Caching;
use Resize;

class CompanyController extends Controller
{
    public function about(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::with('image_one')->where('siteID', $siteID)->where('type', 2)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Получаем разделы для формирования левого меню
        $sections = Sections::where('siteID', $siteID)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// DOCTYPE
        if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='О компании';}
        if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
        if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
        $canonical = $request->url();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.company.about_1', ['settings' => $settings, 'sections' => $sections, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function documents(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::with('image_one')->where('siteID', $siteID)->where('type', 14)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Получаем разделы для формирования левого меню
        $sections = Sections::where('siteID', $siteID)->where('published', 1)->get();
        /// Получаем список документов
        $documents = Documentations::where('siteID', $siteID)->get();
        /// DOCTYPE
        if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Документы';}
        if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
        if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
        $canonical = $request->url();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.company.documents_1', ['settings' => $settings, 'sections' => $sections, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function action(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::with('image_one')->where('siteID', $siteID)->where('type', 8)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Получаем разделы для формирования левого меню
        $sections = Sections::where('siteID', $siteID)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем список акций
        $action = Actions::where('siteID', $siteID)->where('published', 1)->get();
        /// DOCTYPE
        if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Акции';}
        if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
        if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
        $canonical = $request->url();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.company.action.index', ['settings' => $settings, 'sections' => $sections, 'action' => $action, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function actionView(request $request, $slug){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $action_id = explode('-', $slug)[0];
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Получаем разделы для формирования левого меню
        $sections = Sections::where('siteID', $siteID)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем акцию, на которую зашли
        $action = Actions::where('id', $action_id)->where('published', 1)->first();
        /// Получаем список других акций
        $similar = Actions::where('siteID', $siteID)->where('id', '!=', $action_id)->where('published', 1)->limit(3)->get();
        /// DOCTYPE
        if($action->title){$seoTitle = $action->title;}else{$seoTitle = 'Название акции';}
        {$seoDescription = null;}
        {$keywords = null;}
        $canonical = $request->url();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.company.action.view', ['sections' => $sections, 'action' => $action, 'similar' => $similar, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function news(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::with('image_one')->where('siteID', $siteID)->where('type', 9)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Получаем разделы для формирования левого меню
        $sections = Sections::where('siteID', $siteID)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем список новостей
        $news = News::where('siteID', $siteID)->where('published', 1)->get();
        /// DOCTYPE
        if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Новости';}
        if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
        if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
        $canonical = $request->url();

        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.company.news.index', ['settings' => $settings, 'sections' => $sections, 'news' => $news, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function newsView(request $request, $slug){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $news_id = explode('-', $slug)[0];
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Получаем новость, на которую зашли
        $news = News::where('id', $news_id)->where('published', 1)->first();
        /// Получаем разделы для формирования левого меню
        $sections = Sections::where('siteID', $siteID)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем список других новостей
        $similar = News::where('siteID', $siteID)->where('id', '!=', $news_id)->where('published', 1)->limit(3)->get();
        /// DOCTYPE
        if($news->title){$seoTitle = $news->title;}else{$seoTitle = 'Название новости';}
        {$seoDescription = null;}
        {$keywords = null;}
        $canonical = $request->url();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.company.news.view', ['sections' => $sections, 'news' => $news, 'similar' => $similar, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function priceList(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::with('image_one')->where('siteID', $siteID)->where('type', 11)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Получаем разделы для формирования левого меню
        $sections = Sections::where('siteID', $siteID)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Достаю категории для прайслист + связка прайслист
        $pricelist_categories = Pricelist_category::with('pricelist_item')->where('siteID', $siteID)->get();
        /// DOCTYPE
        if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Прайс-лист';}
        if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
        if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
        $canonical = $request->url();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.company.pricelist_1', ['sections' => $sections, 'settings' => $settings, 'pricelist_categories' => $pricelist_categories, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }
    public function jobs(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::with('image_one')->where('siteID', $siteID)->where('type', 7)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Получаем информацию о компании
        $company = Site::with('about')->with('info')->where('id', $siteID)->first();
        $info = $company->info;
        $about = $company->about;
        /// Получаем разделы для формирования левого меню
        $sections = Sections::where('siteID', $siteID)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// DOCTYPE
        $seoTitle = ('Вакансии  - ' . $info->short_name);
        $seoDescription = null;
        $keywords = null;
        $canonical = $request->url();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.company.jobs_1', ['sections' => $sections, 'settings'=>$settings,'info' => $info,'about' => $about, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function requisites(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Получаем информацию о компании
        $company = Site::with('about')->with('info')->where('id', $siteID)->first();

        $about = $company->about;
        /// Получаем разделы для формирования левого меню
        $sections = Sections::where('siteID', $siteID)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// DOCTYPE
        $seoTitle = ('Реквизиты компании - ' . $company->name);
        $seoDescription = null;
        $keywords = null;
        $canonical = $request->url();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.company.requisites_1', ['sections' => $sections, 'about' => $about,'company'=>$company, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function contacts(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::where('siteID', $siteID)->where('type', 3)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Получаем информацию о компании
        $company = Site::with('cities')->with('about')->with('info')->where('id', $siteID)->first();
        $info = $company->info;
        /// Получаем разделы для формирования левого меню
        $sections = Sections::where('siteID', $siteID)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем список телефонов
        $tels = Tels::where('siteID', $siteID)->get();
        /// Получаем список email
        $emails = Emails::where('siteID', $siteID)->get();
        /// DOCTYPE
        if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Контакты';}
        if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
        if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
        $canonical = $request->url();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.company.contact_1', ['settings' => $settings, 'sections' => $sections, 'company'=>$company, 'info' => $info, 'tels' => $tels, 'emails' => $emails, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function soglasiye(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Получаем информацию о компании

        $emails = Emails::where('siteID', $siteID)->first();
        /// DOCTYPE
        $seoTitle='Согласие на обработку персональных данных';
        $seoDescription = 'Согласие на обработку персональных данных. Качественное возведение домов, проекты и цены на любой вкус. С нашей компанией легко построить любой дом. Звоните!';
        $keywords = null;
        $canonical = $request->getSchemeAndHttpHost();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.company.soglasiye_1', ['emails' => $emails, 'canonical' => $canonical]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function politika(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Получаем информацию о компании

        $emails = Emails::where('siteID', $siteID)->first();
        /// DOCTYPE
        $seoTitle='Согласие на обработку персональных данных';
        $seoDescription = 'Согласие на обработку персональных данных. Качественное возведение домов, проекты и цены на любой вкус. С нашей компанией легко построить любой дом. Звоните!';
        $keywords = null;
        $canonical = $request->getSchemeAndHttpHost();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.company.politika_1', ['emails' => $emails, 'canonical' => $canonical]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }
}