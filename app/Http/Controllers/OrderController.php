<?php


namespace App\Http\Controllers;

use App\Model\Information;
use App\Model\Order_column;
use App\Model\Order_customer;
use App\Model\Order_message;
use App\Model\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class OrderController extends Controller
{
    public function modalMap(Request $request){
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $info = Information::where('siteID', $siteID)->first();
        return view('2019.layouts.modal.standard.map', ['info' => $info]);
    }

    public function modalOpen(request $request){
        if($request->block == 1){$blockname = 'Форма «Обратный звонок» - верхнее меню';}
        elseif($request->block == 2){$blockname = 'Форма «Закать звонок» - нижнее меню';}
        elseif($request->block == 3){$blockname = 'Форма «Бесплатный вызов замерщика»';}
        elseif($request->block == 4){$blockname = 'Форма «Заказать дизайн проект»';}
        elseif($request->block == 5){$blockname = 'Форма «Задать вопрос эксперту»';}
        return view('2019.layouts.modal.mebel.free', ['blockname' => $blockname,]);
    }

    public function orderSend(request $request){
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $description = 'Сообщение: '.htmlspecialchars($request->description).'<br><br>Cтраница заказа: '.$request->urlhost ?? null;
        $form = $request->form_name ?? null;

        $column = Order_column::where('siteID', $siteID)->orderBy('position', 'asc')->orderBy('id', 'asc')->first();
        if($column == null){
            $column_id = Order_column::insertGetId(['siteID' => $siteID, 'title' => 'Автоматически созданный список', 'position' => '1', 'created_at' => date('Y-m-d H:i:s')]);
            $order_id = Orders::insertGetId(['siteID' => $siteID, 'column_id' => $column_id, 'form' => $form, 'position' => '1', 'created_at' => date('Y-m-d H:i:s')]);
        }
        else{$order_id = Orders::insertGetId(['siteID' => $siteID, 'column_id' => $column->id, 'form' => $form, 'position' => '1', 'created_at' => date('Y-m-d H:i:s')]);}
        Order_customer::insert(['siteID' => $siteID, 'order_id' => $order_id, 'phone' => $request->phone, 'email' => $request->email, 'created_at' => date('Y-m-d H:i:s')]);
        Order_message::insert(['siteID' => $siteID, 'order_id' => $order_id, 'user_id' => '0', 'description' => $description, 'created_at' => date('Y-m-d H:i:s')]);

        return 'Заказ успешно добавлен!';
    }

    public function modalSend2(request $request){
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $order_id = Orders::insertGetId([
            'siteID' => $siteID,
            'title' => 'Форма "Бесплатный вызов замерщика"',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        $description = 'Сообщение: '.htmlspecialchars($request->description).'<br><br>Cтраница заказа: '.$request->host ?? null;
        Order_customer::insert(['siteID' => $siteID, 'order_id' => $order_id, 'phone' => $request->phone, 'email' => $request->email, 'created_at' => date('Y-m-d H:i:s')]);
        Order_message::insert(['siteID' => $siteID, 'order_id' => $order_id, 'user_id' => '0', 'description' => $description, 'created_at' => date('Y-m-d H:i:s')]);

        return redirect()->back()->with('success', $order_id);
    }
}