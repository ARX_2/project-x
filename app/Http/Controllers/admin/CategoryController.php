<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Users;
use App\Company;
use ImageResize;
use ImageOptimizer;
//use Spatie\ImageOptimizer\OptimizerChainFactory as OptimizerChainFactory;
use App\Site;
use Illuminate\Support\Facades\Cache;
use Resize;
use Caching;
use App\Subcategory;
use App\CategoryGoods;
use App\Goods;
use App\Services;
use Maatwebsite\Excel\Facades\Excel as Excel;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      Category::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();
      if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/hidden'){
          $categories = Category::with('images')->with('subcategoriesAll')->with('goods')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('parent_id',null)->where('published',0)->orderby('sort', 'asc')->get();
      }
      elseif(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/deleted'){
        $categories = Category::with('images')->with('subcategoriesAll')
        ->with('goods')
        ->where('siteID', Auth::user()->siteID)
        ->where('parent_id',null)
        ->where('published',0)
        ->where('deleted', 1)
        ->orderby('sort', 'asc')
        ->get();
      }
      else{
        $categories = Category::with('images')->with('subcategoriesAll')->with('goods')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('parent_id',null)->where('published',1)->orderby('sort', 'asc')->get();
      }
      //dd($categories);

      $countActive = Category::where('siteID', Auth::user()->siteID)->where('published', 1)->count('id');
      $countHide = Category::where('siteID', Auth::user()->siteID)->where('published', 0)->where('deleted',0)->where('parent_id', null)->count('id');
      $countDeleted = Category::where('siteID', Auth::user()->siteID)->where('published', 0)->where('deleted',1)->where('parent_id', null)->count('id');
      // return redirect()->route('category.hidden');
      // redirect()->route('admin.category.index');
      $urlActive = '/admin/category';
      $urlHidden = route('admin.category.hidden');
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
      $view .= view('admin.categories.index',[
        'categories' => $categories,
        'siteID' =>  Auth::user()->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden,
        'countDeleted' => $countDeleted,
        'link' => $_SERVER['REQUEST_URI'],
      ]);

      return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      //dd(Category::with('children')->get());
      $category =  Category::where('status',1)->where('siteID', Auth::user()->siteID)->first();
      if($category == null){
        $id = Category::insertGetId(['status' => 1, 'siteID' => Auth::user()->siteID]);
        $category = Category::where('id',$id)->first();
      }
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
        $view .= view('admin.categories.create',[
          'categories' => Category::where('siteID', Auth::user()->siteID)->where('parent_id', null)->where('published',1)->get(),
          'category' => $category,
          'subcategories' => null,
          'type' => Site::where('id', Auth::user()->siteID)->value('type'),
        ]);
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd($request->all());
      //dd(Auth::user()->siteID);
      //dd($request->category == 0);

      if($request->type == 0){
        $sort = Category::max('sort');
        $id = Category::insertGetId(['title' => $request->title,
        'description' => $request->text, 'type' => $request->section2, 'seoname' => $request->seoname,
        'seotitle' => $request->seotitle, 'seodescription' => $request->seodescription, 'seokeywords' => $request->seokeywords,
        'youtube' => $request->youtube, 'siteID' => Auth::user()->siteID, 'published' => 1, 'deleted' => 0, 'checkseo' => $request->checkseo, 'sort' => $sort,
        'offer' => $request->offer,'parent_id' => $request->parent_id, 'slug' => str_slug($request->title)
        ]);
        if($request->file('file') !== null){
        $image = Image::where('category', $request->id)->first();
        $img = $request->file('file');

        $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
        ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
          Image::create(['images' => 'public/'.$filename, 'category' => $id, 'siteID' => Auth::user()->siteID]);

        }
            Caching::menu();
      //dd(Cache::get('menu'.Auth::user()->siteID));
      Caching::footer();
      }
      else{
        //$sort = Category::max('sort');
        $sort = Category::max('sort');
        $id = Category::insertGetId(['title' => $request->title,
        'description' => $request->text, 'type' => $request->section2, 'seoname' => $request->seoname,
        'seotitle' => $request->seotitle, 'seodescription' => $request->seodescription, 'seokeywords' => $request->seokeywords,
        'youtube' => $request->youtube, 'siteID' => Auth::user()->siteID, 'published' => 1, 'deleted' => 0, 'checkseo' => $request->checkseo, 'sort' => $sort,
        'offer' => $request->offer,'parent_id' => $request->subcategory, 'slug' => str_slug($request->title)
        ]);
        if($request->file('file') !== null){
        $image = Image::where('category', $request->id)->first();
        $img = $request->file('file');

        $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
        ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
          Image::create(['images' => 'public/'.$filename, 'category' => $id, 'siteID' => Auth::user()->siteID]);

        }

      }



        return redirect()->route('admin.category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Category $category)
    {

      $backdoor = Category::with('imageses')->where('id', $category->id)->where('siteID', Auth::user()->siteID)->first();
      if($backdoor == null || $backdoor->protected == 1 && Auth::user()->userRolle == 6){
        return redirect()->route('admin.category.index');
      }

      $view = '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }

      $view .= view('admin.categories.create',[
        'category' => $backdoor,
        'siteID' => Auth::user()->siteID,
        'categories' => Category::with('subcategories')->where('siteID', Auth::user()->siteID)->where('published',1)->where('parent_id',null)->get(),
        'type' => Site::where('id', Auth::user()->siteID)->value('type'),
      ]);
      return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update($slug,$id,$parametr,Request $request, Category $category)
    {
      if($slug == 'title'){
        $cat = Category::where('id', $id)->value('status');
        if($cat == 1){
          $update['title'] = ['title' => $parametr, 'slug' => str_slug($parametr, "-"), 'updated_at' => date('Y-m-d H:i:s')];
        }
        else{
          $update['title'] = ['title' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
        }
      }
      $unparametr[$parametr] = $parametr;
      $unparametr[0] = 1;
      $unparametr[1] = 0;
      $update['published'] = ['published' => $unparametr[$parametr], 'deleted' => 0, 'dateDelete' => null, 'updated_at' => date('Y-m-d H:i:s')];
      $update['main'] = ['main' => $unparametr[$parametr], 'updated_at' => date('Y-m-d H:i:s')];
      $update['deleted'] = ['deleted' => $unparametr[$parametr], 'updated_at' => date('Y-m-d H:i:s'), 'dateDelete' => \carbon\Carbon::now()->addDays(30)->toDateTimeString()];

      $update['offer'] = ['offer' => $parametr];
        $update['price'] = ['price' => $parametr];
      $update['subcategory'] = ['parent_id' => $parametr, 'type' => Category::where('id', $parametr)->where('siteID', Auth::user()->siteID)->value('type')];
      $update['type'] = ['type' => $parametr, 'parent_id' => null,];
      $update['checkseo'] = ['checkseo' => $parametr];
      $update['seoname'] = ['seoname' => $parametr];
      $update['seotitle'] = ['seotitle' => $parametr];
      $update['seodescription'] = ['seodescription' => $parametr];
      $update['seokeywords'] = ['seokeywords' => $parametr];
      if($request->url == ''){
        $update['youtube'] = ['youtube' => null];
      }
      else{
        $update['youtube'] = ['youtube' => 'https://'.$request->url];
      }

      // $update['offer'] = ['offer' => $parametr];
      // $update['offer'] = ['offer' => $parametr];
      $result =  Category::where('id', $id)->where('siteID', Auth::user()->siteID)->update($update[$slug]);
      if($slug == 'type'){
         Category::where('parent_id', $id)->where('siteID', Auth::user()->siteID)->update(['type' => $parametr]);
      }
      return $result;


        return redirect()->route('admin.category.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request, Category $category)
    {
      //dd($request);
      $backdoor = Category::where('id', $category->id)->where('siteID', Auth::user()->siteID)->first();
      Category::where('id', $category->id)->update(['published', 0]);
      Caching::categoryMenu();
      //Caching::categoryDocs();
      Caching::categories();
      Caching::category($category->id);
      //dd($backdoor);
      // if($backdoor == null){
      //   echo 'Категория не найдена';
      //   die;
      // }
      // //dd($category);
      //   $image = Image::where('category', $category->id)->value('images');
      //   //Image::with('categories')->where('images', $image)->delete();
      //   //Storage::delete($image);
      //   //dd($image);
      //   //$images = DB::table('image')->where('image', $image)->get();
      //   //dd($images);
      //   //foreach ($images as $i) {
      //   //  DB::table('image')->where('image', $image)->delete();
      //   //  Storage::delete($i->fullimagesize);
      //   //}
      //   DB::table('deleted_categories')->insert([
      //     'id' => $category->id, 'title' => $category->title, 'slug' => $category->slug, 'main' => $category->main, 'type' => $category->type,
      //     'parent_id' => $category->parent_id, 'published' => $category->published, 'created_by' => $category->created_by, 'updated_at' => $category->updated_at,
      //     'created_at' => $category->created_at, 'siteID' => $category->siteID
      //   ]);
      //   $category->delete();
        return redirect()->route('admin.category.index');
    }

    public function hide(Request $request){
      //dd($request);
      $published = Category::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('published');
      if($published == 0){
        Category::where('id', $request->id)->update(['published' => 1]);
      }
      elseif($published == 1){
        Category::where('id', $request->id)->update(['published' => 0]);
      }

      return redirect()->route('admin.category.index');
    }

    public function imageDestroy(Request $request){
      //dd($request);
      $img = Image::where('id', $request->id)->where('siteID', Auth::user()->siteID)->first();
      //dd($img);
      if($img !== null){
      Image::with('categories')->where('images', $img->images)->delete();
      Storage::delete($img->images);
      $images = DB::table('image')->where('image', $img->images)->get();
      //dd($images);
      foreach ($images as $i) {
        DB::table('image')->where('image', $img->images)->delete();
        Storage::delete($i->fullimagesize);
      }
      }
    }

    public function deleted(){
      return view('admin.categories.index', [
        'categories' => DB::table('deleted_categories as categories')
        ->select('categories.title', 'categories.published', 'categories.id', 'i.images', 'categories.main')
        ->where('categories.siteID',  Auth::user()->siteID)->leftJoin('images as i', 'categories.id', '=', 'i.category')->paginate(40),
        'Users' => Users::get(),
        'sites' => DB::table('sites')->get(),
        'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
        'deleted' => 1,
        'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
      ]);
    }

    public function return(Request $request){
        //dd($request);

      $category =  DB::table('deleted_categories')
        ->where('siteID', Auth::user()->siteID)
        ->where('id', $request->id)
        ->first();
        if($category == null){
          echo "Данная категория не обноружена";
          die;
        }
        Category::insert([
          'id' => $category->id, 'title' => $category->title, 'slug' => $category->slug, 'main' => $category->main, 'type' => $category->type,
          'parent_id' => $category->parent_id, 'published' => $category->published, 'created_by' => $category->created_by, 'updated_at' => $category->updated_at,
          'created_at' => $category->created_at, 'siteID' => $category->siteID
        ]);
        DB::table('deleted_categories')->where('id', $request->id)->delete();

        return redirect()->route('admin.category.index');
    }

    public function ShowHide(Request $request){
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      if($request->type == 1){
        $categories = Category::with('images')->with('goods')->where('siteId', Auth::user()->siteID)->where('deleted', 1)->where('published',0)->get();
      }
      else{
        $categories = Category::with('images')->with('goods')->where('siteId', Auth::user()->siteID)->where('deleted', 0)->where('published',0)->get();
      }
      $countActive = Category::where('siteID', Auth::user()->siteID)->where('published', 1)->count('id');
      $countHide = Category::where('siteID', Auth::user()->siteID)->where('published', 0)->count('id');
      return view('admin.categories.index',[
        'company' => $company,
        'categories' => $categories,
        'siteID' =>  Auth::user()->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
      ]);
    }

    public function main(Request $request){
      $backdoor = Category::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('title');
      //dd($backdoor);
      if($backdoor == null){
        return redirect()->route('admin.category.index');
      }
    $main = Category::where('id', $request->id)->value('main');
      if($main == 0){
      //  dd($request);
        Category::where('id', $request->id)->update(['main'=> 1]);
      }
      elseif($main == 1){
        Category::where('id', $request->id)->update(['main'=> 0]);
      }
      //Category::where('id', $request->id)->update(['main', ]);
      return redirect()->route('admin.category.index');
    }
    public function dragAndDropCategory(){
      $i = 1;
      foreach ($_GET['arrayorder'] as $s) {
        //dd($s);
        Category::where('id', $s)->update(['sort'=> $i]);
        $i++;
      }

    }

    public function sortCategory(Request $request){

      if($request->type == 3){
        $categories = Category::with('images')->with('subcategoriesAll')->with('goods')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1)->orderby('sort', 'asc')->get();
      }
      else{
        $categories = Category::with('images')->with('subcategoriesAll')->with('goods')->where('type', $request->type)->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1)->orderby('sort', 'asc')->get();
      }

      $view = '';
      $view .= view('admin.categories.categories', ['categories' => $categories]);
      return $view;
    }


    public function CategorySubcategory($category, Request $request){


      $company = Resize::company();
        //dd(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/subcategory/'.$category);
        if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/subcategory/'.$category){
          //dd('111');
        $categories = Category::with('goods')->where('parent_id', $category)->where('siteID', Auth::user()->siteID)->where('published',1)->orderby('sort', 'asc')->paginate(40);

        }
        else{
          //dd('22');
          $categories = Category::with('goods')->where('parent_id', $category)->where('siteID', Auth::user()->siteID)->where('published',0)->orderby('sort', 'asc')->paginate(40);
        }
        // /dd($categories);
        //dd($categories);

        //dd($request);
      $countActive = Category::where('parent_id', $category)->where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
      $countHide = Category::where('parent_id', $category)->where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
      $urlActive = '/admin/category/subcategory/'.$category;
      $urlHidden = '/admin/category/subcategory/hidden/'.$category;
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }

      $view .= view('admin.categories.index',[
        'company' => $company,
        'categories' => $categories,
        'siteID' =>  Auth::user()->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'urlSub' => 1,
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden,
      ]);
      return $view;
    }

    public function imageUpload(Request $request){
      if($request->file('file') !== null){
      $img =  $request->file('file');
        $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
        ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
       $id =  Image::insertGetId(['images' => 'public/'.$filename, 'category' => $request->id, 'siteID' => Auth::user()->siteID]);
        $data = ['image' => 'public/'.$filename, 'id' => $id];
      return json_encode($data);
      }

    }

    public function description(Request $request){
      Category::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['description' => $request->description, 'offer' => $request->offer, 'status' => 2, 'published' => 1, 'updated_at' => date('Y-m-d H:i:s')]);
      $request['ajax'] = 'true';
      return  self::index($request);
    }

    public function excelExport(){
      $categories = Category::where('siteID', Auth::user()->siteID)->get();
      //dd($categories);
      //dd($goods[2]);
      //dd($goods[0]->subcategories[0]->goods);
      $excelName = 'Excel-Export-'.date('d.m.Y');
      //dd($goods);
      //dd($goods);
      Excel::create($excelName, function($excel) use($categories) {
                    $excel->sheet('Товары', function($sheet) use($categories) {
                        $titleExcel = array(
                            array('id',
                                'siteID',
                                'parent_id',
                                'seotitle',
                                'seoname',
                                'seodescription',
                                'seokeywords',
                                'title',
                                'description',
                                'slug',
                                'main',
                                'type',
                                'published',
                                'open_cat',
                                'updated_at',
                                'created_at',
                                'youtube',
                                'deleted',
                                'offer',
                                'sort',
                                'checkseo',
                                'status',
                            ),
                            array(),
                            array(),
                        );
                        $categoryArray = array();
                        $sabcategoryArray = array();

                        foreach ($categories as $category) {
                                $titleExcel[] = [$category->id, $category->siteID, $category->parent_id, $category->seotitle, $category->seoname, $category->seodescription,
                               $category->seokeywords, $category->title, $category->description, $category->slug, $category->main, $category->type, $category->published,
                              $category->open_cat, $category->updated_at, $category->created_at, $category->youtube, $category->deleted, $category->offer,
                            $category->sort,$category->checkseo,$category->status,];

                        }
                        //dd($titleExcel);

                        $sheet->fromArray($titleExcel, null, 'A1', true, false);
                        $sheet->setColumnFormat(array(
                            'C' => '@'
                        ));
                    });
                })->store("xlsx", storage_path('app/excel'));
              //  ExcelUpload::insert(['title' => $excelName, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
                //dd(Storage::path('excel\\'.$excelName));
                return response('<a href="\\storage\\app\\excel\\'.$excelName.'.xlsx" style="margin-right:20px;color:rgb(87, 177, 228);">Выгрузка '.$excelName.'</a>');
    }

    public function uploadExcel(Request $request){
      $book = Excel::load($request->file('file'));
      $sheet = $book->getSheet(0);

      $highestRow = $sheet->getHighestRow();
      $category = [];

      $oldCategoryID = [];
      for ($i = 4; $i <= $highestRow; $i++) {
          $value = (int)$book->getSheet(0)->getCellByColumnAndRow(0, $i)->getValue();
          $category['id'] = $value;
          $value = (int)$book->getSheet(0)->getCellByColumnAndRow(1, $i)->getValue();
          $category['siteID'] = $value;
          $value = (int)$book->getSheet(0)->getCellByColumnAndRow(2, $i)->getValue();
          $category['parent_id'] = $value;
          $value = $book->getSheet(0)->getCellByColumnAndRow(3, $i)->getValue();
          $category['seotitle'] = $value;
          $value = $book->getSheet(0)->getCellByColumnAndRow(4, $i)->getValue();
          $category['seoname'] = $value;
          $value = $book->getSheet(0)->getCellByColumnAndRow(5, $i)->getValue();
          $category['seodescription'] = $value;
          $value = $book->getSheet(0)->getCellByColumnAndRow(6, $i)->getValue();
          $category['seokeywords'] = $value;
          $value = $book->getSheet(0)->getCellByColumnAndRow(7, $i)->getValue();
          $category['title'] = $value;
          $value = $book->getSheet(0)->getCellByColumnAndRow(8, $i)->getValue();
          $category['description'] = $value;
          $value = $book->getSheet(0)->getCellByColumnAndRow(9, $i)->getValue();
          $category['slug'] = $value;
          $value = (int)$book->getSheet(0)->getCellByColumnAndRow(10, $i)->getValue();
          $category['main'] = $value;
          $value = (int)$book->getSheet(0)->getCellByColumnAndRow(11, $i)->getValue();
          $category['type'] = $value;
          $value = (int)$book->getSheet(0)->getCellByColumnAndRow(12, $i)->getValue();
          $category['published'] = $value;
          $value = $book->getSheet(0)->getCellByColumnAndRow(13, $i)->getValue();
          $category['open_cat'] = $value;
          $value = $book->getSheet(0)->getCellByColumnAndRow(14, $i)->getValue();
          $category['updated_at'] = $value;
          $value = $book->getSheet(0)->getCellByColumnAndRow(15, $i)->getValue();
          $category['created_at'] = $value;
          $value = $book->getSheet(0)->getCellByColumnAndRow(16, $i)->getValue();
          $category['youtube'] = $value;
          $value = (int)$book->getSheet(0)->getCellByColumnAndRow(17, $i)->getValue();
          $category['deleted'] = $value;
          $value = $book->getSheet(0)->getCellByColumnAndRow(18, $i)->getValue();
          $category['offer'] = $value;
          $value = (int)$book->getSheet(0)->getCellByColumnAndRow(19, $i)->getValue();
          $category['sort'] = $value;
          $value = (int)$book->getSheet(0)->getCellByColumnAndRow(20, $i)->getValue();
          $category['checkseo'] = $value;
          $value = (int)$book->getSheet(0)->getCellByColumnAndRow(21, $i)->getValue();
          $category['status'] = $value;
          Category::where('id', $category['id'])->update(['seotitle' => $category['seotitle'], 'seoname' => $category['seoname'], 'seodescription' => $category['seodescription'], 'seokeywords' => $category['seokeywords'], 'title' => $category['title'], 'description' => $category['description'],
             'slug' => $category['slug'], 'main' => $category['main'], 'type' => $category['type'], 'published' => $category['published'], 'open_cat' => $category['open_cat'], 'youtube' => $category['youtube'], 'deleted' => 0, 'offer' => $category['offer'],
             'sort' => $category['sort'], 'checkseo' => $category['checkseo'], 'status' => $category['status'], 'created_at' => date('Y-m-d H:i:s')]);
          // if($category['parent_id'] == null){
          //   $id = Category::max('id') + 1;
          //   $newCategoryID = Category::insertGetId(['id' => $id,'seotitle' => $category['seotitle'], 'seoname' => $category['seoname'], 'seodescription' => $category['seodescription'], 'seokeywords' => $category['seokeywords'], 'title' => $category['title'], 'description' => $category['description'],
          //   'slug' => $category['slug'], 'main' => $category['main'], 'type' => $category['type'], 'published' => $category['published'], 'open_cat' => $category['open_cat'], 'youtube' => $category['youtube'], 'deleted' => 0, 'offer' => $category['offer'],
          // 'sort' => $category['sort'], 'checkseo' => $category['checkseo'], 'status' => $category['status'], 'siteID' => $category['siteID'], 'created_at' => date('Y-m-d H:i:s')]);
          // $oldCategoryID[$category['id']] = $newCategoryID;
          // }
          // else{
          //   if(!isset($oldCategoryID[$category['parent_id']])){
          //     $oldCategoryID[$category['parent_id']] = Category::max('id')+ 1 + $category['parent_id'] - $category['id'];
          //   }
          //   $id = Category::max('id') + 1;
          //   //dd($oldCategoryID[$category['parent_id']]);
          //   $newCategoryID = Category::insertGetId(['id' => $id,'seotitle' => $category['seotitle'], 'seoname' => $category['seoname'], 'seodescription' => $category['seodescription'], 'seokeywords' => $category['seokeywords'], 'title' => $category['title'], 'description' => $category['description'],
          //   'slug' => $category['slug'], 'main' => $category['main'], 'type' => $category['type'], 'published' => $category['published'], 'open_cat' => $category['open_cat'], 'youtube' => $category['youtube'], 'deleted' => 0, 'offer' => $category['offer'],
          // 'sort' => $category['sort'], 'checkseo' => $category['checkseo'], 'status' => $category['status'], 'siteID' => $category['siteID'], 'created_at' => date('Y-m-d H:i:s'),
          // 'parent_id' => $oldCategoryID[$category['parent_id']]]);
          // $oldCategoryID[$category['id']] = $newCategoryID;
          // }
          // $image = Image::where('category', $category['id'])->first();
          // if($image !== null && file_exists('storage/app/'.$image->images)){
          //   //dd($category);
          //   $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
          //   Storage::copy($image->images, $newImage);
          //   Image::insertGetId(['images' => $newImage, 'category' => $newCategoryID, 'siteID' => $id, 'created_at' => date('Y-m-d H:i:s')]);
          // }
      }
      return redirect()->back();
    }

    public function coverUpload(Request $request){
    $cover =  Category::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('cover');
    if($cover !== null){
      Storage::delete($cover);
    }
      $img =  $request->file('file');
      $path = Storage::putFile('public', $img);
      Category::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['cover'=> $path]);
      return ['image' => Category::where('id', $request->id)->value('cover')];
    }
}
