<?php

namespace App\Http\Controllers\admin;

use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Image;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Users;
use ImageResize;
use App\Site;
use App\Information;
use App\Contact;
use App\Text;
use App\Template;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      $news = News::where('siteID', Auth::user()->siteID)->where('published',1)->paginate(10);
      $countShow = News::where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
      $countHidden = News::where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
      return view('admin.sections.news.index',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'news'=>$news,
        'countHidden' => $countHidden,
        'countShow' => $countShow
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      return view('admin.sections.news.create', [
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'news'=> null,
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->file('file') !== null){
        $value = $request->file('file');
           $path = Storage::putFile('public', $value);
       }
       else{
         $path = null;
       }

      $company = News::insert(['title' => $request->title,
                                  'text' => $request->text,
                                  'description' => $request->description,
                                  'published' => 1,
                                  'image' => $path,
                                  'siteID' => Auth::user()->siteID,
                                  'created_at' => date('Y-m-d H:i:s')]);

      return redirect()->route('admin.news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
      $news = News::where('siteID', Auth::user()->siteID)->where('id', $news->id)->first();
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      return view('admin.sections.news.create', [
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'news'=> $news,
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
      if($request->file('file') == null){
        $path = $news->image;
      }
      else{
        $value = $request->file('file');
           $path = Storage::putFile('public', $value);
            Storage::delete($news->image);
      }
      $doc = News::where('id', $news->id)
      ->where('siteID', Auth::user()->siteID)
      ->update(['title' => $request->title,
                                  'text' => $request->text,
                                  'description' => $request->description,
                                  'image' => $path,]);
        return redirect()->route('admin.news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        //
    }
    public function hide(Request $request){
      $backdoor = News::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('published');
      if($backdoor == 0){
        News::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 1]);
      }
      elseif($backdoor == 1){
        News::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 0]);
      }
      return redirect()->route('admin.news.index');
    }

    public function main(Request $request){
      $backdoor = News::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('main');
      if($backdoor == 0){
        News::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['main' => 1]);
      }
      elseif($backdoor == 1){
        News::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['main' => 0]);
      }
      return redirect()->route('admin.news.index');
    }
    public function hidden(){
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      $news = News::where('siteID', Auth::user()->siteID)->where('published',0)->paginate(10);
      $countShow = News::where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
      $countHidden = News::where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
      return view('admin.sections.news.index',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'news'=>$news,
        'countHidden' => $countHidden,
        'countShow' => $countShow
      ]);
    }
    public function imageDelete(Request $request){

     $backdoor =  News::where('id', $request->id)->where('siteID', auth::user()->siteID)->first();

      if($backdoor !== null){
        Storage::delete($backdoor->image);
        News::where('id', $request->id)->update(['image' => null]);
      }
    }
}
