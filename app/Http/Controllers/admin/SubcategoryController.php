<?php

namespace App\Http\Controllers\admin;

use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Users;
use App\Image;
use App\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Company;
use ImageResize;
use App\Site;
use App\Property;
use App\Category;
use Caching;
class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $id = Subcategory::insertGetId(['category' => $request->category, 'title' => $request->title, 'slug' => str_slug($request->title, "-"), 'published' => 1, 'siteID' => Auth::user()->siteID, 'description' => $request->text, 'offer' => $request->offer]);
      if($request->file('file') !== null){
        $file =  $request->file('file');
          $path = Storage::putFile('public', $file);
          Image::insert(['images' => $path, 'subcategory' => $id, 'siteID' => Auth::user()->siteID]);
        }
        Caching::productAnother($id);
        return redirect()->route('admin.category.edit', $request->category);
        //dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subcategory  $subcategory
     * @return \Illuminate\Http\Response
     */
    public function show(Subcategory $subcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subcategory  $subcategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Subcategory $subcategory)
    {
    //  dd($subcategory);
      $backdoor = Subcategory::where('id', $subcategory->id)->where('siteID', Auth::user()->siteID)->value('id');
      if($backdoor == null){
        return redirect()->route('admin.category.index');
      }
      //dd($category);
      $subcategory = Subcategory::with('images')->with('categor')->where('id', $subcategory->id)->first();
      $categories = Category::where('siteID', Auth::user()->siteID)->get();
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      return view('admin.categories.subcategoryCreate',[
        'company' => $company,
        'subcategory' => $subcategory,
        'categories' => $categories,
        'siteID' => Auth::user()->siteID,
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subcategory  $subcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subcategory $subcategory)
    {
      //dd($subcategory);

        Subcategory::where('id', $subcategory->id)->update(['title' => $request->title, 'slug' => $request->slug, 'published' => $request->published, 'description' => $request->text, 'offer' => $request->offer]);

        if($request->file('file') !== null){
          $image = Image::where('subcategory', $subcategory->id)->value('images');
          Storage::delete($image);
          Image::where('subcategory', $subcategory->id)->delete();
          $file =  $request->file('file');
            $path = Storage::putFile('public', $file);
            Image::insert(['images' => $path, 'subcategory' => $subcategory->id, 'siteID' => Auth::user()->siteID]);

        }



      Caching::subcategory($subcategory->id);
      Caching::productAnother($subcategory->id);
      return redirect()->route('admin.category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subcategory  $subcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subcategory $subcategory)
    {
        //
    }
    public function dragAndDropSubategory(){
      $i = 1;
      foreach ($_GET['arrayorder'] as $s) {
        //dd($s);
        Subcategory::where('id', $s)->update(['sort'=> $i]);
        $i++;
      }
    }
    public function imageDestroy(Request $request){
      $img = Image::where('id', $request->id)->where('siteID', Auth::user()->siteID)->first();
      //dd($img);
      if($img !== null){
      Image::with('categories')->where('id', $img->id)->delete();
      Storage::delete($img->images);
      }
    }

    public function CategorySubcategory($category, Request $request){
      //dd($category);

      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
        //dd(explode('?',$_SERVER['REQUEST_URI'])[0]);
        if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/subcategory/'.$category){
          //dd('111');
        $categories = Subcategory::with('goods')->where('category', $category)->where('siteID', Auth::user()->siteID)->where('published',1)->orderby('sort', 'asc')->paginate(10);

        }
        else{
          //dd('22');
          $categories = Subcategory::with('goods')->where('category', $category)->where('siteID', Auth::user()->siteID)->where('published',0)->orderby('sort', 'asc')->paginate(10);
        }

        // /dd($categories);
        //dd($categories);

        //dd($request);
      $countActive = Subcategory::where('category', $category)->where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
      $countHide = Subcategory::where('category', $category)->where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
      $urlActive = '/admin/category/subcategory/'.$category;
      $urlHidden = '/admin/category/subcategory/hidden/'.$category;
      return view('admin.categories.index',[
        'company' => $company,
        'categories' => $categories,
        'siteID' =>  Auth::user()->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'urlSub' => 1,
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden,
      ]);
    }
}
