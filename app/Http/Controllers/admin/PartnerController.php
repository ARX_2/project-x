<?php

namespace App\Http\Controllers\admin;

use App\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Image;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Users;
use ImageResize;
use App\Site;
use App\Information;
use App\Contact;
use App\Text;
use App\Template;
class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
       $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
       $partners = Partner::where('siteID', Auth::user()->siteID)->paginate(10);
       $countShow = Partner::where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
       $countHidden = Partner::where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
       return view('admin.sections.partners.index',[
         'company' => $company,
         'siteID' => Auth::user()->siteID,
         'partners'=>$partners,
         'countHidden' => $countHidden,
         'countShow' => $countShow
       ]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
       $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
       return view('admin.sections.partners.create', [
         'company' => $company,
         'siteID' => Auth::user()->siteID,
         'partner'=> null,
       ]);
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
       if($request->file('file') !== null){
         $value = $request->file('file');
            $path = Storage::putFile('public', $value);
        }
        else{
          $path = null;
        }

       $partners = Partner::insert(['title' => $request->title,
                                   'link' => $request->link,
                                   'description' => $request->description,
                                   'published' => 1,
                                   'image' => $path,
                                   'siteID' => Auth::user()->siteID]);

       return redirect()->route('admin.partners.index');
     }

     /**
      * Display the specified resource.
      *
      * @param  \App\News  $news
      * @return \Illuminate\Http\Response
      */
     public function show(Partner $partners)
     {
         //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  \App\News  $news
      * @return \Illuminate\Http\Response
      */
     public function edit(Partner $partners, Request $request)
     {
       $partners = Partner::where('siteID', Auth::user()->siteID)->where('id', $request->id)->first();
       $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
       return view('admin.sections.partners.create', [
         'company' => $company,
         'siteID' => Auth::user()->siteID,
         'partner'=> $partners,
       ]);
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  \App\News  $news
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, Partner $partners)
     {
       $partner = Partner::where('id', $request->id)->where('siteID', Auth::user()->siteID)->first();

       if($request->file('file') == null){
         $path = $partner->image;
       }
       else{
         $value = $request->file('file');
            $path = Storage::putFile('public', $value);
             Storage::delete($partner->image);
       }
       $partners = Partner::where('id', $partner->id)
       ->where('siteID', Auth::user()->siteID)
       ->update(['title' => $request->title,
                                   'link' => $request->link,
                                   'description' => $request->description,
                                   'image' => $path,]);
         return redirect()->route('admin.partners.index');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  \App\News  $news
      * @return \Illuminate\Http\Response
      */
     public function destroy(Partner $partners)
     {
         //
     }
     public function hide(Request $request){
       $backdoor = Partner::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('published');
       if($backdoor == 0){
         Partner::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 1]);
       }
       elseif($backdoor == 1){
         Partner::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 0]);
       }
       return redirect()->route('admin.partners.index');
     }

     public function main(Request $request){
       $backdoor = Partner::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('main');
       if($backdoor == 0){
         Partner::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['main' => 1]);
       }
       elseif($backdoor == 1){
         Partner::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['main' => 0]);
       }
       return redirect()->route('admin.partners.index');
     }
     public function hidden(){
       $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
       $partners = Partner::where('siteID', Auth::user()->siteID)->where('published',0)->paginate(10);
       $countShow = Partner::where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
       $countHidden = Partner::where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
       return view('admin.sections.partners.index',[
         'company' => $company,
         'siteID' => Auth::user()->siteID,
         'partners'=>$partners,
         'countHidden' => $countHidden,
         'countShow' => $countShow
       ]);
     }

     public function imageDelete(Request $request){

      $backdoor =  Partner::where('id', $request->id)->where('siteID', auth::user()->siteID)->first();

       if($backdoor !== null){
         Storage::delete($backdoor->image);
         Partner::where('id', $request->id)->update(['image' => null]);
       }
     }
}
