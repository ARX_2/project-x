<?php

namespace App\Http\Controllers\admin;

use App\Tape;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Users;
use App\Image;
use App\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Company;
use ImageResize;
use App\Site;
use App\Property;
use WebP;
use App\Subcategory;
use App\ExcelUpload;
use App\Goods_object;
use App\Goods_product;
use Resize;
//use App\CategoryGoods;
use Caching;

class TapeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view =  '';
        $view .= view('admin.index', ['company' => Resize::company(),]);
        $tape = Tape::where('published',1)->orderBy('id', 'desc')->paginate(20);
        $countShow = Tape::where('published',1)->count('id');
        $countHidden = Tape::where('published',0)->count('id');
        $view .= view('admin.tape.index',['siteID' => Auth::user()->siteID, 'tape'=>$tape,'countHidden' => $countHidden,'countShow' => $countShow]);
        return $view;
    }
    public function hidden(){
        $view =  '';
        $view .= view('admin.index', ['company' => Resize::company(),]);
        $tape = Tape::where('published',0)->orderBy('id', 'desc')->paginate(20);
        $countShow = Tape::where('published',1)->count('id');
        $countHidden = Tape::where('published',0)->count('id');
        $view .= view('admin.tape.index',['siteID' => Auth::user()->siteID, 'tape'=>$tape, 'countHidden' => $countHidden, 'countShow' => $countShow]);
        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view =  '';
        $view .= view('admin.index', ['company' => Resize::company(),]);
        $view .= view('admin.tape.create', [
            'siteID' => Auth::user()->siteID,
            'tape'=> null,
        ]);
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->file('file') !== null){
            $value = $request->file('file');
            $path = Storage::putFile('public', $value);
        }
        else{
            $path = null;
        }

        $tape = Tape::insert(['title' => $request->title,
            'offer' => $request->offer,
            'description' => $request->description,
            'published' => 1,
            'image' => $path,
            'slug' => str_slug($request->title, "-"),
            'siteID' => Auth::user()->siteID,
            'created_at' => date('Y-m-d H:i:s')]);

        return redirect()->route('admin.tape.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tape  $tape
     * @return \Illuminate\Http\Response
     */
    public function show(Tape $tape)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tape  $tape
     * @return \Illuminate\Http\Response
     */
    public function edit(Tape $tape)
    {
        $view =  '';
        $view .= view('admin.index', ['company' => Resize::company(),]);
        $tape = Tape::where('siteID', Auth::user()->siteID)->where('id', $tape->id)->first();
        $view .= view('admin.tape.create',['siteID' => Auth::user()->siteID, 'tape'=>$tape]);
        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tape  $tape
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tape $tape)
    {
        if($request->file('file') !== null){
            $value = $request->file('file');
            $path = Storage::putFile('public', $value);
        }
        else{
            $path = null;
        }

        $tape = Tape::where('id', $tape->id)
            ->where('siteID', Auth::user()->siteID)
            ->update(['title' => $request->title,
            'offer' => $request->offer,
            'description' => $request->description,
            'published' => 1,
            'image' => $path,
            'slug' => str_slug($request->title, "-"),
            'siteID' => Auth::user()->siteID,
            'updated_at' => date('Y-m-d H:i:s')]);

        return redirect()->route('admin.tape.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tape  $tape
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tape $tape)
    {
        //
    }

    public function hide(Request $request){
        $backdoor = Tape::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('published');
        if($backdoor == 0){
            Tape::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 1]);
        }
        elseif($backdoor == 1){
            Tape::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 0]);
        }
        return redirect()->route('admin.tape.index');
    }

    public function imageDelete(Request $request){

        $backdoor =  Tape::where('id', $request->id)->where('siteID', auth::user()->siteID)->first();

        if($backdoor !== null){
            Storage::delete($backdoor->image);
            Tape::where('id', $request->id)->update(['image' => null]);
        }
    }
}
