<?php

namespace App\Http\Controllers\Admin;

use App\Goods;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Users;
use App\Image;
use App\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Company;
use ImageResize;
use App\Site;
use App\Property;
use WebP;
use App\Subcategory;
use App\ExcelUpload;
use App\Goods_object;
use App\Goods_product;
use Resize;
//use App\CategoryGoods;
use Caching;
use App\Feedback;
use App\Section;
use App\Tel;
use App\Email;
class GoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    //   $stream = ImageResize::make('storage/111.gif')
    // ->stream('gif', 40);
    //dd('asd');

      //return '<img src="'.(string)ImageResize::make('storage/111.gif')->encode('gif', 60).'">';
      //->save('storage/222.gif', 55);

      //dd($request);
      //dd($_SERVER['REQUEST_URI']);
        //ImageResize::make('storage/app/asd.jpg')->encode('webp', 60);
        //dd('sad');
        // ImageResize::make('storage/app/i.jpg')
        // ->fit('1900', '740')
        // //->encode('webp', 5)
        // ->save('storage/app/i2.jpg');

        //dd('sads');
        //dd('asd');
      //dd(\Buglinjo\LaravelWebp\Facades\LaravelWebp::make('storage/app/i2.jpg')->save('storage/app/i2.webp', 90));
      // $products = Goods::with('images')->with('subcategory')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1)->get();
      // foreach ($products as $product) {
      //   Caching::product($product->id);
      // }
      //dd($_SERVER['REQUEST_URI']);
      // $stream = ImageResize::make('storage/app/no-image-170x1701.jpg')
      // ->stream('jpg', 30);
      //
      // ImageResize::make($stream)->save('storage/app/no-image-170x170.jpg', 55);
      // dd($stream);
      //rdd($stream);
      //$stream = '/storage/app/no-image-170x1701.jpg';
    //   if(Auth::user()->id == 27){
    //      $feedback = Feedback::where('created_at', '>', date('Y-m-d H:i:s'))->get();
    //      dd($feedback);
    //      foreach($feedback as $review){
    //          $year = rand(2018, date('Y'));
    //          if($year > date('Y') || $year == date('Y')){
    //              $maxmonth = date('m');
    //          }
    //          else{
    //              $maxmonth = '12';
    //          }
    //          $month = rand(1,$maxmonth);
    //          if($month == 1 || $month == 3 || $month == 5 || $month == 7 || $month == 8 || $month == 10 || $month == 12){
    //              $day = rand(1,31);
    //              if($day<10){
    //                  $day = '0'.$day;
    //              }
    //          }
    //          elseif($month == 2){
    //              $day = rand(1,28);
    //              if($day<10){
    //                  $day = '0'.$day;
    //              }

    //          }
    //          else{
    //              $day = rand(1,30);
    //              if($day<10){
    //                  $day = '0'.$day;
    //              }
    //          }
    //          if($month<10){
    //              $month = '0'.$month;
    //          }

    //          $hour = rand(9, 20);
    //          if($hour<10){
    //              $hour = '0'.$hour;
    //          }
    //          $min = rand(1, 59);
    //          if($min<10){
    //              $min = '0'.$min;
    //          }
    //          $sec = '00';
    //          $date = $year.'-'.$month.'-'.$day.' '.$hour.':'.$min.':'.$sec;
    //          Feedback::where('id', $review->id)->update(['created_at' => $date]);

    //      }
    //      dd($date);
    //   }
    // if(Auth::user()->id == 27){
    //     $sites = Site::where('type', 3)->get();
    //     foreach($sites as $site){
    //         $siteID = $site->id;
    //          if (Cache::has('menu_1_'.$siteID)) {
    //   Cache::forget('menu_1_'.$siteID);//slug = "contact" asc
    // }

    // $company = Site::with('about')->with('info')->where('id',$siteID)->first();
    // //dd($company);
    // $menu = Section::where('siteiD',$siteID)->whereIn('slug',['about','product','services','portfolio', 'contact', 'stroitelstvo', 'bani', 'proyekty', 'izgotovleniye'])
    // ->where('published', 1)->orderByRaw('slug = "about" desc, slug = "contact" asc, slug = "portfolio" asc,  main asc')->get();
    // //dd($menu);
    // $tel = Tel::where('siteID', $siteID)->first();
    // $email = Email::where('siteID', $siteID)->first();
    // $categories = Category::where('siteID', $siteID)->where('published', 1)->where('parent_id', null)->get();
    // //dd($services);
    // $view = '';
    // $view .= view('2019.layouts.menu.menu_1', ['menu' => $menu, 'info' => $company->info, 'tel' => $tel, 'email' => $email, 'siteID' => $siteID, 'categories' => $categories, 'logo' => Cache::get('logoOne'.$siteID)]);
    // Cache::forever('menu_1_'.$siteID, $view);
    //     }
    // }

    // if(Auth::user()->id == 27){
    //     $categories =  Category::where('siteID', 80)->with('goods')->get();
    //             Excel::create('excel', function($excel) use($categories, $request) {
    //                 $excel->sheet('Товары', function($sheet) use($categories, $request) {
    //                     $titleExcel = array(
    //                         array('Категория',
    //                             'Название',
    //                             'Описание',
    //                             'Цена',
    //                             'Фото',
    //                             'Популярный товар',
    //                             'В наличии',
    //                         ),
    //
    //                     );
    //                     $categoryArray = array();
    //                     $sabcategoryArray = array();
    //
    //                     foreach ($categories as $category) {
    //
    //                           foreach ($category->goods as $product) {
    //                             if($product->images == null){
    //                                 continue;
    //                             }
    //                             $titleExcel[] = [$category->title, $product->title,  $product->description, $product->coast, 'https://mebeli62.ru/storage/app/'.$product->images->images, 'нет', 'да'];
    //                             //dd($titleExcel);
    //                           }
    //
    //
    //                     }
    //                     //dd($titleExcel);
    //
    //                     $sheet->fromArray($titleExcel, null, 'A1', true, false);
    //                     $sheet->setColumnFormat(array(
    //                         'C' => '@'
    //                     ));
    //                 });
    //             })->store("xlsx", storage_path('app/excel'));
    //
    //             return response('<a href="\\storage\\app\\excel\\excel.xlsx" style="margin-right:20px;color:rgb(87, 177, 228);">Выгрузка excel</a>');
    //
    // }
        $urlActive = route('admin.goods.index');
        $urlHidden = route('admin.goods.hidden');
        $urlCreate = route('admin.goods.create');
        //dd('asd');
        // $categories  = Goods::with('category')->where('siteID', 16)->get();
        // dd($categories);

        //dd($categories);
      //dd($company);
      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();

      if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/goods/hidden'){
        //$goods = Goods::with('images')->with('subcategory')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);

        $goods = Goods::with('subcategory')->whereHas('categories', function($query){
          $query->where('type', 0);
          $query->orWhereHas('categor', function($sub){
            $sub->where('type',0);
          });
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

        }
        elseif(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/goods/ShowDeleted'){
          //$goods = Goods::with('images')->with('subcategory')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);

          $goods = Goods::with('subcategory')->whereHas('categories', function($query){
            $query->where('type', 0);
            $query->orWhereHas('categor', function($sub){
              $sub->where('type',0);
            });
          })->where('siteID', Auth::user()->siteID)->where('deleted', 1);

          }

        else{
          $goods = Goods::with('subcategory')->whereHas('categories', function($query){
            $query->where('type', 0);
            $query->orWhereHas('categor', function($sub){
              $sub->where('type',0);
            });
          })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);


        }
        //$categories = Category::where('siteID', Auth::user()->siteID)->with('images')->where('published', 1)->orderBy('sort', 'asc')->get();
        //dd($goods->get());

        //dd($goods->get());

        $product = $goods->orderBy('updated_at', 'desc')->paginate(40);

      $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
        $query->where('type', 0);
        $query->orWhereHas('categor', function($sub){
          $sub->where('type',0);
        });
      })->where('published', 1)->count('id');

      $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
        $query->where('type', 0);
        $query->orWhereHas('categor', function($sub){
          $sub->where('type',0);
        });
      })->where('published', 0)->where('deleted', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
        $query->where('type', 0);
        $query->orWhereHas('categor', function($sub){
          $sub->where('type',0);
        });
      })->where('deleted', 1)->count('id');
      //dd($product);
      //dd($product[0]->images);
      //dd($categories);

      //dd($type);
      //dd($urlCreate);
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
      $view .= view('admin.goods.index',[
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::with('subcategories')->where('siteID', Auth::user()->siteID)->where('type',0)->where('parent_id',null)->get(),
        //'type' => $type,
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden,
        'urlCreate' => $urlCreate,
        'link' => $_SERVER['REQUEST_URI'],
      ]);
      return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      //$product = Goods::with('subcategoriesAll')->with('properties')->where('id', $request->id)->first();
      //dd($product);
        $category = Category::with('subcategoriesAll')->where('type',0)->where('published',1)->where('siteID', auth::user()->siteID)->get();
        $goods =  Goods::where('status',1)->where('siteID', Auth::user()->siteID)->first();
        if($goods == null){
          $id = Goods::insertGetId(['status' => 1, 'siteID' => Auth::user()->siteID,'created_at' => date('Y-m-d H:i:s'),]);
          Goods::where('id', $id)->update(['articul' => $id]);
          $goods = Goods::where('id',$id)->first();
        }
        $view =  '';
        if($request->ajax == null){
          $view .= view('admin.index', ['company' => Resize::company(),]);
        }
      //dd($product);
      $view .= view('admin.goods.create',[
        'categories' => $category,
        'siteID' => $request->siteID,
        'product' => $goods,
      ]);
      return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      //dd($request->Property['title']);
    //dd($request->file('file')->getClientOriginalExtension());
    //$sort = Goods::max('sort');
    // dd($request);
    // //dd('');
    // if($request->subcategory !== 0 && $request->subcategory !== null){
    //   $category = $request->subcategory;
    // }
    // else{
    //   $category = $request->category;
    // }
    // //dd($category);
    // $sort = Goods::max('sort');
    // $id = Goods::insertGetId([
    //    'title' => $request->title, 'short_description' => $request->short_description, 'slug' => $request->slug,
    //    'coast' => $request->coast, 'youtube' => $request->youtube, 'seoname' => $request->seoname,
    //    'seotitle' => $request->seotitle, 'seodescription' => $request->seodescription, 'seokeywords' => $request->seokeywords,
    //    'siteID' => auth::user()->siteID, 'deleted' => 0, 'published' => 1, 'sort' => $sort, 'slug' => str_slug($request->title),
    //    'category' => $category, 'created_at' => date('Y-m-d H:i:s'),
    //  ]);

   //   if($request->file('file') !== null){
   //   foreach ($request->file('file') as $img) {
   //     $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
   //     ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
   //     Image::create(['images' => 'public/'.$filename, 'goods' => $id, 'siteID' => Auth::user()->siteID]);
   //   }
   //   }
   // //DB::table('category_goods')->insert(['category'=> $category, 'goods'=> $id]);
   //
   //   return redirect()->route('admin.goods.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Goods  $goods
     * @return \Illuminate\Http\Response
     */
    public function show(Goods $goods)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Goods  $goods
     * @return \Illuminate\Http\Response
     */
    public function edit(Goods $goods, Request $request)
    {
      $backdoor = Goods::where('siteID', auth::user()->siteID)->where('id', $request->id)->value('id');
      $protected = Goods::where('siteID', auth::user()->siteID)->where('id', $request->id)->value('protected');
      if($backdoor == null || $protected == 1 && Auth::user()->userRolle == 6){
        return redirect()->route('admin.goods.index');
      }
      $product = Goods::with('subcategory')->with('imageses')->where('id', $request->id)->first();
      //dd($product->images);
        $category = Category::with('subcategories')->where('type',0)->where('published',1)->where('siteID', auth::user()->siteID)->get();
        //dd($product);
        $view = '';
        if($request->ajax == null){
          $view .= view('admin.index', ['company' => Resize::company(),]);
        }
      //dd($product);

      $view .= view('admin.goods.create',[
        'categories' => $category,
        'product' =>  $product,
        'siteID' => $request->siteID,

      ]);
      return $view;
      //  dd($goods);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Goods  $goods
     * @return \Illuminate\Http\Response
     */
    public function update($slug,$id,$parametr,Request $request, Goods $goods)
    {
      //dd($slug);
      if($request->subcategory !== 0 && $request->subcategory !== null){
        $category = $request->subcategory;
      }
      else{
        $category = $request->category;
      }
      $unparametr[$parametr] = $parametr;
      $unparametr[0] = 1;
      $unparametr[1] = 0;

      if($slug == 'title'){
        $cat = Goods::where('id', $id)->value('status');
        if($cat == 1){
          $update['title'] = ['title' => $parametr, 'slug' => str_slug($parametr, "-"), 'created_at' => date('Y-m-d H:i:s')];
        }
        else{
          $update['title'] = ['title' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
        }
      }

      $update['published'] = ['published' => $unparametr[$parametr], 'deleted' => 0, 'dateDelete' => null, 'updated_at' => date('Y-m-d H:i:s')];
      $update['main'] = ['main' => $unparametr[$parametr], 'updated_at' => date('Y-m-d H:i:s')];
      $update['deleted'] = ['deleted' => $unparametr[$parametr], 'updated_at' => date('Y-m-d H:i:s'), 'dateDelete' => \carbon\Carbon::now()->addDays(30)->toDateTimeString()];

        $update['new'] = ['new' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
        $update['unit'] = ['unit' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
        $update['like'] = ['like' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
        $update['type_cost'] = ['type_cost' => $parametr];
        $update['coast'] = ['coast' => $parametr];
        $update['new_cost'] = ['new_cost' => $parametr];

      $update['category'] = ['category' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
      $update['checkseo'] = ['checkseo' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
      $update['seoname'] = ['seoname' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
      $update['seotitle'] = ['seotitle' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
      $update['seodescription'] = ['seodescription' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
      $update['seokeywords'] = ['seokeywords' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
      $update['update'] = ['title' => $request->title, 'short_description' => $request->short_description,
          'coast' => $request->coast, 'youtube' => $request->youtube, 'seoname' => $request->seoname,
          'seotitle' => $request->seotitle, 'seodescription' => $request->seodescription, 'seokeywords' => $request->seokeywords,
          'checkseo' => $request->checkseo, 'category' => $category, 'updated_at' => date('Y-m-d H:i:s'),];
          if($request->url == ''){
            $update['youtube'] = ['youtube' => null];
          }
          else{
            $update['youtube'] = ['youtube' => 'https://'.$request->url];
          }

    $result =  Goods::with('categories')->where('id', $id)->where('siteID', Auth::user()->siteID)->update($update[$slug]);
      //dd($request->file('file'));
      if($request->file('file') !== null && $result == 1){
      foreach ($request->file('file') as $img) {
        $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
        ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
        Image::create(['images' => 'public/'.$filename, 'goods' => $request->id, 'siteID' => Auth::user()->siteID]);
      }
      }


        //return redirect()->route('admin.goods.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Goods  $goods
     * @return \Illuminate\Http\Response
     */
    public function destroy(Goods $goods)
    {
        //dd($_POST);
        $backdoor = Goods::where('id', $_GET['id'])->where('siteID', Auth::user()->siteID)->first();
        //dd($backdoor);
        if($backdoor == null){
          echo 'Товар не найден';
          die;
        }
        $backdoor = Goods::where('id', $goods->id)->where('siteID', Auth::user()->siteID)->first();
        //dd($backdoor);
        if($backdoor == null){
          echo 'Товар не найден';
          die;
        }
        $image = Image::where('goods', $_POST['id'])->value('images');
        Image::with('categories')->where('images', $image)->delete();
        Storage::delete($image);

        $images = DB::table('image')->where('image', $image)->get();
        //dd($images);
        foreach ($images as $i) {
          DB::table('image')->where('image', $image)->delete();
          Storage::delete($i->fullimagesize);
        }
        Goods::where('id', $_POST['id'])->delete();

        return redirect()->route('admin.goods.index');
    }



    public function imageDestroy(Request $request){
      $img = Image::where('id', $request->id)->where('siteID', Auth::user()->siteID)->first();
      if($img !== null){
      Image::with('categories')->where('images', $img->images)->delete();
      Storage::delete($img->images);
      $images = DB::table('image')->where('image', $img->images)->get();
      //dd($images);
      foreach ($images as $i) {
        DB::table('image')->where('image', $img->images)->delete();
        Storage::delete($i->fullimagesize);
      }
      }
    }

    public function ShowHide(Request $request){
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      //$categories = Category::where('siteId', $request->siteID)->get();

      if($request->type == 1){
        $product = Goods::with('images')->with('categories')->where('siteID', Auth::user()->siteID)->where('deleted', 1)->where('published',0)->paginate(20);
      }
      else{
        $product = Goods::with('images')->with('categories')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0)->paginate(20);
      }
      $countActive = Goods::where('siteID', Auth::user()->siteID)->where('published', 1)->count('id');
      $countHide = Goods::where('siteID', Auth::user()->siteID)->where('published', 0)->count('id');
      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->where('deleted', 1)->count('id');
      //dd($product);
      //dd($product[0]->images);
      //dd($categories);
      return view('admin.goods.index',[
        'company' => $company,
        'product' => $product,
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted
      ]);
    }

    public function ShowDeleted(Request $request){
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      //$categories = Category::where('siteId', $request->siteID)->get();

        $product = Goods::with('images')->with('categories')->where('siteID', Auth::user()->siteID)->where('deleted', 1)->paginate(20);

      $countActive = Goods::where('siteID', Auth::user()->siteID)->where('published', 1)->count('id');
      $countHide = Goods::where('siteID', Auth::user()->siteID)->where('published', 0)->count('id');
      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->where('deleted', 1)->count('id');
      //dd($product);
      //dd($product[0]->images);
      //dd($categories);
      return view('admin.goods.index',[
        'company' => $company,
        'product' => $product,
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted
      ]);
    }


    public function destroyGoods(Request $request){

      if($request->deleted == 1){
        Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['deleted' => 0, 'dateDelete' => null]);
      }
      else{
        Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['deleted' => 1]);
      }
      return redirect()->route('admin.goods.index');
    }

    public function dragAndDropGoods(){
      $i = 1;
      foreach ($_GET['arrayorder'] as $s) {
        Goods::where('id', $s)->update(['sort'=> $i]);
        $i++;
      }
    }
    public function uploadExcel(Request $request){
      //dd($request->file('file'));

      $book = Excel::load($request->file('file'));
      $sheet = $book->getSheet(0);

      $highestRow = $sheet->getHighestRow();
      //dd($highestRow);
      //dd($highestRow);
      //dd($sheet);
      $category = array();
      $goods = array();
      //dd($category);
      //Добавление
      if($request->type == 1){
        //dd($goods);
        Goods::where('siteID', Auth::user()->siteID)->delete();

      for ($i = 4; $i <= $highestRow; $i++) {
          $value = $book->getSheet(0)->getCellByColumnAndRow(0, $i)->getValue();
          if($value !== null){
            $categoryid = (int)$value;
          }

          $value = $book->getSheet(0)->getCellByColumnAndRow(1, $i)->getValue();
          //dd($value);
          $value = $book->getSheet(0)->getCellByColumnAndRow(2, $i)->getValue();
          //dd($value);
          if($value !== null){
            $category['subcategory']['goods']['articulate'] = $value;
          }

          $value = $book->getSheet(0)->getCellByColumnAndRow(3, $i)->getValue();
          if($value !== null){
            $category['subcategory']['goods']['title'] = $value;
          }
          else{
            break;
          }

          $value = $book->getSheet(0)->getCellByColumnAndRow(4, $i)->getValue();
          if($value !== null){
            $category['subcategory']['goods']['description'] = $value;
          }

          else{
            $category['subcategory']['goods']['description'] = null;
          }

          //dd($category);

          $value = $book->getSheet(0)->getCellByColumnAndRow(5, $i)->getValue();
          if($value !== null){
            $value = (int)$value;
            $category['subcategory']['goods']['coast'] = $value;
          }
          else{
            $category['subcategory']['goods']['coast'] = null;
          }

          $value = $book->getSheet(0)->getCellByColumnAndRow(6, $i)->getValue();

          if($value !== null){
            $value = (int)$value;
            $category['subcategory']['goods']['min_count'] = $value;
          }
          else{
            $category['subcategory']['goods']['min_count'] = null;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(7, $i)->getValue();
          if($value !== null){
            $category['subcategory']['goods']['image'] = $value;
          }
          else{
            $category['subcategory']['goods']['image'] = null;
          }

            //dd($category);
            //dd($category);
            $category['subcategory']['goods']['id'] = Goods::max('id')+1;
            Goods::insert(['id' => $category['subcategory']['goods']['id'],
            'category' => $categoryid,
            'title' => $category['subcategory']['goods']['title'],
            //'short_description' => $category['subcategory']['goods']['description'],
            'coast' => $category['subcategory']['goods']['coast'],
            //'articulate' => $category['subcategory']['goods']['articulate'],
            'siteID' => Auth::user()->siteID, 'deleted' => 0, 'published' => 1,
            'min_count' => $category['subcategory']['goods']['min_count']
            ]);
            //dd($category);

            if($category['subcategory']['goods']['image'] !== null){
              Image::insert(['goods' => $category['subcategory']['goods']['id'], 'images' => 'public/'.$category['subcategory']['goods']['image'], 'siteID' => Auth::user()->siteID]);
            }


      }
    }
    else{
      $ExcelFileLast = ExcelUpload::where('siteID', Auth::user()->siteID)->orderBy('created_at','desc')->value('title');
      $ex = Excel::load('/storage/app/excel/'.$ExcelFileLast.'.xlsx');
      $sheetEx = $ex->getSheet(0);
      $highestRowEx = $sheetEx->getHighestRow();
      for ($i = 4; $i <= $highestRow; $i++) {
        $goods['update'] = '';
          $categoryid = $book->getSheet(0)->getCellByColumnAndRow(0, $i)->getValue();
          //dd($value);
          if($categoryid !== null){
            $category['id'] = (int)$categoryid;
          }


          $value = $book->getSheet(0)->getCellByColumnAndRow(1, $i)->getValue();
          $goodsidEx = $ex->getSheet(0)->getCellByColumnAndRow(1, $i)->getValue();
          if($value !== null){
            $goods['id'] =(int) $value;
          }
          else{
            $goods['id'] = null;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(2, $i)->getValue();
          $articulateEx = $ex->getSheet(0)->getCellByColumnAndRow(2, $i)->getValue();
          if($value !== null){
              $goods['articulate'] = $value;
          }
          else{
            $goods['articulate'] = null;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(3, $i)->getValue();
          $titleEx = $ex->getSheet(0)->getCellByColumnAndRow(3, $i)->getValue();
          if($value !== null){
            $goods['title'] = $value;
          }
          else{
            $goods['title'] = null;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(4, $i)->getValue();
          $descriptionEx = $ex->getSheet(0)->getCellByColumnAndRow(4, $i)->getValue();
          if($value !== null){
            $goods['description'] = $value;
          }
          else{
            $goods['description'] = null;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(5, $i)->getValue();
          $coastEx = $ex->getSheet(0)->getCellByColumnAndRow(5, $i)->getValue();
          if($value !== null){
            $goods['coast'] = $value;
          }
          else{
            $goods['coast'] = null;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(6, $i)->getValue();
          $min_countEx = $ex->getSheet(0)->getCellByColumnAndRow(6, $i)->getValue();
          if($value !== null){
            $goods['min_count'] = $value;
          }
          else{
            $goods['min_count'] = null;
          }
          if($i == 8){
            //dd($goods);
          }
          //dd($goods);
          if($goods['id'] !== null){
            if((int)$goods['id'] == (int)$goodsidEx){
              if($goods['articulate'] !== $articulateEx || $goods['description'] !== $descriptionEx || $goods['coast'] !== $coastEx || $goods['min_count'] !== $min_countEx || $goods['title'] !== $titleEx){
                Goods::where('id', (int)$goods['id'])->where('siteID', Auth::user()->siteID)->update(['title' => $goods['title'], 'articulate' => $goods['articulate'], 'short_description' => $goods['description'], 'coast' =>$goods['coast'], 'min_count' => $goods['min_count']]);
              }
            }

          }
          else{

            $goods['id'] = Goods::max('id')+1;
            Goods::insert(['id' => $goods['id'],

            'title' => $goods['title'],
            'short_description' => $goods['description'],
            'coast' => (int)$goods['coast'],
            'articulate' => $goods['articulate'],
            'siteID' => Auth::user()->siteID, 'deleted' => 0, 'published' => 1,
            'min_count' => (int)$goods['min_count']
            ]);
            //dd($category);
            DB::table('category_goods')->insert(['category'=> $$category['id'], 'goods'=> $goods['id']]);
          }
          //dd($goods['title']);
          $value = $book->getSheet(0)->getCellByColumnAndRow(10, $i)->getValue();

          $valueEx = $ex->getSheet(0)->getCellByColumnAndRow(10, $i)->getValue();
          if($value !== null){
            if($value !== $valueEx){
              Image::where('goods', (int)$goods['id'])->update(['images' => 'public/'.$value]);
            }

            else{
              Image::where('goods', $goods['id'])->where('siteID', Auth::user()->siteID)->delete();
              Image::insert(['images' => 'public/'.$value, 'goods' => (int)$goods['id']]);
            }
          }

      }
    }

      return redirect()->route('admin.goods.index');

    }

    public function excelExport(){
      $goods = Category::with('goods')->where('siteID', Auth::user()->siteID)->get();
      //dd($goods[2]);
      $maxexcel = ExcelUpload::max('id');
      //dd($goods[0]->subcategories[0]->goods);
      $excelName = 'Excel-Export-'.date('d.m.Y').'-'.$maxexcel;
      //dd($goods);
      //dd($goods);
      Excel::create($excelName, function($excel) use($goods) {
                    $excel->sheet('Товары', function($sheet) use($goods) {
                        $titleExcel = array(
                            array('id категории',
                              //  'Название категории',
                                //'id подкатегории',
                                //'Название подкатегории',
                                'id товара',
                                'Штрих-код',
                                'Название товара',
                                'Описание',
                                //'Цвет',
                                'Цена',
                                'Минимальный заказ',
                                'Картинка',
                            ),
                            array(),
                            array(),
                        );
                        $categoryArray = array();
                        $sabcategoryArray = array();

                        foreach ($goods as $category) {

                              foreach ($category->goods as $product) {
                                if(isset($categoryArray['id'])){
                                if($categoryArray['id'] == $category->id){
                                  $categoryId = null;
                                  $categoryTitle = null;
                                }
                                else{
                                  $categoryId = $category->id;
                                  $categoryTitle = $category->title;
                                  $categoryArray = ['id' => $category->id];
                                }
                                }
                                else{
                                  $categoryId = $category->id;
                                  $categoryTitle = $category->title;
                                  $categoryArray = ['id' => $category->id];
                                }


                                if($product->parent_id == null){
                                  $parent_id = $product->id;
                                }
                                else{
                                  $parent_id = $product->parent_id;
                                }
                                if($product->images !== null){
                                  $image = explode('/',$product->images->images)[1];
                                }
                                else{
                                  $image = null;

                                }
                                $titleExcel[] = [$category->id, $product->id,  $product->articulate, $product->title, $product->short_description, $product->coast, $product->min_count, $image];
                                //dd($titleExcel);
                              }


                        }
                        //dd($titleExcel);

                        $sheet->fromArray($titleExcel, null, 'A1', true, false);
                        $sheet->setColumnFormat(array(
                            'C' => '@'
                        ));
                    });
                })->store("xlsx", storage_path('app/excel'));
                ExcelUpload::insert(['title' => $excelName, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
                //dd(Storage::path('excel\\'.$excelName));
                return response('<a href="\\storage\\app\\excel\\'.$excelName.'.xlsx" style="margin-right:20px;color:rgb(87, 177, 228);">Выгрузка '.$excelName.'</a>');
                //echo '<a href="\\storage\\app\\excel\\'.$excelName.'.xlsx">'.$excelName.'</a>';
                //echo "<script type='text/javascript'>window.open('" . config('app.url') . "/storage/app/excel/".$excelName.".xlsx');</script>";

    }

    public function sortGoods(Request $request){

        // if($request->min_coast !== null || $request->max_coast !==null){
        //   if($request->id == 0){
        //     $goods = Goods::with('images')
        //     ->with('subcategory')
        //     ->whereBetween('coast', [$request->min_coast, $request->max_coast])
        //     ->where('goods.siteID', Auth::user()->siteID)->where('goods.deleted', 0)->paginate(40);
        //   }
        //   elseif($request->min_coast !== null && $request->max_coast == null && $request->id > 0){
        //     //dd('asd');
        //     $goods = Goods::with('images')
        //     ->with('subcategory')
        //     ->select('goods.id', 'goods.title', 'goods.created_at', 'goods.published', 'goods.deleted', 'goods.updated_at', 'goods.coast', 'goods.checkseo', 'goods.main')
        //     ->join('category_goods as cg', 'goods.id', '=', 'cg.goods')
        //     ->join('subcategories as sub', 'cg.category', '=', 'sub.id')
        //     ->join('categories as c', 'sub.category', '=', 'c.id')
        //     ->where('c.id', $request->id)
        //     ->where('goods.coast', '>', $request->min_coast)
        //     ->where('goods.siteID', Auth::user()->siteID)->where('goods.deleted', 0)->paginate(40);
        //   }
        //   elseif($request->min_coast == null && $request->max_coast !== null && $request->id > 0){
        //     $goods = Goods::
        //     whereBetween('coast', ['100', '200'])
        //     ->where('siteID', Auth::user()->siteID)->get();
        //     dd($goods);
        //   }
        //   else{
        //     //dd($request->max_coast);
        //     $goods = Goods::with('images')
        //     ->with('subcategory')
        //     ->select('goods.id', 'goods.title', 'goods.created_at', 'goods.published', 'goods.deleted', 'goods.updated_at', 'goods.coast', 'goods.checkseo', 'goods.main')
        //     ->join('category_goods as cg', 'goods.id', '=', 'cg.goods')
        //     ->join('subcategories as sub', 'cg.category', '=', 'sub.id')
        //     ->join('categories as c', 'sub.category', '=', 'c.id')
        //     ->where('c.id', $request->id)
        //     ->where('goods.coast', '>', $request->min_coast)
        //     ->where('goods.coast', '<', $request->max_coast)
        //     ->where('goods.siteID', Auth::user()->siteID)->where('goods.deleted', 0)->paginate(40);
        //
        //   }
        // }
        //else{

          if($request->id == 0){
            $goods = Goods::with('images')
            ->with('subcategory')
            ->where('goods.siteID', Auth::user()->siteID)->where('goods.deleted', 0)->paginate(40);
          }
          else{
            Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();
            $subcategories = Category::where('parent_id', $request->id)->get();
            $categories = [];
            $categories[] = (int)$request->id;
            foreach ($subcategories as $key => $value) {
              $categories[] = $value->id;
            }

            $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($categories){
              $query->whereIn('id', $categories);
            })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->paginate(20);
            //dd($goods);
          }
          //dd($goods);
        //}

        $view = '';

        $view .= view('admin.goods.goods', ['product' => $goods]);
        return $view;
    }

    public function CategoryGoods($category,Request $request){


      //dd($type);
      $company = Resize::company();

      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();


      //dd($goods->get());
      //dd(explode('?',$_SERVER['REQUEST_URI'])[0]);
      if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/goods/'.$category){
        //dd('111');
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($category){
          $query->where('id', $category);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);

      }
      else{
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($category){
          $query->where('id', $category);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

      }


      $product = $goods->paginate(40);

    //  dd($product);

      $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('id', $category);
      })->where('published', 1)->count('id');

      $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('id', $category);
      })->where('published', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('id', $category);
      })->where('deleted', 1)->count('id');
      //dd($product);
      //dd($product[0]->images);
      //dd($categories);


      $urlActive = '/admin/category/goods/'.$category;
      $urlHidden = '/admin/category/goods/hidden/'.$category;
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }

      $view .= view('admin.goods.index',[
        'company' => $company,
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::where('siteID', Auth::user()->siteID)->get(),
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden
        //'type' => $type,
      ]);
      return $view;
      //dd($request);
    }

    public function CategoryGoodsAll($category,Request $request){


      //dd($type);
      $company = Resize::company();
      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();

      //dd($categories);


      //dd($goods->get());

      if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/goods/all/'.$category){
        //dd('111');
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($category){
          $query->where('parent_id', $category);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);
      }
      else{
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($category){
          $query->where('parent_id', $category);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

      }


      $product = $goods->paginate(40);

      //dd($product);
      $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('parent_id', $category);
      })->where('published', 1)->count('id');
      $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('parent_id', $category);
      })->where('published', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('parent_id', $category);
      })->where('deleted', 1)->count('id');
      //dd($product);
      //dd($product[0]->images);
      //dd($categories);

      $urlActive = '/admin/category/goods/all/'.$category;
      $urlHidden = '/admin/category/goods/all/hidden/'.$category;
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
      $view .= view('admin.goods.index',[
        'company' => $company,
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::where('siteID', Auth::user()->siteID)->get(),
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden
        //'type' => $type,
      ]);
      return $view;
      //dd($request);
    }

    public function SubcategoryGoods($subcategory, Request $request){
        //dd($subcategory);
      $company = Resize::company();
      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();



        //dd($categoryGoods);
        if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/subcategory/goods/'.$subcategory){
          $goods = Goods::with('subcategory')->where('category', $subcategory)->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);
        }
        else{
          $goods = Goods::with('subcategory')->where('category', $subcategory)->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);
        }
        //$categories = Category::where('siteID', Auth::user()->siteID)->with('images')->where('published', 1)->orderBy('sort', 'asc')->get();
        //dd($goods->get());

        $product = $goods->paginate(40);

      //dd($product);
      $countActive = Goods::where('siteID', Auth::user()->siteID)->where('category', $subcategory)->where('published', 1)->count('id');

      $countHide = Goods::where('siteID', Auth::user()->siteID)->where('category', $subcategory)->where('published', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->where('category', $subcategory)->where('deleted', 1)->count('id');

      //dd($categoryType);

      //dd($type);
      $urlActive = '/admin/subcategory/goods/'.$subcategory;
      $urlHidden = '/admin/subcategory/goods/hidden/'.$subcategory;

      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
      $view .= view('admin.goods.index',[
        'company' => $company,
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::where('siteID', Auth::user()->siteID)->get(),
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden,

        //'type' => $type,
      ]);
      return $view;
    }

    public function imageUpload(Request $request){

      if($request->file('file') !== null){
      $img =  $request->file('file');
        $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
        ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
       $id =  Image::insertGetId(['images' => 'public/'.$filename, 'goods' => $request->id, 'siteID' => Auth::user()->siteID]);
        $data = ['image' => 'public/'.$filename, 'id' => $id];
      return json_encode($data);
      }

    }

    public function description(Request $request){

      Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['short_description' => $request->short_description,'status' => 2, 'updated_at' => date('Y-m-d H:i:s'), 'published' => 1]);
      $request['ajax'] = 'true';
      return  self::index($request);
    }

    public function imagesort(){
      $i = 1;
      foreach ($_GET['arrayorder'] as $s) {
        //dd($s);
        Image::where('id', $s)->update(['sort'=> $i]);
        $i++;
      }
    }

    public function changeTitleSites(Request $request){
      Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['short_description' => $request->short_description,'status' => 2, 'updated_at' => date('Y-m-d H:i:s'), 'published' => 1]);
      $goods = Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->first();

      Goods::where('articul', $goods->articul)->update(['title' => $goods->title, 'short_description' => $goods->short_description, 'coast' => $goods->coast, 'youtube' => $goods->youtube,]);

      if($request->status == 1){
      $type =  Site::where('id', $goods->siteID)->value('type');
      $sites = Site::select('id')->where('type', $type)->get();
      $category = Category::where('id', $goods->category)->value('title');
      $categories = Category::where('title', $category)->where('siteID', '!=', $goods->siteID)->get()->keyBy('siteID');
      $data = [];
      foreach ($sites as $site) {
        if(isset($categories[$site->id])){
          $data[] = ['title' => $goods->title, 'articul' => $request->id, 'short_description' => $goods->short_description, 'coast' => $goods->coast, 'youtube' => $goods->youtube, 'published' => 1, 'status' =>2,  'siteID' => $site->id, 'created_at' => date('Y-m-d H:i:s'), 'category' => $categories[$site->id]->id];
        }

      }
      Goods::insert($data);
      }
      $request['ajax'] = 'true';
      return  self::index($request);
    }

    public function imagesChangeAll($id, Request $request){
      $goods = Goods::where('id', $id)->with('imageses')->first();
      $anotherGoods = Goods::where('articul', $goods->articul)->where('id', '!=', $goods->id)->with('imageses')->get();
      foreach ($anotherGoods as $goodsanother) {
        Image::where('goods', $goodsanother->id)->delete();
        foreach ($goods->imageses as $image) {
          Image::insert(['images' => $image->images, 'goods' => $goodsanother->id, 'siteID' => $goodsanother->siteID]);
        }

      }
    }
}
