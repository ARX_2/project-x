<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use App\Image;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Users;
use ImageResize;
use App\Site;
use App\Information;
use App\Contact;
use App\Text;
use App\Template;
use App\Menu;
use App\Tel;
use App\Email;
use Caching;
use App\Category;
use App\Section;
use App\City;
use Resize;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $company = Resize::company();
      $sites = Site::get();

      if(Cache::has('companyInfo'.Auth::user()->siteID)){
        Cache::forget('companyInfo'.Auth::user()->siteID);
      }
      Cache::forever('companyInfo'.Auth::user()->siteID, $company);
        $visual = DB::table('visual')->where('siteID', Auth::user()->siteID)->first();
        if($visual->menu == null || $visual->menu == 1){Caching::menu();}
        elseif($visual->menu == 2){Caching::menu2();}
        elseif($visual->menu == 3){Caching::menu3();}

        if($visual->footer == null || $visual->footer == 1){Caching::footer();}
        else{Caching::footer2();}

      //dd($company);
      return view('admin.company.index',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'tels' => Tel::where('siteID', Auth::user()->siteID)->get(),
        'emails' => Email::where('siteID', Auth::user()->siteID)->get(),
        'sites' => $sites,
        'cities' => City::get(),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
      $backdoor = Company::where('id', $company->id)->where('siteID', Auth::user()->siteID)->first();
      //dd($backdoor);
      if($backdoor == null){
        echo 'О компании не найдено';
        die;
      }
      if($request->file('image') !== null){
       foreach ($request->file('image') as $value) {
         //dd($article->id);
         $filename  = uniqid() . '.' . $value->getClientOriginalExtension();
       ImageResize::make($value->getRealPath())->save('storage/app/public/'. $filename);

         $image = Image::create(['images' => 'public/'.$filename,'company' => $company->id, ]);
       }
       }

      Company::where('id', $company->id)
      ->update(['title'=> $request->title,
                'short_description'=> $request->short_description,
                'description'=> $request->description,
                'published'=> $request->published,
                'created_by'=> $request->created_by,
                'modified_by'=> $request->modified_by,
            ]);
      return redirect()->route('admin.company.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
      $backdoor = Company::where('id', $company->id)->where('siteID', Auth::user()->siteID)->first();
      //dd($backdoor);
      if($backdoor == null){
        echo 'О компании не найдено';
        die;
      }
      $image = Image::where('category', $company->id)->value('images');
      Image::with('categories')->where('images', $image)->delete();
      Storage::delete($image);
      $images = DB::table('image')->where('image', $image)->get();
      //dd($images);
      foreach ($images as $i) {
        DB::table('image')->where('image', $image)->delete();
        Storage::delete($i->fullimagesize);
      }
      $company->delete();
      return redirect()->route('admin.company.index');
    }



    public function main(Request $request, Company $company){
      //dd($request);
    //Company::where('siteID', Auth::user()->siteID)->update(['title' => $request->title, 'short_description' => $request->description]);
    Site::where('id', Auth::user()->siteID)->update(['title'=> $request->domain, 'description' => $request->description, 'name' => $request->title]);
      return redirect()->route('admin.company.index');
    }


    public function subupdate(Request $request, Company $company){

      $info = Information::where('siteID', Auth::user()->siteID)->value('id');
      if($info !== null){
        Information::where('siteID', Auth::user()->siteID)->update([ 'map' => $request->map, 'address' => $request->address, 'city' => $request->city,  'timeJob' => $request->timeJob]);
      }
      else{
        Information::insert([ 'map' => $request->map, 'address' => $request->address, 'city' => $request->city,  'timeJob' => $request->timeJob, 'siteID' => Auth::user()->siteID]);
      }

      //dd(count($request->tels)>0 && count($request->telsTitles)>0);
      if(isset($request->tels) && count($request->tels)>0 && count($request->telsTitles)>0){
        $i = 0;
        foreach ($request->tels as $tel) {
          Tel::insert(['tel' => $tel, 'title' => $request->telsTitles[$i], 'siteID' => Auth::user()->siteID]);
          $i++;
        }
      }
      //dd($request);
      if(isset($request->emails) && count($request->emails)>0 && count($request->emailsTitles)>0){
        $i = 0;
        foreach ($request->emails as $email) {
          Email::insert(['email' => $email, 'title' => $request->emailsTitles[$i], 'siteID' => Auth::user()->siteID]);
          $i++;
        }
      }

      if(isset($request->update['tel']) && $request->update['tel'] !== null && $request->update['TelsTitles'] !== null){
        foreach ($request->update['tel'] as $idtel => $tel) {
          Tel::where('id', $idtel)->update(['tel' => $tel, 'title' => $request->update['TelsTitles'][$idtel]]);
        }
      }
    //dd(isset($request->update['Emails']) && $request->update['Emails'] !== null && $request->update['EmailsTitles'] !== null);
      if(isset($request->update['Emails']) && $request->update['Emails'] !== null && $request->update['EmailsTitles'] !== null){
        $i = 0;

        foreach ($request->update['Emails'] as $idemail => $email) {
          Email::where('id', $idemail)->update(['email' => $email, 'title' => $request->update['EmailsTitles'][$idemail]]);
          $i++;
        }
      }
      Site::where('id', Auth::user()->siteID)->update(['type' => $request->type,  'country' => $request->country, 'city' => $request->city, 'address' => $request->address]);
      //dd($request->type);
      if($request->type == 0){
        Section::whereIn('slug', ['stroitelstvo', 'bani', 'proyekty', 'izgotovleniye'])->update(['published' => 0]);
      }
      elseif($request->type == 1){
        Section::whereIn('slug', ['stroitelstvo', 'bani', 'proyekty'])->where('siteID', Auth::user()->siteID)->update(['published' => 1]);
        Section::whereIn('slug', ['izgotovleniye'])->where('siteID', Auth::user()->siteID)->update(['published' => 0]);
      }
      else{
        Section::whereIn('slug', ['stroitelstvo', 'bani', 'proyekty'])->where('siteID', Auth::user()->siteID)->update(['published' => 0]);
        Section::whereIn('slug', ['izgotovleniye'])->where('siteID', Auth::user()->siteID)->update(['published' => 1]);
      }

        return redirect()->route('admin.company.index');
    }

    public function requisite(){
      $company = Resize::company();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.company.requisite',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
      ]);
    }
    public function sendRequisite(Request $request){
      Information::where('siteID', Auth::user()->siteID)->update(['INN' => $request->INN, 'KPP' => $request->KPP, 'OGRN' => $request->OGRN,
      'OKPO' => $request->OKPO, 'OKBED' => $request->OKBED, 'OKATO' => $request->OKATO,
      'Bank' => $request->Bank, 'Acount_chek' => $request->Acount_chek, 'Corp_check' => $request->Corp_check, 'BIK' => $request->BIK,'full_name' => $request->full_name, 'short_name'=> $request->short_name, 'legal_address' => $request->legal_address, 'actual_address' => $request->actual_address, 'general_director' => $request->general_director]);
      return redirect()->route('admin.company.requisite');

    }
    public function logo(){
      //dd(Cache::get('ImagelogoOne'.Auth::user()->siteID));
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      $menu = Menu::where('siteID', Auth::user()->siteID)->get();
      $text = Template::with('texts')->where('siteID', Auth::user()->siteID);
      if (Cache::has('menu_1_'.Auth::user()->siteID)) {
        Cache::forget('menu_1_'.Auth::user()->siteID);
      }



      return view('admin.company.logo',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
      ]);
    }

    public function sendLogo(Request $request){
      //dd($request->file('logo1'));
      $information = Information::where('siteID', Auth::user()->siteID)->first();
      if($request->file('logo1') !== null){
        Information::where('siteID', Auth::user()->siteID)->update(['logo1' => $request->file('logo1')]);
        Storage::delete($information->logo1);
        $value = $request->file('logo1');
        $uniq = uniqid();
        $filename  = $uniq . '.'.$value->getClientOriginalExtension();
        //dd($filename);
        ImageResize::make($value->getRealPath())
        //->fit(212, 60)
        //->encode('webp', 60)
        ->save('storage/app/public/'. $filename, 60);
        if (Cache::has('ImagelogoOne'.Auth::user()->siteID)) {
          Cache::forget('ImagelogoOne'.Auth::user()->siteID);
        }
        Cache::forever('ImagelogoOne'.Auth::user()->siteID, $filename);


        $image = Information::where('siteID', Auth::user()->siteID)
        ->update(['logo1' => 'public/'.$filename]);
        $filename  = $uniq .'_5x5'. '.' . $value->getClientOriginalExtension();
        ImageResize::make($value->getRealPath())
        ->fit(5, 5)
        ->save('storage/app/public/'. $filename, 60);
        //$webp = $uniq .'_5x5'.'.'.$value->getClientOriginalExtension();
        //\Buglinjo\LaravelWebp\Facades\LaravelWebp::make('storage/app/public/'.$filename)->save('storage/app/public/'. $webp, 50);
        // if (Cache::has('logoOne'.Auth::user()->siteID)) {
        //   Cache::forget('logoOne'.Auth::user()->siteID);
        // }
        // Cache::forever('logoOne'.Auth::user()->siteID, $webp);


      }
      elseif($request->file('logo2') !== null){
        Information::where('siteID', Auth::user()->siteID)->update(['logo2' => $request->file('logo2')]);
        Storage::delete($information->logo2);
        $value = $request->file('logo2');
        $uniq = uniqid();
        $filename  = $uniq . '.'.$value->getClientOriginalExtension();
        //dd($filename);
        ImageResize::make($value->getRealPath())
        //->fit(212, 60)
        //->encode('webp', 60)
        ->save('storage/app/public/'. $filename, 60);
        if (Cache::has('ImagelogoTwo'.Auth::user()->siteID)) {
          Cache::forget('ImagelogoTwo'.Auth::user()->siteID);
        }
        Cache::forever('ImagelogoTwo'.Auth::user()->siteID, $filename);

        $image = Information::where('siteID', Auth::user()->siteID)
        ->update(['logo2' => 'public/'.$filename]);
        $filename  = $uniq .'5x5'. '.' . $value->getClientOriginalExtension();
        ImageResize::make($value->getRealPath())
        ->fit(5, 5)
        ->save('storage/app/public/'. $filename, 60);
        //$webp = $uniq .'_5x5'.'.'.$value->getClientOriginalExtension();
        //\Buglinjo\LaravelWebp\Facades\LaravelWebp::make('storage/app/public/'.$filename)->save('storage/app/public/'. $webp, 50);
        if (Cache::has('logoTwo'.Auth::user()->siteID)) {
          Cache::forget('logoTwo'.Auth::user()->siteID);
        }
        Cache::forever('logoTwo'.Auth::user()->siteID, $filename);

      }
      elseif($request->file('favicon') !== null){
        Information::where('siteID', Auth::user()->siteID)->update(['favicon' => $request->file('favicon')]);
        Storage::delete($information->favicon);
        $value = $request->file('favicon');
        $filename  = uniqid() . '.' . $value->getClientOriginalExtension();
        ImageResize::make($value->getRealPath())
        ->resize(15, 15,function ($constraint) {
              $constraint->aspectRatio();
              $constraint->upsize();
          })
        ->save('storage/app/public/'. $filename);
         $image = Information::where('siteID', Auth::user()->siteID)
         ->update(['favicon' => 'public/'.$filename]);
         Caching::favicon();
      }
      Caching::menu();
      //dd(Cache::get('menu'.Auth::user()->siteID));
      Caching::footer();
      return redirect()->route('admin.company.logo');
      //dd($request);
    }
    public function description(){
      $company = Resize::company();
      $siteID = Auth::user()->siteID;
      $templates = Template::with(['texts' => function($query) use($siteID){$query->where('siteID', $siteID);}])->with(['section' => function($query) use($siteID){$query->where('siteID', $siteID);}])->where('siteID', Auth::user()->siteID)->orderBy('template', 'asc')->get();

      return view('admin.company.description',[
        'templates' => $templates,
        'company' => $company,
        'siteID' => Auth::user()->siteID,
      ]);
    }
    public function upload(Request $request){
        $company = Resize::company();
        $slug = self::type($request->type);
      $text = Template::with('texts')->with(['section' => function($query){
        $query->where('siteID', Auth::user()->siteID);
      }])->where('slug', $slug)->where('siteID', Auth::user()->siteID)->first();
      return view('admin.company.SendDescription', ['company' => $company, 'text' => $text, 'type' => $request->type]);
    }

    public function about(Request $request){
        dd(12);
        //dd($request);
        $textid = Text::insertGetId(['title' => $request->title, 'offer' => $request->offer, 'text' => $request->text, 'siteID' => Auth::user()->siteID, 'checkseo' => $request->checkseo, 'seoTitle' => $request->seoTitle, 'seoDescription' => $request->seoDescription, 'keywords' => $request->seoKeywords]);
        $id = Text::max('id');
        if($request->file('image') !== null){
        foreach ($request->file('image') as $img) {
          $image = Image::where('text', $id)->first();
          $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
          ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
            Image::create(['images' => 'public/'.$filename, 'text' => $id, 'siteID' => Auth::user()->siteID]);

        }
        }
        $slug = self::type($request->type);
        Template::where('slug', $slug)->where('siteID', Auth::user()->siteID)->update(['setting' => $id]);

      return redirect()->route('admin.company.description');
        //dd($request);
    }
    public function management(Request $request){

        $company = Resize::company();
        $users = Users::where('siteID', Auth::user()->siteID)->whereIn('userRolle', ['2','3','4','5'])->get();
        $userOnSite = Users::where('siteID', Auth::user()->siteID)->where('onSite', 1)->first();
        return view('admin.company.management', ['company' => $company, 'users' => $users,
        'siteID' => Auth::user()->siteID, 'userOnSite' => $userOnSite]);

    }

    public function managementUpdate(Request $request){
      Users::where('siteID', Auth::user()->siteID)->update(['onSite'=> 0]);
      Users::where('siteID', Auth::user()->siteID)->where('id', $request->manager)->update(['timeJob' => $request->timeJob, 'onSite' => 1]);
      return redirect()->route('admin.company.management');
    }
    public function updateDescription(Request $request){

      $backdoor = Text::where('id', $request->id)->where('siteID', Auth::user()->siteID)->first();
      //dd($request->id);
      if($backdoor !== null){
        Text::where('id', $request->id)->update(['title' => $request->title, 'offer' => $request->offer, 'text' => $request->text, 'checkseo' => $request->checkseo, 'seoTitle' => $request->seoTitle, 'seoDescription' => $request->seoDescription, 'keywords' => $request->seoKeywords]);
        $slug = self::type($request->type);
        if($request->checktitle){
          Section::where('siteID', Auth::user()->siteID)->where('slug', $slug)->update(['checktitle' => $request->checktitle, 'title' => $request->name]);
        }
        else{
          Section::where('siteID', Auth::user()->siteID)->where('slug', $slug)->update(['checktitle' => $request->checktitle]);
        }
        if($request->file('image') !== null){
        foreach ($request->file('image') as $img) {
          $image = Image::where('text', $request->id)->first();
          $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
          ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
            Image::create(['images' => 'public/'.$filename, 'text' => $request->id, 'siteID' => Auth::user()->siteID]);

        }
        }
        Template::where('slug', $slug)->where('siteID', Auth::user()->siteID)->update(['setting' => $request->id]);
      }
      return redirect()->route('admin.company.description');


    }
    public function images(Request $request){
      $company = Site::with('about')->with('info')->with('contacts')->where('id', Auth::user()->siteID)->first();
      //dd($company);
      return view('admin.company.images',[
        'company' => $company,
        'siteID' =>  Auth::user()->siteID,
      ]);
    }

    public function imageDestroy(Request $request){

      $img = Image::where('id', $request->id)->where('siteID', Auth::user()->siteID)->first();
      if($img !== null){
      Image::with('categories')->where('images', $img->images)->delete();
      Storage::delete($img->images);
      $images = DB::table('image')->where('image', $img->images)->get();
      //dd($images);
      foreach ($images as $i) {
        DB::table('image')->where('image', $img->images)->delete();
        Storage::delete($i->fullimagesize);
      }
      }
    }
    public function telEmail(Request $request){
      if($request->type == 0){
        Tel::where('id', $request->id)->where('siteID', Auth::user()->siteID)->delete();
      }
      else{
        Email::where('id', $request->id)->where('siteID', Auth::user()->siteID)->delete();
      }
    }

    public function settingUpdate($setting,Request $request){
      $update['verification'] = ['verYa' => $request->verYa, 'verGo' => $request->verGo];
      $update['metrica'] = ['metrika' => $request->metrica];
      $update['analytics'] = ['analytics' => $request->analytics];
      Information::where('siteID', Auth::user()->siteID)->update($update[$setting]);
      return redirect('admin/settings/site/technical/'.$setting);
    }
    static function type($type){
      $slug = ['1' => '/', '2' => 'about', '3' => 'contact', '4' => 'product', '5' => 'services', '6' => 'portfolio', '7' => 'jobs',
               '8' => 'action', '9' => 'news', '10' => 'blog', '11' => 'priceList', '12' => 'faq', '13' => 'partners', '14' => 'documentation',
               '15' => 'stroitelstvo', '16' => 'bani', '17' => 'proyekty', '18' => 'izgotovleniye'];
      return $slug[$type];
    }

    public function socialNetwork(Request $request){
      Information::where('siteID', Auth::user()->siteID)->update(['vkontakte' => $request->vkontakte, 'odnoklassniki' => $request->odnoklassniki, 'instagram' => $request->instagram, 'youtube' => $request->youtube]);
      $footer = DB::table('visual')->where('siteID', Auth::user()->siteID)->value('footer');
      if($footer == 1 || $footer == null){
      }
      else{
        Caching::footer2();
      }
      return redirect()->back();
    }
}
