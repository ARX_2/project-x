<?php

namespace App\Http\Controllers\admin;

use App\Offer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Users;
use App\Image;
use App\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Company;
use ImageResize;
use App\Site;
use App\Property;
class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin.offers.index',[
        'offers' => Offer::with('users')->with('product')->with('offers')->where('parent_id', null)->where('siteID', Auth::user()->id)->paginate(20),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        //
    }
    public function changestatus(Request $request){
      Offer::where('id', $request->id)->update(['status' => $request->status]);
    }

    public function counts(Request $request){
      Offer::where('id', $request->id)->update(['counts' => $request->count]);
      //dd($request);
    }

    static function amount($id){
      $DB = '';
      $offers = Offer::with('offers')->with('product')->where('id', $id)->where('parent_id', null)->first();
      $sum = $offers->product->coast * $offers->counts;
      //dd($offers);
      foreach ($offers->offers as $offer) {
        $sum = $sum + $offer->product->coast * $offer->counts;

      }
      $merchant = '2021';		// ID мерчанта
      $terminal = '2021-alias'; 		// ID терминала
      $psk = '686dd34d081b2291636766c349421c086a318cc0';			// PSK мерчанта

      // Сгенерированы ранее, например, в корзине покупателя.
      $trackid = rand(1, 999999);   		// Идентификатор операции,
      $amount = $sum;		// Сумма операции
      //dd($amount);
      $action = '4';			// Код транзакции, 1 - покупка
      // Записать amount и action с ключем trackid.
      // $DB->set( $trackid . ‘amt’, $amount );
      // $DB->set( $trackid . ‘action’, $action );
      // $DB->save();

      // Хэш пароля
      $salt = $merchant . $amount . $trackid . $action . $psk;		// Данные хэша
      $hash = sha1( $salt );

      // Http POST request to PaymentInitServlet
      $url = 'https://ecm.sngb.ru:443/ECommerce/PaymentInitServlet';	// тестовый
      //$url = 'https://ecm.sngb.ru:443/Gateway/PaymentInitServlet';		// промышленный

      $params = array(
              'merchant' => $merchant ,
              'terminal' => $terminal ,
              'action' => $action ,
              'amt' => $amount ,
              'trackid' => $trackid ,
              'udf1' => $offers->title,
              'udf2' => 'Номер товара',
              'udf3' => $amount,
              'udf4' => Auth::user()->name,
              'udf5' => $hash
      );

       // Параметры запроса
      $postdata = "";
      foreach ( $params as $key => $value ) $postdata .= "&".rawurlencode($key)."=".rawurlencode($value);

      // POST
      $ch = curl_init();
      //dd($ch);
      curl_setopt ($ch, CURLOPT_URL, $url );
      curl_setopt ($ch, CURLOPT_POST, 1 );
      curl_setopt ($ch, CURLOPT_POSTFIELDS, $postdata );
      curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
      curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
      //dd(curl_exec ($ch));
      // Ответ содержит URL для перехода пользователя на HPP или страницу Error.
      $result = curl_exec ($ch);
      curl_close($ch);
      //dd($result);
      return $result;
    }

    public function notification(){
      //dd($request);
      // Адрес сайта магазина
      echo 'Получилось';
      $currentContext = 'https://rukodelnitsa.povishev.ru';

      // Primary Key,
      // В данном примере используется номер заказа как ключ в БД сайта магазина.
  $ID  = $_REQUEST['paymentid'];

  // Код ошибки и сообщение об ошибке. Пусты, если ошибок нет.
  $error      = $_REQUEST['Error'];
  $errortext  = $_REQUEST['ErrorText'];

  // Данные ранее отправленные в запросе на инициализацию.
  // Используются для аутентификации оповещающего запроса от банка.

  // Данные мерчанта, константы.
  $merchant = '2021';		// ID мерчанта
  $psk = '686dd34d081b2291636766c349421c086a318cc0';			// PSK мерчанта

  // Данные транзакции
  $trackid = $_REQUEST['trackid'];	// Идентификатор операции
  $action = $trackid . '1';	// Тип транзакции
  $amount = '4950';	// Сумма операции, считать по trackid .

// Хэш пароля
  $udf5 = $_REQUEST['udf5'];
  $salt = $merchant . $amount . $trackid . $action . $psk;
  $hash = sha1( $salt );

  // Если нет ошибок
  if (!strcmp($error, "")) {

    // Если хэш совпал
    if ( strcmp( $hash, $udf5) == 0 ) {

        // Записать в БД
        // $DB->set($ID . '.paymentid', $_REQUEST['paymentid']);
        // $DB->set($ID . '.result',    $_REQUEST['result']);
        // $DB->set($ID . '.error',     $_REQUEST['Error']);
        // $DB->set($ID . '.errortext', $_REQUEST['ErrorText']);
        // $DB->set($ID . '.responsecode', $_REQUEST['responsecode']);
        // $DB->set($ID . '.udf1',      $_REQUEST['udf1']);
        // $DB->set($ID . '.udf2',      $_REQUEST['udf2']);
        // $DB->set($ID . '.udf3',      $_REQUEST['udf3']);
        // $DB->set($ID . '.udf4',      $_REQUEST['udf4']);
        // $DB->set($ID . '.tranid',    $_REQUEST['tranid']);
        // $DB->set($ID . '.trackid',   $_REQUEST['trackid']);
        // $DB->save();

        // Оправить пользователя на страницу Result, для просмотра результата.
        $reply      = "REDIRECT=" . $currentContext . "?paymentid=" . $ID;

        // Ответ – строка URL для перехода пользователя к просмотру результата операции.
        // Формат строки REDIRECT=<URL>
        echo $reply;

    }

    // Если хэш не совпал – ничего не делать
}

else {

        // Если параметр error не пуст – отправить пользователя на страницу Error.
        $reply      = "REDIRECT=" . $currentContext . "Error.php?error=" .  $error . "&errortext=" .  $errortext;
        echo $reply;
}

    }

    public function paymentAccess(Request $request){


    }
}

 // а то как то одиноко потому что если я что то сделаю то тебя буду отвлекать только.
 // да и так отвлекаю тебя. Я по сути к тебе заходил чтоб Олю увидеть.
 // Я не каждый день вижу обычную девченку)) Я бы сказал даже слишком редко)
 // Может у тебя почти нет времени на меня, зато когда мы вместе, как ты смеешся..., что все знают как Оле весело
 // Постоянно поеш хорошие песни, мы будто в одном дворе выросли, с тобой смешные фильмы становятся еще смешнее)))
 // Я всегда хотел найти такую Олю)) я знаю Олю уже 1.5 года, а узнал совсем не давно
 // Я вот что хотел сказать. Бывает так, то чего мы ищим всю жизнь, на самом деле совсем рядом, а мы и не замечаем.
 // Вот
