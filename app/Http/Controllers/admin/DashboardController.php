<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Users;
use App\Company;
use Illuminate\Support\Facades\Auth;
use App\Template;
use App\Site;
class DashboardController extends Controller
{
    //
    public function dashboard(){
      return redirect()->route('admin.company.index');
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();

      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.dashboard',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
          'siteURL' => DB::table('sites')->where('id', Auth::user()->siteID)->value('title'),
      ]);
    }
}
