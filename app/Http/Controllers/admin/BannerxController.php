<?php

namespace App\Http\Controllers\admin;

use App\Banner;
use App\Tape;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Users;
use App\Image;
use App\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Company;
use ImageResize;
use App\Site;
use App\Property;
use WebP;
use App\Subcategory;
use App\ExcelUpload;
use App\Goods_object;
use App\Goods_product;
use Resize;
//use App\CategoryGoods;
use Caching;


class BannerxController extends Controller
{
    public function index()
    {
        $view =  '';
        $view .= view('admin.index', ['company' => Resize::company(),]);
        $banner = Banner::where('siteID', Auth::user()->siteID)->where('published',1)->orderBy('id', 'desc')->paginate(20);
        $countShow = Banner::where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
        $countHidden = Banner::where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
        $view .= view('admin.sections.banner2.index',['siteID' => Auth::user()->siteID, 'banner'=>$banner,'countHidden' => $countHidden,'countShow' => $countShow]);
        return $view;
    }

    public function hidden(){
        $view =  '';
        $view .= view('admin.index', ['company' => Resize::company(),]);
        $banner = Banner::where('siteID', Auth::user()->siteID)->where('published',0)->orderBy('id', 'desc')->paginate(20);
        $countShow = Banner::where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
        $countHidden = Banner::where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
        $view .= view('admin.sections.banner2.hidden',['siteID' => Auth::user()->siteID, 'banner'=>$banner, 'countHidden' => $countHidden, 'countShow' => $countShow]);
        return $view;
    }
}