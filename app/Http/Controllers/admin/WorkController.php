<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\Work;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Users;
use App\Category;
use App\Image;
use App\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Company;
class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $work = DB::table('works as w')
        ->select('w.id','w.timeFrom', 'w.timeTo', 'w.name as Kname',
        'w.master', 'w.tel', 'w.status', 'w.pay','w.created_at', 'u.name as Mname',
        'w.servicesid', 's.time', 's.money', 's.services')
        ->leftJoin('users as u', 'w.master', '=', 'u.id')
        ->leftJoin('services as s', 'w.servicesid', '=', 's.id')
        ->orderBy('timeFrom', 'desc')
        ->where('w.siteID', Auth::user()->siteID)
        ->get();

        //dd(Auth::user());
        if(Auth::user()->userRolle >3){
          $count = Work::where('siteID', Auth::user()->siteID)->where('status', 1)->where('master', Auth::user()->id)->count();
        }
        else{
          $count = Work::where('siteID', Auth::user()->siteID)->where('status', 0)->count();
        }

        $telCount = array();

        $i=1;
        foreach($work as $w){
          if($w->status == 2){
          $telCount[$w->tel] = $i;
          $i++;
          }
        }
        //dd($telCount);

        //dd($count);
        //dd($work);
      return view('admin.work.index', [
        'works' => $work,
        'users' => Users::where('userRolle', 4)->where('siteID', Auth::user()->siteID)->get(),
        'Users' => Users::get(),
        'sites' => DB::table('sites')->get(),
        'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
        'countwork' => $count,
        'telCount' => $telCount,
        'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.work.create', [
      'Services' => [],
      'Serviceses' => Services::with('users'),
      'delimiter' => '',
      'Services' => Services::get(),
      'Masters' => Users::where('userRolle', '>', 3)->get(),
      'Users' => Users::get(),
      'sites' => DB::table('sites')->get(),
      'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
      'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $timeFrom = date("Y-m-d H:i:s", strtotime($request->date.' '.$request->time));
      //dd($timeFrom);
      //dd($request->services);
      $servicesTime = Services::where('id', $request->services)->value('time');
      $dateTo = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeFrom)
      ->addMinutes($servicesTime*15)->toDateTimeString();
      //dd($dateTo);

      $contact = Work::create(['master' => $request->master,
                                'name' => $request->name,
                                'tel' => $request->tel,
                                'servicesid' => $request->services,
                                'timeFrom' => $timeFrom,
                                'timeTo' => $dateTo,
                                'status' => 0,
                                'siteID' => Auth::user()->siteID]);
      //dd($contact);
      // Categories
      return redirect()->route('admin.work.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function show(Work $work)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function edit(Work $work)
    {
        dd($work);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Work $work)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function destroy(Work $work)
    {
      Work::where('id', $work->id)->delete();
      return redirect()->route('admin.work.index');
        //dd($work);
    }
    public function master(){
      Work::where('id', $_GET['workid'])->update(['master'=>$_GET['masterid']]);
      return redirect()->route('admin.work.index');
    }

    public function status(){
      Work::where('id', $_GET['workid'])->update(['status'=>$_GET['statusid']]);
      return redirect()->route('admin.work.index');
    }

    public function time(Request $request){
      //dd($_POST);

      $dateFrom = $request->date.' '.$request->time;
      $dateTo = $request->dateTo;
      $master = $request->master;

      $newDate = \carbon\Carbon::createFromFormat('Y-m-d H:i', $dateFrom)
      ->addMinutes($dateTo*15)->toDateTimeString();

      $masterTime = Work::select('timeFrom', 'timeTo', 'id')->where('master', $master)->get();

      foreach ($masterTime as $m) {
        if($m->id == $request->workid){
          Work::where('id', $request->workid)->update(['timeFrom'=>$dateFrom, 'timeTo' => $newDate]);
          return redirect()->route('admin.work.index');
        }
        //dd($m->timeTo);
        if($m->timeFrom <= $dateFrom && $m->timeTo >= $dateFrom || $m->timeFrom < $newDate && $m->timeTo >= $newDate ){
          return redirect()->route('admin.work.index');
        }
        else{
          Work::where('id', $request->workid)->update(['timeFrom'=>$dateFrom, 'timeTo' => $newDate]);
          return redirect()->route('admin.work.index');
        }


      }

      //dd($datetime);

      //dd($_POST);
    }


}
