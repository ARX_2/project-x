<?php

namespace App\Http\Controllers\admin;


use App\Support;
use App\Support_message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Comment;
use App\Company;
use App\SupportFile;
use Illuminate\Support\Facades\Validator;
use Resize;
use DB;
use ImageResize;
use App\Events\NewMessageAdded;
use Image;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class SupportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request){
        $view =  '';
        if($request->ajax == null){
            $view .= view('admin.index', ['company' => Resize::company(),]);
        }

        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $questions = Support::with('user')->where('siteID', $siteID)->orderBy('updated_at', 'desc')->get();
        $view .= view('admin.support.index',[
            'questions' => $questions,
        ]);
        return $view;
    }

    public function view(Request $request, $id){
        $view =  '';
        if($request->ajax == null){
            $view .= view('admin.index', ['company' => Resize::company(),]);
        }


        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $question = Support::where('siteID', $siteID)->where('id', $id)->first();
        $message = Support_message::with('user')->where('siteID',$siteID)->where('support_id',$id)->get();

        $view .= view('admin.support.view',[
            'question' => $question,
            'message' => $message,
        ]);
        return $view;
    }

    public function create()
    {
        $view =  '';
        $view .= view('admin.index', ['company' => Resize::company(),]);
        $view .= view('admin.support.create', [
            'siteID' => Auth::user()->siteID,
            'tape'=> null,
        ]);
        return $view;
    }

    public function addMessage(Request $request)
    {

        $siteID = Cache::get('/' . $request->getHttpHost())->id;

        $error = Validator::make($request->all(), [
            'description' => 'required',
        ]);

        if($error->fails()) {return response()->json(['errors' => $error->errors()->all()]);}

        $message_id = Support_message::insertGetId([
            'siteID' => $siteID,
            'support_id' => $request->support_id ?? null,
            'user_id' => $request->user_id ?? null,
            'description' => htmlspecialchars($request->description) ?? null,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        Support::where('id', $request->support_id ?? null)->update(['updated_at' => date('Y-m-d H:i:s'), 'status' => 2]);

        $message = Support_message::with('user')->where('id',$message_id)->first();

        return view('admin.support.message', [
           'message' => $message
       ]);
    }

    public function deleteQuestion($id)
    {
        return view('admin.support.delete_question', ['id' => $id]);
    }

    public function delete($id)
    {
        $column = Support::onlyTrashed()->get();
        $column = Support::where('id', $id)->delete();

        return 'Удалено!';
        //return route();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

    /*    $error = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
        ]);

        if($error->fails()) {return response()->json(['errors' => $error->errors()->all()]);}
        //dd(Auth::user()->id);

        $siteID = Cache::get('/' . $request->getHttpHost())->id;*/



     /*   if ($request->isMethod('post') && $request->file('select_file')) {

            $this->validate($request, [
                'select_file'  => 'required|image|mimes:jpeg,jpg,png|max:4096'
            ]);

            $image = $request->file('select_file');
            $upload_folder = 'image/original/'.$siteID;

            $new_name = rand() . '.' . $image->getClientOriginalExtension();

              Storage::putFileAs($upload_folder, $image, $new_name);
            $path = Resize::ResizeImage($image, '1903x739');

            dd($path);

       }*/


       /*

       $support_id =  Support::insertGetId([
            'title' => $request->title,
            'user_id' => Auth::user()->id,
            'status' => 1,
            'siteID' => $siteID,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ]);

        $message = Support_message::insert([
            'support_id' => $support_id,
            'siteID' => $siteID,
            'user_id' => Auth::user()->id,
            'description' => $request->description,
            'created_at' => date('Y-m-d H:i:s'),
            ]);

       */

        //dd($request->image);

        $siteID = Cache::get('/' . $request->getHttpHost())->id;

        if ($request->hasFile('image')) {
            $image = $request->file('image');

            $filename = time().'.'.$image->getClientOriginalExtension();

            $location = public_path('images/'.$filename);

           // dd($optimize);
            $img = Image::make($image)->resize(1280, 960, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();})->save($location);
            $optimizerChain = app(\Spatie\ImageOptimizer\OptimizerChain::class)->optimize($location);
          //  $opt = ImageOptimizer::optimize($location);
           dd($optimizerChain);
        }

      return redirect('admin/support');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Support  $support
     * @return \Illuminate\Http\Response
     */
    public function show(Support $support)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Support  $support
     * @return \Illuminate\Http\Response
     */
    public function edit(Support $support)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Support  $support
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Support $support)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Support  $support
     * @return \Illuminate\Http\Response
     */
    public function destroy(Support $support)
    {
        //
    }
    public function new(Request $request){
      return view('admin.support.view', [
        'request' => ['comments' => []],
        'company' => Resize::company(),
        'chats' => [],
      ]);
      //dd($request);
    }

    public function sendComment(Request $request){
      $warning = Support::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('id');
      if($warning == null){
        return redirect()->route('admin.support.index');
      }

      // $value = $request->file('file');
      //dd($value->getClientOriginalExtension());
      //dd($value->getClientOriginalName());
      $comment = Comment::insertGetId(['text' => $request->text,
        'readed' => 0,
        'created_at' => date('Y-m-d H:i:s'),
        'users_id' => Auth::user()->id,
        'siteID' => Auth::user()->siteID,
        'support_id' => $request->id,
      ]);

      foreach ($request['img'] as  $value) {
        $imageName = uniqid().'.jpg';
        ImageResize::make($value)
        ->save('storage/app/public/support/'. $imageName);
        SupportFile::insert(['name' => $imageName, 'path' => 'public/support/'.$imageName, 'comment_id' => $comment, 'user_id' => Auth::user()->id]);

      }

    }

    public function messages($id,Request $request){
      $warning = Support::where('id', $id)->where('siteID', Auth::user()->siteID)->value('id');
      if($warning == null){
        return redirect()->route('admin.support.index');
      }
      return view('admin.support.frame', [
        'request' => Support::with('comments')->with('docs')->where('siteID', Auth::user()->siteID)->where('id', $id)->first(),
        'company' => Resize::company(),
      ]);
    }

    public function readComent($id, $user, Request $request){
      Comment::where('support_id', $id)->where('users_id', '!=', $user)->update(['readed'=> 1]);
    }
    public function changeStatus($id,$params){
      Support::where('id', $id)->update(['status' => $params]);
    }


}

