<?php

namespace App\Http\Controllers\admin;

use App\Site;
use App\ForumMessage;
use App\Forumcategory;
use App\Forumsubcategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Basket;
use App\Category;
use App\Model\Settings;
use App\Model\Information;
use Caching;
use Illuminate\Support\Facades\Cache;
use App\Model\Url_request;
class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::user()->siteID == null){
        return redirect('/account/');
      }
      return view('admin.forum.index',[
        'categories' => Forumcategory::with('subcategories')->get(),
        'company' => Site::with('about')->where('id', Auth::user()->siteID)->first(),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if(Auth::user()->siteID == null){
        return redirect('/account/');
      }
      return view('admin.forum.add',[
        'category' => null,
        'company' => Site::with('about')->where('id', Auth::user()->siteID)->first(),
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if(Auth::user()->siteID == null){
        return redirect('/account/');
      }
        Forumcategory::insert(['title' => $request->title, 'slug' => str_slug($request->title, "-"), 'created_at' => date('Y-m-d H:i:s')]);
        return redirect()->route('admin.forum.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ForumMesage  $forumMesage
     * @return \Illuminate\Http\Response
     */
    public function show(ForumMessage $forumMessage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ForumMesage  $forumMesage
     * @return \Illuminate\Http\Response
     */
    public function edit(Forumcategory $forumcategory, Request $request)
    {
      if(Auth::user()->siteID == null){
        return redirect('/account/');
      }
      return view('admin.forum.add', [
        'category' => Forumcategory::with('subcategories')->where('id', $request->id)->first(),
        'company' => Site::with('about')->where('id', Auth::user()->siteID)->first(),
      ]);
        //dd($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ForumMesage  $forumMesage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ForumMessage $forumMessage)
    {
      if(Auth::user()->siteID == null){
        return redirect('/account/');
      }
        Forumcategory::where('id', $request->id)->update(['title' => $request->title]);
        return redirect()->route('admin.forum.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ForumMesage  $forumMesage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

    }
    public function subcategories(Request $request){
      if(Auth::user()->siteID == null){
        return redirect('/account/index');
      }
      return view('admin.forum.subcategories', [
        'category' => Forumcategory::with('subcategories')->where('id', $request->id)->first(),
        'company' => Site::with('about')->where('id', Auth::user()->siteID)->first(),
      ]);
    }
    public function meesages(Request $request){

      if(Auth::user()->siteID == null){
        return redirect('/account/index');
      }
      if($request->category !== null){
        $categories = Forumcategory::with('subcategories')->where('id', $request->category)->get();
      }
      else{
        $subcategory = Forumsubcategory::where('id', $request->subcategory)->where('archive', '=', null)->value('Forumcategory');
        $id= $request->subcategory;
        $categories = Forumcategory::with(['subcategories' => function($sub) use($id){
          $sub->where('id', $id);
        }])->where('id', $subcategory)->get();
      }
      //dd($subcategory);
      //dd($request);
      return view('admin.forum.messages', [
        'categories' => $categories,
        'company' => Site::with('about')->where('id', Auth::user()->siteID)->first(),
      ]);
    }
    public function subcategoryDelete(Request $request){
      if(Auth::user()->siteID == null){
        return redirect('/account/index');
      }
     $id =  Forumsubcategory::where('id',  $request->subcategory)->value('forumcategory');
      Forumsubcategory::where('id', $request->subcategory)->delete();
      return redirect()->route('admin.forum.subcategories', ['id' =>  $id]);
    }

    public function subcategoryArchive(){
      if(Auth::user()->siteID == null){
        return redirect('/account/index');
      }
     $id =  Forumsubcategory::where('id',  $request->subcategory)->value('forumcategory');
      Forumsubcategory::where('id', $request->subcategory)->update('archive', 1);
      return redirect()->route('admin.forum.subcategories', ['id' =>  $id]);
    }
    public function delete(Request $request){
      if(Auth::user()->siteID == null){
        return redirect('/account/index');
      }
      Forumcategory::where('id', $request->id)->delete();
      return redirect()->route('admin.forum.index');
    }
    public function forum(Request $request){
      //dd(Forumcategory::with('subcategories')->get());

      if($request->bann !== null){
        $bann = $request->bann;
      }
      else{
        $bann = null;
      }
      $site = Site::with(['templates' => function($query){
        $query->where('template', 1);
      }])
      ->with('company')->with('category_price')
      ->where('id', 1)->first();
      $siteID = 16;
        $settings = Settings::where('siteID', 16)->where('type', 1)->first();

      $view ='';
      $url_request = Url_request::where('siteID', $siteID)->where('published', 1)->get();
      $favicon = Information::where('siteID', 16)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
      $canonical = $request->getSchemeAndHttpHost();
      if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Главная страница сайта';}
      if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
      if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
      $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);

      /// header
      if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}

      $view .= view('2019.forum.index',[
        'Forumcategories' => Forumcategory::with('subcategories')->paginate(4),
        'categories' => Category::where('published', 1)->get(),
        'bann' => $bann,
        'company' => $site->company,
        'info' => $site->templates->infos,
        'menu' => $site->templates->menu,
      ]);

      /// footer
      if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
      return $view;
    }
    public function report(Request $request){
        $message = ForumMessage::where('id', $request->message)->with('subcategory')->first();
        ForumMessage::where('id', $request->message)->update(['reportType' => $request->type, 'reporter' => Auth::user()->id]);
        return redirect('/forum/'.$message->subcategory->category->slug.'/'.$message->subcategory->id.'-'.$message->subcategory->slug.'?report=1');
        //dd($request);
    }
    public function subcategoryCreate(Request $request){
      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      $categories = Forumcategory::get();
        return view('2019.forum.subcategory',[
          'company' => $site->company,
          'info' => $site->info,
          'categories' => Category::where('published', 1)->get(),
        ]);
        //dd($request);
    }
    public function subcreate(Request $request){
      if(Auth::user()->bann !== null){
        return redirect('/forum?bann=1');
      }
     $id = Forumsubcategory::insertGetId(['forumcategory' => $request->forumcategory, 'title' => $request->title, 'slug' => str_slug($request->title, "-"),
      'userid' => Auth::user()->id, 'created_at' => date('Y-m-d H:i:s'), 'status' => 1]);
     ForumMessage::insert(['text' => $request->text, 'userid' => Auth::user()->id, 'forumsubcategoryid' => $id, 'created_at' => date('Y-m-d H:i:s')]);
    $slug =  Forumsubcategory::with('category')->where('id', $id)->where('archive', '=', null)->first();
      return redirect('/forum/'.$slug->category->slug.'/'.$slug->id.'-'.$slug->slug);
    }
    public function subcategory($category,$slug, Request $request){
      //dd($request);
      $id = explode('-', $slug)[0];

      if($request->bann !== null){
        $bann = $request->bann;
      }
      else{
        $bann = null;
      }
      if($request->report !== null){
        $report = $request->report;
      }
      else{
        $report = null;
      }
      if(Auth::user() !== null){
      $inbasket = Basket::where('userid', Auth::user()->id)->count('id');
      }
      else{
      $inbasket = 0;
      }
      $siteID = 16;
        $settings = Settings::where('siteID', 16)->where('type', 1)->first();

      $view ='';
      $url_request = Url_request::where('siteID', $siteID)->where('published', 1)->get();
      $favicon = Information::where('siteID', 16)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
      $canonical = $request->getSchemeAndHttpHost();
      if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Главная страница сайта';}
      if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
      if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
      $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);

      /// header
      if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}

      //dd($bann);
      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      $subcategory = Forumsubcategory::with('messages')->with('category')->where('archive', '=', null)->where('id', $id)->first();
      $messages = ForumMessage::where('forumsubcategoryid', $subcategory->id)->with('subcategory')->with('user')->paginate(20);
      //dd($messages);
      $view .= view('2019.forum.messages', [
        'subcategory' => $subcategory,
        'company' => $site->company,
        'info' => $site->info,
        'bann' => $bann,
        'report' => $report,
        'messages' => $messages,
        'categories' => Category::where('published', 1)->get(),
        'inbasket' => $inbasket,
      ]);
      if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
      return $view;
    }

    public function category($slug,Request $request){

      if(Auth::user() !== null){
      $inbasket = Basket::where('userid', Auth::user()->id)->count('id');
      }

      else{
      $inbasket = 0;
      }
      //dd($request);
      if($request->bann !== null){
        $bann = $request->bann;
      }
      else{
        $bann = null;
      }
      //dd($bann);
      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      $siteID = 16;
        $settings = Settings::where('siteID', 16)->where('type', 1)->first();

      $view ='';
      $url_request = Url_request::where('siteID', $siteID)->where('published', 1)->get();
      $favicon = Information::where('siteID', 16)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
      $canonical = $request->getSchemeAndHttpHost();
      if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Главная страница сайта';}
      if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
      if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
      $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);

      /// header
      if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}


      $forumcategory = Forumcategory::where('slug', $slug)->with('subcategories')->first();
      //dd($forumcategory);
      $subcategory = Forumsubcategory::with('messages')->where('forumcategory', $forumcategory->id)->where('archive', '=', null)->orderBy('updated_at', 'desc')->paginate(10);
      $view .= view('2019.forum.category', [
        'Forumcategory' =>$forumcategory,
        'subcategories' => $subcategory,
        'company' => $site->company,
        'info' => $site->info,
        'categories' => Category::where('published', 1)->get(),
        'inbasket' => $inbasket,
        'bann' => $bann
      ]);
      if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
      return $view;
    }

    public function message(Request $request, $slug){

      $id = explode('-', $slug)[0];
      $subcategory = Forumsubcategory::with('category')->where('archive', '=', null)->where('id', $id)->first();
      ForumMessage::insert(['text' => $request->text,  'forumsubcategoryid' => $id, 'created_at' => date('Y-m-d H:i:s')]);
      Forumsubcategory::where('id', $id)->where('archive', '=', null)->update(['updated_at'=> date('Y-m-d H:i:s')]);
      return redirect('/forum/'.$subcategory->category->slug.'/'.$subcategory->id.'-'.$subcategory->slug);
    }
    public function subcategoryUpdate(Request $request){

      Forumsubcategory::where('id', $request->id)->update(['title' => $request->title, 'status' => $request->status]);
      $category = Forumsubcategory::where('id', $request->id)->value('forumcategory');
      return redirect()->route('admin.forum.index');
      return redirect('account/forum/1/edit?id='.$category);
    }

    public function status(Request $request){
      //dd($request->id);
     Forumsubcategory::where('id', $request->id)->update(['status'=> 2]);
    }
    public function checked(Request $request){
      ForumMessage::where('id', $request->id)->update(['checked'=> 1]);
    }

    public function messages(Request $request){
      //dd($request);
      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      $subcategory = Forumsubcategory::with('messages')->where('archive', '=', null)->where('id', $request->id)->first();
      return view('admin.forum.messages', [
        'subcategory' => $subcategory,
        'company' => $site->company,
        'info' => $site->info,
      ]);
    }
    public function subcategoryEdit(Request $request){
      if(Auth::user()->siteID == null){
        return redirect('/account/');
      }
      return view('admin.forum.createSubcategory', [
        'subcategory' => Forumsubcategory::where('id', $request->id)->first(),
        'company' => Site::with('about')->where('id', Auth::user()->siteID)->first(),
      ]);
    }
    // public function subcategoryUpdate(Request $request){
    //   if(Auth::user()->siteID == null){
    //     return redirect('/account/');
    //   }
    //     Forumsubcategory::where('id', $request->id)->update(['title' => $request->title]);
    //     return redirect()->route('admin.forum.index');
    // }

    public function messageDelete(request $request){
      ForumMessage::where('id', $request->id)->delete();
    }
    public function userBann(request $request){
      //dd($request);
      ForumMessage::where('id', $request->id)->delete();
      Users::where('id', $request->user)->update(['bann' => 1]);
    }
  }
