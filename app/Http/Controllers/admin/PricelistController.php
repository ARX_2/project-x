<?php

namespace App\Http\Controllers\Admin;

use App\Pricelist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Users;
use App\Company;
use ImageResize;
use App\Site;
class PricelistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.sections.price.index',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'pirces' => Pricelist::with('category')->where('siteID', Auth::user()->siteID)->get(),
        'categories' => DB::table('category_price')->where('siteID', Auth::user()->siteID)->get(),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pricelist  $pricelist
     * @return \Illuminate\Http\Response
     */
    public function show(Pricelist $pricelist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pricelist  $pricelist
     * @return \Illuminate\Http\Response
     */
    public function edit(Pricelist $pricelist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pricelist  $pricelist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pricelist $pricelist)
    {

      $id = DB::table('category_price')->where('id', $request->category)->where('siteID', Auth::user()->siteID)->value('id');
      //dd($id);
      if($id !== null){

        Pricelist::where('category', $id)->update(['title' => $request->title, 'count' => $request->count, 'coast' => $request->coast]);

      }
      return redirect()->route('admin.price.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pricelist  $pricelist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pricelist $pricelist)
    {
        dd($pricelist);
    }
    public function add(Request $request){
      DB::table('category_price')->insert(['title' => $request->title, 'siteID' => Auth::user()->siteID]);
      return redirect()->route('admin.price.index');
    }
    public function addPrice(Request $request){
      //dd($request);
      $id = DB::table('category_price')->where('id', $request->category)->where('siteID', Auth::user()->siteID)->value('id');
      //dd($id);
      if($id !== null){

        Pricelist::insert(['category'=> $id, 'title' => $request->title, 'count' => $request->count, 'coast' => $request->coast, 'siteID' => Auth::user()->siteID]);

      }
      return redirect()->route('admin.price.index');
    }
    public function delete(Request $request){
      Pricelist::where('id', $request->id)->where('siteID', Auth::user()->siteID)->delete();
      return redirect()->route('admin.price.index');
    }

    public function upload(Request $request){
      DB::table('category_price')->where('id', $request->category)->where('siteID', Auth::user()->siteID)->update(['title' => $request->title]);
      return redirect()->route('admin.price.index');
    }
    public function deleteCategory(Request $request){
      DB::table('category_price')->where('id', $request->id)->where('siteID', Auth::user()->siteID)->delete();
      Pricelist::where('category', $request->id)->where('siteID', Auth::user()->siteID)->delete();
      return redirect()->route('admin.price.index');
    }
}
