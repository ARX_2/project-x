<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\Category;
use App\Services;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\masterServeces;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use App\Users;
use App\Company;
use App\Site;
use App\Subcategory;
use App\CategoryGoods;
use App\ExcelUpload;
use App\ImageResize;
use App\Image;
use App\Goods;
use Caching;
use Resize;
class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      //dd(explode('?',$_SERVER['REQUEST_URI'])[0]);
      //Ключ сайта 6Lf87roUAAAAAI_9o29Z0Hxc1HR9w5iyZDX44hc-
      //Секретный ключ 6Lf87roUAAAAADkHx7UQVg1DOSpNsT3NKKfjL3Dx
        $urlActive = route('admin.services.index');
        $urlHidden = null;
        $urlCreate = '/admin/services/create';

      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();

      if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/services/hidden'){
        //$services = Services::with('images')->with('subcategory')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);
        $services = Goods::with('categories')->whereHas('categories', function($query){
          $query->where('type', 1);
          $query->orWhereHas('categor', function($sub){
            $sub->where('type',1);
          });
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

        }
        elseif(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/services/ShowDeleted'){
          //$goods = Goods::with('images')->with('subcategory')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);

          $goods = Goods::with('subcategory')->whereHas('categories', function($query){
            $query->where('type', 1);
            $query->orWhereHas('categor', function($sub){
              $sub->where('type',1);
            });
          })->where('siteID', Auth::user()->siteID)->where('deleted', 1);

          }
        else{
          $services = Goods::with('categories')->whereHas('categories', function($query){
            $query->where('type', 1);
            $query->orWhereHas('categor', function($sub){
              $sub->where('type',1);
            });
          })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);




        }
        //dd($services->get());
        //$categories = Category::where('siteID', Auth::user()->siteID)->with('images')->where('published', 1)->orderBy('sort', 'asc')->get();
        //dd($services->get());
        // if (Cache::has('product_1_'.Auth::user()->siteID)) {
        //   Cache::forget('product_1_'.Auth::user()->siteID);
        // }
        // //dd('asd');
        //
        // $view = '';
        // //dd($services->get());
        // $view .= view('2019.index.products.product_1', ['goods' => $services->get(), 'siteID' => Auth::user()->siteID,]);
        // //dd('asd');
        // Cache::forever('product_1_'.Auth::user()->siteID, $view);

        $product = $services->orderBy('updated_at', 'desc')->paginate(40);

      //dd($product);
      //dd($product);
      $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
        $query->where('type', 1);
        $query->orWhereHas('categor', function($sub){
          $sub->where('type',1);
        });
      })->where('published', 1)->count('id');

      $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
        $query->where('type', 1);
        $query->orWhereHas('categor', function($sub){
          $sub->where('type',1);
        });
      })->where('published', 0)->where('deleted', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
        $query->where('type', 1);
        $query->orWhereHas('categor', function($sub){
          $sub->where('type',1);
        });
      })->where('deleted', 1)->count('id');

      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }

      $view .= view('admin.services.index',[
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::with('subcategories')->where('siteID', Auth::user()->siteID)->where('type',1)->where('parent_id',null)->get(),
        //'type' => $type,
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden,
        'link' => $_SERVER['REQUEST_URI'],
        // 'urlCreate' => $urlCreate,
      ]);
      return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

      //$product = Goods::with('subcategoriesAll')->with('properties')->where('id', $request->id)->first();
      //dd($product);

      $category = Category::with('subcategoriesAll')->where('type',1)->where('published',1)->where('siteID', auth::user()->siteID)->get();
      $goods =  Goods::where('status',1)->where('siteID', Auth::user()->siteID)->first();
      if($goods == null){
        $id = Goods::insertGetId(['status' => 1, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
        Goods::where('id', $id)->update(['articul' => $id]);
        $goods = Goods::where('id',$id)->first();
      }
      //dd($product);
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
    //dd($product);
    $view .= view('admin.services.create',[
      'categories' => $category,
      'siteID' => $request->siteID,
      'product' => $goods,
    ]);
    return $view;
    }

    /**
     * Store a newly created resource in storage.
     *тест
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sort = Goods::max('sort');
         $goodsid = Goods::insertGetId([
           'title' => $request->title, 'short_description' => $request->short_description, 'slug' => $request->slug,
           'coast' => $request->coast, 'youtube' => $request->youtube, 'seoname' => $request->seoname,
           'seotitle' => $request->seotitle, 'seodescription' => $request->seodescription, 'seokeywords' => $request->seokeywords,
           'siteID' => auth::user()->siteID, 'deleted' => 0, 'published' => 1, 'sort' => $sort, 'slug' => str_slug($request->title),
           'category' => $request->subcategory, 'created_at' => date('Y-m-d H:i:s'),
         ]);
         if($request->file('file') !== null){
         foreach ($request->file('file') as $img) {
           $path = Storage::putFile('public', $img);
           Image::create(['images' => $path, 'goods' => $goodsid, 'siteID' => Auth::user()->siteID]);
         }
         }


      return redirect()->route('admin.services.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function show(Services $services, Request $request)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Services $services)
    {
      //dd(auth::user()->siteID);
      $backdoor = Goods::where('siteID', auth::user()->siteID)->where('id', $request->id)->value('id');

      $protected = Goods::where('siteID', auth::user()->siteID)->where('id', $request->id)->value('protected');
      if($backdoor == null || $protected == 1 && Auth::user()->userRolle == 6){
        return redirect()->route('admin.services.index');
      }
      $product = Goods::with('subcategory')->with('imageses')->where('id', $request->id)->first();

        $category = Category::with('subcategoriesAll')->where('type',1)->where('published',1)->where('parent_id',null)->where('siteID', auth::user()->siteID)->get();


      $company = Site::with('sections')->with('info')->where('id', Auth::user()->siteID)->first();
      //dd($product);
      $view = '';
      if($request->ajax ==  null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
      //return $view;
      $view .= view('admin.services.create',[
        'company' => $company,
        'categories' => $category,
        'product' =>  $product,
        'siteID' => $request->siteID,
      ]);
      return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function update($slug,$id,$parametr,Request $request, Services $services)
    {
      //dd($parametr);
      $unparametr[$parametr] = $parametr;
      $unparametr[0] = 1;
      $unparametr[1] = 0;
      if($slug == 'title'){
        $cat = Goods::where('id', $id)->value('status');
        if($cat == 1){
          $update['title'] = ['title' => $parametr, 'slug' => str_slug($parametr, "-"), 'created_at' => date('Y-m-d H:i:s')];
        }
        else{
          $update['title'] = ['title' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
        }
      }
      $update['published'] = ['published' => $unparametr[$parametr], 'deleted' => 0, 'dateDelete' => null, 'updated_at' => date('Y-m-d H:i:s')];
      $update['main'] = ['main' => $unparametr[$parametr], 'updated_at' => date('Y-m-d H:i:s')];
      $update['deleted'] = ['deleted' => $unparametr[$parametr], 'updated_at' => date('Y-m-d H:i:s'), 'dateDelete' => \carbon\Carbon::now()->addDays(30)->toDateTimeString()];

      $update['coast'] = ['coast' => $parametr,'updated_at' => date('Y-m-d H:i:s')];
      $update['category'] = ['category' => $parametr,'updated_at' => date('Y-m-d H:i:s')];
      $update['checkseo'] = ['checkseo' => $parametr,'updated_at' => date('Y-m-d H:i:s')];
      $update['seoname'] = ['seoname' => $parametr,'updated_at' => date('Y-m-d H:i:s')];
      $update['seotitle'] = ['seotitle' => $parametr,'updated_at' => date('Y-m-d H:i:s')];
      $update['seodescription'] = ['seodescription' => $parametr,'updated_at' => date('Y-m-d H:i:s')];
      $update['seokeywords'] = ['seokeywords' => $parametr,'updated_at' => date('Y-m-d H:i:s')];
      $update['update'] = ['title' => $request->title, 'short_description' => $request->short_description,
        'coast' => $request->coast, 'youtube' => $request->youtube, 'seoname' => $request->seoname,
        'seotitle' => $request->seotitle, 'seodescription' => $request->seodescription, 'seokeywords' => $request->seokeywords,
        'checkseo' => $request->checkseo, 'category' => $request->subcategory,'updated_at' => date('Y-m-d H:i:s')];
    $result =  Goods::where('id', $id)->where('siteID', Auth::user()->siteID)->update($update[$slug]);
      if($request->file('file') !== null && $result == 1){
      foreach ($request->file('file') as $img) {
        //dd($request);
        $path = Storage::putFile('public', $img);
        Image::create(['images' => $path, 'goods' => $request->id, 'siteID' => Auth::user()->siteID]);
      }
      }

        //return redirect()->route('admin.services.index');

        //dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function destroy(Services $services)
    {
      //dd($_GET);
      Services::where('id', $_GET['id'])->delete();
      return redirect()->route('admin.services.index');
    }

    public function sortServices(Request $request){
          if($request->id == 0){
            $goods = Goods::with('images')
            ->with('subcategory')
            ->whereHas('categories', function($query){
              $query->whereIn('type', 0);
            })->where('goods.siteID', Auth::user()->siteID)->where('goods.deleted', 0)->paginate(40);
          }
          else{
            Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();
            $subcategories = Category::where('parent_id', $request->id)->get();
            $categories = [];
            $categories[] = (int)$request->id;
            foreach ($subcategories as $key => $value) {
              $categories[] = $value->id;
            }

            $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($categories){
              $query->whereIn('id', $categories);
            })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->paginate(20);
            //dd($goods);
          }
          //dd($goods);
        //}

        $view = '';

        $view .= view('admin.services.services', ['product' => $goods]);
        return $view;
    }

    public function CategoryServices($category,Request $request){


      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();


      //dd($goods->get());
      //dd(explode('?',$_SERVER['REQUEST_URI'])[0]);
      if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/services/'.$category){
        //dd('111');
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($category){
          $query->where('id', $category);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);

      }
      else{
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($category){
          $query->where('id', $category);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

      }


      $product = $goods->paginate(40);
      //dd($product);

      $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('id', $category);
      })->where('published', 1)->count('id');

      $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('id', $category);
      })->where('published', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('id', $category);
      })->where('deleted', 1)->count('id');
      //dd($product);
      //dd($product[0]->images);
      //dd($categories);

      //dd($type);
      $categoryType = Category::where('id', $category)->value('type');
      //dd($categoryType);
      if($categoryType == 0){
        $goodstype = 'goods';
      }
      else{
        $goodstype = 'services';
      }
      $urlActive = '/admin/category/services/'.$category;
      $urlHidden = '/admin/category/services/hidden/'.$category;
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }

      $view .= view('admin.'.$goodstype.'.index',[
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::where('siteID', Auth::user()->siteID)->get(),
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden
        //'type' => $type,
      ]);
      return $view;
      //dd($request);
    }

    public function CategoryServiceAll($category,Request $request){

      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();
      $subcategories = Category::where('parent_id', $category)->get();
      $categories = [];
      $categories[] = (int)$category;
      foreach ($subcategories as $key => $value) {
        $categories[] = $value->id;
      }
      //dd($categories);


      //dd($goods->get());

      if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/services/all/'.$category){
        //dd('111');
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($categories){
          $query->whereIn('id', $categories);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);
      }
      else{
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($categories){
          $query->whereIn('id', $categories);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

      }


      $product = $goods->paginate(40);
      //dd($product);
      $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($categories){
        $query->whereIn('id', $categories);
      })->where('published', 1)->count('id');

      $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($categories){
        $query->whereIn('id', $categories);
      })->where('published', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($categories){
        $query->whereIn('id', $categories);
      })->where('deleted', 1)->count('id');
      //dd($product);
      //dd($product[0]->images);
      //dd($categories);

      //dd($type);
      $categoryType = Category::where('id', $category)->value('type');
      //dd($categoryType);
      if($categoryType == 0){
        $goodstype = 'goods';
      }
      else{
        $goodstype = 'services';
      }

      $urlActive = '/admin/category/services/all/'.$category;
      $urlHidden = '/admin/category/services/all/hidden/'.$category;
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
      $view .= view('admin.'.$goodstype.'.index',[
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::where('siteID', Auth::user()->siteID)->get(),
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden
        //'type' => $type,
      ]);
      return $view;
      //dd($request);
    }

    public function SubcategoryServices($subcategory, Request $request){
        //dd($subcategory);

      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();



        //dd($categoryGoods);
        if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/subcategory/services/'.$subcategory){
          $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($subcategory){
            $query->where('id', $subcategory);
          })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);
        }
        else{
          $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($subcategory){
            $query->where('id', $subcategory);
          })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);
        }
        //$categories = Category::where('siteID', Auth::user()->siteID)->with('images')->where('published', 1)->orderBy('sort', 'asc')->get();
        //dd($goods->get());

        $product = $goods->paginate(40);


      //dd($product);
      $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($subcategory){
        $query->where('id', $subcategory);
      })->where('published', 1)->count('id');

      $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($subcategory){
        $query->where('id', $subcategory);
      })->where('published', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($subcategory){
        $query->where('id', $subcategory);
      })->where('deleted', 1)->count('id');


      $categoryType = Category::where('parent_id', $subcategory)->value('type');
      //dd($categoryType);
      if($categoryType == 0){
        $goodstype = 'goods';
      }
      else{
        $goodstype = 'services';
      }
      //dd($type);
      $urlActive = '/admin/subcategory/services/'.$subcategory;
      $urlHidden = '/admin/subcategory/services/hidden/'.$subcategory;
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
      $view .= view('admin.'.$goodstype.'.index',[
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::where('siteID', Auth::user()->siteID)->get(),
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden,

        //'type' => $type,
      ]);
      return $view;

    }


    public function description(Request $request){

      Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['short_description' => $request->short_description,'status' => 2, 'updated_at' => date('Y-m-d H:i:s'), 'published' => 1]);
      $request['ajax'] = 'true';
      return  self::index($request);
    }
    public function changeTitleSites(Request $request){

      Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['short_description' => $request->short_description,'status' => 2, 'updated_at' => date('Y-m-d H:i:s'), 'published' => 1]);
      $goods = Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->first();
      Goods::where('articul', $goods->articul)->update(['title' => $goods->title, 'short_description' => $goods->short_description, 'coast' => $goods->coast, 'youtube' => $goods->youtube,]);
      $request['ajax'] = 'true';
      return  self::index($request);
    }

}
