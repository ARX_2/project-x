<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use DB;
use App\Users;
use App\Company;
use App\Site;
use Resize;
class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $banner = Banner::where('siteID', Auth::user()->siteID)->where('status', 2)->with('imageResize');
      //dd($banner->first());

        $banner = Banner::where('siteID', Auth::user()->siteID)->where('status', 2)->with('imageResize');

      if (Cache::has('Imagebanner'.Auth::user()->siteID)) {
        Cache::forget('Imagebanner'.Auth::user()->siteID);
      }
      foreach ($banner->get() as $bann) {
          //dd($bann);
        if($bann->published == 1){
            //dd($bann);
          Cache::forever('Imagebanner'.Auth::user()->siteID, Resize::ResizeImage($bann, '1903x739'));
        }

      }
      //dd(Cache::get('Imagebanner17'));
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }

      $view .= view('admin.sections.banner.index',[
        'siteID' => Auth::user()->siteID,
        'banner' => $banner->get(),
        'link' => $_SERVER['REQUEST_URI'],
      ]);
      return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

      $banner =  Banner::where('status',1)->where('siteID',  Auth::user()->siteID)->first();
      if($banner == null){
        $id = Banner::insertGetId(['status' => 1, 'siteID' => Auth::user()->siteID]);
        $banner = Banner::where('id',$id)->first();
      }
      $view = '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
        $view .= view('admin.sections.banner.create', ['banner' => $banner]);
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request);
          $path = Storage::putFile('public', $request->file('image'));
          //dd($path);
        Banner::create(['title' => $request->title, 'text' => $request->text, 'main' => 0, 'image' => $path, 'published' => 1, 'siteID' => auth::user()->siteID]);
        return redirect()->route('admin.banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Banner $banner)
    {
      $backdoor = Banner::where('id', $banner->id)->where('siteID', Auth::user()->siteID)->value('id');
      if($backdoor == null ){
        return redirect()->back();
      }
      $view = '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }

      $view .= view('admin.sections.banner.create', ['banner' => $banner,]);
      return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update($slug,$id,$parametr,Request $request, Banner $banner)
    {
      $unparametr[$parametr] = $parametr;
      $unparametr[0] = 1;
      $unparametr[1] = 0;
      $update['published'] = ['published' => $unparametr[$parametr]];
      $update['main'] = ['main' => $unparametr[$parametr]];
      $update['title'] = ['title' => $parametr];
      $update['text'] = ['text' => $parametr];
      $update['status'] = ['status' => $parametr];
        $result =  Banner::where('id', $id)->where('siteID', Auth::user()->siteID)->update($update[$slug]);
          if($slug = 'status'){
            $request['ajax'] = 'true';
            return  self::index($request);
          }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
      $backdoor = Banner::where('id', $banner->id)->where('siteID', Auth::user()->siteID)->first();
      //dd($backdoor);
      if($backdoor == null){
        echo 'Баннер не найден';
        die;
      }
        //dd($banner);
        Banner::where('id', $banner->id)->delete();
        Storage::delete($banner->image);
        return redirect()->route('admin.banner.index');
    }

    public function imageUpload(Request $request){
      if($request->file('file') !== null){
      $img =  $request->file('file');
        $path = Storage::putFile('public', $img);
        Banner::where('id',$request->id)->where('siteID', Auth::user()->siteID)->update(['image' => $path]);
        $data = ['image' => $path];
      return json_encode($data);
      }

    }
}
