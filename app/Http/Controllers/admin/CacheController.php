<?php

namespace App\Http\Controllers\Admin;

use App\Goods;
use App\Category;
//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Emails;
use App\Model\Partners;
use App\Model\Sites;
use DB;
use App\Users;
use App\Image;
use App\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use App\Company;
use ImageResize;
use App\Site;
use App\Property;
use App\Documentation;
use App\Subcategory;
use App\Information;
use App\Objects;
use App\Text;
use App\Template;
use App\Menu;
use App\Tel;
use App\Email;
use App\Section;
use Request;
use WebP;
use App\City;
class CacheController extends Controller
{
    static function categoryMenu(){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        if (Cache::has('categoryMenuGoods'.$siteID)) {
            Cache::forget('categoryMenuGoods'.$siteID);
        }
        $categories = Category::with('images')->with('subcategories')->where('siteID', $siteID)->where('deleted', 0)->where('published',1)->where('parent_id', null)->where('type', 0)->orderby('sort', 'asc')->get();
        //dd($categories);
        $view = '';
        $view .= view('2019.category.nav-menu', ['categories' => $categories, 'siteID' => $siteID]);
        Cache::forever('categoryMenuGoods'.$siteID, $view);

        if (Cache::has('categoryMenuServices'.$siteID)) {
            Cache::forget('categoryMenuServices'.$siteID);
        }
        $categories = Category::with('images')->with('subcategories')->where('siteID', $siteID)->where('deleted', 0)->where('published',1)->where('parent_id', null)->where('type', 1)->orderby('sort', 'asc')->get();
        //dd($categories);
        $view = '';
        $view .= view('2019.category.nav-menu', ['categories' => $categories, 'siteID' => $siteID]);
        Cache::forever('categoryMenuServices'.$siteID, $view);
    }
    static function categoryDocs(){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        if (Cache::has('categoryMenuDocs'.$siteID)) {
            Cache::forget('categoryMenuDocs'.$siteID);
        }
        $docs = Documentation::where('siteID', $siteID)->get();
        $view = '';
        $view .= view('2019.category.nav-docs', ['documents' => $docs, 'siteID' => $siteID]);
        Cache::forever('categoryMenuDocs'.$siteID, $view);
    }
    static function breadcrumb($id){
        if(Auth::user() !== null){
            $siteID = Auth::user()->siteID;
        }
        else{
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        if (Cache::has('categoryBreadcrumb_'.$id.'_'.$siteID)) {
            Cache::forget('categoryBreadcrumb_'.$id.'_'.$siteID);
        }
        $category = Category::with('categor')->where('siteID', $siteID)->where('id', $id)->first();
        //dd($category);
        if($id == 'goods' || $id == 'product' || $id == 'products'){
            $title = 'Товары';
        }
        elseif($id == 'services'){
            $title = 'Услуги';
        }
        else{
            if($category !== null){
                //$title = $category->title;
                if($category->type ==0){
                    $title = 'Товары';
                }
                else{
                    $title = 'Услуги';
                }
            }
            else{
                $title = 'Товары';
            }

        }
        //dd($category);
        $view = '';
        $view .= view('2019.category.breadcrumb', ['title' => $title, 'siteID' => $siteID, 'category' => $category, 'breadcrumb' => 1, 'siteID' => $siteID,]);
        Cache::forever('categoryBreadcrumb_'.$id.'_'.$siteID, $view);
    }
    static function categories(){

        if(Auth::user() !== null){
            $siteID = Auth::user()->siteID;
        }
        else{
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }

        //dd($siteID);
        if (Cache::has('categoryMenuDocs'.$siteID)) {
            Cache::forget('categoryMenuDocs'.$siteID);
        }
        //dd('app_salon_admin');
        if (Cache::has(Request::getHttpHost().'.product'.$siteID)) {
            Cache::forget(Request::getHttpHost().'.product'.$siteID);
        }
        $id = Site::where('title', Request::getHttpHost())->value('id');
        $categoriesGoods = Category::with('images')->with('goods')->with('subcategories')->where('siteID', $siteID)->where('deleted', 0)->where('published',1)->where('parent_id', null)->where('type', 0)->orderby('sort', 'asc')->get();
        $goods = Goods::with('images')->with('subcategory')->whereHas('categories', function($query){
            $query->where('type', 0);
            $query->orWhereHas('categor', function($sub){
                $sub->where('type',0);
            });
        })->where('siteID', $siteID)->where('deleted', 0)->where('published',1)->paginate(20);
        //dd($categoriesGoods);
        //dd($categoriesGoods);
        $text = Template::with('texts')->with('site')->where('siteID', $siteID)->where('slug', 'product')->first();
        //dd($text);

        if($text->texts->checkseo == 1){
            $seoTitle = $text->texts->seoTitle;
            $seoDescription = $text->texts->seoDescription;
        }

        else{
            $seoTitle = $text->site->name.' товары';
            $seoDescription = null;
        }


        if (Cache::has('doctype_goods_'.$siteID)) {
            Cache::forget('doctype_goods_'.$siteID);
        }
        $texts = Template::where('siteID', $siteID)->with('texts')->where('slug', 'product')->first();
        //dd($texts);
        //dd(Cache::get('about_4'));
        $view = '';
        $view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'jq' => 1, 'siteID' => $siteID, 'seoDescription' => $seoDescription, 'title' => 'Товары']);
        Cache::forever('doctype_goods_'.$siteID, $view);
        self::breadcrumb('goods');
        $view = '';
        //$view .= Cache::get('menu_1_'.$id);
        //dd($categoriesGoods);

        $view.= view('2019.category.category_1', ['content' => 1,'categories' =>$categoriesGoods, 'texts' => $texts->texts, 'goods' =>$goods, 'siteID' => $siteID, 'title' => 'Товары', 'cat' => null]);
        //dd($view);
        //$view .= Cache::get('footer_1_'.$id);
        Cache::forever(Request::getHttpHost().'.product'.$siteID, $view);


        //$view .= Cache::get('footer_1_'.$id);
        Cache::forever(Request::getHttpHost().'.services'.$siteID, $view);

        if (Cache::has('catalog_1_'.$siteID)) {
            Cache::forget('catalog_1_'.$siteID);
        }
        $view = '';

        $categories = Category::with('images')->with('subcategories')->with('services')->where('siteID', $siteID)->where('deleted', 0)->where('published',1)->where('parent_id', null)->orderby('sort', 'asc')->get();
        $view .= view('2019.index.catalog.catalog_1', ['categories' => $categories, 'siteID' => $siteID]);
        Cache::forever('catalog_1_'.$siteID, $view);
        //dd('asd');
    }




    static function categoryServices(){

        if(Auth::user() !== null){
            $siteID = Auth::user()->siteID;
        }
        else{
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        //dd($siteID);

        if (Cache::has('categoryMenuDocs'.$siteID)) {
            Cache::forget('categoryMenuDocs'.$siteID);
        }
        //dd('app_salon_admin');
        if (Cache::has(Request::getHttpHost().'.product'.$siteID)) {
            Cache::forget(Request::getHttpHost().'.product'.$siteID);
        }
        $id = Site::where('title', Request::getHttpHost())->value('id');
        //dd($view);
        //$view .= Cache::get('footer_1_'.$id);

        if (Cache::has(Request::getHttpHost().'.services'.$siteID)) {
            Cache::forget(Request::getHttpHost().'.services'.$siteID);
        }
        $id = Site::where('title', Request::getHttpHost())->value('id');
        $categoriesServices = Category::with('images')->with('services')->with('subcategories')->where('siteID', $siteID)->where('deleted', 0)->where('parent_id', null)->where('published',1)->where('type', 1)->orderby('sort', 'asc')->get();

        //dd($categoriesServices);
        $text = Template::with('texts')->with('companies')->where('siteID', $siteID)->where('slug', 'services')->first();
        //dd($text);
        if($text->texts->checkseo == 1){
            $seoTitle = $text->texts->seoTitle;
            $seoDescription = $text->texts->seoTitle;
        }

        else{
            $seoTitle = $text->site->name.' услуги';
            $seoDescription = null;
        }

        if (Cache::has('doctype_services_'.$siteID)) {
            Cache::forget('doctype_services_'.$siteID);
        }
        $texts = Template::where('siteID', $siteID)->with('texts')->where('slug', 'services')->first();
        //dd(Cache::get('about_4'));
        $view = '';
        $view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'jq' => 1, 'seoDescription' => $seoDescription, 'siteID' => $siteID]);
        Cache::forever('doctype_servic_'.$siteID, $view);
        Cache::get('doctype_servic_'.$siteID);
        //dd(Cache::get('doctype_services_'.Auth::user()->siteID));
        self::breadcrumb('services');
        //dd(Cache::get('about_4'));
        $view = '';
        //$view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'seoDescription' => $seoDescription]);

        //$view .= Cache::get('menu_1_'.$id);
        //dd($categoriesServices);
        $goods = Goods::with('images')->with('subcategory')->whereHas('categories', function($query){
            $query->where('type', 1);
            $query->orWhereHas('categor', function($sub){
                $sub->where('type',1);
            });
        })->where('siteID', $siteID)->where('deleted', 0)->where('published',1)->paginate(20);

        $view .= view('2019.category.category_1', ['content' => 1,'categories' =>$categoriesServices, 'texts' => $texts->texts, 'goods' =>$goods, 'siteID' => $siteID, 'title' => 'Услуги', 'cat' => null]);
        //dd($view);
        //$view .= Cache::get('footer_1_'.$id);
        Cache::forever(Request::getHttpHost().'.services'.$siteID, $view);

        if (Cache::has('catalog_1_'.$siteID)) {
            Cache::forget('catalog_1_'.$siteID);
        }
        $view = '';

        $categories = Category::with('images')->with('subcategories')->with('services')->where('siteID', $siteID)->where('deleted', 0)->where('published',1)->where('parent_id', null)->orderby('sort', 'asc')->get();
        $view .= view('2019.index.catalog.catalog_1', ['categories' => $categories, 'siteID' => $siteID]);
        Cache::forever('catalog_1_'.$siteID, $view);
        //dd('asd');
    }


    static function category($categoryid){
        if(Auth::user() !== null){
            $siteID = Auth::user()->siteID;
        }
        else{
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }


        $id = Site::where('title', Request::getHttpHost())->value('id');
        if (Cache::has(Request::getHttpHost().'.category_'.$id.'_'.$siteID)) {
            Cache::forget(Request::getHttpHost().'.category_'.$id.'_'.$siteID);
        }
        //dd($siteID);
        $category = Category::with('subcategories')->with('images')->with('services')->where('siteID', $siteID)->where('id', $categoryid)->first();
        //dd($siteID);
        if($category->type == 0){
            $title = 'Товары';
            if (Cache::has('categorytype_'.$category->id)) {
                Cache::forget('categorytype_'.$category->id);
            }
            Cache::forever('categorytype_'.$category->id, 'Goods');
        }
        else{
            $title = 'Услуги';
            if (Cache::has('categorytype_'.$category->id)) {
                Cache::forget('categorytype_'.$category->id);
            }
            Cache::forever('categorytype_'.$category->id, 'Services');
        }
        $categories = Category::with('images')->with('subcategories')->where('siteID', $siteID)->where('deleted', 0)->where('published',1)->where('type', $category->type)->orderby('sort', 'asc')->get();

        //dd($category);
        if($category->checkseo !== null){
            $seoTitle = $category->seotitle;
            $seoDescription = $category->seodescription;
        }
        else{
            $seoTitle = null;
            $seoDescription = null;
        }
        if (Cache::has('doctype_services_'.$categoryid)) {
            Cache::forget('doctype_services_'.$categoryid);
        }

        //dd(Cache::get('about_4'));

        $view = '';
        $view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'jq' => '1', 'seoDescription' => $seoDescription, 'siteID' => $siteID]);
        //dd('asd');
        Cache::forever('doctype_'.$categoryid.'_'.$siteID, $view);

        self::breadcrumb($categoryid);

        //dd(Cache::get('about_4'));
        $view = '';
        //$view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'seoDescription' => $seoDescription]);
        //$view .= Cache::get('menu_1_'.$id);
        if (Cache::has('category_'.$category->id)) {
            Cache::forget('category_'.$category->id);
        }
        $view .= view('2019.category.category_1', ['content' => 2, 'categoryCache' => $category, 'siteID' => $siteID, 'title' => $title,'categories' =>$categories, 'id' => $categoryid, 'cat' => $categoryid]);

        //$view .= Cache::get('footer_1_'.$id);
        Cache::forever('category_'.$category->id, $view);
    }
    static function deleteCategory($id){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        if (Cache::has('categorytype_'.$id)) {
            Cache::forget('categorytype_'.$id);
        }
        if (Cache::has('doctype_services_'.$siteID)) {
            Cache::forget('doctype_services_'.$siteID);
        }

        if (Cache::has(Request::getHttpHost().'.category_'.$id.'_'.$siteID)) {
            Cache::forget(Request::getHttpHost().'.category_'.$id.'_'.$siteID);
        }
    }
    static function categoryNull(){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        $id = Site::where('title', Request::getHttpHost())->value('id');
        if (Cache::has(Request::getHttpHost().'.category_'.$id.'_'.$siteID)) {
            Cache::forget(Request::getHttpHost().'.category_'.$id.'_'.$siteID);
        }

        $categoriesAll = Category::where('siteID', $siteID)->with('subcategories')->get();
        foreach ($categoriesAll as  $category) {
            if($category->type == 0){
                $title = 'Товары';
            }
            else{
                $title = 'Услуги';
            }
            $categories = Category::with('images')->with('subcategories')->where('siteID', $siteID)->where('deleted', 0)->where('published',1)->where('type', $category->type)->orderby('sort', 'asc')->get();
            //dd($category);
            if($category->checkseo !== null){
                $seotitle = $category->seoTitle;
                $seoDescription = $category->seoDescription;
            }
            else{
                $seoTitle = null;
                $seoDescription = null;
            }

            //dd(Cache::get('about_4'));
            $view = '';
            //$view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'seoDescription' => $seoDescription]);
            //$view .= Cache::get('menu_1_'.$id);
            $view .= view('2019.category.category_1', ['content' => 2, 'categoryCache' => $category, 'siteID' => $siteID,'categories' =>$categories, 'id' => $category->id]);
            //$view .= Cache::get('footer_1_'.$id);
            Cache::forever(Request::getHttpHost().'.category_'.$id.'_'.$siteID, $view);
        }

    }

    static function subcategories(){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        $id = Site::where('title', Request::getHttpHost())->value('id');
        if (Cache::has(Request::getHttpHost().'.category_'.$id.'_'.$siteID)) {
            Cache::forget(Request::getHttpHost().'.category_'.$id.'_'.$siteID);
        }

        $categoriesAll = Category::where('siteID', $siteID)->with('subcategories')->get();
        foreach ($categoriesAll as  $category) {
            foreach ($category->subcategories as $sub) {
                if($category->type == 0){
                    $title = 'Товары';
                }
                else{
                    $title = 'Услуги';
                }
                $categories = Category::with('images')->with('subcategories')->where('siteID', $siteID)->where('deleted', 0)->where('published',1)->where('type', $category->type)->orderby('sort', 'asc')->get();
                //dd($category);
                if($sub->checkseo !== null){
                    $seotitle = $sub->seoTitle;
                    $seoDescription = $sub->seoDescription;
                }
                else{
                    $seoTitle = null;
                    $seoDescription = null;
                }

                //dd(Cache::get('about_4'));
                $view = '';
                //$view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'seoDescription' => $seoDescription]);
                //$view .= Cache::get('menu_1_'.$id);
                $view .= view('2019.category.category_1', ['content' => null, 'subcategoryCache' => $sub,'categories' =>$categories, 'siteID' => $siteID,]);
                //$view .= Cache::get('footer_1_'.$id);
                Cache::forever(Request::getHttpHost().'.subcategory_'.$id.'_'.$siteID, $view);
            }
        }
    }

    static function deleteSubcategory($id){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        if (Cache::has('doctype_sub_'.$id.'_'.$siteID)) {
            Cache::forget('doctype_sub_'.$id.'_'.$siteID);
        }
        if (Cache::has('subcategoryBreadcrumb_'.$id.'_'.$siteID)) {
            Cache::forget('subcategoryBreadcrumb_'.$id.'_'.$siteID);
        }
        if (Cache::has(Request::getHttpHost().'.subcategory_'.$id.'_'.$siteID)) {
            Cache::forget(Request::getHttpHost().'.subcategory_'.$id.'_'.$siteID);
        }
    }
    static function subcategory($subcategoryid){


        if(Auth::user() !== null){
            $siteID = Auth::user()->siteID;
        }
        else{
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        //dd($siteID);
        $sub = Category::with('goods')->with('images')->with('categor')->where('id', $subcategoryid)->first();
        //dd($sub);
        if($sub->categor->type == 0){
            $title = 'Товары';
            if (Cache::has('categorytype_'.$sub->categor->id)) {
                Cache::forget('categorytype_'.$sub->categor->id);
            }
            Cache::forever('categorytype_'.$sub->categor->id, 'Goods');
        }
        else{
            $title = 'Услуги';
            if (Cache::has('categorytype_'.$sub->categor->id)) {
                Cache::forget('categorytype_'.$sub->categor->id);
            }
            Cache::forever('categorytype_'.$sub->categor->id, 'Services');
        }
        //dd($sub);
        //dd($sub);
        $title = $sub->title;
        //$categories = Category::with('images')->with('subcategories')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1)->where('type', $sub->category->type)->orderby('sort', 'asc')->get();
        //dd($category);
        if($sub->checkseo !== null){
            $seotitle = $sub->seoTitle;
            $seoDescription = $sub->seoDescription;
        }
        else{
            $seoTitle = null;
            $seoDescription = null;
        }
        if (Cache::has('doctype_sub_'.$subcategoryid.'_'.$siteID)) {
            Cache::forget('doctype_sub_'.$subcategoryid.'_'.$siteID);
        }

        //dd(Cache::get('about_4'));
        $view = '';
        $view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'seoDescription' => $seoDescription, 'siteID' => $siteID]);
        Cache::forever('doctype_sub'.$subcategoryid.'_'.$siteID, $view);
        if (Cache::has('subcategoryBreadcrumb_'.$subcategoryid.'_'.$siteID)) {
            Cache::forget('subcategoryBreadcrumb_'.$subcategoryid.'_'.$siteID);
        }

        $view = '';
        $view .= view('2019.category.breadcrumb', ['breadcrumb' => 2,'category' =>$sub->categor, 'subcategory' => $sub, 'siteID' => $siteID ]);
        Cache::forever('subcategoryBreadcrumb_'.$subcategoryid.'_'.$siteID, $view);
        //dd(Cache::get('about_4'));
        $view = '';
        //$view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'seoDescription' => $seoDescription]);
        //$view .= Cache::get('menu_1_'.$id);

        if (Cache::has(Request::getHttpHost().'.subcategory_'.$subcategoryid.'_'.$siteID)) {
            Cache::forget(Request::getHttpHost().'.subcategory_'.$subcategoryid.'_'.$siteID);
        }

        $view .= view('2019.category.category_1', ['content' => null, 'subcategoryCache' => $sub,  'title' => $title, 'siteID' => $siteID, 'cat' => $sub->categor->id ]);
        //dd(Request::getHttpHost().'.subcategory_'.$subcategoryid.'_'.$siteID);
        //dd(Request::getHttpHost().'.subcategory_'.$subcategoryid.'_'.Auth::user()->siteID);
        Cache::forever(Request::getHttpHost().'.subcategory_'.$subcategoryid.'_'.$siteID, $view);

    }

    static function product($id){

        if(Auth::user() !== null){
            $siteID = Auth::user()->siteID;
        }
        else{
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        //dd($siteID);
        //dd($id);
        //dd($id);

        $product = Goods::with('subcategory')->with('images')->with('imageses')->where('id', $id)->first();

        //dd($product);
        //dd($sub);
        //dd($product);
        //dd($product);
        if($product->subcategory->categor == null){
            $category = $product->subcategory;
        }
        else{
            $category = $product->subcategory->categor;
        }
        //dd($category);
        if($category->type == 0){
            $title = 'товара';
            //$section = Information::where('siteID', $siteID)->value('section0');
            $user =  Users::where('siteID', $siteID)->where('userRolle', 3)->first();
            $company = Information::where('siteID', $siteID)->first();
            $objects =  Objects::where('siteID', $siteID)->where('published', 1)->get();
            if($product->checkseo !== null){
                $seoTitle = $product->seotitle;
                $seoDescription = $product->seodescription;
            }
            else{
                $seoTitle = null;
                $seoDescription = null;
            }
            //dd($seoTitle);
            if (Cache::has('doctype_product_'.$id)) {
                Cache::forget('doctype_product_'.$id);
            }
            $view = '';
            $view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'jq' => '1', 'seoDescription' => $seoDescription, 'siteID' => $siteID]);
            Cache::forever('doctype_product_'.$id, $view);
            if (Cache::has('productBreadcrumb_'.$id)) {
                Cache::forget('productBreadcrumb_'.$id);
            }
            $view = '';
            $view .= view('2019.category.breadcrumb', ['breadcrumb' => 3,'product' =>$product, 'type' => 'product', 'siteID' => $siteID]);
            Cache::forever('productBreadcrumb_'.$id, $view);
            if (Cache::has('product_1_'.$id)) {
                Cache::forget('product_1_'.$id);
            }
            $docs =  Documentation::where('siteID', $siteID)->get();
            $userOnSite = Users::where('siteID', $siteID)->where('onSite', 1)->first();
            $view .= view('2019.product_services.product', ['documents' => $docs, 'userOnSite' => $userOnSite,'subcategory' =>$product->subcategory, 'siteID' => $siteID, 'cat' =>$category->id,'objects' => $objects,'user' => $user,'company' =>$company,'product' => $product, 'title' => $title, 'cat' => $category->id ]);

            if(Cache::has('productModal'.$id)){
                Cache::forget('productModal'.$id);
            }
            Cache::forever('productModal'.$id, $product);

            //dd('asd');
            //dd(Request::getHttpHost().'.subcategory_'.$subcategoryid.'_'.Auth::user()->siteID);
            Cache::forever('product_1_'.$id, $view);
        }
        else{
            $title = 'услуги';
            //dd($title);
            if($product->checkseo !== null){
                $seoTitle = $product->seotitle;
                $seoDescription = $product->seodescription;
            }
            else{
                $seoTitle = null;
                $seoDescription = null;
            }

            if (Cache::has('doctype_product_'.$id)) {
                Cache::forget('doctype_product_'.$id);
            }

            //dd(Cache::get('about_4'));
            $view = '';
            $view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'seoDescription' => $seoDescription, 'siteID' => $siteID]);
            Cache::forever('doctype_product_'.$id, $view);
            if (Cache::has('productBreadcrumb_'.$id)) {
                Cache::forget('productBreadcrumb_'.$id);
            }
            $view = '';
            $view .= view('2019.category.breadcrumb', ['breadcrumb' => 3,'product' =>$product,'type' => 'services', 'siteID' => $siteID]);
            Cache::forever('productBreadcrumb_'.$id, $view);
            //dd(Cache::get('about_4'));
            $view = '';
            //$view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'seoDescription' => $seoDescription]);
            //$view .= Cache::get('menu_1_'.$id);

            if (Cache::has('product_1_'.$id)) {
                Cache::forget('product_1_'.$id);
            }

            $view .= view('2019.product_services.product_services_1', ['product' => $product, 'title' => $title, 'cat' => $category->id, 'siteID' => $siteID ]);
            //dd('asd');
            //dd(Request::getHttpHost().'.subcategory_'.$subcategoryid.'_'.Auth::user()->siteID);
            Cache::forever('product_1_'.$id, $view);
        }

        //$categories = Category::with('images')->with('subcategories')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1)->where('type', $sub->category->type)->orderby('sort', 'asc')->get();
        //dd($category);

    }
    static function deleteProduct($id){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        if (Cache::has('doctype_product_'.$id)) {
            Cache::forget('doctype_product_'.$id);
        }
        if (Cache::has('productBreadcrumb_'.$siteID)) {
            Cache::forget('productBreadcrumb_'.$siteID);
        }

        if (Cache::has('product_1_'.$id)) {
            Cache::forget('product_1_'.$id);
        }
    }
    static function servicesContact(){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        if (Cache::has('servicesContact'.$siteID)) {
            Cache::forget('servicesContact'.$siteID);
        }
        $user =  Users::where('siteID', $siteID)->where('userRolle', 3)->first();
        $company = Information::where('siteID', $siteID)->first();
        $view = '';
        $userOnSite = Users::where('siteID', $siteID)->where('onSite', 1)->first();
        $view .= view('2019.product_services.contact_1', ['user' => $user, 'userOnSite' => $userOnSite,'company' =>$company, 'siteID' => $siteID]);
        Cache::forever('servicesContact'.$siteID, $view);
    }
    static function productContact(){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        //dd($siteID);
        if (Cache::has('productContact'.$siteID)) {
            Cache::forget('productContact'.$siteID);
        }
        $view = '';
        $docs =  Documentation::where('siteID', $siteID)->get();
        $userOnSite = Users::where('siteID', $siteID)->where('onSite', 1)->first();
        $company = Site::where('id', $siteID)->first();
        $view .= view('2019.product_services.contact_product', ['documents' => $docs, 'userOnSite' => $userOnSite, 'company' => $company, 'siteID' => $siteID]);
        Cache::forever('productContact'.$siteID, $view);
    }
    static function productObjects(){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        if (Cache::has('productObjects'.$siteID)) {
            Cache::forget('productObjects'.$siteID);
        }
        $objects =  Objects::where('siteID', $siteID)->where('published', 1)->get();
        $view = '';
        $view .= view('2019.product_services.objects_1', ['objects' => $objects, 'siteID' => $siteID]);
        Cache::forever('productObjects'.$siteID, $view);
    }
    static function productAbout(){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        if (Cache::has('productAbout'.$siteID)) {
            Cache::forget('productAbout'.$siteID);
        }
        $view = '';
        $view .= view('2019.product_services.about_1');
        Cache::forever('productAbout'.$siteID, $view);
    }
    static function productAnother($id){
        //dd($id);
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        //dd($siteID);
        $subcategory = Category::where('id', $id)->with('goods')->with('services')->with('categor')->first();
        //dd($subcategory);

        //$subcategory

        //dd($subcategory);
        if (Cache::has('productAnother'.$subcategory->id)) {
            Cache::forget('productAnother'.$subcategory->id);
        }
        if($subcategory->type == 0){
            $title = 'товары';
        }
        else{
            $title = 'услуги';
        }
        $view = '';
        $view .= view('2019.product_services.another_1', ['title' => $title, 'subcategory' =>$subcategory, 'siteID' => $siteID, 'cat' => $subcategory->id]);
        Cache::forever('productAnother'.$subcategory->id, $view);

    }
    static function deleteproductAnother($id){
        if (Cache::has('productAnother'.$id)) {
            Cache::forget('productAnother'.$id);
        }
    }

    static function objects(){
        //dd('asd');
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        if (Cache::has('doctype_objects_'.$siteID)) {
            Cache::forget('doctype_objects_'.$siteID);
        }
        $company = Site::where('id', $siteID)->value('name');
        $city = Information::where('siteID', $siteID)->value('city');
        $categories = Category::with('objects')->whereHas('objects', function($query){
            $query->where('parent_id', '!=', null);
        })->where('siteiD', $siteID)->get();

        $text = Template::with('texts')->where('siteID', $siteID)->where('slug', 'portfolio')->first();

        if($text->texts->checkseo == 1){
            $seoTitle = $text->texts->seoTitle;
            $seoDescription = $text->texts->seoTitle;
        }

        else{
            $seoTitle = $company.' объекты';
            $seoDescription = null;
        }

        $view = '';
        $view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'seoDescription' => $seoDescription, 'siteID' => $siteID]);
        Cache::forever('doctype_objects_'.$siteID, $view);
        if (Cache::has('objectBreadcrumb_'.$siteID)) {
            Cache::forget('objectBreadcrumb_'.$siteID);
        }
        $view = '';
        $view .= view('2019.objects.breadcrumb_1', ['object' => null, 'categories' => $categories, 'objects' => null]);
        Cache::forever('objectBreadcrumb_'.$siteID, $view);
        if (Cache::has('Objects_1_'.$siteID)) {
            Cache::forget('Objects_1_'.$siteID);
        }

        $objects =  Objects::with('category')->with('images')->where('siteID', $siteID)->where('published', 1)->get();

        //dd($categories);
        $view = '';
        $view .= view('2019.objects.objects_1', ['objects' => $objects, 'description' => $text->texts, 'categories' => $categories, 'siteID' => $siteID]);
        Cache::forever('Objects_1_'.$siteID, $view);
    }

    static function categoryObject($categoryid){
        //dd($categoryid);
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        if (Cache::has('doctype_object_category'.$categoryid)) {
            Cache::forget('doctype_object_category'.$categoryid);
        }
        $company = Site::where('id', $siteID)->value('name');
        $city = Information::where('siteID', $siteID)->value('city');
        $categories = Category::with('objects')->whereHas('objects', function($query){
            $query->where('parent_id', '!=', null);
        })->where('siteiD', $siteID)->get();
        $objects =  Objects::with('category')->with('images')->where('siteID', $siteID)->where('published', 1)->where('parent_id', $categoryid)->get();
        //dd($objects);
        $text = Template::with('texts')->where('siteID', $siteID)->where('slug', 'portfolio')->first();

        if($text->texts->checkseo == 1){
            $seoTitle = $text->texts->seoTitle;
            $seoDescription = $text->texts->seoTitle;
        }

        else{
            $seoTitle = $company.' объекты';
            $seoDescription = null;
        }

        $view = '';
        $view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'seoDescription' => $seoDescription, 'siteID' => $siteID]);
        Cache::forever('doctype_object_category'.$categoryid, $view);
        if (Cache::has('objectCategoryBreadcrumb_'.$categoryid)) {
            Cache::forget('objectCategoryBreadcrumb_'.$categoryid);
        }
        $view = '';
        $view .= view('2019.objects.breadcrumb_1', ['object' => null, 'categories' => $categories, 'objects' => $objects,]);
        Cache::forever('objectCategoryBreadcrumb_'.$categoryid, $view);
        if (Cache::has('Object_1_category_'.$categoryid)) {
            Cache::forget('Object_1_category_'.$categoryid);
        }



        //dd($categories);
        $view = '';
        $view .= view('2019.objects.objects_1', ['objects' => $objects, 'description' => $text->texts, 'categories' => $categories, 'siteID' => $siteID]);
        Cache::forever('Object_1_category_'.$categoryid, $view);
    }

    static function viewObject($id){

        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        $object =  Objects::with('images')->where('siteID', $siteID)->where('id', $id)->first();

        if (Cache::has('doctype_object_'.$object->id)) {
            Cache::forget('doctype_object_'.$object->id);
        }
        $company = Site::where('id', $siteID)->value('name');
        $city = Information::where('siteID', $siteID)->value('city');
        $categories = Category::with('objects')->whereHas('objects', function($query){
            $query->where('parent_id', '!=', null);
        })->where('siteiD', $siteID)->get();

        $seoTitle = 'Объект '.$object->title.' компании '.$company;
        $seoDescription = 'Объекты компании '.$company.' в городе '.$city;
        $view = '';
        $view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'seoDescription' => $seoDescription, 'siteID' => $siteID]);
        Cache::forever('doctype_object_'.$object->id, $view);
        if (Cache::has('objectViewBreadcrumb_'.$object->id)) {
            Cache::forget('objectViewBreadcrumb_'.$object->id);
        }
        //dd($object);

        $view = '';
        $view .= view('2019.objects.breadcrumb_1', ['object' => $object, 'siteID' => $siteID, 'categories' => $categories, 'objects' => Objects::where('siteID', $siteID)->where('id', $id)->get(),]);

        Cache::forever('objectViewBreadcrumb_'.$object->id, $view);
        if (Cache::has('Object_1_'.$object->id)) {
            Cache::forget('Object_1_'.$object->id);
        }

        //$objects =  Objects::with('images')->where('siteID', $siteID)->where('published', 1)->get();
        $view = '';

        $view .= view('2019.objects.view_1', ['object' => $object, 'siteID' => $siteID]);
        Cache::forever('Object_1_'.$object->id, $view);
    }

    static function about(){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        $text = Template::where('slug', 'about')->where('siteID', $siteID)->with('texts')->first();
        //dd($text);
        if (Cache::has('doctype_about_'.$siteID)) {
            Cache::forget('doctype_about_'.$siteID);
        }
        $company = Site::where('id', $siteID)->value('name');
        $city = Information::where('siteID', $siteID)->value('city');
        if($text->texts->checkseo == 1){
            $seoTitle = $text->texts->seoTitle;
            $seoDescription = $text->texts->seoTitle;
        }

        else{
            $seoTitle = $company.' о компании';
            $seoDescription = null;
        }
        $sections = Section::where('siteID', $siteID)->where('published', 1)->get();
        //dd($sections);
        $view = '';
        $view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle,  'seoDescription' => $seoDescription, 'siteID' => $siteID]);
        Cache::forever('doctype_about_'.$siteID, $view);
        if (Cache::has('about_'.$siteID)) {
            Cache::forget('about_'.$siteID);
        }
        $objects =  Objects::where('siteID', $siteID)->where('published', 1)->get();
        $view = '';
        $view .= view('2019.about.about_1', ['text' =>$text->texts, 'sections' => $sections,'objects' => $objects, 'siteID' => $siteID]);
        Cache::forever('about_'.$siteID, $view);
    }
    static function aboutObjects(){
        if(Auth::user() !== null){
            $siteID = Auth::user()->siteID;
        }
        else{
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }

        if (Cache::has('aboutObjects'.$siteID)) {
            Cache::forget('aboutObjects'.$siteID);
        }
        $objects =  Objects::where('siteID', $siteID)->where('published', 1)->get();
        $view = '';
        $view .= view('2019.about.objects_1', ['objects' => $objects, 'siteID' => $siteID]);
        Cache::forever('aboutObjects'.$siteID, $view);
    }

    static function contact(){
        if(Auth::user() == null){
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        else{
            $siteID = Auth::user()->siteID;
        }
        $text = Template::where('slug', 'contact')->where('siteID', $siteID)->with('texts')->first();
        //dd($text);
        if (Cache::has('doctype_contact_'.$siteID)) {
            Cache::forget('doctype_contact_'.$siteID);
        }
        $company = Site::where('id', $siteID)->value('name');
        $info = Information::where('siteID', $siteID)->first();
        $tels = Tel::where('siteID', $siteID)->get();
        $emails = Email::where('siteID', $siteID)->get();
        if($text->texts->checkseo == 1){
            $seoTitle = $text->texts->seoTitle;
            $seoDescription = $text->texts->seoTitle;
        }

        else{
            $seoTitle = $company.' о компании';
            $seoDescription = null;
        }
        $view = '';
        $view .= view('2019.layouts.doctype', ['seoTitle' =>$seoTitle, 'seoDescription' => $seoDescription, 'siteID' => $siteID]);
        Cache::forever('doctype_contact_'.$siteID, $view);
        if (Cache::has('contact_'.$siteID)) {
            Cache::forget('contact_'.$siteID);
        }
        $objects =  Objects::where('siteID', $siteID)->where('published', 1)->get();
        $view = '';
        $sections = Section::where('siteID', $siteID)->where('published', 1)->get();
        $view .= view('2019.contact.contact_1', ['text' =>$text->texts, 'sections' => $sections, 'info' => $info, 'tels' => $tels, 'emails' => $emails, 'siteID' => $siteID]);
        Cache::forever('contact_'.$siteID, $view);
    }

    static function menu(){
        //dd(Request::getHttpHost());
        if(Auth::user() !== null){
            $siteID = Auth::user()->siteID;
        }
        else{
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        //dd(Auth::user());

        if (Cache::has('menu_1_'.$siteID)) {
            Cache::forget('menu_1_'.$siteID);//slug = "contact" asc
        }
        $company = Site::with('about')->with('info')->where('id',$siteID)->first();
        //dd($company);
        $menu = Section::where('siteiD',$siteID)->whereIn('slug',['about','product','services','portfolio', 'contact', 'stroitelstvo', 'bani', 'proyekty', 'izgotovleniye'])
            ->where('published', 1)->orderByRaw('slug = "about" desc, slug = "contact" asc, slug = "portfolio" asc,  main asc')->get();

        //dd($menu);
        $tel = Tel::where('siteID', $siteID)->first();
        $email = Email::where('siteID', $siteID)->first();
        $categories = Category::where('siteID', $siteID)->where('published', 1)->where('parent_id', null)->get();
        //dd($services);
        $view = '';
        $view .= view('2019.layouts.menu.menu_1', ['menu' => $menu, 'info' => $company->info, 'tel' => $tel, 'email' => $email, 'siteID' => $siteID, 'categories' => $categories, 'logo' => Cache::get('logoOne'.$siteID)]);
        Cache::forever('menu_1_'.$siteID, $view);
    }

    static function menu2(){
        //dd(Request::getHttpHost());
        if(Auth::user() !== null){
            $siteID = Auth::user()->siteID;
        }
        else{
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        //dd(Auth::user());

        if (Cache::has('menu_1_'.$siteID)) {
            Cache::forget('menu_1_'.$siteID);//slug = "contact" asc
        }
        $company = Site::with('about')->with('info')->where('id',$siteID)->first();
        //dd($company);
        $menu = Section::where('siteiD',$siteID)->whereIn('slug',['about','product','services','portfolio', 'contact', 'stroitelstvo', 'bani', 'proyekty', 'izgotovleniye'])
            ->where('published', 1)->orderByRaw('slug = "about" desc, slug = "contact" asc, slug = "portfolio" asc,  main asc')->get();

        //dd($menu);
        $tel = Tel::where('siteID', $siteID)->first();
        $email = Email::where('siteID', $siteID)->first();
        $categories = Category::where('siteID', $siteID)->where('published', 1)->where('parent_id', null)->orderby('sort', 'asc')->get();
        //dd($services);
        $view = '';
        $view .= view('2019.layouts.menu.menu_2', ['menu' => $menu, 'info' => $company->info, 'tel' => $tel, 'email' => $email, 'siteID' => $siteID, 'categories' => $categories, 'logo' => Cache::get('logoOne'.$siteID)]);
        Cache::forever('menu_1_'.$siteID, $view);
    }
    static function menu3(){
        //dd(Request::getHttpHost());
        if(Auth::user() !== null){
            $siteID = Auth::user()->siteID;
        }
        else{
            $siteID = Site::where('title', Request::getHttpHost())->value('id');
        }
        //dd(Auth::user());

        if (Cache::has('menu_1_'.$siteID)) {
            Cache::forget('menu_1_'.$siteID);//slug = "contact" asc
        }
        $company = Site::with('about')->with('info')->where('id',$siteID)->first();
        //dd($company);

        //dd($menu);
        $tel = Tel::where('siteID', $siteID)->first();
        $email = Email::where('siteID', $siteID)->first();
        $categories = Category::where('siteID', $siteID)->where('published', 1)->where('main', 1)->where('type', 7)->where('parent_id', null)->orderby('sort', 'asc')->get();
        //dd($services);
        $view = '';
        $view .= view('2019.layouts.menu.menu_3', ['info' => $company->info, 'tel' => $tel, 'email' => $email, 'siteID' => $siteID, 'categories' => $categories, 'logo' => Cache::get('logoOne'.$siteID)]);
        Cache::forever('menu_1_'.$siteID, $view);
    }

    static function footer(){
        if(Auth::user() !== null){$siteID = Auth::user()->siteID;}
        else{$siteID = Site::where('title', Request::getHttpHost())->value('id');}

        $menu = Section::where('siteiD',$siteID)->whereIn('slug',['product','services','portfolio','stroitelstvo', 'bani', 'proyekty', 'izgotovleniye'])
            ->where('published', 1)->orderby('main', 'asc')->get();

        $tel = Tel::where('siteID', $siteID)->first();
        $email = Emails::where('siteID', $siteID)->first();
        $categories = Category::where('siteID', $siteID)->where('published', 1)->where('parent_id', null)->get();
        $goods = \App\Model\Goods::with('product_url_cat')->whereHas('product_url_cat', function($query){$query->where('published', 1);})->where('siteID', $siteID)->where('published', 1)->select('id','category','siteID','title','main','slug')->limit(6)->get();

        $company = Sites::with('information')->with('sections')->where('id', $siteID)->first();
        if (Cache::has('footer_1_'.$siteID)){Cache::forget('footer_1_'.$siteID);}
        $view = '';

        if($_SERVER["REQUEST_URI"] == '/'){$jq = 1;}
        else{$jq = null;}
        $view .= view('2019.layouts.footer.footer_1', ['city' => $company->information->cities, 'menu' => $menu, 'tel' => $tel, 'email' => $email, 'categories' =>$categories,'goods'=>$goods,  'jq' => $jq,'company' => $company, 'siteID'=>$siteID]);

        Cache::forever('footer_1_'.$siteID, $view);
    }

    static function footer2(){
        if(Auth::user() !== null){$siteID = Auth::user()->siteID;}
        else{$siteID = Site::where('title', Request::getHttpHost())->value('id');}

        $tel = Tel::where('siteID', $siteID)->first();
        $email = Emails::where('siteID', $siteID)->first();
        $categories = Category::where('siteID', $siteID)->where('published', 1)->where('parent_id', null)->get();
        $goods = \App\Model\Goods::with('product_url_cat')->whereHas('product_url_cat', function($query){$query->where('published', 1);})->where('siteID', $siteID)->where('published', 1)->select('id','category','siteID','title','main','slug')->limit(6)->get();

        $company = Sites::with('information')->with('sections')->where('id', $siteID)->first();
        $mainsection = Section::with(['category' => function($query)use($siteID){$query->where('siteID', $siteID);}])
            ->where('siteID', $siteID)->where('published', 1)->where('type', '!=', null)->orderby('main', 'asc')->limit(2)->get();

        $portfolio = Category::with('objects')->whereHas('objects', function($query){$query->where('parent_id', '!=', null)->where('published', 1);})->where('siteiD', $siteID)->where('published', 1)->select('id','parent_id','title','slug','published','seoname')->get();
        $partners = Partners::where('siteID', $siteID)->where('published', 1)->get();
        if (Cache::has('footer_1_'.$siteID)){Cache::forget('footer_1_'.$siteID);}
        $view = '';
        if($_SERVER["REQUEST_URI"] == '/'){$jq = 1;}
        else{$jq = null;}
        $view .= view('2019.layouts.footer.footer_2', ['city' => $company->information->cities, 'mainsection' => $mainsection, 'info' => $company->information, 'partners'=>$partners, 'portfolio'=>$portfolio, 'tel' => $tel, 'email' => $email, 'categories' =>$categories,'goods'=>$goods,  'jq' => $jq,'company' => $company, 'siteID'=>$siteID]);

        Cache::forever('footer_1_'.$siteID, $view);
    }

    static function favicon(){
        $id = Cache::get('/' . Request::getHttpHost())->id;
        if(Cache::has('favicon'.$id)){
            return Cache::get('favicon'.$id);
        }
        else{
            $favicon =  Information::where('siteID', Auth::user()->siteID)->value('favicon');
            Cache::forever('favicon'.$id, $favicon);
            return '/storage/app/'.$favicon;
        }
    }
}
