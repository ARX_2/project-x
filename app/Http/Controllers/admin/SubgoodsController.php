<?php

namespace App\Http\Controllers\admin;

use App\Subgoods;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Users;
use App\Image;
use App\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Company;
use ImageResize;
use App\Site;
use App\Property;
class SubgoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $id =  Subgoods::insertGetId(['title' => $request->title,
        'type_coast' => $request->type_coast,
        'coast' => $request->coast, 'coast_before' => $request->coast_before,
        'goods' => $request->goods, 'color' => $request->color]);
      //dd($id);
      if($request->file('file') !== null){
        foreach ($request->file('file') as $file) {
            $path = Storage::putFile('public', $file);
            Image::insert(['images' => $path, 'subgoods' => $id]);
        }
      }
        return redirect()->route('admin.goods.edit', $request->goods);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subgoods  $subgoods
     * @return \Illuminate\Http\Response
     */
    public function show(Subgoods $subgoods)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subgoods  $subgoods
     * @return \Illuminate\Http\Response
     */
    public function edit(Subgoods $subgoods)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subgoods  $subgoods
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subgoods $subgoods)
    {

        Subgoods::where('id', $request->subgoods)->update(['title'=> $request->title,
        'type_coast' => $request->type_coast, 'coast' => $request->coast,
        'coast_before' => $request->coast_before, 'color' => $request->color]);
        if($request->file('file') !== null){
        foreach ($request->file('file') as $file) {
            $path = Storage::putFile('public', $file);
            Image::insert(['images' => $path, 'subgoods' => $request->subgoods]);
        }
        }
        return redirect('account/goods/'.$request->goods.'/edit?id='.$request->goods);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subgoods  $subgoods
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subgoods $subgoods)
    {
        //
    }

    public function imageDelete(Request $request){
      $img = Image::where('id', $request->id)->first();
      if($img !== null){
      Image::with('categories')->where('images', $img->images)->delete();
      Storage::delete($img->images);
      $images = DB::table('image')->where('image', $img->images)->get();
      //dd($images);
      foreach ($images as $i) {
        DB::table('image')->where('image', $img->images)->delete();
        Storage::delete($i->fullimagesize);
      }
      }
    }
}
