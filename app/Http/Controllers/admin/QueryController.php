<?php

namespace App\Http\Controllers;

use DB;
use App\Offer;
use Request;
use App\Template;
use App\Work;
use ImageResize;
use App\Forumsubcategory;
use App\ForumMessage;
use App\Information;
use App\Category;
use App\ImageResize as Img;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Section;
use App\Comment;
use App\Support;
use Browser;
class QueryController extends Controller
{
  static function date($date){

    $datemonth = ['01' => 'января', '02' => 'февраля', '03' => 'марта',
    '04' => 'апреля', '05' => 'мая', '06' => 'июня', '07' => 'июля', '08' => 'августа',
    '09' => 'сентября', '10' => 'октября', '11' => 'ноября', '12' => 'декабря'];
    if($date !== null){
      $date = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->addHours(5)->toDateTimeString();
    }

    //dd($date);
    $month = date("m", strtotime($date));
    $day = date("d", strtotime($date));
    $year = date("Y", strtotime($date));
    $hour = date("H", strtotime($date));
    $min = date("i", strtotime($date));
    return $day.' '.$datemonth[$month].' '.$year.' в '.$hour.':'.$min;
  }

  static function messageDate($date){
    $datemonth = ['01' => 'января', '02' => 'февраля', '03' => 'марта',
    '04' => 'апреля', '05' => 'мая', '06' => 'июня', '07' => 'июля', '08' => 'августа',
    '09' => 'сентября', '10' => 'октября', '11' => 'ноября', '12' => 'декабря'];
    if($date !== null){
      $date = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->addHours(5)->toDateTimeString();
    }


    $month = date("m", strtotime($date));
    $day = date("d", strtotime($date));
    $year = date("Y", strtotime($date));
    $hour = date("H", strtotime($date));
    $min = date("i", strtotime($date));
    return $day.' '.$datemonth[$month].' '.$hour.':'.$min;
  }

  static function dayMonth($date){
    $datemonth = ['01' => 'января', '02' => 'февраля', '03' => 'марта',
    '04' => 'апреля', '05' => 'мая', '06' => 'июня', '07' => 'июля', '08' => 'августа',
    '09' => 'сентября', '10' => 'октября', '11' => 'ноября', '12' => 'декабря'];
    if($date !== null){
      $date = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->addHours(5)->toDateTimeString();
    }


    $month = date("m", strtotime($date));
    $day = date("d", strtotime($date));
    $year = date("Y", strtotime($date));
    $hour = date("H", strtotime($date));
    $min = date("i", strtotime($date));
    return $day.' '.$datemonth[$month];
  }

    static function noTimeDate($date){
        $noTimeDate = ['01' => 'января', '02' => 'февраля', '03' => 'марта',
            '04' => 'апреля', '05' => 'мая', '06' => 'июня', '07' => 'июля', '08' => 'августа',
            '09' => 'сентября', '10' => 'октября', '11' => 'ноября', '12' => 'декабря'];
        if($date !== null){
            $date = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->addHours(5)->toDateTimeString();
        }

        $month = date("m", strtotime($date));
        $day = date("d", strtotime($date));
        $year = date("Y", strtotime($date));
        $hour = date("H", strtotime($date));
        $min = date("i", strtotime($date));
        return $day.' '.$noTimeDate[$month].' '.$year;
    }

  static function ResizeImage($image, $size){
    //dd($image->images);
    
    if(explode('/', $image)[0] == 'public'){
        $imageName = $image;
    }
    else{
        if(isset($image->images)){
      $imageName = $image->images;
    }
    elseif(isset($image->image)){
      if($image->image == null){
        return 'public/no-image-170x170.jpg';
      }
      else{
        $imageName = $image->image;
      }
    }
    else{
      return 'public/no-image-170x170.jpg';
    }
    }
    
    $original = $imageName;
    $imagename = explode('.', $imageName);
    $sizearray = explode('x', $size);
    if(Browser::isSafari() || Browser::isIE()){
        $memetype = $imagename[1];
    }
    else{
        $memetype = 'webp';
    }

    //dd($original);
    if(file_exists('storage/app/'.$original)){

          }
           else{
               return 'public/no-image-170x170.jpg';
      }
    if(file_exists('storage/app/imageResize/'. $imagename[0].'_'.$size.'.'.$memetype)){
    
             return 'imageResize/'.$imagename[0].'_'.$size.'.'.$memetype;
          }
           else {
               if($imagename[1] == 'jpeg'){
                   $meme = 'jpg';
               }
               else{
                   $meme = $imagename[1];
               }
      if($sizearray[0] == 0){
        $stream = ImageResize::make('storage/app/'.$original)
      ->stream($meme, 60);
        ImageResize::make($stream)
        ->fit($sizearray[0], $sizearray[1])
        ->save('storage/app/imageResize/'. $imagename[0].'_'.$size.'.'.$imagename[1], 55);
      }
      elseif($sizearray[1] == 0){
        $stream = ImageResize::make('storage/app/'.$original)
      ->stream($meme, 60);
        ImageResize::make($stream)
        ->fit($sizearray[0], $sizearray[1])
        ->save('storage/app/imageResize/'. $imagename[0].'_'.$size.'.'.$imagename[1], 55);
      }
      else{
        $stream = ImageResize::make('storage/app/'.$original)
      ->stream($meme, 80);
        ImageResize::make($stream)
        ->fit($sizearray[0], $sizearray[1])
        ->save('storage/app/imageResize/'. $imagename[0].'_'.$size.'.'.$imagename[1], 80);
      }

        //dd($imagename[0]);
        //dd($imageName);
        if($memetype == 'webp'){
            \Buglinjo\LaravelWebp\Facades\LaravelWebp::make('storage/app/imageResize/'. $imagename[0].'_'.$size.'.'.$imagename[1])->save('storage/app/imageResize/'. $imagename[0].'_'.$size.'.webp', 70);
        
        unlink(storage_path('app/imageResize/'. $imagename[0].'_'.$size.'.'.$imagename[1]));
        }
        
        //$optimizerChain = app(\Spatie\ImageOptimizer\OptimizerChain::class)->optimize('storage/app/imageResize/'. $imagename[0].'_'.$size.'.'.$imagename[1]);
        return 'imageResize/'.$imagename[0].'_'.$size.'.'.$memetype;
      }
  }


    static function sections(){
      $sections =  Section::where('siteID', Auth::user()->siteID)->get();
      //dd($sections);
      $secs = '';
      if(count($sections)>0){
        if($sections[0]->published == 1){
          $secs .= '<li><a href="'.route('admin.documentation.index').'"><div class="name">Документы<div class="new_name">Например "Лицензии"</div></div></a></li>';
        }
        if($sections[1]->published == 1){
          $secs .= '<li><a href="'.route('admin.object.index').'"><div class="name">Портфолио<div class="new_name">Например "Наши работы"</div></div></a></li>';
        }
        if($sections[2]->published == 1){
          $secs .= '<li><a href="'.route('admin.jobs.index').'"><div class="name">Вакансии</div></a></li>';
        }
        if($sections[3]->published == 1){
          $secs .= '<li><a href="'.route('admin.action.index').'"><div class="name">Акции</div></a></li>';
        }
        if($sections[4]->published == 1){
          $secs .= '<li><a href="'.route('admin.news.index').'"><div class="name">Новости</div></a></li>';
        }
        if($sections[5]->published == 1){
          $secs .= '<li><a href="'.route('admin.blog.index').'"><div class="name">Блог</div></a></li>';
        }
        if($sections[6]->published == 1){
          $secs .= '<li><a href="'.route('admin.price.index').'"><div class="name">Прайс-лист</div></a></li>';
        }
      }
        //dd($secs);
        return $secs;
    }

    static function company(){
    return \App\Site::with('about')->with('info')->with('contacts')->with(['sections' => function($sect){
      $sect->whereIn('slug', ['product', 'services', 'stroitelstvo', 'bani', 'proyekty', 'izgotovleniye']);
      $sect->orderBy('main', 'asc');
      }])->where('id', Auth::user()->siteID)->first();
    }

    static function countMessages(){

      $action = Comment::where('users_id', Auth::user()->id)->select('support_id')->distinct()->get();
      $actions = [];
      foreach ($action as $value) {
        $actions[] = $value->support_id;
      }
      $count = Comment::whereIn('support_id', $actions)->where('readed',0)->where('users_id', '!=', Auth::user()->id)->count('id');
      if(Auth::user()->userRolle == 1){
        $count = $count + Support::where('status',0)->count('id');
      }
      if($count == 0){
        return null;
      }
      else{

        return '<span style="color:#55a6e8;">+'.$count.'</span>';
      }

    }

    static function lastComment(){
      $comment = Support::where('users_id', Auth::user()->id)->orderBy('id', 'desc')->value('id');
      if($comment == null){
        $comment = 'none';
      }
      return $comment;
    }
    static function recaptcha(){
        $id = Cache::get('/' . Request::getHttpHost())->id;
       return Information::where('siteID',$id)->value('reCaptcha');
    }
}
