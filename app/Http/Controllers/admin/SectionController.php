<?php

namespace App\Http\Controllers\Admin;

use App\Section;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Caching;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Site;
use Resize;
class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Resize::company();
        return view('admin.sections.index',[
          'company' => $company,
          'sections' => Section::where('siteID', Auth::user()->siteID)->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function show(Section $section)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section)
    {
      //dd($request);
      foreach ($request['published'] as $key => $value) {
        Section::where('siteID', Auth::user()->siteID)->where('id', $key)->update(['published' => $value]);
      }
      Caching::menu();
      Caching::footer();
      return redirect()->route('admin.sections.index');
        //dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section)
    {
        //
    }

    public function main(){
      $i = 1;
      foreach ($_GET['arrayorder'] as $s) {
        //dd($s);
        Section::where('id', $s)->update(['main'=> $i]);
        $i++;
      }
      Caching::menu();
      Caching::footer();
    }
}
