<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\Goods;
use App\User_Site as Usite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Resize;
use App\Image;
use App\Admin;
use DB;
use App\Users;
use App\Template;
use App\Site;
use App\Company;
use App\ImageResize;
use App\Property;
use App\Support;
use App\Menu;
use App\Includes;
use App\Action;
use App\Banner;
use App\Blog;
use App\Category_price;
use App\Documentation;
use App\Jobs;
use App\News;
use App\Contact;
use App\Objects;
use App\Opinion;
use App\Partner;
use App\Pricelist;
use App\Text;
use App\Information;
use App\Subcategory;
use App\Section;
use Caching;
use App\Email;
use App\Tel;
use App\Tizer;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Model\Emails;
use App\Model\Sites;
use App\Model\Partners;
class AdminController extends Controller
{
  static function ResizeImage($image, $size){
    //dd($a);

    if($image->images !== null){
      $imageName = $image->images;
    }
    else{
      $imageName = $image->image;
    }

    if($image->imageResize == null){
      $imagename = explode('.', $imageName);
      $sizearray = explode('x', $size);
      //dd($image);

      ImageResize::make('storage/app/'.$imageName)
      ->resize($sizearray[0], $sizearray[1],function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })
      ->save('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);
      //dd($sizearray);
      $optimizerChain = app(\Spatie\ImageOptimizer\OptimizerChain::class)->optimize('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);
        $img =DB::table('image')->where('image', $imageName)->value('image');
        if($img == null){
          DB::table('image')->insert(['image' => $imageName,'imagesize' => $imagename[0], 'fullimagesize' => $imagename[0].'_'.$size.'.'.$imagename[1], 'size' => $size, 'mele' => $imagename[1]]);
        }
    }
    else{
      if($image->imageResize->size !== $size){
        $imagename = explode('.', $imageName);
        $sizearray = explode('x', $size);

        ImageResize::make('storage/app/'.$imageName)
        ->resize($sizearray[0], $sizearray[1],function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })
        ->save('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);

        $optimizerChain = app(\Spatie\ImageOptimizer\OptimizerChain::class)->optimize('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);
          $img =DB::table('image')->where('image', $imageName)->value('image');
          if($img == null){
            DB::table('image')->insert(['image' => $imageName,'imagesize' => $imagename[0], 'fullimagesize' => $imagename[0].'_'.$size.'.'.$imagename[1], 'size' => $size, 'mele' => $imagename[1]]);
          }
      }
    }


  }

  public function createSite(Request $request){
    $id = Site::insertGetId(['title' => $request->title, 'type' => $request->type]);
    DB::table('visual')->insert(['siteID' => $id, 'menu' => 1, 'footer' => 1]);
    $sateName = $request->title;
    $templates = [0 => ['template' => 1, 'templatename' => 'index', 'slug' => '/', 'siteID' => $id],
                  1 => ['template' => 2, 'templatename' => 'chief_1', 'slug' => 'about', 'siteID' => $id],
                  2 => ['template' => 3, 'templatename' => 'chief_1', 'slug' => 'contact', 'siteID' => $id],
                  3 => ['template' => 4, 'templatename' => 'chief_1', 'slug' => 'product', 'siteID' => $id],
                  4 => ['template' => 5, 'templatename' => 'chief_1', 'slug' => 'services', 'siteID' => $id],
                  5 => ['template' => 6, 'templatename' => 'chief_1', 'slug' => 'portfolio', 'siteID' => $id],
                  6 => ['template' => 7, 'templatename' => 'view_1', 'slug' => 'jobs', 'siteID' => $id],
                  7 => ['template' => 8, 'templatename' => 'view_1', 'slug' => 'action', 'siteID' => $id],
                  8 => ['template' => 9, 'templatename' => 'view_1', 'slug' => 'news', 'siteID' => $id],
                  9 => ['template' => 10, 'templatename' => 'view_1', 'slug' => 'blog', 'siteID' => $id],
                  10 => ['template' => 11, 'templatename' => 'view_1', 'slug' => 'pricelist', 'siteID' => $id],
                  11 => ['template' => 12, 'templatename' => 'view_1', 'slug' => 'faq', 'siteID' => $id],
                  12 => ['template' => 13, 'templatename' => 'view_1', 'slug' => 'partners', 'siteID' => $id],
                  13 => ['template' => 14, 'templatename' => 'view_1', 'slug' => 'documentation', 'siteID' => $id],
                  14 => ['template' => 15, 'templatename' => 'view_1', 'slug' => 'stroitelstvo', 'siteID' => $id],
                  15 => ['template' => 16, 'templatename' => 'view_1', 'slug' => 'bani', 'siteID' => $id],
                  16 => ['template' => 17, 'templatename' => 'view_1', 'slug' => 'proyekty', 'siteID' => $id],
                  17 => ['template' => 18, 'templatename' => 'view_1', 'slug' => 'izgotovleniye', 'siteID' => $id],];

                  $sections = [0 => ['title' => 'Документы', 'slug' => 'documantation', 'published' => 0, 'siteID' => $id,'main' => null,'type' => null],
                               1 => ['title' => 'Портфолио', 'slug' => 'portfolio', 'published' => 0, 'siteID' => $id,'main' => null,'type' => null],
                               2 => ['title' => 'Вакансии', 'slug' => 'jobs','published' => 0, 'siteID' => $id,'main' => null,'type' => null],
                               3 => ['title' => 'Акции', 'slug' => 'action','published' => 0, 'siteID' => $id,'main' => null,'type' => null],
                               4 => ['title' => 'Новости', 'slug' => 'news', 'published' => 0, 'siteID' => $id,'main' => null,'type' => null],
                               5 => ['title' => 'Блог', 'slug' => 'blog', 'published' => 0, 'siteID' => $id,'main' => null,'type' => null],
                               6 => ['title' => 'Прайс-лист', 'slug'=> 'pricelist', 'published' => 0, 'siteID' => $id,'main' => null,'type' => null],
                               7 => ['title' => 'Наши партнеры', 'slug' => 'partners', 'published' => 0, 'siteID' => $id,'main' => null,'type' => null],
                               8 => ['title' => 'Компании', 'slug' => 'about', 'published' => 1, 'siteID' => $id,'main' => null,'type' => null],
                               9 => ['title' => 'Товары', 'oldtitle' => 'Товары', 'icon' => 'fa-cubes',  'slug' => 'product', 'published' => 1, 'siteID' => $id, 'main' => 1, 'type' => 0],
                               10 => ['title' => 'Услуги', 'oldtitle' => 'Услуги', 'icon' => 'fa-handshake-o', 'slug' => 'services', 'published' => 1, 'siteID' => $id, 'main' => 2, 'type' => 1],
                               11 => ['title' => 'Контакты', 'oldtitle' => 'Контакты', 'slug' => 'contact', 'published' => 1, 'siteID' => $id,'main' => null,'type' => null],
                               12 => ['title' => 'Строительство', 'oldtitle' => 'Строительство', 'slug' => 'stroitelstvo', 'published' => 0, 'siteID' => $id, 'main' => 3, 'type' => 4],
                               13 => ['title' => 'Бани', 'oldtitle' => 'Бани', 'slug' => 'bani', 'published' => 0, 'siteID' => $id, 'main' => 4, 'type' => 5],
                               14 => ['title' => 'Проекты', 'oldtitle' => 'Проекты', 'slug' => 'proyekty', 'published' => 0, 'siteID' => $id, 'main' => 5, 'type' => 6],
                               15 => ['title' => 'Мебель', 'oldtitle' => 'Мебель', 'slug' => 'izgotovleniye', 'published' => 0, 'siteID' => $id, 'main' => 6, 'type' => 7],
                             ];
    //dd($templates);
                  //dd($sections);

  $texts = ['/' => ['type' => 1, 'siteID' => $id], 'about' => ['type' => 2, 'siteID' => $id], 'contact' => ['type' => 3, 'siteID' => $id],
            'product' => ['type' => 4, 'siteID' => $id], 'services' => ['type' => 5, 'siteID' => $id], 'portfolio' => ['type' => 6, 'siteID' => $id], 'jobs' => ['type' => 7, 'siteID' => $id],
            'action' => ['type' => 8, 'siteID' => $id], 'news' => ['type' => 9, 'siteID' => $id], 'blog' => ['type' => 10, 'siteID' => $id], 'pricelist' => ['type' => 11, 'siteID' => $id],
            'faq' => ['type' => 12, 'siteID' => $id], 'partners' => ['type' => 13, 'siteID' => $id], 'documentation' => ['type' => 14, 'siteID' => $id], 'stroitelstvo' => ['type' => 15, 'siteID' => $id],
            'bani' => ['type' => 16, 'siteID' => $id], 'proyekty' => ['type' => 17, 'siteID' => $id], 'izgotovleniye' => ['type' => 18, 'siteID' => $id]];
  //dd($texts);
  //dd($texts);
  // foreach ($menu as $m) {
  //   //dd($m['title']);
  //   Menu::insert(['title' => $m['title'], 'slug' => $m['slug'], 'slide2' => $m['slide2'], 'siteID' => $m['siteID']]);
  // }
  foreach ($sections as $section) {
    Section::insert(['title' => $section['title'], 'slug' => $section['slug'], 'published' => $section['published'], 'siteID' => $section['siteID'], 'main' => $section['main'], 'type' => $section['type']]);
  }



    foreach ($templates as $t) {
      //dd($t);
      //dd($texts[$t['slug']]['type']);
      $textid =  Text::insertGetId(['type' => $texts[$t['slug']]['type'], 'siteID' => $texts[$t['slug']]['siteID'], 'created_at' => date('Y-m-d H:i:s')]);

  $idTemplate =  Template::insertGetId(['template' => $t['template'], 'slug' => $t['slug'], 'templatename' => $t['templatename'], 'setting' => $textid, 'siteID' => $t['siteID']]);
    if($t['template'] == 4){
      Includes::insert(['template' => $idTemplate, 'locate' => 1, 'catalog' => 'menu', 'title' => 'doctype_goods_'.$id, 'siteID' => $t['siteID']]);
      Includes::insert(['template' => $idTemplate, 'locate' => 2, 'catalog' => 'banner', 'title' => 'menu_1_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'menu']);
      Includes::insert(['template' => $idTemplate, 'locate' => 3, 'catalog' => 'products', 'title' => 'categoryBreadcrumb_goods_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'breadcrumb']);
      Includes::insert(['template' => $idTemplate, 'locate' => 4, 'catalog' => 'objects', 'title' => 'categoryMenuGoods'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'categoryMenu']);
      Includes::insert(['template' => $idTemplate, 'locate' => 6, 'catalog' => 'about', 'title' => 'categoryMenuDocs'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'categoryDocs']);
      Includes::insert(['template' => $idTemplate, 'locate' => 7, 'catalog' => 'map', 'title' => $sateName.'.product'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'categories']);
      Includes::insert(['template' => $idTemplate, 'locate' => 8, 'catalog' => 'footer', 'title' => 'footer_1_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'footer']);
    }
    elseif($t['template'] == 5){
      Includes::insert(['template' => $idTemplate, 'locate' => 1, 'catalog' => 'menu', 'title' => 'doctype_servic_'.$id, 'siteID' => $t['siteID']]);
      Includes::insert(['template' => $idTemplate, 'locate' => 2, 'catalog' => 'products', 'title' => 'menu_1_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'menu']);
      Includes::insert(['template' => $idTemplate, 'locate' => 3, 'catalog' => 'footer', 'title' => 'categoryBreadcrumb_services_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'breadcrumb']);
      Includes::insert(['template' => $idTemplate, 'locate' => 4, 'catalog' => 'footer', 'title' => 'categoryMenuServices'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'categoryMenu']);
      Includes::insert(['template' => $idTemplate, 'locate' => 5, 'catalog' => 'footer', 'title' => 'categoryMenuDocs'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'categoryDocs']);
      Includes::insert(['template' => $idTemplate, 'locate' => 6, 'catalog' => 'footer', 'title' => $sateName.'.services'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'categories']);
      Includes::insert(['template' => $idTemplate, 'locate' => 7, 'catalog' => 'footer', 'title' => 'footer_1_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'footer']);
    }
    elseif($t['template'] == 6){
      Includes::insert(['template' => $idTemplate, 'locate' => 1, 'catalog' => 'menu', 'title' => 'doctype_objects_'.$id, 'siteID' => $t['siteID']]);
      Includes::insert(['template' => $idTemplate, 'locate' => 2, 'catalog' => 'services', 'title' => 'menu_1_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'menu']);
      Includes::insert(['template' => $idTemplate, 'locate' => 3, 'catalog' => 'footer', 'title' => 'objectBreadcrumb_'.$id, 'siteID' => $t['siteID']]);
      Includes::insert(['template' => $idTemplate, 'locate' => 4, 'catalog' => 'footer', 'title' => 'categoryMenuDocs'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'categoryDocs']);
      Includes::insert(['template' => $idTemplate, 'locate' => 5, 'catalog' => 'footer', 'title' => 'Objects_1_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'objects']);
      Includes::insert(['template' => $idTemplate, 'locate' => 6, 'catalog' => 'footer', 'title' => 'footer_1_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'footer']);
    }
    elseif($t['template'] == 2){
      Includes::insert(['template' => $idTemplate, 'locate' => 1, 'catalog' => 'menu', 'title' => 'doctype_about_'.$id, 'siteID' => $t['siteID']]);
      Includes::insert(['template' => $idTemplate, 'locate' => 2, 'catalog' => 'objects', 'title' => 'menu_1_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'menu']);
      Includes::insert(['template' => $idTemplate, 'locate' => 3, 'catalog' => 'about', 'title' => 'about_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'about']);
      Includes::insert(['template' => $idTemplate, 'locate' => 4, 'catalog' => 'footer', 'title' => 'footer_1_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'footer']);
    }
    elseif($t['template'] == 3){
      Includes::insert(['template' => $idTemplate, 'locate' => 1, 'catalog' => 'menu', 'title' => 'doctype_contact_'.$id, 'siteID' => $t['siteID']]);
      Includes::insert(['template' => $idTemplate, 'locate' => 2, 'catalog' => 'about', 'title' => 'menu_1_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'menu']);
      Includes::insert(['template' => $idTemplate, 'locate' => 3, 'catalog' => 'contact', 'title' => 'contact_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'contact']);
      Includes::insert(['template' => $idTemplate, 'locate' => 4, 'catalog' => 'footer', 'title' => 'footer_1_'.$id, 'siteID' => $t['siteID'], 'CacheMethod' => 'footer']);
    }
    // elseif($t['template'] == 6){
    //   Includes::insert(['template' => $idTemplate, 'locate' => 1, 'catalog' => 'menu', 'title' => 'menu_2', 'siteID' => $t['siteID']]);
    //   Includes::insert(['template' => $idTemplate, 'locate' => 2, 'catalog' => 'contact', 'title' => 'contacts_2', 'siteID' => $t['siteID']]);
    //   Includes::insert(['template' => $idTemplate, 'locate' => 3, 'catalog' => 'footer', 'title' => 'footer_2', 'siteID' => $t['siteID']]);
    // }
    // elseif($t['template'] == 7){
    //   Includes::insert(['template' => $idTemplate, 'locate' => 1, 'catalog' => 'menu', 'title' => 'menu_2', 'siteID' => $t['siteID']]);
    //   Includes::insert(['template' => $idTemplate, 'locate' => 2, 'catalog' => 'products', 'title' => 'goods_6', 'siteID' => $t['siteID']]);
    //   Includes::insert(['template' => $idTemplate, 'locate' => 3, 'catalog' => 'footer', 'title' => 'footer_2', 'siteID' => $t['siteID']]);
    // }


  }
  //dd($id);
  Information::insert(['siteID' => $id]);
  Users::where('id', Auth::user()->id)->update(['siteID'=> $id]);

    if($request->email !== null){
      $firstName = explode(' ', $request->name)[0];
      $lastname = explode(' ', $request->name)[1];
      $contact = Users::create(['name' => $firstName,
                                'email' => $request->email,
                                'password' => bcrypt('31415262738300'),
                                'userRolle' => 4,
                                'published' => 1,
                                'tel' => $request->tel,
                                'siteID' => Auth::user()->siteID,
                                'sex' => 1,
                                'job' => 'Менеджер',
                                'lastname' => $lastname,
                                'onSite' => 1]);
    Tel::insert(['tel' => $request->tel, 'title' => 'Менеджер', 'siteID' => Auth::user()->siteID]);
    Email::insert(['tel' => $request->email, 'title' => 'Менеджер', 'siteID' => Auth::user()->siteID]);

    }

    if($request->content !== null){
      self::takeContent($request->content);
    }

     return redirect()->route('admin.company.index');
    //dd($request->title);
  }

  public function updateSite(Request $request){
    //dd(Category::max('id'));
    if($request->sites == null){
      return redirect()->back();
    }
      foreach ($request->sites as $id) {


        //dd($id);
        // Category::where('siteID',$id)->delete();
        // Goods::where('siteID', $id)->delete();
        // Banner::where('siteID', $id)->delete();
        // Blog::where('siteID', $id)->delete();
        // Documentation::where('siteID', $id)->delete();
        // Email::where('siteID', $id)->delete();
        // Tel::where('siteID', $id)->delete();
        //
        // $images = Image::where('siteID', $id)->get();
        // foreach ($images as $img) {
        //   Storage::delete($img);
        // }
        // Image::where('siteID',$id)->delete();
        // Information::where('siteID', $id)->delete();
        // News::where('siteID', $id)->delete();
        // Objects::where('siteID', $id)->delete();
        // Opinion::where('siteID',$id)->delete();
        // Partner::where('siteID',$id)->delete();
        // Tizer::where('siteID', $id)->delete();
        $categries = Category::with('goodsAll')->with('images')->with(['objects' => function($query){
          $query->with('images');
        }])->where('siteID', Auth::user()->siteID)->get();
        $oldCategoryID = [];
        $oldProductID = [];
        if($request->categories !== null){
          self::backupCategories($id);
          if($request->clear !== null){
            Category::where('siteID',$id)->delete();
          }
        }
        if($request->goods !== null || $request->services || $request->izgotovleniye || $request->stroitelstvo || $request->bani || $request->proyekty){

          if($request->goods !== null && $request->clear !== null){
            self::backupGoods($id, 0);
            Goods::where('siteID', $id)->whereHas('categories', function($query){
              $query->where('type', 0);
            })->delete();
          }
          if($request->services !== null && $request->clear !== null){
            self::backupGoods($id, 1);
            Goods::where('siteID', $id)->whereHas('categories', function($query){
              $query->where('type', 0);
            })->delete();
          }
          if($request->izgotovleniye !== null && $request->clear !== null){
            self::backupGoods($id, 7);
            Goods::where('siteID', $id)->whereHas('categories', function($query){
              $query->where('type', 7);
            })->delete();
          }
          if($request->stroitelstvo !== null && $request->clear !== null){
            self::backupGoods($id, 4);
            Goods::where('siteID', $id)->whereHas('categories', function($query){
              $query->where('type', 4);
            })->delete();
          }
          if($request->bani !== null && $request->clear !== null){
            self::backupGoods($id, 5);
            Goods::where('siteID', $id)->whereHas('categories', function($query){
              $query->where('type', 5);
            })->delete();
          }
          if($request->proyekty !== null && $request->clear !== null){
            self::backupGoods($id, 6);
            Goods::where('siteID', $id)->whereHas('categories', function($query){
              $query->where('type', 6);
            })->delete();
          }
          if($request->categories == null && $request->clear !== null){
            Goods::where('siteID', $id)->delete();
          }
          Goods::where('siteID', $id)->whereNotExists(function($query){
            $query->select(DB::raw(1))
                        ->from('categories')
                        ->whereRaw('categories.id = goods.category');
          })->delete();

        }

        if($request->objects !== null){
          self::backupObjects($id);
        }
        if($request->clear !== null){
          Objects::where('siteID', $id)->delete();
        }

        foreach ($categries as $category) {
          //КАТЕГОРИИ

          if($request->categories !== null){
            if($category->parent_id == null){
              $newCategoryID = Category::insertGetId(['seotitle' => $category->seotitle, 'seoname' => $category->seoname, 'seodescription' => $category->seodescription, 'seokeywords' => $category->seokeywords, 'title' => $category->title, 'description' => $category->description,
              'slug' => $category->slug, 'main' => $category->main, 'type' => $category->type, 'published' => $category->published, 'open_cat' => $category->open_cat, 'youtube' => $category->youtube, 'deleted' => 0, 'offer' => $category->offer,
            'sort' => $category->sort, 'checkseo' => $category->checkseo, 'status' => $category->status, 'siteID' => $id, 'created_at' => date('Y-m-d H:i:s'), 'protected' => $category->protected]);
            $oldCategoryID[$category->id] = $newCategoryID;
            }
            else{
              //dd($oldCategoryID);
              if(!isset($oldCategoryID[$category->parent_id])){
                $oldCategoryID[$category->parent_id] = Category::max('id') + $category->parent_id - $category->id;
              }
              $newCategoryID = Category::insertGetId(['seotitle' => $category->seotitle, 'seoname' => $category->seoname, 'seodescription' => $category->seodescription, 'seokeywords' => $category->seokeywords, 'title' => $category->title, 'description' => $category->description,
              'slug' => $category->slug, 'main' => $category->main, 'type' => $category->type, 'published' => $category->published, 'open_cat' => $category->open_cat, 'youtube' => $category->youtube, 'deleted' => 0, 'offer' => $category->offer,
            'sort' => $category->sort, 'checkseo' => $category->checkseo, 'status' => $category->status, 'siteID' => $id, 'parent_id' => $oldCategoryID[$category->parent_id], 'protected' => $category->protected, 'created_at' => date('Y-m-d H:i:s'), ]);
            $oldCategoryID[$category->id] = $newCategoryID;
            }
            if($category->images !== null && file_exists('storage/app/'.$category->images->images)){
              //dd($category);
              // $newImage = 'public/'.uniqid().'.'.explode('.',$category->images->images)[1];
              // Storage::copy($category->images->images, $newImage);
              Image::insertGetId(['images' => $category->images->images, 'category' => $newCategoryID, 'siteID' => $id, 'created_at' => date('Y-m-d H:i:s')]);
            }
          }
          else{
            $newCategoryID = Category::where('slug', $category->slug)->where('siteID', $id)->value('id');
          }


          //ТОВАРЫ

          if($request->goods !== null && $category->type == 0){
            foreach ($category->goodsAll as $goods) {
              if($goods->parent_id == null){
              $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> $id, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                'counts' => $goods->counts, 'status' => $goods->status, 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'protected' => $goods->protected, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                $oldProductID[$goods->id] = $newProductID;
              }
              else{
                $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> $id, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                  'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                  'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                  'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                  'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                  'counts' => $goods->counts, 'status' => $goods->status, 'parent_id' => $oldProductID[$goods->parent_id], 'min_count' => $goods->min_count, 'protected' => $goods->protected, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                  $oldProductID[$goods->id] = $newProductID;
              }

                foreach ($goods->imageses as $image) {
                  if($image->images !== null && file_exists('storage/app/'.$image->images)){
                    // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                    // Storage::copy($image->images, $newImage);

                    Image::insertGetId(['images' => $image->images, 'goods' => $newProductID, 'siteID' => $id, 'created_at' => date('Y-m-d H:i:s')]);
                  }

                }
            }
          }

          if($request->services !== null && $category->type == 1){
            foreach ($category->goodsAll as $goods) {
              if($goods->parent_id == null){
              $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> $id, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                'counts' => $goods->counts, 'status' => $goods->status, 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'protected' => $goods->protected, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                $oldProductID[$goods->id] = $newProductID;
              }
              else{
                $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> $id, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                  'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                  'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                  'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                  'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                  'counts' => $goods->counts, 'status' => $goods->status, 'parent_id' => $oldProductID[$goods->parent_id], 'min_count' => $goods->min_count, 'protected' => $goods->protected, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                  $oldProductID[$goods->id] = $newProductID;
              }

                foreach ($goods->imageses as $image) {
                  if($image->images !== null && file_exists('storage/app/'.$image->images)){
                    // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                    // Storage::copy($image->images, $newImage);

                    Image::insertGetId(['images' => $image->images, 'goods' => $newProductID, 'siteID' => $id, 'created_at' => date('Y-m-d H:i:s')]);
                  }

                }
            }
          }

          if($request->izgotovleniye !== null && $category->type == 7){
            foreach ($category->goodsAll as $goods) {
              if($goods->parent_id == null){
              $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> $id, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                'counts' => $goods->counts, 'status' => $goods->status, 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'protected' => $goods->protected, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                $oldProductID[$goods->id] = $newProductID;
              }
              else{
                $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> $id, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                  'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                  'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                  'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                  'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                  'counts' => $goods->counts, 'status' => $goods->status, 'parent_id' => $oldProductID[$goods->parent_id], 'min_count' => $goods->min_count, 'protected' => $goods->protected, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                  $oldProductID[$goods->id] = $newProductID;
              }

                foreach ($goods->imageses as $image) {
                  if($image->images !== null && file_exists('storage/app/'.$image->images)){
                    // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                    // Storage::copy($image->images, $newImage);

                    Image::insertGetId(['images' => $image->images, 'goods' => $newProductID, 'siteID' => $id, 'created_at' => date('Y-m-d H:i:s')]);
                  }

                }
            }
          }

          if($request->stroitelstvo !== null && $category->type == 4){
            foreach ($category->goodsAll as $goods) {
              if($goods->parent_id == null){
              $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> $id, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                'counts' => $goods->counts, 'status' => $goods->status, 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'protected' => $goods->protected, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                $oldProductID[$goods->id] = $newProductID;
              }
              else{
                $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> $id, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                  'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                  'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                  'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                  'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                  'counts' => $goods->counts, 'status' => $goods->status, 'parent_id' => $oldProductID[$goods->parent_id], 'min_count' => $goods->min_count, 'protected' => $goods->protected, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                  $oldProductID[$goods->id] = $newProductID;
              }

                foreach ($goods->imageses as $image) {
                  if($image->images !== null && file_exists('storage/app/'.$image->images)){
                    // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                    // Storage::copy($image->images, $newImage);

                    Image::insertGetId(['images' => $image->images, 'goods' => $newProductID, 'siteID' => $id, 'created_at' => date('Y-m-d H:i:s')]);
                  }

                }
            }
          }
          if($request->bani !== null && $category->type == 5){
            foreach ($category->goodsAll as $goods) {
              if($goods->parent_id == null){
              $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> $id, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                'counts' => $goods->counts, 'status' => $goods->status, 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'protected' => $goods->protected, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                $oldProductID[$goods->id] = $newProductID;
              }
              else{
                $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> $id, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                  'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                  'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                  'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                  'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                  'counts' => $goods->counts, 'status' => $goods->status, 'parent_id' => $oldProductID[$goods->parent_id], 'min_count' => $goods->min_count, 'protected' => $goods->protected, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                  $oldProductID[$goods->id] = $newProductID;
              }

                foreach ($goods->imageses as $image) {
                  if($image->images !== null && file_exists('storage/app/'.$image->images)){
                    // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                    // Storage::copy($image->images, $newImage);

                    Image::insertGetId(['images' => $image->images, 'goods' => $newProductID, 'siteID' => $id, 'created_at' => date('Y-m-d H:i:s')]);
                  }

                }
            }
          }
          if($request->proyekty !== null && $category->type == 6){
            foreach ($category->goodsAll as $goods) {
              if($goods->parent_id == null){
              $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> $id, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                'counts' => $goods->counts, 'status' => $goods->status, 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'protected' => $goods->protected, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                $oldProductID[$goods->id] = $newProductID;
              }
              else{
                $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> $id, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                  'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                  'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                  'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                  'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                  'counts' => $goods->counts, 'status' => $goods->status, 'parent_id' => $oldProductID[$goods->parent_id], 'min_count' => $goods->min_count, 'protected' => $goods->protected, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                  $oldProductID[$goods->id] = $newProductID;
              }

                foreach ($goods->imageses as $image) {
                  if($image->images !== null && file_exists('storage/app/'.$image->images)){
                    // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                    // Storage::copy($image->images, $newImage);

                    Image::insertGetId(['images' => $image->images, 'goods' => $newProductID, 'siteID' => $id, 'created_at' => date('Y-m-d H:i:s')]);
                  }

                }
            }
          }


          if($request->objects !== null){

            foreach ($category->objects as $object) {
            $objectID = Objects::insertGetId(['siteID' => $id, 'parent_id' => $newCategoryID, 'title' => $object->title, 'offer' => $object->offer, 'text' => $object->text, 'popular' => $object->popular, 'published' => $object->published,
            'type' => $object->type, 'main' => $object->main, 'slug' => $object->slug, 'created_at' => date('Y-m-d H:i:s')]);

            foreach ($object->images as $image) {
              if($image->images !== null && file_exists('storage/app/'.$image->images)){
                // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                // Storage::copy($image->images, $newImage);

                Image::insertGetId(['images' => $image->images, 'object' => $objectID, 'siteID' => $id, 'created_at' => date('Y-m-d H:i:s')]);
              }

            }
            }
          }


        }

        if($request->text !== null){
          self::beckupTexts($id);
          $newTexts = Text::with('images')->where('siteID', Auth::user()->siteID)->get();
          foreach ($newTexts as $text) {
            Text::where('siteID', $id)->where('type', $text->type)->update(['seoTitle' => $text->seoTitle, 'seoDescription' => $text->seoDescription, 'keywords' => $text->keywords, 'title' => $text->title, 'text' => $text->text, 'checkseo' => $text->checkseo, 'created_at' => date('Y-m-d H:i:s')]);
            $textID = Text::where('siteID', $id)->where('type', $text->type)->value('id');
            foreach ($text->images as $image) {
              if($image->images !== null && file_exists('storage/app/'.$image->images)){
                //$newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                //Storage::copy($image->images, $newImage);

                Image::insertGetId(['images' => $image->images, 'text' => $textID, 'siteID' => $id, 'created_at' => date('Y-m-d H:i:s')]);
              }

            }
          }
        }

        if($request->news){

          self::beckupNews($id);
          if($request->clear !== null){
            News::where('siteID', $id)->delete();
          }
          $newNews = News::where('siteID', Auth::user()->siteID)->get();

          foreach ($newNews as $news) {
            if(file_exists('storage/app/'.$news->image) && $news->image !== null){
              //$newImage = 'public/'.uniqid().'.'.explode('.',$news->image)[1];
              //Storage::copy($news->image, $newImage);
            }
            else{
              $newImage = null;
            }

            News::insertGetId(['siteID'=> $id, 'title' => $news->title, 'text' => $news->text, 'description' => $news->description, 'published' => $news->published, 'image' => $news->image, 'created_at' => date('Y-m-d H:i:s')]);
          }
        }

        if($request->banners !== null){
          self::beckupBanners($id);
          if($request->clear !== null){
            Banner::where('siteID', $id)->delete();
          }
          $newBanners = Banner::where('siteID', Auth::user()->siteID)->get();
          foreach ($newBanners as $banenr) {
            if(file_exists('storage/app/'.$banenr->image) && $banenr->image !== null){
              //$newImage = 'public/'.uniqid().'.'.explode('.',$banenr->image)[1];
              //Storage::copy($banenr->image, $newImage);
            }
            else{
              $newImage = null;
            }

            Banner::insertGetId(['siteID'=> $id, 'title' => $banenr->title, 'text' => $banenr->text, 'type' => $banenr->type, 'published' => $banenr->published, 'image' => $banenr->image, 'color' => $banenr->color, 'main' => $banenr->main, 'status' => $banenr->status, 'created_at' => date('Y-m-d H:i:s')]);
          }
        }
        if($request->tizers !== null){
          self::beckupTizers($id);
          if($request->clear !== null){
            Tizer::where('siteID', $id)->delete();
          }
          $newTizers = Tizer::where('siteID', Auth::user()->siteID)->get();
          foreach ($newTizers as $tizer) {
            if(file_exists('storage/app/'.$tizer->image)  && $tizer->image !== null){
              //$newImage = 'public/'.uniqid().'.'.explode('.',$tizer->image)[1];
              //Storage::copy($tizer->image, $newImage);
            }
            else{
              $newImage = null;
            }

            Tizer::insertGetId(['siteID'=> $id, 'title' => $tizer->title, 'description' => $tizer->description, 'image' => $tizer->image,'status' => $tizer->status, 'created_at' => date('Y-m-d H:i:s')]);
          }
        }
        if($request->tels !== null){
          self::beckupTels($id);
          if($request->clear !== null){
            Tel::where('siteID', $id)->delete();
          }
          $newTels = Tel::where('siteID', Auth::user()->siteID)->get();
          foreach ($newTels as $tel) {
            Tel::insertGetId(['siteID'=> $id, 'title' => $tel->title, 'tel' => $tel->tel, 'created_at' => date('Y-m-d H:i:s')]);
          }
        }
        if($request->emails !== null){
          self::beckupEmails($id);
          if($request->clear !== null){
            Email::where('siteID', $id)->delete();
          }
          $newEmails = Email::where('siteID', Auth::user()->siteID)->get();
          foreach ($newEmails as $email) {
            Email::insertGetId(['siteID'=> $id, 'title' => $email->title, 'email' => $email->email, 'created_at' => date('Y-m-d H:i:s')]);
          }
        }
        if($request->information !== null){
          self::beckupInfo($id);
          if($request->clear !== null){
            Information::where('siteID', $id)->delete();
          }
          $information = Information::where('siteID', Auth::user()->siteID)->first();
          if(file_exists('storage/app/'.$information->logo1)  && $information->logo1 !== null){
            $logo1 = 'public/'.uniqid().'.'.explode('.',$information->logo1)[1];
            Storage::copy($information->logo1, $logo1);
          }
          else{
            $logo1 = null;
          }

          if(file_exists('storage/app/'.$information->logo2)  && $information->logo2 !== null){
            $logo2 = 'public/'.uniqid().'.'.explode('.',$information->logo2)[1];
            Storage::copy($information->logo2, $logo2);
          }
          else{
            $logo2 = null;
          }

          if(file_exists('storage/app/'.$information->favicon)  && $information->favicon !== null){
            $favicon = 'public/'.uniqid().'.'.explode('.',$information->favicon)[1];
            Storage::copy($information->favicon, $favicon);
          }
          else{
            $favicon = null;
          }
          Information::insert(['siteID' => $id, 'timeJob' => $information->timeJob, 'address' => $information->address, 'map' => $information->map, 'metrika' => $information->metrika, 'verYa' => $information->verYa,
          'verGo' => $information->verGo, 'analytics' => $information->analytics, 'city' => $information->city, 'logo1' => $logo1, 'logo2' => $logo2, 'favicon' => $favicon, 'color_menu' => $information->color_menu,
          'image_menu' => $information->image_menu, 'color_text_menu' => $information->color_text_menu, 'full_name' => $information->full_name, 'short_name' => $information->short_name, 'legal_address' => $information->legal_address, 'actual_address' => $information->actual_address,
          'general_director' => $information->general_director, 'INN' => $information->INN, 'KPP' => $information->KPP, 'OGRN' => $information->OGRN, 'OKPO' => $information->OKPO, 'OKBED' => $information->OKBED, 'OKATO' => $information->OKATO, 'Bank' => $information->Bank,
          'Acount_chek' => $information->Acount_chek, 'Corp_check' => $information->Corp_check, 'BIK' => $information->BIK]);

        }

      }

     return redirect()->back();
    //dd($request->title);
  }


  public function addUser(request $request){
    $contact = Users::create(['name' => $request->name,
                              'email' => $request->email,
                              'password' => bcrypt($request->password),
                              'userRolle' => $request->userRolle,
                              'siteID' => $request->site]);
    return redirect()->route('admin.banner.index');
  }


  public function gitpull(Request $request) {
        $branch = 'master';
        if (empty($branch)) {
            $branch = 'master';
        }
        if($request->getHttpHost() !== 'pasha.ru'){
          $hard = 'git reset --hard &&';
        }
        else{
          $hard = '';
        }

        $output = "";
        $return = "";


        exec("".$hard." git pull https://ARX_2:31415262738300dd@gitlab.com/ARX_2/project-x.git ", $output, $return);

        $status = ($return === 0) ? "OK" : "Ошибка";
        return '<strong>'.$status.'</strong> ('.$branch.')<hr><code><pre>'.implode("\n", $output).'</pre></code>';
    }

    public function gitpush(Request $request) {
        //dd('asd');asd
        //return $request->commit;
        //$commit = 'git commit -m "'.$request->commit.'"';
        //dd($commit);
        $branch = 'master';

        $output = "";
        $return = "";


        exec('git add --all && git commit -m "'.$request->commit.'" && git push https://ARX_2:31415262738300dd@gitlab.com/ARX_2/project-x.git ', $output, $return);

        $status = ($return === 0) ? "OK" : "Ошибка";
        return '<strong>'.$status.'</strong> ('.$branch.')<hr><code><pre>'.implode("\n", $output).'</pre></code>';
    }

    public function template(Request $request){

        foreach ($request['template'] as $key => $value) {
          $template =  Template::where('siteID', Auth::user()->siteID)->where('template', $key)->first();
          if($template == null){
          Template::create(['template'=> $key, 'templatename' => $value[0], 'siteID' => Auth::user()->siteID]);
          }
          else{

              Template::where('siteID', Auth::user()->siteID)->where('template', $key)->update(['templatename' => $value[0]]);
        }
      }
      return redirect()->route('admin.index');

    }


    public function index(Request $request){


      $companies = Site::with('info')->orderby('monthly', 'DESC ')->get();
      $favorites = Usite::with('sites')->where('user', Auth::user()->id)->get();

      $countfavorites = $favorites->count();
      $countMebel = Site::where('type', 3)->count();
      $countBuilding = Site::where('type', 1)->count();
        $countSes = Site::where('type', 2)->count();
        $countPamyatkik = Site::where('type', 4)->count();
      $countOther = Site::where('type', 0)->count();

      return view('admin.admin.management.index',[
        'companies' => $companies,
        'company' => Resize::company(),
        'countMebel' => $countMebel,
        'countBuilding' => $countBuilding,
        'countOther' => $countOther,
        'favorites' => $favorites,
        'countSes'=>$countSes,
        'countPamyatkik'=>$countPamyatkik,
        'countfavorites' => $countfavorites,
        'sites' => $companies,
      ]);
    }
    public function about(Request $request){

      //dd($request->id);

      $company = Site::with('about')->where('id', $request->siteID)->first();
      //dd($company);
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.admin.management.template.about',[
        'company' => $company,
        'siteID' => $request->siteID,
      ]);
    }

    public function companyTemplate(Request $request){
        $template = Template::where('siteID', $request->siteID)->where('template', '5')->first();
        if($template !== null){
          Template::where('siteID', $request->siteID)->where('template', '5')->update(['templatename' => 'chief_'.$request->company]);
        }
        else{
          Template::create(['templatename' => 'chief_'.$request->company, 'template' => 5, 'siteID' => $request->siteID]);
        }
        return redirect()->route('admin.admin.about', ['siteID' => $request->siteID]);
    }

    public function information(Request $request){
      //dd($request);
      $company = Site::with('about')->where('id', $request->siteID)->first();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.admin.management.settings.information',[
        'company' => $company,
        'siteID' => $request->siteID,
      ]);

    }
    public function informationMain(Request $request){
      Site::where('id', $request->siteID)->update(['name' => $request->name, 'description' => $request->description, 'title' => $request->title]);
      return redirect()->route('admin.admin.information', ['siteID' => $request->siteID]);
    }

    public function informationSubmain(Request $request){
        Site::where('id', $request->siteID)->update(['subject' => $request->subject, 'tel' => $request->tel, 'country' => $request->country, 'city' => $request->city, 'address' => $request->address]);
        return redirect()->route('admin.admin.information', ['siteID' => $request->siteID]);
    }
    public function sections(Request $request){
      $company = Site::with('about')->where('id', $request->siteID)->first();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.admin.management.settings.sections',[
        'company' => $company,
        'siteID' => $request->siteID,
      ]);
    }


    public function home(Request $request){
      $company = Site::with('about')->where('id', $request->siteID)->first();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.admin.management.template.home',[
        'company' => $company,
        'siteID' => $request->siteID,
      ]);
    }
    public function menu(Request $request){
      $company = Site::with('about')->where('id', $request->siteID)->first();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.admin.management.template.menu',[
        'company' => $company,
        'siteID' => $request->siteID,
      ]);
    }

    public function category(Request $request){
      //dd($request);
      $company = Site::with('about')->where('id', $request->siteID)->first();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.admin.management.template.category',[
        'company' => $company,
        'siteID' => $request->siteID,
      ]);
    }
    public function categoryTemplate(Request $request){
      //dd($request);
      $template = Template::where('siteID', $request->siteID)->where('template', 5)->first();
      if($template !== null){
        Template::where('siteID', $request->siteID)->where('template', 5)->update(['templatename' => 'chief_'.$request->category]);
      }
      else{
        Template::create(['templatename' => 'chief_'.$request->category, 'template' => 5, 'siteID' => $request->siteID]);
      }
      return redirect()->route('admin.admin.category', ['siteID' => $request->siteID]);
    }
    public function product(Request $request){
      $company = Site::with('about')->where('id', $request->siteID)->first();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.admin.management.template.product',[
        'company' => $company,
        'siteID' => $request->siteID,
      ]);
    }
    public function productTemplate(Request $request){
      //dd($request);
      $template2 = Template::where('siteID', $request->siteID)->where('template', '2')->get();
      if($template2 !== null){
        Template::where('siteID', $request->siteID)->where('template', '2')->update(['templatename' => 'chief_'.$request->products]);
      }
      else{
        Template::create(['templatename' => 'chief_'.$request->products, 'template' => 2, 'siteID' => $request->siteID]);
      }
      $template3 = Template::where('siteID', $request->siteID)->where('template', '3')->get();
      if($template3 !== null){
        Template::where('siteID', $request->siteID)->where('template', '3')->update(['templatename' => 'chief_'.$request->services]);
      }
      else{
        Template::create(['templatename' => 'chief_'.$request->services, 'template' => 3, 'siteID' => $request->siteID]);
      }
      return redirect()->route('admin.admin.product', ['siteID' => $request->siteID]);
    }

    public function object(Request $request){
      $company = Site::with('about')->where('id', $request->siteID)->first();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.admin.management.template.object',[
        'company' => $company,
        'siteID' => $request->siteID,
      ]);
    }
    public function objectTemplate(Request $request){
      $template = Template::where('siteID', $request->siteID)->where('template', '4')->first();
      if($template !== null){
        Template::where('siteID', $request->siteID)->where('template', '4')->update(['templatename' => 'chief_'.$request->object]);
      }
      else{
        Template::create(['templatename' => 'chief_'.$request->object, 'template' => 4, 'siteID' => $request->siteID]);
      }
      return redirect()->route('admin.admin.object', ['siteID' => $request->siteID]);
    }
    public function contact(Request $request){
      $company = Site::with('about')->where('id', $request->siteID)->first();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.admin.management.template.contact',[
        'company' => $company,
        'siteID' => $request->siteID,
      ]);
    }
    public function contactTemplate(Request $request){
      $template = Template::where('siteID', $request->siteID)->where('template', 6)->first();
      if($template !== null){
        Template::where('siteID', $request->siteID)->where('template', 6)->update(['templatename' => 'chief_'.$request->contact]);
      }
      else{
        Template::create(['templatename' => 'chief_'.$request->contact, 'template' => 6, 'siteID' => $request->siteID]);
      }
      return redirect()->route('admin.admin.contact', ['siteID' => $request->siteID]);
    }
    public function header(Request $request){
      $company = Site::with('about')->where('id', $request->siteID)->first();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.admin.management.template.header',[
        'company' => $company,
        'siteID' => $request->siteID,
      ]);
    }

    public function footer(Request $request){
      $company = Site::with('about')->where('id', $request->siteID)->first();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.admin.management.template.footer',[
        'company' => $company,
        'siteID' => $request->siteID,
      ]);
    }

    public function user(Request $request){
      $company = Site::with('about')->where('id', $request->siteID)->first();
      $users = Users::where('siteID', $request->siteID)->get();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.admin.management.user.index',[
        'company' => $company,
        'siteID' => $request->siteID,
        'users'=>$users,
      ]);
    }


    public function productionCategory(Request $request){
      //dd($request);
      $company = Site::with('about')->where('id', $request->siteID)->first();
      if($request->type == 1){
        $categories = Category::with('images')->with('goods')->where('siteId', $request->siteID)->where('deleted', 1)->get();
      }
      else{
        $categories = Category::with('images')->with('goods')->where('siteId', $request->siteID)->where('deleted', 0)->get();
      }
      //dd($categories);
      return view('admin.admin.management.production.category',[
        'company' => $company,
        'categories' => $categories,
        'siteID' => $request->siteID,
      ]);
    }

    public function cedit(Request $request){
      $category = Category::with('images')->where('id', $request->id)->first();
      $company = Site::with('about')->where('id', $request->siteID)->first();
      return view('admin.admin.management.production.c-edit',[
        'company' => $company,
        'category' => $category,
        'siteID' => $request->siteID,
      ]);
    }

    public function updatecedit(Request $request){

      //dd($request->file('file')->getRealPath());
      Category::where('id', $request->id)->update(['title' => $request->title,
      'description' => $request->text, 'type' => $request->type, 'seoname' => $request->seoname,
      'seotitle' => $request->seotitle, 'seodescription' => $request->seodescription, 'seokeywords' => $request->seokeywords,
      'youtube' => $request->youtube, 'published' => 1
      ]);
      if($request->file('file') !== null){
      $image = Image::where('category', $request->id)->first();
      $img = $request->file('file');

      $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
      ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
      if($image !== null){
        Image::where('category', $request->id)->update(['images' => 'public/'.$filename]);
        self::imageDestroy($image->images);
      }
      else{
        Image::create(['images' => 'public/'.$filename, 'category' => $request->id]);
      }
      }
      //dd($image);
      return redirect()->route('admin.production.c-edit', ['id' => $request->id, 'siteID' => $request->siteID]);
    }

    static function imageDestroy($image){
      Image::with('categories')->where('images', $image)->delete();
      Storage::delete($image);

      $images = DB::table('image')->where('image', $image)->get();
      //dd($images);
      foreach ($images as $i) {
        DB::table('image')->where('image', $image)->delete();
        Storage::delete($i->fullimagesize);
      }
    }

    public function delcedit(Request $request){
      self::imageDestroy($request->image, 'category');
      if($request->type == 0){
        $type = 'c-edit';
      }
      else{
        $type = 'p-edit';
      }
      return redirect()->route('admin.production.'.$type, ['id' => $request->id, 'siteID' => $request->siteID]);
    }
    public function createcedit(Request $request){
      Category::create(['title' => $request->title,
      'description' => $request->text, 'type' => $request->type, 'seoname' => $request->seoname,
      'seotitle' => $request->seotitle, 'seodescription' => $request->seodescription, 'seokeywords' => $request->seokeywords,
      'youtube' => $request->youtube, 'siteID' => $request->siteID, 'published' => 1, 'deleted' => 0,
      ]);
      $id = Category::max('id');
      if($request->file('file') !== null){
      $image = Image::where('category', $request->id)->first();
      $img = $request->file('file');

      $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
      ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
        Image::create(['images' => 'public/'.$filename, 'category' => $id]);

      }
      //dd($image);
      return redirect()->route('admin.production.c-edit', ['id' => $id, 'siteID' => $request->siteID]);
    }

    public function cdelete(Request $request){

      $deleted = Category::where('id', $request->id)->value('deleted');
      if($deleted == 0){
        Category::where('id', $request->id)->update(['deleted' => 1]);
      }
      else{
        $image = Image::where('category', $request->id)->value('images');
        self::imageDestroy($image);
        Category::where('id', $request->id)->delete();
      }

      return redirect()->route('admin.production.category', ['siteID' => $request->siteID]);
    }
    public function categoryReturn(Request $request){
      Category::where('id', $request->id)->update(['deleted' => 0]);
      return redirect()->route('admin.production.category', ['siteID' => $request->siteID]);
    }
    public function goods(Request $request){
      $company = Site::with('about')->where('id', $request->siteID)->first();
      //$categories = Category::where('siteId', $request->siteID)->get();

      if($request->type == 1){
        $product = Goods::with('images')->with('categories')->where('siteID', $request->siteID)->where('deleted', 1)->get();
      }
      else{
        $product = Goods::with('images')->with('categories')->where('siteID', $request->siteID)->where('deleted', 0)->get();
      }
      //dd($product);
      //dd($product[0]->images);
      //dd($categories);
      return view('admin.admin.management.production.products',[
        'company' => $company,
        'product' => $product,
        'siteID' => $request->siteID,
      ]);
    }

    public function pedit(Request $request){
      $category = Category::where('siteID', $request->siteID)->get();
      $product = Goods::with('categories')->with('properties')->where('id', $request->id)->first();
      $company = Site::with('about')->where('id', $request->siteID)->first();
      //dd($product);
      return view('admin.admin.management.production.p-edit',[
        'company' => $company,
        'categories' => $category,
        'product' =>  $product,
        'siteID' => $request->siteID,
      ]);
    }
    public function updatepedit(Request $request){
      //dd($request);
      //dd($request);

      Goods::with('categories')->where('id', $request->id)->update([
        'title' => $request->title, 'short_description' => $request->short_description,
        'coast' => $request->coast, 'youtube' => $request->youtube, 'seoname' => $request->seoname,
        'seotitle' => $request->seotitle, 'seodescription' => $request->seodescription, 'seokeywords' => $request->seokeywords
      ]);
      if($request->file('file') !== null){
      $image = Image::where('goods', $request->id)->first();
      $img = $request->file('file');

      $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
      ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
      if($image !== null){
        Image::where('goods', $request->id)->update(['images' => 'public/'.$filename]);
        self::imageDestroy($image->images);
      }
      else{
        Image::create(['images' => 'public/'.$filename, 'goods' => $request->id]);
      }
      }
                                    //
                                    // ХАРАКТЕРИСТИКИ
                                    //
      if(isset($request->Property)){
      $i = 0;
      foreach ($request->Property['title'] as $p) {

        if($request->Property['title'][$i] !== null || $request->Property['type'][$i] !== null || $request->Property['name'][$i]){

        Property::insert(['title' => $p, 'type' => $request->Property['type'][$i], 'name' => $request->Property['name'][$i]]);
        //dd($p);
       $propertyid = DB::table('properties')->max('id');
        DB::table('goods_property')->insert(['goods' => $request->id, 'property' => $propertyid]);
        }
        $i++;
      }
    }
    $j=0;
    if(isset($request->UpdateProperty)){
    foreach ($request->UpdateProperty as $up => $key) {
      if($key[0] !== null||$key[1] !== null){
        Property::where('id', $up)->update(['name' => $key[0], 'title' => $key[1], 'type' => $key[2]]);
      }
      else{
        Property::where('id', $up)->delete();
        DB::table('goods_property')->where('id', $up)->delete();
      }
      }

    }
    DB::table('category_goods')->where('goods', $request->id)->update(['category'=> $request->categories]);

      return redirect()->route('admin.production.p-edit', ['siteID' => $request->siteID, 'id' => $request->id ]);

    }


    public function createpedit(Request $request){
      Goods::create([
        'title' => $request->title, 'short_description' => $request->short_description,
        'coast' => $request->coast, 'youtube' => $request->youtube, 'seoname' => $request->seoname,
        'seotitle' => $request->seotitle, 'seodescription' => $request->seodescription, 'seokeywords' => $request->seokeywords,
        'siteID' => $request->siteID, 'deleted' => 0
      ]);
      $id = Goods::max('id');

      if($request->file('file') !== null){
      $img = $request->file('file');
      $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
      ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
        Image::create(['images' => 'public/'.$filename, 'goods' => $id]);
      }
      if(isset($request->Property)){
      $i = 0;
      foreach ($request->Property['title'] as $p) {
        if($request->$p !== null || $request->Property['type'][$i] !== null || $request->Property['name'][$i]){
        Property::insert(['title' => $p, 'type' => $request->Property['type'][$i], 'name' => $request->Property['name'][$i]]);
       $propertyid = DB::table('properties')->max('id');
        DB::table('goods_property')->insert(['goods' => $id, 'property' => $propertyid]);
        }
        $i++;
      }
    }
    DB::table('category_goods')->insert(['category'=> $request->categories, 'goods'=> $id]);
      return redirect()->route('admin.production.p-edit', ['siteID' => $request->siteID, 'id' => $id ]);
    }


    public function pdelete(Request $request){

      $type = Goods::where('id', $request->id)->value('deleted');
      //dd($type);
      if($type == 0){
        Goods::where('id', $request->id)->update(['deleted'=> 1]);
      }
      else{
        $image = Image::where('goods', $request->id)->value('images');
        self::imageDestroy($image);
        Goods::where('id', $request->id)->delete();
        DB::table('category_goods')->where('goods', $request->id)->delete();

      }
      return redirect()->route('admin.production.goods', ['siteID' => $request->siteID]);
    }

    public function productReturn(Request $request){
      Goods::where('id', $request->id)->update(['deleted' => 0]);
      return redirect()->route('admin.production.goods', ['siteID' => $request->siteID]);
    }
    public function support(Request $request){
      return view('admin.admin.management.support.index',[
        'requests' => Support::with('comments')->paginate(10),
        'company' => Site::with('about')->where('id', $request->siteID)->first(),

      ]);
    }

    public function chooseSite(Request $request){
      Users::where('id', Auth::user()->id)->update(['siteID'=> $request->siteID]);
      return redirect()->route('admin.company.index');
    }
    public function uploadExcel(Request $request){
      //dd($request->file('file'));
      //dd($request);
      $book = Excel::load($request->file('file'));
      $sheet = $book->getSheet(0);

      $highestRow = $sheet->getHighestRow();
      //dd($sheet);
      $category = array();
      $goods = array();
      //dd($category);
      //Добавление
      if($request->type == 1){
      for ($i = 1; $i < $highestRow; $i++) {

          $value = $book->getSheet(0)->getCellByColumnAndRow(0, $i)->getValue();
          if($value !== null){
            $value = (int)$value;
            $category['id'] = $value;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(1, $i)->getValue();
          if($value !== null){
            $category['title'] = $value;
            Category::insert(['id'=> $category['id'],
            'title' => $category['title'],
            'slug' => str_slug($category['title'], "-"),
            'siteID' => Auth::user()->siteID,
            'published' => 1,
            'deleted' => 0]);
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(2, $i)->getValue();
          if($value !== null){
            $value = (int)$value;
            $category['subcategory']['id'] = $value;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(3, $i)->getValue();
          if($value !== null){
            $category['subcategory']['title'] = $value;
               Subcategory::insert(['id'=>   $category['subcategory']['id'], 'title' => $category['subcategory']['title'],
               'category' => $category['id'], 'published' => 1, 'slug' => str_slug($category['subcategory']['title'], "-")]);
          }

          $value = $book->getSheet(0)->getCellByColumnAndRow(4, $i)->getValue();
          if($value !== null){
            $value = (int)$value;
            //dd($idgroup);
            if(isset($idgroup)){
              if($idgroup == $value){
                  $category['subcategory']['goods']['id'] = $i;
                  $category['subcategory']['goods']['parent_id'] = $idgroup;
                }
                else{
                  $category['subcategory']['goods']['id'] = $i;
                  $idgroup = $value;
                  $category['subcategory']['goods']['parent_id'] = null;
                }
            }
            else{
              $category['subcategory']['goods']['id'] = $i;
              $idgroup = $value;
              $category['subcategory']['goods']['parent_id'] = null;
            }


          }
          else{
            $category['subcategory']['goods']['id'] = null;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(5, $i)->getValue();
          if($value !== null){
            $category['subcategory']['goods']['articulate'] = $value;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(6, $i)->getValue();
          if($value !== null){
            $category['subcategory']['goods']['title'] = $value;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(7, $i)->getValue();
          if($value !== null){
            $category['subcategory']['goods']['description'] = $value;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(8, $i)->getValue();
          $value = (int)$value;
          if($value !== null){
            $category['subcategory']['goods']['color'] = $value;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(9, $i)->getValue();
          if($value !== null){
            $value = (int)$value;
            $category['subcategory']['goods']['coast'] = $value;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(10, $i)->getValue();
          if($value !== null){
            $value = (int)$value;
            $category['subcategory']['goods']['counts'] = $value;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(11, $i)->getValue();
          if($value !== null){
            $category['subcategory']['goods']['image'] = $value;
          }
          if($category['subcategory']['goods']['id'] !== null){
            //dd($category);
            Goods::insert(['id' => $category['subcategory']['goods']['id'],
            'parent_id' => $category['subcategory']['goods']['parent_id'],
            'title' => $category['subcategory']['goods']['title'],
            'slug' => str_slug($category['subcategory']['goods']['title'], "-"),
            'short_description' => $category['subcategory']['goods']['description'],
            'color' => $category['subcategory']['goods']['color'],
            'coast' => $category['subcategory']['goods']['coast'],
            'articulate' => $category['subcategory']['goods']['articulate'],
            'counts' => $category['subcategory']['goods']['counts'], 'siteID' => Auth::user()->siteID, 'deleted' => 0, 'published' => 1,
            'status' => 1]);
            DB::table('category_goods')->insert(['category'=> $category['subcategory']['id'], 'goods'=> $category['subcategory']['goods']['id']]);
            Image::insert(['goods' => $category['subcategory']['goods']['id'], 'images' => 'public/'.$category['subcategory']['goods']['image'], 'siteID' => Auth::user()->siteID]);
          }

      }
    }
    else{

      for ($i = 1; $i < $highestRow; $i++) {
        $goods['update'] = '';
          $value = $book->getSheet(0)->getCellByColumnAndRow(5, $i)->getValue();
          if($value !== null){
            $goods['articulate'] = $value;
          }
          else{
            $goods['articulate'] = null;
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(6, $i)->getValue();
          if($value !== null && isset($request->update['title'])){
            Goods::where('articulate', $goods['articulate'])->update(['title' => $value]);
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(7, $i)->getValue();
          if($value !== null && isset($request->update['description'])){
            Goods::where('articulate', $goods['articulate'])->update(['short_description' => $value]);
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(8, $i)->getValue();
          $value = (int)$value;
          if($value !== null && isset($request->update['color'])){
            Goods::where('articulate', $goods['articulate'])->update(['color' => $value]);
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(9, $i)->getValue();
          if($value !== null && isset($request->update['coast'])){
            $value = (int)$value;
            Goods::where('articulate', $goods['articulate'])->update(['coast' => $value]);
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(10, $i)->getValue();
          if($value !== null && isset($request->update['counts'])){
            $value = (int)$value;
            Goods::where('articulate', $goods['articulate'])->update(['images' => $value]);
          }
          $value = $book->getSheet(0)->getCellByColumnAndRow(11, $i)->getValue();
          if($value !== null && isset($request->update['image'])){
            $id = Goods::where('articulate', $goods['articulate'])->value('id');
            Image::where('goods', $id)->update(['images' => $value]);
          }
      }
    }
      return redirect()->route('admin.goods.index');

    }

    public function excelExport(){
      $goods = Category::with('subcategories')->get();
      $excelName = 'Excel-Export-'.date('d.m.Y');
      //dd($goods);
      Excel::create($excelName, function($excel) use($goods) {
                    $excel->sheet('reestr', function($sheet) use($goods) {
                        $titleExcel = array(
                            array('Категория',
                                'Название категории',
                                'Подкатегория',
                                'Название подкатегории',
                                'Группа товаров',
                                'Штрих-код',
                                'Название товара',
                                'Характеристика товара',
                                'Цвет',
                                'Цена',
                                'Остаток',
                                'Картинка',
                            ),
                            array(),
                            array(),
                        );
                        $categoryArray = array();
                        $sabcategoryArray = array();

                        foreach ($goods as $category) {

                            foreach ($category->subcategories as $subcategory) {
                              foreach ($subcategory->goods as $product) {
                                if(isset($categoryArray['id'])){
                                if($categoryArray['id'] == $category->id){
                                  $categoryId = null;
                                  $categoryTitle = null;
                                }
                                else{
                                  $categoryId = $category->id;
                                  $categoryTitle = $category->title;
                                  $categoryArray = ['id' => $category->id];
                                }
                                }
                                else{
                                  $categoryId = $category->id;
                                  $categoryTitle = $category->title;
                                  $categoryArray = ['id' => $category->id];
                                }

                                if(isset($sabcategoryArray['id'])){
                                if($sabcategoryArray['id'] == $subcategory->id){
                                  $subcategoryId = null;
                                  $subcategoryTitle = null;
                                }
                                else{
                                  $subcategoryId = $subcategory->id;
                                  $subcategoryTitle = $subcategory->title;
                                  $sabcategoryArray = ['id' => $subcategory->id];
                                }
                                }
                                else{
                                  $subcategoryId = $subcategory->id;
                                  $subcategoryTitle = $subcategory->title;
                                  $sabcategoryArray = ['id' => $subcategory->id];
                                }
                                if($product->parent_id == null){
                                  $parent_id = $product->id;
                                }
                                else{
                                  $parent_id = $product->parent_id;
                                }
                                if($product->images !== null){
                                  $image = explode('/',$product->images->images)[1];
                                }
                                else{
                                  $image = null;

                                }
                                $titleExcel[] = [$categoryId, $categoryTitle, $subcategory->id, $subcategoryTitle, $parent_id, $product->articulate, $product->title, $product->short_description, $product->color, $product->coast, $product->counts, $image];
                                //dd($titleExcel);
                              }
                            }

                        }
                        //dd($titleExcel);

                        $sheet->fromArray($titleExcel, null, 'A1', true, false);
                        $sheet->setColumnFormat(array(
                            'C' => '@'
                        ));
                    });
                })->store("xlsx", storage_path('app/excel'));
                //dd(Storage::path('excel\\'.$excelName));
                return response('<a href="\\storage\\app\\excel\\'.$excelName.'.xlsx" style="margin-right:20px;color:rgb(87, 177, 228);">Выгрузка '.$excelName.'</a>');
                //echo '<a href="\\storage\\app\\excel\\'.$excelName.'.xlsx">'.$excelName.'</a>';
                //echo "<script type='text/javascript'>window.open('" . config('app.url') . "/storage/app/excel/".$excelName.".xlsx');</script>";

    }

    static function uploadCache(){
      $categories = Category::with('subcategoriesAll')->where('siteID', Auth::user()->siteID)->with('images')->get();
      foreach ($categories as $cat) {
        Caching::deleteCategory($cat->id);
        foreach ($cat->subcategoriesAll as $sub) {
          Caching::deleteSubcategory($sub->id);
          Caching::deleteproductAnother($sub->id);
          foreach ($sub->goods as $goods) {
            Caching::deleteProduct($goods->id);
            Caching::product($goods->id);
          }
          Caching::subcategory($sub->id);
          Caching::productAnother($sub->id);
        }
        Caching::category($cat->id);
        Caching::categories();
      }
      $banner = Banner::where('siteID', Auth::user()->siteID);
      //dd($banner->first());
      //dd($banner);
      if (Cache::has('banner'.Auth::user()->siteID)) {
        Cache::forget('banner'.Auth::user()->siteID);
      }
      $view = '';
      $view .= view('2019.index.banner.banner_1', ['banner' => $banner->get()]);

      Cache::forever('banner'.Auth::user()->siteID, $view);

      if (Cache::has('Imagebanner'.Auth::user()->siteID)) {
        Cache::forget('Imagebanner'.Auth::user()->siteID);
      }
      Cache::forever('Imagebanner'.Auth::user()->siteID, Resize::ResizeImage($banner, '1900x690'));

      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      $sites = Site::get();
      // if (Cache::has('map'.Auth::user()->siteID)) {
      //   Cache::forget('map'.Auth::user()->siteID);
      // }
      // $view = '';
      // $view .= view('2019.map.map_2', ['info' => $company->info]);
      // //dd($view);
      // Cache::forever('map'.Auth::user()->siteID, $view);
      //
      if (Cache::has('footer_1_'.Auth::user()->siteID)) {
        Cache::forget('footer_1_'.Auth::user()->siteID);
      }
      $view = '';
      $view .= view('2019.layouts.footer.footer_1', ['info' => $company->info, 'logo' => Cache::get('logoTwo'.Auth::user()->siteID), 'logo' => Cache::get('logoOne'.Auth::user()->siteID)]);
      Cache::forever('footer_1_'.Auth::user()->siteID, $view);

      $menu = Menu::where('siteID', Auth::user()->siteID)->get();
      if (Cache::has('menu_1_'.Auth::user()->siteID)) {
        Cache::forget('menu_1_'.Auth::user()->siteID);
      }
      $view = '';
      $view .= view('2019.layouts.menu.menu_1', ['menu' => $menu, 'info' => $company->info, 'logo' => Cache::get('logoOne'.Auth::user()->siteID)]);
      Cache::forever('menu_1_'.Auth::user()->siteID, $view);
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      $menu = Menu::where('siteID', Auth::user()->siteID)->get();
      $text = Template::with('texts')->where('siteID', Auth::user()->siteID);
      if (Cache::has('menu_1_'.Auth::user()->siteID)) {
        Cache::forget('menu_1_'.Auth::user()->siteID);
      }

      $view = '';
      $view .= view('2019.layouts.menu.menu_1', ['menu' => $menu,'info' => $company->info, 'logo' => Cache::get('logoOne'.Auth::user()->siteID)]);
      Cache::forever('menu_1_'.Auth::user()->siteID, $view);
      //dd(Cache::get('menu'.Auth::user()->siteID));
      if (Cache::has('footer_1_'.Auth::user()->siteID)) {
        Cache::forget('footer_1_'.Auth::user()->siteID);
      }
      $view = '';
      $view .= view('2019.layouts.footer.footer_1', ['info' => $company->info, 'logo' => Cache::get('logoTwo'.Auth::user()->siteID)]);
      //dd($view);
      Cache::forever('footer_1_'.Auth::user()->siteID, $view);

      $company = Site::with('about')->with('info')->with('contacts')->where('id', Auth::user()->siteID)->first();
      $index = Template::with('texts')->where('siteID', Auth::user()->siteID)->where('slug', '/')->first();
      //dd();
      $about = Template::with('texts')->where('siteID', Auth::user()->siteID)->where('slug', 'about')->first();
      $contact = Template::with('texts')->where('siteID', Auth::user()->siteID)->where('slug', 'contact')->first();
      //dd($index);
      if($index !== null){

      if (Cache::has('about_1_'.Auth::user()->siteID)) {
        Cache::forget('about_1_'.Auth::user()->siteID);
      }
      $view = '';
      $view .= view('2019.index.about.about_1', ['texts' => $index->texts,]);
      Cache::forever('about_1_'.Auth::user()->siteID, $view);
      }
      //dd($text);
      if($about !== null){
        //dd('asd');
      Caching::about();
      //Caching::aboutObjects();
    }
    if($contact !== null){
      Caching::contact();
    }

    Caching::servicesContact();
    Caching::productObjects();
    Caching::productAbout();
    Caching::objects();
    Caching::about();
    Caching::aboutObjects();
    Caching::contact();
    }
    public function refreshCache(){

      if(Auth::user()->refreshCache == 1){
        Users::where('id', Auth::user()->id)->update(['refreshCache'=> 0]);
      }
      else{
        Users::where('id', Auth::user()->id)->update(['refreshCache'=> 1]);
      }
    }
    public function atmosphere(){
      if(Auth::user()->atmosphere == 1){
        Users::where('id', Auth::user()->id)->update(['atmosphere' => 0]);
      }
      else{
        Users::where('id', Auth::user()->id)->update(['atmosphere' => 1]);
      }
    }

    public function LocalImages(Request $request){
      $images = Image::where('siteID', Auth::user()->siteID)->get();
      $image = '<div style="background-color:white;">';
      foreach ($images as $img) {
        $image .= '<img src="/storage/app/'.$img->images.'" width="100" id="img_'.$img->id.'" class="locImages">';
      }
      return $image.'</div>';
      // 1602 1603 1604 1606 1611
      //dd($image);
    }

    public function selectSection($type,$parametr,Request $request){
        $unparametr[0] = 1;
        $unparametr[1] = 0;
        $update['product'] = ['published' => $unparametr[$parametr]];
        $update['services'] = ['published' => $unparametr[$parametr]];
        $update['stroitelstvo'] = ['published' => $unparametr[$parametr]];
        $update['bani'] = ['published' => $unparametr[$parametr]];
        $update['proyekty'] = ['published' => $unparametr[$parametr]];
        $update['izgotovleniye'] = ['published' => $unparametr[$parametr]];
        Section::where('slug', $type)->where('siteID', Auth::user()->siteID)->update($update[$type]);
        Caching::menu();
    }

    public function search($title){
      if($title == 'false'){
        return '';
      }
      $view = '<h5 style="margin-left:5px;">Категории</h5>';
      $categories = Category::select('id', 'title', 'published', 'main')->where('siteID', Auth::user()->siteID)->where('title', 'like', '%'.$title.'%')->get();
      $i = 1;
      foreach ($categories as $category) {
        if($i == 10){break;}
        if($category->published == 1){
          $activeEye = 'color:#5cb85c;';
        }
        else{
          $activeEye = '';
        }
        if($category->main == 1){
          $activeMain = 'color:#fec107;';
        }
        else{
          $activeMain = '';
        }
        //dd($category);
        $view .= '<div class="drop-item"><a href="/admin/category/'.$category->id.'/edit" style="font-size:15px; margin-left:10px;">'.$category->title.'</a>
        <i class="fa fa-star" aria-hidden="true" style="float:right;margin:7px 10px;'.$activeMain.'"></i>
        <i class="fa fa-eye" aria-hidden="true" style="float:right;margin:7px 0;'.$activeEye.'"></i>
        </div>';
        $i++;
      }
      $goods = Goods::with('categories')->where('siteID', Auth::user()->siteID)->where('title', 'like', '%'.$title.'%')->get();
      $i = 1;
      $view .= '<h5 style="margin-left:5px;">Товары</h5>';
      foreach ($goods as $product) {
        if($i == 10){break;}
        if($product->categories->type == 0){
          $type = 'goods';
        }
        elseif($product->categories->type == 1){
          $type = 'services';
        }
        elseif($product->categories->type == 4){
          $type = 'stroitelstvo';
        }
        elseif($product->categories->type == 5){
          $type = 'bani';
        }
        elseif($product->categories->type == 6){
          $type = 'proyekty';
        }
        elseif($product->categories->type == 7){
          $type = 'izgotovleniye';
        }
        if($product->published == 1){
          $activeEye = 'color:#5cb85c;';
        }
        else{
          $activeEye = '';
        }
        if($product->main == 1){
          $activeMain = 'color:#fec107;';
        }
        else{
          $activeMain = '';
        }
        $view .= '<div class="drop-item"><a href="/admin/'.$type.'/'.$product->id.'/edit?id='.$product->id.'" style="font-size:15px; margin-left:10px;">'.$product->title.'</a>
        <i class="fa fa-star" aria-hidden="true" style="float:right;margin:7px 10px;'.$activeMain.'"></i>
        <i class="fa fa-eye" aria-hidden="true" style="float:right;margin:7px 0;'.$activeEye.'"></i>
        </div>';
        $i++;
      }
      return $view;
    }

    public function searchIndex(Request $request){
      $categories = Category::with('section')->with('categor')->where('siteID', Auth::user()->siteID)->where('title', 'like', '%'.$request->search.'%')->get();
      //dd($categories);
      $goods = Goods::with('categories')->where('siteID', Auth::user()->siteID)->where('title', 'like', '%'.$request->search.'%')->get();
      $sections = Section::where('siteID', Auth::user()->siteID)->where('published', 1)->where('main', '!=', null)->get();
      $view = (string)view('admin.index', ['company' => Resize::company()]);
      $view .= view('admin.search', ['categories' => $categories, 'goods' => $goods, 'sections' => $sections]);
      return $view;
    }

    static function backupGoods($id, $type){
      $goods = Goods::where('siteID', $id)->whereHas('categories', function($query) use($type){
        $query->where('type', $type);
      })->get();
      if(isset($_GET['title']) && $_GET['title'] !== ''){
        $title[$type] = $_GET['title'];
      }
      else{
        $title[0] = 'goods';
        $title[1] = 'services';
        $title[4] = 'stroitelstvo';
        $title[5] = 'bani';
        $title[6] = 'proyekty';
        $title[7] = 'izgotovleniye';
      }
      $name[0] = 'goods';
      $name[1] = 'services';
      $name[4] = 'stroitelstvo';
      $name[5] = 'bani';
      $name[6] = 'proyekty';
      $name[7] = 'izgotovleniye';
      $excelName = $title[$type].'-'.date('d.m.Y H.i.s').'-'.Auth::user()->id;
                Excel::create($excelName, function($excel) use($goods) {
                              $excel->sheet('Товары', function($sheet) use($goods) {
                                $titleExcel =[];
                                foreach ($goods as $product) {
                                  $product = json_decode(json_encode($product), true);
                                  $titles = [];
                                  foreach ($product as $key => $value) {
                                    $titles[] = $key;
                                  }
                                  $titleExcel[] = $titles;
                                  break;
                                }
                                foreach ($goods as $product) {
                                  $product = json_decode(json_encode($product), true);
                                  $titles = [];
                                  foreach ($product as $key => $value) {
                                    $titles[] = $value;
                                  }
                                  $titleExcel[] = $titles;
                                }
                                  $sheet->fromArray($titleExcel, null, 'A1', true, false);
                              });
                          })->store("xlsx", storage_path('app/excel/backups/'.$id.'/'.$name[$type]));
    }

    static function backupCategories($id){
      $categories = Category::where('siteID', $id)->get();
      if(isset($_GET['title']) && $_GET['title'] !== ''){
        $title = $_GET['title'];
      }
      else{
        $title = 'categories';
      }
      $excelName = $title.'-'.date('d.m.Y H.i.s').'-'.Auth::user()->id;
      //dd($goods);
      //dd($goods);
      Excel::create($excelName, function($excel) use($categories) {
                    $excel->sheet('Товары', function($sheet) use($categories) {
                      $titleExcel =[];
                      foreach ($categories as $category) {
                        $category = json_decode(json_encode($category), true);
                        $titles = [];
                        foreach ($category as $key => $value) {
                          $titles[] = $key;
                        }
                        $titleExcel[] = $titles;
                        break;
                      }
                      foreach ($categories as $category) {
                        $category = json_decode(json_encode($category), true);
                        $titles = [];
                        foreach ($category as $key => $value) {
                          $titles[] = $value;
                        }
                        $titleExcel[] = $titles;
                      }
                        $sheet->fromArray($titleExcel, null, 'A1', true, false);
                    });
                })->store("xlsx", storage_path('app/excel/backups/'.$id.'/categories'));
    }

    static function backupObjects($id){
      $objects = Objects::where('siteID', $id)->get();
      $excelName = 'objects-'.date('d.m.Y H.i.s').'-'.Auth::user()->id;
      //dd($goods);
      //dd($goods);
      Excel::create($excelName, function($excel) use($objects) {
                    $excel->sheet('Товары', function($sheet) use($objects) {
                      $titleExcel =[];
                      foreach ($objects as $object) {
                        $object = json_decode(json_encode($object), true);
                        $titles = [];
                        foreach ($object as $key => $value) {
                          $titles[] = $key;
                        }
                        $titleExcel[] = $titles;
                        break;
                      }
                      foreach ($objects as $object) {
                        $object = json_decode(json_encode($object), true);
                        $titles = [];
                        foreach ($object as $key => $value) {
                          $titles[] = $value;
                        }
                        $titleExcel[] = $titles;
                      }
                        $sheet->fromArray($titleExcel, null, 'A1', true, false);
                    });
                })->store("xlsx", storage_path('app/excel/backups/'.$id.'/objects'));
    }
    static function beckupTexts($id){
      $texts = Text::where('siteID', $id)->get();
      $excelName = 'texts-'.date('d.m.Y H.i.s').'-'.Auth::user()->id;
      //dd($goods);
      //dd($goods);
      Excel::create($excelName, function($excel) use($texts) {
                    $excel->sheet('Товары', function($sheet) use($texts) {
                      $titleExcel =[];
                      foreach ($texts as $text) {
                        $text = json_decode(json_encode($text), true);
                        $titles = [];
                        foreach ($text as $key => $value) {
                          $titles[] = $key;
                        }
                        $titleExcel[] = $titles;
                        break;
                      }
                      foreach ($texts as $text) {
                        $text = json_decode(json_encode($text), true);
                        $titles = [];
                        foreach ($text as $key => $value) {
                          $titles[] = $value;
                        }
                        $titleExcel[] = $titles;
                      }
                        $sheet->fromArray($titleExcel, null, 'A1', true, false);
                    });
                })->store("xlsx", storage_path('app/excel/backups/'.$id.'/texts'));
    }
    static function beckupNews($id){
      $news = News::where('siteID', $id)->get();
      $excelName = 'news-'.date('d.m.Y H.i.s').'-'.Auth::user()->id;
      //dd($goods);
      //dd($goods);
      Excel::create($excelName, function($excel) use($news) {
                    $excel->sheet('Товары', function($sheet) use($news) {
                      $titleExcel =[];
                      foreach ($news as $new) {
                        $new = json_decode(json_encode($new), true);
                        $titles = [];
                        foreach ($new as $key => $value) {
                          $titles[] = $key;
                        }
                        $titleExcel[] = $titles;
                        break;
                      }
                      foreach ($news as $new) {
                        $new = json_decode(json_encode($new), true);
                        $titles = [];
                        foreach ($new as $key => $value) {
                          $titles[] = $value;
                        }
                        $titleExcel[] = $titles;
                      }
                        $sheet->fromArray($titleExcel, null, 'A1', true, false);
                    });
                })->store("xlsx", storage_path('app/excel/backups/'.$id.'/news'));
    }
    static function beckupBanners($id){
      $banners = Banner::where('siteID', $id)->get();
      $excelName = 'banners-'.date('d.m.Y H.i.s').'-'.Auth::user()->id;
      //dd($goods);
      //dd($goods);
      Excel::create($excelName, function($excel) use($banners) {
                    $excel->sheet('Товары', function($sheet) use($banners) {
                      $titleExcel =[];
                      foreach ($banners as $banner) {
                        $banner = json_decode(json_encode($banner), true);
                        $titles = [];
                        foreach ($banner as $key => $value) {
                          $titles[] = $key;
                        }
                        $titleExcel[] = $titles;
                        break;
                      }
                      foreach ($banners as $banner) {
                        $banner = json_decode(json_encode($banner), true);
                        $titles = [];
                        foreach ($banner as $key => $value) {
                          $titles[] = $value;
                        }
                        $titleExcel[] = $titles;
                      }
                        $sheet->fromArray($titleExcel, null, 'A1', true, false);
                    });
                })->store("xlsx", storage_path('app/excel/backups/'.$id.'/banners'));
    }
    static function beckupTizers($id){
      $tizers = Tizer::where('siteID', $id)->get();
      $excelName = 'tizers-'.date('d.m.Y H.i.s').'-'.Auth::user()->id;
      //dd($goods);
      //dd($goods);
      Excel::create($excelName, function($excel) use($tizers) {
                    $excel->sheet('Товары', function($sheet) use($tizers) {
                      $titleExcel =[];
                      foreach ($tizers as $tizer) {
                        $tizer = json_decode(json_encode($tizer), true);
                        $titles = [];
                        foreach ($tizer as $key => $value) {
                          $titles[] = $key;
                        }
                        $titleExcel[] = $titles;
                        break;
                      }
                      foreach ($tizers as $tizer) {
                        $tizer = json_decode(json_encode($tizer), true);
                        $titles = [];
                        foreach ($tizer as $key => $value) {
                          $titles[] = $value;
                        }
                        $titleExcel[] = $titles;
                      }
                        $sheet->fromArray($titleExcel, null, 'A1', true, false);
                    });
                })->store("xlsx", storage_path('app/excel/backups/'.$id.'/tizers'));
    }
    static function beckupTels($id){
      $tels = Tel::where('siteID', $id)->get();
      $excelName = 'tels-'.date('d.m.Y H.i.s').'-'.Auth::user()->id;
      //dd($goods);
      //dd($goods);
      Excel::create($excelName, function($excel) use($tels) {
                    $excel->sheet('Товары', function($sheet) use($tels) {
                      $titleExcel =[];
                      foreach ($tels as $tel) {
                        $tel = json_decode(json_encode($tel), true);
                        $titles = [];
                        foreach ($tel as $key => $value) {
                          $titles[] = $key;
                        }
                        $titleExcel[] = $titles;
                        break;
                      }
                      foreach ($tels as $tel) {
                        $tel = json_decode(json_encode($tel), true);
                        $titles = [];
                        foreach ($tel as $key => $value) {
                          $titles[] = $value;
                        }
                        $titleExcel[] = $titles;
                      }
                        $sheet->fromArray($titleExcel, null, 'A1', true, false);
                    });
                })->store("xlsx", storage_path('app/excel/backups/'.$id.'/tels'));
    }
    static function beckupEmails($id){
      $emails = Email::where('siteID', $id)->get();
      $excelName = 'emails-'.date('d.m.Y H.i.s').'-'.Auth::user()->id;
      //dd($goods);
      //dd($goods);
      Excel::create($excelName, function($excel) use($emails) {
                    $excel->sheet('Товары', function($sheet) use($emails) {
                      $titleExcel =[];
                      foreach ($emails as $email) {
                        $email = json_decode(json_encode($email), true);
                        $titles = [];
                        foreach ($email as $key => $value) {
                          $titles[] = $key;
                        }
                        $titleExcel[] = $titles;
                        break;
                      }
                      foreach ($emails as $email) {
                        $email = json_decode(json_encode($email), true);
                        $titles = [];
                        foreach ($email as $key => $value) {
                          $titles[] = $value;
                        }
                        $titleExcel[] = $titles;
                      }
                        $sheet->fromArray($titleExcel, null, 'A1', true, false);
                    });
                })->store("xlsx", storage_path('app/excel/backups/'.$id.'/emails'));
    }
    static function beckupInfo($id){
      $informations = Information::where('siteID', $id)->get();
      $excelName = 'informations-'.date('d.m.Y H.i.s').'-'.Auth::user()->id;
      //dd($goods);
      //dd($goods);
      Excel::create($excelName, function($excel) use($informations) {
                    $excel->sheet('Товары', function($sheet) use($informations) {
                      $titleExcel =[];
                      foreach ($informations as $info) {
                        $info = json_decode(json_encode($info), true);
                        $titles = [];
                        foreach ($info as $key => $value) {
                          $titles[] = $key;
                        }
                        $titleExcel[] = $titles;
                        break;
                      }
                      foreach ($informations as $info) {
                        $info = json_decode(json_encode($info), true);
                        $info = [];
                        foreach ($info as $key => $value) {
                          $titles[] = $value;
                        }
                        $titleExcel[] = $titles;
                      }
                        $sheet->fromArray($titleExcel, null, 'A1', true, false);
                    });
                })->store("xlsx", storage_path('app/excel/backups/'.$id.'/informations'));
    }

    public function saves($slug){
      if($slug == 'product'){
        $slug = 'goods';
      }
      try{
        $files = scandir('storage/app/excel/backups/'.Auth::user()->siteID.'/'.$slug.'/', 1);
      }
      catch(\Exception $ex){
        return '<tr><td>1</td><td>Сохранений нет</td></tr>';
      }
      $tr = '';
      $i = 1;

      foreach ($files as $file) {


        $fileex = explode('-', $file);
        if(count($fileex)<2){
          break;
        }
        $date = explode('-', $file)[1];
        $date = Resize::date(date("Y-m-d H:i:s", strtotime($date)));
        $userid = explode('.', $fileex[2])[0];
        $user = Users::where('id', $userid)->value('name');
        $tr .= '<tr>
          <td>'.$i.'</td>
          <td>'.iconv("CP1251", "UTF-8", $fileex[0] ).'</td>
          <td>'.$user.'</td>
          <td>'.$date.'</td>
          <td><i class="fa fa-floppy-o" aria-hidden="true" id="'.$file.'" slug="'.$slug.'"></i></td>
        </tr>';
        $i++;
      }
      return $tr;
    }
    public function savesupload($slug, $name){
      if($slug == 'product'){
        $slug = 'goods';
      }
      $file = 'storage/app/excel/backups/'.Auth::user()->siteID.'/'.$slug.'/'.$name;
      $numberColumn = ['A' => 0, 'B' => 1, 'C' => 2, 'D' => 3, 'E' => 4, 'F' => 5, 'G' => 6, 'H' => 7, 'I' => 8, 'J' => 9, 'K' => 10, 'L' => 11,
      'M' => 12, 'N' => 13, 'O' => 14, 'P' => 15, 'Q' => 16, 'R' => 17, 'S' => 18, 'T' => 19, 'U' => 20, 'V' => 21, 'W' => 22, 'X' => 23,
      'Y' => 24, 'Z' => 25, 'AA' => 26, 'AB' => 27, 'AC' => 28, 'AD' => 29, 'AE' => 30, 'AF' => 31, 'AG' => 32, 'AH' => 33, 'AI' => 34, 'AJ' => 35,
      'AK' => 36, 'AL' => 37, 'AM' => 38, 'AN' => 39, 'AO' => 40, 'AP' => 41];
      $book = Excel::load($file);
      $sheet = $book->getSheet(0);
      $highestRow = $sheet->getHighestRow();
      $highestColumn = $sheet->getHighestColumn();
      $cols = [];
      if($slug == 'category'){
        Category::where('siteID', Auth::user()->siteID)->delete();
      }
      elseif($slug == 'goods'){
        Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
          $query->where('type',0);
        })->delete();
      }
      elseif($slug == 'services'){
        Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
          $query->where('type',1);
        })->delete();
      }
      elseif($slug == 'stroitelstvo'){
        Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
          $query->where('type',4);
        })->delete();
      }
      elseif($slug == 'bani'){
        Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
          $query->where('type',5);
        })->delete();
      }
      elseif($slug == 'proyekty'){
        Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
          $query->where('type',6);
        })->delete();
      }
      elseif($slug == 'objects'){
        Objects::where('siteID', Auth::user()->siteID)->delete();
      }
      elseif($slug == 'texts'){
        Text::where('siteID', Auth::user()->siteID)->delete();
      }
      elseif($slug == 'banners'){
        Banner::where('siteID', Auth::user()->siteID)->delete();
      }
      elseif($slug == 'tizers'){
        Tizer::where('siteID', Auth::user()->siteID)->delete();
      }
      elseif($slug == 'informations'){
        Information::where('siteID', Auth::user()->siteID)->delete();
      }
      elseif($slug == 'tels'){
        Tel::where('siteID', Auth::user()->siteID)->delete();
      }
      elseif($slug == 'emails'){
        Email::where('siteID', Auth::user()->siteID)->delete();
      }
      for ($z=0; $z <= $numberColumn[$highestColumn]; $z++) {
        $cols[] = $book->getSheet(0)->getCellByColumnAndRow($z, 1)->getValue();
      }
      for ($i = 2; $i <= $highestRow; $i++) {
        $rows = [];
        for ($t=0; $t <= $numberColumn[$highestColumn]; $t++) {
          $value = $book->getSheet(0)->getCellByColumnAndRow($t, $i)->getValue();
          if(is_numeric($value)){
            $value = (int)$value;
          }
          $rows[$cols[$t]] = $value;
        }
        if($slug == 'category'){
          Category::where('siteID', Auth::user()->siteID)->delete();
        }
        elseif($slug == 'goods' || $slug == 'services' || $slug == 'stroitelstvo' || $slug == 'bani' || $slug == 'proyekty' || $slug == 'izgotovleniye'){
          Goods::insert([$rows]);
        }
        elseif($slug == 'objects'){
          Objects::insert([$rows]);
        }
        elseif($slug == 'texts'){
          Text::insert([$rows]);
        }
        elseif($slug == 'banners'){
          Banner::insert([$rows]);
        }
        elseif($slug == 'tizers'){
          Tizer::insert([$rows]);
        }
        elseif($slug == 'informations'){
          Information::insert([$rows]);
        }
        elseif($slug == 'tels'){
          Tel::insert([$rows]);
        }
        elseif($slug == 'emails'){
          Email::insert([$rows]);
        }

      }
    }
    public function favorite(Request $request){
      $check = Usite::where('site', $request->id)->where('user', Auth::user()->id)->first();
      if($check == null){
        Usite::insert(['site'=> $request->id, 'user' => Auth::user()->id, 'created_at' => date('Y-m-d H:i:s')]);
      }
    }
      public function favoriteDestroy(Request $request){
          Usite::where('id',$request->id)->delete();
    }
    public function sortFavorite(){
      $i = 1;
      foreach ($_GET['arrayorder'] as $s) {
        //dd($s);
        Usite::where('id', $s)->update(['sort'=> $i]);
        $i++;
      }
    }


    static function takeContent($site){
      //dd(Category::max('id'));
      if($site == null){
        return redirect()->back();
      }

          $categries = Category::with('goodsAll')->with('images')->with(['objects' => function($query){
            $query->with('images');
          }])->where('siteID', $site)->get();
          $oldCategoryID = [];
          $oldProductID = [];

          foreach ($categries as $category) {
            //КАТЕГОРИИ
              if($category->parent_id == null){
                $newCategoryID = Category::insertGetId(['seotitle' => $category->seotitle, 'seoname' => $category->seoname, 'seodescription' => $category->seodescription, 'seokeywords' => $category->seokeywords, 'title' => $category->title, 'description' => $category->description,
                'slug' => $category->slug, 'main' => $category->main, 'type' => $category->type, 'published' => $category->published, 'open_cat' => $category->open_cat, 'youtube' => $category->youtube, 'deleted' => 0, 'offer' => $category->offer,
              'sort' => $category->sort, 'checkseo' => $category->checkseo, 'status' => $category->status, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
              $oldCategoryID[$category->id] = $newCategoryID;
              }
              else{
                //dd($oldCategoryID);
                if(!isset($oldCategoryID[$category->parent_id])){
                  $oldCategoryID[$category->parent_id] = Category::max('id') + $category->parent_id - $category->id;
                }
                $newCategoryID = Category::insertGetId(['seotitle' => $category->seotitle, 'seoname' => $category->seoname, 'seodescription' => $category->seodescription, 'seokeywords' => $category->seokeywords, 'title' => $category->title, 'description' => $category->description,
                'slug' => $category->slug, 'main' => $category->main, 'type' => $category->type, 'published' => $category->published, 'open_cat' => $category->open_cat, 'youtube' => $category->youtube, 'deleted' => 0, 'offer' => $category->offer,
              'sort' => $category->sort, 'checkseo' => $category->checkseo, 'status' => $category->status, 'siteID' => Auth::user()->siteID, 'parent_id' => $oldCategoryID[$category->parent_id], 'created_at' => date('Y-m-d H:i:s'), ]);
              $oldCategoryID[$category->id] = $newCategoryID;
              }
              if($category->images !== null && file_exists('storage/app/'.$category->images->images)){
                //dd($category);
                // $newImage = 'public/'.uniqid().'.'.explode('.',$category->images->images)[1];
                // Storage::copy($category->images->images, $newImage);
                Image::insertGetId(['images' => $category->images->images, 'category' => $newCategoryID, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
              }



            //ТОВАРЫ

            if($category->type == 0){
              foreach ($category->goodsAll as $goods) {
                if($goods->parent_id == null){
                $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> Auth::user()->siteID, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                  'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                  'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                  'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                  'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                  'counts' => $goods->counts, 'status' => $goods->status, 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                  $oldProductID[$goods->id] = $newProductID;
                }
                else{
                  $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> Auth::user()->siteID, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                    'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                    'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                    'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                    'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                    'counts' => $goods->counts, 'status' => $goods->status, 'parent_id' => $oldProductID[$goods->parent_id], 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                    $oldProductID[$goods->id] = $newProductID;
                }

                  foreach ($goods->imageses as $image) {
                    if($image->images !== null && file_exists('storage/app/'.$image->images)){
                      // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                      // Storage::copy($image->images, $newImage);

                      Image::insertGetId(['images' => $image->images, 'goods' => $newProductID, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
                    }

                  }
              }
            }

            if($category->type == 1){
              foreach ($category->goodsAll as $goods) {
                if($goods->parent_id == null){
                $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> Auth::user()->siteID, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                  'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                  'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                  'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                  'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                  'counts' => $goods->counts, 'status' => $goods->status, 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                  $oldProductID[$goods->id] = $newProductID;
                }
                else{
                  $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> Auth::user()->siteID, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                    'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                    'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                    'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                    'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                    'counts' => $goods->counts, 'status' => $goods->status, 'parent_id' => $oldProductID[$goods->parent_id], 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                    $oldProductID[$goods->id] = $newProductID;
                }

                  foreach ($goods->imageses as $image) {
                    if($image->images !== null && file_exists('storage/app/'.$image->images)){
                      // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                      // Storage::copy($image->images, $newImage);

                      Image::insertGetId(['images' => $image->images, 'goods' => $newProductID, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
                    }

                  }
              }
            }

            if($category->type == 7){
              foreach ($category->goodsAll as $goods) {
                if($goods->parent_id == null){
                $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> Auth::user()->siteID, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                  'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                  'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                  'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                  'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                  'counts' => $goods->counts, 'status' => $goods->status, 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                  $oldProductID[$goods->id] = $newProductID;
                }
                else{
                  $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> Auth::user()->siteID, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                    'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                    'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                    'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                    'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                    'counts' => $goods->counts, 'status' => $goods->status, 'parent_id' => $oldProductID[$goods->parent_id], 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                    $oldProductID[$goods->id] = $newProductID;
                }

                  foreach ($goods->imageses as $image) {
                    if($image->images !== null && file_exists('storage/app/'.$image->images)){
                      // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                      // Storage::copy($image->images, $newImage);

                      Image::insertGetId(['images' => $image->images, 'goods' => $newProductID, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
                    }

                  }
              }
            }

            if($category->type == 4){
              foreach ($category->goodsAll as $goods) {
                if($goods->parent_id == null){
                $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> Auth::user()->siteID, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                  'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                  'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                  'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                  'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                  'counts' => $goods->counts, 'status' => $goods->status, 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                  $oldProductID[$goods->id] = $newProductID;
                }
                else{
                  $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> Auth::user()->siteID, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                    'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                    'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                    'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                    'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                    'counts' => $goods->counts, 'status' => $goods->status, 'parent_id' => $oldProductID[$goods->parent_id], 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                    $oldProductID[$goods->id] = $newProductID;
                }

                  foreach ($goods->imageses as $image) {
                    if($image->images !== null && file_exists('storage/app/'.$image->images)){
                      // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                      // Storage::copy($image->images, $newImage);

                      Image::insertGetId(['images' => $image->images, 'goods' => $newProductID, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
                    }

                  }
              }
            }
            if($category->type == 5){
              foreach ($category->goodsAll as $goods) {
                if($goods->parent_id == null){
                $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> Auth::user()->siteID, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                  'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                  'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                  'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                  'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                  'counts' => $goods->counts, 'status' => $goods->status, 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                  $oldProductID[$goods->id] = $newProductID;
                }
                else{
                  $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> Auth::user()->siteID, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                    'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                    'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                    'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                    'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                    'counts' => $goods->counts, 'status' => $goods->status, 'parent_id' => $oldProductID[$goods->parent_id], 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                    $oldProductID[$goods->id] = $newProductID;
                }

                  foreach ($goods->imageses as $image) {
                    if($image->images !== null && file_exists('storage/app/'.$image->images)){
                      // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                      // Storage::copy($image->images, $newImage);

                      Image::insertGetId(['images' => $image->images, 'goods' => $newProductID, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
                    }

                  }
              }
            }
            if($category->type == 6){
              foreach ($category->goodsAll as $goods) {
                if($goods->parent_id == null){
                $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> Auth::user()->siteID, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                  'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                  'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                  'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                  'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                  'counts' => $goods->counts, 'status' => $goods->status, 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                  $oldProductID[$goods->id] = $newProductID;
                }
                else{
                  $newProductID =  Goods::insertGetId(['category' => $newCategoryID, 'siteID'=> Auth::user()->siteID, 'seotitle' => $goods->seotitle, 'seodescription' => $goods->seodescription, 'seokeywords' => $goods->seokeywords,
                    'checkseo' => $goods->checkseo, 'title' => $goods->title, 'subtitle' => $goods->subtitle, 'short_description' => $goods->short_description, 'description' => $goods->description, 'main' => $goods->main,
                    'coast' => $goods->coast, 'type_cost' => $goods->type_cost, 'min_cost' => $goods->min_cost, 'type_min_cost' => $goods->type_min_cost, 'articul' => $goods->articul,
                    'presence' => $goods->presence, 'delivery' => $goods->delivery, 'payment' => $goods->payment, 'published' => $goods->published, 'slug' => $goods->slug, 'type' => $goods->type,
                    'seoname' => $goods->seoname, 'youtube' => $goods->youtube, 'seo' => $goods->seo, 'deleted' => $goods->deleted, 'sort' => $goods->sort, 'dateDelete' => $goods->dateDelete, 'articulate' => $goods->articulate,
                    'counts' => $goods->counts, 'status' => $goods->status, 'parent_id' => $oldProductID[$goods->parent_id], 'min_count' => $goods->min_count, 'warningCount' => $goods->warningCount, 'deleted' => 0, 'created_at' => date('Y-m-d H:i:s')]);
                    $oldProductID[$goods->id] = $newProductID;
                }

                  foreach ($goods->imageses as $image) {
                    if($image->images !== null && file_exists('storage/app/'.$image->images)){
                      // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                      // Storage::copy($image->images, $newImage);

                      Image::insertGetId(['images' => $image->images, 'goods' => $newProductID, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
                    }

                  }
              }
            }



              foreach ($category->objects as $object) {
              $objectID = Objects::insertGetId(['siteID' => Auth::user()->siteID, 'parent_id' => $newCategoryID, 'title' => $object->title, 'offer' => $object->offer, 'text' => $object->text, 'popular' => $object->popular, 'published' => $object->published,
              'type' => $object->type, 'main' => $object->main, 'slug' => $object->slug, 'created_at' => date('Y-m-d H:i:s')]);

              foreach ($object->images as $image) {
                if($image->images !== null && file_exists('storage/app/'.$image->images)){
                  // $newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                  // Storage::copy($image->images, $newImage);

                  Image::insertGetId(['images' => $image->images, 'object' => $objectID, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
                }

              }
              }



          }

            $newTexts = Text::with('images')->where('siteID', $site)->get();
            foreach ($newTexts as $text) {
              Text::where('siteID', Auth::user()->siteID)->where('type', $text->type)->update(['seoTitle' => $text->seoTitle, 'seoDescription' => $text->seoDescription, 'keywords' => $text->keywords, 'title' => $text->title, 'text' => $text->text, 'checkseo' => $text->checkseo, 'created_at' => date('Y-m-d H:i:s')]);
              $textID = Text::where('siteID', Auth::user()->siteID)->where('type', $text->type)->value('id');
              foreach ($text->images as $image) {
                if($image->images !== null && file_exists('storage/app/'.$image->images)){
                  //$newImage = 'public/'.uniqid().'.'.explode('.',$image->images)[1];
                  //Storage::copy($image->images, $newImage);

                  Image::insertGetId(['images' => $image->images, 'text' => $textID, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
                }

              }
            }



            $newNews = News::where('siteID', $site)->get();

            foreach ($newNews as $news) {
              if(file_exists('storage/app/'.$news->image) && $news->image !== null){
                //$newImage = 'public/'.uniqid().'.'.explode('.',$news->image)[1];
                //Storage::copy($news->image, $newImage);
              }
              else{
                $newImage = null;
              }

              News::insertGetId(['siteID'=> Auth::user()->siteID, 'title' => $news->title, 'text' => $news->text, 'description' => $news->description, 'published' => $news->published, 'image' => $news->image, 'created_at' => date('Y-m-d H:i:s')]);
            }


            $newBanners = Banner::where('siteID', $site)->get();
            foreach ($newBanners as $banenr) {
              if(file_exists('storage/app/'.$banenr->image) && $banenr->image !== null){
              }
              else{
                $newImage = null;
              }

              Banner::insertGetId(['siteID'=> Auth::user()->siteID, 'title' => $banenr->title, 'text' => $banenr->text, 'type' => $banenr->type, 'published' => $banenr->published, 'image' => $banenr->image, 'color' => $banenr->color, 'main' => $banenr->main, 'status' => $banenr->status, 'created_at' => date('Y-m-d H:i:s')]);
            }


            $newTizers = Tizer::where('siteID', $site)->get();
            foreach ($newTizers as $tizer) {
              if(file_exists('storage/app/'.$tizer->image)  && $tizer->image !== null){
              }
              else{
                $newImage = null;
              }

              Tizer::insertGetId(['siteID'=> Auth::user()->siteID, 'title' => $tizer->title, 'description' => $tizer->description, 'image' => $tizer->image,'status' => $tizer->status, 'created_at' => date('Y-m-d H:i:s')]);
            }



            $information = Information::where('siteID', $site)->first();
            if(file_exists('storage/app/'.$information->logo1)  && $information->logo1 !== null){
              $logo1 = 'public/'.uniqid().'.'.explode('.',$information->logo1)[1];
              Storage::copy($information->logo1, $logo1);
            }
            else{
              $logo1 = null;
            }

            if(file_exists('storage/app/'.$information->logo2)  && $information->logo2 !== null){
              $logo2 = 'public/'.uniqid().'.'.explode('.',$information->logo2)[1];
              Storage::copy($information->logo2, $logo2);
            }
            else{
              $logo2 = null;
            }

            if(file_exists('storage/app/'.$information->favicon)  && $information->favicon !== null){
              $favicon = 'public/'.uniqid().'.'.explode('.',$information->favicon)[1];
              Storage::copy($information->favicon, $favicon);
            }
            else{
              $favicon = null;
            }
            Information::insert(['siteID' => Auth::user()->siteID, 'timeJob' => $information->timeJob, 'address' => $information->address, 'map' => $information->map, 'metrika' => $information->metrika, 'verYa' => $information->verYa,
            'verGo' => $information->verGo, 'analytics' => $information->analytics, 'city' => $information->city, 'logo1' => $logo1, 'logo2' => $logo2, 'favicon' => $favicon, 'color_menu' => $information->color_menu,
            'image_menu' => $information->image_menu, 'color_text_menu' => $information->color_text_menu, 'full_name' => $information->full_name, 'short_name' => $information->short_name, 'legal_address' => $information->legal_address, 'actual_address' => $information->actual_address,
            'general_director' => $information->general_director, 'INN' => $information->INN, 'KPP' => $information->KPP, 'OGRN' => $information->OGRN, 'OKPO' => $information->OKPO, 'OKBED' => $information->OKBED, 'OKATO' => $information->OKATO, 'Bank' => $information->Bank,
            'Acount_chek' => $information->Acount_chek, 'Corp_check' => $information->Corp_check, 'BIK' => $information->BIK]);
            $visual = DB::table('visual')->where('siteID', $site);
            DB::table('visual')->update(['siteID' => Auth::user()->siteID, 'menu' => $visual->menu, 'footer' => $visual->footer]);
}

    public function refrash(){
        $sites = Site::get();
        foreach($sites as $site){
      $siteID = $site->id;
      $visual = DB::table('visual')->where('siteID', $siteID)->first();

    if($visual->menu == 1 || $visual->menu == null){
        if (Cache::has('menu_1_'.$siteID)) {
      Cache::forget('menu_1_'.$siteID);//slug = "contact" asc
    }
    $company = Site::with('about')->with('info')->where('id',$siteID)->first();
    //dd($company);
    $menu = Section::where('siteiD',$siteID)->whereIn('slug',['about','product','services','portfolio', 'contact', 'stroitelstvo', 'bani', 'proyekty', 'izgotovleniye'])
    ->where('published', 1)->orderByRaw('slug = "about" desc, slug = "contact" asc, slug = "portfolio" asc,  main asc')->get();
    //dd($menu);
    $tel = Tel::where('siteID', $siteID)->first();
    $email = Email::where('siteID', $siteID)->first();
    $categories = Category::where('siteID', $siteID)->where('published', 1)->where('parent_id', null)->get();
    //dd($services);
    $view = '';
    $view .= view('2019.layouts.menu.menu_1', ['menu' => $menu, 'url' => $company, 'info' => $company->info, 'tel' => $tel, 'email' => $email, 'siteID' => $siteID, 'categories' => $categories, 'logo' => Cache::get('logoOne'.$siteID)]);
    Cache::forever('menu_1_'.$siteID, $view);
    }
    else{
        if (Cache::has('menu_1_'.$siteID)) {
      Cache::forget('menu_1_'.$siteID);//slug = "contact" asc
    }
    $company = Site::with('about')->with('info')->where('id',$siteID)->first();
    //dd($company);
    $menu = Section::where('siteiD',$siteID)->whereIn('slug',['about','product','services','portfolio', 'contact', 'stroitelstvo', 'bani', 'proyekty', 'izgotovleniye'])
    ->where('published', 1)->orderByRaw('slug = "about" desc, slug = "contact" asc, slug = "portfolio" asc,  main asc')->get();
    //dd($menu);
    $tel = Tel::where('siteID', $siteID)->first();
    $email = Email::where('siteID', $siteID)->first();
    $categories = Category::where('siteID', $siteID)->where('published', 1)->where('parent_id', null)->get();
    //dd($services);
    $view = '';
    $view .= view('2019.layouts.menu.menu_2', ['menu' => $menu, 'info' => $company->info, 'tel' => $tel, 'email' => $email, 'siteID' => $siteID, 'categories' => $categories, 'logo' => Cache::get('logoOne'.$siteID)]);
    Cache::forever('menu_1_'.$siteID, $view);
    }

    if($visual->footer == 1 || $visual->footer == null){
        $menu = Section::where('siteiD',$siteID)->whereIn('slug',['product','services','portfolio','stroitelstvo', 'bani', 'proyekty', 'izgotovleniye'])
    ->where('published', 1)->orderby('main', 'asc')->get();

    $tel = Tel::where('siteID', $siteID)->first();
    $email = Emails::where('siteID', $siteID)->first();
    $categories = Category::where('siteID', $siteID)->where('published', 1)->where('parent_id', null)->get();
    $goods = \App\Model\Goods::with('product_url_cat')->whereHas('product_url_cat', function($query){$query->where('published', 1);})->where('siteID', $siteID)->where('published', 1)->select('id','category','siteID','title','main','slug')->limit(6)->get();

    $company = Sites::with('information')->with('sections')->where('id', $siteID)->first();
    if (Cache::has('footer_1_'.$siteID)){Cache::forget('footer_1_'.$siteID);}
    $view = '';

    if($_SERVER["REQUEST_URI"] == '/'){$jq = 1;}
    else{$jq = null;}
    $view .= view('2019.layouts.footer.footer_1', ['city' => $company->information->cities, 'menu' => $menu, 'tel' => $tel, 'email' => $email, 'categories' =>$categories,'goods'=>$goods,  'jq' => $jq,'company' => $company, 'siteID'=>$siteID]);

    Cache::forever('footer_1_'.$siteID, $view);
    }
    else{
        $tel = Tel::where('siteID', $siteID)->first();
        $email = Emails::where('siteID', $siteID)->first();
        $categories = Category::where('siteID', $siteID)->where('published', 1)->where('parent_id', null)->get();
        $goods = \App\Model\Goods::with('product_url_cat')->whereHas('product_url_cat', function($query){$query->where('published', 1);})->where('siteID', $siteID)->where('published', 1)->select('id','category','siteID','title','main','slug')->limit(6)->get();

        $company = Sites::with('information')->with('sections')->where('id', $siteID)->first();
        $mainsection = Section::with(['category' => function($query)use($siteID){$query->where('siteID', $siteID);}])
        ->where('siteID', $siteID)->where('published', 1)->where('type', '!=', null)->orderby('main', 'asc')->limit(2)->get();

        $portfolio = Category::with('objects')->whereHas('objects', function($query){$query->where('parent_id', '!=', null);})->where('siteiD', $siteID)->where('published', 1)->select('id','parent_id','title','slug','published','seoname')->get();
        $partners = Partners::where('siteID', $siteID)->where('published', 1)->get();
        if (Cache::has('footer_1_'.$siteID)){Cache::forget('footer_1_'.$siteID);}
        $view = '';

        if($_SERVER["REQUEST_URI"] == '/'){$jq = 1;}
        else{$jq = null;}
        $view .= view('2019.layouts.footer.footer_2', ['city' => $company->information->cities, 'mainsection' => $mainsection, 'partners'=>$partners, 'portfolio'=>$portfolio, 'tel' => $tel, 'email' => $email, 'categories' =>$categories,'goods'=>$goods,  'jq' => $jq,'company' => $company, 'siteID'=>$siteID]);

        Cache::forever('footer_1_'.$siteID, $view);
    }
        }
    echo 'Ну вроди фсе';
    }
}
