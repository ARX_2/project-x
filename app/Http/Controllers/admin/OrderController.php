<?php

namespace App\Http\Controllers\admin;

use App\Model\Order_column;
use App\Model\Order_company;
use App\Model\Order_customer;
use App\Model\Order_message;
use App\Orders;
use App\Support;
use App\Users;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use App\Information;
use App\Site;
use App\Goods;
use App\Email;
use Mail;
use Resize;



class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $view =  '';

      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
      //  $order2 = Orders::with('user', 'order_customer', 'order_message.user', 'order_goods.goods')->where('id', 318)->first();
    //  dd($order2);
        $column = Order_column::with('orders.order_customer')->where('siteID', Auth::user()->siteID)->orderBy('position', 'asc')->orderBy('id', 'asc')->get();
     // dd($column);
      $orders = Orders::with('order_customer')->where('siteID', Auth::user()->siteID)->orderBy('id', 'desc')->get();
      //  $order2 = Orders::with('user', 'order_customer', 'order_message.user')->where('id', 312)->first();
//dd($order2);
        $view .= view('admin.orders.index',[
          'siteID' => Auth::user()->siteID,
            'column' => $column,
          'orders' => $orders,
        ]);
      return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $error = Validator::make($request->all(), [
            'create' => 'required',
            'column_id' => 'required',
        ]);

        if($error->fails()) {return response()->json(['errors' => $error->errors()->all()]);}
        //dd(Auth::user()->id);
        $siteID = Cache::get('/' . $request->getHttpHost())->id;

        $create_order_id = Orders::insertGetId([
            'siteID' => $siteID,
            'column_id' => $request->column_id ?? null,
            'form' => 'Новый заказ',
            'position' => '1',
            'created_at' => date('Y-m-d H:i:s')
        ]);
       // dd($create_order_id);
        $description = 'Создал новый заказ!';
        Order_customer::insert(['siteID' => $siteID, 'order_id' => $create_order_id, 'created_at' => date('Y-m-d H:i:s')]);
        Order_message::insert(['siteID' => $siteID, 'order_id' => $create_order_id, 'user_id' => Auth::user()->id, 'description' => $description, 'created_at' => date('Y-m-d H:i:s')]);

        $responsible = Users::where('siteID', $siteID)->get();
        $column = Order_column::where('siteID', $siteID)->get();
        $order = Orders::with('user', 'order_customer', 'order_message.user', 'order_goods.goods')->where('id', $create_order_id)->first();

        return view('admin.orders.modal', [
            'responsible' => $responsible,
            'order' => $order,
            'column' => $column
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
      $id = Cache::get('/' . $request->getHttpHost())->id;
        $email = Email::where('siteID', $id)->value('email');
        Orders::insert([ 'email' => $request->email, 'tel' => $request->tel, 'message' => $request->message, 'goods' => $request->product, 'siteID' => $id, 'created_at' => date('Y-m-d H:i:s')]);
        $goods = Goods::where('id', $request->product)->value('title');
        $subject = "Сообщение от " . $request->getHttpHost();

        Mail::send('mail_to', ['to' => $request->getHttpHost(), 'email' => $request->email, 'tel' => $request->tel, 'text' => $request->message, '$product' => $goods], function($message) use($email, $goods, $request){
        $message->to($email, 'to web')->cc('dmitrypovyshev@yandex.ru');
        $message->from('lara@yandex.ru', $request->getHttpHost())->subject('Поступил новый заказ с '.$request->getHttpHost());
    });
        if($request->stroitelstvo !== null){
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */

    public function show($id, Request $request){
        $siteID = Cache::get('/' . $request->getHttpHost())->id;

        $responsible = Users::where('siteID', $siteID)->get();
        $column = Order_column::where('siteID', $siteID)->get();
        $order = Orders::with('user', 'order_customer', 'order_message.user', 'order_goods.goods', 'order_company')->where('id', $id)->first();
        return view('admin.orders.modal', [
            'responsible' => $responsible,
            'column' => $column,
            'order' => $order
        ]);
    }

    public function position(Request $request){
        if (isset($_POST['update'])) {
            foreach($_POST['positions'] as $position) {
                Orders::where('id', $position[0])->update(['column_id' => $position[1], 'position' => $position[2]]);
            }
        }
        return 'Позиции обновлены!';
    }

    public function addComment(Request $request)
    {

        $siteID = Cache::get('/' . $request->getHttpHost())->id;

        $error = Validator::make($request->all(), [
            'description' => 'required',
        ]);

        if($error->fails()) {return response()->json(['errors' => $error->errors()->all()]);}

        $message_id = Order_message::insertGetId([
            'siteID' => $siteID,
            'order_id' => $request->order_id ?? null,
            'user_id' => $request->user_id ?? null,
            'description' => htmlspecialchars($request->description) ?? null,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        $message = Order_message::with('user')->where('id', $message_id)->first();

        return view('admin.orders.modal_message', [
            'message' => $message
        ]);
    }

    public function editName(Request $request){
        Orders::where('id', $request->order_id ?? null)->update(['title' => htmlspecialchars($request->title) ?? null, 'updated_at' => date('Y-m-d H:i:s')]);
        return response()->json(['success' => 'Название изменено!']);
    }

    public function responsible(Request $request){
        Orders::where('id', $request->order_id ?? null)->update(['user_id' => $request->user_id ?? null, 'updated_at' => date('Y-m-d H:i:s')]);
        return response()->json(['success' => 'Ответственный назначен! - '.$request->order_id ?? null]);
    }

    public function selectColumn(Request $request){
        Orders::where('id', $request->order_id ?? null)->update(['column_id' => $request->column_id ?? null, 'updated_at' => date('Y-m-d H:i:s')]);
        return response()->json(['success' => 'Колонка изменена! - '.$request->order_id ?? null]);
    }

    public function editPerson(Request $request){
        Order_customer::where('order_id', $request->order_id)->update([
            'name' => htmlspecialchars($request->name) ?? null,
            'phone' => htmlspecialchars($request->phone) ?? null,
            'email' => htmlspecialchars($request->email) ?? null,
            'job_position' => htmlspecialchars($request->job_position) ?? null,
            'skype' => htmlspecialchars($request->skype) ?? null,
            'vk' => htmlspecialchars($request->vk) ?? null,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        return response()->json(['success' => 'Клиент сохранен!']);
    }

    public function editCompany(Request $request){
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        if($request->act == 2){
            Order_company::insert([
                'siteID' => $siteID,
                'order_id' => $request->order_id,
                'title' => htmlspecialchars($request->title) ?? null,
                'email' => htmlspecialchars($request->email) ?? null,
                'phone' => htmlspecialchars($request->phone) ?? null,
                'phone2' => htmlspecialchars($request->phone2) ?? null,
                'address' => htmlspecialchars($request->address) ?? null,
                'site' => htmlspecialchars($request->site) ?? null,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            return 'Компания добавлена';
        }
        elseif($request->act == 1){
            Order_company::where('order_id', $request->order_id)->update([
                'title' => htmlspecialchars($request->title) ?? null,
                'email' => htmlspecialchars($request->email) ?? null,
                'phone' => htmlspecialchars($request->phone) ?? null,
                'phone2' => htmlspecialchars($request->phone2) ?? null,
                'address' => htmlspecialchars($request->address) ?? null,
                'site' => htmlspecialchars($request->site) ?? null,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            return 'Компания обновлена';
        }
        else{
            return 'Не обновлю!';
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */

    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function deleteTest(Request $request)
    {
       //  $flights = Orders::onlyTrashed()->get();

        // $flights = Orders::where('id', 428)->delete();
       //dd($flights);
    }

    public function modal(Request $request){
      if(Cache::has('productModal'.$request->id)){
        $product = Cache::get('productModal'.$request->id);
      }
      else{
        $product = Goods::where('id', $request->id)->first();
        Cache::forever('productModal'.$request->id, $product);
      }
      return view('2019.product_services.modal', ['product' => $product]);
      //dd($request->id);
    }

    public function needcall(Request $request){
      return view('2019.product_services.needcall', ['id' => $request->id]);
    }

    public function createColumn(Request $request)
    {
        $error = Validator::make($request->all(), [
            'name_column' => 'required',
        ]);

        if($error->fails()) {return response()->json(['errors' => $error->errors()->all()]);}
        //dd(Auth::user()->id);
        $siteID = Cache::get('/' . $request->getHttpHost())->id;

        $create_order_id = Order_column::insertGetId([
            'siteID' => $siteID,
            'title' => htmlspecialchars($request->name_column) ?? null,
            'position' => '99',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        $column = Order_column::where('id', $create_order_id)->first();

        return view('admin.orders.column', [
            'column' => $column,
        ]);
    }

    public function editTitleColumn(Request $request){

        $error = Validator::make($request->all(), [
            'column_id' => 'required',
            'title' => 'required',
        ]);

        if($error->fails()) {return response()->json(['errors' => $error->errors()->all()]);}

        Order_column::where('id', $request->column_id)->update(['title' => htmlspecialchars($request->title)]);

        return 'Название изменено!';
    }
    public function columnPosition(Request $request){
        if (isset($_POST['update'])) {
            foreach($_POST['positions'] as $position) {
                Order_column::where('id', $position[0])->update(['position' => $position[1]]);
            }
        }
        return 'Позиции колонок обновлены!';
    }
    public function deleteColumn(Request $request)
    {
        $column = Order_column::onlyTrashed()->get();
        $column = Order_column::where('id', $request->column_id)->delete();
        //dd($flights);
        return $request->column_id.' - Колонка удалена!';
    }
}
