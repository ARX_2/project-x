<?php

namespace App\Http\Controllers\admin;

use App\Companyprice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use ImageResize;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Users;
use App\Company;
use App\Information;
class CompanipriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin.companyprice.index', [
        'companies' => Companyprice::where('siteID', Auth::user()->siteID)->paginate(10),
        'Users' => Users::get(),
        'sites' => DB::table('sites')->get(),
        'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
        'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.companyprice.edit', [
      'companyprice' => [],
      'Users' => Users::get(),
      'sites' => DB::table('sites')->get(),
      'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
      'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
          ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Companyprice  $companyprice
     * @return \Illuminate\Http\Response
     */
    public function show(Companyprice $companyprice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Companyprice  $companyprice
     * @return \Illuminate\Http\Response
     */
    public function edit(Companyprice $companyprice)
    {
      return view('admin.companyprice.edit', [
      'companyprice' => Companyprice::where('siteID', Auth::user()->siteID)->get(),
      'Users' => Users::get(),
      'sites' => DB::table('sites')->get(),
      'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
      'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
          ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Companyprice  $companyprice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Companyprice $companyprice)
    {

      if($request->file('image') !== null){


         //dd($value1);
         $value = $request->file('image');
         $filename  = uniqid() . '.' . $value->getClientOriginalExtension();
       ImageResize::make($value->getRealPath())->resize(105, 58)->save('storage/app/public/'. $filename);
         $image = Companyprice::where('id',$companyprice->id)->update(['title' => $request->title, 'subtitle' => $request->subtitle, 'description' => $request->description, 'image' => 'public/'.$filename, 'siteID' => Auth::user()->siteID]);
      }
      else{
        $image = Companyprice::where('id',$companyprice->id)->update(['title' => $request->title, 'subtitle' => $request->subtitle, 'description' => $request->description,  'siteID' => Auth::user()->siteID]);
      }
        // dd($Categoryid);



            return redirect()->route('admin.companyprice.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Companyprice  $companyprice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Companyprice $companyprice)
    {

      Storage::delete($companyprice->image);
      $companyprice->delete();
      return redirect()->route('admin.companyprice.index');
    }

    public function insert(Request $request){

      //dd($request);
      if($request->file('image') !== null){


         //dd($value1);
         $value = $request->file('image');
         $filename  = uniqid() . '.' . $value->getClientOriginalExtension();
       ImageResize::make($value->getRealPath())->resize(105, 58)->save('storage/app/public/'. $filename);
      }
      else{
        $path = null;
      }
        // dd($Categoryid);


            $image = Companyprice::create(['title' => $request->title, 'subtitle' => $request->subtitle, 'description' => $request->description, 'image' => 'public/'.$filename, 'siteID' => Auth::user()->siteID]);
            return redirect()->route('admin.companyprice.index');


    }
}
