<?php

namespace App\Http\Controllers\Admin;

use App\Information;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Users;
use App\Company;
use App\Section;
use ImageResize;
use Caching;
class InformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function show(Information $information)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function edit(Information $information)
    {

	//dd($_GET);
	if($_GET['type'] == 'logo'){
	  return view('admin.information.logo', [
	  'information' => Information::where('siteID', Auth::user()->siteID)->get(),
	  'type' => $_GET['type'],
    'Users' => Users::get(),
    'sites' => DB::table('sites')->get(),
    'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
    'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
        ]);
	}
  if($_GET['type'] == 'contact'){
    return view('admin.information.contact', [
	  'information' => Information::where('siteID', Auth::user()->siteID)->get(),
	  'type' => $_GET['type'],
    'Users' => Users::get(),
    'sites' => DB::table('sites')->get(),
    'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
    'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
        ]);
  }
  if($_GET['type'] == 'map'){
    return view('admin.information.map', [
    'information' => Information::where('siteID', Auth::user()->siteID)->get(),
    'type' => $_GET['type'],
    'Users' => Users::get(),
    'sites' => DB::table('sites')->get(),
    'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
    'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
        ]);
  }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Information $information)
    {

      $backdoor = Information::where('id', $information->id)->where('siteID', Auth::user()->siteID)->first();
      //dd($backdoor);
      //dd($backdoor);
      if($backdoor == null){
        echo 'О компании не найдено';
        die;
      }
      // /dd($request);
    if($request->file('image1') !== null || $request->file('image2') !== null || $request->file('image3') !== null){
      if($request->file('image1') !== null){

        Storage::delete($information->logo1);
        //dd($information);
       foreach ($request->file('image1') as $value) {
         $path = Storage::putFile('public', $value);

        // dd($Categoryid);
         $image = Information::where('siteID', Auth::user()->siteID)
         ->update(['logo1' => $path]);
       }
       }
       if($request->file('image2') !== null){
         Storage::delete($information->logo2);
        foreach ($request->file('image2') as $value1) {
          //dd($value1);
          $path1 = Storage::putFile('public', $value1);
         // dd($Categoryid);
          $image = Information::where('siteID', Auth::user()->siteID)
          ->update(['logo2' => $path1]);
        }
        }
        if($request->file('image3') !== null){
          Storage::delete($information->favicon);
         foreach ($request->file('image3') as $value2) {
           //dd($value1);
           $filename  = uniqid() . '.' . $value2->getClientOriginalExtension();
         ImageResize::make($value2->getRealPath())->resize(15, 15)->save('storage/app/public/'. $filename);
           $image = Information::where('siteID', Auth::user()->siteID)
           ->update(['favicon' => 'public/'.$filename]);
         }
         }


        }


        if($request->tel1 !== null){
          $image = Information::where('siteID', Auth::user()->siteID)
          ->update(['tel1' => $request->tel1, 'tel2' => $request->tel2, 'timeJob' => $request->timeJob, 'address' => $request->address, 'email' => $request->email]);
        }
        //dd($request->map);
        if($request->map !== null){
          $image = Information::where('siteID', Auth::user()->siteID)
          ->update(['map' => $request->map, 'metrika' => $request->metrika, 'verYa' => $request->verYa, 'verGo' => $request->verGo,]);
        }
        //dd($request);
        return redirect()->route('admin.information.edit', ['type' => $request->type]);
        }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function destroy(Information $information)
    {

    }

    public function insert(Request $request){

      //dd($request);
      if($request->file('image1') !== null || $request->file('image2') !== null || $request->file('image3') !== null){
        if($request->file('image1') !== null){

         foreach ($request->file('image1') as $value) {
           $path = Storage::putFile('public', $value);

          // dd($Categoryid);
           $image = Information::create(['logo1' => $path, 'siteID' => Auth::user()->siteID]);
         }
         }
         if($request->file('image2') !== null){

          foreach ($request->file('image2') as $value1) {
            //dd($value1);
            $path1 = Storage::putFile('public', $value1);
           // dd($Categoryid);
            $image = Information::create(['logo2' => $path1, 'siteID' => Auth::user()->siteID]);
          }
          }
          if($request->file('image3') !== null){

           foreach ($request->file('image3') as $value2) {
             //dd($value1);
             $filename  = uniqid() . '.' . $value2->getClientOriginalExtension();
           ImageResize::make($value2->getRealPath())->resize(15, 15)->save('storage/app/public/'. $filename);
            // dd($Categoryid);
             $image = Information::create(['favicon' => 'public/'.$filename, 'siteID' => Auth::user()->siteID]);
           }
           }
          return redirect()->route('admin.information.edit', ['type' => 'logo']);
          }

          if($request->tel1 !== null){
            $image = Information::create(['tel1' => $request->tel1, 'tel2' => $request->tel2, 'timeJob' => $request->timeJob, 'address' => $request->address, 'email' => $request->email, 'siteID' => Auth::user()->siteID]);
            return redirect()->route('admin.information.edit', ['type' => 'contact']);
          }
          if($request->map !== null){
            $image = Information::create(['map' => $request->map, 'metrika' => $request->metrika, 'verYa' => $request->verYa, 'verGo' => $request->verGo, 'siteID' => Auth::user()->siteID]);
            return redirect()->route('admin.information.edit', ['type' => 'map']);
          }

    }


    public function changeSection(Request $request){

      if($request->goods !== null){
        Section::where('siteID', Auth::user()->siteID)->where('slug', 'product')->update(['title' => $request->goods, 'checktitle' => 1]);
        return redirect()->route('admin.goods.index');
      }
      elseif($request->objects !== null){
        Section::where('siteID', Auth::user()->siteID)->where('slug', 'portfolio')->update(['title' => $request->objects, 'checktitle' => 1]);
        return redirect()->route('admin.object.index');
      }
      elseif($request->proyekty !== null){
        Section::where('siteID', Auth::user()->siteID)->where('slug', 'proyekty')->update(['title' => $request->proyekty, 'checktitle' => 1]);
        return redirect()->route('admin.proyekty.index');
      }
      elseif($request->stroitelstvo !== null){
        Section::where('siteID', Auth::user()->siteID)->where('slug', 'stroitelstvo')->update(['title' => $request->stroitelstvo, 'checktitle' => 1]);
        return redirect()->route('admin.stroitelstvo.index');
      }
      elseif($request->bani !== null){
        Section::where('siteID', Auth::user()->siteID)->where('slug', 'bani')->update(['title' => $request->bani, 'checktitle' => 1]);
        return redirect()->route('admin.bani.index');
      }
      elseif($request->izgotovleniye !== null){
        Section::where('siteID', Auth::user()->siteID)->where('slug', 'izgotovleniye')->update(['title' => $request->izgotovleniye, 'checktitle' => 1]);
        return redirect()->route('admin.izgotovleniye.index');
      }
      else{
        Section::where('siteID', Auth::user()->siteID)->where('slug', 'services')->update(['title' => $request->services, 'checktitle' => 1]);
        return redirect()->route('admin.services.index');
      }

    }
    public function settingUpdate($setting,Request $request){
      $update['verification'] = ['verYa' => $request->verYa, 'verGo' => $request->verGo];
      $update['metrica'] = ['metrika' => $request->metrica];
      $update['analytics'] = ['analytics' => $request->analytics];
      Information::where('siteID', Auth::user()->siteID)->update($update[$setting]);
      Caching::menu();
      return redirect('admin/settings/site/technical/'.$setting);
    }
    public function settingDesignUpdate($setting,Request $request){
        //dd($request);
      $color = explode(',', $request->color);
      if($request->file('file') !== null){
        $img = $request->file('file');
        $filename = Storage::putFile('public/background', $img);
        //dd($filename);
      }
      else{
        $filename = null;
      }
      //dd($color);
      if($request->color_menu !== null){
           $update['color'] = ['color_menu' => $request->color_menu, 'color_text_menu' => $request->color_text_menu, 'image_menu' => $filename];

    }
    else{
       if(isset($color[1])){
      $update['color'] = ['color_menu' => $color[0], 'color_text_menu' => $color[1], 'image_menu' => $filename];
      //dd($update);
    }
    else{
      $update['color'] = ['color_menu' => $request->color, 'color_text_menu' => $request->color_text_menu, 'image_menu' => $filename];
    }

    }
    //dd($update);
      //Конец Цвета
      Information::where('siteID', Auth::user()->siteID)->update($update[$setting]);
      Caching::menu();
      return redirect('admin/settings/site/design/'.$setting);
    }

    public function image_menu(Request $request){
     $image_menu =  Information::where('siteID', Auth::user()->siteID)->value('image_menu');
     Storage::delete('backgroung/'.$image_menu);
     Caching::menu();
     Information::where('siteID', Auth::user()->siteID)->update(['image_menu'=> null]);
    }

}
