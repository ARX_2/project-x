<?php

namespace App\Http\Controllers\admin;
use App\Site;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Basket;
use App\Goods;
use App\Users;
use Hash;
use App\Address;
use App\Coupon;
use App\Favorite;
use App\Message;
use App\Offer;
class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::user()->siteID !== null){
        return redirect()->route('admin.company.index');
      }
      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      return view('2019.buyer.index', [
        'company' => $site->company,
        'info' => $site->info,
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function offers(){
      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      $offers = Offer::with('offers')->with('product')->where('userid', Auth::user()->id)->where('parent_id', null)->get();
      //dd($offers);
      return view('2019.buyer.orders', [
        'company' => $site->company,
        'info' => $site->info,
        'offers' => $offers
      ]);
    }
    public function addGoods(Request $request){
      //dd($request);
      $goods = $request->goods;
      $slug = Goods::with('goods')->value('slug');
      //dd($slug);
      Basket::insert(['goods' => $request->goods, 'counts' => $request->count, 'userid' => Auth::user()->id, 'created_at' => date('Y-m-d H:i:s')]);

      return redirect('product/'.$slug);
    }
    public function card(){
      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      $basket = Basket::where('userid', Auth::user()->id)->with('product')->get();
      $address = Address::where('userid', Auth::user()->id)->where('main', 1)->first();
      return view('2019.cart.index', [
        'company' => $site->company,
        'info' => $site->info,
        'basket' => $basket,
        'address' => $address,
      ]);
    }
    public function addtooffer(){
    $basket = Basket::where('userid', Auth::user()->id)->get();
    $parent =  Offer::insertGetId(['goods'=> $basket[0]->goods, 'userid' => $basket[0]->userid, 'counts' => $basket[0]->count, 'status' => 1, 'created_at' => date('Y-m-d H:i:s')]);
    //dd($parent);
      Basket::where('id', $basket[0]->id)->delete();
      $i = 0;
      foreach ($basket as $b) {
        if($i !== 0){
        Offer::insert(['goods'=> $b->goods, 'userid' => $b->userid, 'counts' => $b->count, 'parent_id' => $parent, 'status' => 1, 'created_at' => date('Y-m-d H:i:s')]);
        Basket::where('id', $b->id)->delete();
        }
        $i++;
      }
      return redirect()->route('account.card.index');
    }

    public function addFavorite(Request $request){
      $favorite = Favorite::where('goods', $request->id)->where('userid', Auth::user()->id)->first();
      if($favorite == null){
        Favorite::insert(['userid' => Auth::user()->id, 'goods' => $request->id, 'created_at' => date('Y-m-d H:i:s')]);
      }
      else{
        Favorite::where('goods', $request->id)->where('userid', Auth::user()->id)->delete();
      }
      $slug = Goods::where('id', $request->id)->value('slug');
      if($request->favorite !== null){
        return redirect()->route('account.profile.favorites');
      }
      else{
      return redirect('/product/'.$slug);
      }
    }
    public function profile(Request $request){

      if($request->message == null){
        $message = 3;
      }
      else{
        $message = $request->message;
      }
      //dd($message);
      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      $user = Users::with('addresses')->where('id', Auth::user()->id)->first();
      return view('2019.buyer.profile', [
        'company' => $site->company,
        'info' => $site->info,
        'user' => $user,
        'message' => $message,
        'addresses' => $user->addresses,
      ]);
    }

    public function updateProfile(Request $request){
      //dd($request);
      Users::where('id', Auth::user()->id)->update(['name' => $request->name, 'tel' => $request->tel, 'email' => $request->email, 'type' => $request->type]);
      return redirect()->route('account.profile');
    }




    public function changePass(Request $request){
        $message = 0;
          if($request->password == $request->confirm){
          Users::where('id', Auth::user()->id)->update(['password'=> bcrypt($request->password)]);
          }
          else{
            $message = 1;
          }

        return redirect()->route('account.profile', ['message'=>$message]);
    }

    public function addressCreate(){

      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      $user = Users::where('id', Auth::user()->id)->first();
      return view('2019.buyer.address.view', [
        'company' => $site->company,
        'info' => $site->info,
        'user' => $user,
        'address' => null,
      ]);
    }

    public function addressStore(Request $request){
      Address::insert(['name' => $request->name, 'area' => $request->area, 'city'=> $request->city,
      'address'=> $request->address, 'code'=>$request->code, 'tel'=> $request->tel, 'userid'=> Auth::user()->id, 'main' => $request->main]);
      return redirect()->route('account.profile');
    }

    public function addressEdit(Request $request){
      $id = $request->address;
      $userid = Auth::user()->id;
      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      $user = Users::with(['address' => function($address) use($id, $userid){
        $address->where('id', $id);
        $address->where('userid', $userid);
      }])->where('id', Auth::user()->id)->first();
      if($user->address == null){
        return redirect()->route('account.profile');
      }
      //dd($user);
      return view('2019.buyer.address.view', [
        'company' => $site->company,
        'info' => $site->info,
        'user' => $user,
        'address' => $user->address,
      ]);
    }
    public function addressUpdate(Request $request){
     $backdoor = Address::where('id', $request->id)->where('userid', Auth::user()->id)->value('id');
     //dd($backdoor !== null);
      if($backdoor !== null){
        Address::where('id', $request->id)->where('userid', Auth::user()->id)
        ->update(['name' => $request->name, 'area' => $request->area, 'city'=> $request->city,
        'address'=> $request->address, 'code'=>$request->code, 'tel'=> $request->tel, 'main' => $request->main]);
        if($request->main == 1){
        Address::where('id', '!=', $request->id)->where('userid', Auth::user()->id)->update(['main'=> 2]);
        }
      }
        return redirect()->route('account.profile');
    }

    public function coupons(){
      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      $coupons = Coupon::where('userid', Auth::user()->id)->get();
      //$user = Users::with('addresses')->where('id', Auth::user()->id)->first();
      return view('2019.buyer.kupon', [
        'company' => $site->company,
        'info' => $site->info,
        'coupons' => $coupons,
        //'user' => $user,
      ]);
    }
    public function favorites(){
      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      $favorites = Favorite::with('product')->where('userid', Auth::user()->id)->get();
      //$user = Users::with('addresses')->where('id', Auth::user()->id)->first();
      return view('2019.buyer.favorites', [
        'company' => $site->company,
        'info' => $site->info,
        'favorites' => $favorites,
        //'user' => $user,
      ]);
    }

    public function messages(){
      $site = Site::with('company')->with('info')
      ->where('id', 1)->first();
      $messages = Message::with('user')->where('userid', Auth::user()->id)->get();
      //$user = Users::with('addresses')->where('id', Auth::user()->id)->first();
      return view('2019.buyer.message.index', [
        'company' => $site->company,
        'info' => $site->info,
        'messages' => $messages,
        //'user' => $user,
      ]);
    }

    static function test(Request $request){
      // Данные мерчанта, константы.
$merchant = '2021';		// ID мерчанта
$terminal = '2021-alias'; 		// ID терминала
$psk = '686dd34d081b2291636766c349421c086a318cc0';			// PSK мерчанта

// Сгенерированы ранее, например, в корзине покупателя.
$trackid = rand(1, 999999);   		// Идентификатор операции,
$amount = '1';		// Сумма операции
$action = '1';			// Код транзакции, 1 - покупка

// Хэш пароля
$salt = $merchant . $amount . $trackid . $action . $psk;		// Данные хэша
$hash = sha1( $salt );

// Http POST request to PaymentInitServlet
$url = 'https://ecm.sngb.ru:443/ECommerce/PaymentInitServlet';	// тестовый
//$url = 'https://ecm.sngb.ru:443/Gateway/PaymentInitServlet';		// промышленный

$params = array(
        'merchant' => $merchant ,
        'terminal' => $terminal ,
        'action' => $action ,
        'amt' => $amount ,
        'trackid' => $trackid ,
        'udf1' => 'Вид услуги',
        'udf2' => 'Номер договора',
        'udf3' => 'Фамилия, имя, отчество',
        'udf4' => 'Номер заявки',
        'udf5' => $hash
);

 // Параметры запроса
$postdata = "";
foreach ( $params as $key => $value ) $postdata .= "&".rawurlencode($key)."=".rawurlencode($value);

// POST
$ch = curl_init();
curl_setopt ($ch, CURLOPT_URL, $url );
curl_setopt ($ch, CURLOPT_POST, 1 );
curl_setopt ($ch, CURLOPT_POSTFIELDS, $postdata );
curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

// Ответ содержит URL для перехода пользователя на HPP или страницу Error.
$result = curl_exec ($ch);
curl_close($ch);

return $result;
echo $result.'
<html>
<!— Форма перехода на сайт банка -->
    <form action="'.$result.'" method="post" name="form1" autocomplete="off">
        <input type="submit" name="proceed" value="Proceed to Checkout" class="checkoutButton">
    </form>
</html>
';
    }
}
