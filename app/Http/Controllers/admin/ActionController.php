<?php

namespace App\Http\Controllers\admin;

use App\Action;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use App\Image;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Users;
use ImageResize;
use App\Site;
use App\Information;
use App\Contact;
use App\Text;
use App\Template;
class ActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      $action = Action::where('siteID', Auth::user()->siteID)->where('published',1)->paginate(10);
      $countShow = Action::where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
      $countHidden = Action::where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
      return view('admin.sections.action.index',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'actions'=>$action,
        'countHidden' => $countHidden,
        'countShow' => $countShow
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
        return view('admin.sections.action.create', [
          'company' => $company,
          'siteID' => Auth::user()->siteID,
          'action'=> null,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->file('file') !== null){
        $value = $request->file('file');
           $path = Storage::putFile('public', $value);
       }
       else{
         $path = null;
       }

      $company = Action::insert(['title' => $request->title,
                                  'text' => $request->text,
                                  'description' => $request->description,
                                  'published' => 1,
                                  'image' => $path,
                                  'datefrom' =>$request->datefrom,
                                  'dateto' => $request->dateto,
                                  'slug' => str_slug($request->title, "-"),
                                  'siteID' => Auth::user()->siteID]);

      return redirect()->route('admin.action.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function show(Action $action)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function edit(Action $action)
    {
      $action = Action::where('siteID', Auth::user()->siteID)->where('id', $action->id)->first();
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      return view('admin.sections.action.create', [
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'action'=> $action,
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Action $action)
    {
      //dd($request);
      if($request->file('file') == null){
        $path = $action->image;
      }
      else{
        $value = $request->file('file');
           $path = Storage::putFile('public', $value);
            Storage::delete($action->image);
      }
      $doc = Action::where('id', $action->id)
      ->where('siteID', Auth::user()->siteID)
      ->update(['title' => $request->title,
                                  'text' => $request->text,
                                  'description' => $request->description,
                                  'image' => $path,
                                  'datefrom' =>$request->datefrom,
                                  'dateto' => $request->dateto]);
        return redirect()->route('admin.action.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function destroy(Action $action)
    {
        //
    }

    public function hide(Request $request){
      $backdoor = Action::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('published');
      if($backdoor == 0){
        Action::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 1]);
      }
      elseif($backdoor == 1){
        Action::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 0]);
      }
      return redirect()->route('admin.action.index');
    }

    public function main(Request $request){
      $backdoor = Action::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('main');
      if($backdoor == 0){
        Action::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['main' => 1]);
      }
      elseif($backdoor == 1){
        Action::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['main' => 0]);
      }
      return redirect()->route('admin.action.index');
    }
    public function hidden(){
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      $action = Action::where('siteID', Auth::user()->siteID)->where('published',0)->paginate(10);
      $countShow = Action::where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
      $countHidden = Action::where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
      return view('admin.sections.action.index',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'actions'=>$action,
        'countHidden' => $countHidden,
        'countShow' => $countShow
      ]);
    }
    public function imageDelete(Request $request){

     $backdoor =  Action::where('id', $request->id)->where('siteID', auth::user()->siteID)->first();

      if($backdoor !== null){
        Storage::delete($backdoor->image);
        Action::where('id', $request->id)->update(['image' => null]);
      }
    }
}
