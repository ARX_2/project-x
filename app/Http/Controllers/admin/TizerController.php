<?php

namespace App\Http\Controllers\Admin;

use App\Tizer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use DB;
use App\Users;
use ImageResize;
use App\Site;
use Caching;
use Resize;
class TizerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      $tizers = Tizer::where('siteID', Auth::user()->siteID)->where('status', 2)->get();
      if($request->error !== null){
        $error = $request->error;
      }
      else{
        $error = null;
      }

      //$sites = Site::get();
      //dd($company);
      //dd($company);
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
      $view .= view('admin.sections.tizers.index',[
        'siteID' => Auth::user()->siteID,
        'tizers' => $tizers,
        'error' => $error,
        'link' => $_SERVER['REQUEST_URI'],
      ]);
      return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tizers = Tizer::where('siteID', Auth::user()->siteID)->count('id');
        if($tizers >= 5){
          //dd($tizers);
          $request['ajax'] = 'true';
          $request['error'] = 1;
          return  self::index($request);
        }
        $tizer =  Tizer::where('status',1)->first();
        if($tizer == null){
          $id = Tizer::insertGetId(['status' => 1, 'siteID' => Auth::user()->siteID]);
          $tizer = Tizer::where('id',$id)->first();
        }
        $view = '';
        if($request->ajax == null){
          $view .= view('admin.index', ['company' => Resize::company(),]);
        }
          $view .= view('admin.sections.tizers.create',[
          'tizer' => $tizer,
        ]);
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->file('image') !== null){
        $value = $request->file('image');
        $filename  = uniqid() . '.' . $value->getClientOriginalExtension();

      ImageResize::make($value->getRealPath())->save('storage/app/public/'. $filename);
      $path = 'public/'.$filename;
      }
      else{
        $path = null;
      }
      Tizer::insert(['title' => $request->title, 'description' => $request->description,  'image' => $path, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
      return redirect()->route('admin.tizers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tizer  $tizer
     * @return \Illuminate\Http\Response
     */
    public function show(Tizer $tizer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tizer  $tizer
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Tizer $tizer)
    {
      $tizer = Tizer::where('id', $tizer->id)->where('siteID', Auth::user()->siteID)->first();
      if($tizer == null){
        return redirect()->route('admin.tizers.index');
      }

      $view = '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }

      $view .= view('admin.sections.tizers.create',[
        'tizer' => $tizer
      ]);
      return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tizer  $tizer
     * @return \Illuminate\Http\Response
     */
    public function update($slug,$id,$parametr,Request $request, Tizer $tizer)
    {
      $update['title'] = ['title' => $parametr];
      $update['description'] = ['description' => $parametr];
      $update['status'] = ['status' => $parametr];
      // /dd($path);
        Tizer::where('id', $id)->update($update[$slug]);
        if($slug = 'status'){
          $request['ajax'] = 'true';
          return  self::index($request);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tizer  $tizer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tizer $tizer)
    {
        dd($tizer);
    }
    public function delete(Request $request)
    {
      $tizer = Tizer::where('id', $request->id)->where('siteID', Auth::user()->siteID)->first();
      if($tizer !== null){
        Storage::delete($tizer->image);
        Tizer::where('id', $request->id)->delete();
      }
      return redirect()->route('admin.tizers.index');
    }
    public function imageUpload(Request $request){
      if($request->file('file') !== null){
      $img =  $request->file('file');
        $path = Storage::putFile('public', $img);
        Tizer::where('id',$request->id)->where('siteID', Auth::user()->siteID)->update(['image' => $path]);
        $data = ['image' => $path];
      return json_encode($data);
      }

    }
}
