<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Image;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Users;
use App\Company;
class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin.articles.index', [
        'articles' => Article::where('siteID',  Auth::user()->siteID)->paginate(10),
        'Users' => Users::get(),
        'sites' => DB::table('sites')->get(),
        'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.articles.create', [
          'article' => [],
          'categories' => Category::with('children')->where('parent_id', '0')->get(),
          'delimiter' => '',
          'Users' => Users::get(),
          'sites' => DB::table('sites')->get(),
          'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       //dd($request->all());
       //dd($request);
        //$path = Storage::putFile('iamges', $request->file('image'));

         $article = Article::create(['published' => $request->published,
                                      'title' => $request->title,
                                      'slug' => $request->slug,
                                      'categories' => $request->categories,
                                      'description_short' => $request->description_short,
                                      'description' => $request->description,
                                      'meta_title' => $request->meta_title,
                                      'meta_description' => $request->meta_description,
                                      'meta_keyword' => $request->meta_keyword,
                                      'created_by' => $request->created_by,]);
         $articleid = Article::max('id');
        // dd($articleid);
        if($request->file('image') !== null){
         foreach ($request->file('image') as $value) {
           $path = Storage::putFile('public', $value);
           $image = Image::create(['images' => $path,'article' => $articleid, ]);
         }
         }
        // dd($articleid);

         //dd($image);
        // $image = Image::create($)

         if($request->input('categories')) :
           $article->categories()->attach($request->input('categories'));
         endif;

         return redirect()->route('admin.article.index');
     }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
      //dd($article);
      //dd(Image::with('categories')->where('article', $article->id)->get());
      return view('admin.articles.edit', [
    'article' => $article,
    'categories' => Category::with('children')->where('parent_id', '0')->get(),
    'delimiter' => '',
    'images' => Image::with('categories')->where('article', $article->id)->get(),
    'Users' => Users::get(),
    'sites' => DB::table('sites')->get(),
    'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
  ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
      //  dd($article);
      //dd($request);
      if($request->file('image') !== null){
       foreach ($request->file('image') as $value) {
         //dd($article->id);
         $path = Storage::putFile('public', $value);
         $image = Image::create(['images' => $path,'article' => $article->id, ]);
       }
       }
      // dd($image);
       //dd($image);
      Article::where('id', $article->id)->update(['title' => $article->title,
      'description_short' => $article->description_short,
      'description' => $article->description,
      'meta_title' => $article->meta_title,
      'meta_description' => $article->meta_description,
      'meta_keyword' => $article->meta_keyword,
      'published' => $article->published,
      'viewed' => $article->viewed,
      'created_by' => $article->created_by,
      'modified_by' => $article->modified_by,
      'updated_at' => $article->updated_at,
      'created_at' => $article->created_at,
      ]);
    return redirect()->route('admin.article.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
      //dd($article);
      //dd($article);
      $article->delete();
    return redirect()->route('admin.article.index');

    }
    public function imageDestroy(){
      //dd($img);
      //dd($_GET);
      Image::with('categories')->where('images', $_GET['image'])->delete();
      Storage::delete($_GET['image']);
      //$img->delete();
      return redirect()->route('admin.article.edit', [$_GET['editurl']]);
    }
}
