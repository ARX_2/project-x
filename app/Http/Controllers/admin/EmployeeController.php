<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\Users;
use App\Employee;
use App\Services;
use App\masterServeces;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Company;
use Illuminate\Support\Facades\Storage;
use App\Site;
use App\UserRolle;
use Resize;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //dd(masterServeces::get());
      $company = Resize::company();
      //$users = Users::with('userrolle')->where('siteID', Auth::user()->siteID)->where('userRolle', '!=', 1)->get();
      $userRolles = UserRolle::whereHas('users', function($query){
        $query->where('siteID', Auth::user()->siteID);
      })->with(['users' => function($query){
        $query->where('siteID', Auth::user()->siteID);
      }])->get();
      //dd($userRolles);
      //dd($userRolles);
      return view('admin.employee.index',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        //'users'=>$users,
        'userRolles' => $userRolles,
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.employee.create', [
      'contact' => [],
      'Users' => Users::with('users'),
      'delimiter' => '',
      'Users' => Users::get(),
      'sites' => DB::table('sites')->get(),
      'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
      'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request->password);
        //dd($request->userRolle);
        //dd(Auth::user()->siteID);
        //dd($request);
        if($request->file('image') !== null){
           $path = Storage::putFile('public', $request->file('image'));

         }
         else{
           $path = null;
         }
      $contact = Users::create(['name' => $request->name,
                                'email' => $request->email,
                                'password' => bcrypt($request->password),
                                'userRolle' => $request->userRolle,
                                'published' => $request->published,
                                'title' => $request->title,
                                'description' => $request->description,
                                'image' => $path,
                                'tel' => $request->tel,
                                'siteID' => Auth::user()->siteID,
                                'sex' => $request->user_sex,
                                'job' => $request->job,
                                'lastname' => $request->lastname]);

      //dd($contact);
      // Categories
      return redirect()->route('admin.employee.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Users $employee)
    {
    //  dd($employee);
      return view('admin.employee.edit', [
    'User' => $employee,
    'Users' => Users::with('users'),
    'delimiter' => '',
    'Users' => Users::get(),
    'sites' => DB::table('sites')->get(),
    'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
    'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
  ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Users $employee)
    {
      //dd($employee);
      //dd($request);
      if($request->file('image') !== null){
        Storage::delete($employee->image);
         $path = Storage::putFile('public', $request->file('image'));

       }
       else{
         $path = $employee->image;
       }
       //dd($request->lastname);
       Users::where('id', $employee->id)->where('siteID', Auth::user()->siteID)
                      ->update(['name' => $request->name,
                                'lastname' => $request->lastname,
                                 'email' => $request->email,
                                 'password' => bcrypt($request->password),
                                 'title' => $request->title,
                                 'description' => $request->description,
                                 'image' => $path,
                                 'tel' => $request->tel,
                                 'userRolle' => $request->userRolle,
                                 'sex' => $request->user_sex,
                                 'job' => $request->job,]);
      // $employee->update(['name' => $request->name,
      //                     'lastname' => $request->lastname,
      //                           'email' => $request->email,
      //                           'password' => bcrypt($request->password),
      //                           'title' => $request->title,
      //                           'description' => $request->description,
      //                           'image' => $path,
      //                           'tel' => $request->tel,
      //                           'userRolle' => $request->userRolle,
      //                           'sex' => $request->user_sex,
      //                           'job' => $request->job,
      //                           ]);

      return redirect()->route('admin.employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      //dd($request);
      $employee->delete();
      return redirect()->route('admin.employee.index');
    }
    public function delete(Request $request){
      $backdoor = Users::where('id', $request->id)->where('siteID', Auth::user()->siteID)->delete();
      return redirect()->route('admin.employee.index');
    }
    public function addServices(){
      masterServeces::insert(['master' => $_GET['userid'], 'serveces' => $_GET['servicesid']]);
      return redirect()->route('admin.employee.index');
    }
    public function deleteServices(){
      //dd($_GET);

      masterServeces::where('id',$_GET['servicesid'])->delete();
      return redirect()->route('admin.employee.index');
    }

    public function imageDestroy(){

      Storage::delete($_GET['image']);
      Users::where('image', $_GET['image'])->update(['image' => null]);
        return redirect()->route('admin.company.edit', [$_GET['editurl']]);
    }

    public function profile(Request $request){
      $company = Site::with(['sections' => function($sect){
        $sect->whereIn('slug', ['product', 'services']);
        $sect->orderBy('slug', 'asc');
    }])->with('info')->where('id', Auth::user()->siteID)->first();
      //dd($company);
      return view('admin.profile.index', [
        'company' => $company,
      ]);
    }

    public function profileEdit(Request $request){
      $company = Site::with(['sections' => function($sect){
        $sect->whereIn('slug', ['product', 'services']);
        $sect->orderBy('slug', 'asc');
    }])->with('info')->where('id', Auth::user()->siteID)->first();
      //dd($company);
      return view('admin.profile.edit', [
        'company' => $company,
        'error' => $request->error,
      ]);
    }

    public function profileUpdate(Request $request){
      Users::where('id', Auth::user()->id)->update(['name' => $request->name, 'lastname' => $request->lastname, 'sex' => $request->user_sex, 'email' => $request->email, 'tel' => $request->tel]);
      return redirect()->route('admin.profile');
    }

    public function password(Request $request){

      if (!Hash::check($request->lastpassword, Auth::user()->password)){
        return redirect()->route('admin.profile.edit', ['error' => 1]);
      }
      if($request->newpassword !== $request->confirmpassword){
        return redirect()->route('admin.profile.edit', ['error' => 2]);
      }
      if($request->lastpassword == $request->newpassword){
        return redirect()->route('admin.profile.edit', ['error' => 3]);
      }
      //dd($request->newpassword);
      Users::where('id', Auth::user()->id)->update(['password' => bcrypt($request->newpassword)]);
      return redirect()->route('admin.profile');
    }
}
