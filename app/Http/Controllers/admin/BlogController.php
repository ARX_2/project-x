<?php

namespace App\Http\Controllers\admin;

use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Image;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Users;
use ImageResize;
use App\Site;
use App\Information;
use App\Contact;
use App\Text;
use App\Template;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
       $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
       $blog = Blog::where('siteID', Auth::user()->siteID)->where('published',1)->paginate(10);
       $countShow = Blog::where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
       $countHidden = Blog::where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
       return view('admin.sections.blog.index',[
         'company' => $company,
         'siteID' => Auth::user()->siteID,
         'blogs'=>$blog,
         'countHidden' => $countHidden,
         'countShow' => $countShow
       ]);
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
       $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
       return view('admin.sections.blog.create', [
         'company' => $company,
         'siteID' => Auth::user()->siteID,
         'blog'=> null,
       ]);
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
       if($request->file('file') !== null){
         $value = $request->file('file');
            $path = Storage::putFile('public', $value);
        }
        else{
          $path = null;
        }

       $company = Blog::insert(['title' => $request->title,
                                   'text' => $request->text,
                                   'description' => $request->description,
                                   'published' => 1,
                                   'image' => $path,
                                   'siteID' => Auth::user()->siteID]);

       return redirect()->route('admin.blog.index');
     }

     /**
      * Display the specified resource.
      *
      * @param  \App\News  $news
      * @return \Illuminate\Http\Response
      */
     public function show(News $news)
     {
         //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  \App\News  $news
      * @return \Illuminate\Http\Response
      */
     public function edit(Blog $blog)
     {
       $blog = Blog::where('siteID', Auth::user()->siteID)->where('id', $blog->id)->first();
       $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
       return view('admin.sections.blog.create', [
         'company' => $company,
         'siteID' => Auth::user()->siteID,
         'blog'=> $blog,
       ]);
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  \App\News  $news
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, Blog $blog)
     {
       if($request->file('file') == null){
         $path = $blog->image;
       }
       else{
         $value = $request->file('file');
            $path = Storage::putFile('public', $value);
             Storage::delete($blog->image);
       }
       $doc = Blog::where('id', $blog->id)
       ->where('siteID', Auth::user()->siteID)
       ->update(['title' => $request->title,
                                   'text' => $request->text,
                                   'description' => $request->description,
                                   'image' => $path,]);
         return redirect()->route('admin.blog.index');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  \App\News  $news
      * @return \Illuminate\Http\Response
      */
     public function destroy(Blog $blog)
     {
         //
     }
     public function hide(Request $request){
       $backdoor = Blog::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('published');
       if($backdoor == 0){
         Blog::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 1]);
       }
       elseif($backdoor == 1){
         Blog::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 0]);
       }
       return redirect()->route('admin.blog.index');
     }

     public function main(Request $request){
       $backdoor = Blog::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('main');
       if($backdoor == 0){
         Blog::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['main' => 1]);
       }
       elseif($backdoor == 1){
         Blog::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['main' => 0]);
       }
       return redirect()->route('admin.blog.index');
     }
     public function hidden(){
       $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
       $blog = Blog::where('siteID', Auth::user()->siteID)->where('published',0)->paginate(10);
       $countShow = Blog::where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
       $countHidden = Blog::where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
       return view('admin.sections.blog.index',[
         'company' => $company,
         'siteID' => Auth::user()->siteID,
         'blogs'=>$blog,
         'countHidden' => $countHidden,
         'countShow' => $countShow
       ]);
     }
     public function imageDelete(Request $request){

      $backdoor =  Blog::where('id', $request->id)->where('siteID', auth::user()->siteID)->first();

       if($backdoor !== null){
         Storage::delete($backdoor->image);
         Blog::where('id', $request->id)->update(['image' => null]);
       }
     }
}
