<?php

namespace App\Http\Controllers\admin;

use App\Seo;
use App\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Users;
use App\Company;
use Illuminate\Support\Facades\Auth;
class SeoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin.seo.index', [
        'seos' => Seo::where('siteID', Auth::user()->siteID)->paginate(10),
        'Users' => Users::get(),
        'sites' => DB::table('sites')->get(),
        'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
        'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.seo.create', [
      'seo' => [],
      'seos' => Seo::with('categories'),
      'delimiter' => '',
      'Users' => Users::get(),
      'sites' => DB::table('sites')->get(),
      'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
      'menu' => Menu::where('siteID', Auth::user()->siteID)->first(),
      'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $contact = Seo::create(['locate' => $request->locate, 'title'=> $request->title, 'description' => $request->description, 'keywords' => $request->keywords, 'siteID' => Auth::user()->siteID]);
      //dd($contact);
      // Categories
      return redirect()->route('admin.seo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Seo  $seo
     * @return \Illuminate\Http\Response
     */
    public function show(Seo $seo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Seo  $seo
     * @return \Illuminate\Http\Response
     */
    public function edit(Seo $seo)
    {
      return view('admin.seo.edit', [
    'seo' => $seo,
    'Users' => Users::get(),
    'sites' => DB::table('sites')->get(),
    'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
    'menu' => Menu::where('siteID', Auth::user()->siteID)->first(),
    'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
  ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Seo  $seo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seo $seo)
    {
      Seo::where('siteID', Auth::user()->siteID)->update([
        'locate' => $request->locate, 'title'=> $request->title, 'description' => $request->description, 'keywords' => $request->keywords,
      ]);
      return redirect()->route('admin.seo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Seo  $seo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seo $seo)
    {
      $seo->delete();
      return redirect()->route('admin.seo.index');
    }
}
