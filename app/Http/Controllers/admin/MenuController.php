<?php

namespace App\Http\Controllers\admin;

use App\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use DB;
use App\Users;
use App\Company;
use App\Information;
use App\Template;
use Caching;
class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $menu = Menu::where('siteID', Auth::user()->siteID)->get();
      $info = Information::where('siteID', Auth::user()->siteID)->first();
      if (Cache::has('menu_1_'.Auth::user()->siteID)) {
        Cache::forget('menu_1_'.Auth::user()->siteID);
      }
      $view = '';
      $view .= view('2019.layouts.menu.menu_1', ['menu' => $menu, 'siteID' => Auth::user()->siteID,'info' => $info, 'logo' => Cache::get('logoOne'.Auth::user()->siteID)]);
      //dd($view);
      Cache::forever('menu_1_'.Auth::user()->siteID, $view);

      return view('admin.menu.create', [
      'menu' => $menu,
      'Users' => Users::get(),
      'sites' => DB::table('sites')->get(),
      'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
      'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),

          ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
      $backdoor = Menu::where('id', $menu->id)->where('siteID', Auth::user()->siteID)->first();
      //dd($backdoor);
      //dd($backdoor);
      if($backdoor == null){
        echo 'Меню не найдено';
        die;
      }
        return view('admin.menu.create', [
        'menu' => Menu::where('siteID', Auth::user()->siteID)->get(),
        'Users' => Users::get(),
        'sites' => DB::table('sites')->get(),
        'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
        'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
            ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
      //dd($request);
      $backdoor = Menu::where('id', $menu->id)->where('siteID', Auth::user()->siteID)->first();
      if($backdoor == null){
        echo 'Меню не найдено';
        die;
      }

      foreach ($request['array'] as $id => $m) {
        if(isset($m['slide'])){
          $slide = $m['slide'];
        }
        else{
          $slide = null;
        }
        Menu::where('id', $id)->update(['title'=> $m['menu'], 'slug' => $m['slug'], 'slide2' =>$slide]);
      }
      $i=0;
      if(isset($request['dopmenu'])){
        foreach ($request['dopmenu']['title'] as $value) {
          Menu::insert(['title' => $value, 'slug' => $request['dopmenu']['slug'][$i], 'siteID' => Auth::user()->siteID]);
          $i++;
        }
      }
      Caching::categoryNull();
        return redirect()->route('admin.menu.create');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        //
    }
    public function insert(Request $request){


        $image = Menu::create(['menu1' => $request->menu1, 'menu2' => $request->menu2, 'menu3' => $request->menu3, 'menu4' => $request->menu4,
      'menu5' => $request->menu5, 'menu6' => $request->menu6, 'slide2' => 0, 'slide2' => 0, 'siteID' => Auth::user()->siteID]);
        return redirect()->route('admin.menu.create');

    }

    public function slide(Request $request){

      $menu = Menu::where('siteID', Auth::user()->siteID)->first();
      if($menu !== null){
        Menu::where('siteID', Auth::user()->siteID)->update(['slide2' => $request->goods, 'slide3' => $request->services]);
      }
      else{
          Menu::create(['slide2' => $request->goods, 'slide3' => $request->services, 'siteID', Auth::user()->siteID]);
      }
      return redirect()->route('admin.index');
    }
}
