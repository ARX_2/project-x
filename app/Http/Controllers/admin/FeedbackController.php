<?php

namespace App\Http\Controllers\admin;

use App\Feedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Goods;
use Resize;
use App\Image;
use Mail;
use Illuminate\Support\Facades\Cache;
class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/feedback/hidden'){
      $feedbacks = Feedback::where('siteID', Auth::user()->siteID)->where('published',0)->with('images')->paginate(17);
      }
      else{
      $feedbacks = Feedback::where('siteID', Auth::user()->siteID)->where('published',1)->with('images')->paginate(17);
      }
      $countShow = Feedback::where('siteID', Auth::user()->siteID)->where('published', 1)->count();
      $countHide = Feedback::where('siteID', Auth::user()->siteID)->where('published', '!=', 1)->count();
      //dd($banner->first());

      return view('admin.sections.reviews.index',[
        'company' => Resize::company(),
        'siteID' => Auth::user()->siteID,
        'feedbacks' => $feedbacks,
        'countActive' => $countShow,
        'countHide' => $countHide,

      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view('admin.sections.reviews.edit', ['feedback' => null, 'company' => Resize::company()]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($param,Request $request)
    {
      $description = $request->description;

      $pos1 = strpos($description, 'http');
      if($pos1 === false){

      }
      else{
        return redirect()->back();
      }
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
      if($request->file('ava') !== null){
        $type = mime_content_type((string)$request->file('ava'));
        if($type == 'image/jpeg' || $type == 'image/png'){
          $path = Storage::putFile('public', $request->file('ava'));
        }
        else{
          $path = null;
        }
      }
      else{
        $path = null;
      }
      $year = rand(2018, date('Y'));
      if($year == date('Y') || $year == date('Y')){
          $maxmonth = date('m');
      }
      else{
          $maxmonth = '12';
      }
      $month = rand(1,$maxmonth);
             if($month == 1 || $month == 3 || $month == 5 || $month == 7 || $month == 8 || $month == 10 || $month == 12){
                 $day = rand(1,31);
                 if($day<10){
                     $day = '0'.$day;
                 }
             }
             elseif($month == 2){
                 $day = rand(1,28);
                 if($day<10){
                     $day = '0'.$day;
                 }

             }
             else{
                 $day = rand(1,30);
                 if($day<10){
                     $day = '0'.$day;
                 }
             }
             if($month<10){
                 $month = '0'.$month;
             }

             $hour = rand(9, 20);
             if($hour<10){
                 $hour = '0'.$hour;
             }
             $min = rand(1, 59);
             if($min<10){
                 $min = '0'.$min;
             }
             $sec = '00';
             $date = $year.'-'.$month.'-'.$day.' '.$hour.':'.$min.':'.$sec;
      $update['category'] = ['name' => $request->name, 'description' => $request->description, 'image' => $path, 'siteID' => $siteID, 'category' => $param, 'published' =>1, 'created_at' => $date];
      $update['goods'] = ['name' => $request->name, 'description' => $request->description, 'image' => $path, 'siteID' => $siteID, 'goods' => $param, 'published' =>1, 'created_at' => $date];
      $update['section'] = ['name' => $request->name, 'description' => $request->description, 'image' => $path, 'siteID' => $siteID, 'section' => $param, 'published' =>1, 'created_at' => $date];
      $id = Feedback::insertGetId($update[$request->type]);
      if($request->file('files') !== null){
        foreach($request->file('files') as $img){
          $image = Storage::putFile('public', $img);
          Image::insertGetId(['feedback' => $id, 'images' => $image, 'siteID' => $siteID]);
        }
      }
      Mail::send('mail_feedback', ['to' => $request->getHttpHost(), 'description' => $request->description, 'id' => $id], function($message) use($id, $request){
          $message->to('dmitrypovyshev@yandex.ru', 'to web')->cc('Pavellipatkin@yandex.ru');;
          $message->from('lara@yandex.ru', $request->getHttpHost())->subject('Новый отзыв на '.$request->getHttpHost());
      });

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function show(Feedback $feedback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedback $feedback)
    {
        $feedback = Feedback::where('id', $feedback->id)->where('siteID', Auth::user()->siteID)->with('images')->first();
        return view('admin.sections.reviews.edit', ['feedback' => $feedback, 'company' => Resize::company()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function update($id ,Request $request)
    {
      $id = Feedback::where('id', $id)->where('siteID', Auth::user()->siteID)->value('id');
      if($id == null){
        return redirect()->route('admin.feedback.index');
      }
      if($request->file('ava') !== null){
        $path = Storage::putFile('public', $request->file('ava'));
      }
      else{

        $path = Feedback::where('id', $id)->value('image');
      }
      if($request->file('files') !== null){
      foreach($request->file('files') as $img){
        $image = Storage::putFile('public', $img);
        Image::insertGetId(['feedback' => $id, 'images' => $image, 'siteID' => Auth::user()->siteID]);
      }
    }
      Feedback::where('id', $id)->update(['name' => $request->name, 'description' => $request->text, 'image' => $path]);
        return redirect()->route('admin.feedback.index');
    }

    public function published($id, $param ,Request $request)
    {
      $unparametr[0] = 1;
      $unparametr[1] = 0;
      Feedback::where('id', $id)->where('siteID', Auth::user()->siteID)->update(['published' => $unparametr[$param]]);
      if($request->mail !== null){
        return redirect()->back();
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Feedback::where('siteID', Auth::user()->siteID)->where('id', $id)->value('image');
        if($image !== null){
          Storage::delete($image);
        }
        Feedback::where('siteID', Auth::user()->siteID)->where('id', $id)->delete();
        return redirect()->back();
    }
    public function imageDestroy(Request $request){
      Feedback::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['image' => null]);
    }
}
