<?php

namespace App\Http\Controllers\admin;

use App\Goods;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Users;
use App\Image;
use App\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Company;
use ImageResize;
use App\Site;
use App\Property;
use WebP;
use App\Subcategory;
use App\ExcelUpload;
use App\Goods_object;
use App\Goods_product;
use App\Model\Goods_doma as Dom;
use Resize;
//use App\CategoryGoods;
use Caching;

class BuildingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $urlActive = route('admin.stroitelstvo.index');
      $urlHidden = route('admin.stroitelstvo.hidden');
      $urlCreate = route('admin.stroitelstvo.create');
      //dd('asd');
      // $categories  = Goods::with('category')->where('siteID', 16)->get();
      // dd($categories);

      //dd($categories);
    //dd($company);
    //$categories = Category::where('siteId', $request->siteID)->get();
    Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();

    if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/stroitelstvo/hidden'){
      //$goods = Goods::with('images')->with('subcategory')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);

      $goods = Goods::with('subcategory')->with('doma')->whereHas('categories', function($query){
        $query->where('type', 4);
        $query->orWhereHas('categor', function($sub){
          $sub->where('type',4);
        });
      })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

      }
      elseif(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/stroitelstvo/ShowDeleted'){
        //$goods = Goods::with('images')->with('subcategory')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);

        $goods = Goods::with('subcategory')->whereHas('categories', function($query){
          $query->where('type', 4);
          $query->orWhereHas('categor', function($sub){
            $sub->where('type',4);
          });
        })->where('siteID', Auth::user()->siteID)->where('deleted', 1);

        }
      else{
        $goods = Goods::with('subcategory')->with('doma')->whereHas('categories', function($query){
          $query->where('type', 4);
          $query->orWhereHas('categor', function($sub){
            $sub->where('type',4);
          });
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);


      }
      //$categories = Category::where('siteID', Auth::user()->siteID)->with('images')->where('published', 1)->orderBy('sort', 'asc')->get();
      //dd($goods->get());

      //dd($goods->get());

      $product = $goods->orderBy('updated_at', 'desc')->paginate(40);

    $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
      $query->where('type', 4);
      $query->orWhereHas('categor', function($sub){
        $sub->where('type',4);
      });
    })->where('published', 1)->count('id');

    $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
      $query->where('type', 4);
      $query->orWhereHas('categor', function($sub){
        $sub->where('type',4);
      });
    })->where('published', 0)->where('deleted', 0)->count('id');

    $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
      $query->where('type', 4);
      $query->orWhereHas('categor', function($sub){
        $sub->where('type',4);
      });
    })->where('deleted', 1)->count('id');
    //dd($product);
    //dd($product[0]->images);
    //dd($categories);

    //dd($type);
    //dd($urlCreate);
    $view =  '';
    if($request->ajax == null){
      $view .= view('admin.index', ['company' => Resize::company(),]);
    }
    $view .= view('admin.stroitelstvo.index',[
      'product' => $product,
      'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
      'countActive' => $countActive,
      'countHide' => $countHide,
      'countDeleted' => $countDeleted,
      'categories' => Category::with('subcategories')->where('siteID', Auth::user()->siteID)->where('type',4)->where('parent_id',null)->get(),
      //'type' => $type,
      'urlActive' => $urlActive,
      'urlHidden' => $urlHidden,
      'urlCreate' => $urlCreate,
      'link' => $_SERVER['REQUEST_URI'],
    ]);
    return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create(Request $request)
     {
       //$product = Goods::with('subcategoriesAll')->with('properties')->where('id', $request->id)->first();
       //dd($product);
         $category = Category::with('subcategoriesAll')->where('type',4)->where('published',1)->where('siteID', auth::user()->siteID)->get();
         $goods =  Goods::where('status',1)->where('siteID', Auth::user()->siteID)->first();
         if($goods == null){
           $id = Goods::insertGetId(['status' => 1, 'siteID' => Auth::user()->siteID,'created_at' => date('Y-m-d H:i:s')]);
           Goods::where('id', $id)->update(['articul' => $id]);
           $goods = Goods::where('id',$id)->first();
         }
       //dd($product);
       $view =  '';
       if($request->ajax == null){
         $view .= view('admin.index', ['company' => Resize::company(),]);
       }
     //dd($product);
     $view .= view('admin.stroitelstvo.create',[
       'categories' => $category,
       'siteID' => $request->siteID,
       'product' => $goods,
     ]);
     return $view;
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Goods  $goods
     * @return \Illuminate\Http\Response
     */
    public function show(Goods $goods)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Goods  $goods
     * @return \Illuminate\Http\Response
     */
     public function edit(Goods $goods, Request $request)
     {
       $backdoor = Goods::where('siteID', auth::user()->siteID)->where('id', $request->id)->value('id');
       $protected = Goods::where('siteID', auth::user()->siteID)->where('id', $request->id)->value('protected');
       if($backdoor == null || $protected == 1 && Auth::user()->userRolle == 6){
         return redirect()->route('admin.goods.index');
       }
       $product = Goods::with('subcategory')->with('imageses')->where('id', $request->id)->first();
       //dd($product->images);
         $category = Category::with('subcategories')->where('type',4)->where('published',1)->where('siteID', auth::user()->siteID)->get();
         //dd($product);
         $view = '';
         if($request->ajax == null){
           $view .= view('admin.index', ['company' => Resize::company(),]);
         }
       //dd($product);

       $view .= view('admin.stroitelstvo.create',[
         'categories' => $category,
         'product' =>  $product,
         'siteID' => $request->siteID,

       ]);
       return $view;
       //  dd($goods);
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Goods  $goods
     * @return \Illuminate\Http\Response
     */
     public function update($slug,$id,$parametr,Request $request, Goods $goods)
     {
       if($request->subcategory !== 0 && $request->subcategory !== null){
         $category = $request->subcategory;
       }
       else{
         $category = $request->category;
       }
       $unparametr[$parametr] = $parametr;
       $unparametr[0] = 1;
       $unparametr[1] = 0;
       if($slug == 'title'){
         $cat = Goods::where('id', $id)->value('status');
         if($cat == 1){
           $update['title'] = ['title' => $parametr, 'slug' => str_slug($parametr, "-"), 'created_at' => date('Y-m-d H:i:s')];
         }
         else{
           $update['title'] = ['title' => $parametr, 'updated_at' => date('Y-m-d H:i:s')];
         }
       }
       $update['published'] = ['published' => $unparametr[$parametr], 'deleted' => 0, 'dateDelete' => null, 'updated_at' => date('Y-m-d H:i:s')];
       $update['main'] = ['main' => $unparametr[$parametr], 'updated_at' => date('Y-m-d H:i:s')];
       $update['deleted'] = ['deleted' => $unparametr[$parametr], 'updated_at' => date('Y-m-d H:i:s'), 'dateDelete' => \carbon\Carbon::now()->addDays(30)->toDateTimeString()];

       $update['coast'] = ['coast' => $parametr];
       $update['category'] = ['category' => $parametr];
       $update['checkseo'] = ['checkseo' => $parametr];
       $update['seoname'] = ['seoname' => $parametr];
       $update['seotitle'] = ['seotitle' => $parametr];
       $update['seodescription'] = ['seodescription' => $parametr];
       $update['seokeywords'] = ['seokeywords' => $parametr];
       $update['type_cost'] = ['type_cost' => $parametr];
       $update['coast'] = ['coast' => $parametr];
       $update['new_cost'] = ['new_cost' => $parametr];
       $update['percent'] = ['percent' => $parametr];
       $update['update'] = ['title' => $request->title, 'short_description' => $request->short_description,
           'coast' => $request->coast, 'youtube' => $request->youtube, 'seoname' => $request->seoname,
           'seotitle' => $request->seotitle, 'seodescription' => $request->seodescription, 'seokeywords' => $request->seokeywords,
           'checkseo' => $request->checkseo, 'category' => $category,];


     $result =  Goods::with('categories')->where('id', $id)->where('siteID', Auth::user()->siteID)->update($update[$slug]);
       //dd($request->file('file'));
       if($request->file('file') !== null && $result == 1){
       foreach ($request->file('file') as $img) {
         $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
         ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
         Image::create(['images' => 'public/'.$filename, 'goods' => $request->id, 'siteID' => Auth::user()->siteID]);
       }
       }


         //return redirect()->route('admin.goods.index');


     }

     public function subUpdate($slug,$id,$parametr,Request $request, Goods $goods){
      $domid = Dom::where('p_g_id', $id)->value('id');
      if($domid == null){
        $domid = Dom::insertGetId(['p_g_id' => $id]);
      }
      $unparametr[$parametr] = $parametr;
      $unparametr[0] = 1;
      $unparametr[1] = 0;

      $update['area'] = ['area' => $parametr];
      $update['living_area'] = ['living_area' => $parametr];
      $update['floors'] = ['floors' => $parametr];
      $update['bedroom'] = ['bedroom' => $parametr];
      $update['dimensions'] = ['dimensions' => $parametr];
      $update['building_period'] = ['building_period' => $parametr];
      $update['garage'] = ['garage' => $unparametr[$parametr]];
      $update['terrace'] = ['terrace' => $unparametr[$parametr]];
      $update['balcony'] = ['balcony' => $unparametr[$parametr]];
      $update['veranda'] = ['veranda' => $unparametr[$parametr]];
      $update['oriel'] = ['oriel' => $unparametr[$parametr]];
      $update['baths'] = ['baths' => $unparametr[$parametr]];

      Dom::where('id', $domid)->update($update[$slug]);
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Goods  $goods
     * @return \Illuminate\Http\Response
     */
    public function destroy(Goods $goods)
    {
        //
    }

    public function CategoryGoods($category,Request $request){


      //dd($type);
      $company = Resize::company();

      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();


      //dd($goods->get());
      //dd(explode('?',$_SERVER['REQUEST_URI'])[0]);
      if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/stroitelstvo/'.$category){
        //dd('111');
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($category){
          $query->where('id', $category);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);

      }
      else{
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($category){
          $query->where('id', $category);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

      }


      $product = $goods->paginate(40);

    //  dd($product);

      $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('id', $category);
      })->where('published', 1)->count('id');

      $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('id', $category);
      })->where('published', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('id', $category);
      })->where('deleted', 1)->count('id');
      //dd($product);
      //dd($product[0]->images);
      //dd($categories);


      $urlActive = '/admin/category/stroitelstvo/'.$category;
      $urlHidden = '/admin/category/stroitelstvo/hidden/'.$category;
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }

      $view .= view('admin.stroitelstvo.index',[
        'company' => $company,
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::where('siteID', Auth::user()->siteID)->get(),
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden
        //'type' => $type,
      ]);
      return $view;
      //dd($request);
    }

    public function CategoryGoodsAll($category,Request $request){


      //dd($type);
      $company = Resize::company();
      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();

      //dd($categories);


      //dd($goods->get());

      if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/stroitelstvo/all/'.$category){
        //dd('111');
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($category){
          $query->where('parent_id', $category);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);
      }
      else{
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($category){
          $query->where('parent_id', $category);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

      }


      $product = $goods->paginate(40);

      //dd($product);
      $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('parent_id', $category);
      })->where('published', 1)->count('id');
      $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('parent_id', $category);
      })->where('published', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('parent_id', $category);
      })->where('deleted', 1)->count('id');
      //dd($product);
      //dd($product[0]->images);
      //dd($categories);

      $urlActive = '/admin/category/stroitelstvo/all/'.$category;
      $urlHidden = '/admin/category/stroitelstvo/all/hidden/'.$category;
      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
      $view .= view('admin.stroitelstvo.index',[
        'company' => $company,
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::where('siteID', Auth::user()->siteID)->get(),
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden
        //'type' => $type,
      ]);
      return $view;
      //dd($request);
    }

    public function SubcategoryGoods($subcategory, Request $request){
        //dd($subcategory);
      $company = Resize::company();
      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();



        //dd($categoryGoods);
        if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/subcategory/stroitelstvo/'.$subcategory){
          $goods = Goods::with('subcategory')->where('category', $subcategory)->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);
        }
        else{
          $goods = Goods::with('subcategory')->where('category', $subcategory)->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);
        }
        //$categories = Category::where('siteID', Auth::user()->siteID)->with('images')->where('published', 1)->orderBy('sort', 'asc')->get();
        //dd($goods->get());

        $product = $goods->paginate(40);

      //dd($product);
      $countActive = Goods::where('siteID', Auth::user()->siteID)->where('category', $subcategory)->where('published', 1)->count('id');

      $countHide = Goods::where('siteID', Auth::user()->siteID)->where('category', $subcategory)->where('published', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->where('category', $subcategory)->where('deleted', 1)->count('id');

      //dd($categoryType);

      //dd($type);
      $urlActive = '/admin/subcategory/stroitelstvo/'.$subcategory;
      $urlHidden = '/admin/subcategory/stroitelstvo/hidden/'.$subcategory;

      $view =  '';
      if($request->ajax == null){
        $view .= view('admin.index', ['company' => Resize::company(),]);
      }
      $view .= view('admin.stroitelstvo.index',[
        'company' => $company,
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::where('siteID', Auth::user()->siteID)->get(),
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden,

        //'type' => $type,
      ]);
      return $view;
    }

    public function description(Request $request){

      Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['short_description' => $request->short_description,'status' => 2, 'updated_at' => date('Y-m-d H:i:s'), 'published' => 1]);
      $request['ajax'] = 'true';
      return  self::index($request);
    }

    public function changeTitleSites(Request $request){

      Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['short_description' => $request->short_description,'status' => 2, 'updated_at' => date('Y-m-d H:i:s'), 'published' => 1]);
      $goods = Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->first();
      Goods::where('articul', $goods->articul)->update(['title' => $goods->title, 'short_description' => $goods->short_description, 'coast' => $goods->coast, 'youtube' => $goods->youtube,]);
      $request['ajax'] = 'true';
      return  self::index($request);
    }
}
