<?php


namespace App\Http\Controllers\admin;


use App\Biz;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use App\Information;
use App\Site;
use App\Goods;
use App\Email;
use Mail;
use Resize;

class BizController extends Controller
{
    public function index(Request $request)
    {

        $view =  '';
        if($request->ajax == null){
            $view .= view('admin.index', ['company' => Resize::company(),]);
        }
        $view .= view('admin.biz.index',['siteID' => Auth::user()->siteID]);
        return $view;
    }

    public function show(){

        return view('admin.biz.modal');
    }
}