<?php

namespace App\Http\Controllers\Admin;

use App\Opinion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Users;
use App\Company;
class OpinionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin.opinion.index', [
        'Opinions' => Opinion::where('siteID', Auth::user()->siteID)->paginate(10),
        'Users' => Users::get(),
        'sites' => DB::table('sites')->get(),
        'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
        'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $path = Storage::putFile('public', $request->file('image'));
      Opinion::insert(['name' => $request->name, 'text' => $request->text, 'image' => $path, 'published' => $request->published, 'siteID' => Auth::user()->siteID]);
      return redirect()->route('admin.opinion.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Opinion  $opinion
     * @return \Illuminate\Http\Response
     */
    public function show(Opinion $opinion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Opinion  $opinion
     * @return \Illuminate\Http\Response
     */
    public function edit(Opinion $opinion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Opinion  $opinion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Opinion $opinion)
    {
      if($request->file('image') !== null){
        $path = Storage::putFile('public', $request->file('image'));
        Storage::delete($opinion->image);
      }
      else{
        $path = $opinion->image;
      }

        Opinion::where('id', $opinion->id)->update(['name' => $request->name, 'text' => $request->text, 'published' => $request->published, 'image' => $path]);
          return redirect()->route('admin.opinion.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Opinion  $opinion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Opinion $opinion)
    {
      Opinion::where('id', $opinion->id)->delete();
      Storage::delete($opinion->image);
      return redirect()->route('admin.opinion.index');
    }
}
