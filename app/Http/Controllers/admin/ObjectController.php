<?php

namespace App\Http\Controllers\Admin;

use App\Objects;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use DB;
use App\Users;
use App\Category;
use App\Company;
use ImageResize;
use App\Site;
use App\Image;
use Caching;
use Resize;
class ObjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $company = Resize::company();
    $countShow = Objects::where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
    $countHidden = Objects::where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
      $objects = Objects::where('siteID', Auth::user()->siteID)->with('images')->with('category')->where('published',1)->with('imageResize')->orderby('id', 'desc')->get();
      return view('admin.sections.object.index',[
        'company' => $company,
        'countHidden' => $countHidden,
        'countShow' => $countShow,
        'siteID' => Auth::user()->siteID,
        'objects' => $objects,
      ]);
    }

    public function hidden()
    {
        $company = Resize::company();
        $countShow = Objects::where('siteID', Auth::user()->siteID)->where('published',1)->count('id');
        $countHidden = Objects::where('siteID', Auth::user()->siteID)->where('published',0)->count('id');
        $objects = Objects::where('siteID', Auth::user()->siteID)->with('images')->with('category')->where('published',0)->with('imageResize')->orderby('id', 'desc')->get();
        return view('admin.sections.object.hidden',[
            'company' => $company,
            'countHidden' => $countHidden,
            'countShow' => $countShow,
            'siteID' => Auth::user()->siteID,
            'objects' => $objects,
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      //dd($request);
      $company = Resize::company();
      return view('admin.sections.object.create',[
        'company' => $company,
        'object' => null,
        'categories' => Category::with('subcategoriesAll')->where('siteID', Auth::user()->siteID)->where('parent_id',null)->where('published', 1)->get(),
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     $objectid =  Objects::insertGetId(['title' => $request->title, 'offer' => $request->offer, 'text' => $request->text, 'parent_id' => $request->parent_id, 'published' => 1, 'main' =>1, 'siteID' => Auth::user()->siteID, 'slug' => str_slug($request->title, "-"),'type' => 1, 'created_at' => date('Y-m-d H:i:s')]);
    Objects::where('id', $objectid)->update(['articul' => $objectid]);
      if($request->file('image') !== null){
        foreach ($request->file('image') as $value) {
          $filename  = uniqid() . '.' . $value->getClientOriginalExtension();

        ImageResize::make($value->getRealPath())->save('storage/app/public/'. $filename);
          $path = 'public/'.$filename;
          Image::insert(['images' => $path, 'object' => $objectid, 'siteID' => Auth::user()->siteID]);
        }
      }


      return redirect()->route('admin.object.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Object  $object
     * @return \Illuminate\Http\Response
     */
    public function show(Objects $objects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Object  $object
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Objects $objects)
    {
      $object = Objects::with('images')->where('id', $request->id)->where('siteID', Auth::user()->siteID)->first();
      //dd($object);
      $company = Resize::company();
      //dd($request);
      return view('admin.sections.object.create',[
        'company' => $company,
        'object' => $object,
        'categories' => Category::where('siteID', Auth::user()->siteID)->where('published', 1)->get(),
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Object  $object
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Objects $object)
    {
      $backdoor = Objects::where('id', $object->id)->where('siteID', Auth::user()->siteID)->first();
      //dd($backdoor);
      if($backdoor == null){
        echo 'Товар не найден';
        die;
      }

      if($request->file('image') !== null){

        foreach ($request->file('image') as $value) {
          $filename  = uniqid() . '.' . $value->getClientOriginalExtension();
        ImageResize::make($value->getRealPath())->save('storage/app/public/'. $filename);
          $path = 'public/'.$filename;
          Image::insert(['images' => $path, 'object' => $object->articul, 'siteID' => Auth::user()->siteID]);
        }


      }
      // /dd($path);
        Objects::where('id', $object->id)->update(['title' => $request->title, 'offer' => $request->offer, 'parent_id' => $request->parent_id, 'text' => $request->text]);
        // Caching::objects();
          return redirect()->route('admin.object.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Object  $object
     * @return \Illuminate\Http\Response
     */
    public function destroy(Objects $object)
    {
      $backdoor = Objects::where('id', $object->id)->where('siteID', Auth::user()->siteID)->first();
      //dd($backdoor);
      if($backdoor == null){
        echo 'Товар не найден';
        die;
      }
      Objects::where('id', $object->id)->delete();
      Storage::delete($object->image);

      $images = DB::table('image')->where('image', $object->image)->get();
      //dd($images);
      foreach ($images as $i) {
        DB::table('image')->where('image', $_GET['image'])->delete();
        Storage::delete($i->fullimagesize);
      }
      return redirect()->route('admin.object.index');
    }
    /*public function hide(Request $request){
      $backdoor = Objects::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('published');
      if($backdoor == 0){
        Objects::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 1]);
      }
      elseif($backdoor == 1){
        Objects::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 0]);
      }
      return redirect()->route('admin.object.index');
    }*/
    public function main(Request $request){
      $backdoor = Objects::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('main');
      if($backdoor == 0){
        Objects::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['main' => 1]);
      }
      elseif($backdoor == 1){
        Objects::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['main' => 0]);
      }
      return redirect()->route('admin.object.index');
    }
    public function type(Request $request){
      $backdoor = Objects::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('type');
      if($backdoor == 1){
        Objects::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['type' => 2]);
      }
      elseif($backdoor == 2){
        Objects::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['type' => 1]);
      }

      return redirect()->route('admin.object.index');
    }

    public function updatex($slug,$id,$parametr,Request $request, Objects $object)
    {

        $unparametr[$parametr] = $parametr;
        $unparametr[0] = 1;
        $unparametr[1] = 0;
        $update['published'] = ['published' => $unparametr[$parametr], 'updated_at' => date('Y-m-d H:i:s')];
        $update['main'] = ['main' => $unparametr[$parametr], 'updated_at' => date('Y-m-d H:i:s')];

        $result =  Objects::where('id', $id)->where('siteID', Auth::user()->siteID)->update($update[$slug]);

        return $result;

        return redirect()->route('admin.object.index');
    }
}
