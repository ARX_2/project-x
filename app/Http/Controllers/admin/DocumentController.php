<?php

namespace App\Http\Controllers\Admin;

use App\Documentation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Image;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Users;
use ImageResize;
use App\Site;
use App\Information;
use App\Contact;
use App\Text;
use App\Template;
class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      $docs = Documentation::where('siteID', Auth::user()->siteID)->paginate(10);
      return view('admin.sections.documentation.index',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'docs'=>$docs,

      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      return view('admin.sections.documentation.create',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'doc'=> null,

      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->file('file') !== null){


        $value = $request->file('file');
           $path = Storage::putFile('public', $value);
           $size = round(Storage::size($path)/1024/1024, 1);

       }
      $doc = Documentation::insert(['title' => $request->title,
                                    'type' => $request->type,
                                    'doc' => $path,
                                    'size' => $size,
                                  'created_by' => date('Y-d-m'),
                                  'siteID' => Auth::user()->siteID]);
        return redirect()->route('admin.documentation.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Documentation  $documentation
     * @return \Illuminate\Http\Response
     */
    public function show(Documentation $documentation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Documentation  $documentation
     * @return \Illuminate\Http\Response
     */
    public function edit(Documentation $documentation)
    {
      $documentation = Documentation::where('siteID', Auth::user()->siteID)->where('id', $documentation->id)->first();
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      return view('admin.sections.documentation.create',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'doc'=> $documentation,

      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Documentation  $documentation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Documentation $documentation)
    {
        if($request->file('file') == null){
          $path = $documentation->doc;
        }
        else{
          $value = $request->file('file');
          $path = Storage::putFile('public', $value);
          Storage::delete($documentation->doc);
        }
        $doc = Documentation::where('id', $documentation->id)
        ->where('siteID', Auth::user()->siteID)
        ->update([
            'title' => $request->title,
            'type' => $request->type,
            'doc' => $path,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
          return redirect()->route('admin.documentation.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Documentation  $documentation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Documentation $documentation)
    {
        $documentation = Documentation::where('id', $documentation->id)->where('siteID', auth::user()->siteID)->first();
        if($documentation !== null){
          Documentation::where('id', $documentation->id)->where('siteID', auth::user()->siteID)->delete();
          Storage::delete($documentation->doc);
        }
        return redirect()->route('admin.documentation.index');
    }

    public function updatex($slug,$id,$parametr,Request $request, Documentation $documentation)
    {

        $unparametr[$parametr] = $parametr;
        $unparametr[0] = 1;
        $unparametr[1] = 0;
        $update['popular'] = ['popular' => $unparametr[$parametr], 'updated_at' => date('Y-m-d H:i:s')];

        $result =  Documentation::where('id', $id)->where('siteID', Auth::user()->siteID)->update($update[$slug]);

        return $result;

        return redirect()->route('admin.documentation.index');
    }


}
