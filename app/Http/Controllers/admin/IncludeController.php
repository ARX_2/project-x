<?php

namespace App\Http\Controllers\admin;

use App\Includes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Site;
class IncludeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd($request);
      $id = Site::where('title', $request->getHttpHost())->value('id');
      if(isset($request->update)){
        Includes::where('template', $request->template)->where('locate', $request->locate)->update(['catalog' => $request->catalog, 'title' => $request->include, 'siteID' => $id]);
      }
      else{
        Includes::insert(['template' => $request->template, 'locate' => $request->locate, 'catalog' => $request->catalog, 'title' => $request->include, 'siteID' => $id]);
      }
      $includes = Includes::where('template', $request->template)->get();
      if (Cache::has('includes'.$request->template)) {
        Cache::forget('includes'.$request->template);
      }
      Cache::forever('includes'.$request->template, $includes);

      return redirect($request->slug);
        //dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Includes  $includes
     * @return \Illuminate\Http\Response
     */
    public function show(Includes $includes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Includes  $includes
     * @return \Illuminate\Http\Response
     */
    public function edit(Includes $includes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Includes  $includes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Includes $includes)
    {
        //dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Includes  $includes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Includes $includes, Request $request)
    {
      Includes::where('id', $request->id)->delete();
      return redirect($request->slug);
    }
}
