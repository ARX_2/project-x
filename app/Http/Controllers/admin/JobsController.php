<?php

namespace App\Http\Controllers\admin;

use App\Jobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Users;
use App\Company;
use ImageResize;
use App\Site;
use Resize;
class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $company = Resize::company();
      //$sites = Site::get();
      //dd($company);
      //dd($company);
      return view('admin.sections.jobs.index',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        'jobs' => jobs::where('siteID', Auth::user()->siteID)->get(),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = Resize::company();
        return view('admin.sections.jobs.create',[
          'company' => $company,
          'job' => null
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->file('image') !== null){
        $value = $request->file('image');
        $filename  = uniqid() . '.' . $value->getClientOriginalExtension();

      ImageResize::make($value->getRealPath())->save('storage/app/public/'. $filename);
      $path = 'public/'.$filename;
      }
      else{
        $path = null;
      }
      Jobs::insert(['title' => $request->title, 'text' => $request->text,  'image' => $path, 'pay' => $request->pay, 'published' => 1,'siteID' => Auth::user()->siteID]);
      return redirect()->route('admin.jobs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jobs  $jobs
     * @return \Illuminate\Http\Response
     */
    public function show(Jobs $jobs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jobs  $jobs
     * @return \Illuminate\Http\Response
     */
    public function edit(Jobs $jobs, Request $request)
    {

      $company = Site::with('sections')->with('info')->where('id', Auth::user()->siteID)->first();
      return view('admin.sections.jobs.create',[
        'company' => $company,
        'job' => Jobs::where('id', $request->id)->where('siteID', Auth::user()->siteID)->first(),
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jobs  $jobs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jobs $jobs)
    {
      $backdoor = Jobs::where('id', $request->id)->where('siteID', Auth::user()->siteID)->first();
      //dd($backdoor);
      if($backdoor == null){
        echo 'Товар не найден';
        die;
      }
      if($request->file('image') !== null){
        $value = $request->file('image');
        $filename  = uniqid() . '.' . $value->getClientOriginalExtension();

      ImageResize::make($value->getRealPath())->save('storage/app/public/'. $filename);
        Storage::delete($backdoor->image);
        $path = 'public/'.$filename;
      }
      else{
        $path = $backdoor->image;
      }
      // /dd($path);
        Jobs::where('id', $backdoor->id)->update(['title' => $request->title, 'text' => $request->text, 'image' => $path]);
          return redirect()->route('admin.jobs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jobs  $jobs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jobs $jobs)
    {
        //
    }
    public function hide(Request $request){
      $backdoor = Jobs::where('id', $request->id)->where('siteID', Auth::user()->siteID)->value('published');
      if($backdoor == 0){
        Jobs::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 1]);
      }
      elseif($backdoor == 1){
        Jobs::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['published' => 0]);
      }
      return redirect()->route('admin.jobs.index');
    }

    public function imageDelete(Request $request){

      $backdoor =  Jobs::where('id', $request->id)->where('siteID', auth::user()->siteID)->first();

       if($backdoor !== null){
         Storage::delete($backdoor->image);
         Jobs::where('id', $request->id)->update(['image' => null]);
       }
    }
}
