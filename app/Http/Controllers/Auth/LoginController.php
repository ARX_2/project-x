<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Users;
use App\Site;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      if(isset($_POST['email'])){
        $email = $_POST['email'];

        if(substr($email, 0,1) == '+'){
          if (Auth::attempt(['tel' => $email, 'password' => $_POST['password']])) {
            return redirect('/admin/company');
            }
        }
        else{
          if (Auth::attempt(['email' => $email, 'password' => $_POST['password']])) {
              if(Auth::user()->userRolle == 1){
                $siteID = Site::where('title', $_SERVER['SERVER_NAME'])->value('id');
                Users::where('id', Auth::user()->id)->update(['siteiD' => $siteID]);
              }
            }

          $this->middleware('guest')->except('logout');
        }
      }
        $this->middleware('guest')->except('logout');
    }
}
