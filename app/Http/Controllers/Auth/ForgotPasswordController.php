<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //dd($_POST);
      if(isset($_POST['email'])){
        $email = $_POST['email'];

        if(substr($email, 0,1) == '+'){
          if (Auth::attempt(['tel' => $email, 'password' => $_POST['password']])) {
            return redirect('/admin/company');
            }
        }
        else{
          $this->middleware('guest')->except('logout');
        }
      }
        $this->middleware('guest');
    }
}
