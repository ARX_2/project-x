<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 13.03.2019
 * Time: 14:47
 */

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Cities;
use App\Model\Documentations;
use App\Model\Feedback;
use App\Model\Goods;
use App\Model\Portfolio;
use App\Model\Settings;
use App\Model\Information;
use App\Model\Users;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Caching;
use Resize;


class ServicesController extends Controller
{
    public function index(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::with('templates')->where('title', $request->getHttpHost())->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::with('section_image_one')->where('siteID', $siteID)->where('type', 5)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Левое меню - получаем главные категории со связкой подкатегорий, далее связка с goods выводит услуги
        $category_nav = Category::with('sub_category_nav')->where('siteID', $siteID)->where('parent_id', null)->where('type', 1)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Вывод услуг + связка, которая достает url категории
        $goods = Goods::with('images')->with('product_url_cat')->whereHas('product_url_cat', function($query){$query->where('published', 1)->where('type', 1);})->where('siteiD', $siteID)->where('published', 1)->paginate(12);
        // отзывы
        $reviews = Feedback::with('imageses')->where('siteID', $siteID)->where('section', 1)->where('published', 1)->limit(6)->get();
        /// DOCTYPE
        if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Услуги';}
        if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
        if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
        $canonical = $request->getSchemeAndHttpHost().'/services';
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'jq' => 1, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.services.index_1', ['settings' => $settings, 'reviews' => $reviews,'category_nav' => $category_nav, 'documents' => $documents, 'goods' => $goods]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }
    public function category(request $request, $urlcategory){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::with('templates')->where('title', $request->getHttpHost())->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $category_id = explode('-', $urlcategory)[0];
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::where('siteID', $siteID)->where('type', 5)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon','city', 'verYa', 'verGo')->first();
        $city_name = Cities::where('id', '=', $favicon->city)->first();
        /// Левое меню - получаем главные категории со связкой подкатегорий
        $category_nav = Category::with('sub_category_nav')->where('siteID', $siteID)->where('parent_id', null)->where('type', 1)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем саму категорию, на которую зашли
        $category = Category::with('bread_cat')->with('video')->where('id', $category_id)->where('siteID', $siteID)->where('published', 1)->first();
        if($category == null){abort(404);}
        /// Получаем подкатегории
        $sub_category = Category::where('parent_id', $category_id)->where('published', 1)->get();
        /// Получаем товары для категории, на которой находимся
        $goods = Goods::with('product_url_scat')->whereHas('product_url_scat', function($query)use($category_id){
            $query->where('published', 1)->where('parent_id', $category_id);
        })->where('siteiD', $siteID)->where('published', 1)->orWhere('category', $category_id)->where('published', 1)->paginate(12);
        // отзывы
        $reviews = Feedback::with('imageses')->where('siteID', $siteID)->where('category', $category_id)->where('published', 1)->limit(6)->get();
        /// DOCTYPE
        $t1 = ' заказать';
        $g1 = ' в '.$city_name->gorode;
        $d1 = $category->title.' - цены, характеристики, отзывы, заказать в '.$city_name->gorode;
        if($category->seotitle){$seoTitle = $category->seotitle;}else $seoTitle = $category->title.$t1.$g1;
        if($category->seodescription){$seoDescription = $category->seodescription;}else{$seoDescription = $d1;}
        if($category->seokeywords){$keywords = $category->seokeywords;}else{$keywords = null;}
        $canonical = $request->getSchemeAndHttpHost().'/services'.'/'.$category->id.'-'.$category->slug;
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.services.category_1', ['settings' => $settings, 'reviews' => $reviews, 'category_nav' => $category_nav, 'category_id' => $category_id, 'category' => $category, 'goods' => $goods,'sub_category' => $sub_category, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }
    public function view(request $request, $urlcategory, $slug){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::with('templates')->where('title', $request->getHttpHost())->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $services_id = explode('-', $slug)[0];
        $category_id = explode('-', $urlcategory)[0];
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::where('siteID', $siteID)->where('type', 5)->first();
        $services = Goods::with('imageses')->with('video')->where('id', $services_id)->where('published', 1)->where('siteID', $siteID)->first();
        if($services == null){abort(404);}
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon','city', 'verYa', 'verGo', 'metrika_short')->first();
        $city_name = Cities::where('id', '=', $favicon->city)->first();
        /// Левое меню - получаем главные категории со связкой подкатегорий
        $category_nav = Category::with('sub_category_nav')->where('siteID', $siteID)->where('parent_id', null)->where('type', 1)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем главную категорию для товара - breadcrump и ссылки
        $category_parent = Category::with('bread_cat')->where('id', $category_id)->select('id', 'title', 'slug', 'parent_id')->first();
        // портфолио
        $portfolio = Portfolio::with('image_one')->where('siteID', $siteID)->where('published', 1)->limit(16)->get();
        // отзывы
        $reviews = Feedback::with('imageses')->where('siteID', $siteID)->where('goods', $services_id)->where('published', 1)->limit(6)->get();
        /// Получаем менеджера
        $manager = Users::where('siteID', $siteID)->where('onSite', 1)->first();
        $company = Information::with('sites')->with('cities')->where('siteID', $siteID)->first();
        /// Получаем похожие услуги
        $similar = Goods::where('category', $category_id)->where('id', '!=', $services_id)->where('published', 1)->limit(4)->get();
        /// DOCTYPE
        if($services->coast) $t1 = ' заказать за '.$services->coast.' руб,'; else $t1 = ' заказать';
        $g1 = ' в '.$city_name->gorode;
        $d1 = ' заказать в '.$city_name->gorode.' - характеристики, фото, отзывы. Звоните!';
        if($services->seotitle){$seoTitle = $services->seotitle;}else $seoTitle = $services->title.$t1.$g1;
        if($services->seodescription){$seoDescription = $services->seodescription;}else $seoDescription = $services->title.$d1;
        if($services->seokeywords){$keywords = $services->seokeywords;}else{$keywords = null;}
        $canonical = $request->getSchemeAndHttpHost().'/services'.'/'.$category_parent->id.'-'.$category_parent->slug.'/'.$services->id.'-'.$services->slug;
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        ///HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.services.view_1', ['settings' => $settings, 'category_nav' => $category_nav, 'category_parent' => $category_parent, 'services' => $services, 'portfolio' => $portfolio, 'reviews' => $reviews, 'manager' => $manager, 'company' => $company, 'similar' => $similar, 'documents' => $documents, 'favicon' => $favicon]);
        ///FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        ///END FOOTER
        return $view;
    }
}
