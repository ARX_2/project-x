<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 06.08.2019
 * Time: 21:49
 */

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Feedback;

use App\Model\Information;
use App\Model\Users;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Caching;
use Resize;


class AllReviewsController extends Controller
{
    public function index(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        // отзывы
        $category = Category::with('images')->with('sub_category_nav')->with('reviews')->with('section')->where('parent_id', null)->where('published', 1)->where('siteID', $siteID)->get();

        $reviews = Feedback::with('count_category')->whereHas('count_category', function($query){$query->where('published', 1)->where('type', '!=' , null);})->where('siteID', $siteID)->get();
        /// DOCTYPE
        $seoTitle='Отзывы на категории';
        $seoDescription = null;
        $keywords = null;
        $canonical = $request->getSchemeAndHttpHost().'/all-reviews';
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.all_reviews.index', ['category' => $category,'reviews' => $reviews]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }
}