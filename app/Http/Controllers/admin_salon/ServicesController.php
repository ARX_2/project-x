<?php

namespace App\Http\Controllers\admin_salon;
use DB;
use App\Category;
use App\Services;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\masterServeces;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use App\Users;
use App\Company;
use App\Site;
use App\Subcategory;
use App\CategoryGoods;
use App\ExcelUpload;
use App\ImageResize;
use App\Image;
use App\Goods;
use Caching;
use App\Supplier;
use App\Goods_salon;
use App\Service_goods;
use App\Service_option;
class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      //dd(explode('?',$_SERVER['REQUEST_URI'])[0]);
      $urlActive = route('admin.goods.index');
      $urlHidden = route('admin.goods.hidden');
      $urlCreate = route('admin.goods.create');
      // $categories  = Goods::with('category')->where('siteID', 16)->get();
      // dd($categories);

      //dd($categories);
    //dd($company);
    //$categories = Category::where('siteId', $request->siteID)->get();
    Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();

    if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin-salon/services/hidden'){
      //$goods = Goods::with('images')->with('subcategory')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);

      $goods = Goods::with('subcategory')->whereHas('categories', function($query){
        $query->where('type', 1);
        $query->orWhereHas('categor', function($sub){
          $sub->where('type',1);
        });
      })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

      }
      else{
        $goods = Goods::with('subcategory')->whereHas('categories', function($query){
          $query->where('type', 1);
          $query->orWhereHas('categor', function($sub){
            $sub->where('type',1);
          });
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);


      }

      $product = $goods->with('Goods_salon')->orderBy('updated_at', 'desc')->paginate(10);

    $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
      $query->where('type', 1);
      $query->orWhereHas('categor', function($sub){
        $sub->where('type',1);
      });
    })->where('published', 1)->count('id');

    $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
      $query->where('type', 1);
      $query->orWhereHas('categor', function($sub){
        $sub->where('type',1);
      });
    })->where('published', 0)->count('id');

    $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
      $query->where('type', 1);
      $query->orWhereHas('categor', function($sub){
        $sub->where('type',1);
      });
    })->where('deleted', 1)->count('id');

    return view('admin_salon.services.index',[
      'product' => $product,
      'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
      'countActive' => $countActive,
      'countHide' => $countHide,
      'countDeleted' => $countDeleted,
      'categories' => Category::with('subcategories')->where('siteID', Auth::user()->siteID)->where('type',0)->where('parent_id',null)->get(),
      //'type' => $type,
      'urlActive' => $urlActive,
      'urlHidden' => $urlHidden,
      'urlCreate' => $urlCreate,
      'link' => $_SERVER['REQUEST_URI'],
    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

      //$product = Goods::with('subcategoriesAll')->with('properties')->where('id', $request->id)->first();
      //dd($product);

      $category = Category::with('subcategoriesAll')->where('type',1)->where('published',1)->where('siteID', auth::user()->siteID)->get();
      $goods =  Goods::where('status',1)->with('service_goods')->with('options')->first();
      if($goods == null){
        $id = Goods::insertGetId(['status' => 1, 'siteID' => Auth::user()->siteID]);
        $goods = Goods::where('id',$id)->with('service_goods')->with('options')->first();
        Goods_salon::insert(['parent_id' => $id, 'created_at' => date('Y-m-d H:i:s'), 'siteID' => Auth::user()->siteID]);
      }
      $materials = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
        $query->where('type', '!=', 1);
        $query->where('published',1);
        $query->orWhereHas('categor', function($sub){
          $sub->where('type', '!=', 1);
          $sub->where('published',1);
        });
      })->with('goods_salon')->get();
      //dd($product);
      return view('admin_salon.services.create',[
        'categories' => $category,
        'product' =>  $goods,
        'siteID' => $request->siteID,
        'materials' => $materials,
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *тест
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sort = Goods::max('sort');
         $goodsid = Goods::insertGetId([
           'title' => $request->title, 'short_description' => $request->short_description, 'slug' => $request->slug,
           'coast' => $request->coast, 'youtube' => $request->youtube, 'seoname' => $request->seoname,
           'seotitle' => $request->seotitle, 'seodescription' => $request->seodescription, 'seokeywords' => $request->seokeywords,
           'siteID' => auth::user()->siteID, 'deleted' => 0, 'published' => 1, 'sort' => $sort, 'slug' => str_slug($request->title),
           'category' => $request->subcategory, 'created_at' => date('Y-m-d H:i:s'),
         ]);
         if($request->file('file') !== null){
         foreach ($request->file('file') as $img) {
           $path = Storage::putFile('public', $img);
           Image::create(['images' => $path, 'goods' => $goodsid, 'siteID' => Auth::user()->siteID]);
         }
         }


      return redirect()->route('admin.services.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function show(Services $services, Request $request)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Services $services)
    {
      $backdoor = Goods::where('siteID', auth::user()->siteID)->where('id', $request->id)->value('id');
      if($backdoor == null){
        return redirect()->route('admin.services.index');
      }

      $category = Category::with('subcategoriesAll')->where('type',1)->where('published',1)->where('siteID', auth::user()->siteID)->get();
      $goods =  Goods::with('goods_salon')->with('service_goods')->with('options')->where('id', $request->id)->first();
      $materials = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
        $query->where('type', '!=', 1);
        $query->where('published',1);
        $query->orWhereHas('categor', function($sub){
          $sub->where('type', '!=', 1);
          $sub->where('published',1);
        });
      })->with('goods_salon')->get();
      //dd($product);
      return view('admin_salon.services.create',[
        'categories' => $category,
        'product' =>  $goods,
        'siteID' => $request->siteID,
        'materials' => $materials,
      ]);
      //dd($product);
      return view('admin.services.create',[
        'company' => $company,
        'categories' => $category,
        'product' =>  $product,
        'siteID' => $request->siteID,
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function update($slug,$id,$parametr,Request $request, Services $services)
    {
      //dd($parametr);
      if($request->subcategory !== 0 && $request->subcategory !== null){
        $category = $request->subcategory;
      }
      else{
        $category = $request->category;
      }
      $unparametr[$parametr] = $parametr;
      $unparametr[0] = 1;
      $unparametr[1] = 0;

      $update['published'] = ['published' => $unparametr[$parametr]];
      $update['main'] = ['main' => $unparametr[$parametr]];
      $update['title'] = ['title' => $parametr];
      $update['coast'] = ['coast' => $parametr];
      $update['category'] = ['category' => $parametr];
      $update['checkseo'] = ['checkseo' => $parametr];
      $update['seoname'] = ['seoname' => $parametr];
      $update['seotitle'] = ['seotitle' => $parametr];
      $update['seodescription'] = ['seodescription' => $parametr];
      $update['seokeywords'] = ['seokeywords' => $parametr];

      $update['articulate'] = ['articulate' => $parametr];

    $result =  Goods::with('categories')->where('id', $id)->where('siteID', Auth::user()->siteID)->update($update[$slug]);
      //dd($request->file('file'));
      if($request->file('file') !== null && $result == 1){
      foreach ($request->file('file') as $img) {
        $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
        ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
        Image::create(['images' => 'public/'.$filename, 'goods' => $request->id, 'siteID' => Auth::user()->siteID]);
      }
      }
    }
    public function subservice($slug,$id,$parametr,Request $request){
      $update['time'] = ['timework' => $parametr];
      Goods_salon::where('id', $id)->where('siteID', Auth::user()->siteID)->update($update[$slug]);

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function destroy(Services $services)
    {
      //dd($_GET);
      Services::where('id', $_GET['id'])->delete();
      return redirect()->route('admin.services.index');
    }

    public function sortServices(Request $request){
          if($request->id == 0){
            $goods = Goods::with('images')
            ->with('subcategory')
            ->whereHas('categories', function($query){
              $query->whereIn('type', 0);
            })->where('goods.siteID', Auth::user()->siteID)->where('goods.deleted', 0)->paginate(10);
          }
          else{
            Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();
            $subcategories = Category::where('parent_id', $request->id)->get();
            $categories = [];
            $categories[] = (int)$request->id;
            foreach ($subcategories as $key => $value) {
              $categories[] = $value->id;
            }

            $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($categories){
              $query->whereIn('id', $categories);
            })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->paginate(20);
            //dd($goods);
          }
          //dd($goods);
        //}

        $view = '';

        $view .= view('admin.services.services', ['product' => $goods]);
        return $view;
    }

    public function CategoryServices($category,Request $request){


      //dd($type);
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();

      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();


      //dd($goods->get());
      //dd(explode('?',$_SERVER['REQUEST_URI'])[0]);
      if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/services/'.$category){
        //dd('111');
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($category){
          $query->where('id', $category);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);

      }
      else{
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($category){
          $query->where('id', $category);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

      }


      $product = $goods->paginate(10);
      //dd($product);

      $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('id', $category);
      })->where('published', 1)->count('id');

      $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('id', $category);
      })->where('published', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($category){
        $query->where('id', $category);
      })->where('deleted', 1)->count('id');
      //dd($product);
      //dd($product[0]->images);
      //dd($categories);

      //dd($type);
      $categoryType = Category::where('id', $category)->value('type');
      //dd($categoryType);
      if($categoryType == 0){
        $goodstype = 'goods';
      }
      else{
        $goodstype = 'services';
      }
      $urlActive = '/admin/category/services/'.$category;
      $urlHidden = '/admin/category/services/hidden/'.$category;
      return view('admin.'.$goodstype.'.index',[
        'company' => $company,
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::where('siteID', Auth::user()->siteID)->get(),
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden
        //'type' => $type,
      ]);
      //dd($request);
    }

    public function CategoryServiceAll($category,Request $request){


      //dd($type);
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();
      $subcategories = Category::where('parent_id', $category)->get();
      $categories = [];
      $categories[] = (int)$category;
      foreach ($subcategories as $key => $value) {
        $categories[] = $value->id;
      }
      //dd($categories);


      //dd($goods->get());

      if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/category/services/all/'.$category){
        //dd('111');
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($categories){
          $query->whereIn('id', $categories);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);
      }
      else{
        $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($categories){
          $query->whereIn('id', $categories);
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

      }


      $product = $goods->paginate(10);
      //dd($product);
      $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($categories){
        $query->whereIn('id', $categories);
      })->where('published', 1)->count('id');

      $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($categories){
        $query->whereIn('id', $categories);
      })->where('published', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($categories){
        $query->whereIn('id', $categories);
      })->where('deleted', 1)->count('id');
      //dd($product);
      //dd($product[0]->images);
      //dd($categories);

      //dd($type);
      $categoryType = Category::where('id', $category)->value('type');
      //dd($categoryType);
      if($categoryType == 0){
        $goodstype = 'goods';
      }
      else{
        $goodstype = 'services';
      }

      $urlActive = '/admin/category/services/all/'.$category;
      $urlHidden = '/admin/category/services/all/hidden/'.$category;
      return view('admin.'.$goodstype.'.index',[
        'company' => $company,
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::where('siteID', Auth::user()->siteID)->get(),
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden
        //'type' => $type,
      ]);
      //dd($request);
    }

    public function SubcategoryServices($subcategory, Request $request){
        //dd($subcategory);
      $company = Site::with(['sections' => function($sect){
  $sect->whereIn('slug', ['product', 'services']);
  $sect->orderBy('slug', 'asc');
}])->with('info')->where('id', Auth::user()->siteID)->first();
      //$categories = Category::where('siteId', $request->siteID)->get();
      Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();



        //dd($categoryGoods);
        if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin/subcategory/services/'.$subcategory){
          $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($subcategory){
            $query->where('id', $subcategory);
          })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);
        }
        else{
          $goods = Goods::with('subcategory')->whereHas('categories', function($query) use($subcategory){
            $query->where('id', $subcategory);
          })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);
        }
        //$categories = Category::where('siteID', Auth::user()->siteID)->with('images')->where('published', 1)->orderBy('sort', 'asc')->get();
        //dd($goods->get());

        $product = $goods->paginate(10);


      //dd($product);
      $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($subcategory){
        $query->where('id', $subcategory);
      })->where('published', 1)->count('id');

      $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($subcategory){
        $query->where('id', $subcategory);
      })->where('published', 0)->count('id');

      $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query) use($subcategory){
        $query->where('id', $subcategory);
      })->where('deleted', 1)->count('id');


      $categoryType = Category::where('parent_id', $subcategory)->value('type');
      //dd($categoryType);
      if($categoryType == 0){
        $goodstype = 'goods';
      }
      else{
        $goodstype = 'services';
      }
      //dd($type);
      $urlActive = '/admin/subcategory/services/'.$subcategory;
      $urlHidden = '/admin/subcategory/services/hidden/'.$subcategory;
      return view('admin.'.$goodstype.'.index',[
        'company' => $company,
        'product' => $product,
        'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
        'siteID' => $request->siteID,
        'countActive' => $countActive,
        'countHide' => $countHide,
        'countDeleted' => $countDeleted,
        'categories' => Category::where('siteID', Auth::user()->siteID)->get(),
        'urlActive' => $urlActive,
        'urlHidden' => $urlHidden,

        //'type' => $type,
      ]);

    }


    public function description(Request $request){

      Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['short_description' => $request->short_description,'status' => 2, 'updated_at' => date('Y-m-d H:i:s'), 'published' => 1]);
      return  self::index();
    }
    public function material($morfid, $id,$params, Request $request){
      if($morfid == '0'){
        $id =  Service_goods::insertGetId(['goods' => $params, 'service' => $id, 'created_at' => date('Y-m-d H:i:s'), 'siteID' => Auth::user()->siteID]);
      }
      else{
        Service_goods::where('id', $morfid)->where('siteID', Auth::user()->siteID)->update(['goods' => $params]);
        $id = $morfid;
      }
      return $id;
    }
    public function materialCounts($id,$params, Request $request){
      //dd($id);
      Service_goods::where('id', $id)->where('siteID', Auth::user()->siteID)->update(['counts' => $params]);
    }
    public function payment($productID,$id, Request $request){
      $params = (int)Service_goods::where('id', $id)->where('siteID', Auth::user()->siteID)->value('payment');
      $unparametr[0] = 1;
      $unparametr[1] = 0;
      Service_goods::where('id', $id)->where('siteID', Auth::user()->siteID)->update(['payment' => $unparametr[$params]]);
    }

    public function materialDelete($id){
      Service_goods::where('id', $id)->where('siteID', Auth::user()->siteID)->delete();
    }

    public function option($id){
    $id =  Service_option::insertGetId(['service' => $id, 'siteID' => Auth::user()->siteID,
       'created_at' => date('Y-m-d H:i:s')]);
       return $id;
}

public function optionUpdate($slug, $id, $params){
    $update['title'] = ['title' => $params];
    $update['cost'] = ['cost' => $params];
    Service_option::where('id', $id)->where('siteID', Auth::user()->siteID)->update($update[$slug]);
}
public function optionDel($id){
  Service_option::where('id', $id)->where('siteID', Auth::user()->siteID)->delete();
}
}
