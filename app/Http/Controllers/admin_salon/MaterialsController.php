<?php

namespace App\Http\Controllers\admin_salon;

use App\Goods_salon;
use App\Category;
use App\Goods;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Users;
use App\Image;
use App\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Company;
use ImageResize;
use App\Site;
use App\Property;
use WebP;
use App\Subcategory;
use App\ExcelUpload;
use App\Goods_object;
use App\Goods_product;
use App\Supplier;
use App\Inventory;
use App\Type_cost;
class MaterialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $urlActive = route('admin.goods.index');
      $urlHidden = route('admin.goods.hidden');
      $urlCreate = route('admin.goods.create');
      // $categories  = Goods::with('category')->where('siteID', 16)->get();
      // dd($categories);

      //dd($categories);
    //dd($company);
    //$categories = Category::where('siteId', $request->siteID)->get();
    Goods::where('siteID', Auth::user()->siteID)->where('dateDelete', '<=', date('Y-m-d H:i:s'))->delete();

    if(explode('?',$_SERVER['REQUEST_URI'])[0] == '/admin-salon/materials/hidden'){
      //$goods = Goods::with('images')->with('subcategory')->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);

      $goods = Goods::with('subcategory')->whereHas('categories', function($query){
        $query->where('type', 3);
        $query->orWhereHas('categor', function($sub){
          $sub->where('type',3);
        });
      })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',0);

      }
      else{
        $goods = Goods::with('subcategory')->whereHas('categories', function($query){
          $query->where('type', 3);
          $query->orWhereHas('categor', function($sub){
            $sub->where('type',3);
          });
        })->where('siteID', Auth::user()->siteID)->where('deleted', 0)->where('published',1);


      }

      $product = $goods->with('Goods_salon')->orderBy('updated_at', 'desc')->paginate(10);

    $countActive = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
      $query->where('type', 3);
      $query->orWhereHas('categor', function($sub){
        $sub->where('type',3);
      });
    })->where('published', 1)->count('id');

    $countHide = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
      $query->where('type', 3);
      $query->orWhereHas('categor', function($sub){
        $sub->where('type',3);
      });
    })->where('published', 0)->count('id');

    $countDeleted = Goods::where('siteID', Auth::user()->siteID)->whereHas('categories', function($query){
      $query->where('type', 3);
      $query->orWhereHas('categor', function($sub){
        $sub->where('type',3);
      });
    })->where('deleted', 1)->count('id');

    return view('admin_salon.materials.index',[
      'product' => $product,
      'excelUpload' => ExcelUpload::where('siteID', Auth::user()->siteID)->get(),
      'countActive' => $countActive,
      'countHide' => $countHide,
      'countDeleted' => $countDeleted,
      'categories' => Category::with('subcategories')->where('siteID', Auth::user()->siteID)->where('type',0)->where('parent_id',null)->get(),
      //'type' => $type,
      'urlCreate' => $urlCreate,
      'link' => $_SERVER['REQUEST_URI'],
      'suppliers' => Supplier::where('siteID', Auth::user()->siteID)->get(),
      'type_coasts' => Type_cost::get(),
    ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $category = Category::with('subcategoriesAll')->where('type',3)->where('published',1)->where('siteID', auth::user()->siteID)->get();
      $goods =  Goods::where('status',1)->first();
      if($goods == null){
        $id = Goods::insertGetId(['status' => 1, 'siteID' => Auth::user()->siteID]);
        $goods = Goods::where('id',$id)->first();
      }

    //dd($product);
    return view('admin_salon.materials.create',[
      'categories' => $category,
      'product' =>  null,
      'siteID' => $request->siteID,
      'product' => $goods,
      'suppliers' => Supplier::where('siteID', Auth::user()->siteID)->get(),
      'type_coasts' => Type_cost::get(),

    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Goods_salon  $goods_salon
     * @return \Illuminate\Http\Response
     */
    public function show(Goods_salon $goods_salon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Goods_salon  $goods_salon
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Goods_salon $goods_salon)
    {
      $backdoor = Goods::where('siteID', auth::user()->siteID)->where('id', $request->id)->value('id');
      if($backdoor == null){
        return redirect()->route('admin.goods.index');
      }
      $product = Goods::with('subcategory')->with('goods_salon')->with('imageses')->where('id', $request->id)->first();
        $category = Category::with('subcategories')->where('type',3)->where('published',1)->where('siteID', auth::user()->siteID)->get();
        //dd($product);

      //dd($product);
      return view('admin_salon.materials.create',[
        'categories' => $category,
        'product' =>  $product,
        'siteID' => $request->siteID,
        'suppliers' => Supplier::where('siteID', Auth::user()->siteID)->get(),
        'type_coasts' => Type_cost::get(),
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Goods_salon  $goods_salon
     * @return \Illuminate\Http\Response
     */
     public function update($slug,$id,$parametr,Request $request, Goods $goods)
     {
       //dd($slug);
       if($request->subcategory !== 0 && $request->subcategory !== null){
         $category = $request->subcategory;
       }
       else{
         $category = $request->category;
       }
       $unparametr[$parametr] = $parametr;
       $unparametr[0] = 1;
       $unparametr[1] = 0;

       $update['published'] = ['published' => $unparametr[$parametr]];
       $update['main'] = ['main' => $unparametr[$parametr]];
       $update['title'] = ['title' => $parametr];
       $update['coast'] = ['coast' => $parametr];
       $update['category'] = ['category' => $parametr];
       $update['checkseo'] = ['checkseo' => $parametr];
       $update['seoname'] = ['seoname' => $parametr];
       $update['seotitle'] = ['seotitle' => $parametr];
       $update['seodescription'] = ['seodescription' => $parametr];
       $update['seokeywords'] = ['seokeywords' => $parametr];

       $update['articulate'] = ['articulate' => $parametr];
      $update['type_cost'] = ['type_cost' => $parametr];
     $result =  Goods::with('categories')->where('id', $id)->where('siteID', Auth::user()->siteID)->update($update[$slug]);
       //dd($request->file('file'));
       if($request->file('file') !== null && $result == 1){
       foreach ($request->file('file') as $img) {
         $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
         ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
         Image::create(['images' => 'public/'.$filename, 'goods' => $request->id, 'siteID' => Auth::user()->siteID]);
       }
       }
     }

     public function subgoods($slug,$id,$parametr,Request $request, Goods $goods){
       $salon = Goods_salon::where('parent_id', $id)->value('id');
        if($salon == null){
         $salonID = Goods_salon::insertGetId(['siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s'), 'parent_id' => $id]);
        }
        else{
          $salonID = $salon;
        }

        $update['bar_code'] = ['bar_code' => $parametr];
        $update['supplier'] = ['supplier' => Supplier::where('title',$parametr)->value('id')];
        $update['master_sale_type'] = ['master_sale_type' => $parametr];
        $update['master_sale_count'] = ['master_sale_count' => $parametr];
        $update['administrator_sale_type'] = ['administrator_sale_type' => $parametr];
        $update['administrator_sale_count'] = ['administrator_sale_count' => $parametr];
        //dd($update[$slug]);
        $result =  Goods_salon::where('id', $salonID)->where('siteID', Auth::user()->siteID)->update($update[$slug]);
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Goods_salon  $goods_salon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Goods_salon $goods_salon)
    {
        //
    }
    public function imageUpload(Request $request){

      if($request->file('file') !== null){
      $img =  $request->file('file');
        $filename  = uniqid() . '.' . $img->getClientOriginalExtension();
        ImageResize::make($img->getRealPath())->save('storage/app/public/'. $filename);
       $id =  Image::insertGetId(['images' => 'public/'.$filename, 'goods' => $request->id, 'siteID' => Auth::user()->siteID]);
        $data = ['image' => 'public/'.$filename, 'id' => $id];
      return json_encode($data);
      }

    }
    public function createSupplier(Request $request){
    $id =  Supplier::insertGetId(['title' => $request->title, 'name' => $request->name, 'tel' => $request->tel, 'email' => $request->email,
    'inn' => $request->inn, 'kpp' => $request->kpp, 'comment' => $request->comment, 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d H:i:s')]);
    $suppliers = Supplier::where('siteID', Auth::user()->siteID)->get();
    $options = '';
     foreach ($suppliers as $value) {
       if($value->id == $id){
         $selected = 'selected';
       }
       else{
         $selected = '';
       }
     $options .= '<option value="'.$value->id.'" '.$selected.'>'.$value->title.'</option>';
    }

    $select = '
    <select name="supplier" class="form-control" style="width:200px;">
      '.$options.'
    </select>
    ';
      return $select;
    }

    public function description(Request $request){

      Goods::where('id', $request->id)->where('siteID', Auth::user()->siteID)->update(['short_description' => $request->short_description,'status' => 2, 'updated_at' => date('Y-m-d H:i:s'), 'created_at' => date('Y-m-d H:i:s'), 'published' => 1]);
      return  self::index();
    }
}
