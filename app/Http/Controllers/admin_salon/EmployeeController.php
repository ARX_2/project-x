<?php

namespace App\Http\Controllers\admin_salon;
use DB;
use App\Users;
use App\Employee;
use App\Services;
use App\masterServeces;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Company;
use Illuminate\Support\Facades\Storage;
use App\Site;
use App\UserRolle;
use App\Users_salon;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Site::with(['sections' => function($sect){
        $sect->whereIn('slug', ['product', 'services']);
        $sect->orderBy('slug', 'asc');
      }])->with('info')->where('id', Auth::user()->siteID)->first();
      //$users = Users::with('userrolle')->where('siteID', Auth::user()->siteID)->where('userRolle', '!=', 1)->get();
      $userRolles = UserRolle::whereHas('users', function($query){
        $query->where('siteID', Auth::user()->siteID);
      })->with(['users' => function($query){
        $query->where('siteID', Auth::user()->siteID);
      }])->get();
      //dd($userRolles);
      //dd($userRolles);
      return view('admin_salon.employee.index',[
        'company' => $company,
        'siteID' => Auth::user()->siteID,
        //'users'=>$users,
        'userRolles' => $userRolles,
        'allRolles' => UserRolle::get(),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin_salon.employee.create', [
      'contact' => [],
      'Users' => Users::with('users'),
      'delimiter' => '',
      'Users' => Users::get(),
      'sites' => DB::table('sites')->get(),
      'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
      'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request->password);
        //dd($request->userRolle);
        //dd(Auth::user()->siteID);
        //dd($request);
        if($request->file('image') !== null){
           $path = Storage::putFile('public', $request->file('image'));

         }
         else{
           $path = null;
         }
      $id = Users::insertGetId(['name' => $request->name,
                                'email' => $request->email,
                                'password' => bcrypt($request->password),
                                'userRolle' => $request->userRolle,
                                'published' => $request->published,
                                'title' => $request->title,
                                'description' => $request->description,
                                'image' => $path,
                                'tel' => $request->tel,
                                'siteID' => Auth::user()->siteID,
                                'sex' => $request->user_sex,
                                'job' => $request->job,
                                'lastname' => $request->lastname,
                                'datestart' => $request->datestart,
                                'datestart' => $request->datestart,
                                'status' => $request->status,
                                'datefinish' => $request->datefinish,
                                'payday' => $request->payday]);
      $id = Users_salon::insertGetId(['user' => $id, 'siteID' => auth::user()->siteID, 'check_master' => (int)$request->check_master,
    'render_service' => (int)$request->render_service, 'sale_goods' => (int)$request->sale_goods, 'sale_aboniments' => (int)$request->sale_aboniments,
    'sale_materials' => (int)$request->sale_materials, 'sale_sertificats' => (int)$request->sale_sertificats, 'take_discount' => (int)$request->take_discount,
    'pay_from_aboniments' => (int)$request->pay_from_aboniments, 'check_administrator' => (int)$request->check_administrator, 'all_services' => (int)$request->all_services,
    'all_sale_goods' => (int)$request->all_sale_goods, 'all_sale_aboniments' => (int)$request->all_sale_aboniments, 'all_sale_angelfish' => (int)$request->all_sale_angelfish,
    'all_sale_materials' => (int)$request->all_sale_materials, 'all_sale_sertificats' => (int)$request->all_sale_sertificats, 'show_in_chart' => (int)$request->show_in_chart,
    'take_discount_admin' => (int)$request->take_discount_admin, 'pay_from_aboniments_admin' => (int)$request->pay_from_aboniments_admin, 'check_payday' => (int)$request->check_payday,
    'type_payday' => (int)$request->type_payday, 'count1' => (int)$request->count1, 'payday1' => (int)$request->payday1, 'count2' => (int)$request->count2, 'payday2' => (int)$request->payday2,
    'created_at' => date('Y-m-d H:i:s')]);
      //dd($contact);
      // Categories
      return redirect()->route('admin-salon.employee.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Users $employee)
    {
    //  dd($employee);
      return view('admin_salon.employee.edit', [
    'User' => $employee,
    'Users' => Users::with('users'),
    'delimiter' => '',
    'Users' => Users::get(),
    'sites' => DB::table('sites')->get(),
    'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
    'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
  ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Users $employee)
    {
      foreach ($_GET as $key => $value) {
        $id = $key;
      }
      $id = Users::where('id', $id)->where('siteID', Auth::user()->siteID)->value('id');
      if($id == null){
        return redirect()->route('admin-salon.employee.index');
      }
      //dd($employee);
      if($request->file('image') !== null){
        Storage::delete($employee->image);
         $path = Storage::putFile('public', $request->file('image'));

       }
       else{
         $path = $employee->image;
       }
       //dd($request->lastname);
            Users::where('id', $id)->where('siteID', Auth::user()->siteID)
                      ->update(['name' => $request->name,
                                'lastname' => $request->lastname,
                                 'email' => $request->email,
                                 'password' => bcrypt($request->password),
                                 'title' => $request->title,
                                 'description' => $request->description,
                                 'image' => $path,
                                 'tel' => $request->tel,
                                 'userRolle' => $request->userRolle,
                                 'sex' => $request->user_sex,
                                 'job' => $request->job,
                                 'datestart' => $request->datestart,
                                 'status' => $request->status,
                                 'datefinish' => $request->datefinish,
                                 'payday' => $request->payday]);

              $id = Users_salon::insertGetId(['user' => $id, 'siteID' => auth::user()->siteID, 'check_master' => (int)$request->check_master,
            'render_service' => (int)$request->render_service, 'sale_goods' => (int)$request->sale_goods, 'sale_aboniments' => (int)$request->sale_aboniments,
            'sale_materials' => (int)$request->sale_materials, 'sale_sertificats' => (int)$request->sale_sertificats, 'take_discount' => (int)$request->take_discount,
            'pay_from_aboniments' => (int)$request->pay_from_aboniments, 'check_administrator' => (int)$request->check_administrator, 'all_services' => (int)$request->all_services,
            'all_sale_goods' => (int)$request->all_sale_goods, 'all_sale_aboniments' => (int)$request->all_sale_aboniments, 'all_sale_angelfish' => (int)$request->all_sale_angelfish,
            'all_sale_materials' => (int)$request->all_sale_materials, 'all_sale_sertificats' => (int)$request->all_sale_sertificats, 'show_in_chart' => (int)$request->show_in_chart,
            'take_discount_admin' => (int)$request->take_discount_admin, 'pay_from_aboniments_admin' => (int)$request->pay_from_aboniments_admin, 'check_payday' => (int)$request->check_payday,
            'type_payday' => (int)$request->type_payday, 'count1' => (int)$request->count1, 'payday1' => (int)$request->payday1, 'count2' => (int)$request->count2, 'payday2' => (int)$request->payday2,
            'created_at' => date('Y-m-d H:i:s')]);

      return redirect()->route('admin-salon.employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      //dd($request);
      $employee->delete();
      return redirect()->route('admin.employee.index');
    }
    public function delete(Request $request){
      $backdoor = Users::where('id', $request->id)->where('siteID', Auth::user()->siteID)->delete();
      return redirect()->route('admin.employee.index');
    }
    public function addServices(){
      masterServeces::insert(['master' => $_GET['userid'], 'serveces' => $_GET['servicesid']]);
      return redirect()->route('admin.employee.index');
    }
    public function deleteServices(){
      //dd($_GET);

      masterServeces::where('id',$_GET['servicesid'])->delete();
      return redirect()->route('admin.employee.index');
    }

    public function imageDestroy(){

      Storage::delete($_GET['image']);
      Users::where('image', $_GET['image'])->update(['image' => null]);
        return redirect()->route('admin.company.edit', [$_GET['editurl']]);
    }

    public function profile(Request $request){
      $company = Site::with(['sections' => function($sect){
        $sect->whereIn('slug', ['product', 'services']);
        $sect->orderBy('slug', 'asc');
    }])->with('info')->where('id', Auth::user()->siteID)->first();
      //dd($company);
      return view('admin_salon.profile.index', [
        'company' => $company,
      ]);
    }

    public function profileEdit(Request $request){
      $company = Site::with(['sections' => function($sect){
        $sect->whereIn('slug', ['product', 'services']);
        $sect->orderBy('slug', 'asc');
    }])->with('info')->where('id', Auth::user()->siteID)->first();
      //dd($company);
      return view('admin_salon.profile.edit', [
        'company' => $company,
        'error' => $request->error,
      ]);
    }

    public function profileUpdate(Request $request){
      Users::where('id', Auth::user()->id)->update(['name' => $request->name, 'lastname' => $request->lastname, 'sex' => $request->user_sex, 'email' => $request->email, 'tel' => $request->tel]);
      return redirect()->route('admin.profile');
    }

    public function password(Request $request){

      if (!Hash::check($request->lastpassword, Auth::user()->password)){
        return redirect()->route('admin.profile.edit', ['error' => 1]);
      }
      if($request->newpassword !== $request->confirmpassword){
        return redirect()->route('admin.profile.edit', ['error' => 2]);
      }
      if($request->lastpassword == $request->newpassword){
        return redirect()->route('admin.profile.edit', ['error' => 3]);
      }
      //dd($request->newpassword);
      Users::where('id', Auth::user()->id)->update(['password' => bcrypt($request->newpassword)]);
      return redirect()->route('admin.profile');
    }
}
