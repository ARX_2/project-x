<?php

namespace App\Http\Controllers\admin_salon;
use DB;
use App\Work;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Users;
use App\Category;
use App\Image;
use App\Services;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use App\Company;
use App\Calendar;
use App\Goods;
use Resize;
use DateTime;
class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$company = Resize::company();
    $company =  Cache::get('companyInfo'.Auth::user()->siteID);
    //$masters =  Users::with('calendar')->where('userRolle', 5)->where('siteID', Auth::user()->siteID)->get();
    $date = [];
    for ($i=0; $i <= 5 ; $i++) {
      $newDate = \carbon\Carbon::createFromFormat('d.m.Y', date('d.m.Y'))->addDays($i)->toDateTimeString();
      $date[] = date("Y-m-d H:i:s", strtotime($newDate));
    }

    $engMonth = ['01' => 'january', '02' => 'february', '03' => 'mart',
    '04' => 'april', '05' => 'may', '06' => 'june', '07' => 'jule', '08' => 'august',
    '09' => 'september', '10' => 'october', '11' => 'november', '12' => 'december'];

    $masters = Users::whereHas($engMonth[date('m')], function($query){
      $query->where('day_'.date('d'),1);
    })->where('userRolle',5)->get();

    $goods = Goods::with('categories')->whereHas('categories', function($query){
      $query->where('type', 0);
      $query->orWhereHas('subcategories', function($sub){
        $sub->where('type', 0);
      });
    })->where('siteID', Auth::user()->siteID)->get();
    $services = Goods::with('categories')->whereHas('categories', function($query){
      $query->where('type', 1);
      $query->orWhereHas('subcategories', function($sub){
        $sub->where('type', 1);
      });
    })->where('siteID', Auth::user()->siteID)->get();
    $materials = Goods::with('categories')->whereHas('categories', function($query){
      $query->where('type', 3);
      $query->orWhereHas('subcategories', function($sub){
        $sub->where('type', 3);
      });
    })->where('siteID', Auth::user()->siteID)->get();
    //dd($goods);

      return view('admin_salon.work.index', [
        'company' => $company,
        'dates' => $date,
        'masters' => $masters,
        'goods' => $goods,
        'services' => $services,
        'materials' => $materials,
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.work.create', [
      'Services' => [],
      'Serviceses' => Services::with('users'),
      'delimiter' => '',
      'Services' => Services::get(),
      'Masters' => Users::where('userRolle', '>', 3)->get(),
      'Users' => Users::get(),
      'sites' => DB::table('sites')->get(),
      'Company' => Company::where('siteID', Auth::user()->siteID)->get(),
      'siteType' => DB::table('sites')->where('id', Auth::user()->siteID)->value('siteType'),
    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $timeFrom = date("Y-m-d H:i:s", strtotime($request->date.' '.$request->time));
      //dd($timeFrom);
      //dd($request->services);
      $servicesTime = Services::where('id', $request->services)->value('time');
      $dateTo = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeFrom)
      ->addMinutes($servicesTime*15)->toDateTimeString();
      //dd($dateTo);

      $contact = Work::create(['master' => $request->master,
                                'name' => $request->name,
                                'tel' => $request->tel,
                                'servicesid' => $request->services,
                                'timeFrom' => $timeFrom,
                                'timeTo' => $dateTo,
                                'status' => 0,
                                'siteID' => Auth::user()->siteID]);
      //dd($contact);
      // Categories
      return redirect()->route('admin.work.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function show(Work $work)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function edit(Work $work)
    {
        dd($work);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Work $work)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function destroy(Work $work)
    {
      Work::where('id', $work->id)->delete();
      return redirect()->route('admin.work.index');
        //dd($work);
    }
    public function master(){
      Work::where('id', $_GET['workid'])->update(['master'=>$_GET['masterid']]);
      return redirect()->route('admin.work.index');
    }

    public function status(){
      Work::where('id', $_GET['workid'])->update(['status'=>$_GET['statusid']]);
      return redirect()->route('admin.work.index');
    }

    public function time(Request $request){
      //dd($_POST);

      $dateFrom = $request->date.' '.$request->time;
      $dateTo = $request->dateTo;
      $master = $request->master;

      $newDate = \carbon\Carbon::createFromFormat('Y-m-d H:i', $dateFrom)
      ->addMinutes($dateTo*15)->toDateTimeString();

      $masterTime = Work::select('timeFrom', 'timeTo', 'id')->where('master', $master)->get();

      foreach ($masterTime as $m) {
        if($m->id == $request->workid){
          Work::where('id', $request->workid)->update(['timeFrom'=>$dateFrom, 'timeTo' => $newDate]);
          return redirect()->route('admin.work.index');
        }
        //dd($m->timeTo);
        if($m->timeFrom <= $dateFrom && $m->timeTo >= $dateFrom || $m->timeFrom < $newDate && $m->timeTo >= $newDate ){
          return redirect()->route('admin.work.index');
        }
        else{
          Work::where('id', $request->workid)->update(['timeFrom'=>$dateFrom, 'timeTo' => $newDate]);
          return redirect()->route('admin.work.index');
        }


      }

    }
    public function calendar($slug, $date){


      $datemonth = ['01' => 'январь', '02' => 'февраль', '03' => 'март',
      '04' => 'апрель', '05' => 'май', '06' => 'июнь', '07' => 'июль', '08' => 'август',
      '09' => 'сентябрь', '10' => 'октябрь', '11' => 'ноябрь', '12' => 'декабрь'];

      $engMonth = ['01' => 'january', '02' => 'february', '03' => 'mart',
      '04' => 'april', '05' => 'may', '06' => 'june', '07' => 'jule', '08' => 'august',
      '09' => 'september', '10' => 'october', '11' => 'november', '12' => 'december'];
      if($slug == 'now'){
        $date = \carbon\Carbon::createFromFormat('d.m.Y', $date)->subDays(10)->toDateTimeString();
        $date = date("d.m.Y", strtotime($date));
      }

      $nextDays = \carbon\Carbon::createFromFormat('d.m.Y', $date)->addDays(10)->toDateTimeString();
      $nextDays = date("d.m.Y", strtotime($nextDays));
      $lastDays = \carbon\Carbon::createFromFormat('d.m.Y', $date)->subDays(10)->toDateTimeString();
      $lastDays = date("d.m.Y", strtotime($lastDays));
      $month = date("m", strtotime($date));
      $d = (int)date("d", strtotime($date));
      $daylast = date("d", strtotime($date));
      //dd($d);
      $newDate = new DateTime($date);
      $oldDate = $newDate;
      $inMonth = $newDate->modify('last day of this month');
      $lastDay = $inMonth->format('d');
      $diff = $inMonth->diff(new DateTime($date))->format("%a");


      $masters =  Users::with($engMonth[$month])->where('userRolle', 5)->where('siteID', Auth::user()->siteID)->get();

      //dd($engMonth[$month]);
      $monthmaster = $month;
      $th = '';
      $tr = [];
      $masterInfo = [];
      if($diff < 25){

        for ($i=1; $i <= 24; $i++) {
          if($i == $diff +1){
            $border = 'style="border-right: 2px solid #ddd"';
          }
          else{
            $border = '';
          }

          $j = 0;
          foreach ($masters as $key => $master) {
            if($j == 1){
              //dd($d);
            }
            if($master[$engMonth[$monthmaster]] == null){
              if($d == 1 || $i == 1){
                Calendar::insert(['userid' => $master->id, 'month' => (int)$monthmaster, 'year' => date('Y'), 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d Hi:i:s')]);
              }
              $tr[$master->id][] = '<td style="border: 1px solid #ddd" class="checkday" id="'.$d.'" name="'.$master->id.'" month="'.$month.'"><span class="tdspan"></span></td>';
              $masterInfo[$master->id] = ['name' => $master->name, 'job' => $master->job];
            }
            else{
                //dd($master[$engMonth[$month]]['day_'.$d]);
                //dd($month);
                //dd($master[$engMonth[$month]]);
                if($master[$engMonth[$monthmaster]]['day_'.$d] == 1){
                  $back = 'tdspanback';
                }
                else{
                  $back = '';
                }

                $tr[$master->id][] = '<td style="border: 1px solid #ddd" class="checkday" id="'.$d.'" name="'.$master->id.'" month="'.$monthmaster.'"><span class="tdspan '.$back.'"></span></td>';
                $masterInfo[$master->id] = ['name' => $master->name, 'job' => $master->job];
            }
            $j++;
          }
          if($d.'.'.$month == date('d.m')){
            $border = 'style="border: 2px solid #d21a1a"';
          }

          $th .= '<th '.$border.'>'.$d.'</th> ';
          if($d == $lastDay){
            $d = 0;
            $monthmaster++;
            if($monthmaster < 10){
              $monthmaster = '0'.$monthmaster;
            }
          }
          $d++;

        }
        $nextMonth = $month +1;
        if($nextMonth < 10){
        $nextMonth =  '0'.$nextMonth;
        }
        else{
          $nextMonth = (string)$nextMonth;
        }
        $Collspan = 24 - $diff;
        $monthAll = '<th colspan="'.(string)($diff +1) .'" style="border:2px solid #ddd;" class="dinamic">
            <div style="overflow: hidden; width: 100%; text-align:center;">'.$datemonth[$month].'</div>
        </th>';
        $monthAll .= '<th colspan="'. (string)$Collspan.'" style="border:2px solid #ddd;" class="dinamic">
            <div style="overflow: hidden; width: 100%; text-align:center;">'.$datemonth[$nextMonth].'</div>
        </th>';
        //dd($lastDays);
        $d = $d-1;
        if($d<10){$d= '0'.$d;}else{$d= (string)$d;}

        $mastersDays = '';
        //dd($tr);

        foreach ($tr as $key => $value) {
          $newtd = '';
          foreach ($value as $td) {
            $newtd .= $td;
          }
          $mastersDays .= '<tr class="crosshover change">
              <th nowrap="" style="font-weight: normal; text-decoration: inherit;">
                  <span>'.$masterInfo[$key]['job'].'</span>
              </th>
              <th nowrap="">
                  <span>'.$masterInfo[$key]['name'].'</span>
              </th>
              <th>
                  <button class="tiny" style="float: right" tooltip="Отправить расписание по SMS">
                      <i class="fa fa-envelope-o"></i></button>
              </th>
              '.$newtd.'
          </tr>';
        }
        $data = ['month' => $monthAll, 'days' => $th, 'nextDays' => $nextDays, 'lastdays' => $lastDays, 'monthlast' => $datemonth[$month],
        'monthnext' => $datemonth[$nextMonth], 'daylast' => $daylast, 'daynext' => $d, 'mastersDays' => $mastersDays];
        return json_encode($data);
      }
      else{
        $th = '';
        for ($i=1; $i <= $diff; $i++) {
          if($i == 25){
            break;
          }
          if($d.'.'.$month == date('d.m')){
            $border = 'style="border: 2px solid #d21a1a"';
          }
          else{
            $border = '';
          }


          foreach ($masters as $key => $master) {
            if($master[$engMonth[$month]] == null){
              if($d == 1 || $i == 1){
                Calendar::insert(['userid' => $master->id, 'month' => (int)$monthmaster, 'year' => date('Y'), 'siteID' => Auth::user()->siteID, 'created_at' => date('Y-m-d Hi:i:s')]);
              }
              $tr[$master->id][] = '<td style="border: 1px solid #ddd" class="checkday" id="'.$d.'" name="'.$master->id.'" month="'.$month.'"><span class="tdspan"></span></td>';
              $masterInfo[$master->id] = ['name' => $master->name, 'job' => $master->job];
            }
            else{
                //dd($master[$engMonth[$month]]['day_'.$d]);
                //dd($month);
                //dd($master[$engMonth[$month]]);
                if($master[$engMonth[$month]]['day_'.$d] == 1){
                  $back = 'tdspanback';
                }
                else{
                  $back = '';
                }

                $tr[$master->id][] = '<td style="border: 1px solid #ddd" class="checkday" id="'.$d.'" name="'.$master->id.'" month="'.$monthmaster.'"><span class="tdspan '.$back.'"></span></td>';
                $masterInfo[$master->id] = ['name' => $master->name, 'job' => $master->job];
                //dd($tr);
            }

          }

           $th .= '<th '.$border.'>'.$d.'</th> ';
           $d++;
        }

        $monthAll = '<th colspan="'.$diff.'" style="border:2px solid #ddd;" class="dinamic">
            <div style="overflow: hidden; width: 100%; text-align:center;">'.$datemonth[$month].'</div>
        </th>';
        $d = $d-1;
        if($d<10){$d= '0'.$d;}else{$d= (string)$d;}
        $mastersDays = '';
        foreach ($tr as $key => $value) {
          $newtd = '';
          foreach ($value as $td) {
            $newtd .= $td;
          }
          $mastersDays .= '<tr class="crosshover change">
              <th nowrap="" style="font-weight: normal; text-decoration: inherit;">
                  <span>'.$masterInfo[$key]['job'].'</span>
              </th>
              <th nowrap="">
                  <span>'.$masterInfo[$key]['name'].'</span>
              </th>
              <th>
                  <button class="tiny" style="float: right" tooltip="Отправить расписание по SMS">
                      <i class="fa fa-envelope-o"></i></button>
              </th>
              '.$newtd.'
          </tr>';
        }
        //dd($mastersDays);
        $data = ['month' => $monthAll, 'days' => $th, 'nextDays' => $nextDays, 'lastdays' => $lastDays, 'monthlast' => $datemonth[$month],
        'monthnext' => $datemonth[$month], 'daylast' => $daylast, 'daynext' => $d, 'mastersDays' => $mastersDays];
        return json_encode($data);
      }


    }

    public function chackday($slug, $day, $month, $user){
      $id = Calendar::where('userid',$user)->where('month', $month)->where('siteID', Auth::user()->siteID)->value('id');
      if($id == null){
        return false;
      }
      $params['add'] = ['day_'.$day => 1];
      $params['remove'] = ['day_'.$day => 0];
      Calendar::where('id', $id)->update($params[$slug]);
    }

    public function days($day, $month){
      $engMonth = ['01' => 'january', '02' => 'february', '03' => 'mart',
      '04' => 'april', '05' => 'may', '06' => 'june', '07' => 'jule', '08' => 'august',
      '09' => 'september', '10' => 'october', '11' => 'november', '12' => 'december'];
      $masters = Users::whereHas($engMonth[$month], function($query)use($day){
        $query->where('day_'.$day,1);
      })->where('userRolle',5)->get();
      return view('admin_salon.work.work', ['masters' => $masters]);
    }

    public function service($type, $id){
       $goods = Goods::with('service_goods')->with('goods_salon')->where('id', $id)->where('siteID', Auth::user()->siteID)->first();
       //dd($goods);
       $masters = Users::where('siteID', Auth::user()->siteID)->where('userRolle', 5)->get();
       foreach ($masters as $master) {
        $optionMaster = '<option value="'.$master->title.'">'.$master->name.'</option>';
       }
       $amount = str_replace(" ","",$goods->coast);
       $tr = '<div class="opt"><span style="float:left;margin-top:5px;">'.$goods->title.'</span>
       <span style="float:right;"><i class="fa fa-times"></i></span>
       <span style="float:right;margin-right:5px;width:9%;"><b class="amount">'.$goods->coast.'р.</b> </span>
       <input type="text" name="percent" value="" placeholder"%" style="width:6%;float:right;margin-right:5px;" class="form-control">
       <input type="text" name="cost" value="'.str_replace(" ","",$goods->coast).'" placeholder"р." style="width:11%;float:right;margin-right:5px;" class="form-control">
       <input type="text" name="counts" value="1" placeholder"шт." style="width:7%;float:right;margin-right:5px;" class="form-control">
       <select name="" style="width:20%;float:right;margin-right:5px;"class="form-control">
       '.$optionMaster.'
       </select>
       <input type="text" name="" value="'.$goods->goods_salon->timework.'" placeholder"мин." style="width:6%;float:right;margin-right:5px;" class="form-control">
       <input type="hidden" value="'.str_replace(" ","",$goods->coast).'" name="hiddenCost">
       <div class="clearfix"></div></div>';
       if($goods->service_goods !== null){
         $i =0;
         foreach ($goods->service_goods as $service_goods) {
           if($i=0){
            $back  = 'background-color:#eee';
             $i++;
           }
           else{
             $back = '';
             $i =0;
           }
           //dd($service_goods);
           $type = Category::where('id', $service_goods->products->category)->where('siteID', Auth::user()->siteID)->value('type');
           if($type == 0){
             $type = 'Т.';
           }
           elseif($type == 3){
            $type = 'М.';
           }
           if($service_goods->payment ==1){$cost = str_replace(" ","",$service_goods->products->coast) * (int)$service_goods->counts;} else{$cost ='';}
           $amount = $amount + $cost;
           //dd($service_goods);
           $tr .= '<div style="margin-top:5px; '.$back.'" class="opt">
             <span style="float:left;margin:5px 20px;"><b>'.$type.'</b> '.$service_goods->products->title.'</span>
             <span style="float:right;"><i class="fa fa-times"></i></span>
             <span style="float:right;margin-right:5px;width:9%;"><b class="amount">'.$cost.'р.</b> </span>
             <input type="text" name="percent" value="" placeholder"%" style="width:6%;float:right;margin-right:5px;" class="form-control">
             <input type="text" name="cost" value="'.$cost.'" placeholder"р." style="width:11%;float:right;margin-right:5px;" class="form-control">
             <input type="text" name="counts" value="'.$service_goods->counts.'" placeholder"шт." style="width:7%;float:right;margin-right:5px;" class="form-control">
             <select name="" style="width:20%;float:right;margin-right:5px;"class="form-control">
             '.$optionMaster.'
             </select>
             <input type="text" name="" placeholder"мин." style="width:6%;float:right;margin-right:5px;" class="form-control">
             <input type="hidden" value="'.$cost.'" name="hiddenCost">
             <div class="clearfix"></div>
             </div>';
         }
       }
       $tr .= '<input type="hidden" name="amount" value="'.$amount.'">';
       return $tr;

    }
}
