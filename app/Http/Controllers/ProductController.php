<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 29.03.2019
 * Time: 12:27
 */

namespace App\Http\Controllers;
use App\Model\Category;
use App\Model\Cities;
use App\Model\Documentations;
use App\Model\Feedback;
use App\Model\Goods;
use App\Model\Portfolio;
use App\Model\Settings;
use App\Model\Information;
use App\Model\Users;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Caching;
use Resize;

class ProductController extends Controller
{
    public function index(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::with('section_image_one')->with('video')->where('siteID', $siteID)->where('type', 4)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo')->first();
        /// Левое меню - получаем главные категории со связкой подкатегорий, далее связка с goods выводит товары /// Загрузка 0.17 сек
        $category_nav = Category::with('sub_category_nav')->where('siteID', $siteID)->where('parent_id', null)->where('type', 0)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Вывод товаров + связка, которая достает url категории /// 0.170
        $goods = Goods::with('images')->with('product_url_cat')->whereHas('product_url_cat', function($query){$query->where('published', 1)->where('type', 0);})->where('siteiD', $siteID)->where('published', 1)->paginate(12);
        /// DOCTYPE
        if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Товары';}
        if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
        if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
        $canonical = $request->getSchemeAndHttpHost().'/product';
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.product.index_1', ['settings' => $settings, 'category_nav' => $category_nav, 'goods' => $goods, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function category(request $request, $urlcategory){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::with('templates')->where('title', $request->getHttpHost())->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $category_id = explode('-', $urlcategory)[0];
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::where('siteID', $siteID)->where('type', 4)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon','city', 'verYa', 'verGo')->first();
        $city_name = Cities::where('id', '=', $favicon->city)->first();
        /// Левое меню - получаем главные категории со связкой подкатегорий
        $category_nav = Category::with('sub_category_nav')->where('siteID', $siteID)->where('parent_id', null)->where('type', 0)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем саму категорию, на которую зашли + получаем товары
        $category = Category::with('bread_cat')->with('video')->where('id', $category_id)->where('published', 1)->where('siteID', $siteID)->first();
        if($category == null){abort(404);}
        /// Получаем подкатегории
        $sub_category = Category::where('parent_id', $category_id)->where('published', 1)->get();
        /// Получаем товары для категории, на которой находимся
        $goods = Goods::with('product_url_scat')->whereHas('product_url_scat', function($query)use($category_id){
            $query->where('published', 1)->where('parent_id', $category_id);
        })->where('siteiD', $siteID)->where('published', 1)->orWhere('category', $category_id)->where('published', 1)->paginate(12);
        /// DOCTYPE
        $t1 = ' купить';
        $g1 = ' в '.$city_name->gorode;
        $d1 = 'Цены, характеристики, отзывы на '.$category->title.' в '.$city_name->gorode.'. Купить с бесплатной доставкой по '.$city_name->gorody;
        if($category->seotitle){$seoTitle = $category->seotitle;}else $seoTitle = $category->title.$t1.$g1;
        if($category->seodescription){$seoDescription = $category->seodescription;}else $seoDescription = $d1;
        if($category->seokeywords){$keywords = $category->seokeywords;}else{$keywords = null;}
        $canonical = $request->getSchemeAndHttpHost().'/product'.'/'.$category->id.'-'.$category->slug;
        
        $view .= view('2019.layouts.doctype', ['settings' => $settings, 'seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.product.category_1', ['settings' => $settings, 'category_nav' => $category_nav, 'category_id' => $category_id, 'category' => $category, 'goods' => $goods, 'sub_category' => $sub_category, 'documents' => $documents]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function view(request $request, $urlcategory, $slug){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::with('templates')->where('title', $request->getHttpHost())->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $product_id = explode('-', $slug)[0];
        $category_id = explode('-', $urlcategory)[0];
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::where('siteID', $siteID)->where('type', 4)->first();
        $product = Goods::with('imageses')->with('video')->with('feedbacks')->where('id', $product_id)->where('published', 1)->where('siteID', $siteID)->first();
        if($product == null){abort(404);}
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon','city', 'verYa', 'verGo', 'metrika_short')->first();
        $city_name = Cities::where('id', '=', $favicon->city)->first();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем главную категорию для товара - breadcrump и ссылки
        $category_parent = Category::with('bread_cat')->where('id', $category_id)->select('id', 'title', 'slug', 'parent_id')->first();
        // портфолио
        $portfolio = Portfolio::with('image_one')->where('siteID', $siteID)->where('published', 1)->limit(16)->get();
        // отзывы
        $reviews = Feedback::with('imageses')->where('siteID', $siteID)->where('goods', $product_id)->where('published', 1)->limit(6)->get();
        /// Получаем менеджера
        $manager = Users::where('siteID', $siteID)->where('onSite', 1)->first();
        /// Получаем похожие услуги
        $similar = Goods::where('category', $category_id)->where('id', '!=', $product_id)->where('published', 1)->limit(4)->get();
        /// DOCTYPE
        if($product->coast) $t1 = ' купить за '.$product->coast.' руб,'; else $t1 = ' купить';
        $g1 = ' в '.$city_name->gorode;
        $d1 = ' купить в '.$city_name->gorode.' - характеристики, фото, отзывы. Звоните!';
        if($product->seotitle){$seoTitle = $product->seotitle;} else $seoTitle = $product->title.$t1.$g1;
        if($product->seodescription){$seoDescription = $product->seodescription;}else $seoDescription = $product->title.$d1;
        if($product->seokeywords){$keywords = $product->seokeywords;}else{$keywords = null;}
        $canonical = $request->getSchemeAndHttpHost().'/product'.'/'.$category_parent->id.'-'.$category_parent->slug.'/'.$product->id.'-'.$product->slug;
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        ///HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.product.view_1', ['settings' => $settings, 'category_parent' => $category_parent, 'product' => $product, 'manager' => $manager, 'portfolio' => $portfolio, 'reviews' => $reviews, 'similar' => $similar, 'documents' => $documents, 'favicon' => $favicon]);
        ///FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        ///END FOOTER
        return $view;
    }
}
