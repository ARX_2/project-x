<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Offer;
use Illuminate\Http\Request;
use App\Template;
use App\Work;
use Query;
use App\Site;
use App\Goods;
use App\Subcategory;
class SiteController extends Controller
{



    static function index(request $request)
    {
      $site = Site::with(['templates' => function($query){
        $query->where('template', 1);
      }])
      ->with('company')->with('category_price')
      ->where('id', 1)->first();
      //login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d
      //dd($site);
      //dd($request->session()->all());

      if($site->templates->texts !== null){
        $seoTitle = $site->templates->texts->seoTitle;
        $seoDescription = $site->templates->texts->seoTitle;
        $keywords = $site->templates->texts->keywords;
      }
      else{
        $seoTitle = null;
        $seoDescription = null;
        $keywords = null;
      }
      //dd($site);
      return view('2019.index', [
        'categories' => $site->templates->categories,
        'banner' => $site->templates->banners,
        'objects' => $site->templates->objects,
        'company' => $site->company,
        'goods' => $site->templates->goods,
        'info' => $site->templates->infos,
        'menu' => $site->templates->menu,
        'template' => $site->templates,
        'slug' => '/',
        'url' => $request->getHttpHost(),
        'price' => $site->category_price,
        'jobs' => $site->templates->jobs,
        'category' =>$site->category,
        'texts' =>$site->templates->texts,
        'seoTitle' => $seoTitle,
        'seoDescription' => $seoDescription,
        'keywords' => $keywords,
      ]);
    }

    public function other(request $request, $slug){
      $site = Site::with(['templates' => function($query) use($slug){
        $query->where('slug', $slug);
      }])->with('category_price')->with('actions')
      ->where('id', 1)->first();
      //dd($site);
      if($site->templates->texts !== null){
        $seoTitle = $site->templates->texts->seoTitle;
        $seoDescription = $site->templates->texts->seoTitle;
        $keywords = $site->templates->texts->keywords;
      }
      else{
        $seoTitle = null;
        $seoDescription = null;
        $keywords = null;
      }
      return view('2019.'.$site->templates->templatename, [
        'categories' => $site->templates->categories,
        'banner' => $site->templates->banners,
        'objects' => $site->templates->objects,
        'company' => $site->templates->companies,
        'goods' => $site->templates->goods,
        'info' => $site->templates->infos,
        'site' => $site->id,
        'menu' => $site->templates->menu,
        'template' => $site->templates,
        'slug' => $slug,
        'url' => $request->getHttpHost(),
        'price' => $site->category_price,
        'jobs' => $site->templates->jobs,
        'category' =>$site->category,
        'texts' =>$site->templates->texts,
        'seoTitle' => $seoTitle,
        'seoDescription' => $seoDescription,
        'keywords' => $keywords,
        'actions' => $site->actions,
      ]);
    }

    static function goods(request $request, $slug){
      if(Auth::user() !== null){
          $userid = Auth::user()->id;
      }
      else{
        $userid = null;
      }

      $site = Site::with(['templates' => function($query) use($slug){
        $query->where('template', 2);
      }])
      ->with(['product' => function($product) use($slug, $userid){
        $product->with(['favorites' => function($favorite) use($userid){
          $favorite->where('userid',$userid);
        }]);
        $product->where('slug', $slug);
      }])
      ->with('user')->with('category_price')
      ->where('id', 1)->first();
      //dd($site);
        $goods = $site->templates->goods->where('parent_id', null);
          $populat = Goods::with('images')->where('status', 3)->latest()->take(3)->get();
          $buing = Goods::with('images')->where('status', 4)->latest()->take(3)->get();
          $count = $goods->count() > 9 ? 10 : $goods->count();
          //dd($goods);
      ///dd($site);
      if($site->product->categories[0]->type == 0){
        $seo1 = 'заказать';
      }
      else{
        $seo1 = 'купить';
      }
      if($site->product->coast !== null){
        $coast = $site->product->coast;
      }
      else{$coast = 'цена договорная';}
      //Конец

      if($site->product->seotitle !== null){
        $seoTitle = $site->product->seotitle;
      }
      else{

        $seoTitle = $site->product->title.' '.$seo1.' за '.$coast.' в '.$site->templates->infos->city;
      }
      if($site->product->seodescription){ $seoDescription = $site->product->seodescription; }
      else{
        $seoDescription = $seo1.' '.$site->product->title.' за '.$coast.' от '.$site->templates->companies->title.' в '.$site->templates->infos->city;
      }

      // <title>{{$product[0]->title or ''}}@if($product[0]->ctype == 0) заказать@else купить@endif за {{$product[0]->coast or 'цена договорная'}} в {{$info->city or ''}}</title>
      // <meta name="description" content="@if($product[0]->ctype == 0) Заказать@else Купить@endif {{$product[0]->title or ''}} за {{$product[0]->coast or 'цена договорная'}} от {{$company[0]->title or ''}} в {{$info->city or ''}}">

      return view('2019.product.index', [
        'goods' => $goods->random($count),
          'product' => $site->product,
          'categories' => $site->templates->categories,
          'company' => $site->templates->companies,
          'objects' => $site->templates->objects,
          'info' => $site->templates->infos,
          'User' => $site->user,
          'site' => $site->id,
          'template' => $site->templates,
          'slug' => 'services/'.$slug,
          'url' => $request->getHttpHost(),
          'seoTitle' => $seoTitle,
          'seoDescription' => $seoDescription,
          'keywords' => $site->product->seokeywords,
          'populat' => $populat,
          'buing' => $buing,
      ]);

    }



    static function category(request $request, $slug)
    {
      $site = Site::with(['templates' => function($query){
        $query->where('template', 7);
      }])
      ->with(['category' => function($product) use($slug){
        $product->where('slug', $slug);
      }])
      ->with('user')->with('category_price')
      ->where('id', 1)->first();

      //dd($site->category->subcategories[0]->goods);
      if(count($site->category->subcategories)>0){
        $subcategory = $site->category->subcategories[0];
      }
      else{
        $subcategory = ['goods' => []];
      }

      return view('2019.category.view', [
        'goods' => $site->templates->goods,
          'categories' => $site->templates->categories,
          'company' => $site->templates->companies,
          'info' => $site->templates->infos,
          'User' => $site->user,
          'category' =>$site->category,
          'template' => $site->templates,
          'slug' => 'category/'.$slug,
          'subcategory' => $subcategory,
      ]);
    }

    static function subcategory(request $request, $slug, $subcategory)
    {
      //dd($slug);
      $site = Site::with(['templates' => function($query){
        $query->where('template', 7);
      }])
      ->with(['category' => function($product) use($slug){
        $product->where('slug', $slug);
      }])
      ->where('id', 1)->first();
    $subcategory = Subcategory::where('slug', $subcategory)->with('goods')->get();
      //dd($subcategory);
      //dd($site->templates);
      //dd($subcategory);
      return view('2019.category.view', [
        'goods' => $site->templates->goods,
          'categories' => $site->templates->categories,
          'company' => $site->templates->companies,
          'info' => $site->templates->infos,
          'User' => $site->user,
          'category' =>$site->category,
          'template' => $site->templates,
          'slug' => 'category/'.$slug,
          'subcategory' => $subcategory,
      ]);
    }

    public function categories(){
      $site = Site::with(['templates' => function($query){
        $query->where('template', 3);
      }])
      ->with('user')->with('category_price')
      ->where('id', 1)->first();
      //dd($site);
      //dd($site->templates);
      $goods = Goods::with('images')->where('parent_id', null)->latest()->take(12)->get();
      $count = $goods->count() > 9 ? 10 : $goods->count();
      //dd($goods);
      return view('2019.'.$site->templates->templatename, [
        'goods' => $goods->random($count),
          'categories' => $site->templates->categories,
          'company' => $site->templates->companies,
          'objects' => $site->templates->objects,
          'info' => $site->templates->infos,
          'User' => $site->user,
          'site' => $site->id,
          'menu' => $site->templates->menu,
          'template' => $site->templates,
          'price' => $site->category_price,
          'jobs' => $site->templates->jobs,
      ]);
    }

    public function filter(Request $request){
      //dd($request);
      if($request->orderby == null){
        $orderby = 'asc';
      }
      else{
        if($request->orderby == 'asc'){
          $orderby = 'desc';
        }
        elseif($request->orderby == 'desc'){
          $orderby = 'asc';
        }
      }
      //dd($orderby);
      $site = Site::with(['templates' => function($query){
        $query->where('template', 9);
      }])
      ->where('id', 1)->first();
      $goods = Goods::with('subgoods');
      if($request->has('mincoast') && $request->has('maxcoast')){
        $goods->whereBetween('coast', [$request->mincoast, $request->maxcoast]);
      }
      else{
        if($request->has('mincoast')){
          $goods->where('coast', $request->mincoast);
        }
        if($request->has('maxcoast')){
          $goods->where('coast', $request->maxcoast);
        }
      }

      if($request->has('color')){

        //dd($request);
        $goods->where('color', $request->color);
      }
      if($request->has('orderby')){
        $goods->orderBy('coast', $request->orderby);
      }
      //dd($goods->get());
      $goods = $goods->get();
      //dd($goods);
      $goods = Goods::whereBetween('coast', [$request->mincoast, $request->maxcoast])
        ->where('color', $request->color)->get();
      //dd($goods);

      return view('2019.category.filter', [
          'categories' => $site->templates->categories,
          'company' => $site->templates->companies,
          'info' => $site->templates->infos,
          'menu' => $site->templates->menu,
          'template' => $site->templates,
          'goods' => $goods,
          'color' => $request->color,
          'mincoast' => $request->mincoast,
          'maxcoast' => $request->maxcoast,
          'orderby' => $orderby,
      ]);
    }

    public function filterCoast(Request $request){
      $site = Site::with(['templates' => function($query){
        $query->where('template', 9);
      }])
      ->where('id', 1)->first();
      if($request)
      $goods = Goods::whereBetween('coast', [$request->mincoast, $request->maxcoast])
        ->where('color', $request->color)->get();
      //dd($goods);
      return view('2019.category.filter', [
          'categories' => $site->templates->categories,
          'company' => $site->templates->companies,
          'info' => $site->templates->infos,
          'menu' => $site->templates->menu,
          'template' => $site->templates,
          'goods' => $goods,
          'color' => $request->color,
          'mincoast' => $request->mincoast,
          'maxcoast' => $request->maxcoast
      ]);
    }

    public function mail(Request $request)
    {
      //dd($request);
        $siteid = DB::table('sites')->where('title', $request->getHttpHost())->value('id');
        $to = DB::table('information')->where('siteID', $siteid)->value('email');

        //dd($request);uralpoliplastic@yandex.ru
        //dd($to);
        $subject = "Сообщение от " . $request->getHttpHost();

        $message = '
            <html>
                <head>
                    <title>Новое сообщение</title>
                      <meta charset="utf-8">
                </head>
                <body>
                    <p>Добрый день! Поступил заказ на звонок</p>
                    <p>тел.' . $request->phone . '</p>
                    <p>Имя.' . $request->name . '</p>
                </body>
            </html>';

        $headers = "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= "From: Birthday Reminder " . $to . "\r\n";
        $headers .= "Bcc: birthday-archive@example.com\r\n";

        mail($to, $subject, $message, $headers);
        $site = Site::with(['templates' => function($query){
          $query->where('template', 1);
        }])->with('category_price')
        ->with('company')
        ->where('title', $request->getHttpHost())->first();
        //dd($site->templates);
        if($site->templates->templatename == 'povishev.ru'){
          $templatename = 'povishev';
        }
        else{
          $templatename = $site->templates->templatename;
        }
        if($site->type == 1){
          $type = 'site';
        }
        else{
          $type = 'salon';
        }
        return view('index', [
          'categories' => $site->templates->categories,
          'seos' => $site->templates->seos,
          'banner' => $site->templates->banners,
          'objects' => $site->templates->objects,
          'company' => $site->company,
          'goods' => $site->templates->goods,
          'info' => $site->templates->infos,
          'companyprices' => $site->templates->companyprices,
          'site' => $site->id,
          'menu' => $site->templates->menu,
          'includes' => $site->templates->includes,
          'template' => $site->templates,
          'slug' => '/',
          'mail' => 1,
          'url' => $request->getHttpHost(),
          'jobs' => $site->templates->jobs,
          'price' => $site->category_price,
        ]);
    }




}
