<?php

namespace App\Http\Controllers;

use App\Offer;
use Illuminate\Http\Request;
use App\Services;
use DB;
use App\Category;
use Carbon;
use App\Calendar;
use App\Work;
class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    //  dd($-sadsad);
  //  sadsad
      return view('admin.offers.index', [

      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        //
    }

    public function services(Request $request){

        $imageRand = rand(1,99);
        //dd($imageRand);
        $site = DB::table('sites')->where('title', $request->getHttpHost())->value('id');
        return view('2019.offer.services', [
          'services' => Services::where('services.siteID', $site)->get(),
          'categores' => Category::
          select('categories.id', 'categories.title', 'cs.category', 'cs.services')
          ->leftJoin('category_services as cs', 'categories.id', '=', 'cs.category')
          ->where('categories.siteID', $site)
          ->where('cs.category', '!=', null)
          ->get(),
          'imageRand' => $imageRand,
        ]);
        //dd($request);

    }

    public function date(Request $request){
      $site = DB::table('sites')->where('title', $request->getHttpHost())->value('id');
      $tomorrow = carbon\Carbon::parse(date('d-m-Y'))->addDay(1)->toDateString();
      $tomorrow = date("d.m", strtotime($tomorrow));
      $afterTomorrow = carbon\Carbon::parse(date('d-m-Y'))->addDay(2)->toDateString();

      $afterTomorrow = date("d.m", strtotime($afterTomorrow));

      $servicename = Services::where('siteID', $site)->where('id', $request->service)->value('services');
      //dd($servicename);
      //dd($tomorrow);

      //dd($request);
      $imageRand = rand(1,99);
      return view('2019.offer.date', [
        'services' => $request->service,
        'servicename' => $servicename,
        'today' => date('d.m'),
        'tomorrow' => $tomorrow,
        'afterTomorrow' => $afterTomorrow,
        'imageRand' => $imageRand,
      ]);


    }

    public function master(Request $request){

  $site = DB::table('sites')->where('title', $request->getHttpHost())->value('id');
      $date = $request->date.'.'.date('Y');

      $dateCalendar = explode('.',$date);
      $day = 'day_'.$dateCalendar[0];
      //dd($day);
      //dd($request->services);
      $masters = Calendar::
      leftJoin('master_serveces as ms', 'calendars.userid', '=', 'ms.master')
      ->leftJoin('services as s', 'ms.serveces', '=', 's.id')
      ->join('users as u', 'calendars.userid', '=', 'u.id')
      ->where('s.siteID', $site)
      ->where($day, 1)
      ->where('month', $dateCalendar[1])
      ->where('year', $dateCalendar[2])
      ->where('s.id', $request->services)
      ->get();

      //dd($masters);
      //dd($date);
      if($date == date('d.m.Y')){
        $datename = '«Сегодня»';
      }
      else{
        $datename = $date;
      }
      $servicename = Services::where('siteID', $site)->where('id', $request->services)->value('services');
      //dd($servicename);
      $imageRand = rand(1,99);
      return view('2019.offer.master', [
        'datename' => $datename,
        'date' => $date,
        'masters' => $masters,
        'services' => $request->services,
        'servicename' => $servicename,
        'imageRand' => $imageRand,
      ]);
    }

    public function actualTime(Request $request){
      //dd($request);
      $site = DB::table('sites')->where('title', $request->getHttpHost())->value('id');
      //dd($request);
      $servicesTime = Services::where('siteID', $site)->where('id', $request->services)->first();
      $user = DB::table('users')->where('siteID', $site)->where('id', $request->master)->value('name');
      if($servicesTime == null || $user == null){
        echo '<h2>Пользователь или сервис не обнаружен!</h2>';
        die();
      }
      $date = date("Y-m-d", strtotime($request->date));
      //dd($date);
      $work = Work::where('master', $request->master)->where('date', $date)->get();
      //dd($work);
      //dd($work);
      //dd($masterTimeEnd);
      $timeSet = array();

      $timeSet[] = $date.' 08:00:00';
        //$masterTime = Work::select('timeFrom', 'timeTo', 'id')->where('master', 15)->get();
        $returnTime = array();
        //dd($masterTimeEnd);
        $watchTime = array();
      for($i=0;$i<=39;$i++){


        $timeToTimeSet = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeSet[$i])->addMinutes($servicesTime->time*15)->toDateTimeString();


        foreach ($work as $w) {

        $timeTo =  \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $w->timeTo)->subMinutes(1)->toDateTimeString();
        //dd($timeTo);
          //dd($w->timeFrom);
          if($w->timeFrom <= $timeSet[$i] && $timeTo >= $timeSet[$i] || $w->timeFrom <= $timeToTimeSet && $w->timeTo >= $timeToTimeSet ){
            //dd($timeSet);
            $break = 1;
            break;
          }
          else{
            $break = 0;
          }
        }
        if(!isset($break)){
          $break = null;
        }
        if($break == 1){
          //dd($timeTo);
          if(!isset($watchTime[$timeTo])){

            $watchTime[$timeTo] = 1;
            $returnTime[] = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeTo)->addMinutes(1)->toDateTimeString();
          }

          $timeSet[] = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeSet[$i])->addMinutes(15)->toDateTimeString();
          continue;
        }
        //dd($date.'14:00:00');

            $timeSet[] = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeSet[$i])->addMinutes(15)->toDateTimeString();
            $returnTime[] = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeSet[$i])->addMinutes(15)->toDateTimeString();


        //dd($i);


      }

      //dd($returnTime);
      if($request->date == date('d.m.Y')){
        $datename = '«Сегодня»';
      }
      else{
        $datename = $request->date;
      }
      $date = date("d.m", strtotime($request->date));
      //dd($datename);
      $imageRand = rand(1,99);
      $servicename = Services::where('siteID', $site)->where('id', $request->services)->value('services');
      return view('2019.offer.actualTime', ['time' => $returnTime,
      'userid' => $request->master,
      'serviceTime'=> $servicesTime->time,
      'services' => $request->services,
      'servicename' => $servicename,
      'master' =>$user,
      'datename'=> $datename,
      'date'=>$date,
      'imageRand' => $imageRand,
    ]);

    }

    public function form(Request $request){
      $site = DB::table('sites')->where('title', $request->getHttpHost())->value('id');
      $servicesTime = Services::where('siteID', $site)->where('id', $request->services)->first();
      $user = DB::table('users')->where('siteID', $site)->where('id', $request->master)->value('name');

      if($servicesTime == null || $user == null){
        echo '<h2>Пользователь или сервис не обнаружен!</h2>';
        die();
      }
      $date = $request->date.'.'.date('Y');

      $date = date("Y-m-d", strtotime($date));
      //dd($date);
      //dd($date);
      $work = Work::where('master', $request->master)->where('date', $date)->get();
      //dd($work);

        $time = explode('-', $request->time);
        $timeFrom = $date.' '.$time[0];
        $timeTo = $date.' '.$time[1];
        //dd($timeTo);

        foreach ($work as $w) {

        $timeFrom =  \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeFrom)->subMinutes(1)->toDateTimeString();
        $timeTo =  \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeTo)->subMinutes(1)->toDateTimeString();

        //dd($timeFrom);
          if($w->timeFrom <= $timeFrom && $w->$timeTo >= $timeFrom || $w->timeFrom <= $timeTo && $w->timeTo >= $timeTo ){
            //dd($timeTo);
            echo '<h2>Извините но это время уже успели забронировать</h2>';
            die();
          }
          }
          if($request->date.'.'.date('Y') == date('d.m.Y')){
            $datename = '«Сегодня»';
          }
          else{
            $datename = $request->date;
          }


          $imageRand = rand(1,99);
          return view('2019.offer.form', [
          'time' => $request->time,
          'userid' => $request->master,
          'serviceTime'=> $servicesTime->time,
          'services' => $request->services,
          'servicename' => $servicesTime->services,
          'master' =>$user,
          'datename'=> $datename,
          'date'=>$date,
          'imageRand' => $imageRand,
        ]);
        //dd($site);



    }

    public function send(Request $request){


      $site = DB::table('sites')->where('title', $request->getHttpHost())->value('id');
      $servicesTime = Services::where('siteID', $site)->where('id', $request->services)->first();
      $user = DB::table('users')->where('siteID', $site)->where('id', $request->userid)->value('name');
      //dd($request);
      if($servicesTime == null || $user == null){
        echo '<h2>Пользователь или сервис не обнаружен!</h2>';
        die();
      }
      $date = $request->date;
      //dd($date);
      //dd($date);
      $work = Work::where('master', $request->userid)->where('date', $date)->get();
      //dd($work);
      //dd($request->time);
        $time = explode('-', $request->time);
        $timeFrom = $date.' '.$time[0];
        $timeTo = $date.' '.$time[1];
        //dd($timeTo);
        // /dd($timeFrom);
        foreach ($work as $w) {
        $timeFrom =  \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeFrom)->subMinutes(1)->toDateTimeString();
        $timeTo =  \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $timeTo)->subMinutes(1)->toDateTimeString();
          if($w->timeFrom <= $timeFrom && $w->$timeTo >= $timeFrom || $w->timeFrom <= $timeTo && $w->timeTo >= $timeTo ){
            //dd($timeTo);
            echo '<h2>Извините но это время уже успели забронировать</h2>';
            die();
          }
    }
    if($request->date == date('Y-m-d')){
      $datename = '«Сегодня»';
    }
    else{
      $datename = $request->date;
    }

    $time = explode('-', $request->time);
    $timeFrom = $date.' '.$time[0];
    $timeTo = $date.' '.$time[1];
  //dd($request);

    Work::create(['master' => $request->userid,
    'timeFrom' => $timeFrom,
    'timeTo' => $timeTo,
    'servicesid' => $request->services,
    'name' => $request->name,
    'tel' => $request->tel,
    'status' => 0,
    'date' => $date,
    'siteID' => $site,

    ]);


    //dd($request);
    $imageRand = rand(1,99);
    return view('2019.offer.form', [
    'time' => $request->time,
    'userid' => $request->userid,
    'serviceTime'=> $servicesTime->time,
    'services' => $request->services,
    'servicename' => $servicesTime->services,
    'master' =>$user,
    'datename'=> $datename,
    'date'=>$date,
    'imageRand' => $imageRand,
    'send' => 1,
  ]);

  }
}
