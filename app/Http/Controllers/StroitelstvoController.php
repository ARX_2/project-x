<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 17.05.2019
 * Time: 9:57
 */

namespace App\Http\Controllers;
use App\Model\Category;
use App\Model\Cities;
use App\Model\Documentations;
use App\Model\Feedback;
use App\Model\Goods;
use App\Model\Goods_doma_price;
use App\Model\Portfolio;
use App\Model\Settings;
use App\Model\Information;
use App\Model\Users;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Caching;
use Resize;

class StroitelstvoController extends Controller
{
    public function index(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::with('section_image_one')->where('siteID', $siteID)->where('type', 15)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo', 'metrika_short')->first();
        /// Левое меню - получаем главные категории со связкой подкатегорий, далее связка с goods выводит товары
        $category_nav = Category::with('sub_category_nav')->where('siteID', $siteID)->where('parent_id', null)->where('type', 4)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Вывод товаров + связка, которая достает url категории
        $goods = Goods::with('goods_doma')->with('product_url_cat')->whereHas('product_url_cat', function($query){$query->where('published', 1)->where('type', 4);})->where('siteiD', $siteID)->where('published', 1)->paginate(12);
        // портфолио
        $portfolio = Portfolio::with('image_one')->where('siteID', $siteID)->where('published', 1)->limit(16)->get();
        // отзывы
        $reviews = Feedback::with('imageses')->where('siteID', $siteID)->where('section', 4)->limit(9)->get();
        /// DOCTYPE
        if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Строительство';}
        if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
        if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
        $canonical = $request->getSchemeAndHttpHost().'/stroitelstvo';
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.stroitelstvo.index_1', ['settings' => $settings, 'category_nav' => $category_nav, 'goods' => $goods, 'portfolio' => $portfolio, 'reviews' => $reviews, 'documents' => $documents, 'favicon' => $favicon]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function category(request $request, $urlcategory){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::with('templates')->where('title', $request->getHttpHost())->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $category_id = explode('-', $urlcategory)[0];
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo', 'metrika_short')->first();
        /// Левое меню - получаем главные категории со связкой подкатегорий
        $category_nav = Category::with('sub_category_nav')->where('siteID', $siteID)->where('parent_id', null)->where('type', 4)->where('published', 1)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем саму категорию, на которую зашли + получаем товары
        $category = Category::with('images')->with('bread_cat')->where('id', $category_id)->where('published', 1)->where('siteID', $siteID)->first();
        if($category == null){abort(404);}
        /// Получаем подкатегории
        $sub_category = Category::where('parent_id', $category_id)->where('published', 1)->get();
        // портфолио
        $portfolio = Portfolio::with('image_one')->where('siteID', $siteID)->where('published', 1)->limit(16)->get();
        // отзывы
        $reviews = Feedback::with('imageses')->where('siteID', $siteID)->where('category', $category_id)->limit(9)->get();
        /// Получаем товары для категории, на которой находимся
        $products_form_cat = Goods::with('goods_doma')->with('product_url_cat')->whereHas('product_url_cat', function($query){$query->where('published', 1)->where('type', 4);})
            ->where('siteID', $siteID)->where('published', 1)->orderByRaw('category = '.$category_id.' DESC')->paginate(12);
        /// DOCTYPE
        if($category->seotitle){$seoTitle = $category->seotitle;}else{$seoTitle = $category->title;}
        if($category->seodescription){$seoDescription = $category->seodescription;}else{$seoDescription = null;}
        if($category->seokeywords){$keywords = $category->seokeywords;}else{$keywords = null;}
        $canonical = $request->getSchemeAndHttpHost().'/stroitelstvo'.'/'.$category->id.'-'.$category->slug;
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.stroitelstvo.category_1', ['category_nav' => $category_nav, 'category_id' => $category_id, 'category' => $category, 'portfolio' => $portfolio, 'reviews' => $reviews, 'products_form_cat' => $products_form_cat, 'sub_category' => $sub_category, 'documents' => $documents, 'favicon' => $favicon]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function view(request $request, $urlcategory, $slug){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::with('templates')->where('title', $request->getHttpHost())->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $product_id = explode('-', $slug)[0];
        $category_id = explode('-', $urlcategory)[0];
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $product = Goods::with('goods_doma')->with('imageses')->where('id', $product_id)->where('published', 1)->where('siteID', $siteID)->first();
  
        if($product == null){abort(404);}
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon','city', 'verYa', 'verGo', 'metrika_short')->first();
        $city_name = Cities::where('id', '=', $favicon->city)->first();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем главную категорию для товара - breadcrump и ссылки
        $category_parent = Category::with('bread_cat')->where('id', $category_id)->select('id', 'title', 'slug', 'parent_id')->first();
        /// Получаем менеджера
        $manager = Users::where('siteID', $siteID)->where('onSite', 1)->first();
        $company = Information::with('sites')->where('siteID', $siteID)->first();
        // получаем цены для "комплектации и цены"
        $goods_doma_price = Goods_doma_price::where('siteID', $siteID)->first();
        
        // портфолио
        $portfolio = Portfolio::with('image_one')->where('siteID', $siteID)->where('published', 1)->limit(16)->get();
        // отзывы
        $reviews = Feedback::with('imageses')->where('siteID', $siteID)->where('goods', $product_id)->limit(6)->get();
        /// Получаем похожие услуги
        $similar = Goods::with('goods_doma')->where('category', $category_id)->where('id', '!=', $product_id)->where('published', 1)->limit(4)->get();

        /// DOCTYPE
        $t1 = 'Проект дома «'.$product->title.'»'.' в '.$city_name->gorode;
        $d1 = 'Проект дома «'.$product->title.'»'.' - возможна разная комплектация: из оцилиндрованного или рубленного бревна, из профилированного или клееного бруса. Так-же можем построить каркасный дом по данному проекту. Звоните!';
        if($product->seotitle) $seoTitle = $product->seotitle; else $seoTitle = $t1;
        if($product->seodescription) $seoDescription = $product->seodescription; else $seoDescription = $d1;
        if($product->seokeywords){$keywords = $product->seokeywords;}else{$keywords = null;}
        $canonical = $request->getSchemeAndHttpHost().'/stroitelstvo'.'/'.$category_parent->id.'-'.$category_parent->slug.'/'.$product->id.'-'.$product->slug;
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        ///HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.stroitelstvo.view_1', ['category_parent' => $category_parent, 'product' => $product, 'manager' => $manager, 'similar' => $similar, 'goods_doma_price' => $goods_doma_price, 'portfolio' => $portfolio, 'reviews' => $reviews, 'documents' => $documents, 'favicon' => $favicon]);
        ///FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        ///END FOOTER
        return $view;
    }
}
