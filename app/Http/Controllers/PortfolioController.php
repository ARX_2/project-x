<?php
/**
 * Created by PhpStorm.
 * User: Lipatkin
 * Date: 18.03.2019
 * Time: 16:32
 */

namespace App\Http\Controllers;
use App\Category;
use App\Model\Documentations;
use App\Model\Portfolio;
use App\Model\Information;
use App\Model\Settings;
use App\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Caching;
use Resize;
use App\User;
class PortfolioController extends Controller
{
    public function index(request $request){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $settings = Settings::with('section_image_one')->where('siteID', $siteID)->where('type', 6)->first();
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo', 'metrika_short')->first();
        /// Получаем категории для формирования левого меню + связка с портфолио для определения массива категорий
        $category_nav = Category::with('objects')->with('images')->whereHas('objects', function($query){$query->where('parent_id', '!=', null)->where('published', 1);})->where('siteiD', $siteID)->where('published', 1)->select('id','parent_id','seoname','title','slug','published', 'articul')->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем список портфолио
        $portfolio = Portfolio::with('imageses')->with('category_parent')->whereHas('category_parent', function($query){$query->where('id', '!=', null);})->where('siteID', $siteID)->where('published', 1)->where('parent_id', '!=', null)->paginate(12);

        /// DOCTYPE
        if($settings->seoTitle){$seoTitle=$settings->seoTitle;}elseif($settings->title){$seoTitle=$settings->title;}else{$seoTitle='Портфолио';}
        if($settings->seoDescription){$seoDescription = $settings->seoDescription;}else{$seoDescription = null;}
        if($settings->seokeywords){$keywords = $settings->seokeywords;}else{$keywords = null;}
        $canonical = $request->url();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.portfolio.index_1', ['settings' => $settings, 'portfolio' => $portfolio, 'category_nav' => $category_nav, 'documents' => $documents, 'favicon' => $favicon]);
        /// FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        return $view;
    }

    public function category(request $request, $urlcategory){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $category_id = explode('-', $urlcategory)[0];

        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo', 'metrika_short')->first();

        /// Получаем категории для формирования левого меню + связка с портфолио для определения массива категорий
        $category_nav = Category::with('objects')->whereHas('objects', function($query){$query->where('parent_id', '!=', null)->where('published', 1);})->where('published', 1)->where('siteiD', $siteID)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем категорию
        $category = Category::where('id', $category_id)->where('published', 1)->select('id','parent_id','seoname','title','slug')->first();
        /// Получаем список портфолио для категории
        $portfolio = Portfolio::with('image_one')->where('siteID', $siteID)->where('parent_id', $category_id)->where('published', 1)->paginate(12);

        /// DOCTYPE
        $seoTitle = $category->title . ' - наши работы';
        $seoDescription = null;
        $keywords = null;
        $canonical = $request->url();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.portfolio.category_1', ['portfolio' => $portfolio, 'category_nav' => $category_nav, 'category' => $category, 'favicon' => $favicon, 'documents' => $documents]);
        ///FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        ///END FOOTER
        return $view;
    }

    public function view(request $request, $urlcategory, $slug){
        if (Cache::has('/' . $request->getHttpHost())) {}
        else {$site = Site::where('title', $request->getHttpHost())->with('templates')->first();Cache::forever('/' . $request->getHttpHost(), $site);}
        $view = '';
        $siteID = Cache::get('/' . $request->getHttpHost())->id;
        $portfolio_id = explode('-', $slug)[0];
        $category_id = explode('-', $urlcategory)[0];
        $favicon = Information::where('siteID', $siteID)->select('id','siteID','favicon', 'verYa', 'verGo', 'metrika_short')->first();
        /// Получаем категории для формирования левого меню + связка с портфолио для определения массива категорий
        $category_nav = Category::with('objects')->whereHas('objects', function($query){$query->where('parent_id', '!=', null)->where('published', 1)->where('published', 1);})->where('siteiD', $siteID)->get();
        /// Получаем документы для формирование в левом меню
        $documents = Documentations::where('siteID', $siteID)->where('popular', 1)->limit(6)->get();
        /// Получаем категорию для объекта портфолио
        $category = Category::where('id', $category_id)->select('id','parent_id','title','slug')->first();
        /// Получаем объект портфолио
        $portfolio = Portfolio::with('images')->where('siteID', $siteID)->where('id', $portfolio_id)->first();
        /// DOCTYPE
        $seoTitle = $portfolio->title;
        $seoDescription = null;
        $keywords = null;
        $canonical = $request->url();
        $view .= view('2019.layouts.doctype', ['seoTitle' => $seoTitle, 'seoDescription' => $seoDescription, 'keywords' => $keywords, 'favicon' => $favicon, 'canonical' => $canonical]);
        /// HEADER
        if (Cache::has('menu_1_' . $siteID)){$view .= Cache::get('menu_1_' . $siteID);}else{Caching::menu();$view .= Cache::get('menu_1_' . $siteID);}
        /// CONTENT
        $view .= view('2019.portfolio.view_1', ['portfolio' => $portfolio, 'category_nav' => $category_nav, 'category' => $category, 'documents' => $documents]);
        ///FOOTER
        if (Cache::has('footer_1_' . $siteID)){$view .= Cache::get('footer_1_' . $siteID);}else{Caching::footer();$view .= Cache::get('footer_1_' . $siteID);}
        ///END FOOTER
        return $view;
    }

    public function show(Request $request,$id){
    $portfolio =  Portfolio::with('imageses')->where('id', $id)->first();
    $siteID = Cache::get('/' . $request->getHttpHost())->id;
    $manager = User::where('onSite', 1)->where('siteID', $siteID)->first();
    return view('2019.portfolio.modal', compact('manager', 'portfolio'));
    $data = ['portfolio' => $portfolio, 'manager' => $manager];
    return json_encode($data, true);
    }
}
