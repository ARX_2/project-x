<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Site extends Model
{
    public function sites(){
      return $this->hasOne('App\Site', 'id', 'site')->with('info');
    }
}
