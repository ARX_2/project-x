<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  protected $fillable = ['support_id', 'users_id', 'text', 'datecreate', 'created_by', 'modified_by'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }

  public function support(){

      return $this->belongsTo('App\Support', 'support_id', 'id')->orderBy('id', 'desc');
  }
  public function users(){
      return $this->hasOne('App\Users', 'id', 'users_id');
  }
  public function docs(){
      return $this->hasMany('App\SupportFile');
  }
}
