<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumMessage extends Model
{
    public function user(){
      return $this->hasOne('App\Users', 'id', 'userid');
    }
    public function subcategory(){
      return $this->hasOne('App\Forumsubcategory', 'id', 'forumsubcategoryid')->where('archive', '=', null)->with('category');
    }
}
