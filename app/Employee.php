<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
  protected $fillable = ['name', 'lastname', 'email', 'password', 'description', 'created_at', 'updated_at', 'userRolle', 'siteID', 'job', 'sex'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function users(){
      return $this->has();
    }
}
