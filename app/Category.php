<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;
class Category extends Model
{
    protected $fillable = ['title', 'type', 'slug', 'main', 'parent_id', 'published', 'created_by', 'siteID', 'modified_by', 'seoname', 'seotitle', 'seodescription', 'seokeywords', 'description', 'youtube', 'deleted', 'checkseo', 'sort', 'offer'];

    public function setSlugAttribute($value){
      //dd($this->title);
      $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
    }

    public function children(){

      return $this->hasMany(self::class, 'parent_id');
    }

    public function images(){
      return $this->hasOne('App\Image', 'category', 'articul');
    }
    public function imageses(){
      return $this->hasMany('App\Image', 'category', 'articul');
    }
    public function goods(){
      return $this->hasMany('App\Goods', 'category', 'id')->with('Goods_object')->with('images')->where('published', 1);
    }

    public function services(){
      return $this->hasMany('App\Goods', 'category', 'id')->with('Goods_object')->where('published', 1);
    }
    public function subcategories(){
      return $this->hasMany('App\Category', 'parent_id', 'id')->where('published',1)->with('goods')->with('services')->with('images')->orderBy('sort', 'asc');
    }
    public function subcategoriesAll(){
      return $this->hasMany('App\Category', 'parent_id', 'id')->with('goods')->with('images')->orderby('sort', 'asc');
    }
    public function categor(){
      return $this->hasOne('App\Category', 'id', 'parent_id')->with('section');
    }
    public function objects(){
         return $this->hasMany('App\Objects', 'parent_id', 'id');
    }
    public function goodsAll(){
       return $this->hasMany('App\Goods', 'category', 'id')->with('imageses');
    }
    public function section(){

      return $this->hasOne('App\Section', 'type', 'type');
    }
    // public function images(){
    //   return $this->hasMany('App\Image', 'company', 'id')->with('imageResize');
    // }
}
