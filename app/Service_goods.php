<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service_goods extends Model
{
    public function products(){
      return $this->hasOne('App\Goods', 'id', 'goods');
    }
}
