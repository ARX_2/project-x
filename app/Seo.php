<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
  protected $fillable = ['title', 'locate', 'description', 'keywords', 'siteID', 'created_by', 'modified_by'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function seo(){
      return $this->has();
    }
}
