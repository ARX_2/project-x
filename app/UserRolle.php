<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRolle extends Model
{
    public function users(){
      return $this->hasMany('App\Users', 'userRolle', 'id')->with('users_salon');
    }
}
