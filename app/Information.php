<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{


  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }

  public function contacts(){
    return $this->has();
  }
  public function sections(){
    return $this->hasMany('App\Section', 'siteID', 'siteID')->where('published',1);
  }
  public function cities(){
    return $this->hasOne('App\City', 'id', 'city');
  }
}
