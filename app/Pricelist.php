<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pricelist extends Model
{
    public function category(){
      return $this->hasOne('App\Category_price', 'id', 'category');
    }
}
