<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
  protected $fillable = ['title', 'type', 'color', 'image', 'text', '
  published', 'siteID', 'created_by', 'modified_by', 'main'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function banners(){
      return $this->has();
    }
    public function imageResize(){
      return $this->hasMany('App\ImageResize', 'image', 'image');
    }
}
