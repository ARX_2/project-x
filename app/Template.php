<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
  protected $fillable = ['templatename', 'template', 'siteID', 'created_at', 'updated_at'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function templates(){
      return $this->has();
    }

    public function seos(){
      return $this->hasOne('App\Seo', 'locate', 'template');
    }
    public function companies(){
      return $this->hasOne('App\Company', 'siteID', 'siteID')->with('images');
    }

    public function goods(){
      return $this->hasMany('App\Goods', 'siteID', 'siteID')->with('images')->with('subcategory')->where('published', 1);
    }
    public function categories(){
      return $this->hasMany('App\Category', 'siteID', 'siteID')->with('subcategories')->with('images')->where('published', 1)->orderBy('sort', 'asc');
    }
    public function infos(){
      return $this->hasOne('App\Information', 'siteID', 'siteID');
    }
    public function companyprices(){
      return $this->hasMany('App\Companyprice', 'siteID', 'siteID');
    }
    public function objects(){
      return $this->hasMany('App\Objects', 'siteID', 'siteID')->with('imageResize')->where('published', 1);
    }
    public function menues(){
      return $this->hasOne('App\Menu', 'siteID', 'siteID');
    }
    public function banners(){
      return $this->hasMany('App\Banner', 'siteID', 'siteID')->with('imageResize')->where('published', 1);
    }
    public function menu(){
      return $this->hasMany('App\Menu', 'siteID', 'siteID');
    }

    public function includes(){
      return $this->hasMany('App\Includes', 'template', 'id');
    }
    public function pricelist(){
      return $this->hasMany('App\Pricelist', 'siteID', 'siteID');
    }
    public function jobs(){
      return $this->hasMany('App\Jobs', 'siteID', 'siteID')->where('published',1);
    }
    public function texts(){
      return $this->hasOne('App\Text', 'id', 'setting')->with('images');
    }
    public function site(){
      return $this->hasOne('App\Site', 'id', 'siteID');
    }
    public function section(){
      return $this->hasOne('App\Section', 'slug', 'slug');
    }
}
