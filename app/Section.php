<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    //
    public function category(){
        return $this->hasMany('App\Category', 'type', 'type')->where('published', 1)->limit(10);
    }
    public function texts(){
        return $this->hasOne('App\Text', 'id', 'setting')->with('images');;
    }
}
