<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
  protected $fillable = ['title', 'text', 'image', 'published', 'siteID', 'created_by', 'modified_by'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function opinions(){
      return $this->has();
    }
    public function imageResize(){
      return $this->hasOne('App\ImageResize', 'image', 'image');
    }
}
