<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
  protected $fillable = ['master', 'timeFrom', 'date', 'timeTo', 'servicesid', 'name', 'tel', 'status', 'siteID'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }

  public function offers(){
    return $this->has();
  }
}
