<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Helpers;

use DB;
use Auth;
use App\Employee;
use ImageResize;

class Resize {

static function ResizeImage($image, $size){

  //dd($size);
  if($image->images !== null){
    $imageName = $image->images;
  }
  else{
    $imageName = $image->image;
  }

  if($image->imageResize == null){
    $imagename = explode('.', $imageName);
    $sizearray = explode('x', $size);
    //dd($image);

    ImageResize::make('storage/app/'.$imageName)
    ->resize($sizearray[0], $sizearray[1],function ($constraint) {
          $constraint->aspectRatio();
          $constraint->upsize();
      })
    ->save('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);
    dd($sizearray);
    $optimizerChain = app(\Spatie\ImageOptimizer\OptimizerChain::class)->optimize('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);
      $img =DB::table('image')->where('image', $imageName)->value('image');
      if($img == null){
        DB::table('image')->insert(['image' => $imageName,'imagesize' => $imagename[0], 'fullimagesize' => $imagename[0].'_'.$size.'.'.$imagename[1], 'size' => $size, 'mele' => $imagename[1]]);
      }
  }
  else{
    if($image->imageResize->size !== $size){
      $imagename = explode('.', $imageName);
      $sizearray = explode('x', $size);

      ImageResize::make('storage/app/'.$imageName)
      ->resize($sizearray[0], $sizearray[1],function ($constraint) {
          $constraint->aspectRatio();
          $constraint->upsize();
      })
      ->save('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);

      $optimizerChain = app(\Spatie\ImageOptimizer\OptimizerChain::class)->optimize('storage/app/'. $imagename[0].'_'.$size.'.'.$imagename[1]);
        $img =DB::table('image')->where('image', $imageName)->value('image');
        if($img == null){
          DB::table('image')->insert(['image' => $imageName,'imagesize' => $imagename[0], 'fullimagesize' => $imagename[0].'_'.$size.'.'.$imagename[1], 'size' => $size, 'mele' => $imagename[1]]);
        }
    }
  }

}
}
