<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    public function images(){
      return $this->hasMany('App\Image', 'text', 'id');
    }
    public function template(){
      return $this->hasOne('App\Template', 'setting', 'id');
    }
}
