<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class Subcategory extends Model
{

    public function images(){
      return $this->hasOne('App\Image', 'subcategory', 'id');
    }
    public function goods(){
      return $this->belongsToMany('App\Goods', 'category_goods', 'category', 'goods')->with('images');
    }
    public function categor(){
      return $this->hasOne('App\Category', 'id', 'category')->with('images');
    }
    public function goods12(){
      return $this->belongsToMany('App\Goods', 'category_goods', 'category', 'goods')->limit(12);
    }
    public function goodsPaginate(){
        return $this->belongsToMany('App\Goods', 'category_goods', 'category', 'goods');
    }

}
