<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
  protected $fillable = ['Lname', 'Fname', 'text', 'Mname', 'tel', 'City', 'TimeJob', 'published', 'created_by', 'modified_by', 'title'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }

  public function contacts(){
    return $this->has();
  }
}
