<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    public function product(){
      return $this->hasOne('App\Goods', 'id', 'goods')->with('images');
    }
    public function address(){
      return $this->hasOne('App\Address', 'userid', 'userid')->where('main', 1);
    }
}
