<?php


namespace App;
use Illuminate\Database\Eloquent\Model;

class Support_message extends Model
{
    protected $table = 'support_message';

    public function user(){
        return $this->hasOne('App\Users', 'id', 'user_id');
    }
}