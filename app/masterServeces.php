<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class masterServeces extends Model
{
  protected $fillable = ['master', 'serveces', 'siteID'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function users(){
      return $this->has();
    }
}
