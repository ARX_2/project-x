<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $softDelete = true;

    public function product(){
      return $this->hasOne('App\Goods', 'id', 'goods');
    }
    public function user(){
        return $this->hasOne('App\Model\Users', 'id', 'user_id');
    }
    public function order_customer(){
        return $this->hasOne('App\Model\Order_customer', 'order_id', 'id');
    }
    public function order_message(){
        return $this->hasMany('App\Model\Order_message', 'order_id', 'id')->with('user')->orderby('created_at', 'desc');
    }
    public function order_goods(){
        return $this->hasMany('App\Model\Order_goods', 'order_id', 'id')->with('goods');
    }
    public function order_company(){
        return $this->hasOne('App\Model\Order_company', 'order_id', 'id');
    }


}
