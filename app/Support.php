<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Support extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $softDelete = true;

    protected $table = 'supports';

    public function user(){
        return $this->hasOne('App\Users', 'id', 'user_id');
    }
}
