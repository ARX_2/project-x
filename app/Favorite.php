<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    public function product(){
      return $this->hasOne('App\Goods', 'id', 'goods')->with('images');
    }
}
