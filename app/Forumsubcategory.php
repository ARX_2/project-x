<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forumsubcategory extends Model
{
    public function messages(){
      return $this->hasMany('App\ForumMessage', 'forumsubcategoryid', 'id')->with('user');
    }
    public function category(){
      return $this->hasOne('App\Forumcategory', 'id', 'forumcategory');
    }
}
