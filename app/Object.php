<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Object extends Model
{
  protected $fillable = ['title', 'text', 'image', 'siteID', 'published', 'created_by', 'modified_by', 'parent_id'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function banners(){
      return $this->has();
    }
}
