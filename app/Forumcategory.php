<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forumcategory extends Model
{
    public function subcategories(){
      return $this->hasMany('App\Forumsubcategory', 'forumcategory', 'id')->where('archive', '=', null)->with('messages')->orderBy('updated_at', 'desc');
    }
}
