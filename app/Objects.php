<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Objects extends Model
{
  protected $fillable = ['title', 'text', 'image', 'siteID', 'published', 'main', 'created_by', 'modified_by', 'slug', 'offer'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function banners(){
      return $this->has();
    }
    public function images(){
      return $this->hasMany('App\Image', 'object', 'articul');
    }
    public function imageResize(){
      return $this->hasOne('App\ImageResize', 'image', 'image');
    }
    public function category(){
        return $this->hasOne('App\Category', 'id', 'parent_id');
    }
}
