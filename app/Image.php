<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class Image extends Model
{
  protected $fillable = ['images', 'article', 'company', 'goods', 'category', 'text','subcategory', 'services', 'siteID', 'updated_at', 'created_at'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function categories(){
      return $this->belongsTo('App\Article', 'images');
    }
    public function imageResize(){
      return $this->hasOne('App\ImageResize', 'image', 'images');
    }
}
