<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
  protected $fillable = ['published', 'main', 'slug', 'title', 'short_description', 'description', 'coast', 'type_coast', 'min_coast', 'type_min_coast', 'articuldate', 'presence', 'delivery', 'payment', 'category', 'created_by', 'modified_by', 'siteID', 'seoname', 'seotitle', 'seodescription', 'seokeywords', 'deleted', 'youtube', 'checkseo', 'sort'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function serviceses(){
      return $this->has();
    }
    public function images(){
      return $this->hasOne('App\Image', 'services', 'id');
    }
    public function imageses(){
      return $this->hasMany('App\Image', 'services', 'id');
    }
    public function subcategory(){
      return $this->belongsToMany('App\Category', 'category_goods', 'services', 'category')->with('categor');
    }
    public function categories(){
      return $this->belongsToMany('App\Category', 'category_goods', 'services', 'category');
    }
}
