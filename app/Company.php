<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
  protected $fillable = ['title', 'siteID', 'short_description', 'description', 'image', 'published', 'created_by', 'modified_by', 'mainHeader', 'aboutHeader'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
  public function companies(){
    return $this->has();
  }

  public function categories(){
    return $this->hasMany('App\Category', 'siteID', 'siteID')->where('deleted', '!=', '1');
  }

  public function goods(){
    return $this->hasMany('App\Goods', 'siteID', 'siteID')->where('deleted', '!=', '1');
  }
  public function management(){
    return $this->hasOne('App\User', 'siteID', 'siteID')->where('userRolle', '2');
  }
  public function contactUser(){
    return $this->hasMany('App\User', 'siteID', 'siteID')->where('userRolle', '>', '2');
  }

  public function infoImage(){
    return $this->hasOne('App\Information', 'siteID', 'siteID');
  }

  public function template(){
      return $this->hasMany('App\Template', 'siteID', 'siteID')->with('texts');
  }

  public function images(){
    return $this->hasMany('App\Image', 'company', 'id');
  }
  public function sections(){
    return $this->hasMany('App\Section', 'siteID', 'siteID');
  }
  public function city(){
    return $this->hasOne('App\City', 'id', 'city');
  }
}
