<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
  protected $fillable = ['title', 'slug', 'locate', 'menu1', 'menu2', 'menu3', 'menu4', 'menu5', 'menu6', 'slide2', 'slide3', 'siteID', 'created_by', 'modified_by'];

  public function setSlugAttribute($value){
    //dd($this->title);
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40). "-". \Carbon\Carbon::now()->format('dmyHi'), '-');
  }
    public function menu(){
      return $this->has();
    }
}
