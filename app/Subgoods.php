<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subgoods extends Model
{
    public function images(){
      return $this->hasMany('App\Image', 'subgoods', 'id');
    }
}
