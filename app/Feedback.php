<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    public function images(){
      return $this->hasMany('App\Image', 'feedback', 'id');
    }
}
