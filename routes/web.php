<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//САЛОНЫ
Route::get('/index.php', function(){
    return redirect('/');
});
Route::group(['prefix' => 'admin-salon', 'namespace' => 'admin_salon', 'middleware' => ['auth']], function () {
  Route::get('/', function(){
    return view('admin.index', ['company' => Resize::company()]);
  });
  //ТОВАРЫ
  Route::any('/goods', 'GoodsController@index')->name('admin-salon.goods.index');
  Route::any('/goods/hidden', 'GoodsController@index');
  Route::any('/goods/create', 'GoodsController@create')->name('admin-salon.goods.create');
  Route::any('/supplier/create', 'GoodsController@createSupplier');
  Route::any('/goods/counts/{id}', 'GoodsController@oldCount');
  Route::any('/invenory/entrance', 'GoodsController@entrance');
  Route::any('/invenory/inventory', 'GoodsController@invenory');
  Route::any('/invenory/charge', 'GoodsController@charge');
  Route::any('goods/update/description', 'GoodsController@description');
  Route::any('stroitelstvo/update/description', 'BuildingController@description');
  Route::any('proyekty/update/description', 'ProductController@description');
  Route::any('izgotovleniye/update/description', 'ManufacturingController@description');
  Route::any('bani/update/description', 'BathController@description');
  Route::any('goods/{slug}/{id}/{parametr}', 'GoodsController@update')->name('admin.goods.update.id');
  Route::any('subgoods/{slug}/{id}/{parametr}', 'GoodsController@subgoods');
  Route::any('goods/{id}/edit/', 'GoodsController@edit')->name('admin-salon.goods.edit');

  //МАТЕРИАЛЫ
  Route::any('/materials', 'MaterialsController@index')->name('admin-salon.materials.index');
  Route::any('/materials/hidden', 'MaterialsController@index');
  Route::any('/materials/create', 'MaterialsController@create')->name('admin-salon.materials.create');
  Route::any('materials/update/description', 'MaterialsController@description');
  Route::any('materials/{slug}/{id}/{parametr}', 'MaterialsController@update')->name('admin.materials.update.id');
  Route::any('submaterials/{slug}/{id}/{parametr}', 'MaterialsController@subgoods');
  Route::any('materials/{id}/edit/', 'MaterialsController@edit')->name('admin-salon.materials.edit');
  Route::any('image/materials/upload', 'MaterialsController@imageUpload');
  //УСЛУГ
  Route::any('/services', 'ServicesController@index')->name('admin-salon.services.index');
  Route::any('/services/hidden', 'ServicesController@index');
  Route::any('/services/create', 'ServicesController@create')->name('admin-salon.services.create');
  Route::any('service/{slug}/{id}/{parametr}', 'ServicesController@update');
  Route::any('services/material/delete/{id}', 'ServicesController@materialDelete');
  Route::any('services/material/counts/{id}/{params}', 'ServicesController@materialCounts');
  Route::any('services/material/{morfid}/{id}/{params}', 'ServicesController@material');
  Route::any('services/payment/{id}/{params}', 'ServicesController@payment');
  Route::any('services/option/{id}/', 'ServicesController@option');
  Route::any('services/option/{slug}/{id}/{params}', 'ServicesController@optionUpdate');
  Route::any('services/option/delete/{id}', 'ServicesController@optionDel');
  Route::any('subservices/{slug}/{id}/{parametr}', 'ServicesController@subservice');
  Route::any('services/{slug}/{id}/{parametr}', 'ServicesController@update');
  Route::any('services/{id}/edit/', 'ServicesController@edit')->name('admin-salon.services.edit');
  Route::any('service/update/description', 'ServicesController@description');

  //СОТРУДНИКИ
  Route::any('employee/', 'EmployeeController@index')->name('admin-salon.employee.index');
  Route::any('employee/store', 'EmployeeController@store')->name('admin-salon.employee.store');
  Route::any('employee/update', 'EmployeeController@update')->name('admin-salon.employee.update');

  Route::any('work/', 'WorkController@index')->name('admin-salon.work.index');
  Route::any('calendar/{slug}/{date}', 'WorkController@calendar');
  Route::any('checkday/{slug}/{date}/{month}/{user}', 'WorkController@chackday');

  Route::any('work/days/{day}/{month}/', 'WorkController@days');
  Route::any('work/{type}/{id}/', 'WorkController@service');
});







//СЕРВИС
Route::group(['prefix' => 'admin', 'namespace' => 'admin', 'middleware' => ['auth']], function () {
  Route::get('/', function(){
    $company = Resize::company();
    //dd($company);
    if($company->type == 2){
      return redirect('admin_salon/');
    }
    else{
      return view('admin.index', ['company' => Resize::company()]);
    }
  })->name('admin.index');
  Route::any('/goods/ShowDeleted', 'GoodsController@index')->name('admin.goods.ShowDeleted');
  Route::any('/bani/ShowDeleted', 'BathController@index')->name('admin.bani.ShowDeleted');
  Route::any('/izgotovleniye/ShowDeleted', 'ManufacturingController@index')->name('admin.izgotovleniye.ShowDeleted');
  Route::any('/proyekty/ShowDeleted', 'ProjectController@index')->name('admin.proyekty.ShowDeleted');
  Route::any('/service/ShowDeleted', 'ServicesController@index')->name('admin.service.ShowDeleted');
  Route::any('/stroitelstvo/ShowDeleted', 'BuildingController@index')->name('admin.stroitelstvo.ShowDeleted');
  Route::any('search/index', 'AdminController@searchIndex');
  Route::any('search/{title}', 'AdminController@search');
  //Route::any('support/view/none', 'SupportController@new')->name('admin.support.view');
  //Route::any('support/view/{id}', 'SupportController@view')->name('admin.support.view');
  Route::any('goods/images/change/all/{id}', 'GoodsController@imagesChangeAll')->name('admin.images.change.all');
    //Route::any('/support/{params}', 'SupportController@index')->name('admin.support.index');
    Route::any('/goods', 'GoodsController@index')->name('admin.product.index');
    //Route::any('/stroitelstvo', 'GoodsController@index')->name('admin.stroitelstvo.index');

    Route::any('/category/hidden', 'CategoryController@index')->name('admin.category.hidden');
    Route::any('/category/deleted', 'CategoryController@index')->name('admin.categories.ShowDeleted');
    Route::any('/goods/hidden', 'GoodsController@index')->name('admin.goods.hidden');
    Route::any('/stroitelstvo/hidden', 'buildingController@index')->name('admin.stroitelstvo.hidden');
    Route::any('/proyekty/hidden', 'ProjectController@index')->name('admin.proyekty.hidden');
    Route::any('/izgotovleniye/hidden', 'ManufacturingController@index')->name('admin.izgotovleniye.hidden');
    Route::any('/bani/hidden', 'BathController@index')->name('admin.bani.hidden');

    Route::any('/objects', 'ObjectsController@index')->name('admin.portfolio.index');
    Route::any('feedback/update/{id}', 'FeedbackController@update')->name('admin.feedback.update.new');
    Route::any('feedback/published/{id}/{param}', 'FeedbackController@published')->name('admin.feedback.published');
    Route::any('feedback/hidden', 'FeedbackController@index')->name('admin.feedback.hidden');

    Route::resource('/tape', 'TapeController', ['as' => 'admin']);
    Route::any('admin/tape/hide', 'TapeController@hide')->name('admin.tape.hide');
    Route::any('admin/tape/hidden', 'TapeController@hidden')->name('admin.tape.hidden');
    Route::any('admin/tape/imageDelete', 'TapeController@imageDelete')->name('admin.tape.imageDelete');

    Route::resource('/banner2', 'BannerxController', ['as' => 'admin']);
    Route::any('/admin/banner2/hide', 'BannerxController@hide')->name('admin.banner2.hide');
    Route::any('/admin/banner2/hidden', 'BannerxController@hidden')->name('admin.banner2.hidden');
    Route::any('/admin/banner2/imageDelete', 'BannerxController@imageDelete')->name('admin.banner2.imageDelete');

    Route::resource('/category', 'CategoryController', ['as' => 'admin']);
    Route::resource('/article', 'ArticleController', ['as' => 'admin']);
    Route::resource('/contact', 'ContactController', ['as' => 'admin']);
    Route::resource('/company', 'CompanyController', ['as' => 'admin']);
    Route::resource('/work', 'WorkController', ['as' => 'admin']);
    Route::resource('/employee', 'EmployeeController', ['as' => 'admin']);
    Route::resource('/services', 'ServicesController', ['as' => 'admin']);
    Route::resource('/goods', 'GoodsController', ['as' => 'admin']);
    Route::resource('/banner', 'BannerController', ['as' => 'admin']);
    Route::resource('/opinion', 'OpinionController', ['as' => 'admin']);

    Route::resource('/information', 'InformationController', ['as' => 'admin']);
    Route::resource('/companyprice', 'CompanipriceController', ['as' => 'admin']);
    Route::resource('/menu', 'MenuController', ['as' => 'admin']);
    Route::resource('/seo', 'SeoController', ['as' => 'admin']);
    Route::resource('/include', 'IncludeController', ['as' => 'admin']);

    Route::resource('/price', 'PricelistController', ['as' => 'admin']);
    Route::resource('/jobs', 'JobsController', ['as' => 'admin']);
    Route::resource('/action', 'ActionController', ['as' => 'admin']);
    Route::resource('/news', 'NewsController', ['as' => 'admin']);
    Route::resource('/blog', 'BlogController', ['as' => 'admin']);
    Route::resource('/partners', 'PartnerController', ['as' => 'admin']);
    Route::resource('/offers', 'OfferController', ['as' => 'admin']);
    Route::resource('/subcategory', 'SubcategoryController', ['as' => 'admin']);
    Route::resource('/tizers', 'TizerController', ['as' => 'admin']);
    Route::resource('/sections', 'SectionController', ['as' => 'admin']);
    Route::resource('/orders', 'OrderController', ['as' => 'admin']);
    Route::resource('/biz', 'BizController', ['as' => 'admin']);
    Route::resource('/stroitelstvo', 'BuildingController', ['as' => 'admin']);
    Route::resource('/bani', 'BathController', ['as' => 'admin']);
    Route::resource('/proyekty', 'ProjectController', ['as' => 'admin']);
    Route::resource('/izgotovleniye', 'ManufacturingController', ['as' => 'admin']);
    Route::resource('/feedback', 'FeedbackController', ['as' => 'admin']);

    Route::resource('/object', 'ObjectController', ['as' => 'admin']);
    Route::any('admin/object/hidden', 'ObjectController@hidden')->name('admin.object.hidden');
    Route::any('admin/object/hide', 'ObjectController@hide')->name('admin.object.hide');
    Route::any('object/{slug}/{id}/{parametr}', 'ObjectController@updatex')->name('admin.object.update.id');

    Route::resource('/documentation', 'DocumentController', ['as' => 'admin']);
    Route::any('documentation/{slug}/{id}/{parametr}', 'DocumentController@updatex')->name('admin.documentation.update.id');

    Route::any('order/modal', 'OrderController@modal')->name('admin.order.modal');
    Route::any('/order/create/fast', 'OrderController@create');

    Route::any('order/column/edittitle', 'OrderController@editTitleColumn');
    Route::any('order/column/create', 'OrderController@createColumn');
    Route::any('/order/column/position', 'OrderController@columnPosition')->name('order.column.position');
    Route::any('order/column/delete', 'OrderController@deleteColumn');

    Route::post('/order/modal/addcomment', 'OrderController@addComment');
    Route::any('/order/modal/editname', 'OrderController@editName');
    Route::any('/order/modal/responsible', 'OrderController@responsible');
    Route::any('/order/modal/selectcolumn', 'OrderController@selectColumn');
    Route::any('/order/modal/editperson', 'OrderController@editPerson');
    Route::any('/order/modal/editcompany', 'OrderController@editCompany');
    Route::any('/order/modal/deletetest', 'OrderController@deleteTest');
    Route::any('/order/modal/position', 'OrderController@position')->name('order.position');

    Route::any('/support', 'SupportController@index')->name('admin.support.index');
    Route::any('/support/question/{id}', 'SupportController@view');
    Route::any('/support/create', 'SupportController@create');
    Route::any('/support/store', 'SupportController@store')->name('admin.support.store');
    Route::any('/support/view/addmessage', 'SupportController@addMessage');
    Route::any('/support/view/deletequestion/{id}', 'SupportController@deleteQuestion');
    Route::post('/support/view/delete/{id}', 'SupportController@delete');

    Route::post('/information/insert', 'MenuController@insert')->name('admin.menu.insert');
    Route::post('/section/update', 'SectionController@update')->name('admin.section.update');
    Route::any('feedback/image/destroy', 'FeedbackController@imageDestroy')->name('admin.feedback.image.desctroy');
    Route::any('goods/update/description', 'GoodsController@description');
    Route::any('service/update/description', 'ServicesController@description');
    Route::any('category/update/description', 'CategoryController@description');
    Route::any('goods/update/description', 'GoodsController@description');
    Route::any('stroitelstvo/update/description', 'BuildingController@description');
    Route::any('proyekty/update/description', 'ProjectController@description');
    Route::any('izgotovleniye/update/description', 'ManufacturingController@description');
    Route::any('bani/update/description', 'BathController@description');
    //Route::any('/goods/updated/{id}/{type}/{params}', 'GoodsController@update')->name('admin.goods.updated');
    //Route::any('/services', 'GoodsController@index')->name('admin.services');
    //Route::any('/services/create', 'GoodsController@create');
    Route::post('/menu/insert', 'InformationController@insert')->name('admin.information.insert');
    Route::get('/image/company/destroy', 'CompanyController@imageDestroy')->name('admin.image.company.destroy');
    Route::get('/image/category/destroy', 'CategoryController@imageDestroy')->name('admin.image.category.destroy');
    Route::get('/image/subcategory/destroy', 'SubcategoryController@imageDestroy')->name('admin.image.subcategory.destroy');
    Route::any('image/upload', 'GoodsController@imageUpload');
    Route::any('image/category/upload', 'CategoryController@imageUpload');
    Route::any('image/category/cover/upload', 'CategoryController@coverUpload');
    Route::any('image/banner/upload', 'BannerController@imageUpload');
    Route::any('image/tizer/upload', 'TizerController@imageUpload');

    // Route::get('/master/work/choose', 'WorkController@master')->name('admin.master.work.choose');
    // Route::get('/master/work/status', 'WorkController@status')->name('admin.master.work.status');
    // Route::post('/master/work/time', 'WorkController@time')->name('admin.master.work.time');
    // Route::get('/employee/work/addServices', 'EmployeeController@addServices')->name('admin.employee.work.addServices');
    // Route::get('/employee/work/deleteServices', 'EmployeeController@deleteServices')->name('admin.employee.work.deleteServices');
    // Route::get('/services/work/addServices', 'ServicesController@addServices')->name('admin.services.work.addServices');
    // Route::get('/services/work/deleteServices', 'ServicesController@deleteServices')->name('admin.services.work.deleteServices');

    Route::get('/image/goods/destroy', 'GoodsController@imageDestroy')->name('admin.image.goods.destroy');

    Route::get('/goods/destroy/goods', 'GoodsController@destroyGoods')->name('admin.goods.destroy.goods');

    Route::get('/admin/information/edit', 'InformationController@imageDestroy')->name('admin.image.information.destroy');

    Route::post('/admin/createSite', 'AdminController@createSite')->name('admin.createSite');
    Route::post('/admin/updateSite', 'AdminController@updateSite')->name('admin.updateSite');


    Route::get('/image/employee/destroy', 'EmployeeController@imageDestroy')->name('admin.image.employee.destroy');

    Route::any('admin/git/pull', 'AdminController@gitpull')->name('admin.git.pull');
    Route::any('admin/git/push', 'AdminController@gitpush')->name('admin.git.push');


    // Route::any('admin/employee/calendar', 'EmployeeController@calendar')->name('admin.employee.calendar');
    // Route::any('admin/employee/calendar/store', 'EmployeeController@calendarstore')->name('admin.employee.calendar.store');
    Route::any('admin/category/deleted', 'CategoryController@deleted')->name('admin.category.deleted');
    Route::any('admin/category/return', 'CategoryController@return')->name('admin.category.return');

   // Route::any('admin/support/comment', 'SupportController@sendComment')->name('admin.support.comment');
    //Route::any('/support/messages/{id}', 'SupportController@messages')->name('admin.support.messages');
    //Route::any('/support/changeStatus/{id}/{params}', 'SupportController@changeStatus')->name('admin.support.changeStatus');
    //Route::any('comments/read/{id}/{user}', 'SupportController@readComent');

    Route::any('admin/include/delete', 'IncludeController@destroy')->name('admin.include.delete');
    Route::any('admin/category/hide', 'CategoryController@hide')->name('admin.category.hide');
    Route::any('admin/category/ShowHide', 'CategoryController@ShowHide')->name('admin.category.ShowHide');
    Route::any('admin/category/main', 'CategoryController@main')->name('admin.category.main');

    Route::any('admin/goods/uploadExcel', 'GoodsController@uploadExcel')->name('admin.goods.uploadExcel');
    Route::any('admin/goods/excelExport', 'GoodsController@excelExport')->name('admin.goods.excelExport');
    Route::any('admin/category/uploadExcel', 'CategoryController@uploadExcel')->name('admin.category.uploadExcel');
    Route::any('admin/category/excelExport', 'CategoryController@excelExport')->name('admin.category.excelExport');
    Route::any('admin/goods/ShowHide', 'GoodsController@ShowHide')->name('admin.goods.ShowHide');

    Route::any('goods/{slug}/{id}/{parametr}', 'GoodsController@update')->name('admin.goods.update.id');
    Route::any('stroitelstvo/{slug}/{id}/{parametr}', 'BuildingController@update')->name('admin.stroitelstvo.update.id');
    Route::any('stroitelstvo/sub/{slug}/{id}/{parametr}', 'BuildingController@subUpdate')->name('admin.stroitelstvo.sub.update.id');
    Route::any('proyekty/{slug}/{id}/{parametr}', 'ProjectController@update')->name('admin.proyekty.update.id');
    Route::any('izgotovleniye/{slug}/{id}/{parametr}', 'ManufacturingController@update')->name('admin.izgotovleniye.update.id');
    Route::any('bani/{slug}/{id}/{parametr}', 'BathController@update')->name('admin.bani.update.id');



    Route::any('/service/{slug}/{id}/{parametr}', 'ServicesController@update')->name('admin.service.update.id');
    Route::any('/service/hidden', 'ServicesController@index')->name('admin.service.hidden');
    Route::any('/service/main', 'ServicesController@main')->name('admin.service.main');
    Route::any('/admin/refreshCache', 'ServicesController@main')->name('admin.refreshCache');


    Route::any('admin/company/subupdate', 'CompanyController@subupdate')->name('admin.company.subupdate');
    Route::any('admin/company/main', 'CompanyController@main')->name('admin.company.main');
    Route::any('admin/company/requisite', 'CompanyController@requisite')->name('admin.company.requisite');
    Route::any('admin/company/sendRequisite', 'CompanyController@sendRequisite')->name('admin.company.sendRequisite');
    Route::any('admin/company/logo', 'CompanyController@logo')->name('admin.company.logo');
    Route::any('admin/company/sendLogo', 'CompanyController@sendLogo')->name('admin.company.sendLogo');
    Route::any('admin/company/images', 'CompanyController@images')->name('admin.company.images');
    Route::any('admin/company/description', 'CompanyController@description')->name('admin.company.description');
    Route::any('admin/company/about', 'CompanyController@about')->name('admin.company.about');
    Route::any('admin/company/management', 'CompanyController@management')->name('admin.company.management');
    Route::any('management/update', 'CompanyController@managementUpdate')->name('admin.management.update');
    Route::any('admin/company/upload', 'CompanyController@upload')->name('admin.company.upload');
    Route::any('admin/company/updateDescription', 'CompanyController@updateDescription')->name('admin.company.updateDescription');




    Route::any('settings/site/technical/{setting}', function($setting){
      return view('admin.settings.site.technical.'.$setting,[
      'company' => Resize::company(),
    ]);
  });
  Route::any('settings/update/{setting}', 'InformationController@settingUpdate')->name('admin.settings.update');


    Route::any('settings/site/index', function(){
      return view('admin.settings.site.index',[
      'company' => Resize::company(),
    ]);
  });
  Route::any('settings/site/design/{setting}', function($setting){
    return view('admin.settings.site.design.'.$setting,[
      'company' => Resize::company(),
    ]);
  });
    Route::any('settings/design/update/{setting}', 'InformationController@settingDesignUpdate')->name('admin.settings.design.update');
    Route::any('image_menu/delete/', 'InformationController@image_menu')->name('admin.image_menu.delete');

    Route::any('admin/employee/delete', 'EmployeeController@delete')->name('admin.employee.delete');

    Route::any('banner/{slug}/{id}/{parametr}', 'BannerController@update')->name('admin.banner.update');
    Route::any('tizer/{slug}/{id}/{parametr}', 'TizerController@update')->name('admin.tizer.update');

    Route::any('admin/object/edit', 'ObjectController@hide')->name('admin.object.hide');
    Route::any('admin/object/main', 'ObjectController@main')->name('admin.object.main');
    Route::any('admin/object/type', 'ObjectController@type')->name('admin.object.type');
    Route::any('admin/price/add', 'PricelistController@add')->name('admin.price.add');
    Route::any('admin/price/addPrice', 'PricelistController@addPrice')->name('admin.price.addPrice');
    Route::any('admin/price/delete', 'PricelistController@delete')->name('admin.price.delete');
    Route::any('admin/price/upload', 'PricelistController@upload')->name('admin.price.upload');
    Route::any('admin/price/deleteCategory', 'PricelistController@deleteCategory')->name('admin.price.deleteCategory');
    Route::any('admin/jobs/hide', 'JobsController@hide')->name('admin.jobs.hide');
    Route::any('admin/jobs/imageDelete', 'JobsController@imageDelete')->name('admin.jobs.imageDelete');
    Route::any('admin/action/hide', 'ActionController@hide')->name('admin.action.hide');
    Route::any('admin/action/main', 'ActionController@main')->name('admin.action.main');
    Route::any('admin/action/hidden', 'ActionController@hidden')->name('admin.action.hidden');
    Route::any('admin/action/imageDelete', 'ActionController@imageDelete')->name('admin.action.imageDelete');
    Route::any('admin/news/hide', 'NewsController@hide')->name('admin.news.hide');
    Route::any('admin/news/main', 'NewsController@main')->name('admin.news.main');
    Route::any('admin/news/imageDelete', 'NewsController@imageDelete')->name('admin.news.imageDelete');
    Route::any('admin/news/hidden', 'NewsController@hidden')->name('admin.news.hidden');
    Route::any('admin/blog/hide', 'BlogController@hide')->name('admin.blog.hide');
    Route::any('admin/blog/main', 'BlogController@main')->name('admin.blog.main');
    Route::any('admin/blog/hidden', 'BlogController@hidden')->name('admin.blog.hidden');
    Route::any('admin/blog/imageDelete', 'BlogController@imageDelete')->name('admin.blog.imageDelete');
    Route::any('admin/partners/hide', 'PartnerController@hide')->name('admin.partners.hide');
    Route::any('admin/partners/main', 'PartnerController@main')->name('admin.partners.main');
    Route::any('admin/partners/hidden', 'PartnerController@hidden')->name('admin.partners.hidden');
    Route::any('admin/partners/imageDelete', 'PartnerController@imageDelete')->name('admin.partners.imageDelete');
    Route::any('admin/dragAndDrop/category', 'CategoryController@dragAndDropCategory')->name('admin.dragAndDrop.category');
    Route::any('admin/dragAndDrop/subcategory', 'SubcategoryController@dragAndDropSubategory')->name('admin.dragAndDrop.subcategory');

    Route::any('admin/sections/main', 'SectionController@main')->name('admin.sections.main');

    Route::any('admin/tizers/delete', 'TizerController@delete')->name('admin.tizers.delete');
    Route::any('admin/del/telEmail', 'CompanyController@telEmail')->name('admin.del.telEmail');
    Route::any('admin/sort/category', 'CategoryController@sortCategory')->name('admin.sort.category');


    Route::any('admin/admin/index', 'AdminController@index')->name('admin.admin.index');
    Route::any('admin/admin/about', 'AdminController@about')->name('admin.admin.about');
    Route::any('admin/admin/company/template', 'AdminController@companyTemplate')->name('admin.admin.company.template');
    Route::any('admin/admin/information', 'AdminController@information')->name('admin.admin.information');
    Route::any('admin/admin/information/main', 'AdminController@informationMain')->name('admin.admin.information.main');
    Route::any('admin/admin/information/submain', 'AdminController@informationSubmain')->name('admin.admin.information.submain');
    Route::any('admin/admin/sections', 'AdminController@sections')->name('admin.admin.sections');
    Route::any('admin/admin/home', 'AdminController@home')->name('admin.admin.home');
    Route::any('admin/admin/menu', 'AdminController@menu')->name('admin.admin.menu');
    Route::any('admin/admin/category', 'AdminController@category')->name('admin.admin.category');
    Route::any('admin/admin/category/template', 'AdminController@categoryTemplate')->name('admin.admin.category.template');
    Route::any('admin/admin/product', 'AdminController@product')->name('admin.admin.product');
    Route::any('admin/admin/product/template', 'AdminController@productTemplate')->name('admin.admin.product.template');
    Route::any('admin/admin/object', 'AdminController@object')->name('admin.admin.object');
    Route::any('admin/admin/object/template', 'AdminController@objectTemplate')->name('admin.admin.object.template');
    Route::any('admin/admin/contact', 'AdminController@contact')->name('admin.admin.contact');
    Route::any('admin/admin/contact/template', 'AdminController@contactTemplate')->name('admin.admin.contact.template');
    Route::any('admin/admin/header', 'AdminController@header')->name('admin.admin.header');
    Route::any('admin/admin/footer', 'AdminController@footer')->name('admin.admin.footer');
    Route::any('admin/admin/user', 'AdminController@user')->name('admin.admin.user');
    Route::any('admin/production/category', 'AdminController@productionCategory')->name('admin.production.category');
    Route::any('admin/production/c-edit', 'AdminController@cedit')->name('admin.production.c-edit');
    Route::any('admin/update/cedit', 'AdminController@updatecedit')->name('admin.update.cedit');
    Route::any('admin/del/cedit', 'AdminController@delcedit')->name('admin.del.cedit');
    Route::any('admin/create/cedit', 'AdminController@createcedit')->name('admin.create.cedit');
    Route::any('admin/production/cdelete', 'AdminController@cdelete')->name('admin.production.cdelete');
    Route::any('admin/production/creturn', 'AdminController@categoryReturn')->name('admin.production.creturn');
    Route::any('admin/production/goods', 'AdminController@goods')->name('admin.production.goods');
    Route::any('admin/production/p-edit', 'AdminController@pedit')->name('admin.production.p-edit');
    Route::any('admin/production/pdelete', 'AdminController@pdelete')->name('admin.production.pdelete');
    Route::any('admin/update/pedit', 'AdminController@updatepedit')->name('admin.update.pedit');
    Route::any('admin/create/pedit', 'AdminController@createpedit')->name('admin.create.pedit');
    Route::any('admin/production/preturn', 'AdminController@productReturn')->name('admin.production.preturn');
   // Route::any('admin/admin/support', 'AdminController@support')->name('admin.admin.support');
    Route::any('admin/admin/chooseSite', 'AdminController@chooseSite')->name('admin.admin.chooseSite');
    Route::any('admin/goods/uploadExcel', 'GoodsController@uploadExcel')->name('admin.goods.uploadExcel');
    Route::any('admin/goods/excelExport', 'GoodsController@excelExport')->name('admin.goods.excelExport');
    Route::any('admin/refreshCache', 'AdminController@refreshCache')->name('admin.refreshCache');
    Route::any('admin/change/atmosphere', 'AdminController@atmosphere')->name('admin.change.atmosphere');
    Route::any('local/images', 'AdminController@LocalImages')->name('admin.local.images');
    Route::any('choose/section/{type}/{parametr}', 'AdminController@selectSection')->name('admin.choose.section');
    Route::any('admin/sort/goods', 'GoodsController@sortGoods')->name('admin.sort.goods');
    Route::any('admin/sort/services', 'ServicesController@sortServices')->name('admin.sort.services');

    Route::any('/category/goods/{category}', 'GoodsController@CategoryGoods');
    Route::any('/category/goods/{category}', 'GoodsController@CategoryGoods')->name('admin.refreshCache');
    Route::any('/category/goods/hidden/{category}', 'GoodsController@CategoryGoods');
    Route::any('/category/goods/all/{category}', 'GoodsController@CategoryGoodsAll');
    Route::any('/category/goods/all/hidden/{category}', 'GoodsController@CategoryGoodsAll');


    Route::any('/category/stroitelstvo/{category}', 'BuildingController@CategoryGoods');
    Route::any('/category/stroitelstvo/{category}', 'BuildingController@CategoryGoods')->name('admin.refreshCache');
    Route::any('/category/stroitelstvo/hidden/{category}', 'BuildingController@CategoryGoods');
    Route::any('/category/stroitelstvo/all/{category}', 'BuildingController@CategoryGoodsAll');
    Route::any('/category/stroitelstvo/all/hidden/{category}', 'BuildingController@CategoryGoodsAll');

    Route::any('/category/bani/{category}', 'BathController@CategoryGoods');
    Route::any('/category/bani/{category}', 'BathController@CategoryGoods')->name('admin.refreshCache');
    Route::any('/category/bani/hidden/{category}', 'BathController@CategoryGoods');
    Route::any('/category/bani/all/{category}', 'BathController@CategoryGoodsAll');
    Route::any('/category/bani/all/hidden/{category}', 'BathController@CategoryGoodsAll');

    Route::any('/category/proyekty/{category}', 'ProjectController@CategoryGoods');
    Route::any('/category/proyekty/{category}', 'ProjectController@CategoryGoods')->name('admin.refreshCache');
    Route::any('/category/proyekty/hidden/{category}', 'ProjectController@CategoryGoods');
    Route::any('/category/proyekty/all/{category}', 'ProjectController@CategoryGoodsAll');
    Route::any('/category/proyekty/all/hidden/{category}', 'ProjectController@CategoryGoodsAll');

    Route::any('/category/izgotovleniye/{category}', 'ManufacturingController@CategoryGoods');
    Route::any('/category/izgotovleniye/{category}', 'ManufacturingController@CategoryGoods')->name('admin.refreshCache');
    Route::any('/category/izgotovleniye/hidden/{category}', 'ManufacturingController@CategoryGoods');
    Route::any('/category/izgotovleniye/all/{category}', 'ManufacturingController@CategoryGoodsAll');
    Route::any('/category/izgotovleniye/all/hidden/{category}', 'ManufacturingController@CategoryGoodsAll');




    Route::any('/category/subcategory/{Category}', 'CategoryController@CategorySubcategory');
    Route::any('/category/subcategory/hidden/{Category}', 'CategoryController@CategorySubcategory');

    Route::any('/subcategory/goods/{subcategory}', 'GoodsController@SubcategoryGoods');
    Route::any('/subcategory/goods/hidden/{subcategory}', 'GoodsController@SubcategoryGoods');

    Route::any('/subcategory/izgotovleniye/{subcategory}', 'ManufacturingController@SubcategoryGoods');
    Route::any('/subcategory/izgotovleniye/hidden/{subcategory}', 'ManufacturingController@SubcategoryGoods');




    Route::any('/category/services/{category}', 'ServicesController@CategoryServices');
    Route::any('/category/services/{category}', 'ServicesController@CategoryServices')->name('admin.refreshCache');
    Route::any('/category/services/hidden/{category}', 'ServicesController@CategoryServices');

    Route::any('/category/services/all/{category}', 'ServicesController@CategoryServicesAll');
    Route::any('/category/services/all/hidden/{category}', 'ServicesController@CategoryServicesAll');

    Route::any('/subcategory/services/{subcategory}', 'ServicesController@SubcategoryServices');
    Route::any('/subcategory/services/hidden/{subcategory}', 'ServicesController@SubcategoryServices');

    Route::any('/admin/change/section/', 'InformationController@changeSection')->name('admin.change.section');
    //Route::any('/section/update/', 'InformationController@SubcategoryGoods');
    Route::any('/profile/', 'EmployeeController@profile')->name('admin.profile');
    Route::any('/profile/edit', 'EmployeeController@profileEdit')->name('admin.profile.edit');
    Route::any('/profile/update', 'EmployeeController@profileUpdate')->name('admin.profile.update');
    Route::any('/profile/password', 'EmployeeController@password')->name('admin.profile.password');
    Route::any('category/{slug}/{id}/{parametr}', 'CategoryController@update')->name('admin.category.update.id');
    Route::any('/images/sort', 'GoodsController@imagesort')->name('admin.images.sort');
    Route::any('/saves/{slug}', 'AdminController@saves');
    Route::any('/saves/upload/{slug}/{name}', 'AdminController@savesupload');
    Route::any('/save/categories/{id}', 'AdminController@backupCategories');
    Route::any('/save/goods/{id}/{type}', 'AdminController@backupGoods');
    Route::any('/admin/admin/favorite', 'AdminController@favorite')->name('admin.admin.favorite');
    Route::any('/admin/admin/favorite/destroy', 'AdminController@favoriteDestroy')->name('admin.admin.favorite.destroy');
    Route::any('/admin/admin/favorite/sort', 'AdminController@sortFavorite')->name('admin.favorite.sort');
    Route::any('/refresh/cahce/company', 'AdminController@refrash')->name('admin.favorite.sort');
    Route::any('/SocialNetwork/update/', 'CompanyController@socialNetwork')->name('admin.SocialNetwork.update');
    Route::any('/change/title/sites', 'GoodsController@changeTitleSites')->name('admin.change.title.sites');
    Route::any('/change/title/sites/services', 'ServicesController@changeTitleSites')->name('admin.change.title.sites.services');
    Route::any('/change/title/sites/bath', 'BathController@changeTitleSites')->name('admin.change.title.sites.bath');
    Route::any('/change/title/sites/building', 'BuildingController@changeTitleSites')->name('admin.change.title.sites.building');
    Route::any('/change/title/sites/project', 'ProjectController@changeTitleSites')->name('admin.change.title.sites.project');
    Route::any('/change/title/sites/manufacturing', 'ManufacturingController@changeTitleSites')->name('admin.change.title.sites.manufacturing');
    Route::any('/admin/forum/subcategoryEdit', 'ForumController@subcategoryEdit')->name('admin.forum.subcategoryEdit');
    Route::any('admin/forum/subcategories/meesages', 'ForumController@meesages')->name('admin.forum.subcategories.meesages');
    Route::any('admin/forum/subcategory/delete', 'ForumController@subcategoryDelete')->name('admin.forum.subcategory.delete');
    Route::any('admin/forum/subcategory/Archive', 'ForumController@subcategoryArchive')->name('admin.forum.subcategory.archive');


});

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('/offer', function () {
//  return view('admin.offers.create', [
//    'offer' => DB::table('offers')->get(),
//  ]);
//});
//$sites = DB::table('sites')->get();
//foreach ($sites as $site) {
//  Route::domain($site->title)->group(function () {

Route::get('/index.html', function(){
    return redirect('/');
});



/////////////////////// ServicesController /////////////////////////////////////////

Route::get('/izgotovleniye/', 'IzgotovleniyeController@index')->name('izgotovleniye.index');
Route::get('/izgotovleniye/{urlcategory}/', 'IzgotovleniyeController@category');
Route::get('/izgotovleniye/{urlcategory}/{slug}', 'IzgotovleniyeController@view');

Route::any('/modal/open/izgotovleniye', 'OrderController@modalOpen')->name('modal.open.izgotovleniye');
Route::any('/modal/open/map', 'OrderController@modalMap')->name('modal.open.map');
Route::post('/modal/mebel/free/send', 'OrderController@orderSend')->name('modal.send.izgotovleniye');

Route::get('/stroitelstvo/', 'StroitelstvoController@index');
Route::get('/stroitelstvo/{urlcategory}/', 'StroitelstvoController@category');
Route::get('/stroitelstvo/{urlcategory}/{slug}', 'StroitelstvoController@view');

Route::get('/proyekty/', 'ProyektyController@index');
Route::get('/proyekty/{urlcategory}/', 'ProyektyController@category');
Route::get('/proyekty/{urlcategory}/{slug}', 'ProyektyController@view');

Route::get('/bani/', 'BaniController@index');
Route::get('/bani/{urlcategory}/', 'BaniController@category');
Route::get('/bani/{urlcategory}/{slug}', 'BaniController@view');

Route::get('/services/', 'ServicesController@index');
Route::get('/services/{urlcategory}/', 'ServicesController@category');
Route::get('/services/{urlcategory}/{slug}', 'ServicesController@view');

Route::get('/product/', 'ProductController@index');
Route::get('/product/{urlcategory}/', 'ProductController@category');
Route::get('/product/{urlcategory}/{slug}', 'ProductController@view');

Route::get('/portfolio/', 'PortfolioController@index');
Route::get('/portfolio/{urlcategory}/', 'PortfolioController@category');
Route::get('/portfolio/{urlcategory}/{slug}', 'PortfolioController@view');
Route::any('/take/modal/portfolio', 'PortfolioController@modal')->name('take.modal.portfolio');

Route::get('/all-reviews/', 'AllReviewsController@index');

/////////////////////// END ServicesController /////////////////////////////////////////

/////////////////////// CompanyController /////////////////////////////////////////

Route::get('/sitemap', function(){
  $siteID = Cache::get('/' . Request::getHttpHost())->id;
  $categries = App\Category::where('siteID', $siteID)->with('goods')->get();
  $objects = App\Objects::where('siteID', $siteID)->with('category')->get();
  $title = App\Site::where('id', $siteID)->first();
  return view('2019.sitemap', ['categories' => $categries, 'objects' => $objects, 'url' => $title,]);
});

Route::get('/yml', function(){
  $siteID = Cache::get('/' . Request::getHttpHost())->id;
  $categries = App\Category::where('siteID', $siteID)->with('goods')->get();
  $title = App\Site::where('id', $siteID)->first();
  return view('2019.yml', ['categories' => $categries,'title' => $title,]);
});

Route::get('/', 'IndexController@index')->name('index');

Route::get('/about/', 'CompanyController@about');
Route::get('/about/documents/', 'CompanyController@documents');
Route::get('/action/', 'CompanyController@action');
Route::get('/action/{slug}', 'CompanyController@actionView');
Route::get('/news/', 'CompanyController@news');
Route::get('/news/{slug}', 'CompanyController@newsView');
Route::get('/jobs/', 'CompanyController@jobs');
Route::get('/about/requisites/', 'CompanyController@requisites');
Route::get('/contact/', 'CompanyController@contacts');
Route::get('/forum/', 'admin\ForumController@forum');
Route::get('/forum/report', 'admin\ForumController@report');
Route::get('/forum/create', 'admin\ForumController@subcategoryCreate');
Route::get('/forum/{slug}', 'admin\ForumController@category');
Route::get('/forum/{category}/{slug}', 'admin\ForumController@subcategory');
Route::post('/forum/subcategory/{slug}/message', 'admin\ForumController@message');

Route::get('/soglasiye-na-obrabotku-personalnykh-dannykh/', 'CompanyController@soglasiye');
Route::get('/politika-konfidentsialnosti/', 'CompanyController@politika');

Route::get('/price-list/', 'CompanyController@priceList');

/////////////////////// END CompanyController /////////////////////////////////////////
      Route::get('/login', 'LoginController@login');
      Route::get('/logout', 'Auth\LoginController@logout');
      Route::any('/alice', function(){
          return Alice::skill();
      });
      Route::get('/{slug}', function(){
          return abort(404);
      })->name('index');

      //Route::get('/home', 'HomeController@index');
      Route::any('/create/order', 'admin\OrderController@store')->name('create.order');

      Route::any('/take/modal/prodsuct', 'admin\OrderController@modal')->name('take.modal.product');
      Route::any('/take/modal/needcall', 'admin\OrderController@needcall')->name('take.modal.needcall');

      Route::any('/take/modal/map', 'admin\OrderController@map')->name('take.modal.map');
      Route::any('/site/page', 'UralpoliplasticController@page')->name('2019.page');

      Route::any('/feedback/store/{goods}', 'admin\FeedbackController@store');
      Route::any('/feedback/delete/{id}', 'admin\FeedbackController@destroy');
      Route::any('/portfolio/view/image/description/{id}', 'PortfolioController@show');
      Route::any('/admin/orders/modal/{id}', 'admin\OrderController@show');
      Route::any('/admin/biz/modal/{id}', 'admin\BizController@show');
      //Route::get('/{slug}', 'UralpoliplasticController@other');

      // Route::get('/about', 'UralpoliplasticController@about');
      // Route::get('/product', 'UralpoliplasticController@product');
      // Route::get('/services', 'UralpoliplasticController@services');
      // Route::get('/objects', 'UralpoliplasticController@objects');
      // Route::get('/contact', 'UralpoliplasticController@contact');
      // Route::get('/category', 'UralpoliplasticController@category');
       Route::post('/mail', 'UralpoliplasticController@mail');
       Route::post('/mail/povishev', 'UralpoliplasticController@mailpovishev');
       //Route::get('services/{slug}', 'UralpoliplasticController@services');
       //Route::get('products/{slug}', 'UralpoliplasticController@goods');
       // Route::post('/mail/povishev', 'UralpoliplasticController@mailpovishev');
      // Route::get('/category/{slug}', 'UralpoliplasticController@SalonCategory');
      // Route::get('/shares', 'UralpoliplasticController@shares');
       //Route::get('/product/{slug}', 'UralpoliplasticController@category');
       Route::get('/portfolio2/{slug}', 'UralpoliplasticController@objectCategory');
       Route::get('/portfolio2/{category}/{slug}', 'UralpoliplasticController@viewObject');
       Route::get('/product3/{slug}', 'UralpoliplasticController@subcategory');
       Route::get('/services2/{slug}', 'UralpoliplasticController@category');
       Route::get('/services2/{category}/{slug}', 'UralpoliplasticController@services');

       Route::get('/category/{category}/{subcategory}/{slug}', 'UralpoliplasticController@services');
       Route::get('/services2/{category}/{subcategory}/{slug}', 'UralpoliplasticController@services');
       Route::get('/product3/{category}/{slug}', 'UralpoliplasticController@product');

       Route::get('load/content/ajax', 'UralpoliplasticController@loadContent');
       Route::get('image/load/ajax', 'UralpoliplasticController@image')->name('image.load.ajax');
       Route::get('/cache/upload/update', 'UralpoliplasticController@uploadCache');

      /////////////////////////////////////////////////////////////////

      //                   ФОРМА ЗАКАЗОВ                            //

      //////////////////////////////////////////////////////////////////
      Route::any('/offer/actialTime', 'OfferController@actualTime');
      Route::any('/offer/services', 'OfferController@services');
      Route::any('/offer/master', 'OfferController@master');
      Route::any('/offer/date', 'OfferController@date');
      Route::any('/offer/form', 'OfferController@form');
      Route::any('/offer/form/send', 'OfferController@send');

      /////////////////////////////////////////////////////////////////

      //                   ФОРМА ЗАКАЗОВ                            //

      //////////////////////////////////////////////////////////////////



      //Route::any('admin/offers/index', 'OfferController@index')->name('admin.offers.index');
//  });
//}





Route::group(['prefix' => 'management', 'namespace' => 'management', 'middleware' => ['auth']], function () {
    Route::any('support', 'SupportController@index');
});


Auth::routes();

///// ШРИФТЫ
//// Amatic SC
//// Shadows Into Light
//Route::get('/home', 'HomeController@index')->name('home');
