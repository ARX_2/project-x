var io = require('socket.io')(6001);
io.on('connection', function(socket){
  socket.on('channelfxer', function(mychanel){
    socket.join(mychanel);
  });
  socket.on('message', function(data){
    console.log(data.room);
    socket.broadcast.emit('message',data);
  });

  socket.on('readed', function(data){
    console.log(data);
    socket.to(data.room).emit('readed',data);
  });

socket.on('disconnect', function(){ });
  // console.log('New connection!');
  // socket.emit('server-info', {version:.1});
  // socket.broadcast.send('New user');
  // socket.send('Message from');
});

// var io = require('socket.io')(6001),
//     Redis = require('ioredis'),
//     redis = new Redis(8081);
// redis.psubscribe('*', function(error, count){
//   //...
// });
// // redis.on("error", function(err){
// //     console.log(err);
// // });
// redis.on('pmessage', function(pattern, channel, comment){
//   comment = JSON.parse(comment);
//   io.emit(comment + ':' + comment.event, comment.data.message);
//   console.log(channel, comment);
// });
