<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix'=>'admin', 'namespace'=>'admin', 'middleware'=>['auth']], function(){
  Route::get('/', 'DashboardController@dashboard')->name('admin.index');
  Route::resource('/category', 'CategoryController', ['as'=>'admin']);
  Route::resource('/article', 'ArticleController', ['as'=>'admin']);
  Route::resource('/contact', 'ContactController', ['as'=>'admin']);
  Route::resource('/company', 'CompanyController', ['as'=>'admin']);
  Route::resource('/work', 'WorkController', ['as'=>'admin']);
  Route::resource('/employee', 'EmployeeController', ['as'=>'admin']);
  Route::resource('/services', 'ServicesController', ['as'=>'admin']);
  Route::resource('/goods', 'GoodsController', ['as'=>'admin']);
  Route::resource('/banner', 'BannerController', ['as'=>'admin']);
  Route::resource('/opinion', 'OpinionController', ['as'=>'admin']);
  Route::resource('/object', 'ObjectController', ['as'=>'admin']);
  Route::get('/image/article/destroy', 'ArticleController@imageDestroy')->name('admin.image.article.destroy');
  Route::get('/image/company/destroy', 'CompanyController@imageDestroy')->name('admin.image.company.destroy');
  Route::get('/image/category/destroy', 'CategoryController@imageDestroy')->name('admin.image.category.destroy');
  Route::get('/master/work/choose', 'WorkController@master')->name('admin.master.work.choose');
  Route::get('/master/work/status', 'WorkController@status')->name('admin.master.work.status');
  Route::post('/master/work/time', 'WorkController@time')->name('admin.master.work.time');
  Route::get('/employee/work/addServices', 'EmployeeController@addServices')->name('admin.employee.work.addServices');
  Route::get('/employee/work/deleteServices', 'EmployeeController@deleteServices')->name('admin.employee.work.deleteServices');
  Route::get('/services/work/addServices', 'ServicesController@addServices')->name('admin.services.work.addServices');
  Route::get('/services/work/deleteServices', 'ServicesController@deleteServices')->name('admin.services.work.deleteServices');
  Route::post('/goods/addType/property', 'GoodsController@addProperty')->name('admin.goods.addType.property');
  Route::post('/goods/addType/coast', 'GoodsController@addCoast')->name('admin.goods.addType.coast');
  Route::post('/goods/deleteType/property', 'GoodsController@deleteProperty')->name('admin.goods.deleteType.property');
  Route::post('/goods/deleteType/coast', 'GoodsController@deleteCoast')->name('admin.goods.deleteType.coast');
  Route::get('/image/goods/destroy', 'GoodsController@imageDestroy')->name('admin.image.goods.destroy');
  Route::get('/goods/destroy/goods', 'GoodsController@destroyGoods')->name('admin.goods.destroy.goods');
});

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/offer', function () {
  return view('admin.offers.create', [
    'offer' => DB::table('offers')->get(),
  ]);
});
Route::get('/', function () {
  return view('2019.services-lipatkin-com.index', [
    'categories' => DB::table('categories')
    ->select('categories.id','categories.title', 'categories.slug', 'categories.main', 'categories.parent_id', 'categories.published',
    'images.images')
    ->leftJoin('images', 'categories.id', '=', 'images.category')
    ->where('categories.published', 1)
    ->where('main', '1')->get(),
    'banner' => DB::table('banners')->where('siteID', '1')->where('published', '1')->get(),
    'objects' => DB::table('objects')->where('siteID', '1')->where('published', '1')->get(),
    'company' => DB::table('companies')->leftJoin('images', 'companies.id', '=', 'images.company')
    ->where('companies.siteID', '1')->where('companies.published', 1)->get(),
    'categories_head' => DB::table('categories')
    ->select('categories.id','categories.title', 'categories.slug', 'categories.main', 'categories.parent_id', 'categories.published',
    'images.images', 'g.title as title_goods', 'g.slug as slug_goods')
    ->leftJoin('images', 'categories.id', '=', 'images.category')
    ->leftJoin('category_goods as cg', 'categories.id', '=', 'cg.category')
    ->leftJoin('goods as g', 'cg.goods', '=', 'g.id')
    ->where('categories.published', 1)
    ->get(),
  ]);
});
Route::get('/about', function () {
  return view('2019.services-lipatkin-com.about', [
    'company' => DB::table('companies')->leftJoin('images', 'companies.id', '=', 'images.company')
    ->where('companies.siteID', '1')->where('companies.published', 1)->get(),
    'categories_head' => DB::table('categories')
    ->select('categories.id','categories.title', 'categories.slug', 'categories.main', 'categories.parent_id', 'categories.published',
    'images.images')
    ->leftJoin('images', 'categories.id', '=', 'images.category')
    ->where('categories.published', 1)
    ->get(),
  ]);
});

Route::get('/product', function () {
  return view('2019.services-lipatkin-com.product', [
    'categories' => DB::table('categories')
    ->select('categories.id','categories.title', 'categories.slug', 'categories.main', 'categories.parent_id', 'categories.published',
    'images.images')
    ->leftJoin('images', 'categories.id', '=', 'images.category')
    ->where('categories.published', 1)
    ->get(),
    'categories_head' => DB::table('categories')
    ->select('categories.id','categories.title', 'categories.slug', 'categories.main', 'categories.parent_id', 'categories.published',
    'images.images')
    ->leftJoin('images', 'categories.id', '=', 'images.category')
    ->where('categories.published', 1)
    ->get(),
  ]);
});

Route::get('/objects', function () {
  return view('2019.services-lipatkin-com.object', [
  'objects' => DB::table('objects')->where('siteID', '1')->where('published', '1')->get(),
  'categories_head' => DB::table('categories')
  ->select('categories.id','categories.title', 'categories.slug', 'categories.main', 'categories.parent_id', 'categories.published',
  'images.images')
  ->leftJoin('images', 'categories.id', '=', 'images.category')
  ->where('categories.published', 1)
  ->get(),
  ]);
});

Route::get('/category', function () {
  return view('2019.category', [
    'categories' => DB::table('categories')->where('published', 1)->get(),
  ]);
});

Route::get('/contact', function () {
  return view('2019.services-lipatkin-com.contact', [
    'categories_head' => DB::table('categories')
    ->select('categories.id','categories.title', 'categories.slug', 'categories.main', 'categories.parent_id', 'categories.published',
    'images.images')
    ->leftJoin('images', 'categories.id', '=', 'images.category')
    ->where('categories.published', 1)
    ->get(),
    'contacts' => DB::table('contacts')->get(),
  ]);
});

Route::get('/reduct', function () {
  return view('2019.reduct', [
    'Article' => DB::table('articles')->get(),
  ]);
});
Auth::routes();

$categories = DB::table('categories')->get();

foreach($categories as $category){
  //dd($category->id);
  
      //dd($goods);
  Route::get('/'.$category->slug, function () {

    //dd($_GET);
    $goods = DB::table('goods as g')
      ->select('g.title', 'g.short_description', 'g.description', 'g.coast', 'g.min_coast', 'p.property', 'p.title as title_p', 'tp.type', 'images.images')
      ->leftJoin('category_goods as cg', 'g.id', '=', 'cg.goods')
      ->leftJoin('goods_property as gp', 'g.id', '=', 'gp.goods')
      ->leftJoin('property as p', 'gp.property', '=', 'p.id')
      ->leftJoin('type_property as tp', 'p.type', '=', 'tp.id')
      ->leftJoin('images', 'g.id', '=', 'images.goods')
      ->where('cg.category', $_GET['id'])

      ->get();
      //dd($goods);
  return view('2019.services-lipatkin-com.product2', [
    'categories_head' => DB::table('categories')
    ->select('categories.id','categories.title', 'categories.slug', 'categories.main', 'categories.parent_id', 'categories.published',
    'images.images')
    ->leftJoin('images', 'categories.id', '=', 'images.category')

    ->where('categories.published', 1)
    ->get(),
    'goods' => $goods,
      'categories_head' => DB::table('categories')
  ->select('categories.id','categories.title', 'categories.slug', 'categories.main', 'categories.parent_id', 'categories.published',
  'images.images')
  ->leftJoin('images', 'categories.id', '=', 'images.category')
  ->where('categories.published', 1)
  ->get(),
    'active_category' => DB::table('categories')->where('categories.published', 1)->get(),
    'id' => $_GET['id'],
    'objects' => DB::table('objects')->where('siteID', '1')->where('published', '1')->get(),
  ]);
});

}



Route::get('/home', 'HomeController@index')->name('home');
