<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('goods')->nullable();
            $table->integer('counts')->nullable();
            $table->integer('delivery')->nullable();
            $table->string('color')->nullable();
            $table->integer('status')->nullable();
            $table->integer('userid')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('trackid')->nullable();
            $table->integer('amount')->nullable();
            $table->string('tel')->nullable();
            $table->string('email')->nullable();
            $table->text('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
