<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('p_g_id')->nullable();
            $table->integer('grouping')->nullable();
            $table->string('unit')->nullble();
            $table->string('min_order')->nullalbe();
            $table->string('payment')->nullalbe();
            $table->string('delivery')->nullalbe();
            $table->integer('quantity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_products');
    }
}
