<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('goods')->nullable();
            $table->integer('siteID')->nullable();
            $table->integer('oldCount')->nullable();
            $table->integer('newCount')->nullable();
            $table->integer('allCount')->nullable();
            $table->integer('warningCount')->nullable();
            $table->integer('cost')->nullable();
            $table->integer('payment')->nullable();
            $table->integer('supplier')->nullable();
            $table->string('comment')->nullalbe();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
