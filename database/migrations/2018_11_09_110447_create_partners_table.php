<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
          $table->increments('id');
          $table->string('title')->nullable();
          $table->text('link')->nullable();
          $table->text('description')->nullable();
          $table->integer('published')->nullable();
          $table->integer('main')->nullable();
          $table->string('image')->nullable();
          $table->integer('siteID')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
