<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods_objects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('p_g_id')->nullable();
            $table->integer('grouping')->nullable();
            $table->string('area')->nullable();
            $table->string('living_area')->nullable();
            $table->string('floors')->nullable();
            $table->string('dimensios')->nullable();
            $table->string('bedroom')->nullable();
            $table->string('build_period')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_objects');
    }
}
