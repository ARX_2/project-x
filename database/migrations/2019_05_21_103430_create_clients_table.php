<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('tel')->nullable();
            $table->integer('card')->nullable();
            $table->integer('descount')->nullable();
            $table->datetime('birthdate')->nullable();
            $table->integer('sex')->nullable();
            $table->integer('vip')->nullable();
            $table->integer('blacklist')->nullable();
            $table->integer('from')->nullable();
            $table->integer('sms')->nullable();
            $table->string('comment')->nullable();
            $table->integer('siteID')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
