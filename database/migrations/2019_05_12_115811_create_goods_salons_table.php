<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoodsSalonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods_salons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->integer('count')->nullable();
            $table->integer('supplier')->nullable();
            $table->integer('type_count')->nullable();
            $table->integer('master_sale_type')->nullable();
            $table->integer('master_sale_count')->nullable();
            $table->integer('administrator_sale_type')->nullable();
            $table->integer('administrator_sale_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods_salons');
    }
}
