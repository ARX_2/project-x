<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('rod_padezh')->nullable();
            $table->string('dateln_padezh')->nullable();
            $table->string('area')->nullable();
            $table->string('area_rod_padezh')->nullable();
            $table->string('area_dateln_padezh')->nullable();
            $table->integer('area_cod')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
