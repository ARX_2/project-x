<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersSalonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_salons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user')->nullable();
            $table->integer('siteID')->nullable();
            $table->integer('check_master')->nullable();
            $table->integer('render_service')->nullable();
            $table->integer('sale_goods')->nullable();
            $table->integer('sale_aboniments')->nullable();
            $table->integer('sale_materials')->nullable();
            $table->integer('sale_sertificats')->nullable();
            $table->integer('take_discount')->nullable();
            $table->integer('pay_from_aboniments')->nullable();
            $table->integer('check_administrator')->nullable();
            $table->integer('all_services')->nullable();
            $table->integer('all_sale_goods')->nullable();
            $table->integer('all_sale_aboniments')->nullable();
            $table->integer('all_sale_angelfish')->nullable();
            $table->integer('all_sale_materials')->nullable();
            $table->integer('all_sale_sertificats')->nullable();
            $table->integer('show_in_chart')->nullable();
            $table->integer('take_discount_admin')->nullable();
            $table->integer('pay_from_aboniments_admin')->nullable();
            $table->integer('check_payday')->nullable();
            $table->integer('type_payday')->nullable();
            $table->integer('count1')->nullable();
            $table->integer('payday1')->nullable();
            $table->integer('count2')->nullable();
            $table->integer('payday2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_salons');
    }
}
