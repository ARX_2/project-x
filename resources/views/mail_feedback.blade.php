<html>
                <head>
                    <title>Новое сообщение</title>
                      <meta charset="utf-8">
                </head>
                <body>
                    <p>Добрый день! Поступил новый отзыв на сайт: {{$to}}</p>
                    <p><b>Описание</b>: {{$description or ''}}</p>
                    <a href="https://{{$to}}/admin/feedback/published/{{$id}}/0?mail=true">Опубликовать</a>
                </body>
            </html>
