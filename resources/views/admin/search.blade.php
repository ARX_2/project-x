

      <div class="row">
        <div class="col-lg-4">
            <h2>Управление товарами</h2>
        </div>
        <div class="col-lg-8">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.index')}}">Главная</a></li>
                <li class="active">Поиск</li>
            </ol>
        </div>
    </div>
    <hr>
        <div class="c_p_company">
            <div class="wide">
                <div class="box" style="position: fixed;">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        <div class="item">
                            <div class="name">
                                <a href="/admin/admin/company/upload?type=4">Описание на первой странице товаров</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="name">
                                <a href="" data-toggle="modal" data-target="#excelModalLong">Загрузка / выгрузка товаров и категорий</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="name">
                                <a href="" data-toggle="modal" data-target="#editsection">Переименовать раздел</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="name">
                                <span class="button-edit"><a href="{{$urlCreate or '/admin/goods/create'}}">Добавить</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="narrow">
                <div class="box">
                    <div class="production">
                        <div class="narrow_title">
                            <div class="left">
                                <i class="fa fa-refresh" aria-hidden="true" style="float:left; font-size:20px;color:#14aae6;padding: 5px;border-radius: 50px;" id="refresh"></i>
                                <h2 style="float:left;margin-left:10px;margin-top:5px;">Поиск</h2>
                                <p class="clearFix"></p>
                                <style media="screen">
                                  #refresh:hover{
                                    background-color: #e7e6e6;
                                  }
                                </style>
                            </div>
                            <div class="right">
                                <span class="button-edit"><a href="{{$urlCreate or '/admin/goods/create'}}">Добавить</a></span>
                            </div>
                        </div>
                        <div class="sort">
                            <div class="page_cat">
                                <span><a href="{{$urlActive or ''}}" class="active" id="link_active">Активные <span id="count_active"></span></a></span>
                                <span> | </span>
                                <span><a href="{{$urlHidden or ''}}" id="link_hidden">Скрытые <span id="count_hidden"></span></a></span>
                                {{--<span> | </span>
                                <span><a href="{{route('admin.goods.ShowDeleted')}}">Удаленные</a></span>--}}
                            </div>
                            <?php //dd($categories); ?>
                            <div class="sort_block">
                                <div class="category">
                                    <div class="left" style="margin-top: 5px;">
                                        <div class="name">Сортировать по:</div>
                                    </div>
                                    <div class="right" style="text-align: left;width:auto;">
                                        <button type="button" class="btn filter oldbtn" id="categories">Категории</button>
                                    </div>
                                    <div class="right" style="text-align: left;width:auto;margin-left:10px;">
                                        <button type="button" class="btn filter oldbtn" id="subcategories">Подкатегории</button>
                                    </div>
                                    @forelse($sections as $section)
                                    <div class="right" style="text-align: left;width:auto;margin-left:10px;">
                                        <button type="button" class="btn filter oldbtn" id="{{$section->slug}}">{{$section->title or ''}}</button>

                                    </div>
                                    @empty
                                    @endforelse
                                </div>
                                <!-- <div class="price">
                                    <div class="left">
                                        <div class="name">Цена:</div>
                                    </div>
                                    <div class="right">
                                        <input type="text" id="min_coast"   >
                                        <span> - </span>
                                        <input type="text" id='max_coast'  >
                                    </div>
                                </div>
                                <div class="sort_button">
                                    <a id="filtr" style="cursor:pointer"><div class="button">Применить</div></a>
                                </div> -->
                            </div>
                        </div>
                        <script type="text/javascript">

                        </script>
                        <script type="text/javascript">
                          $('.filter').click(function(){
                            if($(this).hasClass('oldbtn')){
                              $('.btn').removeClass('btn-outline-primary');
                              $('.btn').addClass('oldbtn');
                              $('.section').hide();
                              var section = $(this).attr('id');
                              $('.'+section).show();
                              $(this).addClass('btn-outline-primary');
                              $(this).removeClass('oldbtn');
                            }
                            else{
                              $(this).removeClass('btn-outline-primary');
                              $(this).addClass('oldbtn');
                              $('.section').show();
                            }

                          });
                        </script>

                        <div class="categories section">
                          <h4>Категории</h4>
                          @forelse($categories as $category)
                          @if($category->parent_id == null)
                          <div class="item">
                              <div class="image">
                                  <img src="{{asset('storage/app')}}/{{Resize::ResizeImage($category->images, '80x80')}}" alt="">
                              </div>
                              <div class="info_goods">
                                  <div class="block">
                                      <div class="name"><a href=""data-toggle="modal" data-target="#exampleModalLong">{{$category->title}}</a></div>
                                      <div class="price">Цена: <span>{{$category->coast}}</span> руб.</div>
                                  </div>
                                  <div class="additionally">
                                      <div class="p_category">Раздел: <span>{{$category->section->title or 'не выбран'}}</span></div>
                                      <div class="date">Добавлено: <span>{{Resize::date($category->created_at)}}</span><span> | </span> Обновлено: <span>{{Resize::date($category->updated_at)}}</span></div>
                                  </div>
                              </div>
                              <div class="status">
                                  <span class="sign eae @if($category->published == 1)active @endif"><a href="{{route('admin.category.update.id', ['published',$category->id,$category->published])}}" class="link_published"></a></span>
                                  <span class="sign star @if($category->main == 1)activeStar @endif"><a href="{{route('admin.category.update.id', ['main',$category->id,(int)$category->main])}}"></a></span>
                              </div>
                              <div class="action">
                                  <a href="#" class="dropdown-toggle edit" data-toggle="dropdown" ></a>
                                  <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                      <!-- <li><a href="{{route('admin.goods.update.id', ['published',$category->id,$category->published])}}" class="link_published">@if($category->published == 1)Скрыть @else Показать @endif</a></li> -->
                                      {{--<li><a href="{{route('admin.category.destroy.goods', ['id'=>$category->id, 'deleted' => $category->deleted])}}">@if($category->deleted == 0 || $category->deleted == null)Удалить @else Восстановить @endif</a></li>--}}
                                      <li><a href="{{route('admin.category.edit', [$category->id])}}?id={{$category->id}}" class="link_edit">Редактировать</a></li>
                                      <li><a href="{{route('admin.category.update.id', ['main',$category->id,(int)$category->main])}}">@if($category->main == 0)На главную @else Снять с главной @endif</a></li>
                                  </ul>
                              </div>
                          </div>
                          @endif
                          @empty
                          @endforelse
                        </div>
                        <div class="subcategories section">
                          <h4>Подкатегории</h4>
                          @forelse($categories as $category)
                          @if($category->parent_id !== null)
                          <?php //dd($category); ?>
                          <div class="item">
                              <div class="image">
                                  <img src="{{asset('storage/app')}}/{{Resize::ResizeImage($category->images, '80x80')}}" alt="">
                              </div>
                              <div class="info_goods">
                                  <div class="block">
                                      <div class="name"><a href=""data-toggle="modal" data-target="#exampleModalLong">{{$category->title}}</a></div>
                                      <div class="price">Цена: <span>{{$category->coast}}</span> руб.</div>
                                  </div>
                                  <div class="additionally">
                                      <div class="p_category">Раздел: <span>{{$category->categor->section->title or 'не выбран'}}</span></div>
                                      <div class="date">Добавлено: <span>{{Resize::date($category->created_at)}}</span><span> | </span> Обновлено: <span>{{Resize::date($category->updated_at)}}</span></div>
                                  </div>
                              </div>
                              <div class="status">
                                  <span class="sign eae @if($category->published == 1)active @endif"><a href="{{route('admin.category.update.id', ['published',$category->id,$category->published])}}" class="link_published"></a></span>
                                  <span class="sign star @if($category->main == 1)activeStar @endif"><a href="{{route('admin.category.update.id', ['main',$category->id,(int)$category->main])}}"></a></span>
                              </div>
                              <div class="action">
                                  <a href="#" class="dropdown-toggle edit" data-toggle="dropdown" ></a>
                                  <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                      <!-- <li><a href="{{route('admin.goods.update.id', ['published',$category->id,$category->published])}}" class="link_published">@if($category->published == 1)Скрыть @else Показать @endif</a></li> -->
                                      {{--<li><a href="{{route('admin.category.destroy.goods', ['id'=>$category->id, 'deleted' => $category->deleted])}}">@if($category->deleted == 0 || $category->deleted == null)Удалить @else Восстановить @endif</a></li>--}}
                                      <li><a href="{{route('admin.category.edit', [$category->id])}}?id={{$category->id}}" class="link_edit">Редактировать</a></li>
                                      <li><a href="{{route('admin.category.update.id', ['main',$category->id,(int)$category->main])}}">@if($category->main == 0)На главную @else Снять с главной @endif</a></li>
                                  </ul>
                              </div>
                          </div>
                          @endif
                          @empty
                          @endforelse
                      </div>
                      @forelse($sections as $section)
                      <div class="{{$section->slug or ''}} section">
                        <h4>{{$section->title}}</h4>
                        @forelse($goods as $g)
                        <?php //dd($g); ?>
                        @if($section->type !== $g->categories->type)
                        @if($section->type == 1)
                        <?php $type = 'service'; $nextType = 'services' ?>
                        @elseif($section->type == 0)
                        <?php $type = 'goods'; $nextType = 'goods' ?>
                        @else
                        <?php $type = $section->slug; $nextType =$section->slug ?>
                        @endif
                        <?php //dd($category); ?>
                        <div class="item" id="arrayorder_{{$g->id}}">
                            <div class="image">
                                <img src="{{asset('storage/app')}}/{{Resize::ResizeImage($g->images, '80x80')}}" alt="">
                            </div>
                            <div class="info_goods">
                                <div class="block">
                                    <div class="name"><a href=""data-toggle="modal" data-target="#exampleModalLong">{{$g->title}}</a></div>
                                    <div class="price">Цена: <span>{{$g->coast}}</span> руб.</div>
                                </div>
                                <div class="additionally">
                                    <div class="p_category">Раздел: <span>{{$section->title or 'не выбран'}}</span></div>
                                    <div class="date">Добавлено: <span>{{Resize::date($g->created_at)}}</span><span> | </span> Обновлено: <span>{{Resize::date($g->updated_at)}}</span></div>
                                </div>
                            </div>
                            <div class="status">
                                <span class="sign eae @if($g->published == 1)active @endif"><a href="{{route('admin.'.$type.'.update.id', ['published',$g->id,$g->published])}}" class="link_published"></a></span>
                                <span class="sign search @if($g->checkseo == 1)active @endif"></span>
                                <span class="sign star @if($g->main == 1)activeStar @endif"><a href="{{route('admin.'.$type.'.update.id', ['main',$g->id,(int)$g->main])}}"></a></span>
                            </div>
                            <div class="action">
                                <a href="#" class="dropdown-toggle edit" data-toggle="dropdown" ></a>
                                <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">

                                    {{--<li><a href="{{route('admin.$section->slug.destroy.goods', ['id'=>$g->id, 'deleted' => $g->deleted])}}">@if($g->deleted == 0 || $g->deleted == null)Удалить @else Восстановить @endif</a></li>--}}
                                    <li><a href="{{route('admin.'.$nextType.'.edit', [$g->id])}}?id={{$g->id}}" class="link_edit">Редактировать</a></li>
                                    <li><a href="{{route('admin.'.$type.'.update.id', ['main',$g->id,(int)$g->main])}}">@if($g->main == 0)На главную @else Снять с главной @endif</a></li>
                                </ul>
                            </div>
                        </div>
                        @endif
                        @empty
                        @endforelse
                    </div>
                    @empty
                    @endforelse
                    </div>
                </div>
            </div>
        </div>
        <style>

        .prokrutka {
        height: 55vh;
        width: 100%; /* ширина нашего блока */
        background: #fff; /* цвет фона, белый */
        overflow-y: scroll; /* прокрутка по вертикали */
        }

    ::-webkit-scrollbar {
    width: 3px; height: 3px;
    }


    ::-webkit-scrollbar-track-piece  {
    background-color: #c7c7c7;
    }

    ::-webkit-scrollbar-thumb:vertical {
    height: 50px; background-color: #666; border-radius: 3px;
    }

    ::-webkit-scrollbar-thumb:horizontal {
    height: 50px; background-color: #666; border-radius: 3px;
    }

    ::-webkit-scrollbar-corner{
      background-color: #999;
    }
    .emoji{
      width: 18px !important; height:18px !important;
    }
        </style>

        <script type="text/javascript">
          $('.eae').click(function(){
            var link = $(this).find('a').attr('href');
            $(this).parent('.status').parent('.item').remove();
            // var count_active = $('#count_active').html();
            // count_active = Number.parseInt(count_active);
            //
            //
            // var count_hidden = $('#count_hidden').html();
            // count_hidden = Number.parseInt(count_hidden);
            // var check = $('#link_active').hasClass('active');
            // if(check == true){
            //   count_active = count_active-1;
            //   count_hidden = count_hidden+1;
            // }
            // else{
            //   count_active = count_active+1;
            //   count_hidden = count_hidden-1;
            // }
            //
            // $('#count_active').text(count_active);
            // $('#count_hidden').text(count_hidden);

            $.get(link, function(data){});

          });

          $('.star').click(function(){
            var link = $(this).find('a').attr('href');
            var check =  $(this).hasClass('activeStar');
            if(check == true){
              $(this).removeClass('activeStar');
            }
            else{
              $(this).addClass('activeStar');
            }
            $.get(link, function(data){});

          });
        </script>


    <div class="modal fade" id="editsection" tabindex="-1" role="dialog" aria-labelledby="editsection" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content vm-goods">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="modal-body modal_edit">
                    <div class="content">
                        <div class="vm-header">
                            <div class="name">Переименовать раздел "Товары"</div>
                        </div>
                        <div class="vm-body">
                            <div class="vm-edit">
                              <form class="" action="{{route('admin.change.section')}}" method="post">
                                {{ csrf_field()}}
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Название раздела:</label></div>
                                    <div class="center">
                                        <input type="text" name="goods" class="" placeholder="Товары" value="">
                                    </div>
                                </div>

                                <div class="perent" style="margin-bottom: 0">
                                    <div class="left"><label for="" class="item_name"></label></div>
                                    <div class="center">
                                        <input class="button-save" id="saveSection" type="submit" value="Сохранить" style="width: 115px;background-color: #269ffd;color: #fff;">
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="excelModalLong" tabindex="-1" role="dialog" aria-labelledby="excelModalLongTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content vm-goods">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <div class="modal-body">
                <div class="content">

                    <div class="vm-goods">
                        <div class="vm-header">
                            <div class="block">
                                <div class="psh">
                                    <div class="left">
                                        <div class="name">Управление кампанией с помощью XLSX</div>
                                    </div>
                                    <div class="right"></div>
                                </div>
                            </div>
                        </div>

                        <div class="vm-body">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Выгрузка товаров</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Загрузка товаров</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Обновить количество товаров</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab">Архив</a>
                                </li>
                            </ul><!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane" id="tabs-1" role="tabpanel">
                                    <p>Формат XLSX</p>
                                    <a href="#" id="excelExport" class="button-xlsx" style="padding: 3px 10px 5px;display: table; color: #222; background: #ffdb4d; border-radius: 3px;">Выгрузить</a>
                                    <i id="excelUrl"> <img src="{{asset('public/img')}}/giphy.gif" alt="" id="loader" style="display:none; width:50px;"> </i>
                                </div>
                                <div class="tab-pane active" id="tabs-2" role="tabpanel">
                                    <p>Формат XLSX</p>
                                    <form class="" action="{{route('admin.goods.uploadExcel')}}" method="post" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                      <input type="hidden" name="type" value="1" id="uploadExcel">
                                    <input type="file" name="file" style="margin-bottom: 15px;">
                                    <button type="submit" class="button-xlsx" style="border: none;padding: 3px 10px 5px;display: table; color: #222; background: #ffdb4d; border-radius: 3px;">Загрузить</button>
                                    </form>
                                </div>
                                <div class="tab-pane" id="tabs-3" role="tabpanel">
                                    <p>Формат XLSX</p>
                                    <form class="" action="{{route('admin.goods.uploadExcel')}}" method="post" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                    <input type="hidden" name="type" value="2" id="updateExcel">
                                    <input type="file" name="file" style="margin-bottom: 15px;">
                                    <button type="submith" class="button-xlsx" style="border: none;padding: 3px 10px 5px; display: table; color: #222; background: #ffdb4d; border-radius: 3px;">Обновить</button>
                                  </form>
                                </div>
                                <div class="tab-pane" id="tabs-4" role="tabpanel">
                                    <p>Формат XLSX</p>

                                </div>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

    ajaxForm();
    function ajaxForm(){
      $('#excelExport').click(function(){
        $('#loader').show();
        var data = new FormData($("#form")[0]);
          $.ajax({
              type: 'GET',
              url: "{{route('admin.goods.excelExport')}}",
              data: data,
              headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
              contentType: false,
              processData : false,
              success: function(data) {
                  $('#excelUrl').html(data);
              }
          });
          return false;
      });
    }
    $('#refresh').click(function(){
      $('#refresh').addClass('rotate');
      $.get('{{$link or ''}}?ajax=true', function(data){
        $('.admin-content').html(data);
      });
      history.pushState(null, null, link);
    });

    $('#link_active').click(function(){

      var active_link = $(this).attr('href');
      $.get(active_link+'?ajax=true', function(data){
        $('.admin-content').html(data);
        $('#link_hidden').removeClass('active');
        $('#link_active').addClass('active');
      });
      history.pushState(null, null, active_link);
      return false;
    });
    $('#link_hidden').click(function(){

      var hidden_link = $(this).attr('href');
      $.get(hidden_link+'?ajax=true', function(data){
        $('.admin-content').html(data);
        $('#link_active').removeClass('active');
        $('#link_hidden').addClass('active');
      });
      history.pushState(null, null, hidden_link);
      return false;
    });

    $('.button-edit').click(function(){
      var link = $('.button-edit').find('a').attr('href');
        $.get(link+'?ajax=true', function(data){
          $('.admin-content').html(data);
        });
        history.pushState(null, null, link);
        return false;
    });

    $('.link_edit').click(function(){
      var link = $(this).attr('href');
        $.get(link+'&&ajax=true', function(data){
          $('.admin-content').html(data);
        });
        history.pushState(null, null, link);
        return false;
    });
    </script>

    <style media="screen">
      .rotate{
        -webkit-animation-name: cog;
-webkit-animation-duration: 1s;
-webkit-animation-iteration-count: infinite;
-webkit-animation-timing-function: linear;
-moz-animation-name: cog;
-moz-animation-duration: 1s;
-moz-animation-iteration-count: infinite;
-moz-animation-timing-function: linear;
-ms-animation-name: cog;
-ms-animation-duration: 1s;
-ms-animation-iteration-count: infinite;
-ms-animation-timing-function: linear;

animation-name: cog;
animation-duration: 1s;
animation-iteration-count: infinite;
animation-timing-function: linear;
}
@-ms-keyframes cog {
from { -ms-transform: rotate(0deg); }
to { -ms-transform: rotate(20deg); }
}
@-moz-keyframes cog {
from { -moz-transform: rotate(0deg); }
to { -moz-transform: rotate(20deg); }
}
@-webkit-keyframes cog {
from { -webkit-transform: rotate(0deg); }
to { -webkit-transform: rotate(20deg); }
}
@keyframes cog {
from {
transform:rotate(0deg);
}
to {
transform:rotate(360deg);
}
      }
    .btn-outline-primary{
      color: #007bff;
  background-color: transparent;
  background-image: none;
  border-color: #007bff;
    }
    .oldbtn{
      background-color: #e8e8e8;
    border-color: #c7c7c7;
    }
    </style>
    <script type="text/javascript" src="/public/admin/js/jquery.mask.js"></script>
