@include('admin.index')
    <script src="/public/admin/js/maskedinput.min.js"></script>
        <div class="row">
            <div class="col-sm-4">
                <h2>Управление компанией</h2>
            </div>
            <div class="col-sm-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Технические настройки, оформление сайта</li>
                </ol>
            </div>
        </div>
        <hr>
        <form class="" action="{{route('admin.settings.design.update', 'color')}}" method="post" enctype="multipart/form-data">
        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="help">
                        <div class="wide_title">
                            <h2>Правая штука</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="narrow_title">
                        <h2>Редактировать цвет сайта</h2>
                    </div>
                    <div class="a_design @if($company->info->color_menu !== null) c-{{$company->info->color_menu}} @endif">
                        <style>
                            .demo_header .top_header{width:100%;display:table;background-color:#fff;padding:3px 0;border-top:3px solid #269ffd}
                            .demo_header .top_header .left{display:inline-flex}
                            .demo_header .top_header .i{padding:0 10px;color:#656565;border-right:1px solid #e6e6e6}
                            .demo_header .top_header .i:first-child{border-left:1px solid #e6e6e6}
                            .demo_header .top_header .i span{padding-left:10px}
                            .demo_header .top_header .right{float:right}
                            .demo_header .top_header .right .i{margin-right:0}
                            .demo_header .logo{margin:5px 0;height:60px;float:left;width:100%;max-width:210px}
                            .demo_header .brand{background-size:contain;background-position:left;height:60px;padding:0;width:100%;overflow:hidden;background-repeat:no-repeat;display:block}
                            .demo_header .body_header{background-color:#269ffd;width:100%;padding:0 10px}
                            .demo_header .body_header ul{margin:0;padding:0;float:right;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-webkit-justify-content:flex-end;-ms-flex-pack:inherit;justify-content:flex-end;text-align:left}
                            .demo_header .body_header li{display:block;position:relative;text-transform:uppercase;font-weight:600;text-decoration:none;font-size:13px}
                            .demo_header .body_header li a{padding:25.5px 13px;display:block;color:#fff}
                            .demo_header .body_header li i{padding-left:5px}
                        </style>
                        <?php //dd($company); ?>
                        <div class="demo_header">
                            <div class="top_header">
                                <div class="left">
                                    <div class="i"><i class="fa fa-map-marker red" aria-hidden="true"></i><span>г. Город</span></div>
                                    <div class="i"><i class="fa fa-envelope red" aria-hidden="true"></i><span>email@company.com</span></div>
                                    <div class="i"><i class="fa fa-phone red" aria-hidden="true"></i><span>+7 (900) 000-00-00</span></div>
                                </div>
                                <div class="right">
                                    <div class="i"><i class="fa fa-user red" aria-hidden="true"></i><span>Войти</span></div>
                                </div>
                            </div>
                            <div class="body_header" >
                                <div class="logo"><a href="javascript:void(0);" class="brand" style="background-image: url(/storage/app/{{$company->info->logo1}});"></a></div>
                                <div class="nav">
                                    <ul>
                                        <li><a href="javascript:void(0);">Компания<i class="fa fa-caret-down"></i></a></li>
                                        <li><a href="javascript:void(0);">Услуги<i class="fa fa-caret-down"></i></a></li>
                                        <li><a href="javascript:void(0);">Наши работы</a></li>
                                        <li><a href="javascript:void(0);">Контакты</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <style>
                            .color_edit{margin:25px 0;display:inline-flex}
                            .color_edit .left{width:120px;min-width:120px;float:left}
                            .color_edit .left .name{padding:7px 0;font-weight:600}
                            .color_edit .color_check{width:100%}
                            .color_edit .i{width:25px;height:25px;background:#f60}
                            .s_action{display:block;float:left;border:1px solid #e6e6e6;cursor:pointer;width:35px;height:35px;margin-right:5px;margin-bottom:5px}
                            .s_action.back{background-color:#269ffd}
                            input[type="radio"] + span{border:0;margin:0;width:1px;height:1px}
                            .s_action.back span{color:#fff}
                            .radio-name{vertical-align:top;font-weight:400}
                        </style>

                          {{csrf_field()}}
                        <div class="color_edit">
                            <div class="left">
                                <div class="name">Выберите цвет:</div>
                            </div>
                            <div class="color_check">
                                <!-- Красный -->
                                <style>
                                    .back.c-d02223{background: #d02223;color: #fff}
                                    .back.c-e23c42{background: #e23c42;color: #fff}
                                    .back.c-f56d53{background: #f56d53;color: #fff}

                                    .c-d02223 .demo_header .body_header{background-color: #d02223}.c-d02223 .demo_header .top_header{border-top: 3px solid #d02223}
                                    .c-e23c42 .demo_header .body_header{background-color: #e23c42}.c-e23c42 .demo_header .top_header{border-top: 3px solid #e23c42}
                                    .c-f56d53 .demo_header .body_header{background-color: #f56d53}.c-f56d53 .demo_header .top_header{border-top: 3px solid #f56d53}
                                </style>
                                <label class="s_action c-d02223 @if($company->info->color_menu == 'd02223') back @endif" style="border: 2px solid #d02223"><input id="c-d02223" type="radio" name="color" hidden="" value="d02223,fff" @if($company->info->color_menu == 'd02223') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-e23c42 @if($company->info->color_menu == 'e23c42') back @endif" style="border: 2px solid #e23c42"><input id="c-e23c42" type="radio" name="color" hidden="" value="e23c42,fff" @if($company->info->color_menu == 'e23c42') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-f56d53 @if($company->info->color_menu == 'f56d53') back @endif" style="border: 2px solid #f56d53"><input id="c-f56d53" type="radio" name="color" hidden="" value="f56d53,fff" @if($company->info->color_menu == 'f56d53') checked @endif><span></span><span class="radio-name"></span></label>


                                <!-- Оранжевый -->
                                <style>
                                    .back.c-f36017{background: #f36017;color: #fff}
                                    .back.c-ffb001{background: #ffb001;color: #fff}
                                    .back.c-ffc387{background: #ffc387;color: #222}
                                    .c-f36017 .demo_header .body_header{background-color: #f36017}.c-f36017 .demo_header .top_header{border-top: 3px solid #f36017}
                                    .c-ffb001 .demo_header .body_header{background-color: #ffb001}.c-ffb001 .demo_header .top_header{border-top: 3px solid #ffb001}
                                    .c-ffc387 .demo_header .body_header{background-color: #ffc387}.c-ffc387 .demo_header .top_header{border-top: 3px solid #ffc387}
                                </style>
                                <label class="s_action c-f36017 @if($company->info->color_menu == 'f36017') back @endif" style="border: 2px solid #f36017"><input id="c-f36017" type="radio" name="color" hidden="" value="f36017,fff" @if($company->info->color_menu == 'f36017') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-ffb001 @if($company->info->color_menu == 'ffb001') back @endif" style="border: 2px solid #ffb001"><input id="c-ffb001" type="radio" name="color" hidden="" value="ffb001,fff" @if($company->info->color_menu == 'ffb001') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-ffc387 @if($company->info->color_menu == 'ffc387') back @endif" style="border: 2px solid #ffc387"><input id="c-ffc387" type="radio" name="color" hidden="" value="ffc387,fff" @if($company->info->color_menu == 'ffc387') checked @endif><span></span><span class="radio-name"></span></label>
                                <!-- Желтый -->
                                <style>
                                    .back.c-efde74{background: #efde74;color: #fff}
                                    .back.c-f7d700{background: #f7d700;color: #222}
                                    .c-efde74 .demo_header .body_header{background-color: #efde74}.c-efde74 .demo_header .top_header{border-top: 3px solid #efde74}
                                    .c-f7d700 .demo_header .body_header{background-color: #f7d700}.c-f7d700 .demo_header .top_header{border-top: 3px solid #f7d700}
                                    .c-f7d700 .demo_header .body_header a{color: #222}
                                </style>
                                <label class="s_action c-f7d700 @if($company->info->color_menu == 'f7d700') back @endif" style="border: 2px solid #f7d700"><input id="c-f7d700" type="radio" name="color" hidden="" value="f7d700,fff" @if($company->info->color_menu == 'f7d700') checked @endif><span></span><span class="radio-name"></span></label>
                                <!-- Зеленый -->
                                <style>
                                    .back.c-b3d886{background: #b3d886;color: #222}
                                    .back.c-8bc346{background: #8bc346;color: #fff}
                                    .back.c-5baa00{background: #5baa00;color: #fff}
                                    .back.c-32a601{background: #32a601;color: #fff}
                                    .c-b3d886 .demo_header .body_header{background-color: #b3d886;}.c-b3d886 .demo_header .top_header{border-top: 3px solid #b3d886}
                                    .c-b3d886 .demo_header .body_header a{color: #222}
                                    .c-8bc346 .demo_header .body_header{background-color: #8bc346;}.c-8bc346 .demo_header .top_header{border-top: 3px solid #8bc346}
                                    .c-5baa00 .demo_header .body_header{background-color: #5baa00;}.c-5baa00 .demo_header .top_header{border-top: 3px solid #5baa00}
                                    .c-32a601 .demo_header .body_header{background-color: #32a601;}.c-32a601 .demo_header .top_header{border-top: 3px solid #32a601}
                                </style>
                                <label class="s_action c-b3d886 @if($company->info->color_menu == 'b3d886') back @endif" style="border: 2px solid #b3d886"><input id="c-b3d886" type="radio" name="color" hidden="" value="b3d886,fff" @if($company->info->color_menu == 'b3d886') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-8bc346 @if($company->info->color_menu == '8bc346') back @endif" style="border: 2px solid #8bc346"><input id="c-8bc346" type="radio" name="color" hidden="" value="8bc346,fff" @if($company->info->color_menu == '8bc346') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-5baa00 @if($company->info->color_menu == '5baa00') back @endif" style="border: 2px solid #5baa00"><input id="c-5baa00" type="radio" name="color" hidden="" value="5baa00,fff" @if($company->info->color_menu == '5baa00') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-32a601 @if($company->info->color_menu == '32a601') back @endif" style="border: 2px solid #32a601"><input id="c-32a601" type="radio" name="color" hidden="" value="32a601,fff" @if($company->info->color_menu == '32a601') checked @endif><span></span><span class="radio-name"></span></label>
                                <!-- Голубой -->
                                <style>
                                    .back.c-39a1ae{background: #39a1ae;color: #fff}
                                    .back.c-01b4c8{background: #01b4c8;color: #fff}
                                    .back.c-269ffd{background: #269ffd;color: #fff}
                                    .c-39a1ae .demo_header .body_header{background-color: #39a1ae;}.c-39a1ae .demo_header .top_header{border-top: 3px solid #39a1ae}
                                    .c-01b4c8 .demo_header .body_header{background-color: #01b4c8;}.c-01b4c8 .demo_header .top_header{border-top: 3px solid #01b4c8}
                                    .c-269ffd .demo_header .body_header{background-color: #269ffd;}.c-269ffd .demo_header .top_header{border-top: 3px solid #269ffd}
                                </style>
                                <label class="s_action c-39a1ae @if($company->info->color_menu == '39a1ae') back @endif" style="border: 2px solid #39a1ae"><input id="c-39a1ae" type="radio" name="color" hidden="" value="39a1ae,fff" @if($company->info->color_menu == '39a1ae') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-01b4c8 @if($company->info->color_menu == '01b4c8') back @endif" style="border: 2px solid #01b4c8"><input id="c-01b4c8" type="radio" name="color" hidden="" value="01b4c8,fff" @if($company->info->color_menu == '01b4c8') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-269ffd @if($company->info->color_menu == '269ffd') back @endif" style="border: 2px solid #269ffd"><input id="c-269ffd" type="radio" name="color" hidden="" value="269ffd,fff" @if($company->info->color_menu == '269ffd') checked @endif><span></span><span class="radio-name"></span></label>
                                <!-- Синий -->
                                <style>
                                    .back.c-0170cc{background: #0170cc;color: #fff}
                                    .c-0170cc .demo_header .body_header{background-color: #0170cc;}.c-0170cc .demo_header .top_header{border-top: 3px solid #0170cc}
                                </style>
                                <label class="s_action c-0170cc @if($company->info->color_menu == '0170cc') back @endif" style="border: 2px solid #0170cc"><input id="c-0170cc" type="radio" name="color" hidden="" value="0170cc,fff"  @if($company->info->color_menu == '0170cc') checked @endif><span></span><span class="radio-name"></span></label>
                                <!-- Фиолетовый -->
                                <style>
                                    .back.c-bb4fbc{background: #bb4fbc;color: #fff}
                                    .back.c-c0518d{background: #c0518d;color: #fff}
                                    .back.c-8c4f96{background: #8c4f96;color: #fff}
                                    .back.c-4f4ca7{background: #4f4ca7;color: #fff}
                                    .back.c-35184e{background: #35184e;color: #fff}
                                    .c-bb4fbc .demo_header .body_header{background-color: #bb4fbc;}.c-bb4fbc .demo_header .top_header{border-top: 3px solid #bb4fbc}
                                    .c-c0518d .demo_header .body_header{background-color: #c0518d;}.c-c0518d .demo_header .top_header{border-top: 3px solid #c0518d}
                                    .c-8c4f96 .demo_header .body_header{background-color: #8c4f96;}.c-8c4f96 .demo_header .top_header{border-top: 3px solid #8c4f96}
                                    .c-4f4ca7 .demo_header .body_header{background-color: #4f4ca7;}.c-4f4ca7 .demo_header .top_header{border-top: 3px solid #4f4ca7}
                                    .c-35184e .demo_header .body_header{background-color: #35184e;}.c-35184e .demo_header .top_header{border-top: 3px solid #35184e}
                                </style>
                                <label class="s_action c-bb4fbc @if($company->info->color_menu == 'bb4fbc') back @endif" style="border: 2px solid #bb4fbc"><input id="c-bb4fbc" type="radio" name="color" hidden="" value="bb4fbc,fff" @if($company->info->color_menu == 'bb4fbc') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-c0518d @if($company->info->color_menu == 'c0518d') back @endif" style="border: 2px solid #c0518d"><input id="c-c0518d" type="radio" name="color" hidden="" value="c0518d,fff" @if($company->info->color_menu == 'c0518d') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-8c4f96 @if($company->info->color_menu == '8c4f96') back @endif" style="border: 2px solid #8c4f96"><input id="c-8c4f96" type="radio" name="color" hidden="" value="8c4f96,fff" @if($company->info->color_menu == '8c4f96') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-4f4ca7 @if($company->info->color_menu == '4f4ca7') back @endif" style="border: 2px solid #4f4ca7"><input id="c-4f4ca7" type="radio" name="color" hidden="" value="4f4ca7,fff" @if($company->info->color_menu == '4f4ca7') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-35184e @if($company->info->color_menu == '35184e') back @endif" style="border: 2px solid #35184e"><input id="c-35184e" type="radio" name="color" hidden="" value="35184e,fff" @if($company->info->color_menu == '35184e') checked @endif><span></span><span class="radio-name"></span></label>
                                <!-- Белый / Черный -->
                                <style>
                                    .back.c-ffffff{background: #ffffff;color: #222;border: 2px solid #269ffd!important;}
                                    .back.c-dadada{background: #dadada;color: #fff}
                                    .back.c-242424{background: #242424;color: #fff}
                                    .c-ffffff .demo_header .body_header{background-color: #ffffff;}.c-ffffff .demo_header .top_header{border-top: 3px solid #ffffff;background-color:#f3f3f3;}
                                    .c-ffffff .demo_header .body_header a{color: #222}
                                    .c-dadada .demo_header .body_header{background-color: #dadada;}.c-dadada .demo_header .top_header{border-top: 3px solid #dadada}
                                    .c-dadada .demo_header .body_header a{color: #222}
                                    .c-242424 .demo_header .body_header{background-color: #242424;}.c-242424 .demo_header .top_header{border-top: 3px solid #242424}
                                </style>
                                <label class="s_action c-ffffff @if($company->info->color_menu == 'ffffff') back @endif" style="border: 2px solid #eeeeee"><input id="c-ffffff" type="radio" name="color" hidden="" value="ffffff,222" @if($company->info->color_menu == 'ffffff') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-dadada @if($company->info->color_menu == 'dadada') back @endif" style="border: 2px solid #dadada"><input id="c-dadada" type="radio" name="color" hidden="" value="dadada,222" @if($company->info->color_menu == 'dadada') checked @endif><span></span><span class="radio-name"></span></label>
                                <label class="s_action c-242424 @if($company->info->color_menu == '242424') back @endif" style="border: 2px solid #242424"><input id="c-242424" type="radio" name="color" hidden="" value="242424,fff" @if($company->info->color_menu == '242424') checked @endif><span></span><span class="radio-name"></span></label>
                            </div>
                        </div>
                        <style>
                            .d_edit .perent{margin-bottom:20px;display:inline-flex;width:100%}
                            .perent .left{float:left;width:120px;min-width:120px;padding-right:20px}
                            .center{width:100%;float:left}
                            input{background-color:#fff;width:100%;height:36px;border:1px solid #ccc;font-size:14px;font-weight:400;border-radius:5px;color:#222;padding-left:5px}
                            .perent .right{width:20%;float:left;padding-left:15px;position:relative}
                            .perent .right .help:before{font-family:FontAwesome;content:"\f29c";line-height:36px;font-size:18px;color:#2196f3}
                            .perent .item_button{width:115px;background:#269ffd;color:#fff;border:1px solid #fff}
                            .perent .image_group{display:table;width:100%}
                            .perent .image_group .image{width:100px;height:75px;padding:2px;display:table;float:left;border:1px solid #ddd;margin:0 10px 10px 0;position:relative}
                            .perent .image_group img{width:100px;height:75px;object-fit:cover}
                            .perent .image_group .image .delete:before{position:absolute;right:2px;top:2px;line-height:16px;border:1px solid #ddd;font-size:20px;background:#2196F3;font-family:FontAwesome;color:#fff;content:"\f057";border-radius:50%}
                        </style>
                        <div class="d_edit">
                            <div class="perent">
                                <div class="left"><label for="" class="item_name">Или укажите свой цвет:</label></div>
                                <div class="center"><input type="text" id="color_write" name="color_menu" class="" placeholder="8c4f96" value="{{$company->info->color_menu or ''}}" ></div>
                                <div class="right"><div class="help"></div></div>
                            </div>
                            <div class="perent">
                                <div class="left"><label for="" class="item_name" style="margin: 8px 0;">Цвет текста:</label></div>
                                <div class="center"><input type="text" name="color_text_menu" class="" placeholder="8c4f96" value="{{$company->info->color_text_menu or ''}}" ></div>
                                <div class="right"><div class="help"></div></div>
                            </div>
                            <div class="perent" style="margin-top: 10px;">
                                <div class="left"><label for="" class="item_name" style="">Изображение:</label></div>
                                <div class="center">
                                    <input type="file" name="file" class="" value=""  style="border: 0;padding-left: 0;">
                                    <?php //dd($company);?>
                                    @if($company->info->image_menu !== null)
                                    <?php
                                        //dd($company->info);
                                    ?>
                                    <div class="image_group">
                                        <div class="image">
                                            <img src="/storage/app/{{$company->info->image_menu or ''}}" alt="">
                                            <a class="delete"></a>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                      $('.delete').click(function(){
                                        $('.image_group').remove();
                                        $.get('{{route('admin.image_menu.delete')}}', function(data){});
                                      });
                                    </script>
                                    @endif
                                </div>
                            </div>
                            <div class="perent" style="margin-top: 20px;">
                                <div class="left">&nbsp</div>
                                <div class="center"><input type="submit" class="item_button" value="Сохранить"></div>
                                <div class="right"></div>
                            </div>
                        </div>

                        <script>
                            $(document).ready(function () {
                                $('input').click(function () {
                                    $('input[id="c-d02223"]:not(:checked)').parent().parent().parent().parent().removeClass("c-d02223");
                                    $('input[id="c-d02223"]:checked').parent().parent().parent().parent().addClass("c-d02223");
                                    $('input[id="c-e23c42"]:not(:checked)').parent().parent().parent().parent().removeClass("c-e23c42");
                                    $('input[id="c-e23c42"]:checked').parent().parent().parent().parent().addClass("c-e23c42");
                                    $('input[id="c-f56d53"]:not(:checked)').parent().parent().parent().parent().removeClass("c-f56d53");
                                    $('input[id="c-f56d53"]:checked').parent().parent().parent().parent().addClass("c-f56d53");
                                    $('input[id="c-f36017"]:not(:checked)').parent().parent().parent().parent().removeClass("c-f36017");
                                    $('input[id="c-f36017"]:checked').parent().parent().parent().parent().addClass("c-f36017");
                                    $('input[id="c-ffb001"]:not(:checked)').parent().parent().parent().parent().removeClass("c-ffb001");
                                    $('input[id="c-ffb001"]:checked').parent().parent().parent().parent().addClass("c-ffb001");
                                    $('input[id="c-ffc387"]:not(:checked)').parent().parent().parent().parent().removeClass("c-ffc387");
                                    $('input[id="c-ffc387"]:checked').parent().parent().parent().parent().addClass("c-ffc387");
                                    $('input[id="c-f7d700"]:not(:checked)').parent().parent().parent().parent().removeClass("c-f7d700");
                                    $('input[id="c-f7d700"]:checked').parent().parent().parent().parent().addClass("c-f7d700");
                                    $('input[id="c-b3d886"]:not(:checked)').parent().parent().parent().parent().removeClass("c-b3d886");
                                    $('input[id="c-b3d886"]:checked').parent().parent().parent().parent().addClass("c-b3d886");
                                    $('input[id="c-8bc346"]:not(:checked)').parent().parent().parent().parent().removeClass("c-8bc346");
                                    $('input[id="c-8bc346"]:checked').parent().parent().parent().parent().addClass("c-8bc346");
                                    $('input[id="c-5baa00"]:not(:checked)').parent().parent().parent().parent().removeClass("c-5baa00");
                                    $('input[id="c-5baa00"]:checked').parent().parent().parent().parent().addClass("c-5baa00");
                                    $('input[id="c-32a601"]:not(:checked)').parent().parent().parent().parent().removeClass("c-32a601");
                                    $('input[id="c-32a601"]:checked').parent().parent().parent().parent().addClass("c-32a601");
                                    $('input[id="c-39a1ae"]:not(:checked)').parent().parent().parent().parent().removeClass("c-39a1ae");
                                    $('input[id="c-39a1ae"]:checked').parent().parent().parent().parent().addClass("c-39a1ae");
                                    $('input[id="c-01b4c8"]:not(:checked)').parent().parent().parent().parent().removeClass("c-01b4c8");
                                    $('input[id="c-01b4c8"]:checked').parent().parent().parent().parent().addClass("c-01b4c8");
                                    $('input[id="c-269ffd"]:not(:checked)').parent().parent().parent().parent().removeClass("c-269ffd");
                                    $('input[id="c-269ffd"]:checked').parent().parent().parent().parent().addClass("c-269ffd");
                                    $('input[id="c-0170cc"]:not(:checked)').parent().parent().parent().parent().removeClass("c-0170cc");
                                    $('input[id="c-0170cc"]:checked').parent().parent().parent().parent().addClass("c-0170cc");
                                    $('input[id="c-bb4fbc"]:not(:checked)').parent().parent().parent().parent().removeClass("c-bb4fbc");
                                    $('input[id="c-bb4fbc"]:checked').parent().parent().parent().parent().addClass("c-bb4fbc");
                                    $('input[id="c-c0518d"]:not(:checked)').parent().parent().parent().parent().removeClass("c-c0518d");
                                    $('input[id="c-c0518d"]:checked').parent().parent().parent().parent().addClass("c-c0518d");
                                    $('input[id="c-8c4f96"]:not(:checked)').parent().parent().parent().parent().removeClass("c-8c4f96");
                                    $('input[id="c-8c4f96"]:checked').parent().parent().parent().parent().addClass("c-8c4f96");
                                    $('input[id="c-4f4ca7"]:not(:checked)').parent().parent().parent().parent().removeClass("c-4f4ca7");
                                    $('input[id="c-4f4ca7"]:checked').parent().parent().parent().parent().addClass("c-4f4ca7");
                                    $('input[id="c-35184e"]:not(:checked)').parent().parent().parent().parent().removeClass("c-35184e");
                                    $('input[id="c-35184e"]:checked').parent().parent().parent().parent().addClass("c-35184e");
                                    $('input[id="c-ffffff"]:not(:checked)').parent().parent().parent().parent().removeClass("c-ffffff");
                                    $('input[id="c-ffffff"]:checked').parent().parent().parent().parent().addClass("c-ffffff");
                                    $('input[id="c-dadada"]:not(:checked)').parent().parent().parent().parent().removeClass("c-dadada");
                                    $('input[id="c-dadada"]:checked').parent().parent().parent().parent().addClass("c-dadada");
                                    $('input[id="c-242424"]:not(:checked)').parent().parent().parent().parent().removeClass("c-242424");
                                    $('input[id="c-242424"]:checked').parent().parent().parent().parent().addClass("c-242424");
                                    $('input[id="c-000000"]:not(:checked)').parent().parent().parent().parent().removeClass("c-000000");
                                    $('input[id="c-000000"]:checked').parent().parent().parent().parent().addClass("c-000000");
                                    $('#color_write').click(function(){
                                      $('input[type="radio"]').prop('checked', false);
                                      $('.s_action').removeClass('back');
                                    });
                                });
                            });
                        </script>
                        <script>
                            $('.color_edit .s_action').on("click", function(){
                                $('.s_action').removeClass('back');
                                $(this).addClass('back');
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
