@include('admin.index')
    <div class="row">
        <div class="col-lg-4">
            <h2>Управление компанией</h2>
        </div>
        <div class="col-lg-8">
            <ol class="breadcrumb">
                <li><a href="/">Главная</a></li>
                <li class="active">Технические настройки и оформление сайта</li>
            </ol>
        </div>
    </div>
    <hr>

    <div class="c_p_company">
        <div class="wide">
            <div class="box">
                <div class="help">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="narrow">
            <div class="box">
                <div class="narrow_title">
                    <h2>Технические настройки и оформление сайта</h2>
                </div>
                <style>
                    .s_site{width: 100%;display: table}
                    .s_site .i{width: 33.333%;padding: 1%;float: left}
                    .s_site .i .name{font-size: 15px;font-weight: 600;margin-bottom: 10px}
                    .s_site .i ul{padding-left: 20px;}
                    .s_site .i a{color: #0095eb}
                </style>
                <div class="s_site">
                    <div class="i">
                        <div class="info">
                            <div class="name">Техническая настройка сайта</div>
                            <ul>
                                <li><a href="/admin/settings/site/technical/verification">Верификация сайта</a></li>
                                <li><a href="/admin/settings/site/technical/metrica">Настройка Яндекс.Метрика</a></li>
                                <li><a href="/admin/settings/site/technical/analytics">Настройка Google Analytics</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="i">
                        <div class="info">
                            <div class="name">Настройка элементов на сайте</div>
                            <ul>
                                <li><a href="">Настройка верхнего меню</a></li>
                                <li><a href="">Настройка категорий</a></li>
                                <li><a href="">Настройка товаров</a></li>
                                <li><a href="">Настройка услуг</a></li>
                                <li><a href="">Настройка портфолио</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="i">
                        <div class="info">
                            <div class="name">Оформление</div>
                            <ul>
                                <li><a href="/admin/settings/site/design/color">Цветовые схемы для сайта</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

</html>
