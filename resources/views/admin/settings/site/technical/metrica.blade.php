@include('admin.index')
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление компанией</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Технические настройки, оформление сайта</li>
                </ol>
            </div>
        </div>
        <hr>
        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="help">
                        <div class="wide_title">
                            <h2>Правая штука</h2>
                        </div>
                        <div class="undefined_item">
                          <div class="item"><div class="name"><a href="/admin/settings/site/technical/verification/">Верификация сайта</a></div></div>
                          <div class="item"><div class="name"><a href="/admin/settings/site/technical/metrica/">Настройка Яндекс.Метрика</a></div></div>
                          <div class="item"><div class="name"><a href="/admin/settings/site/technical/analytics/">Настройка Google Analytics</a></div></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="narrow_title">
                        <h2>Яндекс.Метрика</h2>
                    </div>
                    <form class="" action="{{route('admin.settings.update', 'metrica')}}" method="post">
                      {{csrf_field()}}
                    <div class="cp_edit">
                        <div class="perent">
                            <div class="left"><label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Код счетчика:</label></div>
                            <div class="center"><input type="text" name="metrica" class="" placeholder="" value="{{$company->info->metrika or ''}}" required=""></div>
                            <div class="right"><div class="help"></div></div>
                        </div>
                        <div class="perent" style="margin-top: 20px;">
                            <div class="left">&nbsp;</div>
                            <div class="center"><input type="submit" class="item_button" value="Сохранить"></div>
                            <div class="right"></div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
