@include('admin.index')
<script type="text/javascript">
  $('#admin-cont').remove();
</script>

    <div class="admin-content">
        <div class="row">
            <div class="col-xs-4">
                <h2>Управление сайтами</h2>
            </div>
            <div class="col-xs-8">
                <ol class="breadcrumb">
                    <li><a href="">Главная</a></li>
                    <li class="active">Категории</li>
                </ol>
            </div>
        </div>
        <hr>

        <style>
            .orders .title{padding-bottom:20px;border-bottom:1px solid #ddd;margin-bottom:20px;display:table;width:100%}
            .orders .title .left{width:50%;float:left}
            .orders .title h2{font-size:16px;font-weight:600;margin:0}
            .orders .title h2 a{color: #666}
            .orders .title h2 span{}
            .orders .title .right{width:50%;float:left;font-size:14px;line-height:14px;color:#666;text-align:right}
            .orders .button-edit{cursor:pointer;z-index:3;width:102px;height:30px;padding:7px 14px;margin-left:25px;font-size:14px;background:#0095eb;color:#fff;border-radius:5px;text-align:center;vertical-align:-webkit-baseline-middle}
            .orders .title .right .button-edit a{color:#fff!important}
            .orders table{width:100%}
            .orders table input{width:100%}
            .orders table a{color:#0095eb}
            .orders table thead{border-bottom:1px solid #ddd}
            .orders table thead th{font-weight:600}
            .orders table tbody tr{border-bottom:1px solid #ddd}
            .orders table tbody tr:hover{background:rgba(233,235,238,0.5)}
            .orders .order-number{min-width:70px;width:5%;padding:10px;border-right:1px solid #ddd;vertical-align:top}
            .orders .order-goods{width:25%;min-width:200px;padding:10px;border-right:1px solid #ddd;vertical-align:top}
            .orders .order-goods .goods{display:table;margin-top:30px}
            .orders .order-goods .open{text-align:center;position:relative;margin-top:20px}
            .orders .order-goods .open:after{font-family:FontAwesome;color:#0095eb;content:"\f103";font-size:13px;display:block;padding-left:5px;font-weight:400}
            .orders .order-goods .left{width:20%;float:left}
            .orders .order-goods .right{width:80%;float:left;padding-left:15px}
            .orders .order-goods .name{margin-bottom:10px;font-weight:600;color:#333;overflow:hidden;max-height:40px}
            .orders .order-goods .price{color:#666}
            .orders .order-goods .price span{font-weight:600;color:#333}
            .orders .order-goods .price span:after{font-family:FontAwesome;color:#333;content:"\f158";font-size:13px;padding-left:5px;font-weight:400}
            .orders .order-user{width:25%;padding:10px;border-right:1px solid #ddd;vertical-align:top}
            .orders .order-user .name{padding-right:25px;margin-bottom:10px}
            .orders .order-user .address{margin-top:10px}
            .orders .order-full-price{vertical-align:top;width:10%;padding:10px;border-right:1px solid #ddd}
            .orders .order-full-price .full-price .sum{font-weight:600;margin-bottom:10px}
            .orders .order-full-price .full-price .sum:after{font-family:FontAwesome;color:#333;content:"\f158";font-size:13px;padding-left:5px;font-weight:400}
            .orders .order-full-price .status-pay{font-size:16px;font-weight:400;text-align:right;float:right}
            .clock:after{font-family:FontAwesome;content:"\f017";color:#e46a76}
            .percent:after{font-family:FontAwesome;content:"\f0d6";color:#f0ad4e}
            .check:after{font-family:FontAwesome;content:"\f00c";color:#00c292}
            .orders .order-status{width:10%;padding:10px;border-right:1px solid #ddd;vertical-align:top}
            .sort_input{border-radius:25px;padding:3px 10px;border:1px solid #ddd;background:#fff;color:#333;font-weight:400}
            .sort_input:focus{outline:0;outline-offset:0}
            .sort_select{border-radius:25px;padding:3px 10px;border:none;background:#e9ebee;color:#333;font-weight:400}
            .sort_select:focus{outline:0;outline-offset:0}
            .orders .select_status{display:inline;padding:3px 10px;border-radius:25px;border:none;font-size:12px}
            .orders .select_status option{background:#fff;color:#666}
            .orders .order-status .button-status{display:inline;padding:3px 10px;border-radius:25px;font-size:12px}
            .orders .order-status .danger{background:#e46a76;color:#fff}
            .orders .order-status .warning{background:#f0ad4e;color:#fff}
            .orders .order-status .success{background:#00c292;color:#fff}
            .orders .order-status .secondary{background:#aaa;color:#fff}
            .orders .order-delivery{width:10%;padding:10px;border-right:1px solid #ddd;vertical-align:top}
            .orders .order-action{vertical-align:top;padding:10px;width:5%;min-width:70px;position:relative}
            .orders .action{text-align:right}
            .orders .action .edit:before{font-family:FontAwesome;color:#666;content:"\f141"}
        </style>
        <?php //dd($offers); ?>
        <div class="c_p_company">
            <div class="narrow" style="width: 100%;padding-right: 0">
                <div class="box">
                    <div class="orders">
                        <div class="narrow_title">
                            <div class="left">
                                <h2>Заказы</h2>
                            </div>
                            <div class="right">
                                <span class="button-edit"><a href="new.php">Добавить заказ</a></span>
                            </div>
                        </div>
                        <?php //dd($offers); ?>
                        <div class="block">

                            <table>
                                <thead>
                                    <tr>
                                        <th class="order-number">
                                            № зак.
                                        </th>
                                        <th class="order-goods">
                                            Товары
                                        </th>
                                        <th class="order-user">
                                            Клиент
                                        </th>
                                        <th class="order-full-price">
                                            Итого
                                        </th>
                                        <th class="order-status">
                                            Статус
                                        </th>
                                        <th class="order-delivery">
                                            Доставка
                                        </th>
                                        <th class="order-action">

                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="order-number">
                                            <div class="number">
                                                <input type="text" class="sort_input">
                                            </div>
                                        </th>
                                        <th class="order-goods">
                                            <div class="goods">

                                            </div>
                                        </th>
                                        <th class="order-user">
                                            <div class="user">
                                                <input type="text" class="sort_input">
                                            </div>
                                        </th>
                                        <th class="order-full-price"></th>
                                        <th class="order-status">
                                            <div class="status">
                                                <select name="select" class="sort_select">
                                                    <option value="" selected="">Все</option><!--Supplement an id here instead of using 'name'-->
                                                    <option value="">Новый заказ</option>
                                                    <option value="">В обработке</option>
                                                    <option value="">В работе</option>
                                                    <option value="">Завершен</option>
                                                    <option value="">Треш</option>
                                                </select>
                                            </div>
                                        </th>
                                        <th class="order-delivery"></th>
                                        <th class="order-action">
                                            <div class="action">

                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @forelse($offers as $offer)
                                    <?php $sum = $offer->product->coast * $offer->counts; ?>
                                    <?php //dd($offer); ?>
                                    <tr>
                                        <td class="order-number">
                                            <div class="number">

                                                {{$offer->id}}
                                            </div>
                                        </td>
                                        <td class="order-goods">
                                            <div class="goods" style="margin-top: 0">
                                                <div class="left">
                                                    <div class="image">
                                                        <img src="{{asset('storage/app')}}/{{$offer->product->images->images or ''}}" alt="">
                                                    </div>
                                                </div>
                                                <div class="right">
                                                    <div class="name">
                                                        {{$offer->product->title or ''}}
                                                    </div>
                                                    <div class="price">Цена: <span>{{$offer->product->coast or ''}}</span> x

                                                      <i id="count{{$offer->id}}">{{$offer->counts}}</i>
                                                        </div>
                                                        <span id="minus{{$offer->id}}">-</span>
                                                        <span id="plus{{$offer->id}}">+</span>


                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                            ajaxForm();
                                            function ajaxForm(){
                                              $('#plus{{$offer->id}}').click( function () {
                                                var data = new FormData($("#form")[0]);
                                                var i = +$('#count{{$offer->id}}').text() + 1;
                                                $('#count{{$offer->id}}').text(i);
                                                $('#countgoods{{$offer->id}}').attr('value', i);
                                                  $.ajax({
                                                      type: 'GET',
                                                      url: "{{route('admin.offers.counts')}}?id={{$offer->id}}&&count="+i,
                                                      data: data,
                                                      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                                      contentType: false,
                                                      processData : false,
                                                      success: function(data) {
                                                      }
                                                  });
                                                  return false;
                                              });
                                              $('#minus{{$offer->id}}').click( function () {
                                                var data = new FormData($("#form")[0]);
                                                var i = +$('#count{{$offer->id}}').text() -1;
                                                $('#count{{$offer->id}}').text(i);
                                                $('#countgoods').attr('value', i);
                                                  $.ajax({
                                                      type: 'GET',
                                                      url: "{{route('admin.offers.counts')}}?id={{$offer->id}}&&count="+i,
                                                      data: data,
                                                      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                                      contentType: false,
                                                      processData : false,
                                                      success: function(data) {
                                                      }
                                                  });
                                                  return false;
                                              });
                                            }

                                            // $('#plus').click( function () {
                                            //   var i = +$('#count').text() + 1;
                                            //   $('#count').text(i);
                                            //   $('#countgoods').attr('value', i);
                                            // });
                                            // $('#minus').click( function () {
                                            //   var i = +$('#count').text() -1;
                                            //   $('#count').text(i);
                                            //   $('#countgoods').attr('value', i);
                                            // });
                                            </script>
                                            @if(count($offer->offers) > 0)
                                            <div class="open">
                                                <a href="" class="spoiler-trigger" data-toggle="collapse">Показать ещё {{count($offer->offers)}} товара</a>
                                            </div>
                                            @endif
                                            <div class="info collapse">
                                              @forelse($offer->offers as $suboffer)
                                              <?php $sum = $sum + ($suboffer->product->coast * $suboffer->counts); ?>

                                                <div class="goods">
                                                    <div class="left">
                                                        <div class="image">
                                                            <img src="{{asset('storage/app')}}/{{$suboffer->product->images->images or ''}}" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="right">
                                                        <div class="name">
                                                            {{$suboffer->product->title or ''}}
                                                        </div>
                                                        <div class="price">Цена: <span>{{$suboffer->product->coast or ''}}</span> x
                                                          <i id="count{{$suboffer->id}}">{{$suboffer->counts or ''}}</i>
                                                          </div>
                                                          <span id="subminus{{$suboffer->id}}">-</span>
                                                          <span id="subplus{{$suboffer->id}}">+</span>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                ajaxForm();
                                                function ajaxForm(){
                                                  $('#subplus{{$suboffer->id}}').click( function () {
                                                    var data = new FormData($("#form")[0]);
                                                    var i = +$('#count{{$suboffer->id}}').text() + 1;
                                                    $('#count{{$suboffer->id}}').text(i);
                                                    $('#countgoods{{$suboffer->id}}').attr('value', i);
                                                      $.ajax({
                                                          type: 'GET',
                                                          url: "{{route('admin.offers.counts')}}?id={{$suboffer->id}}&&count="+i,
                                                          data: data,
                                                          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                                          contentType: false,
                                                          processData : false,
                                                          success: function(data) {
                                                          }
                                                      });
                                                      return false;
                                                  });
                                                  $('#subminus{{$suboffer->id}}').click( function () {
                                                    var data = new FormData($("#form")[0]);
                                                    var i = +$('#count{{$offer->id}}').text() -1;
                                                    $('#count{{$suboffer->id}}').text(i);
                                                    $('#countgoods').attr('value', i);
                                                      $.ajax({
                                                          type: 'GET',
                                                          url: "{{route('admin.offers.counts')}}?id={{$suboffer->id}}&&count="+i,
                                                          data: data,
                                                          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                                          contentType: false,
                                                          processData : false,
                                                          success: function(data) {
                                                          }
                                                      });
                                                      return false;
                                                  });
                                                }
                                                </script>
                                                @empty
                                                @endforelse
                                                {{$product->links()}}
                                            </div>
                                        </td>
                                        <td class="order-user">
                                            <div class="user">
                                                <div class="name">{{$offer->users->mainAddress->name or ''}}</div>
                                                <div class="from"><a href="">Рукодельница</a></div>
                                                <div class="phone"><a href="">{{$offer->users->mainAddress->tel or ''}}</a></div>
                                                <div class="email"><a href="">{{$offer->users->email or ''}}</a></div>
                                                <div class="address">
                                                    <div class="country">Россия, <span>{{$offer->users->mainAddress->area or ''}} {{$offer->users->mainAddress->city or ''}}</span></div>
                                                    <div class="street">{{$offer->users->mainAddress->address or ''}}</div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="order-full-price">
                                            <div class="full-price">
                                                <div class="sum">{{$sum or ''}}<span class="status-pay clock"></span></div>
                                                <div class="scoring">Счёт: <a href=""><span>выберите</span></a></div>
                                            </div>
                                        </td>
                                        <td class="order-status">
                                            <select name="hero" id="status{{$offer->id}}" class="select_status @if($offer->status==1)danger @elseif($offer->status==2)warning @elseif($offer->status==3)success @elseif($offer->status==4)secondary @endif">
                                                <option value="1" @if($offer->status==1) selected @endif>Новый заказ</option>
                                                <option value="2" @if($offer->status==2) selected @endif>В обработке</option>
                                                <option value="3" @if($offer->status==3) selected @endif>В работе</option>
                                                <option value="4" @if($offer->status==4) selected @endif>Завершенный</option>
                                                <option value="5" @if($offer->status==5) selected @endif>Треш</option>
                                            </select>
                                        </td>
                                        <td class="order-delivery">
                                            <div class="delivery">
                                                Самовывоз
                                            </div>
                                        </td>
                                       <td class="order-action">
                                           <div class="action">
                                               <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                               <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                                   <li><a href="">Скрыть</a></li>
                                                   <li><a href="/admin/goods/edit.php">Редактировать</a></li>
                                               </ul>
                                           </div>
                                       </td>
                                    </tr>
                                    <script type="text/javascript">

                                    $(document).ready(function() {

                                        ajaxForm();
                                        function ajaxForm(){
                                          $('#status{{$offer->id}}').on('change', function() {
                                              console.log('click');
                                              if($(this).val() == '1') {
                                                $('#status{{$offer->id}}').attr('class', 'select_status danger');
                                            var data = new FormData($("#form")[0]);
                                              $.ajax({
                                                  type: 'GET',
                                                  url: "{{route('admin.offers.changestatus')}}?id={{$offer->id}}&&status=1",
                                                  data: data,
                                                  headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                                  contentType: false,
                                                  processData : false,
                                                  success: function(data) {

                                                  }
                                              });
                                              return false;
                                            }
                                            if($(this).val() == '2') {
                                              $('#status{{$offer->id}}').attr('class', 'select_status warning');
                                          var data = new FormData($("#form")[0]);
                                            $.ajax({
                                                type: 'GET',
                                                url: "{{route('admin.offers.changestatus')}}?id={{$offer->id}}&&status=2",
                                                data: data,
                                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                                contentType: false,
                                                processData : false,
                                                success: function(data) {

                                                }
                                            });
                                            return false;
                                          }
                                          if($(this).val() == '3') {
                                            $('#status{{$offer->id}}').attr('class', 'select_status success');
                                        var data = new FormData($("#form")[0]);
                                          $.ajax({
                                              type: 'GET',
                                              url: "{{route('admin.offers.changestatus')}}?id={{$offer->id}}&&status=3",
                                              data: data,
                                              headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                              contentType: false,
                                              processData : false,
                                              success: function(data) {

                                              }
                                          });
                                          return false;
                                        }
                                        if($(this).val() == '4') {
                                          $('#status{{$offer->id}}').attr('class', 'select_status secondary');
                                      var data = new FormData($("#form")[0]);
                                        $.ajax({
                                            type: 'GET',
                                            url: "{{route('admin.offers.changestatus')}}?id={{$offer->id}}&&status=4",
                                            data: data,
                                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                            contentType: false,
                                            processData : false,
                                            success: function(data) {

                                            }
                                        });
                                        return false;
                                      }
                                      if($(this).val() == '5') {
                                        $('#status{{$offer->id}}').attr('class', 'select_status');
                                    var data = new FormData($("#form")[0]);
                                      $.ajax({
                                          type: 'GET',
                                          url: "{{route('admin.offers.changestatus')}}?id={{$offer->id}}&&status=5",
                                          data: data,
                                          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                          contentType: false,
                                          processData : false,
                                          success: function(data) {

                                          }
                                      });
                                      return false;
                                    }
                                          });
                                        }
                                    });
                                    </script>
                                    @empty
                                    @endforelse


                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>


    <script>
        $(".spoiler-trigger").click(function() {
            $(this).parent().next().collapse('toggle');
        });
    </script>



<footer class="footer">
<a href="http://lipatkin.com/">Lipatkin.com</a>
</footer>


</body></html>
