@include('admin.index')
<div class="row">
    <div class="col-lg-4">
        <h2>Описание на страницах сайта</h2>
    </div>
    <div class="col-lg-8">
        <ol class="breadcrumb">
            <li><a href="{{route('admin.index')}}">Главная</a></li>
            <li><a href="{{route('admin.company.index')}}">Компания</a></li>
            <li class="active">Описание на страницах сайта</li>
        </ol>
    </div>
</div>
<hr>
<style>
    .settings_description .title{padding:10px;display:table;width:100%}
    .settings_description .title .left{width:50%;float:left}
    .settings_description .title h2{font-size:16px;line-height:16px;font-weight:600;margin:0}
    .settings_description .title .right{width:50%;float:left;font-size:14px;line-height:14px;color:#666;text-align:right}
    .settings_description .item{padding:10px;position:relative}
    .settings_description .item .i_bottom{width:100%;display:table;margin-top:40px}
    .settings_description .item .i_bottom .image{width:25%;height:145px;display:table;float:left;padding:0 10px 10px 0;position:relative}
    .settings_description .item .i_bottom img{width:100%;height:145px;object-fit:cover}
    .settings_description .i_head{width:100%;margin-bottom:20px;display:table;padding-bottom:20px;border-bottom:1px solid #ddd}
    .settings_description .i_head .name{width:50%;float:left;font-weight:600;font-size:16px}
    .settings_description .i_head .action{width:50%;float:left;text-align:right}
    .settings_description .action .edit:before{font-family:FontAwesome;color:#666;content:"\f141"}
    .settings_description .i_head .name .signature{font-weight:400;color:#999;font-size:12px}
    .settings_description .item .images{display:table;width:100%;margin-top:20px}
    .settings_description .item .images .image{width:105px;height:70px;float:left;margin-right:10px;margin-bottom:10px}
    .settings_description .item .images .image img{width:100%;height:100%;object-fit:cover}
</style>
<div class="c_p_company">
    @include('admin.company.menu')
    <div class="narrow">
        <div class="settings_description">
            <div class="box">
                <div class="title">
                    <div class="left">
                        <h2>Описание на страницах сайта</h2>
                    </div>
                    <div class="right"></div>
                </div>
            </div>
           {{-- Описание на странице: @if($t->texts->type == 1)«Главная страница сайта»
            @elseif($t->texts->type == 2)«Главная»
            @elseif($t->texts->type == 3)«Главная»
            @elseif($t->texts->type == 4)«Главная»
            @elseif($t->texts->type == 5)«Главная»
            @elseif($t->texts->type == 6)«Главная»
            @elseif($t->texts->type == 7)«Главная»
            @elseif($t->texts->type == 8)«Главная»
            @elseif($t->texts->type == 9)«Главная»
            @elseif($t->texts->type == 10)«Главная»
            @elseif($t->texts->type == 11)«Главная»
            @elseif($t->texts->type == 12)«Главная»
            @elseif($t->texts->type == 13)«Главная»
            @elseif($t->texts->type == 14)«Главная»
            @elseif($t->texts->type == 15)«Главная»
            @elseif($t->texts->type == 16)«Главная»
            @elseif($t->texts->type == 17)«Главная»
            @elseif($t->texts->type == 18)«Главная»
            @elseif($t->texts->type == 19)«Главная»
            @elseif($t->texts->type == 20)«Главная»

            @endif--}}
            @forelse($templates as $t)
            @if($t->section !== null && $t->section->published == 1 || $t->section == null)
                <div class="box">
                    <div class="item">
                        <div class="i_head">
                            <div class="name">
                                {{$t->section->title or 'Название компании'}} - {{$t->section->id or ''}}
                                <div class="signature">
                                    Описание на странице
                                    @if($t->texts->type == 1) Описание на главной странице сайта @else
                                    Описание на странице "{{$t->section->title or $t->texts->slug}}" - {{$t->texts->id}}
                                    @endif
                                </div>
                            </div>
                            <div class="action">
                                <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                    <li><a href="{{route('admin.company.upload')}}?type={{$t->texts->type}}">Редактировать</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="info">
                            <p>{!! $t->texts->text or 'Без описания' !!}</p>
                        </div>
                        @if($t->texts !== null && $t->texts->images !== null)
                        <div class="images">
                            @forelse($t->texts->images as $img)
                            <div class="image">
                                <img src="{{asset('storage/app')}}/{{$img->images}}" alt="">
                            </div>
                            @empty
                            @endforelse
                        </div>
                        @endif
                    </div>
                </div>
                @endif
            @empty
            @endforelse
        </div>
    </div>
</div>
</div>
</div>
