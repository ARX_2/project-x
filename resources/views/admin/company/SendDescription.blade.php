@include('admin.index')

        <div class="row">
            <div class="col-lg-4">
                <h2>Управление сайтами</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Категории</li>
                </ol>
            </div>
        </div>
        <hr>
        <?php //dd($text); ?>
        <div class="c_p_company">
            @include('admin.company.menu')
            @if($text->texts == null)
            <form class="" action="{{route('admin.company.about')}}" method="post" enctype="multipart/form-data">
              @else
            <form class="" action="{{route('admin.company.updateDescription')}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="id" value="{{$text->texts->id}}">
              @endif
              <input type="hidden" name="type" value="{{$type}}">
              {{ csrf_field() }}
            <div class="narrow">
                <div class="box">
                    <div class="cp_edit">
                        <div class="narrow_title">
                            <h2>Редактирование описание: Название описания</h2>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Заголовок на странице раздела:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="title" class="" placeholder="Введите название раздела" value="{{$text->texts->title or ''}}">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <?php //dd($text);?>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="seo_name" style="line-height: 14px;padding-top: 4px;">Изменить название в меню</label>
                            </div>
                            <div class="center">
                                <label class="switch">
                                    <input type="checkbox" id="namenav" name="checktitle" value="1" @if($text->section !== null &&  $text->section->checktitle == 1) checked @endif>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="seo_group" id="switch_namenav" style="display:none">
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name" style="">Название раздела в меню</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="name" class="" placeholder="Название на странице категории" value="{{$text->section->title or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="seo_name">SEO</label>
                            </div>
                            <div class="center">
                                <label class="switch">
                                    <input type="checkbox" id="watch" name="checkseo" value="1" @if($text->texts !== null) @if($text->texts->checkseo == 1)checked @endif @endif>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="seo_group" id="switch_seo" style="display:none">
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name" style="padding-top: 4px">Название на странице товара или услуги:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seoName" class="" placeholder="Название на странице категории" value="">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Title:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seoTitle" class="" placeholder="" value="{{$text->texts->seoTitle or ''}}" >
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Description:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seoDescription" class="" placeholder="" value="{{$text->texts->seoDescription or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Keywords:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seoKeywords" class="" placeholder="" value="{{$text->texts->keywords or ''}}" >
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left"><label for="" class="item_name" style="line-height: 14px;">Оффер:</label></div>
                            <div class="center"><textarea  name="offer" id="offer" class="" placeholder="Оффер раздела" >{{$text->texts->offer or ''}}</textarea></div>
                        </div>
                        <script>
                            var myOffer;
                            ClassicEditor
                                .create( document.querySelector( '#offer' ) )
                                .then( offer => {myOffer = offer;})
                            .catch( error => {});
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Описание:</label>
                            </div>
                            <div class="center">
                                <textarea rows="10" name="text" id="text">{{$text->texts->text or ''}}</textarea>
                            </div>
                        </div>
                        <script>
                            var myEditor;
                            ClassicEditor
                            .create(document.querySelector( '#text' ))
                            .then( editor => {myEditor = editor;})
                            .catch( err => {});
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Фотографии:</label>
                            </div>
                            <div class="center">
                                <div class="file-upload">
                                    <label>
                                        <input type="file" name="image[]" multiple>
                                        <span>Выбрать файл</span>
                                    </label>
                                </div>
                                <input type="text" id="filename" class="filename" disabled="" style="border: none;font-size: 12px;">
                                @if($text->texts !== null)
                                @forelse($text->texts->images as $img)
                                <div class="image_group" style="float:left !important; width:auto;" id="image{{$img->id}}">

                                    <div class="image" >
                                        <img src="{{asset('storage/app')}}/{{$img->images or ''}}" alt="">
                                        <a  class="delete" id="send{{$img->id}}"></a>
                                    </div>
                                    <script>

                                    ajaxForm();
                                    function ajaxForm(){
                                      $('#send{{$img->id}}').click(function(){
                                        var data = new FormData($("#form{{$img->id}}")[0]);
                                          $.ajax({
                                              type: 'GET',
                                              url: "{{route('admin.image.company.destroy')}}?id={{$img->id}}",
                                              data: data,
                                              headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                              contentType: false,
                                              processData : false,
                                              success: function(data) {
                                                  $('#image{{$img->id}}').html('');
                                              }
                                          });
                                          return false;
                                      });
                                    }

                            </script>

                                </div>
                                @empty
                                @endforelse
                                @endif
                                <div style="clear:both;"></div>
                            </div>
                        </div>
                        <!--<div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Видео с YouTube:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="youtube" class="" placeholder="Например: http://www.youtube.com/watch?v=9X-8JiOhzX4" value="" >
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>-->
                        <div class="perent" style="margin-top: 40px;">
                            <div class="left">
                                &nbsp;
                            </div>

                            <!--

                            Кнопка сохранить и обновить меняются!

                            На странице добавления товаров - добавить
                            На странице редактировать товара - Обновить

                            -->

                            <div class="center">
                                <input type="submit" class="item_button" value="Добавить">
                            </div>
                            <div class="right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>

        <script>
            $(document).ready( function() {
                $(".file-upload input[type=file]").change(function(){
                    var filename = $(this).val().replace(/.*\\/, "");
                    $("#filename").val(filename);
                });
            });
        </script>
        <script>
            $(document).ready(function() {
                $('input[type="checkbox"]').click(function() {
                    if($(this).attr('id') == 'namenav') {
                        if (this.checked) {
                            $('#switch_namenav').show();
                        } else {
                            $('#switch_namenav').hide();
                        }
                    }
                });
            });
        </script>
        <script>
            $(document).ready(function() {
                $('input[type="checkbox"]').click(function() {
                    if($(this).attr('id') == 'watch') {
                        if (this.checked) {
                            $('#switch_seo').show();
                        } else {
                            $('#switch_seo').hide();
                        }
                    }
                });
            });
        </script>
        @if($text->texts !== null)
        @if($text->texts->checkseo == 1)
        <script>
            $(document).ready(function() {
                            $('#switch_seo').show();
            });
        </script>
        @endif
        @endif
        @if($text->section !== null &&$text->section->checktitle == 1)
        <script type="text/javascript">
        $(document).ready(function() {
          $('#switch_namenav').show();

          });
        </script>
        @endif
    </div>
