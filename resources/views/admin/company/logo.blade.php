<?php //dd($company); ?>
@include('admin.index')

        <div class="row">
            <div class="col-lg-4">
                <h2>Управление логотипом сайта</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li><a href="{{route('admin.company.index')}}">Компания</a></li>
                    <li class="active">Логотип</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
          @include('admin.company.menu')
            <div class="narrow">

                <style>
                    .setting .file-upload .upload-logo{
                        border: none!important;
                        line-height: 22px;
                        padding-top: 7px;
                    }
                    .setting .file-upload .image_group{
                        padding: 5px;
                    }
                    .setting .file-upload .image_group .image{
                        height: 80px;
                    }
                    .setting .file-upload .image_group img{
                        height: 80px;
                        background: #333;
                    }
                </style>
                <form class="" action="{{route('admin.company.sendLogo')}}" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                <div class="box">
                    <div class="setting">
                        <h2>Логотипы</h2>
                        <div class="group-edit-row">
                            <div class="edit-label">Логотип:</div>
                            <div class="field">
                                <div class="file-upload">
                                    <label>
                                        <input type="file" class="upload-logo" name="logo1">
                                    </label>
                                    <div class="image_group">
                                        <div class="image">
                                            <img src="{{asset('storage/app')}}/{{$company->info->logo1 or '/public/none-img.png'}}" alt="">
                                            <a href="" class="delete"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="group-edit-row">
                            <div class="edit-label">Логотип (белый):</div>
                            <div class="field">
                                <div class="file-upload">
                                    <label>
                                        <input type="file" class="upload-logo" name="logo2">
                                    </label>
                                    <div class="image_group">
                                        <div class="image">
                                            <img src="{{asset('storage/app')}}/{{$company->info->logo2 or '/public/none-img.png'}}" alt="">
                                            <a href="" class="delete"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="group-edit-row">
                            <div class="edit-label">Favicon:</div>
                            <div class="field">
                                <div class="file-upload">
                                    <label>
                                        <input type="file" class="upload-logo" name="favicon">
                                    </label>
                                    <div class="image_group">
                                        <div class="image">
                                            <img src="{{asset('storage/app')}}/{{$company->info->favicon or '/public/none-img.png'}}" alt="">
                                            <a href="" class="delete"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="group-edit-row">
                            <div class="edit-label">&nbsp;</div>
                            <div class="field">
                                <input class="button-save" type="submit" value="Сохранить">
                            </div>
                        </div>
                    </div>
                </div>
              </form>
            </div>
        </div>


    </div>
