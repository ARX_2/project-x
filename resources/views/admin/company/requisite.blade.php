@include('admin.index')

<div class="row">
    <div class="col-lg-4">
        <h2>Управление реквизитами компании</h2>
    </div>
    <div class="col-lg-8">
        <ol class="breadcrumb">
            <li><a href="{{route('admin.index')}}">Главная</a></li>
            <li><a href="{{route('admin.company.index')}}">Компания</a></li>
            <li class="active">Реквизиты</li>
        </ol>
    </div>
</div>
<hr>

<div class="c_p_company">
    @include('admin.company.menu')

    <div class="narrow">
      <form class="" action="{{route('admin.company.sendRequisite')}}" method="post">
        {{ csrf_field() }}

        <div class="box">
            <div class="setting">
                <h2>Реквизиты</h2>
                <div class="group-edit-row">
                    <div class="edit-label">Полное наименование:</div>
                    <div class="field">
                        <input type="text" name="full_name" class="" value="{{$company->info->full_name or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">Сокращенное наименование:</div>
                    <div class="field">
                        <input type="text" class="" name="short_name" value="{{$company->info->short_name or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">Юридический адрес:</div>
                    <div class="field">
                        <input type="text" class="" name="legal_address" value="{{$company->info->legal_address or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">Фактический адрес:</div>
                    <div class="field">
                        <input type="text" class="" name="actual_address" value="{{$company->info->actual_address or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">Генеральный директор:</div>
                    <div class="field">
                        <input type="text" class="" name="general_director" value="{{$company->info->general_director or ''}}" maxlength="250">
                    </div>
                </div>

                <div class="group-edit-row">
                    <div class="edit-label">ИНН:</div>
                    <div class="field">
                        <input type="text" name="INN" class="" value="{{$company->info->INN or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">КПП:</div>
                    <div class="field">
                        <input type="text" class="" name="KPP" value="{{$company->info->KPP or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">ОГРН:</div>
                    <div class="field">
                        <input type="text" class="" name="OGRN" value="{{$company->info->OGRN or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">ОКПО:</div>
                    <div class="field">
                        <input type="text" class="" name="OKPO" value="{{$company->info->OKPO or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">ОКВЭД:</div>
                    <div class="field">
                        <input type="text" class="" name="OKBED" value="{{$company->info->OKBED or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">ОКАТО:</div>
                    <div class="field">
                        <input type="text" class="" name="OKATO" value="{{$company->info->OKATO or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">Банк:</div>
                    <div class="field">
                        <input type="text" class="" name="Bank" value="{{$company->info->Bank or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">Расч. счет:</div>
                    <div class="field">
                        <input type="text" class="" name="Acount_chek" value="{{$company->info->Acount_chek or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">Кор. счет:</div>
                    <div class="field">
                        <input type="text" class="" name="Corp_check" value="{{$company->info->Corp_check or ''}}" maxlength="250">
                    </div>
                </div>
                <div class="group-edit-row">
                    <div class="edit-label">БИК:</div>
                    <div class="field">
                        <input type="text" class="" name="BIK" value="{{$company->info->BIK or ''}}" maxlength="250">
                    </div>
                </div>
                <hr>
                <div class="group-edit-row">
                    <div class="edit-label">&nbsp;</div>
                    <div class="field">
                        <input class="button-save" type="submit" value="Сохранить">
                    </div>
                </div>
            </div>
        </div>
      </form>
    </div>
</div>


</div>
