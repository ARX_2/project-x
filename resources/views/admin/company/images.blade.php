<?php //dd($company); ?>
@include('admin.admin.include.mainHeader')
<div class="admin">

    @include('admin.admin.include.header')
    @include('admin.admin.include.lmenu')
    <div class="admin-content">
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление сайтами</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="">Главная</a></li>
                    <li class="active">Категории</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="rmenu">
                    <ul>
                        <li><a href="{{route('admin.company.index')}}">Информация</a></li>
                        <li><a href="{{route('admin.company.requisite')}}">Реквизиты</a></li>
                        <li><a href="{{route('admin.company.logo')}}">Логотип</a></li>
                        <li><a href="/admin/settings/verification.php">Верификация сайта</a></li>
                        <li><a href="{{route('admin.company.description')}}">Описание на страницах сайта</a></li>
                        <li><a href="{{route('admin.company.images')}}">Картинки о компинии</a></li>
                    </ul>
                </div>
            </div>
            <div class="narrow">

                <style>
                    .setting .file-upload .upload-logo{
                        border: none!important;
                        line-height: 22px;
                        padding-top: 7px;
                    }
                    .setting .file-upload .image_group{
                        padding: 5px;
                    }
                    .setting .file-upload .image_group .image{
                        height: 80px;
                    }
                    .setting .file-upload .image_group img{
                        height: 80px;
                        background: #333;
                    }
                </style>
                <form class="" action="{{route('admin.company.sendLogo')}}" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                <div class="box">
                  <div class="file-upload">
                      <label>
                          <input type="file" name="file">
                          <span>Выбрать файл</span>
                      </label>
                  </div>
                  <input type="text" id="filename" class="filename" disabled style="border: none;font-size: 12px;">
                  @forelse($company->about->images as $img)
                  <div class="col-sm-3">
                    <div class="image_group">
                       <div class="image">
                           <img src="{{asset('storage/app')}}/{{$img->images or ''}}" width="150" alt="">
                           <a href="{{route('admin.del.cedit')}}?siteID={{$siteID}}&&image={{$img->images}}" class="delete"></a>
                       </div>
                   </div>
                  </div>

                  @empty
                  @endforelse
                </div>
              </form>
            </div>
        </div>


    </div>
