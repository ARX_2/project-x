<?php //dd($company); ?>
@include('admin.index')

        <div class="row">
            <div class="col-lg-4">
                <h2>Управление компанией</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Компания</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            @include('admin.company.menu')
            <div class="narrow">
              <form class="" action="{{route('admin.company.main', $company->about)}}" method="post">
                <input type="hidden" name="_method" value="put">
                    {{ csrf_field() }}
                <div class="box">
                    <div class="setting">
                        <h2>Основная информация</h2>
                        <div class="group-edit-row">
                            <div class="edit-label">Название:</div>
                            <div class="field">
                                <input type="text" name="title" class="" value="{{$company->name}}" maxlength="48"  placeholder="Введите название компании">
                            </div>
                        </div>
                        <div class="group-edit-row">
                            <div class="edit-label">Описание компании:</div>
                            <div class="field">
                                <textarea class="edit-textarea" maxlength="300" name="description" style="overflow: hidden; resize: none;"  placeholder="Введите краткое описание чем занимается компания">{{$company->description}}</textarea>
                            </div>
                        </div>
                        <div class="group-edit-row">
                            <div class="edit-label">Адрес сайта:</div>
                            <div class="field">
                                <input type="text" class="" name="domain" value="{{$company->title}}" maxlength="100" readonly="readonly">
                            </div>
                        </div>
                        <hr>
                        <div class="group-edit-row">
                            <div class="edit-label">&nbsp;</div>
                            <div class="field">
                                <input class="button-save" type="submit" value="Сохранить">
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <form class="" action="{{route('admin.company.subupdate', $company->about)}}" method="post">
                  <input type="hidden" name="_method" value="put">
                      {{ csrf_field() }}
                <div class="box">
                    <div class="setting">
                        <h2>Дополнительная информация</h2>
                        <div class="group-edit-row">
                            <div class="edit-label">Тематика сайта:</div>
                            <div class="field">
                            <select name="type">
                                <option selected disabled>Выберите тематику</option>
                                <option value="0" @if($company->type==0)selected @endif>Инной</option>
                                <option value="1" @if($company->type==1)selected @endif>Строительство, ремонт</option>
                                <option value="2" @if($company->type==2)selected @endif>Салоны красоты</option>
                                <option value="3" @if($company->type==3)selected @endif>Изготовление мебели</option>
                            </select>
                            </div>
                        </div>
                        <style>
                        .edit-block{width:80%;float:right}
                        .c_p_company .narrow .setting .edit-clarification{position:relative;margin-bottom:5px;display: flex;width: 100%}
                        .c_p_company .narrow .setting .edit-clarification .data{float:left;width:39%;min-width:140px;margin-right:1%}
                        .c_p_company .narrow .setting .edit-clarification .clarification{width:60%;float:left}
                        .c_p_company .narrow .setting .edit-clarification a{color:#2986c7}
                        .c_p_company .narrow .setting .edit-clarification .del{line-height:36px;font-size:18px;padding-left:10px;color: #888; cursor: pointer;}
                        </style>
                        <div class="group-edit-row ">
                            <div class="edit-label">Телефон:</div>
                            <div class="edit-block">
                              @forelse($tels as $tel)
                              <div id="tels" class="tel{{$tel->id}}">
                                <div class="field edit-clarification">
                                    <div class="data">
                                        <input type="text" id="newTel" name="update[tel][{{$tel->id}}]" placeholder="+7 (___) ___-__-__"  maxlength="21" class="phone_mask" value="{{$tel->tel or ''}}">
                                        <script>$(".phone_mask").mask("+7 (999) 999-99-99");</script>
                                    </div>
                                    <div class="clarification">
                                        <input type="text" id="newTitleTel" name="update[TelsTitles][{{$tel->id}}]" placeholder="Например: менеджер" maxlength="70" value="{{$tel->title or ''}}">
                                    </div>
                                    <div class="del" id="delTel{{$tel->id}}">✕</div>
                                </div>
                                </div>
                                <script type="text/javascript">
                                $('#delTel{{$tel->id}}').click(function(){
                                  $('.tel{{$tel->id}}').remove();
                                  $.get('{{route("admin.del.telEmail")}}?type=0&&id={{$tel->id}}', function(data) {
                                  });
                                });
                                </script>
                                @empty
                                <div id="tels">
                                <div class="field edit-clarification">
                                    <div class="data">
                                        <input type="text"  name="tels[]" placeholder="+7 (___) ___-__-__"  maxlength="21" class="phone_mask">
                                        <script>$(".phone_mask").mask("+7 (999) 999-99-99");</script>

                                    </div>
                                    <div class="clarification">
                                        <input type="text" name="telsTitles[]" placeholder="Например: менеджер" maxlength="70">
                                    </div>
                                </div>
                                </div>
                                @endforelse
                                <script type="text/javascript">
                                $(document).ready(function(){
                                  $('#addTel').click(function(){
                                    $('#tels').clone().appendTo('.addTel');
                                    $('.addTel').find('#newTel').attr('name', 'tels[]');
                                    $('.addTel').find('#newTitleTel').attr('name', 'telsTitles[]');
                                  });
                                });
                                </script>
                                <div class="addTel"></div>
                                <div class="field edit-clarification">
                                    <a  id="addTel" style="cursor:pointer">Добавить номер телефона</a>
                                </div>
                            </div>
                        </div>

                        <div class="group-edit-row ">
                            <div class="edit-label">E-mail:</div>
                            <div class="edit-block">
                                @forelse($emails as $email)
                                <div id="emails" class="email{{$email->id}}">
                                <div class="field edit-clarification">
                                    <div class="data">
                                        <input type="text" id="newEmail" name="update[Emails][{{$email->id}}]" placeholder="Адрес эл. почты" maxlength="70" value="{{$email->email or ''}}">
                                    </div>
                                    <div class="clarification">
                                        <input type="text" id="newEmailTitle" name="update[EmailsTitles][{{$email->id}}]" placeholder="Например: пресс-служба" maxlength="70" value="{{$email->title or ''}}">
                                    </div>
                                    <div class="del" id="delEmail{{$email->id}}">✕</div>
                                </div>
                                </div>
                                <script type="text/javascript">
                                $('#delEmail{{$email->id}}').click(function(){
                                  $('.email{{$email->id}}').remove();
                                  $.get('{{route("admin.del.telEmail")}}?type=1&&id={{$email->id}}', function(data) {
                                  });
                                });
                                </script>
                                @empty
                                <div id="emails">
                                <div class="field edit-clarification">
                                    <div class="data">
                                        <input type="text" name="emails[]" placeholder="Адрес эл. почты" maxlength="70">
                                    </div>
                                    <div class="clarification">
                                        <input type="text" name="emailsTitles[]" placeholder="Например: пресс-служба" maxlength="70">
                                    </div>
                                </div>
                                </div>
                                @endforelse
                                <div class="addEmail"></div>
                                <div class="field edit-clarification">
                                    <a  id="addEmail" style="cursor:pointer">Добавить почту</a>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $(document).ready(function(){
                          $('#addEmail').click(function(){
                            $('#emails').clone().appendTo('.addEmail');
                            $('.addEmail').find('#newEmail').attr('name', 'emails[]');
                            $('.addEmail').find('#newEmailTitle').attr('name', 'emailsTitles[]');
                          });
                        });
                        </script>
                        <div class="group-edit-row">
                            <div class="edit-label">Страна:</div>
                            <div class="field">
                                <select name="country">
                                  <option selected disabled>Не выбрана</option>
                                  <option value="Россия" @if($company->country=='Россия')selected @endif>Россия</option>
                                </select>
                            </div>
                        </div>
                        <div class="group-edit-row">
                            <div class="edit-label">Город:</div>
                            <div class="field">
                                <select name="city">
                                  <option selected disabled>Не выбран</option>
                                  @forelse($cities as $city)
                                  <option value="{{$city->id}}" @if($company->city== $city->id)selected @endif>{{$city->gorod}}</option>
                                  @empty
                                  @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="group-edit-row">
                            <div class="edit-label">Время работы</div>
                            <div class="field">
                                <input type="text" class="" value="{{$company->info->timeJob or ''}}" maxlength="100" name="timeJob" placeholder="Например: пн-пт с 09:00 до 17:00, сб-вс - выходной">
                            </div>
                        </div>
                        <div class="group-edit-row">
                            <div class="edit-label">Адрес:</div>
                            <div class="field">
                                <input type="text" class="" value="{{$company->address or ''}}" maxlength="100" name="address"placeholder="Например: ул. Ленина 1к1, офис 101">
                            </div>
                        </div>
                        <div class="group-edit-row">
                            <div class="edit-label">Карта:</div>
                            <div class="field">
                                <textarea name="map" class="edit-textarea" maxlength="300" style="overflow: hidden; resize: none;">{{$company->info->map or ''}}</textarea>
                            </div>
                        </div>
                        <hr>
                        <div class="group-edit-row">
                            <div class="edit-label">&nbsp;</div>
                            <div class="field">
                              <input class="button-save" type="submit" value="Сохранить">
                            </div>
                        </div>
                    </div>
                </div>
              </form>
                <style>

                    textarea::-moz-placeholder {color:#bbb}
                    textarea::-webkit-input-placeholder {color:#bbb}
                    textarea:-ms-input-placeholder {color:#bbb}
                    textarea::-ms-input-placeholder {color:#bbb}
                    textarea::placeholder {color:#bbb}

                    input[type="text"]::-moz-placeholder {color:#bbb}
                    input[type="text"]::-webkit-input-placeholder {color:#bbb}
                    input[type="text"]:-ms-input-placeholder {color:#bbb}
                    input[type="text"]::-ms-input-placeholder {color:#bbb}
                    input[type="text"]::placeholder {color:#bbb}
                </style>
              <form class="" action="{{route('admin.SocialNetwork.update')}}" method="post">
                    {{ csrf_field() }}
                <div class="box">
                    <div class="setting">
                        <h2>Социальные сети</h2>
                        <div class="group-edit-row">
                            <div class="edit-label">Вконтакте:</div>
                            <div class="field">
                                <input type="text" name="vkontakte" class="" value="{{$company->info->vkontakte or ''}}" placeholder="https://vk.com/">
                            </div>
                        </div>
                        <div class="group-edit-row">
                            <div class="edit-label">Одноклассники:</div>
                            <div class="field">
                                <input type="text" name="odnoklassniki" class="" value="{{$company->info->odnoklassniki or ''}}" placeholder="http://ok-gid.ru/">
                            </div>
                        </div>
                        <div class="group-edit-row">
                            <div class="edit-label">Instagram:</div>
                            <div class="field">
                                <input type="text" name="instagram" class="" value="{{$company->info->instagram or ''}}" placeholder="https://www.instagram.com/">
                            </div>
                        </div>
                        <div class="group-edit-row">
                            <div class="edit-label">YouTube:</div>
                            <div class="field">
                                <input type="text" name="youtube" class="" value="{{$company->info->youtube or ''}}" placeholder="https://www.youtube.com/channel/">
                            </div>
                        </div>
                        <hr>
                        <div class="group-edit-row">
                            <div class="edit-label">&nbsp;</div>
                            <div class="field">
                                <input class="button-save" type="submit" value="Сохранить">
                            </div>
                        </div>
                    </div>
                </div>
                </form>

              @if(Auth::user()->userRolle == 1)
              <form class="" action="{{route('admin.updateSite')}}" method="post">
                    {{ csrf_field() }}
              <div class="box">
                  <div class="setting">
                      <h2>Для разработчиков</h2>
                      <!-- <div class="group-edit-row">
                          <div class="edit-label">Типа сайта</div>
                          <div class="field">
                            <label><input type="radio" name="type" value="1" style="display:none;" ><img src="{{asset('public/imgIncludes/site_1.jpg')}}" width="150" id="site1"></label>
                            <label><input type="radio" name="type" value="2" style="display:none;" ><img src="{{asset('public/imgIncludes/site_2.jpg')}}" width="150" id="site2"></label>
                          </div>
                      </div> -->
                      <div class="group-edit-row">
                          <div class="edit-label">Перенести контент</div>
                          <div class="field">
                            <div class="btn-group">
                              <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:100%">
                                Выбрать сайты
                              </button>


                              <div class="dropdown-menu prokrutka" style="padding:10px; width:300%">
                                <input type="text" class="search form-control" placeholder="povishev.ru">
                                @foreach($sites as $s)
                                <div class="site">
                                  <label for="" style="font-size:17px;" class="form-check-label" for="sites"><input type="checkbox" name="sites[]" value="{{$s->id}}" style="width:20px;height:20px;" class="chooseSite"> {{$s->title}}</label><br>
                                </div>

                                @endforeach
                              </div>
                            </div>

                            <!-- <div class="clearfix"></div>
                            <br>
                            <span class="viewSite" style="width:100%;margin-top:10px;border-bottom:2px solid #ddd;float:left;display:none;">
                              <a href="http://povishev.ru" style="font-size:16px;float:left;">povishev.ru</a>
                              <i class="fa fa-times" aria-hidden="true" style="float:right;font-size:20px; margin-top:2px;"></i>
                              <div class="clearfix"></div>
                            </span>
                            <div class="clearfix"></div>-->
                          </div>

                      </div>
                      <table class="table" style="width:100%; display:none;">
                        <tr>
                          <td><input type="checkbox" name="categories" value="1" class="option" style="height:15px; width:15px;"> Категории </td>
                          <?php $i = 1; ?>
                          @if($company->sections[0]->published == 1)
                          <td><input type="checkbox" name="{{$company->sections[0]->slug}}" value="1" class="option" style="height:15px; width:15px;"> {{$company->sections[0]->title}} </td>
                          @endif
                          @if($company->sections[1]->published == 1)
                          <td><input type="checkbox" name="{{$company->sections[1]->slug}}" value="1" class="option" style="height:15px; width:15px;"> {{$company->sections[1]->title}} </td>
                          @endif
                        </tr>
                        <tr>
                          @if($company->sections[2]->published == 1)
                          <td><input type="checkbox" name="{{$company->sections[2]->slug}}" value="1" class="option" style="height:15px; width:15px;"> {{$company->sections[2]->title}} </td>
                          @endif
                          @if($company->sections[3]->published == 1)
                          <td><input type="checkbox" name="{{$company->sections[3]->slug}}" value="1" class="option" style="height:15px; width:15px;"> {{$company->sections[3]->title}} </td>
                          @endif
                          @if($company->sections[4]->published == 1)
                          <td><input type="checkbox" name="{{$company->sections[4]->slug}}" value="1" class="option" style="height:15px; width:15px;"> {{$company->sections[4]->title}} </td>
                          @endif
                        </tr>
                        <tr>
                          @if($company->sections[5]->published == 1)
                          <td><input type="checkbox" name="{{$company->sections[5]->slug}}" value="1" class="option" style="height:15px; width:15px;"> {{$company->sections[5]->title}} </td>
                          @endif
                          <td><input type="checkbox" name="objects" value="1" class="option" style="height:15px; width:15px;">Портфолио</td>
                          <td><input type="checkbox" name="text" value="1" class="option" style="height:15px; width:15px;">Описание на страницах</td>
                        </tr>
                        <tr>
                          <td><input type="checkbox" name="news" value="1" class="option" style="height:15px; width:15px;">Новости</td>
                          <td><input type="checkbox" name="banners" value="1" class="option" style="height:15px; width:15px;">Баннеры</td>
                          <td><input type="checkbox" name="tizers" value="1" class="option" style="height:15px; width:15px;">Тизеры</td>
                        </tr>
                        <tr>
                          <td><input type="checkbox" name="information" value="1" class="option" style="height:15px; width:15px;">Информация</td>
                          <td><input type="checkbox" name="tels" value="1" class="option" style="height:15px; width:15px;">Телефоны</td>
                          <td><input type="checkbox" name="emails" value="1" class="option" style="height:15px; width:15px;">Емелы</td>
                        </tr>
                        <tr>
                          <td><input type="checkbox" name="all" value="1" style="height:15px; width:15px;"><b>Все</b></td>
                          <td><input type="checkbox" name="clear" value="1" style="height:15px; width:15px;"><b>Очистить</b></td>
                          <td><input type="checkbox" name="cities" value="1" style="height:15px; width:15px;" disabled><b style="color:#ddd;">Проставить город</b></td>
                        </tr>
                      </table>
                      <script type="text/javascript">
                        $('.dropdown-toggle').click(function(){
                          $('.table').show();
                        });
                        $('input[name="all"]').click(function(){
                          var prop = $(this).prop('checked');
                          if(prop == true){
                            $('.option').prop('checked',true);
                          }
                          else{
                            $('.option').prop('checked',false);
                          }
                        });
                      </script>
                      <div class="group-edit-row">
                          <div class="edit-label">&nbsp;</div>
                          <div class="field">
                            <button type="submit" class="btn btn-warning form-control" style="color:black;">Перенести контент</button>
                          </div>
                      </div>
                      <hr>
                    </form>
                      <style media="screen">
                        .cache,.sectionType{
                          padding: 5px;
                          font-size: 17px;
                          cursor: pointer;
                        }
                        #cacheTrue,#goods,#services{
                          padding:5px 8px;
                        }
                        .cacheColor,.sectionTrue{
                          border:3px solid rgb(17,172,238);
                        }
                      </style>
                      <div class="group-edit-row">
                          <div class="edit-label">Cохранения:</div>
                          <div class="field">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                              Откатиться
                            </button>
                          </div>

                      </div>

                      <div class="group-edit-row">
                          <div class="edit-label">Обнов. КЭШ</div>
                          <div class="field">
                            <span class="cache @if(Auth::user()->refreshCache == 1)cacheColor @endif" id="cacheTrue" >Да</span>
                            <span class="cache @if(Auth::user()->refreshCache == 0 || Auth::user()->refreshCache == null)cacheColor @endif" id="cacheFalse" >Нет</span>
                          </div>

                      </div>

                      <div class="group-edit-row">
                          <div class="edit-label">Разделы</div>
                          <div class="field">
                            <div id="list">
                            <div id="response"> </div>
                            <div id="sort">
                            @forelse($company->sections as $section)
                            @if($section->slug == 'stroitelstvo' && $company->type == 1 || $section->slug == 'bani' && $company->type == 1 || $section->slug == 'proyekty' && $company->type == 1)
                            <span class="sectionType @if($section->published == 1)sectionTrue @endif" id="arrayorder_{{$section->id}}" slug="{{$section->slug}}">{{$section->title}}</span>
                            @elseif($section->slug == 'izgotovleniye' && $company->type == 3)
                            <span class="sectionType @if($section->published == 1)sectionTrue @endif" id="arrayorder_{{$section->id}}" slug="{{$section->slug}}">{{$section->title}}</span>
                            @elseif($section->slug == 'product' || $section->slug == 'services')
                            <span class="sectionType @if($section->published == 1)sectionTrue @endif" id="arrayorder_{{$section->id}}" slug="{{$section->slug}}">{{$section->title}}</span>
                            @endif
                            @empty
                            @endforelse
                          </div>

                      </div>
                      <hr>

                  </div>
              </div>

            @endif
            </div>
        </div>
    </div>
    <?php //dd($company); ?>
    <script type="text/javascript" src="/public/admin/js/jquery.mask.js"></script>
    <script type="text/javascript">

      $('.dropdown-menu').on('keyup', '.search', function(){
                    _this = this;
                    $.each($(".site"), function() {
                        if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                           $(this).hide();
                        else
                           $(this).show();
                    });
                });
    </script>

    <script type="text/javascript">
    $('.sectionType').click(function(){
      if($(this).hasClass('sectionTrue')){
        $(this).removeClass('sectionTrue');
        var params = 1;
      }
      else{
        $(this).addClass('sectionTrue');
        var params = 0;
      }
      $.get('/admin/choose/section/'+$(this).attr('slug')+'/'+params, function(data){});
    });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('#site1').click(function(){
          $('#site1').attr('style', 'border:4px solid rgba(19, 174, 230, 0.61);background-color:white;box-shadow:1px 1px 1px 2px rgb(110,211,221)');
          $('#site2').attr('style', '');
        });
        $('#site2').click(function(){
          $('#site2').attr('style', 'border:4px solid rgba(19, 174, 230, 0.61);background-color:white;box-shadow:1px 1px 1px 2px rgb(110,211,221)');
          $('#site1').attr('style', '');
        });
      });
    </script>

    <script type="text/javascript">
    $(document).ready(function(){
    $(function() {
    $("#list #sort").sortable({ opacity: 0.8, cursor: 'move', update: function() {

    var order = $(this).sortable("serialize") + '&update=update';


    $.get("{{route('admin.sections.main')}}", order, function(theResponse){
    $("#response").html(theResponse);
    $("#response").slideDown('slow');
    slideout();
    });
    }
    });
    });
    });

    </script>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document" style="width:65%">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <button type="button" name="button" class="form-control button-saves" id="categories">Категории</button>
            @forelse($company->sections as $section)
            @if($section->published == 1)
            <button type="button" name="button" class="form-control button-saves" id="{{$section->slug}}">{{$section->title}}</button>
            @endif
            @empty
            @endforelse
            <button type="button" name="button" class="form-control button-saves" id="objects">Портфолио</button>
            <button type="button" name="button" class="form-control button-saves" id="texts">Описание на страницах</button>
            <button type="button" name="button" class="form-control button-saves" id="news">Новости</button>
            <button type="button" name="button" class="form-control button-saves" id="banners">Баннеры</button>
            <button type="button" name="button" class="form-control button-saves" id="tizers">Тизеры</button>
            <button type="button" name="button" class="form-control button-saves" id="informations">Информация</button>
            <button type="button" name="button" class="form-control button-saves" id="tels">Телефоны</button>
            <button type="button" name="button" class="form-control button-saves" id="emails">E-mails</button>
            <div class="clearfix"></div>
            <hr>
            <div class="saves prokrutka" style="height:40vh">
              <table class="table">
                <thead>
                  <th>id</th>
                  <th>Название</th>
                  <th>Создатель</th>
                  <th>Дата создания</th>
                </thead>
                <tbody class="tbody-saves">

                </tbody>
              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button type="button" class="btn btn-primary">Откатить</button>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $('.button-saves').click(function(){
        var slug = $(this).attr('id');
        $.get('/admin/saves/'+slug, function(data){
          $('.tbody-saves').html(data);
        });
      });
      $('.tbody-saves').on('click', '.fa-floppy-o',function(){
        if(confirm("Ты хорошо подумал(а)?")){
          var name = $(this).attr('id');
          var slug = $(this).attr('slug');
          $.get('/admin/saves/upload/'+slug+'/'+name, function(data){
            alert('Сохранение восстановлено!');
          });
        }

      });
    </script>
    <style>
    .button-saves{
      width:auto;float:left;margin-left:10px; background-color:#383838; color:white;
    }
    .prokrutka {
    height: 20vh;
    background: #fff; /* цвет фона, белый */
    overflow-y: scroll; /* прокрутка по вертикали */
    }

::-webkit-scrollbar {
width: 3px; height: 3px;
}


::-webkit-scrollbar-track-piece  {
background-color: #c7c7c7;
}

::-webkit-scrollbar-thumb:vertical {
height: 50px; background-color: #666; border-radius: 3px;
}

::-webkit-scrollbar-thumb:horizontal {
height: 50px; background-color: #666; border-radius: 3px;
}

::-webkit-scrollbar-corner{
  background-color: #999;
}
.emoji{
  width: 18px !important; height:18px !important;
}
    </style>

    <style media="screen">
      .rotate{
        -webkit-animation-name: cog;
-webkit-animation-duration: 1s;
-webkit-animation-iteration-count: infinite;
-webkit-animation-timing-function: linear;
-moz-animation-name: cog;
-moz-animation-duration: 1s;
-moz-animation-iteration-count: infinite;
-moz-animation-timing-function: linear;
-ms-animation-name: cog;
-ms-animation-duration: 1s;
-ms-animation-iteration-count: infinite;
-ms-animation-timing-function: linear;

animation-name: cog;
animation-duration: 1s;
animation-iteration-count: infinite;
animation-timing-function: linear;
}
@-ms-keyframes cog {
from { -ms-transform: rotate(0deg); }
to { -ms-transform: rotate(20deg); }
}
@-moz-keyframes cog {
from { -moz-transform: rotate(0deg); }
to { -moz-transform: rotate(20deg); }
}
@-webkit-keyframes cog {
from { -webkit-transform: rotate(0deg); }
to { -webkit-transform: rotate(20deg); }
}
@keyframes cog {
from {
transform:rotate(0deg);
}
to {
transform:rotate(360deg);
}
      }
    </style>
