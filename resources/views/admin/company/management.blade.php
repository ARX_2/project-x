<?php //dd($company); ?>
@include('admin.index')

        <div class="row">
            <div class="col-lg-4">
                <h2>Управление компанией</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Компания</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            @include('admin.company.menu')
            <div class="narrow">
                <div class="box">
                    <div class="setting">
                        <h2>Настройки менеджера на сайте</h2>
                        <div class="a_m">
                          <form class="" action="{{route('admin.management.update')}}" method="post">
                            {{csrf_field()}}
                            <div class="group-edit-row">
                                <div class="edit-label">Выберите сотрудника:</div>
                                <div class="field">

                                    <select name="manager">
                                        <option selected disabled>Не выбран</option>
                                        @forelse($users as $u)

                                        <option value="{{$u->id}}" @if($u->onSite == 1) selected @endif>{{$u->name or ''}} {{$u->lastname or ''}}</option>
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Фотография:</div>
                                <div class="field">
                                    <div class="file-upload">
                                        <div class="image_group">
                                            <div class="image">
                                                <img src="/storage/app/{{$userOnSite->image or ''}}" alt="">
                                                <a href="" class="delete"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Имя Фамилия:</div>
                                <div class="field">{{$userOnSite->name or ''}} {{$userOnSite->lastname or ''}}</div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Должность:</div>
                                <div class="field">Менеджер</div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Телефон:</div>
                                <div class="field">{{$userOnSite->tel or ''}}</div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Адрес:</div>
                                <div class="field">{{$userOnSite->email or ''}}</div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Время работы</div>
                                <div class="field">
                                    <input type="text" class="" value="{{$userOnSite->timeJob or 'пн-сб 10:00-16:00'}}" maxlength="100" name="timeJob">
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">&nbsp;</div>
                                <div class="field">
                                    <input class="button-save" type="submit" value="Сохранить">
                                </div>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/public/admin/js/jquery.mask.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#site1').click(function(){
                $('#site1').attr('style', 'border:4px solid rgba(19, 174, 230, 0.61);background-color:white;box-shadow:1px 1px 1px 2px rgb(110,211,221)');
                $('#site2').attr('style', '');
            });
            $('#site2').click(function(){
                $('#site2').attr('style', 'border:4px solid rgba(19, 174, 230, 0.61);background-color:white;box-shadow:1px 1px 1px 2px rgb(110,211,221)');
                $('#site1').attr('style', '');
            });
        });
    </script>
</div>
