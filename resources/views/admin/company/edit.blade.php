
@extends('admin.layout.'.$siteType)

@section('content')
<div class="admin-content">
  @component('admin.components.breadcrumb')
  @slot('title') Редактирование о компании @endslot
  @slot('parent') Главная @endslot
  @slot('active') О компании @endslot
  @endcomponent
  <hr>

  <form class="form-horizontal" action="{{route('admin.company.update', $company)}}" method="post" enctype="multipart/form-data">
<input type="hidden" name="_method" value="put">
    {{ csrf_field() }}
    @include('admin.company.partials.form')

    <input type="hidden" name="created_by" value="{{Auth::id()}}">

  </form>

</div>
@endsection
