<div class="c_p_company">
    <div class="wide">
        <div class="box">
            <div class="wide_title">
                <h2>Правая штука</h2>
            </div>
            <div class="undefined_item">
            <div class="item">
                <div class="name">
                    <a href="{{route('admin.company.index')}}">Информация</a>
                </div>
            </div>
            <div class="item">
                <div class="name">
                    <a href="{{route('admin.company.requisite')}}">Реквизиты</a>
                </div>
            </div>
            <div class="item">
                <div class="name">
                    <a href="{{route('admin.company.logo')}}">Логотип</a>
                </div>
            </div>
            <div class="item">
                <div class="name">
                    <a href="/admin/settings/site/technical/verification/">Верификация сайта</a>
                </div>
            </div>
            <div class="item">
                <div class="name">
                    <a href="{{route('admin.company.management')}}">Настроить менеджера на сайте</a>
                </div>
            </div>
            <div class="item">
                <div class="name">
                    <a href="{{route('admin.company.description')}}">Описание на страницах сайта</a>
                </div>
            </div>
        </div>
    </div>
</div>
