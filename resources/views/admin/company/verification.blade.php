<?php //dd($company); ?>
@include('admin.index')
<script type="text/javascript">
  $('#admin-cont').remove();
</script>
<div class="admin">

    <div class="admin-content">
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление компанией</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li><a href="{{route('admin.company.index')}}">Компания</a></li>
                    <li class="active">Верификация сайта</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            @include('admin.company.menu')
            <div class="narrow">


            </div>
        </div>


    </div>
