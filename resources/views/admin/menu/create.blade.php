@extends('admin.layout.'.$siteType)

@section('content')
<div class="container">
  @component('admin.components.breadcrumb')
  @slot('title') Создание меню @endslot
  @slot('parent') Главная @endslot
  @slot('active') Контакты @endslot
  @endcomponent
  <hr>
  <?php

  ?>
  @if(count($menu)>0)
  <form class="form-horizontal" action="{{route('admin.menu.update', $menu[0]->id)}}" method="post" enctype="multipart/form-data">
<input type="hidden" name="_method" value="put">
  @else
  <form class="form-horizontal" action="{{route('admin.menu.insert')}}" method="post" enctype="multipart/form-data">
  @endif
    {{ csrf_field() }}
    <div class="col-sm-4">
    <?php $i = 1; //dd($menu);?>
    @foreach($menu as $m)


    <label for="">Меню {{$i}}</label>
    <input type="text" name="array[{{$m->id}}][menu]" class="form-control" value="{{$m->title or ''}}">
    <label for="">slug</label>
    <input type="text" name="array[{{$m->id}}][slug]" class="form-control" value="{{$m->slug or ''}}">
    @if($i<3)
    <label>Добавить всплывающий список</label><br>
    <label for="">Да<input type="radio" name="array[{{$m->id}}][slide]" value="0" @if($m->slide2 == 0) checked @endif></label>
    <label for="">Нет<input type="radio" name="array[{{$m->id}}][slide]" value="1" @if($m->slide2 == 1) checked @endif></label><br><br>
    @endif
    <?php $i++; ?>
    @endforeach

    <div class="menu"></div>

    <a class="AddMenu" style="cursor:pointer;">Добавить меню</a>
    <br>
    <br>
    <input type="submit" class="btn btn-primary" value="Сохранить" id="button">
    </div>
  </form>
  <div id="loader" style="display:none;">


  <h3 style="color:rgb(21,157,244);">Обновляется подождите...</h3>
  <img src="{{asset('public/img/giphy.gif')}}" alt="">
  </div>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#button').click(function(){
        $('#loader').show();
      });

      $(".AddMenu").click(function () {
          $('.menu').append(
            '<label>Доп. меню</label>'+
            '<input type="text" name="dopmenu[title][]" class="form-control">'+
          '<label for="">slug</label>'+
          '<input type="text" name="dopmenu[slug][]" class="form-control"><br>');
      });
    });
  </script>
</div>
@endsection
