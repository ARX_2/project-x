@extends('admin.layout.'.$siteType)

@section('content')
<div class="admin-content">
  @component('admin.components.breadcrumb')
  @slot('title') Редактирование сотрудника @endslot
  @slot('parent') Главная @endslot
  @slot('active') Сотрудники @endslot
  @endcomponent
  <hr>
  <?php
  //dd($User->id);
  ?>
  <form class="form-horizontal" action="{{route('admin.employee.update', $User)}}" method="post" enctype="multipart/form-data">
<input type="hidden" name="_method" value="put">
    {{ csrf_field() }}
    @include('admin.employee.partials.form')


  </form>

</div>
@endsection
