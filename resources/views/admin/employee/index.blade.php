@include('admin.index')

        <div class="row">
            <div class="col-lg-4">
                <h2>Управление сотрудниками</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Сотрудники</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="narrow_title" style="padding: 10px 0 20px;width: 50%;margin: 0 auto 20px;">
                        <div class="left">
                            <h2>Сотрудники</h2>
                        </div>
                        <div class="right">
                            <span class="button-edit"><span data-toggle="modal" data-target="#adduser">Добавить</span></span>
                        </div>
                    </div>
                    <style>
                        .users{width:50%;margin:10px auto}
                        .users .user-lvl{padding-bottom:20px;border-bottom:1px solid #ddd;margin-bottom:20px}
                        .users .user-lvl .title{font-size: 16px;font-weight: 600;margin-bottom: 10px;}
                        .users .item{display:inline-flex;padding:10px 0;width:100%;position:relative;}
                        .users .item:hover{background:#f7f7f7}
                        .users .image{float:left;width:60px;height:60px;margin-right:15px}
                        .users .image img{width:100%;height:100%;object-fit:cover;border-radius:50%}
                        .users .info {position: relative}
                        .users .info .name{font-size:14px;line-height: 16px;font-weight:600;color:#333}
                        .users .position{color:#999;font-size:14px;line-height: 16px;}
                        .users .more{font-size:14px;line-height: 16px;color: #2986c7;position: absolute;bottom: 0;left: 0}
                        .users .personal{color:#666;font-size:12px;line-height: 14px;margin-top: 5px}
                        .users .personal span{color:#888;font-weight:600}
                        .users .edit:before{position:absolute;right:15px;top:5px;font-family:FontAwesome;content:"\f141"}
                    </style>
                    <div class="users">
                        <form name="home" action="" method="POST">
                            <!-- Каждый уроверь разделяется через user-lvl
                            Последовательность такая (сверху вниз):
                            1. Менеджеры
                            2. Редакторы
                            3. Админинстраторы
                            4. Владелец
                            5. Разработчик
                            -->
                            @forelse($userRolles as $userRolle)
                            <div class="user-lvl">
                                <div class="user_list">
                                @forelse($userRolle->users as $u)
                                    <div class="item">
                                        <div class="image">
                                            <img src="{{asset('storage/app')}}/{{$u->image or '/public/no-image-80x80.png'}}" alt="">
                                        </div>
                                        <div class="info">
                                            <div class="basic" style="position: relative;height: 60px">
                                                <div class="name">{{$u->name or 'Имя Фамилия'}}</div>
                                                <div class="position">@if($u->userRolle == 1) Разработчик @elseif($u->userRolle == 2) Администратор @elseif($u->userRolle == 3) Редактор @elseif($u->userRolle == 4) Менеджер @endif</div>
                                                <a href="javascript:void(0);" class="more collapsed" id="user{{$u->id}}" data-toggle="collapse" data-target="#c_user{{$u->id}}" aria-expanded="false" aria-controls="collapseTwo">
                                                    Информация
                                                </a>
                                            </div>
                                            <div id="c_user{{$u->id}}" class="advanced collapse" aria-labelledby="user{{$u->id}}" data-parent="#accordion">
                                                <div class="personal">Телефон: <span>{{$u->tel}}</span></div>
                                                <div class="personal">Почта: <span>{{$u->email}}</span></div>
                                                <div class="personal">Должность: <span>{{$u->job}}</span></div>
                                            </div>
                                        </div>
                                        <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                        <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                            <li><a href="" data-toggle="modal" data-target="#exampleModalLong{{$u->id}}">Редактировать</a></li>
                                            @if($u->userRolle <= 2)<li><a href="{{route('admin.employee.delete', ['id' => $u->id])}}">Удалить</a></li>@endif
                                        </ul>
                                    </div>
                                    @include('admin.employee.partials.modal', ['u' => $u])
                                    @empty
                                @endforelse
                                </div>
                            </div>
                            @empty
                            @endforelse




                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="modal fade" id="adduser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content vm-goods">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="modal-body">
                        <div class="content">

                            <form class="" action="{{route('admin.employee.store')}}" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                            <div class="vm-goods modal_edit">
                                <div class="vm-header">
                                    <div class="name">Добавление сотрудника</div>
                                </div>

                                @if(Auth::user()->userRolle <= 3)
                                <div class="vm-body">
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Имя и фамилия:</label></div>
                                        <div class="center">
                                            <input type="text" name="name" class="user_name" placeholder="Имя" value="" required="">
                                            <input type="text" name="lastname" class="user_name" placeholder="Фамилия" value="" required="">
                                        </div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Пол:</label></div>
                                        <div class="center">
                                            <label class="radio_item">
                                                <input type="radio" id="user_sex" name="user_sex" class="sex" placeholder="Мужской" value="0" required="">
                                                <div class="radio_box">Мужской</div>
                                            </label>
                                            <label class="radio_item">
                                                <input type="radio" id="user_sex" name="user_sex" class="sex" placeholder="Женский" value="1" required="">
                                                <div class="radio_box">Женский</div>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Email:</label></div>
                                        <div class="center">
                                            <input type="text" name="email" class="" placeholder="email@company.ru" value="" required="">
                                        </div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Телефон:</label></div>
                                        <div class="center">
                                            <input type="text" name="tel" class="" placeholder="+7 (___) ___-__-__" value="" required="">
                                        </div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Пароль:</label></div>
                                        <div class="center">
                                            <input type="text" name="password" class="" placeholder="" value="" required="">
                                        </div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Уровень полномочий:</label></div>
                                        <div class="center">
                                            <div class="row_item">
                                                <label class="type">
                                                    <div class="left"><input type="radio" name="userRolle" value="4"></div>
                                                    <div class="right">
                                                        <div class="position">Менеджер</div>
                                                        <div class="info">Прием, обработка заказов</div>
                                                    </div>
                                                </label>
                                                <label class="type">
                                                    <div class="left"><input type="radio" name="userRolle" value="3"></div>
                                                    <div class="right">
                                                        <div class="position">Редактор</div>
                                                        <div class="info">Может добавлять, редактировать, удалять категории, товары и другое содержимое сайтом.</div>
                                                    </div>
                                                </label>
                                                <label class="type">
                                                    <div class="left"><input type="radio" name="userRolle" value="2"></div>
                                                    <div class="right">
                                                        <div class="position">Администратор</div>
                                                        <div class="info">Может управвлять всем содержимым на сайте, изменять настройки, добавлять и удалять сотрудников</div>
                                                    </div>
                                                </label>
                                                <label class="type">
                                                    <div class="left"><input type="radio" name="userRolle" value="6"></div>
                                                    <div class="right">
                                                        <div class="position">Арендатор</div>
                                                        <div class="info">Для людей, которые арендовали сайт</div>
                                                    </div>
                                                </label>
                                                @if(Auth::user()->userRolle == 1)
                                                <label class="type">
                                                    <div class="left"><input type="radio" name="userRolle" value="1"></div>
                                                    <div class="right">
                                                        <div class="position">Разрабочик</div>
                                                        <div class="info">Режим бога, управление всеми сайтами</div>
                                                    </div>
                                                </label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Должность:</label></div>
                                        <div class="center"><input type="text" name="job" class="" placeholder="Например: менеджер" value=""></div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Фото:</label></div>
                                        <div class="center"><input type="file" name="image" ></div>
                                    </div>
                                    <div class="perent" style="margin-bottom: 0">
                                        <div class="left"><label for="" class="item_name"></label></div>
                                        <div class="center">
                                            <input class="button-save" type="submit" value="Добавить сотрудника" style="width: 175px;background-color: #269ffd;color: #fff;">
                                        </div>
                                    </div>
                                </div>
                                @else
                                Доступ закрыт
                                @endif
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
