@extends('admin.layout.'.$siteType)

@section('content')
<div class="admin-content">
  @component('admin.components.breadcrumb')
  @slot('title') Календарь сотрудника: {{$user->name or ''}} @endslot
  @slot('parent') Главная @endslot
  @slot('active') Календарь @endslot
  @endcomponent
  <hr>
  <?php //dd($nownonth); ?>

  <style>
  .boxes {
margin: auto;
padding: 0 50px;

}

/*Checkboxes styles*/
input[type="checkbox"] { display: none; }

input[type="checkbox"] + label {
display: block;
position: relative;
padding-left: 35px;
margin-bottom: 20px;
font: 14px/20px 'Open Sans', Arial, sans-serif;
color: black;
cursor: pointer;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
}

input[type="checkbox"] + label:last-child { margin-bottom: 0; }

input[type="checkbox"] + label:before {
content: '';
display: block;
width: 20px;
height: 20px;
border: 1px solid black;
position: absolute;
left: 0;
top: 0;
opacity: .6;
-webkit-transition: all .12s, border-color .08s;
transition: all .12s, border-color .08s;
}

input[type="checkbox"]:checked + label:before {
width: 10px;
top: -5px;
left: 5px;
border-radius: 0;
opacity: 1;
border-top-color: transparent;
border-left-color: transparent;
-webkit-transform: rotate(45deg);
transform: rotate(45deg);
}
  </style>



  <form class="form-horizontal"  action="{{route('admin.employee.calendar.store')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="row">
      <div class="col-sm-3">
        <label for="">Выберите месяц</label>
        <select class="form-control" name="month" id="zapros">
          <option value="1" @if($nownonth == '01') selected="" @endif>Январь</option>
          <option value="2" @if($nownonth == '02') selected="" @endif>Февраль</option>
          <option value="3" @if($nownonth == '03') selected="" @endif>Март</option>
          <option value="4" @if($nownonth == '04') selected="" @endif>Апрель</option>
          <option value="5" @if($nownonth == '05') selected="" @endif>Май</option>
          <option value="6" @if($nownonth == '06') selected="" @endif>Июнь</option>
          <option value="7" @if($nownonth == '07') selected="" @endif>Июль</option>
          <option value="8" @if($nownonth == '08') selected="" @endif>Август</option>
          <option value="9" @if($nownonth == '09') selected="" @endif>Сентябрь</option>
          <option value="10" @if($nownonth == '10') selected="" @endif>Октябрь</option>
          <option value="11" @if($nownonth == '11') selected="" @endif>Ноябрь</option>
          <option value="12" @if($nownonth == '12') selected="" @endif>Декабрь</option>
        </select>

      </div>
    </div>
    <?php $m = array(); ?>
    <h3 style=" padding:20px 50px;">Выберите числа</h3>
    <?php $i=1; $arraymonth = array( 0,1,2,3,4,5,6,7,8,9,10,11,12); ?>
    @forelse($calendar as $cal)

    <?php $m[] = $cal->month ?>

    <div class="row" @if($nownonth == $cal->month)style="display:block;" @else style="display:none;"@endif id="{{$cal->month}}" class="cal">

        <div style="float:left;">
          <div class="boxes">
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_1]" @if($cal->day_1 == 1) checked @endif>
            <label for="box-{{$i}}">1</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_2]" @if($cal->day_2 == 1) checked @endif>
            <label for="box-{{$i}}">2</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_3]" @if($cal->day_3 == 1) checked @endif>
            <label for="box-{{$i}}">3</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_4]" @if($cal->day_4 == 1) checked @endif>
            <label for="box-{{$i}}">4</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_5]" @if($cal->day_5 == 1) checked @endif>
            <label for="box-{{$i}}">5</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_6]" @if($cal->day_6 == 1) checked @endif>
            <label for="box-{{$i}}">6</label>
          </div>

        </div>
        <div style="float:left;">
          <div class="boxes">

            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_7]" @if($cal->day_7 == 1) checked @endif>
            <label for="box-{{$i}}">7</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_8]" @if($cal->day_8 == 1) checked @endif>
            <label for="box-{{$i}}">8</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_9]" @if($cal->day_9 == 1) checked @endif>
            <label for="box-{{$i}}">9</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_10]" @if($cal->day_10 == 1) checked @endif>
            <label for="box-{{$i}}">10</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_11]" @if($cal->day_11 == 1) checked @endif>
            <label for="box-{{$i}}">11</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_12]" @if($cal->day_12 == 1) checked @endif>
            <label for="box-{{$i}}">12</label>
          </div>

        </div>
        <div style="float:left;">
          <div class="boxes">

            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_13]" @if($cal->day_13 == 1) checked @endif>
            <label for="box-{{$i}}">13</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_14]" @if($cal->day_14 == 1) checked @endif>
            <label for="box-{{$i}}">14</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_15]" @if($cal->day_15 == 1) checked @endif>
            <label for="box-{{$i}}">15</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_16]" @if($cal->day_16 == 1) checked @endif>
            <label for="box-{{$i}}">16</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_17]" @if($cal->day_17 == 1) checked @endif>
            <label for="box-{{$i}}">17</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_18]" @if($cal->day_18 == 1) checked @endif>
            <label for="box-{{$i}}">18</label>
          </div>

        </div>
        <div style="float:left;">
          <div class="boxes">

            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_19]" @if($cal->day_19 == 1) checked @endif>
            <label for="box-{{$i}}">19</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_20]" @if($cal->day_20 == 1) checked @endif>
            <label for="box-{{$i}}">20</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_21]" @if($cal->day_21 == 1) checked @endif>
            <label for="box-{{$i}}">21</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_22]" @if($cal->day_22 == 1) checked @endif>
            <label for="box-{{$i}}">22</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_23]" @if($cal->day_23 == 1) checked @endif>
            <label for="box-{{$i}}">23</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_24]" @if($cal->day_24 == 1) checked @endif>
            <label for="box-{{$i}}">24</label>


          </div>
        </div>



        <div style="float:left;">
          <div class="boxes">
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_25]" @if($cal->day_25 == 1) checked @endif>
            <label for="box-{{$i}}">25</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_26]" @if($cal->day_26 == 1) checked @endif>
            <label for="box-{{$i}}">26</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_27]" @if($cal->day_27 == 1) checked @endif>
            <label for="box-{{$i}}">27</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_28]" @if($cal->day_28 == 1) checked @endif>
            <label for="box-{{$i}}">28</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_29]" @if($cal->day_29 == 1) checked @endif>
            <label for="box-{{$i}}">29</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_30]" @if($cal->day_30 == 1) checked @endif>
            <label for="box-{{$i}}">30</label>
            <?php $i++; ?>


          </div>

        </div>
        <div style="float:left;">
          <div class="boxes">
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$cal->month}}[day_31]" @if($cal->day_31 == 1) checked @endif>
            <label for="box-{{$i}}">31</label>
          </div>
        </div>


      <div style="clear:both;">

      </div>

    </div>
    @empty

    <div class="row">

        <div style="float:left;">
          <div class="boxes">
            <input type="checkbox" id="box-1" value="1" name="day_1">
            <label for="box-1">1</label>

            <input type="checkbox" id="box-2" value="1" name="day_2">
            <label for="box-2">2</label>

            <input type="checkbox" id="box-3" value="1" name="day_3">
            <label for="box-3">3</label>

            <input type="checkbox" id="box-4" value="1" name="day_4">
            <label for="box-4">4</label>

            <input type="checkbox" id="box-5" value="1" name="day_5">
            <label for="box-5">5</label>

            <input type="checkbox" id="box-6" value="1" name="day_6">
            <label for="box-6">6</label>
          </div>

        </div>
        <div style="float:left;">
          <div class="boxes">


            <input type="checkbox" id="box-7" value="1" name="day_7">
            <label for="box-7">7</label>

            <input type="checkbox" id="box-8" value="1" name="day_8">
            <label for="box-8">8</label>

            <input type="checkbox" id="box-9" value="1" name="day_9">
            <label for="box-9">9</label>

            <input type="checkbox" id="box-10" value="1" name="day_10">
            <label for="box-10">10</label>

            <input type="checkbox" id="box-11" value="1" name="day_11">
            <label for="box-11">11</label>

            <input type="checkbox" id="box-12" value="1" name="day_12">
            <label for="box-12">12</label>
          </div>

        </div>
        <div style="float:left;">
          <div class="boxes">


            <input type="checkbox" id="box-13" value="1" name="day_13">
            <label for="box-13">13</label>

            <input type="checkbox" id="box-14" value="1" name="day_14">
            <label for="box-14">14</label>

            <input type="checkbox" id="box-15" value="1" name="day_15">
            <label for="box-15">15</label>

            <input type="checkbox" id="box-16" value="1" name="day_16">
            <label for="box-16">16</label>

            <input type="checkbox" id="box-17" value="1" name="day_17">
            <label for="box-17">17</label>

            <input type="checkbox" id="box-18" value="1" name="day_18">
            <label for="box-18">18</label>
          </div>

        </div>
        <div style="float:left;">
          <div class="boxes">


            <input type="checkbox" id="box-19" value="1" name="day_19">
            <label for="box-19">19</label>

            <input type="checkbox" id="box-20" value="1" name="day_20">
            <label for="box-20">20</label>

            <input type="checkbox" id="box-21" value="1" name="day_21">
            <label for="box-21">21</label>

            <input type="checkbox" id="box-22" value="1" name="day_22">
            <label for="box-22">22</label>

            <input type="checkbox" id="box-23" value="1" name="day_23">
            <label for="box-23">23</label>

            <input type="checkbox" id="box-24" value="1" name="day_24">
            <label for="box-24">24</label>


          </div>
        </div>



        <div style="float:left;">
          <div class="boxes">
            <input type="checkbox" id="box-25" value="1" name="day_25">
            <label for="box-25">25</label>

            <input type="checkbox" id="box-26" value="1" name="day_26">
            <label for="box-26">26</label>

            <input type="checkbox" id="box-27" value="1" name="day_27">
            <label for="box-27">27</label>

            <input type="checkbox" id="box-28" value="1" name="day_28">
            <label for="box-28">28</label>

            <input type="checkbox" id="box-29" value="1" name="day_29">
            <label for="box-29">29</label>

            <input type="checkbox" id="box-30" value="1" name="day_30">
            <label for="box-30">30</label>



          </div>

        </div>
        <div style="float:left;">
          <div class="boxes">
            <input type="checkbox" id="box-31" value="1" name="day_31">
            <label for="box-31">31</label>
          </div>
        </div>


      <div style="clear:both;">

      </div>

    </div>
    @endforelse

    <?php $result = array_diff($arraymonth, $m);  ?>
    <?php //dd($result); ?>
    @for($mo = 1;$mo<14; $mo++)
    <?php //dd($mo); ?>
    @if(isset($result[$mo]))
    <div class="row"  style="display:none;"id="{{$mo}}" class="cal">

        <div style="float:left;">
          <div class="boxes">
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_1]">
            <label for="box-{{$i}}">1</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_2]" >
            <label for="box-{{$i}}">2</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_3]" >
            <label for="box-{{$i}}">3</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_4]" >
            <label for="box-{{$i}}">4</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_5]" >
            <label for="box-{{$i}}">5</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_6]" >
            <label for="box-{{$i}}">6</label>
          </div>

        </div>
        <div style="float:left;">
          <div class="boxes">

            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_7]" >
            <label for="box-{{$i}}">7</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_8]" >
            <label for="box-{{$i}}">8</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_9]" >
            <label for="box-{{$i}}">9</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_10]" >
            <label for="box-{{$i}}">10</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_11]" >
            <label for="box-{{$i}}">11</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_12]" >
            <label for="box-{{$i}}">12</label>
          </div>

        </div>
        <div style="float:left;">
          <div class="boxes">

            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_13]">
            <label for="box-{{$i}}">13</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_14]" >
            <label for="box-{{$i}}">14</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_15]" >
            <label for="box-{{$i}}">15</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_16]" >
            <label for="box-{{$i}}">16</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_17]" >
            <label for="box-{{$i}}">17</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_18]">
            <label for="box-{{$i}}">18</label>
          </div>

        </div>
        <div style="float:left;">
          <div class="boxes">

            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_19]">
            <label for="box-{{$i}}">19</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_20]">
            <label for="box-{{$i}}">20</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_21]">
            <label for="box-{{$i}}">21</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_22]" >
            <label for="box-{{$i}}">22</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_23]">
            <label for="box-{{$i}}">23</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_24]">
            <label for="box-{{$i}}">24</label>


          </div>
        </div>



        <div style="float:left;">
          <div class="boxes">
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_25]">
            <label for="box-{{$i}}">25</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_26]">
            <label for="box-{{$i}}">26</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_27]">
            <label for="box-{{$i}}">27</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_28]">
            <label for="box-{{$i}}">28</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_29]">
            <label for="box-{{$i}}">29</label>
            <?php $i++; ?>
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_30]">
            <label for="box-{{$i}}">30</label>
            <?php $i++; ?>


          </div>

        </div>
        <div style="float:left;">
          <div class="boxes">
            <input type="checkbox" id="box-{{$i}}" value="1" name="{{$mo}}[day_31]">
            <label for="box-{{$i}}">31</label>
          </div>
        </div>


      <div style="clear:both;">

      </div>

    </div>
    @endif
    @endfor


    <input type="hidden" name="created_by" value="{{Auth::id()}}">
    <input type="hidden" name="id" value="{{$id}}">
    <hr>
    <input type="submit" class="btn btn-primary"  value="Сохранить" id="button">

  </form>
  <div id="loader" style="display:none; ">


  <h3 style="color:rgb(21,157,244);">Обновляется подождите...</h3>
  <img src="{{asset('public/img/giphy.gif')}}" alt="">
  </div>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#button').click(function(){
        $('#loader').show();
      });
    });
  </script>
  @if(isset($error))
  <h3>К сожалению пользователь не найден</h3>
  @endif
</div>
<script type="text/javascript">

$(document).ready(function() {
    $('#zapros').val('{{$nownonth}}');
    $('#zapros').on('change', function() {
        console.log('click');
        if($(this).val() == '1') {
            $('#1').show();
            $('#2').hide();
            $('#3').hide();
            $('#4').hide();
            $('#5').hide();
            $('#6').hide();
            $('#7').hide();
            $('#8').hide();
            $('#9').hide();
            $('#10').hide();
            $('#11').hide();
            $('#12').hide();
        }
        else if ($(this).val() == '2') {
          $('#1').hide();
          $('#2').show();
          $('#3').hide();
          $('#4').hide();
          $('#5').hide();
          $('#6').hide();
          $('#7').hide();
          $('#8').hide();
          $('#9').hide();
          $('#10').hide();
          $('#11').hide();
          $('#12').hide();
        }
        else if ($(this).val() == '3') {
          $('#1').hide();
          $('#2').hide();
          $('#3').show();
          $('#4').hide();
          $('#5').hide();
          $('#6').hide();
          $('#7').hide();
          $('#8').hide();
          $('#9').hide();
          $('#10').hide();
          $('#11').hide();
          $('#12').hide();
        }
        else if ($(this).val() == '4') {
          $('#1').hide();
          $('#2').hide();
          $('#3').hide();
          $('#4').show();
          $('#5').hide();
          $('#6').hide();
          $('#7').hide();
          $('#8').hide();
          $('#9').hide();
          $('#10').hide();
          $('#11').hide();
          $('#12').hide();
        }
        else if ($(this).val() == '5') {
          $('#1').hide();
          $('#2').hide();
          $('#3').hide();
          $('#4').hide();
          $('#5').show();
          $('#6').hide();
          $('#7').hide();
          $('#8').hide();
          $('#9').hide();
          $('#10').hide();
          $('#11').hide();
          $('#12').hide();
        }
        else if ($(this).val() == '6') {
          $('#1').hide();
          $('#2').hide();
          $('#3').hide();
          $('#4').hide();
          $('#5').hide();
          $('#6').show();
          $('#7').hide();
          $('#8').hide();
          $('#9').hide();
          $('#10').hide();
          $('#11').hide();
          $('#12').hide();
        }
        else if ($(this).val() == '7') {
          $('#1').hide();
          $('#2').hide();
          $('#3').hide();
          $('#4').hide();
          $('#5').hide();
          $('#6').hide();
          $('#7').show();
          $('#8').hide();
          $('#9').hide();
          $('#10').hide();
          $('#11').hide();
          $('#12').hide();
        }
        else if ($(this).val() == '8') {
          $('#1').hide();
          $('#2').hide();
          $('#3').hide();
          $('#4').hide();
          $('#5').hide();
          $('#6').hide();
          $('#7').hide();
          $('#8').show();
          $('#9').hide();
          $('#10').hide();
          $('#11').hide();
          $('#12').hide();
        }
        else if ($(this).val() == '9') {
          $('#1').hide();
          $('#2').hide();
          $('#3').hide();
          $('#4').hide();
          $('#5').hide();
          $('#6').hide();
          $('#7').hide();
          $('#8').hide();
          $('#9').show();
          $('#10').hide();
          $('#11').hide();
          $('#12').hide();
        }
        else if ($(this).val() == '10') {
          $('#1').hide();
          $('#2').hide();
          $('#3').hide();
          $('#4').hide();
          $('#5').hide();
          $('#6').hide();
          $('#7').hide();
          $('#8').hide();
          $('#9').hide();
          $('#10').show();
          $('#11').hide();
          $('#12').hide();
        }
        else if ($(this).val() == '11') {
          $('#1').hide();
          $('#2').hide();
          $('#3').hide();
          $('#4').hide();
          $('#5').hide();
          $('#6').hide();
          $('#7').hide();
          $('#8').hide();
          $('#9').hide();
          $('#10').hide();
          $('#11').show();
          $('#12').hide();
        }
        else if ($(this).val() == '12') {
          $('#1').hide();
          $('#2').hide();
          $('#3').hide();
          $('#4').hide();
          $('#5').hide();
          $('#6').hide();
          $('#7').hide();
          $('#8').hide();
          $('#9').hide();
          $('#10').hide();
          $('#11').hide();
          $('#12').show();
        }
    });
});
</script>

@endsection
