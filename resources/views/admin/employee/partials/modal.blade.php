<div class="modal fade" id="exampleModalLong{{$u->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content vm-goods">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="content">
                        <style>
                            .vm-header .name{font-size:16px;font-weight:600;padding-bottom:20px;border-bottom:1px solid #ddd;margin-bottom:20px}
                            .modal_edit .perent{width:100%;display:table;margin-bottom:20px}
                            .modal_edit .perent .left{float:left;width:30%;padding-right:20px}
                            .modal_edit .perent .item_name{line-height:32px;margin-bottom: 0}
                            .modal_edit .perent .center{width:70%;float:left}
                            .modal_edit .perent input{background-color:#fff;width:100%;height:32px;border:1px solid #ccc;font-size:14px;font-weight:400;border-radius:3px;color:#222;padding-left:5px}
                            .modal_edit .perent input.user_name{width:48%;float:left;margin-right:2%;padding-right:20px}
                            .modal_edit .perent input.sex{width:30%;margin-right:1%;text-align:center;float:left}
                            .radio_item{width:35%;float:left;margin-right:1%;margin-bottom: 0}
                            .radio_box{background-color:#fff;height:32px;border:1px solid #ccc;font-size:14px;text-align:center;line-height:32px;vertical-align:middle;font-weight:400;border-radius:3px;color:#222;padding-left:5px}
                            .radio_item input{display:none}
                            .radio_item input.sex:checked + .radio_box{background-color:#2196F3;color:#fff}
                            .modal_edit .perent .type{display:table;width:100%;margin-bottom:10px}
                            .modal_edit .perent .type .left{width:5%;float:left;padding:0}
                            .modal_edit .perent .type .left input{width:14px;height:14px}
                            .modal_edit .perent .type .right{width:95%;padding-left:15px;float:left}
                            .modal_edit .perent .type .right .position{font-size:14px;line-height: 16px;font-weight:600;color:#0095eb}
                            .modal_edit .perent .type .right .info{color:#888;line-height:15px;font-size:13px;margin-top:3px;font-weight:400}
                        </style>
                        <form class="" action="{{route('admin.employee.update', $u)}}" method="post" enctype="multipart/form-data">
                          <input type="hidden" name="_method" value="put">
                              {{ csrf_field() }}
                        <div class="vm-goods modal_edit">
                            <div class="vm-header">
                                <div class="name">Редактирование сотрудника: {{$u->name or ''}}</div>
                            </div>
                            @if(Auth::user()->userRolle <= 2)
                            <div class="vm-body">
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Имя и фамилия:</label></div>
                                    <div class="center">
                                        <input type="text" name="name" class="user_name" placeholder="Имя" value="{{$u->name or 'Не заполнено'}}" required="">
                                        <input type="text" name="lastname" class="user_name" placeholder="Фамилия" value="{{$u->lastname or ''}}" required="">
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Пол:</label></div>
                                    <div class="center">
                                        <label class="radio_item">
                                            <input type="radio" id="user_sex" name="user_sex" class="sex" placeholder="Мужской" value="0" @if($u->sex == 0) checked @endif>
                                            <div class="radio_box">Мужской</div>
                                        </label>
                                        <label class="radio_item">
                                            <input type="radio" id="user_sex" name="user_sex" class="sex" placeholder="Женский" value="1" @if($u->sex == 1) checked @endif>
                                            <div class="radio_box">Женский</div>
                                        </label>
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Email:</label></div>
                                    <div class="center">
                                        <input type="text" name="email" class="" placeholder="email@company.ru" value="{{$u->email or 'Не заполнен'}}" required="">
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Телефон:</label></div>
                                    <div class="center">
                                        <input type="text" name="tel" class="" placeholder="+7 (___) ___-__-__" value="{{$u->tel or 'Не заполнен'}}" required="">
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Уровень полномочий:</label></div>
                                    <div class="center">
                                        <div class="row_item">
                                            <label class="type">
                                                <div class="left"><input type="radio" name="userRolle" value="4" @if($u->userRolle == 4) checked @endif></div>
                                                <div class="right">
                                                    <div class="position">Менеджер</div>
                                                    <div class="info">Прием, обработка заказов</div>
                                                </div>
                                            </label>
                                            <label class="type">
                                                <div class="left"><input type="radio" name="userRolle" value="3" @if($u->userRolle == 3) checked @endif></div>
                                                <div class="right">
                                                    <div class="position">Редактор</div>
                                                    <div class="info">Может добавлять, редактировать, удалять категории, товары и другое содержимое сайтом.</div>
                                                </div>
                                            </label>
                                            <label class="type">
                                                <div class="left"><input type="radio" name="userRolle" value="2" @if($u->userRolle == 2) checked @endif></div>
                                                <div class="right">
                                                    <div class="position">Администратор</div>
                                                    <div class="info">Может управвлять всем содержимым на сайте, изменять настройки, добавлять и удалять сотрудников</div>
                                                </div>
                                            </label>
                                            <label class="type">
                                                <div class="left"><input type="radio" name="userRolle" value="6"></div>
                                                <div class="right">
                                                    <div class="position">Арендатор</div>
                                                    <div class="info">Для людей, которые арендовали сайт</div>
                                                </div>
                                            </label>
                                            @if($u->userRolle == 1)
                                                <label class="type">
                                                    <div class="left"><input type="radio" name="userRolle" value="1" @if($u->userRolle == 1) checked @endif></div>
                                                    <div class="right">
                                                        <div class="position">Разрабочик</div>
                                                        <div class="info">Режим бога, управление всеми сайтами</div>
                                                    </div>
                                                </label>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Должность:</label></div>
                                    <div class="center"><input type="text" name="job" class="" placeholder="Например: менеджер" value="{{$u->job or 'Не заполнено'}}"></div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Фото:</label></div>
                                    <div class="center"><input type="file" name="image"></div>
                                </div>
                                <div class="perent" style="margin-bottom: 0">
                                    <div class="left"><label for="" class="item_name"></label></div>
                                    <div class="center">
                                        <input class="button-save" type="submit" value="Обновить" style="width: 115px;background-color: #269ffd;color: #fff;">
                                    </div>
                                </div>
                            </div>
                            @else
                            Доступ закрыт
                            @endif
                        </div>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
