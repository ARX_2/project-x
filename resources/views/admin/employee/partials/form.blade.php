

<label for="">Имя</label>
<input type="text" name="name" class="form-control" placeholder="Надежда"
value="{{$User->name or " "}}">

<label for="">Email</label>
<input type="text" name="email" class="form-control" placeholder="+7 900 800 60 70"
value="{{$User->email or " "}}">

<label for="">Пароль</label>
<input type="text" name="password" class="form-control" placeholder="123456">

<label for="">Должность</label>
<select class="form-control" name="userRolle">
  @if(isset($User->id))
  <option value="3" @if($User->userRolle == 3) selected="" @endif>Администратор</option>
  <option value="4" @if($User->userRolle == 4) selected="" @endif>Мастер</option>
  @else
  <option value="3">Администратор</option>
  <option value="4">Мастер</option>
  @endif
</select>
<label for="">Телефон</label>
<input type="text" name="tel" class="form-control" value="{{$User->tel or ''}}">
<label for="">Вывести в форму обратной связи?</label><br>
@if(isset($User))
<label for=""><input type="radio" name="published" value="1" class="radio2" @if($User->published == 1) checked @endif > Да</label><br>
<label for=""><input type="radio" name="published" value="0"  class="radio1" @if($User->published == 0) checked  @endif > Нет</label><br>
@else
<label for=""><input type="radio" name="published" value="1" class="radio2"  > Да</label><br>
<label for=""><input type="radio" name="published" value="0"  class="radio1" checked> Нет</label><br>
@endif
<div class="readonly">
  <label for="">Заголовок</label>
  <input type="text" name="title" value="{{$User->title or " "}}" class="form-control" maxlength="70"  >
  <label for="" class="readonly">Описание</label>
  <textarea name="description" class="form-control" maxlength="170" >{{$User->description or " "}}</textarea>

</div>

<br>

@isset($User->image)


<div style="float:left; margin: 0 20px; ">
  <div style="background-image:url('{{asset('storage/app')}}/{{$User->image}}'); background-repeat: no-repeat;background-size: 50%; width:300px; height:200px;">
      {{csrf_field()}}
            <a href="{{route('admin.image.employee.destroy',  ['image'=> $User->image, 'editurl' => $User->id])}}" class="btn" style="background-color:#dedbdb;"><i class="fa fa-trash-o"></i></a>



  </div>

</div>

<div style="clear:both;">

</div>
<br>
@endisset

<label for="">Аватар</label>
<br>
<input type="file" name="image">
<hr>
<input type="submit" class="btn btn-primary"  value="Сохранить">
<script type="text/javascript">
$(document).ready(function(){
  $('.radio1').on('change', function(){
 if($('.radio1').prop('checked')){
  $('.readonly input').prop('readonly', true);
  $('.readonly textarea').prop('readonly', true);
  $('.readonly label').attr('style', 'color:#ccc');
 }
 });

 $('.radio2').on('change', function(){
if($('.radio2').prop('checked')){
 $('.readonly input').prop('readonly', false);
 $('.readonly textarea').prop('readonly', false);
 $('.readonly label').attr('style', 'color:black');
}
});
if ($('.radio1').prop('checked')) {
  $('.readonly input').prop('readonly', true);
  $('.readonly textarea').prop('readonly', true);
  $('.readonly label').attr('style', 'color:#ccc');
}
});


</script>
