

<style>
    .c_p_company_biz{    margin-top: 40px;}
    .biz_box{width: 100%;margin: -5px;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flex;
        display: flex;
        -webkit-flex-wrap: wrap;
        -moz-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;margin-bottom: 40px;}
    .biz_box .biz_title{width: 100%;
        display: table;
        margin: 0 5px 5px;
        font-size: 16px;
        font-weight: 600;}
    .biz .item{    background: #fff;
        width: 250px;
        float: left;
        position: relative;
        padding-bottom: 80px;
        margin: 5px;
        border: 1px solid #e6e6e6;}
    .biz .item .info{padding: 15px}
    .biz .item .biz_name{     font-size: 16px;
        font-weight: 600;
        line-height: 18px;
        margin-bottom: 10px;}
    .biz .item .offer{    font-size: 12px;
        line-height: 16px;
        font-weight: 600;}
    .act{padding: 30px 15px 20px;border-top: 1px solid #ddd;
        display: flex;
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;background: #f8f9fc;}
    .act .price{font-size: 13px;line-height: 15px;
        font-weight: 600;    display: inline-block;
        width: 100%;    margin: auto 0;}
    .act .price .old_price{display: block;
        width: 100%;
        font-weight: 400;
        color: #888;
        text-decoration: line-through;}
    .act .biz_button{display: inline-block;margin: auto 0;}
    .act .biz_i_button{    position: relative;
        overflow: hidden;
        width: 69px;
        height: 27px;
        font-size: 13px;
        background: #249ffe;
        border: none;
        border-radius: 3px;
        padding: 4px;
        color: #fff;
        text-align: center;}
</style>
<div class="c_p_company_biz">
    <div class="biz">
        <div class="biz_box">
            <div class="biz_title">Реклама и продвижение сайта</div>
            <div class="item">
                <div class="image"><img src="https://komandor-tyumen.ru/storage/app/imageResize/public/5cfa7b6edcb57_400x270.webp" alt=""></div>
                <div class="info">
                    <div class="biz_name">SEO продвижение </div>
                    <div class="offer">Поисковое продвижение в поиске Яндекс и Google</div>
                </div>
                <div class="act">
                    <div class="price"><span class="old_price">40 000 руб</span>от 10 000 руб</div>
                    <div class="biz_button"><input type="submit" class="biz_i_button" value="Купить"></div>
                </div>
            </div>
            <div class="item">
                <div class="image"><img src="https://komandor-tyumen.ru/storage/app/imageResize/public/5cfa7b6edcb57_400x270.webp" alt=""></div>
                <div class="info">
                    <div class="biz_name">ЯндексДирект</div>
                    <div class="offer">Создание и поддержка рекламной компании в Яндексе</div>
                </div>
                <div class="act">
                    <div class="price"><span class="old_price">20 000 руб</span>от 10 000 руб</div>
                    <div class="biz_button"><input type="submit" class="biz_i_button" value="Купить"></div>
                </div>
            </div>
            <div class="item">
                <div class="image"><img src="https://komandor-tyumen.ru/storage/app/imageResize/public/5cfa7b6edcb57_400x270.webp" alt=""></div>
                <div class="info">
                    <div class="biz_name">Google Adwords</div>
                    <div class="offer">Создание и поддержка рекламной компании в Google</div>
                </div>
                <div class="act">
                    <div class="price"><span class="old_price">20 000 руб</span>от 10 000 руб</div>
                    <div class="biz_button"><input type="submit" class="biz_i_button" value="Купить"></div>
                </div>
            </div>
        </div>
        <div class="biz_box">
            <div class="biz_title">Социальные сети</div>
            <div class="item">
                <div class="image"><img src="https://komandor-tyumen.ru/storage/app/imageResize/public/5cfa7b6edcb57_400x270.webp" alt=""></div>
                <div class="info">
                    <div class="biz_name">Инстаграм</div>
                    <div class="offer">Создание, дизайн, заполнение продвижение вашего бизнеса в Инстаграм</div>
                </div>
                <div class="act">
                    <div class="price"><span class="old_price">20 000 руб</span>от 10 000 руб</div>
                    <div class="biz_button"><input type="submit" class="biz_i_button" value="Купить"></div>
                </div>
            </div>
            <div class="item">
                <div class="image"><img src="https://komandor-tyumen.ru/storage/app/imageResize/public/5cfa7b6edcb57_400x270.webp" alt=""></div>
                <div class="info">
                    <div class="biz_name">Вконтакте</div>
                    <div class="offer">Создание, оформление, ведение групп Вконтакте. Продвижение групп в поиске</div>
                </div>
                <div class="act">
                    <div class="price">от 10 000 руб</div>
                    <div class="biz_button"><input type="submit" class="biz_i_button" value="Купить"></div>
                </div>
            </div>
            <div class="item">
                <div class="image"><img src="https://komandor-tyumen.ru/storage/app/imageResize/public/5cfa7b6edcb57_400x270.webp" alt=""></div>
                <div class="info">
                    <div class="biz_name">Facebook</div>
                    <div class="offer">Создание, оформление, ведение сообществ Facebook. Рекламные компании Facebook</div>
                </div>
                <div class="act">
                    <div class="price">от 10 000 руб</div>
                    <div class="biz_button"><input type="submit" class="biz_i_button" value="Купить"></div>
                </div>
            </div>
            <div class="item">
                <div class="image"><img src="https://komandor-tyumen.ru/storage/app/imageResize/public/5cfa7b6edcb57_400x270.webp" alt=""></div>
                <div class="info">
                    <div class="biz_name">Ютуб</div>
                    <div class="offer">Создание, оформление, монтаж видео. Продвижение видео в ЮТУБ</div>
                </div>
                <div class="act">
                    <div class="price">от 10 000 руб</div>
                    <div class="biz_button"><input type="submit" class="biz_i_button" value="Купить"></div>
                </div>
            </div>
        </div>
        <div class="biz_box">
            <div class="biz_title">Помощь по сайту</div>
            <div class="item">
                <div class="image"><img src="https://komandor-tyumen.ru/storage/app/imageResize/public/5cfa7b6edcb57_400x270.webp" alt=""></div>
                <div class="info">
                    <div class="biz_name">Текста на сайт</div>
                    <div class="offer">Качественное написание текстов для заполнения сайта</div>
                </div>
                <div class="act">
                    <div class="price">от 300 руб</div>
                    <div class="biz_button"><input type="submit" class="biz_i_button" value="Купить"></div>
                </div>
            </div>
            <div class="item biz_modal" slug="112">
                <div class="image"><img src="https://komandor-tyumen.ru/storage/app/imageResize/public/5cfa7b6edcb57_400x270.webp" alt=""></div>
                <div class="info">
                    <div class="biz_name">Дизайн: обложки</div>
                    <div class="offer">Дизайн обложек для категорий</div>
                </div>
                <div class="act">
                    <div class="price">от 250 руб</div>
                    <div class="biz_button"><input type="submit" class="biz_i_button" value="Купить"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    // .image - отвечает за класс на который надо нажимать, чтоб сработал ajax
    $('.biz_modal').click(function(){
        var id = $(this).attr('slug');
        $.get('/admin/biz/modal/'+id, function(data){
            $('.modal_biz').html(data);
        });
    });
</script>
<div class="modal_biz"></div>

<style>
    .modal-ui-wrapper{top:0;left:0;right:0;bottom:0;position:fixed;width:100%;background:#fff;justify-content:center;align-items:center;z-index:2500;overflow-x:hidden;overflow:auto;display:flex}
    .modal-ui-modal{min-width:320px;width:900px;min-height:150px;max-height:none;overflow-x:hidden;overflow-y:auto;padding:20px 0;justify-content:center;align-items:center;position:relative;display:table}
    .modal-ui-content{width:100%;height:100%;z-index:10;display:block}
    .modal-ui-wrapper{align-items:normal}
    .modal-ui-modal{margin:40px 0}
    .modal-ui-close{top:-7px;right:10px;color:#222;font-size:50px;font-size:60px;line-height:60px;font-weight:700;display:table;position:absolute;color:#fff;top:0;right:-50px;z-index:9;cursor:pointer}
    .modal-ui-close span{display:block}
    .modal-ui-close{top:-7px;right:10px;color:#222;font-size:50px}
    .modal-ui-content .title{padding:0;width:100%;margin-bottom:20px}
    .modal-ui-content .title h2{font-weight:600;font-size:18px;line-height:18px;color:#444;margin:0 0 9px;width:90%}
    .modal-ui-content .title span{display: block}
    .modal-ui-content .title span a{text-decoration: underline;}

</style>

<div id="modal_form" class="modal-ui-wrapper" style="background: rgba(0, 0, 0, 0.565);">
    <div class="modal-ui-modal" style="background: rgb(255, 255, 255);padding: 0">
        <div class="modal-ui-content tz-gallery" style="padding: 20px">
            <div class="title"><h2>Очень длинное название услуги, которое не влезет в блок услуги</h2></div>
            <style>
                .biz_m_c{display: table;width: 100%}
                .biz_m_c .big_image{width: 100%;height: 300px;display: table}
                .biz_m_c .big_image img{    width: 100%;
                    height: 100%;
                    object-fit: contain;}
                .biz_offer{margin-top: 20px;
                    font-size: 15px;
                    line-height: 20px;}
                 .biz_tizer{width: 100%;
                     display: table;
                     margin-top: 20px;
                     text-align: center;}
                .biz_tizer .item{width: 24.5%;
                    display: inline-block;
                    vertical-align: middle;
                    float: none;
                    padding: 0px 1%;}
                .biz_tizer .image{width: 50px;
                    height: 50px;
                    min-width: 50px;
                    margin: auto 0;
                    display: table-cell;
                    vertical-align: middle;}
                .biz_tizer .image img{width: 100%;
                    height: 100%;
                    object-fit: cover;}
                .biz_tizer .info{margin-left: 10px;
                    margin: auto 0;
                    display: table-cell;
                    vertical-align: middle;
                    text-align: left;
                    padding-left: 10px;}
                .biz_tizer .info .name{font-size: 14px;
                    line-height: 14px;
                    font-weight: 600;
                    color: #444;}
                .biz_tizer .info .desc{font-size: 13px;
                    line-height: 13px;
                    margin-top: 5px;}
            </style>
            <div class="biz_m_c">
                <div class="big_image" style="background: linear-gradient(rgb(247, 142, 157), rgb(234, 115, 134));"><img src="https://maxst.icons8.com/vue-static/landings/primary-landings/banners/photo-creator@1.5.png" alt=""></div>

                <div class="biz_tizer">

                    <div class="item">
                        <div class="image">
                            <img src="https://img.icons8.com/material/4ac144/256/twitter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="name">Название тизера</div>
                            <div class="desc">Длинное описание тизера описание тизера</div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img src="https://img.icons8.com/material/4ac144/256/twitter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="name">Еще одно название</div>
                            <div class="desc">После проверки лишний </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img src="https://img.icons8.com/material/4ac144/256/twitter.png" alt="">
                        </div>
                        <div class="info">
                            <div class="name">Тут текст</div>
                            <div class="desc">На постоянной основе мы не просим, проверка</div>
                        </div>
                    </div>
                </div>
                <div class="biz_offer">Пользоваться другим браузером на постоянной основе мы не просим, проверка нужна исключительно для локализации проблемы. После проверки лишний браузер можно удалить. Пользоваться другим браузером на постоянной основе мы не просим, проверка нужна исключительно для локализации проблемы. После проверки лишний браузер можно удалить.</div>
            </div>
        </div>
        <div id="modal_close" class="modal-ui-close"><span>×</span></div>
    </div>
</div>
<script type="text/javascript">
    $('#modal_close, #overlay').click( function(){
        $('#modal_form')
            .animate({opacity: 0}, 100,
                function(){
                    $(this).css('display', 'none');
                    $('#overlay').fadeOut(100);
                }
            );
    });
</script>
