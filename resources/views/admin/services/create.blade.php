      <div class="row">
        <div class="col-lg-4">
            <h2>Управление услугами</h2>
        </div>
        <div class="col-lg-8">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.index')}}">Главная</a></li>
                <li class="active">{{$company->info->section0 or 'Товары'}}</li>
            </ol>
        </div>
    </div>
    <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        {{--<div class="item">
                            <div class="name">
                                <a href=""></a>
                            </div>
                        </div>--}}

                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="cp_edit">
                        <div class="narrow_title">
                            @if($product == null)
                            <h2>Добавление услуги</h2>
                            @else
                            <h2>Редактирование услуги: <span id="edit-title">{{$product->title}}</span></h2>
                            @endif

                        </div>
                        @if($product == null)
                        <form class="" action="{{route('admin.services.store')}}" method="post" enctype="multipart/form-data">
                        @else
                        <form  action="{{route('admin.service.update.id', ['update',$product->id,0])}}" method="post"  enctype="multipart/form-data">
                          <input type="hidden" name="_method" value="put">
                          <input type="hidden" name="id" value="{{$product->id}}">
                        @endif

                        {{ csrf_field() }}
                        <input type="hidden" name="slug" class="form-control" placeholder="Автоматическая генерация" value="{{$goods->slug or ""}}" readonly>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="name_item">Выберите категорию:</label>
                            </div>
                            <div class="center">
                                <style>
                                    .category_selection{width: 100%;    display: inline-flex;}
                                    .category_group{width: 50%;border: 1px solid #ddd;padding-bottom: 10px;    margin-left: -1px;}
                                    .category_group_title{line-height: 13px;font-size: 13px;padding: 10px;color: #888;}
                                    .category_group input[type="radio"] + span{border: 0;margin: 0;width: 1px;height: 1px}
                                    /* input[type="radio"]:checked + span:before {content: "";display: block;background: #fff;border-radius: 100%;width: 8px;height: 8px;margin: 2px;pointer-events: none;} */
                                    .c_action{display:block;width:100%;padding:4px 10px;margin:0;border:0;background-color:#fff;color:#555;text-align:left;line-height:16px;cursor:pointer}
                                    .c_action:hover{background-color:#cee9ff}
                                    .c_action.back{background-color:#269ffd}
                                    .c_action.back span{color:#fff}
                                    .s_action{display:block;width:100%;padding:4px 10px;margin:0;border:0;background-color:#fff;color:#555;text-align:left;line-height:16px;cursor:pointer}
                                    .s_action:hover{background-color:#cee9ff}
                                    .s_action.back{background-color:#269ffd}
                                    .s_action.back span{color:#fff}
                                </style>
                                <div class="category_selection">
                                    <div class="category_group cat">
                                        <div class="category_group_title">Категория</div>
                                        @forelse($categories as $c)
                                        <?php //dd($c); ?>
                                        @if($c->parent_id == null)
                                        <label class="c_action @if($product !== null && $product->subcategory !== null && $c->id == $product->subcategory->id || $product !== null && $product->subcategory !== null && $c->id == $product->subcategory->parent_id) back @endif" id="cat{{$c->id}}"><input type="radio" name="category" hidden="" value="{{$c->id}}" @if($product !== null && $product->subcategory !== null && $c->id == $product->subcategory->id) checked @endif><span></span><span class="radio-name">{{$c->title or 'Название категории'}}</span></label>
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('#cat{{$c->id}}').click(function(){
                                                  //alert('asd');
                                                    @if(count($c->subcategoriesAll)>0)
                                                    $('.s_cat').show();
                                                    @else
                                                    $('.s_cat').hide();
                                                    @endif
                                                    $("input[name='subcategory']").prop("checked", false);
                                                    $('.cat{{$c->id}}').removeClass('back');
                                                    $('.s_action').hide();
                                                    $('.cat{{$c->id}}').show();
                                                    $('#category_group_title').text('{{$c->title or ''}}');
                                                });
                                            });


                                        </script>

                                        @endif
                                        @empty
                                        Сперва нужно добавить категорию
                                        @endforelse

                                    </div>
                                    <?php //dd($product); ?>
                                    <div class="category_group s_cat" style="display: @if($product !== null && $product->subcategory !== null && $product->subcategory->parent_id !== null) block @else none @endif">
                                      <div class="category_group_title" id="category_group_title">Название на нажатую категорию</div>
                                      @forelse($categories as $c)
                                      @forelse($c->subcategoriesAll as $subcategory)

                                        @if($subcategory->published == 1)
                                        <?php //dd($product->subcategory);?>
                                      <label class="s_action cat{{$c->id}} @if($product !== null && $product->subcategory !== null && $subcategory->id == $product->subcategory->id) back @endif" style="display:@if($product !== null && $product->category !== null && $product->subcategory->parent_id == $c->id) block @else none @endif"><?php //dd($subcategory); ?> <input type="radio" name="subcategory" hidden="" class="inputcategory{{$c->id}}" @if($product !== null && $product->subcategory !== null && $subcategory->id == $product->subcategory->id) checked @endif value="{{$subcategory->id}}"><span></span><span class="radio-name">{{$subcategory->title or ''}}</span></label>
                                      @endif

                                      @empty
                                      @endforelse
                                      @empty
                                      @endforelse
                                    </div>
                                </div>
                                <script type="text/javascript">
                                $('.c_action, .s_action').click(function(){
                                  var cat = $(this).find('input').val();
                                  $.get('/admin/service/category/{{$product->id}}/'+cat,function(data){});
                                  return false;
                                });

                                </script>

                                <script>
                                    $('.cat .c_action').on("click", function(){
                                        $('.c_action').removeClass('back');
                                        $(this).addClass('back');
                                    });
                                    $('.s_cat .s_action').on("click", function(){
                                        $('.s_action').removeClass('back');
                                        $(this).addClass('back');
                                    });
                                </script>

                            </div>
                        </div>
                        <script type="text/javascript">
                        $('select[name="subcategory"]').on('change',function(){
                          $.get('/admin/service/category/{{$product->id}}/'+$(this).val(), function(data){});
                        });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="seo_name">SEO</label>
                            </div>
                            <div class="center">
                                <label class="switch">
                                    <input type="checkbox" id="watch" name="checkseo" value="1" @if($product !== null)@if($product->checkseo == 1)checked @endif @endif>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="seo_group" id="switch_seo" style="display:none">
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name" style="padding-top: 4px">Название на странице товара или услуги:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seoname" class="" placeholder="Название на странице категории" value="{{$product->seoname or ''}}" >
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('input[name="seoname"]')
                            .on({
                              blur: function() {
                                $.get('/admin/goods/seoname/{{$product->id}}/'+this.value);
                              }
                            });
                            </script>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Title:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seotitle" class="" placeholder="" value="{{$product->seotitle or ''}}" >
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('input[name="seotitle"]')
                            .on({
                              blur: function() {
                                $.get('/admin/goods/seotitle/{{$product->id}}/'+this.value);
                              }
                            });
                            </script>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Description:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seodescription" class="" placeholder="" value="{{$product->seodescription or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('input[name="seodescription"]')
                            .on({
                              blur: function() {
                                $.get('/admin/goods/seodescription/{{$product->id}}/'+this.value);
                              }
                            });
                            </script>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Keywords:</label>
                                </div>
                                <div class="center">
                                  <input type="text" name="seokeywords" class="" placeholder="" value="{{$product->seokeywords or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('input[name="seokeywords"]')
                            .on({
                              blur: function() {
                                $.get('/admin/goods/seokeywords/{{$product->id}}/'+this.value);
                              }
                            });
                            </script>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Название товара или услуги:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="title" class="" placeholder="Введите название товара или услуги" value="{{$product->title or ''}}" required="">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('input[name="title"]').on('input',function(){
                          var title = $(this).val();
                          $('#edit-title').text(title);
                        });
                        $('input[name="title"]')
                        .on({
                          blur: function() {
                            $.get('/admin/goods/title/{{$product->id}}/'+this.value);
                          }
                        });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Описание:</label>
                            </div>
                            <div class="center">
                                <textarea rows="10" name="short_description" id="short_description">{!! $product->short_description or '' !!}</textarea>
                                <script>
	                    var myEditor;
                            ClassicEditor
                                .create( document.querySelector( '#short_description' ) )
                                .then( editor => {
                                myEditor = editor;
                                    } )
                                .catch( err => {
                                } );
                        </script>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Стоимость:</label>
                            </div>
                            <style>

                            </style>
                            <div class="center price">
                                <input type="text" name="coast" class=" a-price" placeholder="0" value="{{$product->coast or ''}}" data-mask="000 000 000" data-mask-reverse="true" data-mask-maxlength="false">
                                <div class="input-part">
                                        <span>
                                            <span class="number-format-font">₽</span>
                                        </span>
                                </div>

                            </div>
                        </div>
                        <script type="text/javascript">
                        $('input[name="coast"]')
                        .on({
                          blur: function() {
                            $.get('/admin/service/coast/{{$product->id}}/'+this.value);
                          }
                        });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Фотографии:</label>
                            </div>
                            <div class="center">
                                <div class="file-upload">
                                    <label>
                                        <input id="sortpicture" type="file" name="sortpic[]" multiple />
                                        <span>Выбрать файл</span>
                                    </label>
                                </div>
                                <input type="text" id="filename" class="filename" disabled style="border: none;font-size: 12px;">
                                <div class="imgs">
                                @if($product !== null)
                                <div id="list">
                                <div id="response"> </div>
                                <div id="sort">
                                @forelse($product->imageses as $image)
                                <div class="image_group" id="arrayorder_{{$image->id}}" style="width:110px; float:left;">
                                    <div class="image">
                                        <img src="{{asset('storage/app')}}/{{$image->images or ''}}" alt="">
                                        <a  class="delete" id="send{{$image->id}}"></a>
                                    </div>
                                </div>
                                <script>

                                ajaxForm();
                                function ajaxForm(){
                                  $('#send{{$image->id}}').click(function(){
                                    var data = new FormData($("#form")[0]);
                                      $.ajax({
                                          type: 'GET',
                                          url: "{{route('admin.image.goods.destroy')}}?id={{$image->id}}",
                                          data: data,
                                          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                          contentType: false,
                                          processData : false,
                                          success: function(data) {
                                              $('#arrayorder_{{$image->id}}').remove();
                                          }
                                      });
                                      return false;
                                  });
                                }

                        </script>
                                @empty
                                @endforelse
                              </div>
                                </div>
                                <div class="clearfix"></div>
                                @endif
                              </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('#sortpicture').on('change',function() {
                          //alert('asd');
                          var i = 1;
                          var files = $('#sortpicture').prop('files');
                          $.each(files, function (index, value) {
                            var file_data = value;
                            var form_data = new FormData();
                            form_data.append('file', file_data);
                            form_data.append('id', {{$product->articul}});
                            $.ajax({
                                        url: '/admin/image/upload',
                                        dataType: 'text',
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'post',
                                        success: function(data){
                                          data = $.parseJSON(data);
                                            $('.imgs').append('<div id="list"><div id="response"> </div><div id="sort"><div class="image_group" id="arrayorder_'+i+'" style="width:auto;float:left;">              <div class="image">                                                    <img src="{{asset('storage/app')}}/'+data.image+'" alt="">                                                    <a  class="delete" id="send'+i+'"></a>                                             </div>                                            </div></div></div>');
                                            $('#send'+i).click(function(){

                                              $.get('{{route('admin.image.goods.destroy')}}?id='+data.id,function(data){$('#arrayorder_'+i).remove();});
                                            });
                                        }
                             });
                             i++;
                          });

                        });

                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Видео с YouTube:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="youtube" class="" placeholder="Например: http://www.youtube.com/watch?v=9X-8JiOhzX4" value="{{$product->youtube or ''}}">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <div class="perent" style="margin-top: 40px;">
                            <div class="left">&nbsp;</div>
                            <div class="center">
                              @if(Auth::user()->userRolle == 1)
                               <input type="button" class="item_button saveall" value="Сохранить на всех сайтах">
                               @endif
                                <input type="button" class="item_button" value="Сохранить и вернуться">
                            </div>
                            <div class="right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
        @if(Auth::user()->userRolle == 1)
        <script type="text/javascript">
          $('.saveall').click(function(){
            var text = myEditor.getData();
            $.post('{{route('admin.change.title.sites.services')}}', {short_description:text, id:{{$product->id}}}, function(data){
              $('.admin-content').html(data);
            });
          });
        </script>
        @endif
        <script type="text/javascript">
        $('#refresh').click(function(){
          $.get('{{route('admin.services.index')}}', function(data){
            $('.admin-content').html(data);
          });
        });
        $('.item_button').click(function(){
          var text = myEditor.getData();
          $.post('/admin/service/update/description', {short_description:text, id:{{$product->id}}}, function(data){
            history.pushState(null, null, '/admin/services/');
            $('.admin-content').html(data);
          });
        });
        </script>

        <script>
            $(document).ready( function() {
                $(".file-upload input[type=file]").change(function(){var filename = $(this).val().replace(/.*\\/, "");$("#filename").val(filename);});
            });
        </script>
        <script>
            $(document).ready(function() {
                $('input[type="checkbox"]').click(function() {
                    if($(this).attr('id') == 'watch') {
                      var val = 0;
                        if (this.checked) {$('#switch_seo').show(); val = 1;}
                        else {$('#switch_seo').hide(); val = 0;}
                        $.get('/admin/goods/checkseo/{{$product->id}}/'+val);
                    }
                });
            });
        </script>
        @if($product !== null)
        @if($product->checkseo == 1)
        <script>
            $(document).ready(function() {$('#switch_seo').show();});
        </script>
        @endif
        @endif
        <script type="text/javascript">
        $(document).ready(function(){
        $(function() {
        $("#list #sort").sortable({ opacity: 0.8, cursor: 'move', update: function() {

        var order = $(this).sortable("serialize") + '&update=update';


        $.get("{{route('admin.images.sort')}}", order, function(theResponse){
        $("#response").html(theResponse);
        $("#response").slideDown('slow');
        slideout();
        });
        }
        });
        });
        });

        </script>
    <script type="text/javascript" src="/public/admin/js/jquery.mask.js"></script>
