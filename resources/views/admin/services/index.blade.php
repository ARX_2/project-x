      <div class="row">
        <div class="col-lg-4">
            <h2>Управление услугами</h2>
        </div>
        <div class="col-lg-8">
            <ol class="breadcrumb">
                <li><a href="">Главная</a></li>
                <li class="active">{{$company->info->section0 or 'Товары'}}</li>
            </ol>
        </div>
    </div>
    <hr>
        <div class="c_p_company">
            <div class="wide">
                <div class="box" style="position: fixed;">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        <div class="item">
                            <div class="name">
                                <a href="/admin/admin/company/upload?type=5">Описание на первой странице услуг</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="name">
                                <a href="" data-toggle="modal" data-target="#excelModalLong">Загрузка / выгрузка товаров и категорий</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="name">
                                <a href="" data-toggle="modal" data-target="#editsection">Переименовать раздел</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="name">
                                <span class="button-edit"><a href="{{route('admin.services.create')}}">Добавить</a></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="name">
                                <a href="" data-toggle="modal" data-target="#save">Сохраниться</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="narrow">
                <div class="box">
                    <div class="production">
                        <div class="narrow_title">
                            <div class="left">
                                <i class="fa fa-refresh" aria-hidden="true" style="float:left; font-size:20px;color:#14aae6;padding: 5px;border-radius: 50px;" id="refresh"></i>
                                <h2 style="float:left;margin-left:10px;margin-top:5px;">{{$company->info->section0 or 'Услуги'}}</h2>
                                <p class="clearFix"></p>
                                <style media="screen">
                                  #refresh:hover{
                                    background-color: #e7e6e6;
                                  }
                                </style>
                            </div>
                            <div class="right">
                                <span class="button-edit"><a href="{{route('admin.services.create')}}">Добавить</a></span>
                            </div>
                        </div>
                        <div class="sort">
                            <div class="page_cat">
                                <span><a href="{{$urlActive or ''}}" class="active" id="link_active">Активные <span id="count_active">{{$countActive or ''}}</span></a></span>
                                <span> | </span>
                                <span><a href="{{route('admin.service.hidden')}}" id="link_hidden">Скрытые <span id="count_hidden">{{$countHide or ''}}</span></a></span>
                                <span> | </span>
                                <span><a href="{{route('admin.service.ShowDeleted')}}" id="link_deleted">Удаленные <span id="count_deleted">{{$countDeleted or ''}}</span></a></span>
                            </div>
                            <div class="sort_block">
                                <div class="category">
                                    <div class="left">
                                        <div class="name">Сортировать по:</div>
                                    </div>
                                    <div class="right">
                                      <select class="sort_select" id=category>
                                          <option value="0" selected="">Все товары</option>
                                          @forelse($categories as $category)
                                          <option value="{{$category->id}}" style="font-weight:600;color:black">{{$category->title or ''}}</option>
                                          @forelse($category->subcategories as $sub)
                                          <option value="{{$sub->id}}">&nbsp;&nbsp;&nbsp;{{$sub->title or ''}}</option>
                                          @empty
                                          @endforelse
                                          @empty
                                          @endforelse
                                      </select>
                                    </div>
                                </div>
                                <!-- <div class="price">
                                    <div class="left">
                                        <div class="name">Цена:</div>
                                    </div>
                                    <div class="right">
                                        <input type="text" id="min_coast"   >
                                        <span> - </span>
                                        <input type="text" id='max_coast'  >
                                    </div>
                                </div>
                                <div class="sort_button">
                                    <a id="filtr" style="cursor:pointer"><div class="button">Применить</div></a>
                                </div> -->
                            </div>
                        </div>
                        <script type="text/javascript">

                        </script>
                        <script type="text/javascript">
                          $('#filtr').click(function(){

                            var category = $('#category').val();
                            $.post('{{route('admin.sort.services')}}',{id:category}, function(data){
                              $('.goodsSort').html(data);
                            });
                          });
                        </script>
                        <script type="text/javascript">
                        $('.sort_select').on('change', function() {
                          if($(this).val() == '0') {
                            $.get('{{route('admin.sort.services')}}?id=0',function(data){
                              $('.goodsSort').html(data);
                            });
                          }


                            $.get('{{route('admin.sort.services')}}?id='+$(this).val(),function(data){
                              $('.goodsSort').html(data);
                            });




                        });
                        </script>
                        <div class="goodsSort">


                        @include('admin.services.services', ['product' => $product])

                      </div>
                      @include('admin.layout.paginator', ['paginate' => $product])
                    </div>
                </div>
            </div>
        </div>

        <style>

        .prokrutka {
        height: 55vh;
        width: 100%; /* ширина нашего блока */
        background: #fff; /* цвет фона, белый */
        overflow-y: scroll; /* прокрутка по вертикали */
        }

    ::-webkit-scrollbar {
    width: 3px; height: 3px;
    }


    ::-webkit-scrollbar-track-piece  {
    background-color: #c7c7c7;
    }

    ::-webkit-scrollbar-thumb:vertical {
    height: 50px; background-color: #666; border-radius: 3px;
    }

    ::-webkit-scrollbar-thumb:horizontal {
    height: 50px; background-color: #666; border-radius: 3px;
    }

    ::-webkit-scrollbar-corner{
      background-color: #999;
    }
    .emoji{
      width: 18px !important; height:18px !important;
    }
        </style>



    <div class="modal fade" id="editsection" tabindex="-1" role="dialog" aria-labelledby="editsection" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content vm-goods">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="modal-body modal_edit">
                    <div class="content">
                        <div class="vm-header">
                            <div class="name">Переименовать раздел "Услуги"</div>
                        </div>
                        <div class="vm-body">
                            <div class="vm-edit">
                              <form class="" action="{{route('admin.change.section')}}" method="post">
                                {{ csrf_field()}}
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Название раздела:</label></div>
                                    <div class="center">
                                        <input type="text" name="services" class="" placeholder="Услуги" value="">
                                    </div>
                                </div>
                                <div class="perent" style="margin-bottom: 0">
                                    <div class="left"><label for="" class="item_name"></label></div>
                                    <div class="center">
                                        <input class="button-save" type="submit" value="Сохранить" style="width: 115px;background-color: #269ffd;color: #fff;">
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="save" tabindex="-1" role="dialog" aria-labelledby="editsection" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content vm-goods">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="modal-body modal_edit">
                    <div class="content">
                        <div class="vm-header">
                            <div class="name">Сохранение</div>
                        </div>
                        <div class="vm-body">
                            <div class="vm-edit">

                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Название:</label></div>
                                    <div class="center">
                                        <input type="text" name="saveTitle" class="" placeholder="Услуги" value="">
                                    </div>
                                </div>

                                <div class="perent" style="margin-bottom: 0">
                                    <div class="left"><label for="" class="item_name"></label></div>
                                    <div class="center">
                                        <input class="button-save" id="newSave" type="submit" value="Сохранить" style="width: 115px;background-color: #269ffd;color: #fff;">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
      $('#newSave').click(function(){
        var title = $('input[name="saveTitle"]').val();
        $.get('/admin/save/goods/{{Auth::user()->siteID}}/1?title='+title, function(data){
          alert('Сохранение успешно выполнено!');
        });
      });
    </script>
    <div class="modal fade" id="excelModalLong" tabindex="-1" role="dialog" aria-labelledby="excelModalLongTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content vm-goods">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <div class="modal-body">
                <div class="content">

                    <div class="vm-goods">
                        <div class="vm-header">
                            <div class="block">
                                <div class="psh">
                                    <div class="left">
                                        <div class="name">Управление кампанией с помощью XLSX</div>
                                    </div>
                                    <div class="right"></div>
                                </div>
                            </div>
                        </div>

                        <div class="vm-body">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Выгрузка товаров</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Загрузка товаров</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Обновить количество товаров</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab">Архив</a>
                                </li>
                            </ul><!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane" id="tabs-1" role="tabpanel">
                                    <p>Формат XLSX</p>
                                    <a href="#" id="excelExport" class="button-xlsx" style="padding: 3px 10px 5px;display: table; color: #222; background: #ffdb4d; border-radius: 3px;">Выгрузить</a>
                                    <i id="excelUrl"> <img src="{{asset('public/img')}}/giphy.gif" alt="" id="loader" style="display:none; width:50px;"> </i>
                                </div>
                                <div class="tab-pane active" id="tabs-2" role="tabpanel">
                                    <p>Формат XLSX</p>
                                    <form class="" action="{{route('admin.goods.uploadExcel')}}" method="post" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                      <input type="hidden" name="type" value="1" id="uploadExcel">
                                    <input type="file" name="file" style="margin-bottom: 15px;">
                                    <button type="submit" class="button-xlsx" style="border: none;padding: 3px 10px 5px;display: table; color: #222; background: #ffdb4d; border-radius: 3px;">Загрузить</button>
                                    </form>
                                </div>
                                <div class="tab-pane" id="tabs-3" role="tabpanel">
                                    <p>Формат XLSX</p>
                                    <form class="" action="{{route('admin.goods.uploadExcel')}}" method="post" enctype="multipart/form-data">
                                      {{ csrf_field() }}
                                    <input type="hidden" name="type" value="2" id="updateExcel">
                                    <input type="file" name="file" style="margin-bottom: 15px;">
                                    <button type="submith" class="button-xlsx" style="border: none;padding: 3px 10px 5px; display: table; color: #222; background: #ffdb4d; border-radius: 3px;">Обновить</button>
                                  </form>
                                </div>
                                <div class="tab-pane" id="tabs-4" role="tabpanel">
                                    <p>Формат XLSX</p>
                                    @forelse($excelUpload as $eu)
                                    <a href="/storage/app/excel/{{$eu->title}}.xlsx" style="margin-right:20px;color:rgb(87, 177, 228);">Выгрузка {{$eu->title}}</a><br>
                                    @empty
                                    <p>У вас нет выгрузок Excel</p>
                                    @endforelse
                                </div>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

    ajaxForm();
    function ajaxForm(){
      $('#excelExport').click(function(){
        $('#loader').show();
        var data = new FormData($("#form")[0]);
          $.ajax({
              type: 'GET',
              url: "{{route('admin.goods.excelExport')}}",
              data: data,
              headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
              contentType: false,
              processData : false,
              success: function(data) {
                  $('#excelUrl').html(data);
              }
          });
          return false;
      });
    }
    $('#refresh').click(function(){
      $('#refresh').addClass('rotate');
      $.get('{{$link or ''}}?ajax=true', function(data){
        $('.admin-content').html(data);
      });
    });

    $('#link_active').click(function(){

      var active_link = $(this).attr('href');
      $.get(active_link+'?ajax=true', function(data){
        $('.admin-content').html(data);
        $('#link_hidden').removeClass('active');
        $('#link_deleted').removeClass('active');
        $('#link_active').addClass('active');
      });
      history.pushState(null, null, active_link);
      return false;
    });
    $('#link_hidden').click(function(){

      var hidden_link = $(this).attr('href');
      $.get(hidden_link+'?ajax=true', function(data){
        $('.admin-content').html(data);
        $('#link_active').removeClass('active');
        $('#link_deleted').removeClass('active');
        $('#link_hidden').addClass('active');
      });
      history.pushState(null, null, hidden_link);
      return false;
    });
    $('#link_deleted').click(function(){

      var deleted_link = $(this).attr('href');
      $.get(deleted_link+'?ajax=true', function(data){
        $('.admin-content').html(data);
        $('#link_active').removeClass('active');
        $('#link_hidden').removeClass('active');
        $('#link_deleted').addClass('active');
      });
      history.pushState(null, null, deleted_link);
      return false;
    });

    $('.button-edit').click(function(){
      var link = $('.button-edit').find('a').attr('href');
        $.get(link+'?ajax=true', function(data){
          $('.admin-content').html(data);
        });
        history.pushState(null, null, link);
        return false;
    });

    $('.link_edit').click(function(){
      var link = $(this).attr('href');
        $.get(link+'&&ajax=true', function(data){
          $('.admin-content').html(data);
        });
        history.pushState(null, null, link);
        return false;
    });
    </script>

    <style media="screen">
      .rotate{
        -webkit-animation-name: cog;
-webkit-animation-duration: 1s;
-webkit-animation-iteration-count: infinite;
-webkit-animation-timing-function: linear;
-moz-animation-name: cog;
-moz-animation-duration: 1s;
-moz-animation-iteration-count: infinite;
-moz-animation-timing-function: linear;
-ms-animation-name: cog;
-ms-animation-duration: 1s;
-ms-animation-iteration-count: infinite;
-ms-animation-timing-function: linear;

animation-name: cog;
animation-duration: 1s;
animation-iteration-count: infinite;
animation-timing-function: linear;
}
@-ms-keyframes cog {
from { -ms-transform: rotate(0deg); }
to { -ms-transform: rotate(20deg); }
}
@-moz-keyframes cog {
from { -moz-transform: rotate(0deg); }
to { -moz-transform: rotate(20deg); }
}
@-webkit-keyframes cog {
from { -webkit-transform: rotate(0deg); }
to { -webkit-transform: rotate(20deg); }
}
@keyframes cog {
from {
transform:rotate(0deg);
}
to {
transform:rotate(360deg);
}
      }
    </style>
    <script type="text/javascript" src="/public/admin/js/jquery.mask.js"></script>
