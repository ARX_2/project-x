@forelse($product as $g)
<?php
    if($g->created_at !== null){
        $created = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $g->created_at)->addHours(5)->toDateTimeString();
        $created = date("d-m-Y", strtotime($created));
    }
    if($g->updated_at !== null){
        $updated = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $g->updated_at)->addHours(5)->toDateTimeString();
        $updated = date("d-m-Y", strtotime($updated));
    }
?>
<div class="item" id="arrayorder_{{$g->id}}">
    <div class="image">
        <img src="{{asset('storage/app')}}/{{$g->images->images or '/public/no-image-80x80.png'}}" alt="">
    </div>
    <div class="info_goods">
        <div class="block">
            <div class="name"><a href=""data-toggle="modal" data-target="#exampleModalLong">{{$g->title}}</a></div>
            <div class="price">Цена: <span>{{$g->coast}}</span> руб.</div>
        </div>
        <div class="additionally">
            <div class="p_category">Раздел: <span>{{$g->subcategory->title or 'не выбран'}}</span></div>
            <div class="date">Добавлено: <span>{{$created or ''}}</span><span> | </span> Обновлено: <span>{{$updated or ''}}</span></div>
        </div>
    </div>
    <div class="status">
        <span class="sign eae @if($g->published == 1)active @endif"></span>
        <span class="sign search @if($g->checkseo == 1)active @endif"></span>
        <span class="sign @if($g->main == 1)star @endif"></span>
    </div>
    <div class="action">
        <a href="#" class="dropdown-toggle edit" data-toggle="dropdown" ></a>
        <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
            <li><a href="{{route('admin.goods.hide', ['id'=>$g->id])}}">@if($g->published == 1)Скрыть @else Показать @endif</a></li>
            {{--<li><a href="{{route('admin.goods.destroy.goods', ['id'=>$g->id, 'deleted' => $g->deleted])}}">@if($g->deleted == 0 || $g->deleted == null)Удалить @else Восстановить @endif</a></li>--}}
            <li><a href="{{route('admin.goods.edit', [$g->id])}}?id={{$g->id}}">Редактировать</a></li>
            <li><a href="{{route('admin.goods.main', ['id'=>$g->id])}}">@if($g->main == 0)На главную @else Снять с главной @endif</a></li>
        </ul>
    </div>
</div>

@empty
@endforelse
