<div class="ticket">
    <div class="ticket_image">
        <div class="image"><img src="" alt=""></div>
    </div>
    <div class="ticket_content">
        <div class="ticket_user_name">@if($message->user_id != null){{$message->user->name}} {{$message->user->lastname}}@elseif($message->agent_id != null)Агент Поддежки #{{$message->agent_id}} @endif</div>
        <div class="ticket_user_mess">{{$message->description}}</div>
        <div class="ticket_user_date">{{Resize::Date($message->created_at)}}</div>
    </div>
</div>