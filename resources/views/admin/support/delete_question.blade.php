<style>
    .box_modal_fixed{position:fixed;top:0;right:0;bottom:0;left:0;background:rgba(0,0,0,0.5);z-index:1050;-webkit-transition:opacity 400ms ease-in;-moz-transition:opacity 400ms ease-in;transition:opacity 400ms ease-in}
    .box_modal_fixed:target{opacity:1;pointer-events:auto;overflow-y:auto}
    .box_modal{position:relative;width:auto;margin:10px}
    @media (min-width: 576px) {
        .box_modal{max-width:500px;margin:30px auto}
    }
    .box_modal_content{position:relative;outline:0}
    @media (min-width: 768px) {
        .box_modal_content{-webkit-box-shadow:0 5px 15px rgba(0,0,0,.5);box-shadow:0 5px 15px rgba(0,0,0,.5)}
    }
    .box_modal_header{background:#0095eb;width:100%;border-radius:4px 4px 0 0;padding:15px;display:flex;border-bottom:1px solid #e6e6e6}
    .box_modal_header .title{display:inline-block;color:#fff;width:100%;font-size:16px}
    .box_modal_header .modal_close{display:inline-block;font-size:30px;line-height:16px;font-weight:600;color:#fff}
    .box_modal_header .modal_close:focus,.box_modal_header .modal_close:hover{color:#fff;text-decoration:none;cursor:pointer}
    .box_modal_body{padding:15px;overflow:auto;background:#fff}
    .box_modal_footer{background:#fafbfc;border-radius:0 0 4px 4px;border-top:1px solid #e6e6e6;padding:10px 15px}
    .button_block{float:right;display:flex}
    .button_close button{margin-left:10px;position:relative;overflow:hidden;width:95px;height:28px;background:none;border:none;border-radius:3px;padding:4px;color:#249ffe;text-align:center}
    .button_close button:hover{background:#eee}
    .button_delete button{margin-left:10px;position:relative;overflow:hidden;width:95px;height:28px;background:#249ffe;border:none;border-radius:3px;padding:4px;color:#fff;text-align:center}
</style>
<div class="box_modal_fixed">
    <div class="box_modal">
        <div class="box_modal_content">
            <div class="box_modal_header">
                <div class="title">Удаление вопроса</div>
                <div class="modal_close">×</div>
            </div>
            <div class="box_modal_body">
                <div class="description">Вы действительно хотите удалить вопрос? Это действие нельзя будет отменить.</div>
            </div>
            <div class="box_modal_footer clear_fix">
                <div class="button_block ">
                    <div class="button_close"><button>Отменить</button></div>
                    <div class="button_delete" data-id-question="{{$id}}"><button>Удалить</button></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.button_delete').click( function(){
        var id = $(this).data('id-question');
        $.post('/admin/support/view/delete/'+id, {_token: '{!! csrf_token() !!}'}, function(data){
            window.location.href = '/admin/support';
        });
    });
</script>
<script type="text/javascript">
    $('.modal_close, .button_close').click( function(){
        $('.box_modal_fixed')
            .animate({opacity: 0}, 100,
                function(){
                    $(this).css('display', 'none');
                    $('#overlay').fadeOut(100);
                }
            );
    });
</script>