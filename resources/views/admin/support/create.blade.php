<div class="c_p_company" style="margin-top: 30px;">
    <div class="wide">
        <div class="box">
            <div class="wide_title">
                <h2>Правая штука</h2>
            </div>
            <div class="undefined_item">

            </div>
        </div>
    </div>
    <div class="narrow">
        <div class="box messages" style="padding: 0;    ">
            <div class="narrow_title" style="margin-bottom: 0;background: #fafbfc;padding: 20px 15px 20px;">
                <div class="left">
                    <h2><a href="/admin/support" style="color: #656565;">Мои вопросы</a> -> Новый вопрос</h2>
                </div>
            </div>
            <style>
                .tickets_create{}
                .tickets_create .title{   font-size: 15px; margin-top: 15px;
                    padding: 0 15px;}
                .add_form_mess{    padding: 15px;
                    display: table;
                    width: 100%;}
                .tickets_create .ticket_post{width: 100%;
                    margin-bottom: 10px;}
                .tickets_create .ticket_post:last-child{margin-bottom: 0}
                .tickets_create .ticket_post input{    width: 100%;
                    border: 1px solid #d3d9de;
                    box-sizing: border-box;
                    padding: 5px 10px 4px;
                    line-height: 19px;
                    outline: none;
                    padding-right: 50px;}
                .tickets_create .ticket_post textarea{box-sizing: border-box;
                    margin: 0px;
                    padding: 5px 10px;
                    border: 1px solid #d3d9de;
                    width: 100%;
                    height: 120px;
                    vertical-align: top;
                    outline: none;
                    overflow: hidden;
                    resize: none;}
                .comment_buttom {
                    position: relative;
                    overflow: hidden;
                    width: 95px;
                    height: 28px;
                    background: #249ffe;
                    border: none;
                    border-radius: 3px;
                    padding: 4px;
                    color: #fff;
                    text-align: center;}
            </style>
            <div class="tickets_create">
                <div class="title">Здесь Вы можете задать любой вопрос по Сайту</div>
                <form action="{{route('admin.support.store')}}" class="add_form_mess" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="ticket_post">
                        <input type="text" name="title" autocomplete="off" placeholder="Пожалуйста, опишите Вашу проблему в двух словах...">
                    </div>
                    <div class="ticket_post">
                        <textarea name="description" dir="auto" placeholder="Пожалуйста, расскажите о Вашей проблеме чуть подробнее..."></textarea>
                    </div>
                    <input type="file" name="image">
                    <div class="ticket_button">
                        <button class="comment_buttom" type="submit">Отправить</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>