<div class="c_p_company" style="margin-top: 30px;">
    <div class="wide">
        <div class="box">
            <div class="wide_title">
                <h2>Правая штука</h2>
            </div>
            <div class="undefined_item">

            </div>
        </div>
    </div>
    <div class="narrow">
        <div class="box messages" style="padding: 0;    ">
            <div class="narrow_title" style="margin-bottom: 0;background: #fafbfc;padding: 20px 15px 20px;">
                <div class="left">
                    <h2><a href="/admin/support" style="color: #656565;">Мои вопросы</a> -> Вопрос</h2>
                </div>
            </div>
            <style>
                .title_header{box-sizing:border-box;padding:10px 15px;font-size:13px;color:#000;border-bottom:1px solid #ddd;background:#fff;display:block}
                .title_header .left{float:left;max-width:100%;position:relative}
                .title_header .right{float:right;position:relative;line-height:35.002px}
                .clear_fix:after{content:'.';display:block;height:0;font-size:0;line-height:0;clear:both;visibility:hidden}
                .title_header .title_question{font-size:15px;font-weight:600;color:#444}
                .title_header .status_question{color:#666}
                .title_header .right .act a{color:#2a5885;cursor:pointer}

                .ticket{padding:15px;border-bottom:1px solid #ddd}
                .ticket:last-child{border-bottom:none}
                .ticket_image{float:left;margin-right:12px;padding-top:2px;width:42px}
                .ticket_image .image{width:40px;height:40px;background:#269ffd;border-radius:50%}
                .ticket_content{margin-left:54px;display:block}
                .ticket_content .ticket_user_name{font-size:14px;line-height:17px;color:#222;font-weight:600;overflow:hidden}
                .ticket_content .ticket_user_mess{color:#222;word-wrap:break-word;margin-top:5px}
                .ticket_content .ticket_user_date{color:#939393;font-size:12px;margin-top:5px;line-height:17px}

                .ticket_post{padding:10px 15px;border-bottom:1px solid #ddd}
                .ticket_post textarea{overflow:hidden;width: 100%;resize:none;height:50px;border:none;background:none;outline:none}
                .ticket_button{padding:10px 15px;background:#fafbfc}
                .ticket_button .comment_buttom{position:relative;overflow:hidden;width:95px;height:28px;background:#249ffe;border:none;border-radius:3px;padding:4px;color:#fff;text-align:center}
            </style>
            <div class="title_header clear_fix">
                <div class="left">
                    <div class="title_question">{{$question->title}}</div>
                    <div class="status_question">@if($question->status == 1)Вопрос ожидает обработки.
                        @elseif($question->status == 2)Вопрос находится на рассмотрении.
                        @elseif($question->status == 3)Есть ответ.
                        @elseif($question->status == 4)Рассмотрен.
                        @endif</div>
                </div>
                <div class="right">
                    <div class="act"><a href="" id="delete_question" data-question="{{$question->id}}">Удалить вопрос</a></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $('#delete_question').click( function(e) {
                            e.preventDefault();
                            var id = $(this).data('question');
                            $.get('/admin/support/view/deletequestion/'+id, function(data){
                                $('.modal_del_question').html(data);
                            });
                        });
                    });
                </script>
            </div>
            <div class="tickets_list">
                @forelse($message as $m)
                <div class="ticket">
                    <div class="ticket_image">
                        <div class="image"><img src="" alt=""></div>
                    </div>
                    <div class="ticket_content">
                        <div class="ticket_user_name">@if($m->user_id != null){{$m->user->name}} {{$m->user->lastname}}@elseif($m->agent_id != null)Агент Поддежки #{{$m->agent_id}} @endif</div>
                        <div class="ticket_user_mess">{{$m->description}}</div>
                        <div class="ticket_user_date">{{Resize::Date($m->created_at)}}</div>
                    </div>
                </div>
                @empty
                @endforelse
                <form id="addmessage" class="add_form_mess" method="POST" >
                    {{ csrf_field() }}
                <div class="ticket_post">
                    <textarea name="description" dir="auto" placeholder="Комментировать..."></textarea>
                </div>
                <input type="hidden" name="user_id" value="{{Auth::id()}}">
                <input type="hidden" name="support_id" value="{{$question->id}}">
                <div class="ticket_button">
                    <button class="comment_buttom" type="submit">Отправить</button>
                </div>
                </form>
                    <script>
                        $(document).ready(function(){
                            $('#addmessage').on('submit', function(e){
                                e.preventDefault();
                                $.ajax({
                                    type: 'POST',
                                    url: '/admin/support/view/addmessage',
                                    data: $('#addmessage').serialize(),
                                    success:function(data){
                                        console.log(data);
                                        $('.add_form_mess').before(data);
                                    }
                                });
                                $('#addmessage')[0].reset();
                            });
                        });
                    </script>
            </div>
        </div>
    </div>
</div>

<div class="modal_del_question"></div>