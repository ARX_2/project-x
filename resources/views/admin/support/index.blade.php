<div class="c_p_company" style="margin-top: 30px;">
    <div class="wide">
        <div class="box">
            <div class="wide_title">
                <h2>Правая штука</h2>
            </div>
            <div class="undefined_item">

            </div>
        </div>
    </div>
    <style>
        .messages .request{display:table;width:100%;font-size:13px;padding:15px 0;border-bottom:1px solid #ddd}
        .messages .request:last-child{border-bottom: none}
        .messages .request .left{float:left;width:60%}

        .messages .request .left .check:before {
            width: 32px;
            height: 32px;
            background: url('https://vk.com/images/support/status_icons.png') no-repeat 0 0;
            content: '';
            position: absolute;
            background-position: left 0;
            top: 0;
            left: 2px;
        }
        .messages .request .left .waiting:before {
            width: 32px;
            height: 32px;
            background: url('https://vk.com/images/support/status_icons.png') no-repeat 0 0;
            content: '';
            position: absolute;
            background-position: left -32px;
            top: 0;
            left: 2px;
        }
        .messages .request .left .info{position: relative;display:inline-block;width:85%;padding-left: 49px;}
        .messages .request .left .info .name{font-weight:600;line-height: 13px;margin-bottom: 2px;}
        .messages .request .left .info a{color:#2a6496;}
        .messages .request .left .info .mess{color:#333}
        .messages .request .right{display: flex;float:right;width: 217px;text-align:left}
        .messages .request .right .image{vertical-align:top;width:40px;min-width:40px;height:40px;position:relative;max-width:15%;display:inline-block;background: #0095eb;
            border-radius: 50%;}
        .messages .request .right .image img{width:40px;min-width:40px;height:40px;border-radius:50%}
        .messages .request .right .info{display:inline-block;width:80%;padding-left:10px;vertical-align:-webkit-baseline-middle;text-align:left}
        .messages .request .right .info .name{font-weight:600;color:#222;max-width:155px}
        .messages .request .right .info .date{color:#888}
    </style>

    <div class="narrow">
        <div class="box messages" style="padding: 10px 10px 0 10px;">
            <div class="narrow_title" style="margin-bottom: 0;">
                <div class="left">
                    <h2>Поддержка</h2>
                </div>
                <div class="right">
                    <span class="button-edit"><a href="/admin/support/create">Новый вопрос</a></span>
                </div>
            </div>
            @forelse($questions as $t)
            <div class="request">
                <div class="left">
                    <div class="info @if($t->status <= 2) waiting @else check @endif">
                        <div class="name"><a href="/admin/support/question/{{$t->id}}">{{$t->title}}</a></div>
                        <div class="mess">
                            <span>
                                @if($t->status == 1)Вопрос ожидает обработки.
                                @elseif($t->status == 2)Вопрос находится на рассмотрении.
                                @elseif($t->status == 3)Есть ответ.
                                @elseif($t->status == 4)Рассмотрен.
                                @endif</span>
                        </div>
                    </div>
                </div>
                <div class="right">
                    <div class="image">
                        <img src="" alt="">
                    </div>
                    <div class="info">
                        <div class="name">@if($t->status <= 2){{$t->user->name or ''}} {{$t->user->lastname or ''}} @elseif($t->status >= 3)Агент Поддержки @endif</div>
                        <div class="date">@if($t->status >= 3) ответил @endif{{Resize::noTimeDate($t->created_at)}}</div>
                    </div>
                </div>
            </div>
            @empty
            @endforelse
        </div>
    </div>
</div>

<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="editsection" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content vm-goods">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <div class="modal-body modal_edit">
                <div class="content">
                    <div class="vm-header">
                        <div class="name">Переименовать раздел "Услуги"</div>
                    </div>
                    <div class="vm-body">
                        <div class="vm-edit">
                            <form class="" action="" method="post" enctype="multipart/form-data">
                                {{ csrf_field()}}
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Заголовок причины:</label></div>
                                    <div class="center">
                                        <input type="text" name="title" class="" placeholder="Ошибка с редактированием новостей" value="" required="">
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Описание причины:</label></div>
                                    <div class="center">
                                <textarea  name="description" class="" placeholder="Добрый день. Сегодня нажал на какую то кнопочку и все исчезло..." style="background-color: #fff;
width: 100%;
height: 80px;
border: 1px solid #ccc;
font-size: 14px;
font-weight: 400;
border-radius: 3px;
color: #222;
padding-left: 5px;" required=""></textarea>
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Тип заявки:</label></div>
                                    <div class="center">
                                        <label for=""><input type="radio" name="type" class="" value="0" required="">Ошибка</label>
                                        <label for=""><input type="radio" name="type" class="" value="1" required="">Разработка</label>
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Скриншот (Картинка с экрана компьютера):</label></div>
                                    <div class="center">
                                        <input type="file" name="file[]" class="" style="background-color:rgba(46,173,54,0) !important;" multiple>
                                    </div>
                                </div>
                                <div class="perent" style="margin-bottom: 0">
                                    <div class="left"><label for="" class="item_name"></label></div>
                                    <div class="center">
                                        <input class="button-save" type="submit" value="Сохранить" style="width: 115px;background-color: #269ffd;color: #fff;">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
