
@forelse($product as $g)
<div class="item" id="arrayorder_{{$g->id}}">
    <div class="image">
        <img src="{{asset('storage/app')}}/{{Resize::ResizeImage($g->images, '80x80')}}" alt="">
    </div>
    <div class="info_goods">
        <div class="block">
            <div class="name"><a href=""data-toggle="modal" data-target="#exampleModalLong">{{$g->title}}</a></div>
            <div class="price">Цена: <span>{{$g->coast}}</span> руб.</div>
        </div>
        <div class="additionally">
            <div class="p_category">Раздел: <span>{{$g->categories->title or 'не выбран'}}</span></div>
            <div class="date">Добавлено: <span>{{Resize::date($g->created_at)}}</span><span> | </span> Обновлено: <span>{{Resize::date($g->updated_at)}}</span></div>
        </div>
    </div>
    <div class="status">
        <span class="sign eae @if($g->published == 1)active @endif"><a href="{{route('admin.stroitelstvo.update.id', ['published',$g->id,$g->published])}}" class="link_published"></a></span>
        @if($g->published == 1 && $g->deleted == 0)<span class="sign search @if($g->checkseo == 1)active @endif"></span>@endif
        @if($g->published == 1 && $g->deleted == 0)<span class="sign star @if($g->main == 1)activeStar @endif"><a href="{{route('admin.stroitelstvo.update.id', ['main',$g->id,(int)$g->main])}}"></a></span>@endif
        @if($g->published == 0 && $g->deleted == 0)<a href="{{route('admin.stroitelstvo.update.id', ['deleted',$g->id,(int)$g->deleted])}}" class="link_delete"><i class="fa fa-times" aria-hidden="true"></i></a>@endif
    </div>
    @if($g->protected !== 1 || $g->protected == 1 && Auth::user()->userRolle !== 6)
    <div class="action">
        <a href="#" class="dropdown-toggle edit" data-toggle="dropdown" ></a>
        <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
            <!-- <li><a href="{{route('admin.stroitelstvo.update.id', ['published',$g->id,$g->published])}}" class="link_published">@if($g->published == 1)Скрыть @else Показать @endif</a></li> -->
            {{--<li><a href="{{route('admin.stroitelstvo.destroy.building', ['id'=>$g->id, 'deleted' => $g->deleted])}}">@if($g->deleted == 0 || $g->deleted == null)Удалить @else Восстановить @endif</a></li>--}}
            <li><a href="{{route('admin.stroitelstvo.edit', [$g->id])}}?id={{$g->id}}" class="link_edit">Редактировать</a></li>
            <li><a href="{{route('admin.stroitelstvo.update.id', ['main',$g->id,(int)$g->main])}}">@if($g->main == 0)На главную @else Снять с главной @endif</a></li>
        </ul>
    </div>
    @endif
</div>

@empty
@endforelse

<script type="text/javascript">
  $('.eae').click(function(){
    var link = $(this).find('a').attr('href');
    $(this).parent('.status').parent('.item').remove();
    var count_active = $('#count_active').html();
    count_active = Number.parseInt(count_active);


    var count_hidden = $('#count_hidden').html();
    count_hidden = Number.parseInt(count_hidden);
    var check = $('#link_active').hasClass('active');
    if(check == true){
      count_active = count_active-1;
      count_hidden = count_hidden+1;
    }
    else{
      count_active = count_active+1;
      count_hidden = count_hidden-1;
    }

    $('#count_active').text(count_active);
    $('#count_hidden').text(count_hidden);

    $.get(link, function(data){});

  });

  $('.link_delete').click(function(){
    var link = $(this).attr('href');
    $(this).parent('.status').parent('.item').remove();
    var count_hidden = $('#count_hidden').html();
    count_hidden = Number.parseInt(count_hidden);


    var count_deleted = $('#count_deleted').html();
    count_deleted = Number.parseInt(count_deleted);
      count_hidden = count_hidden-1;
      count_deleted = count_deleted+1;
    $('#count_hidden').text(count_hidden);
    $('#count_deleted').text(count_deleted);

    $.get(link, function(data){});
    return false;
  });

  $('.star').click(function(){
    var link = $(this).find('a').attr('href');
    var check =  $(this).hasClass('activeStar');
    if(check == true){
      $(this).removeClass('activeStar');
    }
    else{
      $(this).addClass('activeStar');
    }
    $.get(link, function(data){});

  });
</script>
