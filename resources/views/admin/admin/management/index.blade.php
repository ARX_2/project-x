  @include('admin.admin.include.mainHeader')

    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            <div class="row">
                <div class="col-lg-4">
                    <h2>Управление сайтами</h2>
                </div>
                <div class="col-lg-8">
                    <ol class="breadcrumb">
                        <li>
                            <a href="">Главная</a>
                        </li>
                        <li class="active">Управление сайтами</li>
                    </ol>
                </div>
            </div>
            <hr>

            <div class="c_p_company">
                <div class="wide">
                    <div class="box">
                        <div class="wide_title">
                            <h2>Правая штука</h2>
                        </div>
                        <div class="undefined_item">
                            <div class="item">
                                <div class="name">
                                    <a href="#mebel">Изготовление мебели</a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="name">
                                    <a href="#stroitelstvo">Строительство</a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="name">
                                    <a href="#ses">Уничтожение насекомых, сэс</a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="name">
                                    <a href="#pamyatkik">Памятники</a>
                                </div>
                            </div>

                            <div class="item">
                                <div class="name">
                                    <a href="#another">Остальные</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style>
                    .management .sort{width: 100%;
                        display: -ms-flexbox;
                        display: -webkit-flex;
                        display: -moz-flex;
                        display: -ms-flex;
                        display: flex;
                        -webkit-flex-wrap: wrap;
                        -moz-flex-wrap: wrap;
                        -ms-flex-wrap: wrap;
                        flex-wrap: wrap;}
                    .management h2{font-size:16px;font-weight:600;margin: 0}
                    .sitebox_title{margin-bottom: 25px}
                    .management h3{font-size:16px;font-weight:600;margin: 0}
                </style>
                <style>
                    .sitebox{display: table;padding: 10px;margin-top: 10px;width: 100%;}
                    .sitebox .i{display: flex;width: 23%;margin-right: 2%;float: left;margin-bottom: 15px;}
                    .sitebox .i .left{width: 100%;min-width: 33.3%}
                    .sitebox .i .left .image{width: 100%;height: 60px;margin-bottom: 10px;    background: #aaa;}
                    #favorite .sitebox .i .left .image{background:#c0518d}
                    #mebel .sitebox .i .left .image{background:#5cb85c}
                    #stroitelstvo .sitebox .i .left .image{background:#cc937c}
                    #ses .sitebox .i .left .image{background:#e41039}
                    #pamyatkik .sitebox .i .left .image{background:#28252c}
                    .sitebox .i .left .image img{width: 100%;height: 100%;object-fit: contain;object-position: left;}
                    .sitebox .i .left .name{font-size: 14px;line-height: 14px;margin-bottom: 5px}
                    .sitebox .i .left .city{font-size: 14px;line-height: 14px;margin-bottom: 5px}
                    .sitebox .i .left .domain{font-size: 14px;line-height: 14px;    white-space: nowrap;
                        overflow: hidden;
                        width: 100%;display: inline-flex;}
                    .sitebox .i .left .domain .DelFavorite{    width: 15%;
                        text-align: right;color: #aaa;     }
                    .sitebox .i .left .domain .DelFavorite:hover    {color: #d02223}
                    .sitebox .i .left .domain .favorite    {    width: 15%;
                        text-align: right;color: #aaa;}
                    .sitebox .i .left .domain .favorite:hover{color: #d02223}
                    .sitebox .i .domain a{width: 85%;
                        display: block;
                        overflow: hidden;}
                    .sitebox .i a{color: #2986c7}
                </style>
                <div class="narrow">

                  <div class="box" id="favorite">
                      <div class="management">
                          <div class="narrow_title">
                              <div class="left">
                                  <h2>Сайты</h2>
                              </div>
                              <div class="right">
                                    <span class="button-edit"><a href="#" data-toggle="modal" data-target="#exampleModalLong">Добавить</a></span>
                              </div>
                          </div>
                          <div class="sitebox">
                              <div class="sitebox_title">
                                  <h3>Избранные</h3>
                                  <div>{{$countfavorites or ''}} сайтов</div>
                              </div>
                              <div class="list">
                                <div class="response"> </div>
                              <div class="sort" id="favorites">
                              @forelse($favorites as $favorite)
                              <div class="i">
                                  <div class="left">
                                    <div id="arrayorder_{{$favorite->id}}">
                                      <a href="{{route('admin.admin.chooseSite')}}?siteID={{$favorite->sites->id}}"><div class="image"><img src="/storage/app/{{$favorite->sites->info->logo1 or ''}}" alt=""></div></a>
                                      <div class="name" @if($favorite->sites->monthly != null)style="font-weight: 600;"@endif>{{$favorite->sites->name or ''}}</div>
                                      <div class="city">г. {{$favorite->sites->info->cities->gorod or ''}}</div>
                                        @if($favorite->sites->monthly != null)<div class="price" style="font-weight: 600;margin-bottom: 5px;background: #fdcc63;display: table;padding: 2px 10px 2px 5px;border-radius: 0 10px 10px 0;">{{$favorite->sites->monthly or ''}} руб./мес</div>@endif
                                      <div class="domain"><a href="https://{{$favorite->sites->title or ''}}" target="_blank">{{$favorite->sites->title or ''}}</a> <a  style="float:right; cursor:pointer;" class="DelFavorite" ident="{{$favorite->id or ''}}"><i class="fa fa-times" aria-hidden="true"></i></a></div>
                                  </div>
                              </div>
                              </div>
                              @empty
                              @endforelse
                              </div>
                            </div>
                          </div>

                      </div>
                  </div>
                  <script type="text/javascript">
                    $('.DelFavorite').click(function(){
                      var id = $(this).attr('ident');
                       $.get('{{route('admin.admin.favorite.destroy')}}?id='+id);
                       $(this).closest('.i').remove();
                    });
                  </script>


                    <div class="box" id="mebel">
                        <div class="management">
                            <div class="sitebox">
                                <div class="sitebox_title">
                                    <h3>Изготовление мебели</h3>
                                    <div>{{$countMebel or ''}} сайтов</div>
                                </div>
                                <div class="sort">
                                @forelse($companies as $company)
                                @if($company->type == 3)
                                <div class="i">
                                    <div class="left">
                                        <a href="{{route('admin.admin.chooseSite')}}?siteID={{$company->id}}"><div class="image"><img src="/storage/app/{{$company->info->logo1 or ''}}" alt=""></div></a>
                                        <div class="name">{{$company->name or ''}}</div>
                                        <div class="city">г. {{$company->info->cities->gorod or ''}}</div>
                                        @if($company->monthly != null)<div class="price" style="font-weight: 600;margin-bottom: 5px;background: #fdcc63;display: table;padding: 2px 10px 2px 5px;border-radius: 0 10px 10px 0;">{{$company->monthly or ''}} руб./мес</div>@endif
                                        <div class="domain"><a href="https://{{$company->title or ''}}" target="_blank">{{$company->title or ''}}</a> <a  style="float:right; cursor:pointer;" ident="{{$company->id or ''}}" class="favorite"><i class="fa fa-heart" aria-hidden="true"></i></a> </div>
                                    </div>
                                </div>
                                @endif
                                @empty
                                @endforelse
                              </div>

                            </div>
                        </div>
                    </div>
                    <div class="box" id="stroitelstvo">
                        <div class="management">
                            <div class="sitebox">
                                <div class="sitebox_title">
                                    <h3>Строительство</h3>
                                    <div>{{$countBuilding or ''}} сайтов</div>
                                </div>
                                <div class="sort">
                                @forelse($companies as $company)
                                @if($company->type == 1)
                                <div class="i">
                                    <div class="left">
                                        <a href="{{route('admin.admin.chooseSite')}}?siteID={{$company->id}}"><div class="image"><img src="/storage/app/{{$company->info->logo1 or ''}}" alt=""></div></a>
                                        <div class="name">{{$company->name or ''}}</div>
                                        <div class="city">г. {{$company->info->cities->gorod or ''}}</div>
                                        @if($company->monthly != null)<div class="price" style="font-weight: 600;margin-bottom: 5px;background: #fdcc63;display: table;padding: 2px 10px 2px 5px;border-radius: 0 10px 10px 0;">{{$company->monthly or ''}} руб./мес</div>@endif
                                        <div class="domain"><a href="https://{{$company->title or ''}}" target="_blank">{{$company->title or ''}}</a> <a  style="float:right; cursor:pointer;" ident="{{$company->id or ''}}" class="favorite"><i class="fa fa-heart" aria-hidden="true"></i></a></div>
                                    </div>
                                </div>
                                @endif
                                @empty
                                @endforelse
                              </div>

                            </div>
                        </div>
                    </div>
                    <div class="box" id="ses">
                        <div class="management">
                            <div class="sitebox">
                                <div class="sitebox_title">
                                    <h3>Уничтожение насекомых, СЭС</h3>
                                    <div>{{$countSes or ''}} сайтов</div>
                                </div>
                                <div class="sort">
                                @forelse($companies as $company)
                                @if($company->type == 2)
                                    <div class="i">
                                        <div class="left">
                                            <a href="{{route('admin.admin.chooseSite')}}?siteID={{$company->id}}"><div class="image"><img src="/storage/app/{{$company->info->logo1 or ''}}" alt=""></div></a>
                                            <div class="name">{{$company->name or ''}}</div>
                                            <div class="city">г. {{$company->info->cities->gorod or ''}}</div>
                                            @if($company->monthly != null)<div class="price" style="font-weight: 600;margin-bottom: 5px;background: #fdcc63;display: table;padding: 2px 10px 2px 5px;border-radius: 0 10px 10px 0;">{{$company->monthly or ''}} руб./мес</div>@endif
                                            <div class="domain"><a href="https://{{$company->title or ''}}" target="_blank">{{$company->title or ''}}</a> <a  style="float:right; cursor:pointer;" ident="{{$company->id or ''}}" class="favorite"><i class="fa fa-heart" aria-hidden="true"></i></a></div>
                                        </div>
                                    </div>
                                @endif
                                @empty
                                @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box" id="pamyatkik">
                        <div class="management">
                            <div class="sitebox">
                                <div class="sitebox_title">
                                    <h3>Памятники</h3>
                                    <div>{{$countPamyatkik or ''}} сайтов</div>
                                </div>
                                <div class="sort">
                                @forelse($companies as $company)
                                @if($company->type == 4)
                                    <div class="i">
                                        <div class="left">
                                            <a href="{{route('admin.admin.chooseSite')}}?siteID={{$company->id}}"><div class="image"><img src="/storage/app/{{$company->info->logo1 or ''}}" alt=""></div></a>
                                            <div class="name">{{$company->name or ''}}</div>
                                            <div class="city">г. {{$company->info->cities->gorod or ''}}</div>
                                            @if($company->monthly != null)<div class="price" style="font-weight: 600;margin-bottom: 5px;background: #fdcc63;display: table;padding: 2px 10px 2px 5px;border-radius: 0 10px 10px 0;">{{$company->monthly or ''}} руб./мес</div>@endif
                                            <div class="domain"><a href="https://{{$company->title or ''}}" target="_blank">{{$company->title or ''}}</a> <a  style="float:right; cursor:pointer;" ident="{{$company->id or ''}}" class="favorite"><i class="fa fa-heart" aria-hidden="true"></i></a></div>
                                        </div>
                                    </div>
                                @endif
                                @empty
                                @endforelse
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box" id="another">
                        <div class="management">
                            <div class="sitebox">
                                <div class="sitebox_title">
                                    <h3>Остальные</h3>
                                    <div>{{$countOther or ''}} сайтов</div>
                                </div>
                                <div class="sort">
                                @forelse($companies as $company)
                                @if($company->type == 0)
                                <div class="i">
                                    <div class="left">
                                        <a href="{{route('admin.admin.chooseSite')}}?siteID={{$company->id}}"><div class="image"><img src="/storage/app/{{$company->info->logo1 or ''}}" alt=""></div></a>
                                        <div class="name">{{$company->name or ''}}</div>
                                        <div class="city">г. {{$company->info->cities->gorod or ''}}</div>
                                        @if($company->monthly != null)<div class="price" style="font-weight: 600;margin-bottom: 5px;background: #fdcc63;display: table;padding: 2px 10px 2px 5px;border-radius: 0 10px 10px 0;">{{$company->monthly or ''}} руб./мес</div>@endif
                                        <div class="domain"><a href="https://{{$company->title or ''}}" target="_blank">{{$company->title or ''}}</a> <a  style="float:right; cursor:pointer;" ident="{{$company->id or ''}}" class="favorite"><i class="fa fa-heart" aria-hidden="true"></i></a></div>
                                    </div>
                                </div>
                                @endif
                                @empty
                                @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
              $('.favorite').click(function(){
                 $(this).closest('.i').clone().appendTo('#favorites');
                var id = $(this).attr('ident');
                 $.get('{{route('admin.admin.favorite')}}?id='+id);
              });
            </script>
            <script type="text/javascript">
            $(document).ready(function(){
            function slideout(){
            setTimeout(function(){
            $(".response").slideUp("slow", function () {
            });

            }, 2000);}

            $(".response").hide();
            $(function() {
            $(".list .sort").sortable({ opacity: 0.8, cursor: 'move', update: function() {

            var order = $(this).sortable("serialize") + '&update=update';
            $.get("{{route('admin.favorite.sort')}}", order, function(theResponse){
            $(".response").html(theResponse);
            $(".response").slideDown('slow');
            slideout();
            });
            }
            });
            });
            });

            </script>

<style media="screen">
  .light{
    color: #212529;
background-color: #f8f9fa;
border-color: #f8f9fa;display: inline-block;
font-weight: 400;
text-align: center;
white-space: nowrap;
vertical-align: middle;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
border: 1px solid transparent;
padding: .375rem .75rem;
font-size: 1rem;
line-height: 1.5;
border-radius: .25rem;
transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;font-size:17px;text-decoration:none;
  }
</style>
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="color:black;">Добавление сайта</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('admin.createSite')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
      <div class="modal-body">
        <label for="" style="color:black;">Название сайта</label>
        <input type="text" class="form-control" name="title" placeholder="site.ru">

        <label for="" style="color:black;">Тип сайта</label>
        <select class="form-control" name="type">
          <option value="0">-Инной-</option>
          <option value="1">Строительство</option>
          <option value="2">Салоны</option>
          <option value="3">Мебель</option>
        </select><br>
      <p style="cursor:pointer" class="subinfosubmit">Доп. информация</p>
      <div style="display:none;" class="subinfo hid">
        <label for="" style="color:black;">Почта</label>
        <input type="text" class="form-control" name="mail" placeholder="mail@yandex.ru">

        <label for="" style="color:black;">Телефон</label>
        <input type="text" class="form-control" name="tel" placeholder="+79126200718">

        <label for="" style="color:black;">Имя Фамилия</label>
        <input type="text" class="form-control" name="name" placeholder="Павел Липаткин">

      </div>
      <label for="">Взять контент</label>
      <select class="form-control takeContent" name="content">
        <option value="" selected>Нет</option>
        @forelse($sites as $site)
        <option value="{{$site->id or ''}}">{{$site->title or ''}}</option>
        @empty
        @endforelse
      </select>
      <div style="display:none;" class="renameCity">
        <label for="">Проставить город?</label>
        <label for="">Да <input type="radio" name="city" value="1"></label>
        <label for="">Нет <input type="radio" name="city" value="0"></label>
      </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <input type="submit" class="btn btn-primary"  value="Сохранить">
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('.subinfosubmit').click(function(){
    if($('.subinfo').hasClass('hid')){
      $('.hid').show();
      $('.hid').addClass('show');
      $('.hid').removeClass('hid');
    }
    else{
      $('.show').hide();
      $('.show').addClass('hid');
      $('.show').removeClass('show');
    }

  });
  $('.takeContent').on('change', function(){
    if($(this).val() == ''){
      $('.renameCity').hide();
    }
    else{
      $('.renameCity').show();
    }
  });

</script>
</body></html>
