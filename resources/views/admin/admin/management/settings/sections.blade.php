  @include('admin.admin.include.mainHeader')
    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            @include('admin.admin.include.rmenu')
                <div class="narrow">
                    <div class="box">
                        <div class="setting">
                            <h2>Разделы</h2>

                            <div class="group-edit-row">
                                <div class="edit-label">Документы:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Портфолио:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>

                            <div class="group-edit-row">
                                <div class="edit-label">Вакансии:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Акции:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>

                            <div class="group-edit-row">
                                <div class="edit-label">Новости:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Блог:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Прайс-лист:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Доставка и оплата:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Сертификаты:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Вопрос-ответ:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Отзывы:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Наши партнеры:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Видео:</div>
                                <div class="field">
                                    <select>
                                        <option selected value="">Отключено</option>
                                        <option value="">Включено</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">&nbsp;</div>
                                <div class="field">
                                    <a href="" class="button-save" type="submit" value="Сохранить">Сохранить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


</body></html>
