  @include('admin.admin.include.mainHeader')
    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            @include('admin.admin.include.rmenu')
                <div class="narrow">
                  <form class="" action="{{route('admin.admin.information.main')}}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="siteID" value="{{$siteID}}">
                    <div class="box">
                        <div class="setting">
                            <h2>Основная информация</h2>
                            <div class="group-edit-row">
                                <div class="edit-label">Название:</div>
                                <div class="field">
                                    <input type="text" class="" value="{{$company->name or ''}}" name="name" maxlength="48">
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Описание компании:</div>
                                <div class="field">
                                    <textarea class="edit-textarea" maxlength="300" name="description" style="overflow: hidden; resize: none;">{{$company->description or ''}}</textarea>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Адрес сайта:</div>
                                <div class="field">
                                    <input type="text" class="" name="title" value="{{$company->title or ''}}" maxlength="100" readonly="readonly">
                                </div>
                            </div>
                            <hr>
                            <div class="group-edit-row">
                                <div class="edit-label">&nbsp;</div>
                                <div class="field">
                                    <input href="" style="width:30%;height:36px;display:block;line-height:36px;text-align:center;background-color:#2278dd;color:#fff;border:1px solid #ccc;font-size:14px;font-weight:400;border-radius:5px;padding-left:5px" type="submit" value="Сохранить">
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                    <form class="" action="{{route('admin.admin.information.submain')}}" method="post">
                      <input type="hidden" name="siteID" value="{{$siteID}}">
                      {{ csrf_field() }}
                    <div class="box">
                        <div class="setting">
                            <h2>Дополнительная информация</h2>
                            <div class="group-edit-row">
                                <div class="edit-label">Тематика сайта:</div>
                                <div class="field">

                                    <select name="subject">
                                        <option selected disabled>Выберите тематику</option>
                                        <option value="Строительство, ремонт" @if($company->subject=='Строительство, ремонт')selected @endif>Строительство, ремонт</option>
                                        <option value="Дизайн интерьера" @if($company->subject=='Дизайн интерьера')selected @endif>Дизайн интерьера</option>
                                        <option value="Красота и здоровье" @if($company->subject=='Красота и здоровье')selected @endif>Красота и здоровье</option>
                                    </select>
                                </div>
                            </div>

                            <div class="group-edit-row">
                                <div class="edit-label">Телефон:</div>
                                <div class="field">
                                    <input type="text" class="" value="{{$company->tel or ''}}" maxlength="20" name="tel">
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Страна:</div>
                                <div class="field">
                                    <select name="country">
                                        <option selected disabled>Не выбрана</option>
                                        <option value="Россия" @if($company->country=='Россия')selected @endif>Россия</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Город:</div>
                                <div class="field">
                                    <select name="city">
                                        <option selected disabled>Не выбран</option>
                                        <option value="Екатеринбург" @if($company->city=='Екатеринбург')selected @endif>Екатеринбург</option>
                                        <option value="Заводоуковск" @if($company->city=='Заводоуковск')selected @endif>Заводоуковск</option>
                                        <option value="Ишим" @if($company->city=='Ишим')selected @endif>Ишим</option>
                                        <option value="Каменск-Уральский" @if($company->city=='Каменск-Уральский')selected @endif>Каменск-Уральский</option>
                                        <option value="Когалым" @if($company->city=='Когалым')selected @endif>Когалым</option>
                                        <option value="Курган" @if($company->city=='Курган')selected @endif>Курган</option>
                                        <option value="Нефтеюганск" @if($company->city=='Нефтеюганск')selected @endif>Нефтеюганск</option>
                                        <option value="Нижневартовск" @if($company->city=='Нижневартовск')selected @endif>Нижневартовск</option>
                                        <option value="Новосибирск" @if($company->city=='Новосибирск')selected @endif>Новосибирск</option>
                                        <option value="Сургут" @if($company->city=='Сургут')selected @endif>Сургут</option>
                                        <option value="Тобольск" @if($company->city=='Тобольск')selected @endif>Тобольск</option>
                                        <option value="Тюмень" @if($company->city=='Тюмень')selected @endif>Тюмень</option>
                                        <option value="Ханты-Мансийск" @if($company->city=='Ханты-Мансийск')selected @endif>Ханты-Мансийск</option>
                                        <option value="Челябинск" @if($company->city=='Челябинск')selected @endif>Челябинск</option>
                                        <option value="Шадринск" @if($company->city=='Шадринск')selected @endif>Шадринск</option>
                                    </select>
                                </div>
                            </div>
                            <div class="group-edit-row">
                                <div class="edit-label">Адрес:</div>
                                <div class="field">
                                    <input type="text" class="" value="{{$company->address or ''}}" maxlength="100" name="address">
                                </div>
                            </div>
                            <hr>
                            <div class="group-edit-row">
                                <div class="edit-label">&nbsp;</div>
                                <div class="field">
                                    <input href="" style="width:30%;height:36px;display:block;line-height:36px;text-align:center;background-color:#2278dd;color:#fff;border:1px solid #ccc;font-size:14px;font-weight:400;border-radius:5px;padding-left:5px" type="submit" value="Сохранить">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>

        </div>
    </div>
</div>


</body></html>
