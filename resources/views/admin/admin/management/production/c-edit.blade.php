@include('admin.admin.include.mainHeader')

    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            @include('admin.admin.include.rmenu')
                <div class="narrow">
                    <div class="box">
                      <a href="{{route('admin.production.category')}}?siteID={{$siteID}}" class="btn btn-info">← Назад</a>
                        <div class="cp_edit">
                            <div class="title">
                              @if($category !== null)
                                <h2>Редактирование категории: {{$category->title}}</h2>
                                @else
                                <h2>Добавление категории</h2>
                                @endif
                            </div>
                            <?php //dd($category->type == 1); ?>
                            @if($category !== null)
                            <form action="{{route('admin.update.cedit')}}" method="post" enctype="multipart/form-data">
                              @else
                              <form action="{{route('admin.create.cedit')}}" method="post" enctype="multipart/form-data">
                              @endif
                              <input type="hidden" name="id" value="{{$category->id or ''}}">
                              <input type="hidden" name="siteID" value="{{$siteID or ''}}">
                              {{ csrf_field() }}
                            <div class="perent">
                                <div class="left">
                                <label for="" class="name_item">Выберите раздел:</label>
                                </div>
                                <div class="center button-box">
                                  @if($category !== null)
                                    <label><input type="radio" name="type" value="1" @if($category->type == 1) checked @endif hidden><span></span><span class="radio-name">Раздел "Товары"</span></label>
                                    <label><input type="radio" name="type" value="0" @if($category->type==0) checked @endif hidden><span></span><span class="radio-name">Раздел "Услуги"</span></label>
                                    @else
                                    <label><input type="radio" name="type" value="1"  hidden><span></span><span class="radio-name">Раздел "Товары"</span></label>
                                    <label><input type="radio" name="type" value="0" hidden><span></span><span class="radio-name">Раздел "Услуги"</span></label>
                                    @endif
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left">
                                    <label for="" class="seo_name">SEO</label>
                                </div>
                                <div class="center">
                                    <label class="switch">
                                        <input type="checkbox" id="watch">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="seo_group" id="switch_seo" style="display:none">
                                <div class="item_line">
                                    <div class="left">
                                        <label for="" class="item_name" style="padding-top: 4px">Название на странице категории:</label>
                                    </div>
                                    <div class="center">
                                        <input type="text" name="seoname" class="" placeholder="Название на странице категории" value="{{$category->seoname or ''}}">
                                    </div>
                                    <div class="right">
                                        <div class="help"></div>
                                    </div>
                                </div>
                                <div class="item_line">
                                    <div class="left">
                                        <label for="" class="item_name">Мета-тег Title:</label>
                                    </div>
                                    <div class="center">
                                        <input type="text" name="seotitle" class="" placeholder="" value="{{$category->seotitle or ''}}">
                                    </div>
                                    <div class="right">
                                        <div class="help"></div>
                                    </div>
                                </div>
                                <div class="item_line">
                                    <div class="left">
                                        <label for="" class="item_name">Мета-тег Description:</label>
                                    </div>
                                    <div class="center">
                                        <input type="text" name="seodescription" class="" placeholder="" value="{{$category->seodescription or ''}}">
                                    </div>
                                    <div class="right">
                                        <div class="help"></div>
                                    </div>
                                </div>
                                <div class="item_line">
                                    <div class="left">
                                        <label for="" class="item_name">Мета-тег Keywords:</label>
                                    </div>
                                    <div class="center">
                                        <input type="text" name="seokeywords" class="" placeholder="" value="{{$category->seokeywords or ''}}">
                                    </div>
                                    <div class="right">
                                        <div class="help"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left">
                                    <label for="" class="item_name">Название категории:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="title" class="" placeholder="Введите название категории" value="{{$category->title or ''}}" required="">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left">
                                    <label for="" class="item_name" style="line-height: 14px;">Описание категории:</label>
                                </div>
                                <div class="center">
                                    <textarea rows="10" name="text">{{$category->description or ''}}</textarea>
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left">
                                    <label for="" class="item_name" style="line-height: 14px;">Фотография:</label>
                                </div>
                                <div class="center">
                                    <div class="file-upload">
                                        <label>
                                            <input type="file" name="file">
                                            <span>Выбрать файл</span>
                                        </label>
                                    </div>
                                    <input type="text" id="filename" class="filename" disabled style="border: none;font-size: 12px;">
                                    @if($category !== null)
                                    @if($category->images !== null)
                                    <div class="image_group">
                                        <div class="image">
                                            <img src="{{asset('storage/app')}}/{{$category->images->images or ''}}" alt="">
                                            <a href="{{route('admin.del.cedit')}}?siteID={{$siteID}}&&image={{$category->images->images}}&&id={{$category->id}}&&type=0" class="delete"></a>
                                        </div>
                                    </div>
                                    @endif
                                    @endif
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left">
                                    <label for="" class="item_name">Видео с YouTube:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="youtube" class="" placeholder="Например: http://www.youtube.com/watch?v=9X-8JiOhzX4" value="{{$category->youtube or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <div class="perent" style="margin-top: 40px;">
                                <div class="left">
                                    &nbsp;
                                </div>

                                <!--

                                 Кнопка сохранить и обновить меняются!

                                 На странице добавления категории - добавить
                                 На странице редактировать категорию - Обновить

                                 -->



                                <div class="center">
                                  @if($category !== null)
                                  <input type="submit" class="item_button" value="Обновить">
                                  @else
                                  <input type="submit" class="item_button" value="Добавить">
                                  @endif

                                </div>
                                <div class="right">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script>
    $(document).ready( function() {
        $(".file-upload input[type=file]").change(function(){
            var filename = $(this).val().replace(/.*\\/, "");
            $("#filename").val(filename);
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('input[type="checkbox"]').click(function() {
            if($(this).attr('id') == 'watch') {
                if (this.checked) {
                    $('#switch_seo').show();
                } else {
                    $('#switch_seo').hide();
                }
            }
        });
    });
</script>
</body>
</html>
