@include('admin.admin.include.mainHeader')

    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            @include('admin.admin.include.rmenu')
                <style>

                </style>
                <?php //dd($product); ?>


                <div class="narrow">
                    <div class="box">
                      <a href="{{route('admin.production.goods')}}?siteID={{$siteID}}" class="btn btn-info">← Назад</a>
                        <div class="cp_edit">
                            <div class="title">
                                <h2>{{$product->title or 'Дабавление товара'}}</h2>
                            </div>
                            <div class="perent">
                                <div class="left">
                                    <label for="" class="item_name">Выберите категорию:</label>
                                </div>
                                @if($product !== null)
                                <form action="{{route('admin.update.pedit')}}" method="post" enctype="multipart/form-data">
                                  @else
                                  <form action="{{route('admin.create.pedit')}}" method="post" enctype="multipart/form-data">
                                  @endif
                                  <input type="hidden" name="id" value="{{$product->id or ''}}">
                                  <input type="hidden" name="siteID" value="{{$siteID or ''}}">
                                  {{ csrf_field() }}
                                <div class="center">
                                  <?php //dd($product); ?>
                                    <select class="form-control" name="categories" required>
                                        @forelse($categories as $c)
                                        @if($product !== null && count($product->categories)>0)
                                        <option value="{{$c->id}}" @if($c->id == $product->categories[0]->id)selected="" @endif>{{$c->title}}</option>
                                        @else
                                        <option value="{{$c->id}}">{{$c->title}}</option>
                                        @endif
                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left">
                                    <label for="" class="seo_name">SEO</label>
                                </div>
                                <div class="center">
                                    <label class="switch">
                                        <input type="checkbox" id="watch">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="seo_group" id="switch_seo" style="display:none">
                                <div class="item_line">
                                    <div class="left">
                                        <label for="" class="item_name" style="padding-top: 4px">Название на странице товара или услуги:</label>
                                    </div>
                                    <div class="center">
                                        <input type="text" name="seoname" class="" placeholder="Название на странице категории" value="{{$product->seoname or ''}}" >
                                    </div>
                                    <div class="right">
                                        <div class="help"></div>
                                    </div>
                                </div>
                                <div class="item_line">
                                    <div class="left">
                                        <label for="" class="item_name">Мета-тег Title:</label>
                                    </div>
                                    <div class="center">
                                        <input type="text" name="seotitle" class="" placeholder="" value="{{$product->seotitle or ''}}" >
                                    </div>
                                    <div class="right">
                                        <div class="help"></div>
                                    </div>
                                </div>
                                <div class="item_line">
                                    <div class="left">
                                        <label for="" class="item_name">Мета-тег Description:</label>
                                    </div>
                                    <div class="center">
                                        <input type="text" name="seodescription" class="" placeholder="" value="{{$product->seodescription or ''}}">
                                    </div>
                                    <div class="right">
                                        <div class="help"></div>
                                    </div>
                                </div>
                                <div class="item_line">
                                    <div class="left">
                                        <label for="" class="item_name">Мета-тег Keywords:</label>
                                    </div>
                                    <div class="center">
                                        <input type="text" name="seokeywords" class="" placeholder="" value="{{$product->seokeywords or ''}}">
                                    </div>
                                    <div class="right">
                                        <div class="help"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left">
                                    <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Название товара или услуги:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="title" class="" placeholder="Введите название товара или услуги" value="{{$product->title or ''}}" required="">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <style>
                                .property{}
                                .property .ad-property-input {
                                    margin-top: 10px;
                                    display: table;
                                }
                                .property .ad-property-name {
                                    width: 49%;
                                    margin-right: 1%;
                                    display: inline-block;
                                }
                                .property .ad-property-qty {
                                    width: 19%;
                                    margin-right: 1%;
                                    display: inline-block;
                                }
                                .property .ad-property-type {
                                    width: 30%;
                                    color: #888;
                                    display: inline-block;
                                }
                            </style>
                            <div class="perent">
                                <div class="left">
                                    <label for="">Характеристики</label><br>
                                </div>
                                <div class="center property">
                                    <a class="AddProperty" style="cursor:pointer;display: table;">Добавить характеристику</a>
                                    <!--

                                    характеристики ad-property-input появляются только после нажатия на кнопку "добавить характеристику"

                                    -->

                                    @if($product !== null)
                                    @forelse($product->properties as $pp)
                                    <div class="ad-property-input">
                                        <input type="text" name="UpdateProperty[{{$pp->id}}][]" class="form-control ad-property-name" placeholder="Название характеристики" value="{{$pp->name}}">
                                        <input type="text" name="UpdateProperty[{{$pp->id}}][]" class="form-control ad-property-qty" placeholder="0" value="{{$pp->title}}">
                                        <select class="form-control ad-property-type" name="UpdateProperty[{{$pp->id}}][]">
                                            <option value="" >Выберите</option>
                                            <option value="час" @if($pp->type == 'час') selected @endif)>час</option>
                                            <option value="мм" @if($pp->type == 'мм') selected @endif>мм</option>
                                            <option value="Тонна" @if($pp->type == 'Тонна') selected @endif>Тонна</option>
                                        </select>
                                    </div>
                                    @empty
                                    @endforelse
                                    @endif
                                    <div class="ad-property-input">
                                        <input type="text" name="Property[name][]" class="form-control ad-property-name" placeholder="Название характеристики">
                                        <input type="text" name="Property[title][]" class="form-control ad-property-qty" placeholder="0">
                                        <select class="form-control ad-property-type" name="Property[type][]">
                                            <option value="">Выберите</option>
                                            <option value="час">час</option>
                                            <option value="мм">мм</option>
                                            <option value="Тонна">Тонна</option>
                                        </select>
                                    </div>
                                    <div class="ad-property-input">
                                        <input type="text" name="Property[name][]" class="form-control ad-property-name" placeholder="Название характеристики">
                                        <input type="text" name="Property[title][]" class="form-control ad-property-qty" placeholder="0">
                                        <select class="form-control ad-property-type" name="Property[type][]">
                                            <option value="">Выберите</option>
                                            <option value="час">час</option>
                                            <option value="мм">мм</option>
                                            <option value="Тонна">Тонна</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="perent">
                                <div class="left">
                                    <label for="" class="item_name" style="line-height: 14px;">Описание:</label>
                                </div>
                                <div class="center">
                                    <textarea rows="10" name="short_description">{!! $product->short_description or '' !!}</textarea>
                                    <script>
                                        CKEDITOR.replace('short_description');
                                    </script>
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left">
                                    <label for="" class="item_name">Стоимость:</label>
                                </div>
                                <style>
                                    .price{}
                                    .price .a-price {
                                        width: 30%;
                                        float: left;
                                        display: inline-block;
                                    }
                                    .price .input-part {
                                        background-color: #fff;
                                        float: left;
                                        height: 36px;
                                        border: 1px solid #ccc;
                                        font-size: 14px;
                                        font-weight: 400;
                                        border-radius: 5px;
                                        color: #797979;
                                        margin-left: 3px;
                                        display: inline-block;
                                        align-items: center;
                                        flex-shrink: 0;
                                        width: 40px;
                                        text-align: center;
                                    }
                                    .price .input-part span{
                                        line-height: 36px;
                                    }
                                </style>
                                <div class="center price">
                                    <input type="text" name="coast" class=" a-price" placeholder="0" value="{{$product->coast or ''}}">
                                    <div class="input-part">
                                        <span>
                                            <span class="number-format-font">₽</span>
                                        </span>
                                    </div>

                                </div>
                            </div>
                            <div class="perent">
                                <div class="left">
                                    <label for="" class="item_name" style="line-height: 14px;">Фотографии:</label>
                                </div>
                                <div class="center">
                                    <div class="file-upload">
                                        <label>
                                            <input type="file" name="file">
                                            <span>Выбрать файл</span>
                                        </label>
                                    </div>
                                    <input type="text" id="filename" class="filename" disabled style="border: none;font-size: 12px;">
                                    @if($product !== null)
                                    @if($product->images !== null)
                                    <div class="image_group">
                                        <div class="image">
                                            <img src="{{asset('storage/app')}}/{{$product->images->images or ''}}" alt="">
                                            <a href="{{route('admin.del.cedit')}}?siteID={{$siteID}}&&image={{$product->images->images}}&&id={{$product->id}}&&type=1" class="delete"></a>
                                        </div>
                                    </div>
                                    @endif
                                    @endif
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left">
                                    <label for="" class="item_name">Видео с YouTube:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="youtube" class="" placeholder="Например: http://www.youtube.com/watch?v=9X-8JiOhzX4" value="{{$product->youtube or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <div class="perent" style="margin-top: 40px;">
                                <div class="left">
                                    &nbsp;
                                </div>

                                <!--

                                Кнопка сохранить и обновить меняются!

                                На странице добавления товаров - добавить
                                На странице редактировать товара - Обновить

                                -->

                                <div class="center">
                                  @if($product !== null)
                                  <input type="submit" class="item_button" value="Обновить">
                                  @else
                                  <input type="submit" class="item_button" value="Добавить">
                                  @endif
                                </div>
                                <div class="right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready( function() {
        $(".file-upload input[type=file]").change(function(){
            var filename = $(this).val().replace(/.*\\/, "");
            $("#filename").val(filename);
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('input[type="checkbox"]').click(function() {
            if($(this).attr('id') == 'watch') {
                if (this.checked) {
                    $('#switch_seo').show();
                } else {
                    $('#switch_seo').hide();
                }
            }
        });
    });
</script>
</body>
</html>
