@include('admin.admin.include.mainHeader')

    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            @include('admin.admin.include.rmenu')

            <div class="narrow">
                <div class="box">
                    <div class="production">
                        <div class="title">
                            <div class="left">
                                <h2>Товары</h2>
                            </div>
                            <div class="right">
                                <span class="button-edit"><a href="">Добавить</a></span>
                            </div>
                        </div>
                        <div class="sort">
                            <div class="page_cat">
                                <span><a href="{{route('admin.production.goods')}}?siteID={{$siteID}}&&type=0" class="active">Активные 2</a></span>
                                <span> | </span>
                                <span><a href="">Скрытые 1</a></span>
                                <span> | </span>
                                <span><a href="{{route('admin.production.goods')}}?siteID={{$siteID}}&&type=1">Удаленные</a></span>
                            </div>
                            <div class="sort_block">
                                <div class="category">
                                    <div class="left">
                                        <div class="name">Сортировать по:</div>
                                    </div>
                                    <div class="right">
                                        <select class="sort_select" name="categories[]">
                                            <option value="42" selected="">Категориям</option>
                                            <option value="43">Категория услуг</option>
                                            <option value="61">Ткани</option>
                                            <option value="62">Товары</option>
                                            <option value="63">Название</option>
                                            <option value="64">Налоги</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="price">
                                    <div class="left">
                                        <div class="name">Цена:</div>
                                    </div>
                                    <div class="right">
                                        <input type="text">
                                        <span> - </span>
                                        <input type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @forelse($product as $p)
                        <?php
                        $created = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p->created_at)->addHours(5)->toDateTimeString();
                        $created = date("d-m-Y", strtotime($created));
                        $updated = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p->updated_at)->addHours(5)->toDateTimeString();
                        $updated = date("d-m-Y", strtotime($updated));
                        //dd($p);
                        //($created);
                        ?>
                        <div class="item">
                          <div class="image">
                              <img src="{{asset('storage/app')}}/{{$p->images->images or ''}}" alt="">
                          </div>


                            <div class="info_goods">
                                <div class="block">
                                    <div class="name"><a href=""data-toggle="modal" data-target="#exampleModalLong">{{$p->title or ''}}</a></div>
                                    <div class="price">Цена: <span>{{$p->coast or ''}}</span> руб.</div>
                                </div>
                                <div class="additionally">
                                    <div class="p_category">Категория:: <span>@if(count($p->categories)>0) {{$p->categories[0]->title}} @endif</span></div>
                                      <div class="date">Добавлено: <span>{{$created or ''}}</span><span> | </span> Обновлено: <span>{{$updated or ''}}</span></div>
                                </div>
                            </div>
                            <div class="status">
                                <span class="sign eae @if($p->published == 1)active @endif"></span>
                            </div>
                            <div class="action">
                                <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                    <li><a href="">Скрыть</a></li>
                                    <li><a href="/admin/goods/edit.php">Редактировать</a></li>
                                    <li><a href="">На главную</a></li>
                                </ul>
                            </div>
                        </div>
                        @empty
                        @endforelse
                        <a href="{{route('admin.production.p-edit')}}?siteID={{$siteID}}&&id=0" class="btn btn-primary">Добавить</a>


                    </div>
                </div>
            </div>
        </div>


    </div>



    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content vm-goods">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <div class="modal-body">
                    <div class="content">

                        <div class="vm-goods">
                            <div class="vm-header">
                                <div class="block">
                                    <div class="np">
                                        <div class="left">
                                            <div class="name">Атлас розовый 100D, 15-2216, 80 г/м2, ш.150</div>
                                            <div class="date">№ 1001510, размещено 07 октября 2018</div>
                                        </div>
                                        <div class="right">
                                            <div class="price">690.00 руб.</div>
                                        </div>

                                    </div>

                                    <div class="image preview-pic tab-content">
                                        <div class="tab-pane active" id="pic-1"><img src="http://sun3-4.userapi.com/c543100/v543100824/40f95/O3T7x3Uizdk.jpg" /></div>
                                        <div class="tab-pane" id="pic-2"><img src="http://44.img.avito.st/640x480/4682563244.jpg" /></div>
                                        <div class="tab-pane" id="pic-3"><img src="http://49.img.avito.st/640x480/4682516549.jpg" /></div>
                                        <div class="tab-pane" id="pic-4"><img src="http://77.img.avito.st/640x480/4682520277.jpg" /></div>
                                        <div class="tab-pane" id="pic-5"><img src="http://sun3-4.userapi.com/c543100/v543100824/40f95/O3T7x3Uizdk.jpg" /></div>
                                        <div class="tab-pane" id="pic-6"><img src="http://80.img.avito.st/640x480/4581558980.jpg" /></div>
                                    </div>
                                    <ul class="gallery preview-thumbnail nav nav-tabs">
                                        <li class="active item"><a data-target="#pic-1" data-toggle="tab"><img src="http://sun3-4.userapi.com/c543100/v543100824/40f95/O3T7x3Uizdk.jpg" /></a></li>
                                        <li class="item"><a data-target="#pic-2" data-toggle="tab"><img src="http://44.img.avito.st/640x480/4682563244.jpg" /></a></li>
                                        <li class="item"><a data-target="#pic-3" data-toggle="tab"><img src="http://49.img.avito.st/640x480/4682516549.jpg" /></a></li>
                                        <li class="item"><a data-target="#pic-4" data-toggle="tab"><img src="http://77.img.avito.st/640x480/4682520277.jpg" /></a></li>
                                        <li class="item"><a data-target="#pic-5" data-toggle="tab"><img src="http://sun3-4.userapi.com/c543100/v543100824/40f95/O3T7x3Uizdk.jpg" /></a></li>
                                        <li class="item"><a data-target="#pic-6" data-toggle="tab"><img src="http://80.img.avito.st/640x480/4581558980.jpg" /></a></li>
                                    </ul>

                                </div>
                            </div>

                            <div class="vm-body">
                                <div class="block">
                                    <h3>Характеристики</h3>
                                    <table class="cp-table">
                                        <tbody>
                                        <tr class="cp-tr">
                                            <td class="cp-td ">
                                                <p class="cp-td-p">Диаметр</p>
                                            </td>
                                            <td class="cp-td-val ">
                                                <p class="cp-td-p">110 - 220 мм</p>
                                            </td>
                                        </tr>
                                        <tr class="cp-tr">
                                            <td class="cp-td">
                                                <p class="cp-td-p">Длина</p>
                                            </td>
                                            <td class="cp-td-val">
                                                <p class="cp-td-p">3000 мм</p>
                                            </td>
                                        </tr>
                                        <tr class="cp-tr">
                                            <td class="cp-td">
                                                <p class="cp-td-p">Цвет</p>
                                            </td>
                                            <td class="cp-td-val">
                                                <p class="cp-td-p">серый</p>
                                            </td>
                                        </tr>
                                        <tr class="cp-tr">
                                            <td class="cp-td">
                                                <p class="cp-td-p">Материал</p>
                                            </td>
                                            <td class="cp-td-val">
                                                <p class="cp-td-p">Полипропилен</p>
                                            </td>
                                        </tr>
                                        <tr class="cp-tr">
                                            <td class="cp-td">
                                                <p class="cp-td-p">Макс. температура</p>
                                            </td>
                                            <td class="cp-td-val">
                                                <p class="cp-td-p">+95°С</p>
                                            </td>
                                        </tr>
                                        <tr class="cp-tr">
                                            <td class="cp-td">
                                                <p class="cp-td-p">Толщина </p>
                                            </td>
                                            <td class="cp-td-val">
                                                <p class="cp-td-p">1,8 мм</p>
                                            </td>
                                        </tr>
                                        <tr class="cp-tr">
                                            <td class="cp-td">
                                                <p class="cp-td-p">Гарантия</p>
                                            </td>
                                            <td class="cp-td-val">
                                                <p class="cp-td-p">10 лет</p>
                                            </td>
                                        </tr>
                                        <tr class="cp-tr">
                                            <td class="cp-td">
                                                <p class="cp-td-p">Страна-производитель</p>
                                            </td>
                                            <td class="cp-td-val">
                                                <p class="cp-td-p">Россия</p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <h3>Описание</h3>
                                    <p>Во-первых, смотровая площадка. Самые известные и красивые панорамные фотографии сделаны именно там. И днём, и ночью, и летом, и зимой отсюда видны достопримечательности Москвы. И, конечно, в праздники отсюда лучше всего смотреть салют.</p>
                                    <p>Во-вторых, набережная. Тёплым летним вечером тут много людей, кто-то передвигается на своих двоих, кто-то ездит на роликах, кто крутит педали велосипеда.</p>
                                    <p>Кормить лебедей, послушать музыкальные фонтаны, полежать на траве….»Все, что природа изящного, что искусство прекрасного имеет, — все найдете Вы в Царицыне».</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</body></html>
