@include('admin.admin.include.mainHeader')

    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            @include('admin.admin.include.rmenu')
                <div class="narrow">
                    <div class="box">
                        <div class="production">
                          <div class="title">
                              <div class="left">
                                  <h2>Категории</h2>
                              </div>
                              <div class="right"><span><a href="{{route('admin.production.category')}}?siteID={{$siteID}}&&type=0">Активные</a></span><span> | </span><span><a href="{{route('admin.production.category')}}?siteID={{$siteID}}&&type=1">Удаленные</a></span></div>
                          </div>
                            @forelse($categories as $c)
                            <?php //dd($c); ?>
                            <div class="item">
                                <div class="image">

                                    <img src="{{asset('storage/app')}}/{{$c->images->images or ''}}" alt="">
                                </div>
                                <div class="info">
                                  <div class="block">
                                    <div class="name">{{$c->title}}</div>
                                  </div>
                                  <div class="additionally">
                                    <div class="quantity">Количество товаров в категории: <span><a href="{{route('admin.production.goods')}}?siteID={{$siteID}}&&category={{$c->id}}">{{count($c->goods)}}</a></span> шт.</div>
                                    <div class="type">Раздел: <span>@if($c->type == 0)Услуги @else Товары @endif</span></div>
                                  </div>
                                </div>
                                <div class="status">
                                  <span class="sign eae @if($c->published ==1)none @endif"></span>
                                  <span class="sign search @if($c->seotitle !== null)active @endif"></span>
                                </div>
                                <div class="action">
                                  @if($c->deleted == 1)
                                    <a href="{{route('admin.production.creturn')}}?siteID={{$siteID}}&&id={{$c->id}}" class="edit"></a>
                                    @endif
                                    <a href="{{route('admin.production.c-edit')}}?siteID={{$siteID}}&&id={{$c->id}}" class="edit"></a>
                                    @if($c->deleted == 1)
                                    @if(Auth::user()->userRolle == 1)
                                    <a href="{{route('admin.production.cdelete')}}?siteID={{$siteID}}&&id={{$c->id}}"><i class="fa fa-trash-o" style="color:white;"></i></a>
                                    @endif
                                    @else
                                    <a href="{{route('admin.production.cdelete')}}?siteID={{$siteID}}&&id={{$c->id}}"><i class="fa fa-trash-o" style="color:white;"></i></a>
                                    @endif

                                </div>
                            </div>
                            @empty
                            @endforelse
                            <a href="{{route('admin.production.c-edit')}}?siteID={{$siteID}}&&id=0" class="btn btn-primary">Добавить</a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


</body></html>
