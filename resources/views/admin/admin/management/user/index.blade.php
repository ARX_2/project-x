@include('admin.admin.include.mainHeader')

    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            @include('admin.admin.include.rmenu')
            <?php //dd($users); ?>
                <div class="narrow">
                    <div class="box">
                        <div class="b_user">
                            <h2>Руководители</h2>
                            <form name="home" action="" method="POST">
                                <div class="block">
                                  @forelse($users as $u)
                                    <div class="user">
                                        <div class="image">
                                            <img src="{{asset('storage/app')}}/{{$u->image}}" alt="">
                                        </div>
                                        <div class="info">
                                            <div class="name">{{$u->name}}</div>
                                            <div class="position">@if($u->userRolle == 2)Владелец @elseif($u->userRolle == 3) Администратор @else Мастер @endif</div>
                                            <a href="javascript:void(0);" class="collapsed" id="user{{$u->id}}" data-toggle="collapse" data-target="#c_user{{$u->id}}" aria-expanded="false" aria-controls="collapseTwo">
                                                Информация
                                            </a>
                                            <div id="c_user{{$u->id}}" class="within collapse" aria-labelledby="user{{$u->id}}" data-parent="#accordion">
                                                <div class="personal">
                                                    Телефон: <span>{{$u->tel}}</span>
                                                </div>
                                                <div class="personal">
                                                    Почта: <span>{{$u->email}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="" class="edit"></a>
                                    </div>
                                    @empty
                                    @endforelse
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


</body></html>
