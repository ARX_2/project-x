@include('admin.admin.include.mainHeader')

    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            @include('admin.admin.include.rmenu')
                <div class="narrow">
                    <div class="box">
                        <div class="template">
                            <h2>Макет: шапка сайта</h2>
                            <form name="home" action="" method="POST">
                                <div class="block">
                                    <div class="title">
                                        <h3>Шапка</h3>
                                    </div>
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="banner" value="1" checked> Шапка 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="banner" value="2"> Шапка 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="banner" value="3"> Шапка 3
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </form>
                            <hr>
                            <div class="group-edit-row">
                                <div class="edit-label">&nbsp;</div>
                                <div class="field">
                                    <a href="" class="button-save" type="submit" value="Сохранить">Сохранить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


</body></html>
