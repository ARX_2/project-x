@include('admin.admin.include.mainHeader')

    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            @include('admin.admin.include.rmenu')
                <div class="narrow">
                    <div class="box">
                        <div class="template">
                            <h2>Макет: категории</h2>
                            <form name="home" action="{{route('admin.admin.category.template')}}" method="POST">
                              {{ csrf_field() }}
                              <input type="hidden" name="siteID" value="{{$company->id}}">
                                <div class="block">
                                    <div class="title">
                                        <h3>Категории</h3>
                                    </div>
                                    <?php //dd($company->about->template); ?>
                                    @forelse($company->about->template as $t)
                                    @if($t->template == 5)
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="category" value="1" @if($t->templatename == 'chief_1') checked @endif> Категории 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="category" value="2" @if($t->templatename == 'chief_2') checked @endif> Категории 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="category" value="3" @if($t->templatename == 'chief_3') checked @endif> Категории 3
                                        </label>
                                        <div class="clearfix"></div>

                                    </div>
                                    @endif
                                    @empty
                                    @endforelse
                                </div>


                            <hr>
                            <div class="group-edit-row">
                                <div class="edit-label">&nbsp;</div>
                                <div class="field">
                                    <input href="" class="button-save" type="submit" value="Сохранить">
                                </div>
                            </div>
                              </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


</body></html>
