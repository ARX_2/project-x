@include('admin.admin.include.mainHeader')

    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            @include('admin.admin.include.rmenu')
                <div class="narrow">
                    <div class="box">
                        <div class="template">
                            <h2>Макет: главная страница</h2>
                            <form name="home" action="" method="POST">
                                <div class="block">
                                    <div class="title">
                                        <h3>Выпадающее меню: товары</h3>
                                    </div>
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/b1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="banner" value="1" checked> Товары 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/b2.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="banner" value="2"> Товары 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/b3.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="banner" value="3"> Товары 3
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="block">
                                    <div class="title">
                                        <h3>Выпадающее меню: услуги</h3>
                                    </div>
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="services" value="1" checked> Услуги 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="services" value="2"> Услуги 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="services" value="3"> Услуги 3
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="block">
                                    <div class="title">
                                        <h3>Выпадающее меню: о компании</h3>
                                    </div>
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="about" value="1" checked> О компании 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="about" value="2"> О компании 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="about" value="3"> О компании 3
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </form>
                            <hr>
                            <div class="group-edit-row">
                                <div class="edit-label">&nbsp;</div>
                                <div class="field">
                                    <a href="" class="button-save" type="submit" value="Сохранить">Сохранить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


</body></html>
