@include('admin.admin.include.mainHeader')

    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            @include('admin.admin.include.rmenu')
                <div class="narrow">
                    <div class="box">
                        <div class="template">
                            <h2>Макет: главная страница</h2>
                            <form name="home" action="" method="POST">
                                <div class="block">
                                    <div class="title">
                                        <h3>Баннер</h3>
                                    </div>
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/b1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="banner" value="1" checked> Большой баннер
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/b2.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="banner" value="2"> Два баннера
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/b3.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="banner" value="3"> Баннер + 2 категории
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="block">
                                    <div class="title">
                                        <h3>Категории</h3>
                                    </div>
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="category" value="1" checked> Категория 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="category" value="2"> Категория 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="category" value="3"> Категория 3
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="block">
                                    <div class="title">
                                        <h3>Товары</h3>
                                    </div>
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="product" value="1" checked> Товары 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="product" value="2"> Товары 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="product" value="3"> Товары 3
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="block">
                                    <div class="title">
                                        <h3>Акции</h3>
                                    </div>
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/b1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="action" value="1" checked> Акции 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/b2.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="action" value="2"> Акции 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/b2.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="action" value="3"> Акции 3
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="block">
                                    <div class="title">
                                        <h3>Наши работы</h3>
                                    </div>
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="work" value="1" checked> Наши работы 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="work" value="2"> Наши работы 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="work" value="3"> Наши работы 3
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="block">
                                    <div class="title">
                                        <h3>Обратный звонок</h3>
                                    </div>
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="call" value="1" checked> Обратный звонок 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="call" value="2"> Обратный звонок 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="call" value="3"> Обратный звонок 3
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="block">
                                    <div class="title">
                                        <h3>О компании</h3>
                                    </div>
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="about" value="1" checked> О компании 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="about" value="2"> О компании 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="about" value="3"> О компании 3
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="block">
                                    <div class="title">
                                        <h3>Карта</h3>
                                    </div>
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="map" value="1" checked> Карта 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="map" value="2"> Карта 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="map" value="3"> Карта 3
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </form>
                            <hr>
                            <div class="group-edit-row">
                                <div class="edit-label">&nbsp;</div>
                                <div class="field">
                                    <a href="" class="button-save" type="submit" value="Сохранить">Сохранить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


</body></html>
