@include('admin.admin.include.mainHeader')

    <div class="admin-all">
        @include('admin.admin.include.lmenu')
        <div class="admin-content">
            @include('admin.admin.include.rmenu')
                <div class="narrow">
                    <div class="box">
                        <div class="template">
                            <h2>Макет: контакты</h2>
                            <form name="home" action="{{route('admin.admin.contact.template')}}" method="POST">
                              {{ csrf_field() }}
                              <input type="hidden" name="siteID" value="{{$company->id}}">
                                <div class="block">
                                    <div class="title">
                                        <h3>Контакты</h3>
                                    </div>
                                    @forelse($company->about->template as $t)
                                    @if($t->template == 6)
                                    <div class="row_item">
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="contact" value="1" @if($t->templatename == 'chief_1') checked @endif> Контакты 1
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="contact" value="2" @if($t->templatename == 'chief_2') checked @endif> Контакты 2
                                        </label>
                                        <label class="item">
                                            <div class="image">
                                                <img src="{{asset('public/services/admin/img/k1.jpg')}}" alt="">
                                            </div>
                                            <input type="radio" name="contact" value="3" @if($t->templatename == 'chief_3') checked @endif> Контакты 3
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                    @endif
                                    @empty
                                    @endforelse
                                </div>

                            <hr>
                            <div class="group-edit-row">
                                <div class="edit-label">&nbsp;</div>
                                <div class="field">
                                    <input href="" class="button-save" type="submit" value="Сохранить">
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


</body></html>
