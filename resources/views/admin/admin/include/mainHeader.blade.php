<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Управление сайтом</title>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="{{asset('public/admin/js')}}/jquery-ui.js"></script>
    <script src="{{asset('public/admin/bootstrap-3/js/bootstrap.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/bootstrap-3/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/css/style.css')}}">
    <style>@media print {#ghostery-purple-box {display:none !important}}</style></head>
    <!--<script src="{{ asset('/public/admin/js/ckeditor/ckeditor.js') }}"></script>-->
{{--    <script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>--}}
<script src="{{asset('public/admin/js/ckeditor55/ckeditor.js')}}"></script>
    <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script>

</head>
<body @if(Auth::user()->atmosphere ==1)class="dark"@endif>
@if(Auth::user()->atmosphere ==1)
  @if(Auth::user()->id == 27)
  <div class="Day">
<style media="screen">
  .navbar-default .navbar-nav>li>a, .navbar-default .navbar-brand, .nav>li>a, .fixed-admin-nav .navbar-nav li a, .admin .breadcrumb>.active, .admin .breadcrumb a, .admin h2, .general {color:rgba(255,255,255,0.85);}
  .nav>li>a:hover, .nav .open>a, .nav .open>a:hover, .nav .open>a:focus, .fixed-admin-nav .navbar-nav, .navbar.navbar-inverse,.navbar.navbar-inverse.navbar-fixed-top .nav-header {background-color: #18181899 ;}
  .admin-content, .admin .breadcrumb, .s_action, .c_action{background-color: #222 !important;}
  .admin .navbar-default{border-color: #584e4e;}
  .c_p_company .narrow, .c_p_company .narrow .box, .c_p_company .wide, .dropdown-menu>li>a:hover, .box, .mini_box {background-color:#2f2f2f !important;}
  .user, .site, body, .info .date, .c_p_company .wide .name,.b_user .info .name, .production .item .quantity, .cp_edit .radio-name, .price span, .edit-label, .name, h4,p, .production .action .edit:before{color:rgba(255,255,255,0.85) !important;}
  .template .button-save{border-color:black;}
  body{background-color: #222;}
  .item a, .link a, .rmenu a, .template .item, .info a, .user a, .center a, .right a, .c_p_company .narrow .setting .edit-clarification a,.production .sort_block .category .sort_select, .position{color:#ffdd60 !important;}
  input, button, select, textarea, .c_p_company .narrow .setting .field input, .c_p_company .narrow .setting textarea, .c_p_company .narrow .setting select, .cp_edit input, .cp_edit textarea, .form-control, .cke_chrome{background-color: #222; border-color: rgba(255,255,255,0.2) !important; color:white;}
  .template .item{border-color:#2f2f2f;}
  .c_p_company .rmenu li:hover, .item:hover{background-color: #222 !important;}
  .b_user .user:hover, .admin-content .dropdown-item-edit{background-color: #222;}
  .cacheColor {border-color: #ffdd60 !important; }
  .back, .button-save {background-color:#ffdd60 !important; color: #000 !important;font-weight: 400 !important;}
  .c_action.back span, .s_action.back span {color:black !important;}
 .modal-body, .modal-content .vm-goods{background-color: #181818 !important;}
 .modal_edit .perent .type .right .info{color:white !important;}

</style>
</div>
  @else
    <style>
        body.dark{background:#212121;color:#bbb}
        .dark a,.production .sort a{color:#2986c7}
        .dark .fixed-admin-nav .navbar-nav{background-color:#191919}
        .dark .navbar.navbar-inverse{background:#191919}
        .dark .navbar.navbar-inverse.navbar-fixed-top .nav-header{background:#191919;border-right:1px solid #191919}
        .dark .fixed-admin-nav,.dark .fixed-admin-nav{box-shadow:none}
        .dark .fixed-admin-nav .navbar-nav li a{color:#bbb}
        .dark .fixed-admin-nav .navbar-nav li:hover{background:rgba(233,235,238,0.1)}
        .dark .fixed-admin-nav .navbar-nav li:hover a{color:#fff}
        .dark .navbar.navbar-inverse.navbar-fixed-top .nav-header .logo-wrap .brand-text{color:#fff}
        .dark .navbar.navbar-inverse.navbar-fixed-top .nav-header .logo-wrap .brand-text::after{background:none}
        .dark .admin-content,.dark .admin-content .breadcrumb{background:#212121}
        .dark .admin-content .breadcrumb a{color:#bbb}
        .dark .admin-content .breadcrumb>.active{color:#bbb}
        .dark .c_p_company .narrow .box{background:#292929;border:1px solid #323232;box-shadow:none}
        .dark .dropdown-menu{background-color:#353535;border:1px solid #3a3a3a}
        .dark .dropdown-menu>li>a{color:#bbb}
        .dark .dropdown-menu>li>a:hover,.dark .dropdown-menu>li>a:focus{color:#ccc;background-color:#434343}
        .dark .production .page_cat{background:#212121}
        .dark .production .button-edit{background:#2986c7}
        .dark .mobile-only-brand input[type="search"]{background:#212121;border:1px solid #292929;color:#bbb}
        .dark .production .sort_block .category .sort_select,.dark .production .sort_block .price input,.dark .form-control,.dark .cp_edit input,.dark .cp_edit textarea,.dark .cp_edit .perent .price .input-part{background:#3a3a3a;border:1px solid #4b4b4b;border-bottom:1px solid #4b4b4b;color:#bbb}
        .dark .production .item:hover{background-color:#313131}
        .dark .production .info_goods .price span,.dark .production .info_goods .p_category span,.dark .production .info_goods .date span,.dark .production .info_goods .p_category,.dark .production .info_goods .date,.dark .production .info_goods .price,.dark .cp_edit .seo_group .left .item_name,.dark .mobile-only-brand .search-button:after{color:#bbb}
        .dark .production .info .quantity,.dark .production .info .type,.dark .production .action .edit:before{color:#bbb}
        .dark .production .sort a.active{color:#bbb}
        .dark .production .status .active{color:#67893c}
        .dark .production .trash:before,.dark .production .status .none{color:#cc3e4a}
        .dark .production .star:before{color:#de952c}
        .dark .production .sub_item .arrows-v:hover{background:#3a3a3a}
        .dark .production .sub_item .item:last-child {border-bottom: 1px solid #3a3a3a;}
        .dark .pagination>li>a{background-color:#292929;border:1px solid #bbb}
        .dark .pagination>.disabled>span,.dark .pagination>.disabled>span:hover,.dark .pagination>.disabled>span:focus,.dark .pagination>.disabled>a,.dark .pagination>.disabled>a:hover,.dark .pagination>.disabled>a:focus{background-color:#292929;border-color:#bbb}
        .dark .pagination>li>a:hover,.dark .pagination>li>span:hover,.dark .pagination>li>a:focus,.dark .pagination>li>span:focus{border:1px solid #fff;z-index:2}
        .dark .c_p_company .wide .box{background:#292929;border: 1px solid #323232}
        .dark .c_p_company .narrow .setting .field input{background:#3a3a3a;border:1px solid #4b4b4b;border-bottom:1px solid #4b4b4b;color:#bbb}
        .dark .c_p_company .narrow .setting textarea{background:#3a3a3a;border:1px solid #4b4b4b;border-bottom:1px solid #4b4b4b;color:#bbb}
        .dark .c_p_company .narrow .setting select{background:#3a3a3a;border:1px solid #4b4b4b;border-bottom:1px solid #4b4b4b;color:#bbb}
        .dark .cp_edit .filename{background:#292929}
        .dark .cp_edit .radio-name{color:#ddd}
        .dark .cp_edit .seo_group{border-bottom:1px solid #3a3a3a}
        .dark .cp_edit .button-box input[type="checkbox"] + span,input[type="radio"] + span{background:#fff}
        .dark .admin-content hr{border-top:1px solid #3a3a3a}
        .dark .narrow_title{border-bottom:1px solid #3a3a3a}
        .dark .wide_title{border-bottom:1px solid #3a3a3a}
        .dark .cke_editable,.dark .cke_editable_themed,.dark .cke_contents_ltr{background:#292929!important;background-color:#292929!important}
        .dark .wide_title h2,.dark .c_p_company .narrow .setting .edit-label{color:#bbb}
        .dark .tizers .info .name{color:#bbb}
        .dark .tizers .info .desc{color:#bbb}
        .dark .tizers .item{border-bottom:1px solid #3a3a3a}
        .dark .tizers .item:hover{background:#313131}
        .dark .document .info .name{color:#bbb}
        .dark .document .item{border-bottom:1px solid #3a3a3a}
        .dark .document .item:hover{background:#313131}
        .dark .production .item{border-bottom:1px solid #3a3a3a}
        .dark .production .info .name{color:#bbb}
        .dark .production .info .quantity_cat{color:#bbb}
        .dark .production .info_goods .name{color:#bbb}
        .dark .pricelist .block{border:1px solid #3a3a3a}
        .dark .pricelist .block .item:hover{background:#313131}
        .atmosphere:hover{background:none!important}
        .fixed-admin-nav .navbar-nav li a:hover{color:#fff}
        .cp_edit .switch .slider{background-color:#2196F3}
        .cp_edit .switch input:checked + .slider{background-color:#f0ad4e}
    </style>
    @endif
@endif
