<?php $ava = auth::user()->image; ?>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="mobile-only-brand pull-left">
        <div class="nav-header pull-left">
            <div class="logo-wrap">
                <a href="/admin" title="Перейти на главную страницу">
                    <img class="brand-img" src="{{asset('storage/app')}}/{{$company->about->infoImage->favicon or '/logo.png'}}" alt="">
                    <span class="brand-text">{{$company->name or ''}} </span>
                </a>
            </div>
        </div>
        <style>
            .on_site{    display: inline-block;
                position: relative;
                margin-left: 30px;
                height: 70px;
                line-height: 70px;}
            .on_site a:hover{list-style-type: none;text-decoration: none;}
            .on_site i{    height: 70px;
                font-size: 20px;
                display: block;
                line-height: 70px;
            color: #269ffd}
        </style>

        <div class="on_site"><a href="/" target="_blank" title="Перейти на сайт"><i class="fa fa-sign-out" aria-hidden="true"></i></a></div>
        <div class="navbar-search ml-20">
            <form action="/admin/search/index" method="get">
              <input type="search" name="search" value="" placeholder="Поиск по сайту" autocomplete="off" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <button class="search-button" type="submit"></button>
            <div class="dropdown-menu dropsearch" aria-labelledby="dropdownMenuButton" style="width:150%;">
            </div>
            </form>
        </div>
    </div>
    <div id="mobile_only_nav" class="mobile-only-nav pull-right">
        <ul class="nav navbar-right top-nav pull-right">
            <li class="dropdown auth-drp">
                <a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"  title="Личный кабинет">
                    <img src="/storage/app/{{$ava or 'user.png'}}" alt="user_auth" class="user-auth-img img-circle">
                    <span class="user-online-status"></span>
                </a>
                <ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                    <li><a href="{{route('admin.profile')}}"><i class="zmdi zmdi-account"></i><span>Личный кабинет</span></a></li>
                    <li><a href="{{route('admin.profile.edit')}}"><i class="zmdi zmdi-card"></i><span>Сменить пароль</span></a></li>
                    <li class="divider"></li>
                    <li><a href="/logout/"><i class="zmdi zmdi-power"></i><span>Выйти</span></a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<script type="text/javascript">
  $('input[name="search"]').on('input', function(){
    var title = $(this).val();
    if(title == ''){
      title = false;
    }
    $.get('/admin/search/'+title, function(data){
      $('.dropsearch').html(data);
    });
  });
  $('.dropsearch').on('click', '.fa-star', function(){

  });
  // $('input[name="search"]').keydown(function (e) {
  //   if(e.keyCode == 13){
  //
  //   }
  // });
</script>
<style media="screen">
.drop-item{
  height:3vh;
line-height: 30px;;
}

.dropsearch div:hover{
  background-color: rgb(233,233,233);
}
</style>
