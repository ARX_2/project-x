<?php //dd($company); ?>
<div class="fixed-admin-nav ">
    <div class="scroll">
        <div class="navbar-nav">
            <style>
                .status_payment{padding: 5px 30px 10px}
                .lifetime-bar-container {
                    display: block;
                    width: 100%;
                    cursor: pointer;
                    padding-bottom: 2px;
                }
                .lifetime-bar {
                    height: 6px;
                    position: relative;
                    border-radius: 5px;
                }
                .status_payment .date{
                    font-size: 14px;
                    color: #666;
                    margin-bottom: 5px;
                }
                .meter-progress {
                    position: absolute;
                    border-radius: 5px;
                    left: 0;
                    top: 0;
                    bottom: 0;
                    width: 95%;
                }

                .meter_status-1{background-color:#ffcfd0}
                .meter_status-1 .meter-progress{background-color:#ff6163}
                .meter_status-2{background-color:#ffe7bc}
                .meter_status-2 .meter-progress{background-color:#ffb020}
                .meter_status-3{background-color:#eef7dc}
                .meter_status-3 .meter-progress{background-color:#97cf27}
                .meter_status-4{background-color:#f3f3f3}
                .meter_status-4 .meter-progress{background-color:#d6d6d6}

            </style>
            <div class="status_payment">
                <div class="date">Осталось: 365 дней</div>
                <span class="lifetime-bar-container">
                    <div class="lifetime-bar meter_status-3">
                        <div class="meter-progress"></div>
                    </div>
                </span>
            </div>
            <ul>
                <li class="navigation-header"><span>Главное</span></li>
                <style>
                    .a_s{}
                    .a_s .parent_s{display: table;position: relative;width: 100%}
                    .a_s .parent_s a{width: 100%;display: table;}
                    .a_s .parent_s i{display: table-cell;vertical-align: middle;min-width: 18px;max-width: 18px;}
                    .a_s .parent_s .name{display: table-cell;padding: 0 15px;width: 100%;}
                    .a_s .parent_s .new_name{color: #888;font-size: 12px}
                    .a_s .parent_s a:after {font-family: FontAwesome;content: "\f105";display: table-cell;vertical-align: middle;font-weight: 400;color: #666;}
                    .a_s .parent_s.more a:after {content: "\f106"}
                    .a_s .parent_s.more.collapsed a:after {content: "\f107"}
                    .c_lvl_1 li{display: table;position: relative;width: 100%}
                    .c_lvl_1 li a{width: 100%;display: table;}
                    .c_lvl_1 li i{display: table-cell;vertical-align: middle}
                    .c_lvl_1 li .name{display: table-cell;padding: 0 15px;width: 100%;}
                    .c_lvl_1 li .new_name{color: #888;font-size: 12px}
                    .c_lvl_1 li a:before {font-family: FontAwesome;content: "\f111 ";color: #888;font-size: 5px;vertical-align: middle;width: 18px;display: table-cell;min-width: 18px;padding: 0 4px;max-width: 18px;}
                    .c_lvl_1 li a:after {font-family: FontAwesome;content: "\f105";display: table-cell;vertical-align: middle;font-weight: 400;color: #666;}
                    .c_lvl_1 .add_section{background: #269ffd;color: #fff!important;}
                    .c_lvl_1 .add_section:before{color: #fff}
                    .c_lvl_1 .add_section:after{color: #fff}
                </style>
                {{--<li class="a_s">
                    <div class="parent_s">
                        <a href="{{route('admin.tape.index')}}">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                            <span class="name">Лента новостей</span>
                        </a>
                    </div>
                </li>--}}
                {{--<li class="a_s">
                    <div class="parent_s">
                        <a href="javascript:void(0);">
                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                            <span class="name">Статистика</span>
                        </a>
                    </div>
                </li>--}}
                <li class="a_s">
                    <div class="parent_s">
                        <a href="{{route('admin.orders.index')}}" class="name_link">
                            <i class="fa fa-rub" aria-hidden="true"></i>
                            <span class="name">Заказы</span>
                        </a>
                    </div>
                </li>
                {{--<li class="a_s">
                    <div class="parent_s">
                        <a href="javascript:void(0);">
                            <i class="fa fa-bullhorn" aria-hidden="true"></i>
                            <span class="name">Реклама</span>
                        </a>
                    </div>
                </li>--}}
            </ul>
            <ul>
                <li class="navigation-header"><span>Контент</span></li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="{{route('admin.category.index')}}" class="name_link">
                            <i class="fa fa-th-list" aria-hidden="true"></i>
                            <span class="name">Категории</span>
                        </a>
                    </div>
                </li>


                @forelse($company->sections as $section)
                @if($section->published == 1)
                <li class="a_s">
                    <div class="parent_s">
                        <a href="{{route('admin.'.$section->slug.'.index')}}" class="name_link">
                            @if($section->type == 0)<i class="fa fa-cubes" aria-hidden="true"></i>@endif
                            @if($section->type == 1)<i class="fa fa-handshake-o" aria-hidden="true"></i>@endif

                            @if($section->type == 4)<i class="fa fa-home" aria-hidden="true"></i>@endif
                            @if($section->type == 5)<i class="fa fa-home" aria-hidden="true"></i>@endif
                            @if($section->type == 6)<i class="fa fa-home" aria-hidden="true"></i>@endif
                            @if($section->type == 7)<i class="fa fa-bed" aria-hidden="true"></i>@endif
                            <div class="name">{{$section->title or ''}}<div class="new_name">{{$section->oldtitle or ''}}</div></div>
                        </a>
                    </div>
                </li>
                @endif
                @empty
                @endforelse
                <li class="a_s">
                    <div class="parent_s more collapsed toggle" id="ASection_1" data-toggle="collapse" data-target="#ASection-1" aria-expanded="false" aria-controls="ASection-1">
                        <a href="javascript:void(0);">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                            <span class="name toggle">Разделы</span>
                        </a>
                    </div>
                    <ul id="ASection-1" class="c_lvl_1 collapse" aria-labelledby="ASection_1" data-parent="#accordion">
                        <li><a href="{{route('admin.banner.index')}}" class="name_link"><div class="name">Баннер</div></a></li>
                        <li><a href="{{route('admin.tizers.index')}}" class="name_link"><div class="name">Тизеры</div></a></li>
                        <li><a href="{{route('admin.feedback.index')}}"><div class="name">Отзывы</div></a></li>
                        {!! Resize::sections() !!}
                        <li><a href="{{route('admin.sections.index')}}" class="add_section"><div class="name">Добавить</div></a></li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li class="navigation-header"><span>Компания</span></li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="{{route('admin.company.index')}}">
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                            <div class="name">Основ. информация</div>
                        </a>
                    </div>
                </li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="{{route('admin.employee.index')}}">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <div class="name">Сотрудники</div>
                        </a>
                    </div>
                </li>
            </ul>
            <ul>
                <li class="navigation-header"><span>Настройки</span></li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="javascript:void(0);">
                            <i class="fa fa-toggle-on" aria-hidden="true"></i>
                            <span class="name">Счет и оплата</span>
                        </a>
                    </div>
                </li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="/admin/support">
                            <i class="fa fa-handshake-o" aria-hidden="true"></i>
                            <span class="name" id="support">Поддержка</span>
                        </a>
                    </div>
                </li>
                @if(Auth::user()->userRolle == 1)
                    <li class="a_s">
                        <div class="parent_s">
                            <a href="/admin/settings/site/index">
                                <i class="fa fa-wrench" aria-hidden="true"></i>
                                <div class="name">Верификация и настройка сайта</div>
                            </a>
                        </div>
                    </li>
                @endif
            </ul>
            @if(Auth::user()->userRolle == 1)
            <ul>
                <li class="navigation-header"><span>Разработчик</span></li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="{{route('admin.admin.index')}}">
                            <i class="fa fa-refresh" aria-hidden="true"></i>
                            <span class="name">Изменить сайт</span>
                        </a>
                    </div>
                </li>
            </ul>
            @endif
        </div>
    </div>
</div>