@include('admin.index')s
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление документами</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Документы</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        {{--<div class="item">
                            <div class="name">
                                <a href=""></a>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="sections sm-50">
                        <div class="narrow_title">
                            @if($doc == null)
                                <h2>Добавление документа</h2>
                            @else
                                <h2>Редактирование документа: {{$doc->title or ''}}</h2>
                            @endif
                        </div>
                        <div class="document">
                          @if($doc == null)
                          <form class="" action="{{route('admin.documentation.store')}}" method="post" enctype="multipart/form-data">
                            @else
                            <form class="" action="{{route('admin.documentation.update', $doc)}}" method="post" enctype="multipart/form-data">
                              <input type="hidden" name="_method" value="put">
                            @endif
                            {{ csrf_field() }}
                            <div class="edit">
                                <div class="edit_group">
                                    <div class="name">Выберите тип загружаемого документа:</div>
                                    <div class="row_item">
                                        <label class="type">
                                            <div class="image">
                                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                            </div>
                                            <input type="radio" name="type" value="pdf" @if($doc!==null)@if($doc->type == 'pdf') checked @endif @endif> PDF
                                        </label>
                                        <label class="type">
                                            <div class="image">
                                                <i class="fa fa-file-word-o" aria-hidden="true"></i>
                                            </div>
                                            <input type="radio" name="type" value="word" @if($doc!==null)@if($doc->type == 'word') checked @endif @endif> WORD
                                        </label>
                                        <label class="type">
                                            <div class="image">
                                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                            </div>
                                            <input type="radio" name="type" value="excel" @if($doc!==null)@if($doc->type == 'excel') checked @endif @endif> EXCEL
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="edit_group">
                                    <div class="name">Название:</div>
                                    <div class="row_item">
                                        <input type="text" name="title" class="section_input" placeholder="Введите название документа" value="{{$doc->title or ''}}" required="">
                                    </div>
                                </div>
                                <div class="edit_group">
                                    <div class="name">Документ</div>
                                    <div class="row_item">
                                        <input type="file" class="upload-logo" name="file">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="edit_grout">
                                <div class="row_item">
                                    <input class="button-save" type="submit" value="Сохранить">
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
