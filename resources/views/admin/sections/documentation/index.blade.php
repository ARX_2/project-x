@include('admin.index')
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление документами</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Документы</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        <div class="item">
                            <div class="name">
                                <a href="/admin/admin/company/upload?type=14">Описание на странице документов</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="name">
                                <a href="" data-toggle="modal" data-target="#editsection">Переименовать раздел</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="sections">
                        <div class="narrow_title">
                            <div class="left">
                                <h2>Документы</h2>
                            </div>
                            <div class="right">
                                <span class="button-edit"><a href="{{route('admin.documentation.create')}}">Добавить</a></span>
                            </div>
                        </div>
                        <style>

                            .status .sign {
                                margin-right: 5px;
                                color: #666;
                            }
                            .status .sign.activeStar {
                                color: #fec107;
                            }
                            .star:before {
                                font-family: FontAwesome;
                                content: "\f005";
                            }
                        </style>
                        <div class="document">
                            @forelse($docs as $doc)
                            <div class="item">
                                <div class="image">
                                    <i class="fa fa-file-{{$doc->type}}-o" aria-hidden="true"></i>
                                </div>
                                <div class="info">
                                    <div class="name">
                                        {{$doc->title}}
                                    </div>
                                    <div class="additionally">
                                        <div class="date">Обновлено: @if($doc->updated_at != null)<span>{{Resize::date($doc->updated_at)}}</span>@endif</div>
                                    </div>
                                </div>
                                <div class="size">
                                    {{$doc->size}} KB
                                </div>
                                <div class="status">
                                    <span class="sign star @if($doc->popular == 1)activeStar @endif"><a href="{{route('admin.documentation.update.id', ['popular',$doc->id,(int)$doc->popular])}}"></a></span>
                                </div>
                                <div class="action">
                                    <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                    <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                        <li>
                                            <form onsubmit="if(confirm('Удалить?')) {return true}else{return false}" action="{{route('admin.documentation.destroy', $doc)}}" method="post">
                                                <input type="hidden" name="_method" value="DELETE">{{csrf_field()}}
                                                <button type="submit" name="button" style="border: none;background: #fff;display: block;padding: 3px 20px;clear: both;font-weight: normal;line-height: 1.42857143;color: #333;white-space: nowrap;font-size: 15px;">Удалить</button>
                                            </form>
                                        </li>
                                        <li><a href="{{route('admin.documentation.edit', $doc)}}">Редактировать</a></li>
                                    </ul>
                                </div>
                            </div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
            <style>
                .document .item{width:100%;position:relative;padding:10px;border-bottom:1px solid #ddd;display:inline-flex}
                .document .item:hover{background:#e9ebee}
                .document .image{font-family:FontAwesome;font-size:20px;margin-right:15px;vertical-align:middle;background:#269ffd;color:#fff;border-radius:3px;width:40px;min-width:40px;text-align:center;display:inline-block}
                .document .item .image i{font-size:30px;padding:7px}
                .document .item .info{width:70%;padding-right:30px;position:relative}
                .document .info .additionally{position:absolute;left:0;bottom:0}
                .document .info .date{color:#666;margin-top:3px;font-size:12px;line-height:14px}
                .document .info .date span{color:#888;font-weight:600}
                .document .item .name{padding-right:30px;color:#333;font-weight:600;font-size:14px;line-height:16px}
                .document .item .size{font-size:12px;color:#888;font-weight:600;width:15%}
                .document .item .action{text-align:right;width:10%}
                .document .action .edit:before{font-family:FontAwesome;color:#666;content:"\f141"}
                .document .edit_group{margin-bottom:20px}
                .document .edit{margin:40px 0}
                .document .edit .name{margin-bottom:10px;font-weight:600}
                .document .edit .type{width:32%;margin:.5%;padding:10px .5%;text-align:center;color:#428bca;float:left;font-weight:400;border:1px solid #eee}
                .document .edit .image{margin-bottom:10px}
                .document .edit .image i{font-size:30px}
                .document .edit .row_item label{display:inline-block;display:inline-block}
                .document .edit .type img{width:100%;height:300px;object-fit:contain}
            </style>
        </div>
    </div>

    <div class="modal fade" id="editsection" tabindex="-1" role="dialog" aria-labelledby="editsection" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content vm-goods">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="modal-body modal_edit">
                    <div class="content">
                        <div class="vm-header">
                            <div class="name">Переименовать раздел "Документы"</div>
                        </div>
                        <div class="vm-body">
                            <div class="vm-edit">
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Название раздела:</label></div>
                                    <div class="center">
                                        <input type="text" name="" class="" placeholder="Документы" value="">
                                    </div>
                                </div>
                                <div class="perent" style="margin-bottom: 0">
                                    <div class="left"><label for="" class="item_name"></label></div>
                                    <div class="center">
                                        <input class="button-save" type="submit" value="Сохранить" style="width: 115px;background-color: #269ffd;color: #fff;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $('.star').click(function(){
        var link = $(this).find('a').attr('href');
        var check =  $(this).hasClass('activeStar');
        if(check == true){
            $(this).removeClass('activeStar');
        }
        else{
            $(this).addClass('activeStar');
        }
        $.get(link, function(data){});

    });
</script>
