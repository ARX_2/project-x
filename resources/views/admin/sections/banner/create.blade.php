        <div class="row">
            <div class="col-lg-4">
                <h2>Управление баннером</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Баннер</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">

                    </div>
                </div>
            </div>
            @if($banner !== null)
            <form class="" action="{{route('admin.banner.update', ['update',$banner->id,0])}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_method" value="put">
            @else
            <form class="" action="{{route('admin.banner.store')}}" method="post" enctype="multipart/form-data">
            @endif
            {{ csrf_field() }}
            <div class="narrow">
                <div class="box">
                    <div class="cp_edit">
                        <div class="narrow_title">
                            <h2>Редактирование баннера: <span id="edit-title">{{$banner->title or ''}}</span></h2>

                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Название:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="title" class="" placeholder="Введите название баннера" value="{{$banner->title or ''}}" required="">
                            </div>
                            <div class="right"></div>
                        </div>
                        <script type="text/javascript">
                        $('input[name="title"]').on('input',function(){
                          var title = $(this).val();
                          $('#edit-title').text(title);
                        });
                        $('input[name="title"]')
                        .on({
                          blur: function() {
                            $.get('/admin/banner/title/{{$banner->id}}/'+this.value);
                          }
                        });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Описание:</label>
                            </div>
                            <div class="center">
                                <textarea rows="10" name="text">{{$banner->text or ''}}</textarea>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('textarea[name="text"]')
                        .on({
                          blur: function() {
                            $.get('/admin/banner/text/{{$banner->id}}/'+this.value);
                          }
                        });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Фотография:</label>
                            </div>
                            <div class="center">
                                <div class="file-upload">
                                    <label>
                                        <input id="sortpicture" type="file" name="sortpic" />
                                        <span>Выбрать файл</span>
                                    </label>
                                </div>
                                <input type="text" id="filename" class="filename" disabled="" style="border: none;font-size: 12px;">
                                <div class="image_group">
                                  <div class="imgs">
                                  @if($banner->image !== null)
                                    <div class="image">

                                        <img src="{{asset('storage/app')}}/{{$banner->image or ''}}" alt="">
                                    </div>
                                    @endif
                                  </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('#sortpicture').on('change',function() {
                          //alert('asd');
                          var i = 1;
                          var files = $('#sortpicture').prop('files');
                          $.each(files, function (index, value) {
                            var file_data = value;
                            var form_data = new FormData();
                            form_data.append('file', file_data);
                            form_data.append('id', {{$banner->id}});
                            $.ajax({
                                        url: '/admin/image/banner/upload',
                                        dataType: 'text',
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'post',
                                        success: function(data){
                                          data = $.parseJSON(data);
                                            $('.imgs').html('<div class="image_group" id="image'+i+'" style="width:auto;float:left;"><div class="image"><img src="{{asset('storage/app')}}/'+data.image+'" alt=""></div></div>');
                                            $('#image'+i).click(function(){

                                              $.get('{{route('admin.image.goods.destroy')}}?id='+data.id,function(data){$('#image'+i).remove();});
                                            });
                                        }
                             });
                             i++;
                          });

                        });

                        </script>
                        <div class="perent" style="margin-top: 40px;">
                            <div class="left">
                                &nbsp;
                            </div>

                            <div class="center">
                                <input type="button" class="item_button" value="Обновить">
                            </div>
                            <div class="right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </form>
        </div>
        <script type="text/javascript">
          $('.item_button').click(function(){
            $.get('/admin/banner/status/{{$banner->id}}/2', function(data){
              $('.admin-content').html(data);
            });
          });
        </script>
        <script>
            $(document).ready( function() {
                $(".file-upload input[type=file]").change(function(){
                    var filename = $(this).val().replace(/.*\\/, "");
                    $("#filename").val(filename);
                });
            });
        </script>
