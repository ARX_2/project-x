        <div class="row">
            <div class="col-lg-4">
                <h2>Управление баннером</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Баннер</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">

                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="narrow_title">
                        <div class="left">
                            <i class="fa fa-refresh" aria-hidden="true" style="float:left; font-size:20px;color:#14aae6;padding: 5px;border-radius: 50px;" id="refresh"></i>
                            <h2 style="float:left;margin-left:10px;margin-top:5px;">Баннер</h2>
                            <p class="clearFix"></p>
                            <style media="screen">
                              #refresh:hover{
                                background-color: #e7e6e6;
                              }
                            </style>
                        </div>
                        <div class="right">
                            <span class="button-edit"><a href="{{route('admin.banner.create')}}">Добавить</a></span>
                        </div>
                    </div>
                    <style>
                        .production.big .image {width: 105px;min-width: 105px;height: 80px;}
                    </style>
                    <div class="production big">
                    @forelse($banner as $b)
                    <div class="item">
                        <div class="image">
                            <img src="{{asset('storage/app')}}/{{$b->image or '/public/none-img.png'}}" alt="">
                        </div>
                        <div class="info">
                            <div class="block">
                                <div class="name">{{$b->title or 'Без названия'}}</div>
                                <div class="offer">
                                    {{$b->text or 'Без описания'}}
                                </div>
                            </div>
                            <div class="additionally">
                                <div class="date">Обновлено: <span>{{Resize::date($b->updated_at)}}</span></div>
                            </div>
                        </div>
                        <div class="status">
                            <span class="sign eae @if($b->published == 1) active @endif"><a href="{{route('admin.banner.update', ['published',$b->id,(int)$b->published])}}"></a></span>
                        </div>
                        <div class="action">
                            <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                            <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                <li><a href="{{route('admin.banner.edit', $b)}}" class="link_edit">Редактировать</a></li>
                            </ul>
                        </div>
                    </div>
                    @empty
                    @endforelse
                    </div>
                </div>
            </div>
        </div>

        <script>

        $('#refresh').click(function(){
          $(this).addClass('rotate');
          $.get('{{$link or ''}}?ajax=true', function(data){
            $('.admin-content').html(data);
          });
        });


        $('.button-edit').click(function(){
          var link = $('.button-edit').find('a').attr('href');
            $.get(link+'?ajax=true', function(data){
              $('.admin-content').html(data);
            });
            history.pushState(null, null, link);
            return false;
        });

        $('.link_edit').click(function(){
          var link = $(this).attr('href');
            $.get(link+'?ajax=true', function(data){
              console.log(data);
              $('.admin-content').html(data);
            });
            history.pushState(null, null, link);
            return false;
        });
        $('.eae').click(function(){
          var link = $(this).find('a').attr('href');
          var check = $(this).hasClass('active');

          if(check == true){
            $(this).removeClass('active');
          }
          else{
            $(this).addClass('active');
          }
          $.get(link, function(data){});

        });
        </script>
        <style media="screen">
          .rotate{
            -webkit-animation-name: cog;
    -webkit-animation-duration: 1s;
    -webkit-animation-iteration-count: infinite;
    -webkit-animation-timing-function: linear;
    -moz-animation-name: cog;
    -moz-animation-duration: 1s;
    -moz-animation-iteration-count: infinite;
    -moz-animation-timing-function: linear;
    -ms-animation-name: cog;
    -ms-animation-duration: 1s;
    -ms-animation-iteration-count: infinite;
    -ms-animation-timing-function: linear;

    animation-name: cog;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
    }
    @-ms-keyframes cog {
    from { -ms-transform: rotate(0deg); }
    to { -ms-transform: rotate(20deg); }
    }
    @-moz-keyframes cog {
    from { -moz-transform: rotate(0deg); }
    to { -moz-transform: rotate(20deg); }
    }
    @-webkit-keyframes cog {
    from { -webkit-transform: rotate(0deg); }
    to { -webkit-transform: rotate(20deg); }
    }
    @keyframes cog {
    from {
    transform:rotate(0deg);
    }
    to {
    transform:rotate(360deg);
    }
          }
        </style>
