
@include('admin.index')
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление портфолио</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">{{$company->info->section2 or 'Порфтолио'}}</li>
                </ol>
            </div>
        </div>
        <hr>
        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        <div class="item">
                            <div class="name">
                                <a href="/admin/admin/company/upload?type=6">Описание на странице портфолио</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="name">
                                <a href="" data-toggle="modal" data-target="#editsection">Переименовать раздел</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="narrow_title">
                        <div class="left">
                            <h2>{{$company->info->section2 or 'Порфтолио'}}</h2>
                        </div>
                        <div class="right">
                            <span class="button-edit"><a href="{{route('admin.object.create')}}">Добавить</a></span>
                        </div>
                    </div>
                    <div class="production medium">
                        <div class="sort">
                            <div class="page_cat">
                                <span><a href="{{route('admin.object.index')}}" class="active">Активные {{$countShow or ''}}</a></span>
                                <span> | </span>
                                <span><a href="{{route('admin.object.hidden')}}">Скрытые {{$countHidden or ''}}</a></span>
                            </div>
                        </div>
                        @forelse($objects as $object)
                        <div class="item">
                            <div class="image">
                                <img src="{{asset('storage/app')}}/{{$object->images[0]->images or '/public/none-img.png'}}" alt="">
                            </div>
                            <div class="info">
                                <div class="block">
                                    <div class="name">{{$object->title}}</div>
                                </div>
                                <div class="additionally">
                                    <div class="type">Категория: <span>{{$object->category->title}}</span></div>
                                    <div class="date">Обновлено: <span>{{Resize::date($object->updated_at)}}</span></div>
                                </div>
                            </div>
                            <div class="status">
                                <span class="sign eae @if($object->published == 1)active @endif eaecat"><a href="{{route('admin.object.update.id', ['published',$object->id,(int)$object->published])}}"></a></span>
                                <span class="sign star @if($object->main == 1)activeStar @endif"><a href="{{route('admin.object.update.id', ['main',$object->id,(int)$object->main])}}"></a></span>
                            </div>
                            <div class="action">
                                <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                    <li><a href="{{route('admin.object.edit', $object)}}?id={{$object->id}}">Редактировать</a></li>
                                </ul>
                            </div>
                        </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editsection" tabindex="-1" role="dialog" aria-labelledby="editsection" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content vm-goods">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <div class="modal-body modal_edit">
                <div class="content">
                    <div class="vm-header">
                        <div class="name">Переименовать раздел "Портфолио"</div>
                    </div>
                    <div class="vm-body">
                        <div class="vm-edit">
                            <form class="" action="{{route('admin.change.section')}}" method="post">
                                {{ csrf_field()}}
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Название раздела:</label></div>
                                    <div class="center">
                                        <input type="text" name="objects" class="" placeholder="Портфолио" value="">
                                    </div>
                                </div>
                                <div class="perent" style="margin-bottom: 0">
                                    <div class="left"><label for="" class="item_name"></label></div>
                                    <div class="center">
                                        <input class="button-save" id="saveSection" type="submit" value="Сохранить" style="width: 115px;background-color: #269ffd;color: #fff;">
                                    </div>
                                </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.eae').click(function(){
        var link = $(this).find('a').attr('href');
        var checkClass = $(this).hasClass('eaecat');
        var count_active = $('#count_active').html();
        count_active = Number.parseInt(count_active);
        var count_hidden = $('#count_hidden').html();
        count_hidden = Number.parseInt(count_hidden);
        var check = $('#link_active').hasClass('active');
        if(checkClass == true){
            $(this).parent('.status').parent('.item').remove();
            if(check == true){
                count_active = count_active-1;
                count_hidden = count_hidden+1;
            }
            else{
                count_active = count_active+1;
                count_hidden = count_hidden-1;
            }
        }
        else{
            if($(this).hasClass('active') == true){
                $(this).removeClass('active');
                count_active = count_active-1;
                count_hidden = count_hidden+1;
            }
            else{
                $(this).addClass('active');
                count_active = count_active+1;
                if(count_hidden >0){
                    count_hidden = count_hidden-1;
                }
            }

        }

        $('#count_active').text(count_active);
        $('#count_hidden').text(count_hidden);

        $.get(link, function(data){});
    });
    $('.star').click(function(){
        var link = $(this).find('a').attr('href');
        var check =  $(this).hasClass('activeStar');
        if(check == true){
            $(this).removeClass('activeStar');
        }
        else{
            $(this).addClass('activeStar');
        }
        $.get(link, function(data){});

    });
</script>