@include('admin.index')
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление портфолио</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Портфолио</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item"></div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="cp_edit">
                        <div class="narrow_title">
                            @if($object !== null)
                            <h2>Редактирование портфолио: {{$object->title or ''}}</h2>
                            @else
                            <h2>Добавить портфолио</h2>
                            @endif
                        </div>
                        @if($object == null)
                        <form class="" action="{{route('admin.object.store')}}" method="post" enctype="multipart/form-data">
                          @else
                          <form class="" action="{{route('admin.object.update', $object)}}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="put">
                          @endif
                          {{ csrf_field() }}
                          <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Выберите категорию:</label>
                            </div>
                            <div class="center">
                              <select name="parent_id" required>
                                  <option value="" disabled>Выберите категорию</option>
                                  @forelse($categories as $c)
                                  <option value="{{$c->id or ''}}" @if($object !== null  && $c->id == $object->parent_id)selected="" @endif style="font-weight:600;">{{$c->title or ''}}</option>
                                  @empty
                                  <option value="">Нет категорий</option>
                                  @endforelse
                              </select>
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>

                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Название:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="title" class="" placeholder="Введите название" value="{{$object->title or ''}}" required="">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Оффер:</label>
                            </div>
                            <div class="center">
                                <textarea  name="offer" id="offer" class="" placeholder="Введите сокращенное описание (оффер) - вводный текст">{{$object->offer or ''}}</textarea>
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                      <script>
                          var myOffer;
                          ClassicEditor
                              .create( document.querySelector( '#offer' ) )
                              .then( offer => {myOffer = offer;})
                          .catch( error => {});
                      </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Описание:</label>
                            </div>
                            <div class="center">
                                <textarea rows="10" name="text" id="text" placeholder="Введите описание">{{$object->text or ''}}</textarea>
                            </div>
                        </div>
                              <script>
                                  var myEditor;
                                  ClassicEditor
                                      .create(document.querySelector( '#text' ))
                                      .then( editor => {myEditor = editor;})
                                  .catch( err => {});
                              </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Фотографии:</label>
                            </div>
                            <div class="center">
                                <div class="file-upload">
                                    <label>
                                        <input type="file" name="image[]" multiple>
                                        <span>Выбрать файл</span>
                                    </label>
                                </div>
                                <input type="text" id="filename"  class="filename" disabled="" style="border: none;font-size: 12px;">
                                @if($object !== null)
                                @forelse($object->images as $image)
                                <div class="image_group" id="image{{$image->id}}" style="width:auto; float:left;">

                                    <div class="image">
                                        <img src="{{asset('storage/app')}}/{{$image->images}}" alt="">
                                        <a  class="delete" id="send{{$image->id}}"></a>
                                    </div>

                                </div>
                                <script type="text/javascript">
                                  $('#send{{$image->id}}').click(function(){
                                    $('#image{{$image->id}}').remove();
                                    $.get('{{route('admin.image.goods.destroy', ['id' => $image->id])}}', function(data){});
                                  });
                                </script>
                                @empty
                                @endforelse
                                <div class="clearfix"></div>
                                @endif
                            </div>
                        </div>
                        <!-- <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Видео с YouTube:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="youtube" class="" placeholder="Например: http://www.youtube.com/watch?v=9X-8JiOhzX4" value="" >
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div> -->
                        <div class="perent" style="margin-top: 40px;">
                            <div class="left">
                                &nbsp;
                            </div>

                            <!--

                            Кнопка сохранить и обновить меняются!

                            На странице добавления товаров - добавить
                            На странице редактировать товара - Обновить

                            -->
                            @if($object == null)
                            <div class="center">
                                <input type="submit" class="item_button" value="Добавить">
                            </div>
                            @else
                            <div class="center">
                                <input type="submit" class="item_button" value="Обновить">
                            </div>
                            @endif
                            <div class="right">
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready( function() {
                $(".file-upload input[type=file]").change(function(){
                    var filename = $(this).val().replace(/.*\\/, "");
                    $("#filename").val(filename);
                });
            });
        </script>


    </div>
