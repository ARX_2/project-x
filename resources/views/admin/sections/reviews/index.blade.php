@include('admin.index')
    <div class="row">
        <div class="col-lg-4">
            <h2>Управление сайтами</h2>
        </div>
        <div class="col-lg-8">
            <ol class="breadcrumb">
                <li><a href="">Главная</a></li>
                <li class="active">Отзывы</li>
            </ol>
        </div>
    </div>
    <hr>

    <div class="c_p_company">
        <div class="wide">
            <div class="box">
                <div class="help">
                    <div class="title">
                        <h2>Правая штука</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="narrow">
            <div class="box">
                <style>
                    .reviews h2{font-size:16px;font-weight:600;margin:0}
                    .reviews .item{width:100%;position:relative;padding:10px;border-bottom:1px solid #ddd;display:inline-flex}
                    .reviews .item:hover{background-color:#f7f7f7}
                    .reviews .item .image{background:#ddd;min-width:45px;width:45px;height:45px;margin-right:15px}
                    .reviews .item img{width:45px;height:45px;object-fit:cover}
                    .reviews .item .info{width:70%;padding-right:30px;position:relative}
                    .reviews .info .name{font-size:14px;line-height:16px;font-weight:600;color:#333;margin-bottom:10px}
                    .reviews .gallery{display:table;width:100%;margin-top:10px}
                    .reviews .gallery .image{width:75px;height:50px;margin-right:5px;margin-bottom:5px;float:left}
                    .reviews .gallery img{width:100%;height:100%;object-fit:cover}
                    .rating{color:#fdcc63;font-size:12px;line-height:12px;margin-bottom:6px}
                    .reviews .info .desc{color:#444;margin-top:5px;font-size:15px;line-height:20px;max-height:85px;position:relative;overflow:hidden}
                    .reviews .info .desc p:last-child{margin:0}
                    .reviews .info .toggle-btn{color:#007bff;font-size:15px;line-height:17px;margin-top:5px;display:table}
                    .reviews .info .text-open{overflow:visible;height:auto}
                    .reviews .info .text-open + .toggle-btn{display:none}
                    .reviews .status{font-size:14px;width:15%}
                    .reviews .status .sign{margin-right:5px}
                    .reviews .status .none{color:#e62e04}
                    .reviews .status .active{color:#5cb85c}
                    .reviews .star:before{font-family:FontAwesome;color:#fec107;content:"\f005"}
                    .reviews .search:before{font-family:FontAwesome;content:"\f002"}
                    .reviews .trash:before{font-family:FontAwesome;color:#e62e04;content:"\f014"}
                    .reviews .status .eae:before{font-family:FontAwesome;content:"\f06e"}
                    .reviews .iclose:before{font-family:FontAwesome;content:"\f00d"}
                    .reviews .action{text-align:right;width:10%}
                    .reviews .action .edit:before{font-family:FontAwesome;color:#666;content:"\f141"}
                    .reviews .button-edit{cursor:pointer;z-index:3;width:102px;height:30px;padding:7px 14px;margin-left:25px;font-size:14px;background:#0095eb;color:#fff;border-radius:5px;text-align:center;vertical-align:-webkit-baseline-middle}
                </style>
                <div class="reviews">
                    <div class="narrow_title">
                        <div class="left">
                            <h2>Отзывы</h2>
                        </div>
                        <div class="right">
                            <span class="button-edit"><a href="{{route('admin.feedback.create')}}">Добавить</a></span>
                        </div>
                    </div>
                    <div class="sort">
                      <div class="page_cat">
                          <span><a href="{{route('admin.feedback.index')}}"  id="link_active">Активные <span id="count_active">{{$countActive or ''}}</span></a></span>
                          <span> | </span>
                          <span><a href="{{route('admin.feedback.hidden')}}"  id="link_hidden">Скрытые <span id="count_hidden">{{$countHide or ''}}</span></a></span>
                          {{--<span> | </span>
                          <span><a href="{{route('admin.goods.ShowDeleted')}}">Удаленные {{$countDeleted or ''}}</a></span>--}}
                      </div>
                    </div>
                    @forelse($feedbacks as $feedback)
                    <div class="item">
                        <div class="image">
                            <a href=""><img src="/storage/app/{{$feedback->image}}" alt=""></a>
                        </div>
                        <div class="info">
                            <div class="block">
                                <div class="name">{{$feedback->name or ''}}</div>
                            </div>
                            <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                            <div class="desc">
                              {!! $feedback->description or '' !!}
                            </div>
                            <a href="" class="toggle-btn">Читать полностью</a>
                            <div class="gallery tz-gallery">
                                @forelse($feedback->images as $image)
                                <div class="image">
                                    <a href="/storage/app/{{$image->images or ''}}" class="lightbox">
                                        <img src="/storage/app/{{$image->images or ''}}" alt="">
                                    </a>
                                </div>
                                @empty
                                @endforelse
                            </div>
                        </div>
                        <div class="status">
                            <span class="sign eae @if($feedback->published == 1) active @endif"><a href="{{route('admin.feedback.published', [$feedback->id,$feedback->published])}}" class="link_published"></a></span>
                        </div>
                        <div class="action">
                            <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                            <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                <li><a href="{{route('admin.feedback.edit', [$feedback])}}">Редактировать</a></li>
                            </ul>
                        </div>
                    </div>
                    @empty
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".toggle-btn").click(function(e) {
        e.preventDefault();
        $(this).prev().toggleClass("text-open")
    })
</script>
<script type="text/javascript">
  $('.eae').click(function(){
    var link = $(this).find('a').attr('href');
    $(this).parent('.status').parent('.item').remove();
    var count_active = $('#count_active').html();
    count_active = Number.parseInt(count_active);


    var count_hidden = $('#count_hidden').html();
    count_hidden = Number.parseInt(count_hidden);
    var check = $('#link_active').hasClass('active');
    if(check == true){
      count_active = count_active-1;
      count_hidden = count_hidden+1;
    }
    else{
      count_active = count_active+1;
      count_hidden = count_hidden-1;
    }

    $('#count_active').text(count_active);
    $('#count_hidden').text(count_hidden);

    $.get(link, function(data){});

  });

</script>
<link rel="stylesheet" href="https://rawgit.com/LeshikJanz/libraries/master/Bootstrap/baguetteBox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>

</html>
