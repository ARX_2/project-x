@include('admin.index')

    <div class="row">
        <div class="col-lg-4">
            <h2>Управление компанией</h2>
        </div>
        <div class="col-lg-8">
            <ol class="breadcrumb">
                <li><a href="/">Главная</a></li>
                <li class="active">Отзывы</li>
            </ol>
        </div>
    </div>
    <hr>

    <div class="c_p_company">
        <div class="wide">
            <div class="box">

            </div>
        </div>
        <div class="narrow">
            <div class="box">
                <div class="cp_edit">
                    <div class="narrow_title">
                      @if($feedback == null)
                        <h2>Добавление отзыва</h2>
                      @else
                      <h2>Редактирование отзыва</h2>
                      @endif
                    </div>
                    @if($feedback == null)
                    <form action="{{route('admin.feedback.store')}}" method="post" enctype="multipart/form-data">
                    @else
                    <form action="{{route('admin.feedback.update.new', [$feedback])}}" method="post" enctype="multipart/form-data">
                    @endif
                    {{csrf_field()}}
                    <div class="perent">
                        <div class="left">
                            <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Название:</label>
                        </div>
                        <div class="center">
                            <input type="text" name="name" class="" placeholder="ФИО автора" value="{{$feedback->name or ''}}" required="">
                        </div>
                        <div class="right">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="perent">
                        <div class="left">
                            <label for="" class="item_name" style="line-height: 14px;">Фотография автора:</label>
                        </div>
                        <div class="center">
                            <div class="file-upload">
                                <label>
                                    <input type="file" name="ava">
                                    <span>Выбрать файл</span>
                                </label>
                            </div>
                            <input type="text" id="filename" class="filename"  disabled="" style="border: none;font-size: 12px;">
                            @if($feedback !== null && $feedback->image !== null)
                            <div class="image_group ava">
                                <div class="image">
                                    <img src="/storage/app/{{$feedback->image or ''}}" alt="">
                                    <a class="delete" id="del"></a>
                                </div>
                            </div>

                            <script type="text/javascript">
                              $('#del').click(function(){
                                $('.ava').remove();
                                $.get('{{route('admin.feedback.image.desctroy', ['id' => $feedback->id])}}', function(data){});
                              });
                            </script>
                            @endif
                        </div>
                    </div>
                    <div class="perent">
                        <div class="left">
                            <label for="" class="item_name" style="line-height: 14px;">Описание:</label>
                        </div>
                        <div class="center">
                            <textarea rows="10" name="text">{{$feedback->description or ''}}</textarea>
                        </div>
                    </div>
                    <script>
                        CKEDITOR.replace('text');
                    </script>
                    <div class="perent">
                        <div class="left">
                            <label for="" class="item_name" style="line-height: 14px;">Фотографии:</label>
                        </div>
                        <div class="center">
                            <div class="file-upload">
                                <label>
                                    <input type="file" name="files[]" multiple>
                                    <span>Выбрать файл</span>
                                </label>
                            </div>
                            <input type="text" id="filename" class="filename" disabled="" style="border: none;font-size: 12px;">

                            @if($feedback !== null)
                            @forelse($feedback->images as $image)
                            <div class="image_group" id="image{{$image->id}}" style="width:auto; float:left;">

                                <div class="image">
                                    <img src="{{asset('storage/app')}}/{{$image->images}}" alt="">
                                    <a  class="delete" id="send{{$image->id}}"></a>
                                </div>

                            </div>
                            <script type="text/javascript">
                              $('#send{{$image->id}}').click(function(){
                                $('#image{{$image->id}}').remove();
                                $.get('{{route('admin.image.goods.destroy', ['id' => $image->id])}}', function(data){});
                              });
                            </script>
                            @empty
                            @endforelse
                            <div class="clearfix"></div>
                            @endif
                        </div>
                    </div>
                    <div class="perent" style="margin-top: 40px;">
                        <div class="left">
                            &nbsp;
                        </div>

                        <!--

                        Кнопка сохранить и обновить меняются!

                        На странице добавления товаров - добавить
                        На странице редактировать товара - Обновить

                        -->

                        <div class="center">
                            <input type="submit" class="item_button" value="обновить">
                        </div>
                        <div class="right">
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready( function() {
            $(".file-upload input[type=file]").change(function(){
                var filename = $(this).val().replace(/.*\\/, "");
                $("#filename").val(filename);
            });
        });
    </script>


</div>


</html>
