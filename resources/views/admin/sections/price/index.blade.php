@include('admin.index')
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление прайс-листом</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Прайс-лист</li>
                </ol>
            </div>
        </div>
        <hr>
        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        <div class="item">
                            <div class="name">
                                <a href="/admin/admin/company/upload?type=11">Описание на странице прайс-лист</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="pricelist">
                        <div class="narrow_title">
                            <div class="left">
                                <h2>Прайс лист</h2>
                            </div>
                            <div class="right">
                                <span class="button-edit"><a href="" data-target="#exampleModal" data-toggle="modal">Добавить</a></span>
                            </div>
                        </div>
                        @forelse($categories as $c)
                            <div class="block">
                                <div class="head_list">
                                    <div class="item">
                                        <div class="name">{{$c->title or ''}}
                                        </div>
                                        <div class="unit">Ед.</div>
                                        <div class="price">Цена</div>
                                        <div class="action">
                                            <a href="#" class="dropdown-toggle arrows" data-toggle="dropdown"></a>
                                            <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX"
                                                data-dropdown-out="flipOutX">
                                                <li><a href="{{route('admin.price.deleteCategory', ['id' => $c->id])}}">Удалить</a>
                                                </li>
                                                <li><a href="" data-target="#upload{{$c->id}}" data-toggle="modal">Редактировать</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                @forelse($pirces as $p)
                                    @if($p->category == $c->id)
                                        <div class="item">
                                            <div class="name">{{$p->title or '-'}}</div>
                                            <div class="unit">{{$p->count or '-'}}</div>
                                            <div class="price">{{$p->coast or '-'}}</div>
                                            <div class="action">
                                                <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                                <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX"
                                                    data-dropdown-out="flipOutX">
                                                    <li>
                                                        <a href="{{route('admin.price.delete', ['id' => $p])}}">Удалить</a>
                                                    </li>
                                                    <li><a href="" data-target="#price{{$p->id}}" data-toggle="modal">Редактировать</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="price{{$p->id}}" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Редактирование
                                                            прайса</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{route('admin.price.update', $p)}}" method="post"
                                                          enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="category" value="{{$c->id}}">
                                                        <input type="hidden" name="_method" value="put">
                                                        <div class="modal-body">
                                                            <label for="">Название</label>
                                                            <input type="text" class="form-control" name="title"
                                                                   placeholder="Название" value="{{$p->title or ''}}">
                                                            <label for="">ед.изм</label>
                                                            <input type="text" class="form-control" name="count"
                                                                   placeholder="кв.м" value="{{$p->count or ''}}">
                                                            <label for="">Цена</label>
                                                            <input type="text" class="form-control" name="coast"
                                                                   placeholder="0" value="{{$p->coast or ''}}">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Закрыть
                                                            </button>
                                                            <input type="submit" class="btn btn-primary"
                                                                   value="Сохранить">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @empty
                                @endforelse
                                <div class="item add">
                                    <a href="">
                                        <a href="" class="name" data-target="#exampleModal{{$c->id}}"
                                           data-toggle="modal">Добавить</a>
                                        <div class="unit"></div>
                                        <div class="price"></div>
                                        <div class="action"></div>
                                    </a>
                                </div>

                            </div>
                            <div class="modal fade" id="exampleModal{{$c->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Добавление прайса</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="{{route('admin.price.addPrice')}}" method="post"
                                              enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="category" value="{{$c->id}}">
                                            <div class="modal-body">
                                                <label for="">Название</label>
                                                <input type="text" class="form-control" name="title"
                                                       placeholder="Название">
                                                <label for="">ед.изм</label>
                                                <input type="text" class="form-control" name="count" placeholder="кв.м">
                                                <label for="">Цена</label>
                                                <input type="text" class="form-control" name="coast" placeholder="0">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    Закрыть
                                                </button>
                                                <input type="submit" class="btn btn-primary" value="Сохранить">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                            <div class="modal fade" id="upload{{$c->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Редактирование</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="{{route('admin.price.upload', ['id' => $c])}}" method="post"
                                              enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="category" value="{{$c->id}}">
                                            <div class="modal-body">
                                                <label for="">Название</label>
                                                <input type="text" class="form-control" name="title"
                                                       placeholder="Цены на ремонт стиральных машин"
                                                       value="{{$c->title}}">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    Закрыть
                                                </button>
                                                <input type="submit" class="btn btn-primary" value="Сохранить">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Добавление</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('admin.price.add')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <label for="">Название</label>
                        <input type="text" class="form-control" name="title"
                               placeholder="Цены на ремонт стиральных машин">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        <input type="submit" class="btn btn-primary" value="Сохранить">
                    </div>
                </form>
            </div>
        </div>
    </div>
