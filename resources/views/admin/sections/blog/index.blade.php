@include('admin.index')
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление блогом</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Блог</li>
                </ol>
            </div>
        </div>
        <hr>
        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        <div class="item">
                            <div class="name">
                                <a href="/admin/admin/company/upload?type=10">Описание на главной странице блога</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="narrow_title">
                        <div class="left">
                            <h2>Блог</h2>
                        </div>
                        <div class="right">
                            <span class="button-edit"><a href="{{route('admin.blog.create')}}">Добавить</a></span>
                        </div>
                    </div>
                    <div class="production medium">
                        <div class="sort">
                            <div class="page_cat">
                                <span><a href="{{route('admin.blog.index')}}" class="active">Активные {{$countShow or ''}}</a></span>
                                <span> | </span>
                                <span><a href="{{route('admin.blog.hidden')}}">Скрытые {{$countHidden or ''}}</a></span>
                            </div>
                        </div>
                        @forelse($blogs as $blog)
                        <div class="item">
                            <div class="image">
                                <img src="{{asset('storage/app')}}/{{$blog->image or '/public/none-img.png'}}" alt="">
                            </div>
                            <div class="info">
                                <div class="block">
                                    <div class="name">{{$blog->title or ''}}</div>
                                    <div class="offer">
                                        {{$blog->text or ''}}
                                    </div>
                                </div>
                                <div class="additionally">
                                    <div class="date">Обновлено: <span>{{Resize::date($blog->updated_at)}}</span></div>
                                </div>
                            </div>
                            <div class="status">
                                <span class="sign eae @if($blog->published == 1)active @endif"></span>
                                @if($blog->main == 1)<span class="sign star"></span> @endif
                            </div>
                            <div class="action">
                                <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                    <li><a href="{{route('admin.blog.hide')}}?id={{$blog->id}}">@if($blog->published ==1)Скрыть @else Показать @endif</a></li>
                                    <li><a href="{{route('admin.blog.edit', $blog)}}">Редактировать</a></li>
                                    <li><a href="{{route('admin.blog.main')}}?id={{$blog->id}}">@if($blog->main ==0)На главную @else Снять с главной @endif</a></li>
                                </ul>
                            </div>
                        </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
