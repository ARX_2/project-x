        <div class="row">
            <div class="col-lg-4">
                <h2>Управление тизерами</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Тизеры</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        {{--<div class="item">
                            <div class="name">
                                <a href=""></a>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="cp_edit">
                        <div class="narrow_title">
                            <h2>Редактирование тизера: <span id="edit-title">{{$tizer->title or ''}}</span></h2>
                        </div>
                        @if($tizer == null)
                        <form class="" action="{{route('admin.tizers.store')}}" method="post" enctype="multipart/form-data">
                          @else
                          <form class="" action="{{route('admin.tizers.update', $tizer)}}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="put">
                          @endif
                          {{ csrf_field() }}
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Название:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="title" class="" placeholder="Введите название тизера" maxlength="30" value="{{$tizer->title or ''}}" required="">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('input[name="title"]').on('input',function(){
                          var title = $(this).val();
                          $('#edit-title').text(title);
                        });
                        $('input[name="title"]')
                        .on({
                          blur: function() {
                            $.get('/admin/tizer/title/{{$tizer->id}}/'+this.value);
                          }
                        });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Описание:</label>
                            </div>
                            <div class="center">
                                <textarea rows="10" name="description" maxlength="60">{{$tizer->description or ''}}</textarea>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('textarea[name="description"]')
                        .on({
                          blur: function() {
                            $.get('/admin/tizer/description/{{$tizer->id}}/'+this.value);
                          }
                        });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Фотографии:</label>
                            </div>
                            <div class="center">
                                <div class="file-upload">
                                    <label>
                                        <input id="sortpicture" type="file" name="sortpic" />
                                        <span>Выбрать файл</span>
                                    </label>
                                </div>
                                <input type="text" id="filename"  class="filename" disabled="" style="border: none;font-size: 12px;">
                                <div class="imgs">
                                @if($tizer !== null)
                                @if($tizer->image !==null)
                                <div class="image_group">

                                    <div class="image">
                                        <img src="{{asset('storage/app')}}/{{$tizer->image}}" alt="">

                                    </div>

                                </div>
                                @endif
                                @endif
                              </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('#sortpicture').on('change',function() {
                          //alert('asd');
                          var i = 1;
                          var files = $('#sortpicture').prop('files');
                          $.each(files, function (index, value) {
                            var file_data = value;
                            var form_data = new FormData();
                            form_data.append('file', file_data);
                            form_data.append('id', {{$tizer->id}});
                            $.ajax({
                                        url: '/admin/image/tizer/upload',
                                        dataType: 'text',
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'post',
                                        success: function(data){
                                          data = $.parseJSON(data);
                                            $('.imgs').html('<div class="image_group" id="image'+i+'" style="width:auto;float:left;"><div class="image"><img src="{{asset('storage/app')}}/'+data.image+'" alt=""></div></div>');
                                            $('#image'+i).click(function(){
                                            });
                                        }
                             });
                             i++;
                          });

                        });

                        </script>

                        <!-- <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Видео с YouTube:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="youtube" class="" placeholder="Например: http://www.youtube.com/watch?v=9X-8JiOhzX4" value="" >
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div> -->
                        <div class="perent" style="margin-top: 40px;">
                            <div class="left">
                                &nbsp;
                            </div>

                            <!--

                            Кнопка сохранить и обновить меняются!

                            На странице добавления товаров - добавить
                            На странице редактировать товара - Обновить

                            -->
                            <div class="center">
                                <input type="button" class="item_button" value="Сохранить и вернутся">
                            </div>
                            <div class="right">
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
          $('.item_button').click(function(){
            $.get('/admin/tizer/status/{{$tizer->id}}/2', function(data){
              $('.admin-content').html(data);
            });
          });
        </script>
        <script>
            $(document).ready( function() {
                $(".file-upload input[type=file]").change(function(){
                    var filename = $(this).val().replace(/.*\\/, "");
                    $("#filename").val(filename);
                });
            });
        </script>
