
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление сайтами</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="">Главная</a></li>
                    <li class="active">Категории</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">

                    <div class="tizers">
                        <div class="narrow_title">
                            <div class="left">
                                <i class="fa fa-refresh" aria-hidden="true" style="float:left; font-size:20px;color:#14aae6;padding: 5px;border-radius: 50px;" id="refresh"></i>
                                <h2 style="float:left;margin-left:10px;margin-top:5px;">Тизеры</h2>
                                <p class="clearFix"></p>
                                <style media="screen">
                                  #refresh:hover{
                                    background-color: #e7e6e6;
                                  }
                                </style>
                            </div>
                            <div class="right">
                                <span class="button-edit"><a href="{{route('admin.tizers.create')}}">Добавить</a></span>
                            </div>
                        </div>
                        @forelse($tizers as $tizer)
                        <div class="item">
                            <div class="image">
                                <img src="{{asset('storage/app')}}/{{$tizer->image or '/public/none-img.png'}}" alt="">
                            </div>
                            <div class="info">
                                <div class="block">
                                    <div class="name">{{$tizer->title}}</div>
                                </div>
                                <div class="additionally">
                                    <div class="desc">{{$tizer->description or ''}}</div>
                                </div>
                            </div>
                            <div class="status">

                            </div>
                            <div class="action">
                                <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                    <li><a href="{{route('admin.tizers.edit', $tizer)}}" class="link_edit">Редактировать</a></li>
                                    <li><a href="{{route('admin.tizers.delete', ['id' => $tizer->id])}}">Удалить</a></li>
                                </ul>
                            </div>
                        </div>
                        @empty
                        @endforelse

                    </div>
                </div>
            </div>
        </div>
        <script>

        $('#refresh').click(function(){
          $(this).addClass('rotate');
          $.get('{{$link or ''}}?ajax=true', function(data){
            $('.admin-content').html(data);
          });
        });


        $('.button-edit').click(function(){
          var link = $('.button-edit').find('a').attr('href');
            $.get(link+'?ajax=true', function(data){
              $('.admin-content').html(data);
            });
            history.pushState(null, null, link);
            return false;
        });

        $('.link_edit').click(function(){
          var link = $(this).attr('href');
            $.get(link+'?ajax=true', function(data){
              $('.admin-content').html(data);
            });
            history.pushState(null, null, link);
            return false;
        });
        </script>

        <style media="screen">
          .rotate{
            -webkit-animation-name: cog;
    -webkit-animation-duration: 1s;
    -webkit-animation-iteration-count: infinite;
    -webkit-animation-timing-function: linear;
    -moz-animation-name: cog;
    -moz-animation-duration: 1s;
    -moz-animation-iteration-count: infinite;
    -moz-animation-timing-function: linear;
    -ms-animation-name: cog;
    -ms-animation-duration: 1s;
    -ms-animation-iteration-count: infinite;
    -ms-animation-timing-function: linear;

    animation-name: cog;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
    }
    @-ms-keyframes cog {
    from { -ms-transform: rotate(0deg); }
    to { -ms-transform: rotate(20deg); }
    }
    @-moz-keyframes cog {
    from { -moz-transform: rotate(0deg); }
    to { -moz-transform: rotate(20deg); }
    }
    @-webkit-keyframes cog {
    from { -webkit-transform: rotate(0deg); }
    to { -webkit-transform: rotate(20deg); }
    }
    @keyframes cog {
    from {
    transform:rotate(0deg);
    }
    to {
    transform:rotate(360deg);
    }
          }
        </style>

@if($error == 1)
<script type="text/javascript">
  alert('Внимание! Количество тизеров ограничено, не более 4х!');
</script>
@endif

<link rel="stylesheet" type="text/css" href="/admin/pack/font-awesome/css/font-awesome.min.css">
<script src="/admin/pack/bootstrap-3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/admin/pack/js/jquery.mask.js"></script>


</body></html>
