@include('admin.index')
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление вакансиями</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Вакансии</li>
                </ol>
            </div>
        </div>
        <hr>
        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        <div class="item">
                            <div class="name">
                                <a href="/admin/admin/company/upload?type=7">Описание на странице вакансий</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="sections">
                        <div class="narrow_title">
                            <div class="left">
                                <h2>Вакансии</h2>
                            </div>
                            <div class="right">
                                <span class="button-edit"><a href="{{route('admin.jobs.create')}}">Добавить</a></span>
                            </div>
                        </div>
                        <div class="production medium">
                          @forelse($jobs as $j)
                          <?php
                          if($j->updated_at !== null){
                          $updated = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $j->updated_at)->addHours(5)->toDateTimeString();
                          $updated = date("d-m-Y", strtotime($updated));
                          }
                           ?>
                              <div class="item">
                                  <div class="image">
                                      <img src="{{asset('storage/app')}}/{{$j->image or '/public/none-img.png'}}" alt="">
                                  </div>
                                  <div class="info_goods">
                                      <div class="block">
                                          <div class="name">{{$j->title}}</div>
                                          <div class="price"><span>{{$j->pay}}</span> руб.</div>
                                      </div>
                                      <div class="additionally">
                                          <div class="date">Обновлено: <span>18 января 2018</span></div>
                                      </div>
                                  </div>
                                  <div class="status">
                                      <span class="sign eae @if($j->published == 1)active @endif"></span>
                                  </div>
                                  <div class="action">
                                      <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                      <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                          <li><a href="{{route('admin.jobs.hide', ['id' => $j->id])}}">@if($j->published == 1)Скрыть @else Показать @endif</a></li>
                                          <li><a href="{{route('admin.jobs.edit', $j)}}?id={{$j->id}}">Редактировать</a></li>
                                      </ul>
                                  </div>
                              </div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
