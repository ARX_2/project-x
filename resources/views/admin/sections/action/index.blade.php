@include('admin.index')
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление акциями</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Акции</li>
                </ol>
            </div>
        </div>
        <hr>
        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        <div class="item">
                            <div class="name">
                                <a href="/admin/admin/company/upload?type=8">Описание на странице акций</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                        <div class="narrow_title">
                            <div class="left">
                                <h2>Акции</h2>
                            </div>
                            <div class="right">
                                <span class="button-edit"><a href="{{route('admin.action.create')}}">Добавить</a></span>
                            </div>
                        </div>
                    <div class="production medium">
                        <div class="sort">
                            <div class="page_cat">
                                <span><a href="{{route('admin.action.index')}}" class="active">Активные {{$countShow or ''}}</a></span>
                                <span> | </span>
                                <span><a href="{{route('admin.action.hidden')}}">Скрытые {{$countHidden or ''}}</a></span>
                            </div>
                        </div>
                        @forelse($actions as $action)
                            <div class="item">
                                <div class="image">
                                    <img src="{{asset('storage/app')}}/{{$action->image or '/public/none-img.png'}}" alt="">
                                </div>
                                <div class="info">
                                    <div class="block">
                                        <div class="name">{{$action->title or ''}}</div>
                                        <div class="offer">
                                            {{$action->text or ''}}
                                        </div>
                                    </div>
                                    <div class="additionally">
                                        <div class="date"><span>{{$action->datefrom or ''}} - {{$action->dateto or ''}}</span></div>
                                    </div>
                                </div>
                                <div class="status">
                                    <span class="sign eae @if($action->published == 1)active @endif"></span>
                                    <span class="sign @if($action->main == 1)star @endif"></span>
                                </div>
                                <div class="action">
                                    <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                    <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                        <li><a href="{{route('admin.action.hide')}}?id={{$action->id}}">@if($action->published == 1)Скрыть @else Показать @endif</a></li>
                                        <li><a href="{{route('admin.action.edit', $action)}}">Редактировать</a></li>
                                        <li><a href="{{route('admin.action.main')}}?id={{$action->id}}">@if($action->main == 0)На главную @else Снять с главной @endif</a></li>
                                    </ul>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
