@include('admin.index')
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление акциями</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Акции</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">

                    </div>
                </div>
            </div>
            <div class="narrow">
              @if($action == null)
              <form action="{{route('admin.action.store')}}" method="post" enctype="multipart/form-data">
                @else
                <form action="{{route('admin.action.update', $action)}}" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="_method" value="put">
                @endif
                {{ csrf_field() }}
                <div class="box">
                    <div class="cp_edit">
                        <div class="narrow_title">
                          @if($action == null)
                          <h2>Добавление акции</h2>
                          @else
                          <h2>Редактирование акции: {{$action->title or ''}}</h2>
                          @endif

                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Название:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="title" class="" placeholder="Введите название акции" value="{{$action->title or ''}}" required="">
                            </div>
                            <div class="right"></div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Оффер:</label>
                            </div>
                            <div class="center">
                                <textarea rows="5" name="text">{{$action->text or ''}}</textarea>
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Описание:</label>
                            </div>

                            <div class="center">
                                <textarea rows="10" name="description">{{$action->description or ''}}</textarea>
                            </div>
                            <script>
                                    CKEDITOR.replace('description');
                                </script>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Дата проведения:</label>
                            </div>
                            <div class="center">
                                <div class="add-time">
                                    <input type="text" name="datefrom" class="input-datetime" placeholder="22 августа 2017" value="{{$action->datefrom or ''}}" required="">
                                    <span> - </span>
                                    <input type="text" name="dateto" class="input-datetime" placeholder="11 октября 2018" value="{{$action->dateto or ''}}" required="">
                                </div>
                            </div>
                            <div class="right"></div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Фотографии:</label>
                            </div>
                            <div class="center">
                                <div class="file-upload">
                                    <label>
                                        <input type="file" name="file">
                                        <span>Выбрать файл</span>
                                    </label>
                                </div>
                                <input type="text" id="filename" class="filename" disabled="" style="border: none;font-size: 12px;">
                                @if($action !== null)
                                @if($action->image !== null)
                                <div class="image_group" id="image{{$action->id}}">
                                    <div class="image">
                                        <img src="{{asset('storage/app')}}/{{$action->image}}" alt="">
                                        <a  class="delete" id="send"></a>
                                    </div>
                                </div>
                                <script>

                                ajaxForm();
                                function ajaxForm(){
                                  $('#send').click(function(){
                                    var data = new FormData($("#form")[0]);
                                      $.ajax({
                                          type: 'GET',
                                          url: "{{route('admin.action.imageDelete')}}?id={{$action->id}}",
                                          data: data,
                                          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                          contentType: false,
                                          processData : false,
                                          success: function(data) {
                                              $('#image{{$action->id}}').html('');
                                          }
                                      });
                                      return false;
                                  });
                                }

                        </script>
                                @endif
                                @endif
                            </div>
                        </div>
                        <div class="perent" style="margin-top: 40px;">
                            <div class="left">
                                &nbsp;
                            </div>

                            <!--

                            Кнопка сохранить и обновить меняются!

                            На странице добавления товаров - добавить
                            На странице редактировать товара - Обновить

                            -->
                            @if($action == null)
                            <div class="center">
                                <input type="submit" class="item_button" value="Добавить">
                            </div>
                            @else
                            <div class="center">
                                <input type="submit" class="item_button" value="Обновить">
                            </div>
                            @endif
                            <div class="right">
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
        </div>

        <script>
            $(document).ready( function() {
                $(".file-upload input[type=file]").change(function(){
                    var filename = $(this).val().replace(/.*\\/, "");
                    $("#filename").val(filename);
                });
            });
        </script>


    </div>
