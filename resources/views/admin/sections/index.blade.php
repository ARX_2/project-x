@include('admin.index')
    <div class="row">
        <div class="col-lg-4">
            <h2>Управление компанией</h2>
        </div>
        <div class="col-lg-8">
            <ol class="breadcrumb">
                <li><a href="/">Главная</a></li>
                <li class="active">Разделы</li>
            </ol>
        </div>
    </div>
    <hr>
    <hr>
    <div class="c_p_company">
        <div class="wide">
            <div class="box">
                <div class="help">
                    <div class="title">
                        <h2>Правая штука</h2>
                    </div>
                </div>
            </div>
        </div>
        <?php //dd($sections[0]); ?>
        <div class="narrow">
            <div class="box">
                <div class="setting">
                  <form class="" action="{{route('admin.section.update')}}" method="post">
                    {{ csrf_field()}}
                    <h2>Разделы</h2>
                    @forelse($sections as $section)
                    @if($section->slug !== 'about' && $section->slug !== 'product' && $section->slug !== 'services' && $section->slug !== 'contact' && $section->type == null || $section->type !== null && $section->published == 1)
                    <div class="group-edit-row">
                        <div class="edit-label">{{$section->title}}:</div>
                        <div class="field">
                            <select name="published[{{$section->id}}]">
                                <option @if($section->published ==0) selected="" @endif value="0">Отключено</option>
                                <option @if($section->published ==1) selected="" @endif value="1">Включено</option>
                            </select>
                        </div>
                    </div>
                    @endif
                    @empty
                    @endforelse

                    <hr>
                    <div class="group-edit-row">
                        <div class="edit-label">&nbsp;</div>
                        <div class="field">
                            <button href="" class="button-save" type="submit" value="Сохранить">Сохранить</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
