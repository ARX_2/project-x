@include('admin.index')
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление партнерами</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Партнеры</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        <div class="item">
                            <div class="name">
                                <a href="/admin/admin/company/upload?type=13">Описание на странице партнеров</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">

                    <div class="portfolio">
                        <div class="narrow_title">
                            <div class="left">
                                <h2>Партнеры</h2>
                            </div>
                            <div class="right">
                                <span class="button-edit"><a href="{{route('admin.partners.create')}}">Добавить</a></span>
                            </div>
                        </div>
                        <style>
                            .portfolio .item .site{
                                font-size: 14px;

                                height: 20px;
                                overflow: hidden;
                                margin-bottom: 10px;
                            }
                            .portfolio .item .site a{
                                color: #0095eb;
                            }
                        </style>
                        @forelse($partners as $partner)
                        <div class="item">
                            <div class="image">
                                <img src="{{asset('storage/app')}}/{{$partner->image or '/public/none-img.png'}}" alt="">
                            </div>
                            <div class="info">
                                <div class="block">
                                    <div class="name">{{$partner->title or ''}}</div>
                                    <div class="site"><a href="{{$partner->link or ''}}">{{$partner->link or ''}}</a></div>
                                    <div class="offer">
                                      {{$partner->description or ''}}
                                    </div>
                                </div>
                                <div class="additionally">
                                    <div class="date">Обновлено: <span>{{Resize::date($partner->updated_at)}}</span></div>
                                </div>
                                <div class="status">
                                    <span class="sign eae @if($partner->published == 1)active @endif"></span>
                                </div>
                            </div>
                            <div class="action">
                                <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                    <li><a href="{{route('admin.partners.hide')}}?id={{$partner->id}}">@if($partner->published == 1)Скрыть @else Показать @endif</a></li>
                                    <li><a href="{{route('admin.partners.edit', $partner)}}?id={{$partner->id}}">Редактировать</a></li>
                                </ul>
                            </div>
                        </div>
                        @empty
                        @endforelse


                    </div>
                </div>
            </div>
        </div>


    </div>
