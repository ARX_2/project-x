@include('admin.index')
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление новостной лентой</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Новости</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        {{--<div class="item">
                            <div class="name">
                                <a href=""></a>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
            <div class="narrow">
              @if($news == null)
              <form class="" action="{{route('admin.news.store')}}" method="post" enctype="multipart/form-data">
                @else
                <form class="" action="{{route('admin.news.update', $news)}}" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="_method" value="put">
                @endif
                {{ csrf_field() }}
                <div class="box">
                    <div class="cp_edit">
                        <div class="narrow_title">
                          @if($news == null)
                            <h2>Добавление новости</h2>
                            @else
                            <h2>Редактирование новости: {{$news->title or ''}}</h2>
                            @endif
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Название:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="title" class="" placeholder="Введите название новости" value="{{$news->title or ''}}" required="">
                            </div>
                            <div class="right"></div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Оффер:</label>
                            </div>
                            <div class="center">
                                <textarea rows="5" name="text">{{$news->text or ''}}</textarea>
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Описание:</label>
                            </div>
                            <div class="center">
                                <textarea rows="10" name="description">{{$news->description or ''}}</textarea>
                            </div>
                            <script>
                                    CKEDITOR.replace('description');
                                </script>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Фотографии:</label>
                            </div>
                            <div class="center">
                                <div class="file-upload">
                                    <label>
                                        <input type="file" name="file">
                                        <span>Выбрать файл</span>
                                    </label>
                                </div>
                                <input type="text" id="filename" class="filename" disabled="" style="border: none;font-size: 12px;">
                                @if($news !== null)
                                @if($news->image !== null)
                                <div class="image_group" id="image{{$news->id}}">
                                    <div class="image">
                                        <img src="{{asset('storage/app')}}/{{$news->image or ''}}" alt="">
                                        <a  class="delete" id="send"></a>
                                    </div>
                                </div>
                                <script>

                                ajaxForm();
                                function ajaxForm(){
                                  $('#send').click(function(){
                                    var data = new FormData($("#form")[0]);
                                      $.ajax({
                                          type: 'GET',
                                          url: "{{route('admin.news.imageDelete')}}?id={{$news->id}}",
                                          data: data,
                                          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                          contentType: false,
                                          processData : false,
                                          success: function(data) {
                                              $('#image{{$news->id}}').html('');
                                          }
                                      });
                                      return false;
                                  });
                                }

                        </script>
                                @endif
                                @endif
                            </div>
                        </div>
                        <div class="perent" style="margin-top: 40px;">
                            <div class="left">
                                &nbsp;
                            </div>

                            <!--

                            Кнопка сохранить и обновить меняются!

                            На странице добавления товаров - добавить
                            На странице редактировать товара - Обновить

                            -->
                            @if($news == null)
                            <div class="center">
                                <input type="submit" class="item_button" value="Добавить">
                            </div>
                            @else
                            <div class="center">
                                <input type="submit" class="item_button" value="Обновить">
                            </div>
                            @endif
                            <div class="right">
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>

        <script>
            $(document).ready( function() {
                $(".file-upload input[type=file]").change(function(){
                    var filename = $(this).val().replace(/.*\\/, "");
                    $("#filename").val(filename);
                });
            });
        </script>


    </div>
