@if($tape == null)
<form action="{{route('admin.tape.store')}}" method="post" enctype="multipart/form-data">
    @else
        <form action="{{route('admin.tape.update', $tape)}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="put">
            @endif
            {{ csrf_field() }}
            <div class="box">
                <div class="cp_edit">
                    <div class="narrow_title">
                        @if($tape == null)
                            <h2>Добавление ленты</h2>
                        @else
                            <h2>Редактирование ленты: {{$tape->title or ''}}</h2>
                        @endif

                    </div>
                    <div class="perent">
                        <div class="left">
                            <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Название:</label>
                        </div>
                        <div class="center">
                            <input type="text" name="title" class="" placeholder="Введите название акции" value="{{$tape->title or ''}}" required="">
                        </div>
                        <div class="right"></div>
                    </div>
                    <div class="perent">
                        <div class="left">
                            <label for="" class="item_name" style="line-height: 14px;">Оффер:</label>
                        </div>
                        <div class="center">
                            <textarea rows="5" name="offer">{{$tape->offer or ''}}</textarea>
                        </div>
                        <div class="right">
                            <div class="help"></div>
                        </div>
                    </div>
                    <div class="perent">
                        <div class="left">
                            <label for="" class="item_name" style="line-height: 14px;">Описание:</label>
                        </div>

                        <div class="center">
                            <textarea rows="10" name="description">{{$tape->description or ''}}</textarea>
                        </div>
                        <script>
                            CKEDITOR.replace('description');
                        </script>
                    </div>

                    <div class="perent">
                        <div class="left">
                            <label for="" class="item_name" style="line-height: 14px;">Фотографии:</label>
                        </div>
                        <div class="center">
                            <div class="file-upload">
                                <label>
                                    <input type="file" name="file">
                                    <span>Выбрать файл</span>
                                </label>
                            </div>
                            <input type="text" id="filename" class="filename" disabled="" style="border: none;font-size: 12px;">
                            @if($tape !== null)
                                @if($tape->image !== null)
                                    <div class="image_group" id="image{{$tape->id}}">
                                        <div class="image">
                                            <img src="{{asset('storage/app')}}/{{$tape->image}}" alt="">
                                            <a  class="delete" id="send"></a>
                                        </div>
                                    </div>
                                    <script>
                                        ajaxForm();
                                        function ajaxForm(){
                                            $('#send').click(function(){
                                                var data = new FormData($("#form")[0]);
                                                $.ajax({
                                                    type: 'GET',
                                                    url: "{{route('admin.tape.imageDelete')}}?id={{$tape->id}}",
                                                    data: data,
                                                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                                    contentType: false,
                                                    processData : false,
                                                    success: function(data) {
                                                        $('#image{{$tape->id}}').html('');
                                                    }
                                                });
                                                return false;
                                            });
                                        }
                                    </script>
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="perent" style="margin-top: 40px;">
                        <div class="left">
                            &nbsp;
                        </div>

                        <!--

                        Кнопка сохранить и обновить меняются!

                        На странице добавления товаров - добавить
                        На странице редактировать товара - Обновить

                        -->
                        @if($tape == null)
                            <div class="center">
                                <input type="submit" class="item_button" value="Добавить">
                            </div>
                        @else
                            <div class="center">
                                <input type="submit" class="item_button" value="Обновить">
                            </div>
                        @endif
                        <div class="right">
                        </div>
                    </div>
                </div>
        </form>