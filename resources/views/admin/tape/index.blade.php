<div class="row">
    <div class="col-lg-4">
        <h2>Управление акциями</h2>
    </div>
    <div class="col-lg-8">
        <ol class="breadcrumb">
            <li><a href="{{route('admin.index')}}">Главная</a></li>
            <li class="active">Акции</li>
        </ol>
    </div>
</div>
<hr>
<div class="c_p_company">
    <div class="wide">
        <div class="box">
            <div class="wide_title">
                <h2>Правая штука</h2>
            </div>
            <div class="undefined_item">
                <div class="item">
                    <div class="name">
                        <a href="/admin/admin/company/upload?type=8">Описание на странице акций</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="narrow">
        <div class="box">
            <div class="narrow_title">
                <div class="left">
                    <h2>Лента новостей</h2>
                </div>
                @if(Auth::user()->userRolle == 1)
                <div class="right">
                    <span class="button-edit"><a href="/admin/tape/create">Добавить</a></span>
                </div>
                @endif
            </div>
            <div class="production medium">
                @if(Auth::user()->userRolle == 1)
                <div class="sort" style="margin-bottom: 15px">
                    <div class="page_cat" style="margin-bottom: 0">
                        <span><a href="{{route('admin.tape.index')}}" class="active">Активные {{$countShow or ''}}</a></span>
                        <span> | </span>
                        <span><a href="{{route('admin.tape.hidden')}}">Скрытые {{$countHidden or ''}}</a></span>
                    </div>
                </div>
                @endif
                <style>
                    .news{display:table;width:100%}
                    .news .post{width:50%;min-width:610px;position:relative;padding-bottom:15px;margin:0 auto 40px;border-bottom:1px solid #ddd;display:table}
                    .news .post .image{background:#ddd;width:100%;height:18vw;min-height: 350px;margin-bottom:15px;margin-top: 15px;}
                    .news .post img{width:100%;height:100%;object-fit:cover}
                    .news .action{position:absolute;right:10px;background:#fff;padding:10px 5px;top:0}
                    .news .action .edit:before{font-family:FontAwesome;color:#666;content:"\f141"}
                    .news .action .dropdown-item-edit{top:30px;right:-10px}
                    .news .post .info{width:100%;margin-bottom:0;position:relative}
                    .news .post .block{width:100%;display:table;margin-bottom: 0}
                    .news .post .name{font-size:16px;line-height:16px;font-weight:600;color:#333;overflow:hidden;margin-bottom:5px}
                    .news .post .offer{max-height:60px;overflow:hidden}
                    .news .post .additionally{position:absolute;left:0;bottom:0}
                    .news .post .date{color:#666;font-size:12px;line-height:12px}
                    .news .post .date span{color:#888;font-weight:600}
                    .post_header{width:100%;vertical-align:middle;margin-bottom:10px;display:flex}
                    .post_header .post_image{width:50px;height:50px;display:inline-block;margin-right:10px}
                    .post_header .post_image img{width:100%;height:100%;object-fit:cover;border-radius:50%}
                    .post_info{width:250px;display:inline-block;margin:auto 0}
                    .news .post .offer.text-open {
                        overflow: visible;
                        height: auto;
                        max-height: none;
                    }
                    .text-open + .toggle-btn {
                        display: none;
                    }
                    .toggle-btn {
                        font-size: 14px;
                        line-height: 14px;
                        margin-top: 5px;
                        display: table;
                        color: #007bff;
                    }
                </style>
                    <div class="news tz-gallery" style="margin-top: 15px;" >
            @forelse($tape as $t)

                        <div class="post">
                            <div class="post_header">
                                <div class="post_image">
                                    <img src="/public/admin/img/user.png" alt="">
                                </div>
                                <div class="post_info">
                                    <div class="name">Павел Липаткин</div>
                                    <div class="date">{{$t->created_at or ''}}</div>
                                </div>
                            </div>
                            @if($t->description != null)
                            <div class="info">
                                <div class="block">
                                    <div class="offer">
                                        {{$t->description or ''}}
                                    </div>
                                    <a href="" class="toggle-btn">Читать полностью</a>
                                </div>
                            </div>
                            @endif
                            @if($t->image != null)
                            <div class="image">
                                <a href="{{asset('storage/app')}}/{{$t->image or '/public/none-img.png'}}">
                                    <img src="{{asset('storage/app')}}/{{$t->image or '/public/none-img.png'}}" alt="" class="lightbox">
                                </a>
                            </div>
                            @endif
                            <div class="action">
                                <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                    @if(Auth::user()->userRolle == 1)
                                    <li><a href="{{route('admin.tape.hide')}}?id={{$t->id}}">@if($t->published == 1)Скрыть @else Показать @endif</a></li>
                                    <li><a href="{{route('admin.tape.edit', $t)}}">Редактировать</a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                @empty
                @endforelse
                    </div>
                        <script>
                            $(".toggle-btn").click(function(e) {e.preventDefault();$(this).prev().toggleClass("text-open")})
                        </script>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<link rel="stylesheet" href="/public/site/open-image/open-image.css">
<script src="/public/site/open-image/open-image.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>