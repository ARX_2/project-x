<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Добавление сайта</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('admin.createSite')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
      <div class="modal-body">
        <label for="">Название сайта</label>

        <input type="text" class="form-control" name="title" placeholder="">
        <label for="">Тип сайта</label><br>
        <label for=""><input type="radio" name="type" value="1"> сайт услуг</label><br>
        <label for=""><input type="radio" name="type" value="2"> Салоны</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <input type="submit" class="btn btn-primary"  value="Сохранить">
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="updateSite" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Редактирование сайта</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('admin.updateSite')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
      <div class="modal-body">
        <label for="">Выбери сайт</label>
        <select class="form-control" name="siteid">
          @foreach($sites as $site)
          <option value="{{$site->id}}">{{$site->title}}</option>
          @endforeach
        </select>
        <label for="">Название сайта</label>
        <input type="text" class="form-control" name="title" placeholder="">
        <label for="">Тип сайта</label><br>
        <label for=""><input type="radio" name="type" value="1"> сайт услуг</label><br>
        <label for=""><input type="radio" name="type" value="2"> Салоны</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <input type="submit" class="btn btn-primary"  value="Сохранить">
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Добавление пользователя</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('admin.addUser')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
      <div class="modal-body">
        <label for="">Имя</label>
        <input type="text" class="form-control" name="name" placeholder="">
        <label for="">Email</label>
        <input type="text" class="form-control" name="email" placeholder="">
        <label for="">Папроль</label>
        <input type="text" class="form-control" name="password" placeholder="">
        <label for="">Выберите сайт</label>
        <select class="form-control" name="site">

          @foreach($sites as $site)
          <option value="{{$site->id}}">{{$site->title}}</option>
          @endforeach
        </select>
        <label for="">Выберите роль</label>
        <select class="form-control" name="userRolle">
          <option value="1">Разработчик</option>
          <option value="2">Босс</option>
          <option value="3">Администратор</option>
          <option value="4">Мастер</option>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <input type="submit" class="btn btn-primary"  value="Сохранить">
      </div>
      </form>
    </div>
  </div>
</div>



<div class="modal fade" id="colors" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Цвета</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('admin.banner.color')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
      <div class="modal-body">
        <label for="">Фон банера</label>
        <select class="form-control" name="color">
          <option value="banner_original">Оригинал</option>
          <option value="banner_light">Светлый</option>
          <option value="banner_dark">Темный</option>
        </select>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <input type="submit" class="btn btn-primary"  value="Сохранить">
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="background" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Фон</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('admin.background.color')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
      <div class="modal-body">
        <label for="">Цвет фона</label>
        <input type="text" name="background" class="form-control">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <input type="submit" class="btn btn-primary"  value="Сохранить">
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="slidemenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Выпадающее меню</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('admin.menu.slide')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
      <div class="modal-body">
        <label for="">Меню продукций</label><br>
        <label for="">Да
          <input type="radio" name="goods" value="0" >
        </label>
        <label for="">Нет
        <input type="radio" name="goods" value="1" >
      </label><br><br>
        <label for="">Меню услуг</label><br>
        <label for="">Да
          <input type="radio" name="services" value="0" >
        </label>
        <label for="">Нет
        <input type="radio" name="services" value="1" >
        </label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <input type="submit" class="btn btn-primary"  value="Сохранить">
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="gitpull" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Затянуть на хост</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <script type="text/javascript">
          $(document).ready(function(){
            $('#pull').click(function(){

              $('.git').html('<iframe src="{{route('admin.git.pull')}}" style="width:100%;height100vh;border:none;"></iframe>');
            });
          });
        </script>
      <div class="modal-body" style="height:300px;">
        <div class="git">


        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary" id="pull">Затянуть</button>
      </div>

    </div>
  </div>
</div>


<div class="modal fade" id="offer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content" >
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">форма заказов</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <script type="text/javascript">
        $(document).ready(function(){
          $('#offer').click(function(){

            $('.offer').html('<iframe src="/offer/services" style="width:100%;height: 70vh;border:none; padding:20px;"></iframe>');
          });
        });
      </script>
      <div class="modal-body" style="">
        <div class="offer">

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>

        <button type="submit" class="btn btn-primary" id="offer">Записаться?</button>
      </div>

    </div>
  </div>
</div>


@isset($Templates)
<div class="modal fade" id="templates" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Затянуть на хост</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form  action="{{route('admin.template.update')}}" method="post" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}

      <div class="modal-body" >
        <label for="">Главная</label>
        <select class="form-control" name="template[1][]">

          @forelse($Templates as $t)
          @if($t->template == 1)
          <option value="index" @if($t->templatename == 'index') selected="" @endif>index.blade</option>
          @endif

          @empty
          <option value="index">index.blade</option>
          @endforelse
        </select>
        <label for="">Продукция</label>
        <select class="form-control" name="template[2][]">

          @forelse($Templates as $t)
          @if($t->template == 2)

          <option value="chief_1" @if($t->templatename == 'chief_1') selected="" @endif>chief_1.blade</option>
          <option value="chief_2" @if($t->templatename == 'chief_2') selected="" @endif>chief_2.blade</option>
          @endif
          @empty
          <option value="chief_1">chief_1.blade</option>
          <option value="chief_2">chief_2.blade</option>
          @endforelse
        </select>
        <label for="">Услуги</label>
        <select class="form-control" name="template[3][]">
          @forelse($Templates as $t)
          @if($t->template == 3)
          <option value="chief_1" @if($t->templatename == 'chief_1') selected="" @endif>chief_1.blade</option>
              <option value="chief_2" @if($t->templatename == 'chief_2') selected="" @endif>chief_2.blade</option>
          @endif
          @empty
          <option value="chief_1">chief_1.blade</option>
          @endforelse
        </select>
        <label for="">Объекты</label>
        <select class="form-control" name="template[4][]">
          @forelse($Templates as $t)
          @if($t->template == 4)
          <option value="chief_1" @if($t->templatename == 'chief_1') selected="" @endif>chief_1.blade</option>
          @endif
          @empty
          <option value="chief_1">chief_1.blade</option>
          @endforelse
        </select>
        <label for="">О компании</label>
        <select class="form-control" name="template[5][]">
          @forelse($Templates as $t)
          @if($t->template == 5)
          <option value="chief_1" @if($t->templatename == 'chief_1') selected="" @endif>chief_1.blade</option>
          @endif
          @empty
          <option value="chief_1">chief_1.blade</option>
          @endforelse
        </select>
        <label for="">Контакты</label>
        <select class="form-control" name="template[6][]">
          @forelse($Templates as $t)
          @if($t->template == 6)
          <option value="chief_1" @if($t->templatename == 'chief_1') selected="" @endif>chief_1.blade</option>
          @endif
          @empty
          <option value="chief_1">chief_1.blade</option>
          @endforelse
        </select>
        <label for="">Продукция представление</label>
        <select class="form-control" name="template[7][]">
          @forelse($Templates as $t)
          @if($t->template == 7)
          <option value="view_1" @if($t->templatename == 'view_1') selected="" @endif>view_1.blade</option>
          @endif
          @empty
          <option value="view_1">view_1.blade</option>
          @endforelse
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <input type="submit" class="btn btn-primary"  value="Изменить">
      </div>
    </form>
    </div>
  </div>
</div>
@endisset
