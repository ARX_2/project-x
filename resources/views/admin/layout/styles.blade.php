<style>
    .admin-all {
        width: 100%;
    }
    .fixed-admin-nav {
        position: fixed;
        top: 85px;
        left: 0;
        width: 250px;
        margin-left: 0;
        bottom: 0;
        z-index: 100;
        border: none;
        box-shadow: 0 28px 28px rgba(0, 0, 0, 0.07);
        -webkit-transition: all 0.4s ease;
        -moz-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }
    .fixed-admin-nav .label {
        padding: 3px 6px 3px;
        font-size: 11px;
        border-radius: 50px;
        text-transform: capitalize;
        font-weight: 400;
        line-height: inherit;
    }

    .fixed-admin-nav .pull-right{

        position: absolute;
        right: 15px;
    }
    .fixed-admin-nav .navbar-nav{
        border: none;
        overflow-y: auto;
        width: auto;
        height: 100%;
        position: relative;
        border-radius: 0;
        margin: 0;

        background-color: #fff;
        backface-visibility: hidden;
    }
    .fixed-admin-nav  .scroll{
        position: relative;
        overflow: hidden;
        width: auto;
        height: 100%;
    }
    .fixed-admin-nav .navbar-nav{
        background: #272B34;
        padding-left: 0;
    }
    .fixed-admin-nav .navbar-nav ul{
        padding-left: 0;
        margin-bottom: 20px;
    }
    .fixed-admin-nav .navbar-nav li{
        width: 250px;
        list-style: none;
        color: #fff;
    }
    .fixed-admin-nav .navbar-nav li:hover{
        background: rgba(255, 255, 255, 0.1);
    }
    .fixed-admin-nav .navbar-nav li a{
        position: relative;
        display: block;
        padding: 12px 15px;
        color: #d7d7d7;
        text-decoration: none;
    }
    .fixed-admin-nav .navbar-nav .navigation-header:hover{
        background: none;
    }
    .fixed-admin-nav .navbar-nav .navigation-header span {
        font-size: 11px;
        color: #adadad;
        font-weight: 600;
        padding: 11px 15px;
        display: block;
        text-transform: uppercase;
    }
    .fixed-admin-nav .navbar-nav li i {
        font-size: 18px;
        width: 18px;
        top: 3px;
        display: inline-block;
        position: relative;
        margin-right: 15px !important;
    }
    .admin-content{
        margin-left: 250px;
        padding: 85px 20px;
        position: relative;
        background: #fff;
        -webkit-transition: all 0.4s ease;
        -moz-transition: all 0.4s ease;
        transition: all 0.4s ease;
        left: 0;
    }
    .admin .container{
        margin-left: 250px;
        padding: 85px 20px;
        position: relative;
        width: 80%;
        background: #fff;
        -webkit-transition: all 0.4s ease;
        -moz-transition: all 0.4s ease;
        transition: all 0.4s ease;
        left: 0;
    }
    .clearfix {
        overflow: hidden;
        clear: both;
        float: none;
    }
    .admin .collapse-lvl-1 li a{
        padding: 12px 15px 12px 51px;
    }
</style>
<style>
    .admin{

    }
    .admin h2{
        font-size: 20px;
    }
    .admin .breadcrumb{
        padding: 0;
        margin-bottom: 0;
        margin-top: 20px;
        float: right;
        list-style: none;
        background-color: #fff;
        border-radius: 4px;
    }
    .admin .breadcrumb a{
        color: #272B34;
        opacity: .5;
    }
    .admin .breadcrumb>.active {
        color: #272B34;
    }
    .admin hr{
        margin-top: 5px;
        margin-bottom: 20px;
    }
    .admin .li-btn{
        margin-bottom: 15px;
    }
    .admin .label-success {
        background-color: #5cb85c;
    }
    .admin .label-warning {
        background: #e8af48;
    }
    .admin .label-danger{
        background: #ff354d;
    }
    .admin .btn-primary {
        background-color: #2278dd;
        border-color: #2278dd;
    }
    .admin .avatar img{
        height: 43px;
        width: 43px;
        vertical-align: middle;
        border-radius: 50%;
        margin-right: 10px;
    }
    .admin .dropdown-menu>li>a{
        padding: 5px 20px;
    }
    .admin .avatar .dropdown-menu i{
        margin-right: 10px;
    }
    .admin .navbar{
        height: 85px;
        box-shadow: 0 2px 38px rgba(0, 0, 0, 0.1);
    }
    .admin .navbar-default{
        background-color: #fff;
        border-color: #fff;
        padding: 0 15px 0 0;
    }

    input:-moz-placeholder { color: red; }
    input::-webkit-input-placeholder { color: red; }

    .admin .navbar-header, .admin .navbar-collapse{
        padding-top: 15px;
    }
    .fixed-navbar-top{
        position: fixed;
        right: 0;
        left: 0;
        z-index: 1030;
    }
</style>
