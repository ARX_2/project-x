@if($user = Auth::user()->userRolle > 0)
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>
    <!-- Styles -->
    <?php //dd($user = Auth::user()->userRolle < 4) ?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @isset($option)
    <script type="text/javascript">
        $(document).ready(function () {
            $(".AddProperty").click(function () {
                $('.property').append('<div class="ad-property-input">\n' +
                    '<input type="text" name="Property[name][]" class="form-control ad-property-name" placeholder="Название характеристики">\n' +
                    '<input type="text" name="Property[title][]" class="form-control ad-property-qty" placeholder="0">\n' +
                    '<select class="form-control ad-property-type" name="Property[type][]">\n' +
                    '    <option value="">Выберите</option>\n' +
                    '    @foreach($type_property as $tp)\n' +
                    '    <option value={{$tp->id}}>{{$tp->type}}</option>\n' +
                    '    @endforeach\n' +
                    '</select>\n' +
                    '</div>');
            });


            $(function () {

                $('#dialog').dialog({
                    autoOpen: false
                });

                $('#addProperty').button().click(function (e) {
                    $('#dialog').dialog("open")
                });

            });
        });

    </script>
    @endisset
</head>
<body>





@include('admin.layout.styles')

<div id="app" class="admin">
    <nav class="navbar navbar-default navbar-static-top fixed-navbar-top">
        <div class="">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img class="brand-img" src="http://hencework.com/theme/winkle-demo/img/logo.png" alt="brand">
                    <span style="color:rgb(222,17,17)">!N</span><span style="color:rgb(44,121,212)">Frame</span>
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->


                @if($user = Auth::user()->userRolle == 1)
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle"  data-toggle="dropdown" role="button"
                           aria-expanded='false'>Панель</a>
                        <ul class="dropdown-menu" role="menu">


                            <li>
                                <a href="#" data-toggle="modal" data-target="#exampleModalLong" id="createSite">Добавить сайт</a>
                            </li>
                            <li>
                              <a href="#" data-toggle="modal" data-target="#updateSite">Редактировать сайт</a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-target="#addUser">Добавить пользователя</a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-target="#colors">Цвета</a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-target="#background">Фон</a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-target="#slidemenu">Выпадающее меню</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                @endif
                @if($user = Auth::user()->userRolle == 1)
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" data-toggle="modal" data-target="#gitpull">Git pull</a></li>
                </ul>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" data-toggle="modal" data-target="#offer">Форма услуг</a></li>
                </ul>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="{{route('admin.admin.index')}}">Адмиинка</a></li>
                </ul>

                @isset($Templates)
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" data-toggle="modal" data-target="#templates">Выбор шаблонов</a></li>
                </ul>
                @endisset
                @endif

                @isset($option)
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded='false'>Опции</a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a  data-toggle="modal" data-target="#exampleModal" style="cursor:pointer;">Добавить ед.измерения в характеристики</a></li>
                            <li><a data-toggle="modal" data-target="#delete_property" style="cursor:pointer;">Удалить ед.измерения в характеристиках</a></li>
                        </ul>
                </ul>
                @endisset

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-right avatar">
                    <!-- Authentication Links -->
                    @guest
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                            <img src="http://hencework.com/theme/winkle-demo/img/user1.png" alt="user_auth" class="user-auth-img img-circle">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href=""><i class="fa fa-user" aria-hidden="true"></i>Профиль</a>
                                <a href=""><i class="fa fa-rub" aria-hidden="true"></i>Мой баланс</a>
                                <a href=""><i class="fa fa-envelope-open" aria-hidden="true"></i>Мои сообщения</a>
                                <a href=""><i class="fa fa-cog" aria-hidden="true"></i>Настройки</a>
                                <hr>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off" aria-hidden="true"></i><b>Выйти</b>
                                </a>


                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <div class="admin-all">
        <div class="fixed-admin-nav">
            <div class="scroll">
                <div class="navbar-nav">
                    <!-- <ul>
                        <li class="navigation-header"><span>Главное</span></li>
                        @if($user = Auth::user()->userRolle < 5)
                        <li>
                            <a href="{{route('admin.work.index')}}">
                                <div class="pull-left">
                                    <i class="fa fa-rub" aria-hidden="true"></i>
                                    <span class="right-nav-text">Заказы</span>
                                </div>
                                <div class="pull-right"><span class="label label-success">@isset($countwork)+{{$countwork}}@endisset</span></div>
                                <div class="clearfix"></div>
                            </a>
                        </li>
                        @endif
                        @if($user = Auth::user()->userRolle < 3)
                        <li>
                            <a href="javascript:void(0);">
                                <div class="pull-left">
                                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                    <span class="right-nav-text">Новости</span>
                                </div>
                                <div class="pull-right"><span class="label label-warning">Разраб</span></div>
                                <div class="clearfix"></div>
                            </a>
                        </li>
                        @endif
                    </ul> -->
                    <ul>
                        <li class="navigation-header"><span>Наполнение сайта</span></li>
                        @if($user = Auth::user()->userRolle < 3)
                        <li>
                            <a href="{{route('admin.banner.index')}}">
                                <div class="pull-left">
                                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                                    <span class="right-nav-text">Баннер</span>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.category.index')}}">
                                <div class="pull-left">
                                    <i class="fa fa-th-list" aria-hidden="true"></i>
                                    <span class="right-nav-text">Категория</span>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.goods.index')}}">
                                <div class="pull-left">
                                    <i class="fa fa-product-hunt" aria-hidden="true"></i>
                                    <span class="right-nav-text">Товары</span>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </li>
                        <!-- <li>
                            <a href="javascript:void(0);">
                                <div class="pull-left">
                                    <i class="fa fa-sticky-note-o" aria-hidden="true"></i>
                                    <span class="right-nav-text">Статьи</span>
                                </div>
                                <div class="pull-right"><span class="label label-warning">Разраб</span></div>
                                <div class="clearfix"></div>
                            </a>
                        </li> -->
                        <li>
                            <a href="{{route('admin.object.index')}}">
                                <div class="pull-left">
                                    <i class="fa fa-th-large" aria-hidden="true"></i>
                                    <span class="right-nav-text">Объекты</span>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </li>
                        <!-- <li>
                            <a href="javascript:void(0);">
                                <div class="pull-left">
                                    <i class="fa fa-user-secret" aria-hidden="true"></i>
                                    <span class="right-nav-text">Отзывы</span>
                                </div>
                                <div class="pull-right"><span class="label label-warning">Разраб</span></div>
                                <div class="clearfix"></div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="pull-left">
                                    <i class="fa fa-align-left" aria-hidden="true"></i>
                                    <span class="right-nav-text">Описание</span>
                                </div>
                                <div class="pull-right"><span class="label label-warning">Разраб</span></div>
                                <div class="clearfix"></div>
                            </a>
                        </li> -->
                        @endif
                    </ul>
                    @if($user = Auth::user()->userRolle < 3 )
                    <ul>
                        <li class="navigation-header"><span>Настройки</span></li>
                        <li>
                            <a href="javascript:void(0);" class="collapsed" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <div class="pull-left">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                    <span class="right-nav-text">Компания</span>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                            <ul id="collapseTwo" class="collapse-lvl-1 collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <li>

                                  @if(count($Company)>0)
                                    <a href="/admin/company/{{$Company[0]->id}}/edit">О компании</a>
                                    @else
                                    <a href="{{route('admin.company.create')}}">О компании</a>
                                  @endif
                                </li>
                                <li>
                                    <a href="{{route('admin.information.edit')}}?type=logo">Логотип</a>
                                </li>
                                <li>
                                    <a href="{{route('admin.information.edit')}}?type=contact">Контакты</a>
                                </li>
                                <li>
                                    <a href="{{route('admin.information.edit')}}?type=map">Карта</a>
                                </li>
                                <li>
                                    <a href="{{route('admin.companyprice.index')}}">О компании</a>
                                </li>
                            </ul>

                        </li>



                        <li>
                            <a href="{{route('admin.menu.create')}}">
                                <div class="pull-left">
                                    <i class="fa fa-align-left" aria-hidden="true"></i>
                                    <span class="right-nav-text">Меню</span>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </li>

                        <!-- <li>
                            <a href="javascript:void(0);">
                                <div class="pull-left">
                                    <i class="fa fa-magnet" aria-hidden="true"></i>
                                    <span class="right-nav-text">Виджеты</span>
                                </div>
                                <div class="pull-right"><span class="label label-warning">Разраб</span></div>
                                <div class="clearfix"></div>
                            </a>
                        </li> -->
                        @endif
                        @if($user = Auth::user()->userRolle < 4 )
                        <li>
                            <a href="{{route('admin.employee.index')}}">
                                <div class="pull-left">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <span class="right-nav-text">Сотрудники</span>
                                </div>

                                <div class="clearfix"></div>
                            </a>
                        </li>
                        <!-- <li>
                            <a href="{{route('admin.services.index')}}">
                                <div class="pull-left">
                                    <i class="fa fa-usd" aria-hidden="true"></i>
                                    <span class="right-nav-text">Услуги</span>
                                </div>

                                <div class="clearfix"></div>
                            </a>
                        </li>
                        @endif
                        @if($user = Auth::user()->userRolle < 3 )
                        <li>
                            <a href="javascript:void(0);">
                                <div class="pull-left">
                                    <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                    <span class="right-nav-text">Статистика и верификация</span>
                                </div>
                                <div class="pull-right"><span class="label label-warning">Разраб</span></div>
                                <div class="clearfix"></div>
                            </a>
                        </li> -->
                        <li>
                            <a href="{{route('admin.seo.index')}}">
                                <div class="pull-left">
                                    <i class="fa fa-align-left" aria-hidden="true"></i>
                                    <span class="right-nav-text">SEO описание</span>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </li>

                        <li>
                            <a href="{{route('admin.support.index')}}">
                                <div class="pull-left">
                                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                                    <span class="right-nav-text">Тех. Поддержка</span>
                                </div>
                                <div class="clearfix"></div>
                            </a>
                        </li>

                    </ul>
                    @endif

                </div>

            </div>
        </div>

        @yield('content')
    </div>
</div>
<?php //dd($Users); ?>
@if($user = Auth::user()->userRolle = 1)
@include('admin.layout.adminmodels')
@endif
    <!-- Scripts -->
    <script src="{{ asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    {{--@include("layouts.scripts")--}}
</body>
</html>
@else

@endif
