@if ($paginate->lastPage() > 1)
<ul class="pagination">
  <li class="page-item {{ ($paginate->currentPage() == 1) ? ' disabled' : '' }}">
      <a class="page-link" href="{{ $paginate->url(1) }}">Назад</a>
  </li>
  @for ($i = 1; $i <= $paginate->lastPage(); $i++)
      <li class="page-item {{ ($paginate->currentPage() == $i) ? ' active' : '' }}" id="page{{$i}}">
          <a class="page-link" href="javascript:void(0);">{{ $i }}</a>
      </li>
      <script type="text/javascript">
        $('#page{{$i}}').click(function(){
          $.get('{{ $paginate->url($i) }}&&ajax=true', function(data){
            $('.admin-content').html(data);
          });
        });
      </script>
  @endfor
  <li class="page-item {{ ($paginate->currentPage() == $paginate->lastPage()) ? ' disabled' : '' }}">
      <a class="page-link" href="{{ $paginate->url($paginate->currentPage()+1) }}" >Вперед</a>
  </li>
</ul>
@endif
