
<div class="fixed-admin-nav ">
    <div class="scroll">
        <div class="navbar-nav">
            <ul>
                <li class="navigation-header"><span>Главное</span></li>
                <style>
                    .a_s{}
                    .a_s .parent_s{display: table;position: relative;width: 100%}
                    .a_s .parent_s a{width: 100%;display: table;}
                    .a_s .parent_s i{display: table-cell;vertical-align: middle;min-width: 18px;max-width: 18px;}
                    .a_s .parent_s .name{display: table-cell;padding: 0 15px;width: 100%;}
                    .a_s .parent_s .new_name{color: #888;font-size: 12px}
                    .a_s .parent_s a:after {font-family: FontAwesome;content: "\f105";display: table-cell;vertical-align: middle;font-weight: 400;color: #666;}
                    .a_s .parent_s.more a:after {content: "\f106"}
                    .a_s .parent_s.more.collapsed a:after {content: "\f107"}
                    .c_lvl_1 li{display: table;position: relative;width: 100%}
                    .c_lvl_1 li a{width: 100%;display: table;}
                    .c_lvl_1 li i{display: table-cell;vertical-align: middle}
                    .c_lvl_1 li .name{display: table-cell;padding: 0 15px;width: 100%;}
                    .c_lvl_1 li .new_name{color: #888;font-size: 12px}
                    .c_lvl_1 li a:before {font-family: FontAwesome;content: "\f111 ";color: #888;font-size: 5px;vertical-align: middle;width: 18px;display: table-cell;min-width: 18px;padding: 0 4px;max-width: 18px;}
                    .c_lvl_1 li a:after {font-family: FontAwesome;content: "\f105";display: table-cell;vertical-align: middle;font-weight: 400;color: #666;}
                    .c_lvl_1 .add_section{background: #269ffd;color: #fff!important;}
                    .c_lvl_1 .add_section:before{color: #fff}
                    .c_lvl_1 .add_section:after{color: #fff}
                </style>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="javascript:void(0);">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                            <span class="name">Лента новостей</span>
                        </a>
                    </div>
                </li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="javascript:void(0);">
                            <i class="fa fa-bar-chart" aria-hidden="true"></i>
                            <span class="name">Статистика</span>
                        </a>
                    </div>
                </li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="{{route('admin-salon.work.index')}}" >
                            <i class="fa fa-align-left" aria-hidden="true"></i>
                            <span class="name">Рабочий день</span>
                        </a>
                    </div>
                </li>
                <!-- <li class="a_s">
                    <div class="parent_s">
                        <a href="javascript:void(0);">
                            <i class="fa fa-bullhorn" aria-hidden="true"></i>
                            <span class="name">Реклама</span>
                        </a>
                    </div>
                </li> -->

                <li class="a_s">
                    <div class="parent_s">
                        <a href="" class="name_link">
                            <i class="fa fa-bullhorn" aria-hidden="true"></i>
                            <span class="name">Список клиентов</span>
                        </a>
                    </div>
                </li>
            </ul>
            <ul>
                <li class="navigation-header"><span>Управление</span></li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="{{route('admin.category.index')}}" class="name_link">
                            <i class="fa fa-th-list" aria-hidden="true"></i>
                            <span class="name">Категории</span>
                        </a>
                    </div>
                </li>
    <?php //dd($company[0]);?>
    @if($company[0] !== null)

    <li class="a_s">
        <div class="parent_s">
            <a href="{{route('admin-salon.goods.index')}}" class="name_link">
                <i class="fa fa-cubes" aria-hidden="true"></i>
                <div class="name">Товары<div class="new_name">{{$company[0]->sections[0]->title or ''}}</div></div>
            </a>
        </div>
    </li>

    @else
    @if($company->sections[0]->published == 1)
                <li class="a_s">
                    <div class="parent_s">
                        <a href="{{route('admin-salon.goods.index')}}" class="name_link">
                            <i class="fa fa-cubes" aria-hidden="true"></i>
                              <div class="name">Товары<div class="new_name">{{$company->sections[0]->title or ''}}</div></div>
                        </a>
                    </div>
                </li>
                @endif
@endif
    <li class="a_s">
      <div class="parent_s">
          <a href="{{route('admin-salon.materials.index')}}" class="name_link">
              <i class="fa fa-diamond" aria-hidden="true"></i>
              <div class="name">Материалы<div class="new_name">Материалы</div></div>

            </a>
          </div>
        </li>
          @if($company[0] !== null)

          <li class="a_s">
              <div class="parent_s">
                  <a href="{{route('admin-salon.services.index')}}" class="name_link">
                      <i class="fa fa-handshake-o" aria-hidden="true"></i>
                      <div class="name">Услуги<div class="new_name">{{$company[0]->sections[1]->title or ''}}</div></div>
                  </a>
              </div>
          </li>

          @else
          @if($company->sections[1]->published == 1)
                <li class="a_s">
                    <div class="parent_s">
                        <a href="{{route('admin-salon.services.index')}}" class="name_link">
                            <i class="fa fa-handshake-o" aria-hidden="true"></i>
                            <div class="name">Услуги<div class="new_name">{{$company->sections[1]->title or ''}}</div></div>

                        </a>
                    </div>
                </li>
                @endif
            @endif

                <li class="a_s">
                    <div class="parent_s more collapsed toggle" id="ASection_1" data-toggle="collapse" data-target="#ASection-1" aria-expanded="false" aria-controls="ASection-1">
                        <a href="javascript:void(0);">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                            <span class="name toggle">Разделы</span>
                        </a>
                    </div>
                    <ul id="ASection-1" class="c_lvl_1 collapse" aria-labelledby="ASection_1" data-parent="#accordion">
                        <li><a href="{{route('admin.banner.index')}}" class="name_link"><div class="name">Баннер</div></a></li>
                        <li><a href="{{route('admin.tizers.index')}}" class="name_link"><div class="name">Тизеры</div></a></li>
                        {!! Resize::sections() !!}
                        <li><a href="{{route('admin.sections.index')}}" class="add_section"><div class="name">Добавить</div></a></li>
                    </ul>
                </li>
            </ul>
            <ul>
                <li class="navigation-header"><span>Настройки</span></li>
                <li class="a_s">
                    <div class="parent_s more collapsed toggle" id="ASection_2" data-toggle="collapse" data-target="#ASection-2" aria-expanded="false" aria-controls="ASection-2">
                        <a href="javascript:void(0);">
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                            <span class="name toggle">Компания</span>
                        </a>
                    </div>
                    <ul id="ASection-2" class="c_lvl_1 collapse" aria-labelledby="ASection_2" data-parent="#accordion">
                        <li><a href="{{route('admin.company.index')}}" ><span class="name">Информация о компании</span></a></li>
                        <li><a href="/admin-salon/employee/" ><span class="name">Сотрудники</span></a></li>
                    </ul>
                </li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="/admin/support/view/{{Resize::lastComment()}}">
                            <i class="fa fa-handshake-o" aria-hidden="true"></i>
                            <span class="name" id="support">Поддержка {!! Resize::countMessages() !!} </span>
                        </a>
                    </div>
                </li>
                @if(Auth::user()->userRolle == 1)
                <li class="a_s">
                    <div class="parent_s">
                        <a href="/admin/settings/site/index">
                            <i class="fa fa-wrench" aria-hidden="true"></i>
                            <div class="name">Сайт</div>
                        </a>
                    </div>
                </li>
                @endif
            </ul>
            @if(Auth::user()->userRolle == 1)

            <ul>
                <li class="navigation-header"><span>Разработчик</span></li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="{{route('admin.admin.index')}}">
                            <i class="fa fa-refresh" aria-hidden="true"></i>
                            <span class="name">Изменить сайт</span>
                        </a>
                    </div>
                </li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#gitpull">
                            <i class="fa fa-gitlab" aria-hidden="true"></i>
                            <span class="name">Git Pull</span>
                        </a>
                    </div>
                </li>
                <li class="a_s">
                    <div class="parent_s">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#gitpush">
                            <i class="fa fa-gitlab" aria-hidden="true"></i>
                            <span class="name">Git Push</span>
                        </a>
                    </div>
                </li>
                <input type="checkbox" id="inputAtmosphere" value="1" @if(Auth::user()->atmosphere ==1) checked @endif @if(Auth::user()->atmosphere ==1) class="atmosphereNight" @else class="atmosphereDay" @endif>
            </ul>
            @endif
        </div>
    </div>
</div>
@if(Auth::user()->id == 27)
<style media="screen">
  .atmosphere:hover{
    background: none !important;
  }
  @if(Auth::user()->atmosphere == 1)
  .fixed-admin-nav .navbar-nav li a:hover{color:white;}
  @endif
.cp_edit .switch .slider{background-color:#2196F3;}
.cp_edit .switch input:checked + .slider{background-color:#ffdd60;}
</style>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="checkbox"]').click(function() {
            if($(this).attr('id') == 'inputAtmosphere') {
                if (this.checked) {
                  @if(Auth::user()->id == 27)
                  $('.Night').html('<style media="screen">.navbar-default .navbar-nav>li>a, .navbar-default .navbar-brand, .nav>li>a, .fixed-admin-nav .navbar-nav li a, .admin .breadcrumb>.active, .admin .breadcrumb a, .admin h2, .general {color:rgba(255,255,255,0.85);}.nav>li>a:hover, .nav .open>a, .nav .open>a:hover, .nav .open>a:focus, .fixed-admin-nav .navbar-nav, .navbar.navbar-inverse,.navbar.navbar-inverse.navbar-fixed-top .nav-header {background-color: #18181899 ;}.admin-content, .admin .breadcrumb{background-color: #222;} .admin .navbar-default{border-color: #584e4e;} .c_p_company .narrow, .c_p_company .narrow .box, .c_p_company .wide, .dropdown-menu>li>a:hover, .box, .mini_box {background-color:#2f2f2f !important;}.user, .site, body, .info .date, .c_p_company .wide .name,.b_user .info .name, .production .item .quantity, .cp_edit .radio-name, .price span, .edit-label, .name, h4,p, .production .action .edit:before{color:rgba(255,255,255,0.85) !important;}.template .button-save{border-color:black;}body{background-color: #222;}.item a, .link a, .rmenu a, .template .item, .info a, .user a, .center a, .right a, .c_p_company .narrow .setting .edit-clarification a,.production .sort_block .category .sort_select{color:#ffdd60 !important;}input, button, select, textarea, .c_p_company .narrow .setting .field input, .c_p_company .narrow .setting textarea, .c_p_company .narrow .setting select, .cp_edit input, .cp_edit textarea, .form-control, .cke_chrome{background-color: #222; border-color: rgba(255,255,255,0.2) !important; color:white;}.template .item{border-color:#2f2f2f;}.c_p_company .rmenu li:hover, .item:hover{background-color: #222 !important;}.b_user .user:hover, .admin-content .dropdown-item-edit, .modal-content .vm-goods{background-color: #222;}.cacheColor {border-color: #ffdd60 !important;}.fixed-admin-nav .navbar-nav li a:hover{color:white !important;}</style>');
                  @else
                  $('.Night').html('');
                  @endif
                  $.get('{{route("admin.change.atmosphere")}}', function(data) {});
                } else {
                  @if(Auth::user()->id == 27)
                  $('.Day').html('<style>.fixed-admin-nav .navbar-nav li a:hover{color:#428bca !important;}</style>');
                  @else
                  $('.Day').html('');
                  @endif
                  $.get('{{route("admin.change.atmosphere")}}', function(data) {});
                  $('.Night').html('');

                }
            }
        });
    });


</script>
<!-- <script type="text/javascript">
  $(document).ready(function(){

    $('.atmosphereNight').click(function(){
      $('.Day').html('<style>.fixed-admin-nav .navbar-nav li a:hover{color:#428bca !important;}</style>');
      $.get('{{route("admin.change.atmosphere")}}', function(data) {});
    });
    $('.atmosphereDay').click(function(){
      $('.Night').html('<style media="screen">.navbar-default .navbar-nav>li>a, .navbar-default .navbar-brand, .nav>li>a, .fixed-admin-nav .navbar-nav li a, .admin .breadcrumb>.active, .admin .breadcrumb a, .admin h2, .general {color:rgba(255,255,255,0.85);}.nav>li>a:hover, .nav .open>a, .nav .open>a:hover, .nav .open>a:focus, .fixed-admin-nav .navbar-nav, .navbar.navbar-inverse,.navbar.navbar-inverse.navbar-fixed-top .nav-header {background-color: #18181899 ;}.admin-content, .admin .breadcrumb{background-color: #222;} .admin .navbar-default{border-color: #584e4e;} .c_p_company .narrow, .c_p_company .narrow .box, .c_p_company .wide, .dropdown-menu>li>a:hover, .box {background-color:#2f2f2f !important;}.user, .site, body, .info .date, .c_p_company .wide .name,.b_user .info .name, .production .item .quantity, .cp_edit .radio-name, .price span, .edit-label, .name, h4,p, .production .action .edit:before{color:rgba(255,255,255,0.85) !important;}.template .button-save{border-color:black;}body{background-color: #222;}.item a, .link a, .rmenu a, .template .item, .info a, .user a, .center a, .right a{color:#ffdd60 !important;}input, button, select, textarea, .c_p_company .narrow .setting .field input, .c_p_company .narrow .setting textarea, .c_p_company .narrow .setting select, .cp_edit input, .cp_edit textarea, .form-control, .cke_chrome{background-color: #222; border-color: rgba(255,255,255,0.2) !important; color:white;}.template .item{border-color:#2f2f2f;}.c_p_company .rmenu li:hover, .item:hover{background-color: #222 !important;}.b_user .user:hover, .admin-content .dropdown-item-edit, .modal-content .vm-goods{background-color: #222;}.cacheColor {border-color: #ffdd60 !important;}.fixed-admin-nav .navbar-nav li a:hover{color:white !important;}</style>');
      $.get('{{route("admin.change.atmosphere")}}', function(data) {});
    });
  });
</script> -->
@if(Auth::user()->userRolle == 1)
<!--  -->
    <div class="modal fade" id="gitpull" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Затянуть на хост</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

            <script type="text/javascript">
              $(document).ready(function(){
                $('#pull').click(function(){

                  $('.git').html('<iframe src="{{route('admin.git.pull')}}" style="width:100%;height100vh;border:none;"></iframe>');
                });
              });
            </script>
          <div class="modal-body" style="height:300px;">
            <div class="git">

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button type="submit" class="btn btn-primary" id="pull">Затянуть</button>
          </div>

        </div>
      </div>
    </div>

     <div class="modal fade" id="gitpush" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Затянуть на хост</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            <input type="text" id="commit" placeholder="С хоста">
            <script type="text/javascript">
              $(document).ready(function(){
                $('#push').click(function(){
                    var commit = $('#commit').val();
                    //alert(commit);
                     $.post('{{route('admin.git.push')}}', {commit:commit}, function(data){
                         $('.gitLog').text(data);
                     });
                });
              });
            </script>
          <div class="modal-body" style="height:300px;">
            <div class="gitLog">

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button type="submit" class="btn btn-primary" id="push">Вытолкнуть</button>
          </div>

        </div>
      </div>
    </div>
    @endif
