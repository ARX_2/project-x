<style>
    .messages .block{margin:0 -10px}
    .messages .item{display:table;width:100%;padding:10px 10px;border-bottom:1px solid #ddd}
    .messages .item .left{float:left;width:60px;max-width:10%;text-align:right}
    .messages .item:hover{background:rgba(233,235,238,0.5)}
    .messages .item .left .image img{width:60px;height:60px;border-radius:50%}
    .messages .item .center{float:left;width:90%;padding-left:10px}
    .messages .item .center .name{font-weight:600;color:#333;margin-bottom:3px}
    .messages .item .mess{color:#333;margin-bottom: 3px}
    .messages .item .date{color:#888;font-size: 13px;margin: 0;}
    .messages .form-item{padding-top:20px;padding-bottom:40px;border-bottom:1px solid #ddd;margin-bottom:40px}
    .messages .angle-right:before{font-family:FontAwesome;content:"\f105";padding:0 15px;color:#888}

    .modal-ui-wrapper{top:0;left:0;right:0;bottom:0;position:fixed;width:100%;background:#fff;justify-content:center;align-items:center;z-index:2500;overflow-x:hidden;overflow:auto;display:flex}
    .modal-ui-modal{min-width:320px;width:750px;min-height:150px;max-height:none;overflow-x:hidden;overflow-y:auto;padding:20px 0;justify-content:center;align-items:center;position:relative;display:table}
    .modal-ui-content{width:100%;height:100%;z-index:10;display:block}
    .modal-ui-wrapper{align-items:normal}
    .modal-ui-modal{margin:40px 0}
    .modal-ui-close{top:-7px;right:10px;color:#222;font-size:50px;font-size:60px;line-height:60px;font-weight:700;display:table;position:absolute;color:#fff;top:0;right:-50px;z-index:9;cursor:pointer}
    .modal-ui-close span{display:block}
    .modal-ui-close{top:-7px;right:10px;color:#222;font-size:50px}
    .modal-ui-content .title{padding:0;width:100%;margin-bottom:20px}
    .modal-ui-content .title h2{font-weight:600;font-size:18px;line-height:18px;color:#444;margin:0;width:90%}
    .modal-ui-content .title .io{display: block;margin-top: 5px}
    .modal-ui-content .title span{}
    .modal-ui-content .title span a{text-decoration: underline;}

    .modal_tab{    display: flex;
        width: 100%;}
    .modal_tab .info{display: inline-block;
        width: 43%;}
    #savename {
        display: none;
        margin-top: 5px;
    }
    select[name='user_id']{
        border: none;
        border-bottom: 1px solid #ddd;
    }
    select[name='column_id']{
        border: none;
        border-bottom: 1px solid #ddd;
    }
</style>

<div id="modal_form" class="modal-ui-wrapper" style="background: rgba(0, 0, 0, 0.565);">
    <div class="modal-ui-modal" style="background: rgb(255, 255, 255);padding: 0">
        <div class="modal-ui-content tz-gallery" style="padding: 20px">
            <div class="title">
                <form id="nameorder" method="POST" >
                    {{ csrf_field() }}
                    <input type="hidden" name="order_id" value="{{$order->id or ''}}">
                    <h2><input type="text" name="title" placeholder="Введите название заказа..." value="{{$order->title or ''}}" style="border: none;width: 100%;" autocomplete="off"></h2>
                    <div id="savename"><div class="data"><button class="comment_buttom" type="submit">Сохранить</button></div></div>
                </form>
                <div class="io">
                    <span>Ответственный:
                        <select name="user_id">
                            <option value="0" @if($order->user_id == null) selected="selected" @endif>Назначить</option>
                            @forelse($responsible as $r)
                                <option value="{{$r->id}}" @if($order->user_id == $r->id) selected="selected" @endif>{{$r->name}} {{$r->lastname}}</option>
                            @empty
                            @endforelse
                        </select>,
                    </span>
                    <span>в колонке:
                        <select name="column_id">
                            <option value="0" disabled @if($order->column_id == null) selected="selected" @endif>Выберите</option>
                            @forelse($column as $c)
                                <option value="{{$c->id}}" @if($order->column_id == $c->id) selected="selected" @endif>{{$c->title}}</option>
                            @empty
                            @endforelse
                        </select>
                    </span>
                </div>
            </div>
            <script>
                $(document).ready(function(){
                    $("select[name='user_id']").bind("change", function () {
                        $.post('/admin/order/modal/responsible', {order_id: '{{$order->id}}', _token: '{!! csrf_token() !!}', user_id: $("select[name='user_id']").val()}, function(data){
                            console.log(data);
                        });
                    });
                    $("select[name='column_id']").bind("change", function () {
                        $.post('/admin/order/modal/selectcolumn', {order_id: '{{$order->id}}', _token: '{!! csrf_token() !!}', column_id: $("select[name='column_id']").val()}, function(data){
                            console.log(data);
                        });
                    });
                    $('input[name="title"]').focus(function(){$('#savename').show();})
                    $('#nameorder').on('submit', function(e){
                        e.preventDefault();
                        $.ajax({
                            type: 'POST',
                            url: '/admin/order/modal/editname',
                            data: $('#nameorder').serialize(),
                            success:function(data){
                                console.log(data);
                            }
                        });
                        $('#savename').hide();
                    });
                });
            </script>
            <div class="modal_tab">
                <div class="info">
                    <style>
                        .product{
                            width: 100%;
                            margin-bottom: 15px;
                            padding-bottom: 0px;
                            border-bottom: 1px solid #ddd;}
                        .product .item{display: flex;    padding: 10px 0;    border-bottom: 1px solid #ddd;}
                        .product .item:last-child{border-bottom: 0}
                        .product .image{    width: 80px;min-width: 80px;
                            height: 80px;display: inline-block;}
                        .product .image img{    width: 100%;
                            height: 100%;
                            object-fit: cover;}
                        .product  .product_info{display: inline-block;
                            margin-left: 10px; width: 100% ; position: relative;}
                        .product .product_name{font-weight: 600;
                            height: 36px;
                            line-height: 16px;
                            overflow: hidden;
                            text-overflow: ellipsis;}
                        .product .product_bottom{    position: absolute;
                            bottom: 0;
                            left: 0;}
                        .product .product_qty{font-size: 13px;}
                        .product .prodict_price{font-size: 13px;color: #666;}
                    </style>
                    <div class="product">
                        @forelse($order->order_goods as $g)
                            <div class="item">
                                <div class="image"><img src="https://komandor-tyumen.ru/storage/app/imageResize/public/5cfa7b6edcb57_400x270.webp" alt=""></div>
                                <div class="product_info">
                                    <div class="product_name">{{$g->goods->title ?? ''}}</div>
                                    <div class="product_bottom">
                                        <div class="product_qty">Количество: {{$g->count ?? '0'}}шт. +-</div>
                                        <div class="prodict_price">Цена: <span style="font-weight: 600;color: #222;">{{$g->coast * $g->count}} руб</span> <span>({{$g->coast ?? '0'}} руб/шт.)</span></div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="no_product" style="padding-bottom: 15px;">Товары не добавлены</div>
                        @endforelse
                            <div class="data">
                                <div class="data-label">Общая:</div>
                                <div class="data-block"><input type="text" name="phone" value="" placeholder="0" style="width:75px"> руб</div>
                            </div>
                            <div class="data" style="margin-top: 15px;"><button class="comment_buttom" type="submit">Сохранить</button></div>
                    </div>
                    <style>
                        .order_model_info{margin-bottom: 15px;padding-bottom: 15px;border-bottom: 1px solid #ddd;}
                    </style>
                    {{--<div class="order_model_info">
                        <div class="data"><div class="data-label">Оплачен:</div><div class="data-block">10% (2000 руб)</div></div>
                        <div class="data"><div class="data-label">Доставка:</div><div class="data-block">СДЕК</div></div>
                    </div>--}}
                    <style>
                        .company .name{font-size: 16px;margin-bottom: 10px;}
                        .person .name{font-size: 16px;margin-bottom: 10px;}
                        .person{    margin-bottom: 15px;padding-bottom: 15px;border-bottom: 1px solid #ddd;}
                        .data{display:flex;width:100%;margin-bottom:5px}
                        .data-label{display:inline-block;margin:0;width:35%}
                        .edit-block{display:inline-block;width:60%}
                        .data-block{width: 65%;display: inline-block;}
                        .data-block a{text-decoration:underline}
                        .company{margin-bottom: 15px;padding-bottom: 15px;border-bottom: 1px solid #ddd;}
                        .data input{border: none;border-bottom: 1px solid #ddd;background: #fff;outline: none;}
                        .savecompany, .saveperson{font-size: 13px;color: #888;}
                        .i_hidden{opacity: 0;transition: all 1s linear;}
                    </style>
                    <div class="person">
                        <div class="name">Клиент<span class="saveperson i_hidden"> - сохранено</span></div>
                        <form id="editperson" method="POST" >
                            {{ csrf_field() }}
                            <div class="data">
                                <div class="data-label">Имя:</div>
                                <div class="data-block"><input type="text" name="name" value="{{$order->order_customer->name ?? ''}}" placeholder="Узнать имя!"></div>
                            </div>
                            <div class="data">
                                <div class="data-label">Телефон:</div>
                                <div class="data-block"><input type="text" name="phone" value="{{$order->order_customer->phone ?? ''}}" placeholder="+7 (___) ___-__-__"></div>
                            </div>
                            <div class="data">
                                <div class="data-label">Email:</div>
                                <div class="data-block"><input type="text" name="email" value="{{$order->order_customer->email ?? ''}}" placeholder="Адрес эл. почты"></div>
                            </div>
                            <div class="data">
                                <div class="data-label">Должность:</div>
                                <div class="data-block"><input type="text" name="job_position" value="{{$order->order_customer->job_position ?? ''}}" placeholder="Например: менеджер"></div>
                            </div>
                            <div class="data">
                                <div class="data-label">Skype:</div>
                                <div class="data-block"><input type="text" name="skype" value="{{$order->order_customer->skype ?? ''}}"></div>
                            </div>
                            {{--<div class="data"><div class="data-label">Viber:</div><div class="data-block"><a href="">Написать</a></div></div>
                            <div class="data"><div class="data-label">WhatsApp:</div><div class="data-block"><a href="">Написать</a></div></div>--}}
                            <div class="data">
                                <div class="data-label">ВК:</div>
                                <div class="data-block"><input type="text" name="vk" value="{{$order->order_customer->vk ?? ''}}" placeholder="vk.com/"></div>
                            </div>
                            <input type="hidden" name="order_id" value="{{$order->id ?? ''}}">
                            <div class="data" style="margin-top: 15px;"><button class="comment_buttom" type="submit">Сохранить</button></div>
                        </form>
                    </div>

                    <script>
                        $(document).ready(function(){
                            $('#editperson').on('submit', function(e){
                                e.preventDefault();
                                $.ajax({
                                    type: 'POST',
                                    url: '/admin/order/modal/editperson',
                                    data: $('#editperson').serialize(),
                                    success:function(data){
                                        console.log(data);
                                        $(".saveperson").removeClass('i_hidden');
                                        setTimeout(function() { $(".saveperson").addClass('i_hidden'); }, 2000);
                                    }
                                });
                            });
                        });
                    </script>
                    <div class="company">
                        <div class="name">Компания<span class="savecompany i_hidden"> - сохранено</span></div>
                        <form id="editcompany" method="POST" >
                        {{ csrf_field() }}
                        @if($order->order_company != null)<input class="editcompany_act" type="hidden" name="act" value="1">
                        @else<input class="editcompany_act" type="hidden" name="act" value="2">
                        @endif
                        <input type="hidden" name="order_id" value="{{$order->id ?? ''}}">
                        <div class="data">
                            <div class="data-label">Название:</div>
                            <div class="data-block"><input type="text" name="title" value="{{$order->order_company->title ?? ''}}" placeholder="Название компании"></div>
                        </div>
                        <div class="data">
                            <div class="data-label">Email:</div>
                            <div class="data-block"><input type="text" name="email" value="{{$order->order_company->email ?? ''}}" placeholder="Адрес эл. почты"></div>
                        </div>
                        <div class="data">
                            <div class="data-label">Телефон:</div>
                            <div class="data-block"><input type="text" name="phone" value="{{$order->order_company->phone ?? ''}}" placeholder="+7 (___) ___-__-__"></div>
                        </div>
                        <div class="data">
                            <div class="data-label">Доп. телефон:</div>
                            <div class="data-block"><input type="text" name="phone2" value="{{$order->order_company->phone2 ?? ''}}" placeholder="+7 (___) ___-__-__"></div>
                        </div>
                        <div class="data">
                            <div class="data-label">Адрес:</div>
                            <div class="data-block"><input type="text" name="address" value="{{$order->order_company->address ?? ''}}" placeholder=""></div>
                        </div>
                        <div class="data">
                            <div class="data-label">Сайт:</div>
                            <div class="data-block"><input type="text" name="site" value="{{$order->order_company->site ?? ''}}" placeholder="https://"></div>
                        </div>
                        <div class="data" style="margin-top: 15px;"><button class="comment_buttom" type="submit">Сохранить</button></div>
                        </form>
                        <script>
                            $(document).ready(function(){
                                $('#editcompany').on('submit', function(e){
                                    e.preventDefault();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/admin/order/modal/editcompany',
                                        data: $('#editcompany').serialize(),
                                        success:function(data){
                                            console.log(data);
                                            $(".editcompany_act").val("1");
                                            $(".savecompany").removeClass('i_hidden');
                                            setTimeout(function() { $(".savecompany").addClass('i_hidden'); }, 2000);
                                        }
                                    });
                                });
                            });
                        </script>
                    </div>
                </div>
                <style>
                    .modal_content_block{width: 57%;
                        float: right;
                        border-left: 1px solid #ddd;
                        display: inline-block;}
                </style>
                <style>
                    .comment-box-input{width:100%;max-width:100%;min-width:100%;background-color:#fff;border:1px solid #ccc;font-size:14px;font-weight:400;border-radius:5px;color:#797979;padding-left:5px;height:32px;padding-top:5px;margin-top:2px}
                    .comment_buttom{position:relative;overflow:hidden;width:95px;height:28px;background:#249ffe;border:none;border-radius:3px;padding:4px;color:#fff;text-align:center}
                    .comment-box-act{margin-top:5px;display:flex}
                    .file-upload{position:relative;overflow:hidden;width:28px;height:28px;background:#eee;border-radius:3px;padding:4px;color:#fff;text-align:center;margin-left:5px}
                    .file-upload label{display:block;position:absolute;top:0;left:0;width:100%;height:100%;cursor:pointer;margin:0}
                    .file-upload label input{background-color:#fff;width:100%;height:36px;border:1px solid #ccc;font-size:14px;font-weight:400;border-radius:5px;color:#222;padding-left:5px}
                    .file-upload span{line-height:28px;font-weight:400;color:#666;font-size:20px}
                    .file-upload input[type="file"]{display:none}
                </style>
                <style>
                    .messages .item{padding:10px 0 10px 10px}
                    .modal-ui-wrapper .messages{width:100%}
                    .modal-ui-wrapper .messages .item .left{width:40px}
                    .modal-ui-wrapper .messages .block{margin:0}
                    .messages .left .image{height:40px;width:40px;background:#249ffe;border-radius:50%}
                    .messages .image_group{width:100%;display:table;margin:0 -5px}
                    .messages .image_group .image{float:left;width:100px;height:80px;padding:2px;border:1px solid #ddd;margin:5px}
                    .messages .image_group .image img{width:100px;height:80px;object-fit:cover}
                    .messages .item .date{text-align: left}
                </style>
                <div class="modal_content_block">
                    <div class="messages">
                        <div class="block">
                            <div class="item add_comment_box">
                                <div class="left">
                                    <div class="image">
                                        <img src="/admin/img/user.jpg" alt="">
                                    </div>
                                </div>
                                @if(Session::has('success'))
                                    {{ Session::get('success') }}
                                @endif
                                <div class="center">
                                    <div class="comment-box">
                                        <div class="alert alert-success" style="display:none"></div>
                                        <span id="form_result"></span>
                                        <form id="addcomment" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="user_id" value="{{Auth::id()}}">
                                            <input type="hidden" name="order_id" value="{{$order->id ?? ''}}">
                                            <textarea class="comment-box-input" name="description" placeholder="Напишите комментарий…" dir="auto" style="overflow: hidden; overflow-wrap: break-word;"></textarea>
                                            <div class="comment-box-act">
                                                <button class="comment_buttom" type="submit">Отправить</button>
                                            </div>
                                        </form>
                                    </div>
                                    <script>
                                        $(document).ready(function(){
                                            $('#addcomment').on('submit', function(e){
                                                e.preventDefault();
                                                $.ajax({
                                                    type: 'POST',
                                                    url: '/admin/order/modal/addcomment',
                                                    data: $('#addcomment').serialize(),
                                                    success:function(data){
                                                        console.log(data);
                                                        $('.add_comment_box').after(data);
                                                    }
                                                });
                                                $('#addcomment')[0].reset();
                                            });
                                        });
                                    </script>

                                </div>
                            </div>
                            @forelse($order->order_message as $m)
                                <div class="item">
                                    <div class="left"><div class="image"><img src="/admin/img/user.jpg" alt=""></div></div>
                                    <div class="center">
                                        <div class="name">@if($m->user != null){{$m->user->name ?? ''}} {{$m->user->lastname ?? ''}}@else Клиент @endif</div>
                                        <div class="mess">{!!  $m->description !!}</div>
                                        {{--<div class="image_group">
                                            <div class="image">
                                                <img src="https://uralpoliplastic.ru/storage/app/public/bp32HuPVNWAhfYFwyl7q1cP5E84R3Lft0STnRLCd.jpeg" alt="">
                                            </div>
                                            <div class="image">
                                                <img src="https://uralpoliplastic.ru/storage/app/public/bp32HuPVNWAhfYFwyl7q1cP5E84R3Lft0STnRLCd.jpeg" alt="">
                                            </div>
                                        </div>--}}
                                        <div class="date">{{Resize::Date($m->created_at)}}</div>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modal_close" class="modal-ui-close"><span>×</span></div>
    </div>
</div>

<script type="text/javascript">
    $('#modal_close, #overlay').click( function(){
        $('#modal_form')
            .animate({opacity: 0}, 100,
                function(){
                    $(this).css('display', 'none');
                    $('#overlay').fadeOut(100);
                }
            );
        history.replaceState({}, null, location.pathname);
    });
</script>
