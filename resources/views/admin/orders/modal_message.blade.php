<div class="item">
    <div class="left"><div class="image"><img src="/admin/img/user.jpg" alt=""></div></div>
    <div class="center">
        <div class="name">@if($message->user != null){{$message->user->name or ''}} {{$message->user->lastname or ''}}@else Клиент @endif</div>
        <div class="mess">{!!  $message->description !!}</div>
        <div class="date">{{Resize::Date($message->created_at)}}</div>
    </div>
</div>