<style>
    .orders .title{padding-bottom:20px;border-bottom:1px solid #ddd;margin-bottom:20px;display:table;width:100%}
    .orders .title .left{width:50%;float:left}
    .orders .title h2{font-size:16px;font-weight:600;margin:0}
    .orders .title h2 a{color: #666}
    .orders .title h2 span{}
    .orders .title .right{width:50%;float:left;font-size:14px;line-height:14px;color:#666;text-align:right}
    .orders .button-edit{cursor:pointer;z-index:3;width:102px;height:30px;padding:7px 14px;margin-left:25px;font-size:14px;background:#0095eb;color:#fff;border-radius:5px;text-align:center;vertical-align:-webkit-baseline-middle}
    .orders .title .right .button-edit a{color:#fff!important}
    .orders table{width:100%}
    .orders table input{width:100%}
    .orders table a{color:#0095eb}
    .orders table thead{border-bottom:1px solid #ddd}
    .orders table thead th{font-weight:600}
    .orders table tbody tr{border-bottom:1px solid #ddd}
    .orders table tbody tr:hover{background:rgba(233,235,238,0.5)}
    .orders .order-number{min-width:70px;width:5%;padding:10px;border-right:1px solid #ddd;vertical-align:top}
    .orders .order-goods{width:25%;min-width:200px;padding:10px;border-right:1px solid #ddd;vertical-align:top}
    .orders .order-goods .goods{display:table;margin-top:30px}
    .orders .order-goods .open{text-align:center;position:relative;margin-top:20px}
    .orders .order-goods .open:after{font-family:FontAwesome;color:#0095eb;content:"\f103";font-size:13px;display:block;padding-left:5px;font-weight:400}
    .orders .order-goods .left{width:20%;float:left}
    .orders .order-goods .right{width:80%;float:left;padding-left:15px}
    .orders .order-goods .name{margin-bottom:10px;font-weight:600;color:#333;overflow:hidden;max-height:40px}
    .orders .order-goods .price{color:#666}
    .orders .order-goods .price span{font-weight:600;color:#333}
    .orders .order-goods .price span:after{font-family:FontAwesome;color:#333;content:"\f158";font-size:13px;padding-left:5px;font-weight:400}
    .orders .order-user{width:10%;padding:10px;border-right:1px solid #ddd;vertical-align:top}
    .orders .order-user .name{padding-right:25px;margin-bottom:10px}
    .orders .order-user .address{margin-top:10px}
    .orders .order-full-price{vertical-align:top;width:10%;padding:10px;border-right:1px solid #ddd}
    .orders .order-full-price .full-price .sum{font-weight:600;margin-bottom:10px}
    .orders .order-full-price .full-price .sum:after{font-family:FontAwesome;color:#333;content:"\f158";font-size:13px;padding-left:5px;font-weight:400}
    .orders .order-full-price .status-pay{font-size:16px;font-weight:400;text-align:right;float:right}
    .clock:after{font-family:FontAwesome;content:"\f017";color:#e46a76}
    .percent:after{font-family:FontAwesome;content:"\f0d6";color:#f0ad4e}
    .check:after{font-family:FontAwesome;content:"\f00c";color:#00c292}
    .orders .order-status{width:10%;padding:10px;border-right:1px solid #ddd;vertical-align:top}
    .sort_input{border-radius:25px;padding:3px 10px;border:1px solid #ddd;background:#fff;color:#333;font-weight:400}
    .sort_input:focus{outline:0;outline-offset:0}
    .sort_select{border-radius:25px;padding:3px 10px;border:none;background:#e9ebee;color:#333;font-weight:400}
    .sort_select:focus{outline:0;outline-offset:0}
    .orders .select_status{display:inline;padding:3px 10px;border-radius:25px;border:none;font-size:12px}
    .orders .select_status option{background:#fff;color:#666}
    .orders .order-status .button-status{display:inline;padding:3px 10px;border-radius:25px;font-size:12px}
    .orders .order-status .danger{background:#e46a76;color:#fff}
    .orders .order-status .warning{background:#f0ad4e;color:#fff}
    .orders .order-status .success{background:#00c292;color:#fff}
    .orders .order-status .secondary{background:#aaa;color:#fff}
    .orders .order-delivery{width:10%;padding:10px;border-right:1px solid #ddd;vertical-align:top}
    .orders .order-action{vertical-align:top;padding:10px;width:5%;min-width:70px;position:relative}
    .orders .action{text-align:right}
    .orders .action .edit:before{font-family:FontAwesome;color:#666;content:"\f141"}

    .admin-content{padding-bottom:0!important}
    .c_p_company_order{height:80vh;display:flex;flex-direction:column;margin-right:0;width:100%;min-height:calc(100vh - 100px);position:relative;transition:margin .1s ease-in}
    #board{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;white-space:nowrap;margin-bottom:8px;overflow-x:auto;overflow-y:hidden;padding-bottom:8px;position:absolute;top:0;right:0;bottom:0;left:0;display:flex}
    #board::-webkit-scrollbar{width:5px;background:rgba(0,0,0,.10);border-radius:10px}
    #board::-webkit-scrollbar-thumb{border-radius:10px;background:rgba(0,0,0,.15)}
    .order_board .order_list{position:relative;min-width:310px;width:310px;margin:0 7px;height:95%;box-sizing:border-box;display:inline-block;vertical-align:top}
    .order_board .order_list_header{height:46px;display:table;width:100%;padding:0 7px}
    .order_board .order_list_info{display:flex;background:#f1f3f4;padding:7px 17px;margin-bottom:7px;border-radius:3px 3px 0 0;border-bottom:2px solid #0095eb}
    .order_board .order_list_info .left{display:inline-block;width:100%}
    .order_board .order_list_info .right{display:inline-block}
    .order_board .order_list_info .right .edit:before{font-family:FontAwesome;color:#666;content:"\f141"}
    .order_board .order_list_box{overflow:overlay;height:inherit;flex:1 1 auto;margin:0 -3px;padding:0 3px;z-index:1;min-height:0}
    .order_board .order_list_box::-webkit-scrollbar{width:5px;background:rgba(9,30,66,.08);border-radius:10px}
    .order_board .order_list_box::-webkit-scrollbar-thumb{border-radius:10px;-webkit-box-shadow:inset 0 0 6px rgba(0,0,0,.3);background:rgba(9,30,66,.08)}
    .order_board .list_content{background-color:#ebecf0;border-radius:3px;box-sizing:border-box;display:flex;flex-direction:column;max-height:100%;position:relative;white-space:normal}
    .order_board .order_list .title{font-size:15px;font-weight:600}
    .order_board .order_list .title input{background: none;
    border: none;
    width: 95%;}
    .order_board .order_box{min-width:296px;margin:0 7px 7px;padding:5px 10px;background-color:#fff;border-radius:3px;border:1px solid #e6e6e6;white-space:normal}
    .order_board .order_box:last-child{margin-bottom:0}
    .order_board .order_box_header{width:100%;display:flex;font-size:13px}
    .order_board .name{display:inline-block;width:100%;font-weight:600}
    .order_board .name span{font-weight:400;color:#888}
    .order_board .date{display:inline-block;color:#888;margin-left:10px;min-width:100px;text-align:right}
    .order_board .order_box a{color:#0095eb}
    .order_board .order_goods{font-weight:600;margin:3px 0;font-size:14px;line-height:16px;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;color:#0095eb;cursor:pointer}
    .order_board .order_goods:hover{text-decoration:underline}
    .order_board .order_box_bottom{display:flex}
    .order_board .order_price{font-size:13px;width:100%;color:#666;display:inline-block;margin-right:5px}
    .order_board .order_tag span{border:1px solid #bbb;padding:2px 3px;border-radius:4px;color:#666;font-size:12px;line-height:11px;margin-right:2px;display:inline-block}
    .order_board .time{display:inline-block;white-space:nowrap;margin-left:10px;font-size:13px;color:#666}
    .order_board .time:after{font-family:FontAwesome;content:"\f111";color:#bbb;margin-left:4px;font-size:12px}
    .order_board .time.today:after{color:#e46a76}
    .order_board .time.tom:after{color:#fec107}
    .order_board .time.ok:after{color:#5cb85c}
    .order_board .u-fancy-scrollbar{-webkit-overflow-scrolling:touch;-webkit-transform:translateZ(0)}
    .order_board .time{display:none}
    .order_board .order_box.updated .time{display:block}
    .order_board .create-order{font-size:16px;text-align:center;color:#666;line-height:44px;cursor:pointer}
    .order_board .admin-content .dropdown-item-edit{width:auto}

    .order_board .order_list_add{line-height: 55px;font-size: 15px;color: #666;cursor: pointer;}
    .order_board .order_list_create{height:57px;display:table;width:100%;text-align:center;border:1px dashed #9e9e9e;border-radius:5px}
    .order_board .create_column_name{height:55px;border-bottom:1px solid #bbb;width:90%;margin:0 auto}
    .order_board .create_column_name input{background:none;border:none;font-size:15px;width:90%;text-align:center;line-height:55px;height:55px;outline:none}
    .order_board .create_column_button{height:46px}
    .order_board .create_column_button input{border:none;background:#5baa00;color:#fff;width:146px;height:28px;border-radius:3px;padding:4px;margin:10px 0}
    .order_board .create_column{display: none}
</style>

<div class="c_p_company_order" style="margin-top: 30px;">
    <div id="board" class="order_board u-fancy-scrollbar">
        @php($i = 0)
        @forelse($column as $с)
            <div class="order_list @if($i == 0)first_column @endif" data-column="{{$с->id}}" data-position="{{$с->position ?? ''}}">
                <div class="order_list_header">
                    <div class="order_list_info">
                        <div class="left">
                            <div class="title"><input type="text" name="column_title" autocomplete="off" value="{{$с->title}}" class="input_column_title"></div>
                            <div class="qty_order">{{$с->orders->count()}} сделок: {{$с->orders->sum('total_price')}} руб</div>
                        </div>
                        <div class="right">
                            <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                            <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                <li><a href="#" class="delete_column">Архивировать список</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="order_list_box sortable-ui">
                    @if($i == 0)
                        <div class="order_box nosort" style="background-color:#ffffff00;border:1px dashed #9e9e9e;height:46px;padding:0">
                            <div class="create-order">Добавить заказ</div>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $('.create-order').one('click', function(){
                                        $.post('/admin/order/create/fast', {column_id: '{{$с->id}}', create: 1, _token: '{!! csrf_token() !!}'}, function(data){
                                            $('.modal_orders').html(data);
                                        });
                                    });
                                });
                            </script>
                        </div>
                    @endif
                    @forelse($с->orders as $o)
                        <div class="order_box" data-index="{{$o->id}}" data-position="{{$o->position}}" data-column-item="{{$с->id}}">
                            <div class="order_box_header">
                                <div class="name">{{$o->order_customer->name ?? 'Узнать имя!'}}{{--, <span>Золушка</span>--}}</div>
                                <div class="date">{{Resize::noTimeDate($o->created_at)}}</div>
                            </div>
                            <div class="order_goods open_modal" data-order-id="{{$o->id}}">
                                @if($o->title != null){{$o->title ?? ''}}@else{{$o->form ?? 'Новый заказ'}}@endif
                            </div>
                            <div class="order_box_bottom ">
                                <div class="order_price order_tag">{{$o->total_price ?? '0'}} руб {{--<span>Сайт</span><span>CRM</span>--}}</div>
                                {{--<div class="time"></div>--}}
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
            @php($i++)
        @empty
        @endforelse
        <script type="text/javascript">
            $(document).ready(function(){
                $("input[name='column_title']").blur(function(){
                    var column_title = $(this).val();
                    var column_id = $(this).closest('.order_list').data('column');
                    $.post('/admin/order/column/edittitle', {column_id: column_id, title: column_title, _token: '{!! csrf_token() !!}'}, function(data){
                       console.log(data);
                    });
                });

                $('.delete_column').one('click', function(e){
                    e.preventDefault();
                    var id = $(this).closest('.order_list').data('column');
                    $(this).closest('.order_list').hide();
                    console.log(id);
                    $.post('/admin/order/column/delete', {column_id: id, _token: '{!! csrf_token() !!}'}, function(data){
                        console.log(data);
                    });

                });
            });
        </script>
        <div class="order_list start_create">
            <div class="order_list_create">
                <div class="order_list_add">
                    Добавить еще одну колонку
                </div>
            </div>
        </div>
        <div class="order_list create_column">
            <div class="order_list_create">
                <form id="create_column" method="POST">
                    {{ csrf_field() }}
                    <div class="create_column_name"><input type="text" name="name_column" value="" placeholder="Введите название колонки"></div>
                    <div class="create_column_button"><input type="submit" value="Добавить колонку" class="add_column"></div>
                </form>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('.order_list_add').click(function() {
                    $('.create_column').show();
                    $('.start_create').hide();
                });
                $('.add_column').click(function() {
                    $('.start_create').show();
                    $('.create_column').hide();
                });
                $('#create_column').on('submit', function(e){
                    e.preventDefault();
                    $.ajax({
                        type: 'POST',
                        url: '/admin/order/column/create',
                        data: $('#create_column').serialize(),
                        success:function(data){
                            $(".start_create").before(data);
                            $('.sortable-ui').sortable({
                                items: ".order_box:not(.nosort)",
                                connectWith: '.sortable-ui'
                            }).disableSelection();
                        }
                    });
                    $('#create_column')[0].reset();
                });
            });
        </script>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#board').sortable({
            axis: 'x',
            items: ".order_list:not(.start_create, .first_column)",
            update: function(event, ui) {
                $(this).children().each(function (index){
                    if ($(this).attr('data-position') != (index+1)) {
                        $(this).attr('data-position', (index+1)).addClass('column_updated');
                    }
                });
                saveNewColumnPositions();
            }
        }).disableSelection();

        function saveNewColumnPositions() {
            var positions = [];
            $('.column_updated').each(function () {
                positions.push([$(this).attr('data-column'), $(this).attr('data-position')]);
                $(this).removeClass('column_updated');
            });
            if (positions.length == 0) {console.log('Массив positions пустой');}
            else {
                $.ajax({
                    url: '{{route("order.column.position")}}',
                    method: 'POST',
                    dataType: 'text',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        update: 1,
                        positions: positions
                    },
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
        }

        $('.sortable-ui').sortable({
            items: ".order_box:not(.nosort)",
            connectWith: '.sortable-ui',
            update: async function(event, ui) {
                await (function() {
                    var id = ui.item.closest('.order_list').data('column');
                    ui.item.attr('data-column-item', id).addClass('updated');
                })();
                await $(this).children().each(function (index){
                    if ($(this).attr('data-position') != (index+1)) {
                        $(this).attr('data-position', (index+1)).addClass('updated'); // подставляю класс updated если изменилась позиция
                    }
                });
                saveNewPositions();
            },

        }).disableSelection();
        function saveNewPositions() {
            var positions = [];
            $('.updated').each(function () {
                positions.push([$(this).attr('data-index'), $(this).attr('data-column-item'), $(this).attr('data-position')]);
                $(this).removeClass('updated');
            });
            if (positions.length == 0) {console.log('Массив positions пустой');}
            else {
                $.ajax({
                    url: '{{route("order.position")}}',
                    method: 'POST',
                    dataType: 'text',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        update: 1,
                        positions: positions
                    },
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
        }
    });
</script>

<script>
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };
    var order = getUrlParameter('order');
    if (order > 0) {
        var id = order;
        $.get('/admin/orders/modal/'+id, function(data){
            $('.modal_orders').html(data);
        });
    }
</script>
<script type="text/javascript">
    $('.open_modal').click(function(){
        var id = $(this).data('order-id');
        $.get('/admin/orders/modal/'+id, function(data){
            $('.modal_orders').html(data);
        });
        var url = '?order=' + id;
        history.pushState(null, null, url);
    });
</script>
<div class="modal_orders"></div>
<script>
$(".spoiler-trigger").click(function() {
    $(this).parent().next().collapse('toggle');
});
</script>
</body>
</html>
