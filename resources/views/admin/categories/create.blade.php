
      <div class="row">
          <div class="col-lg-4">
              <h2>Управление категориями</h2>
          </div>
          <div class="col-lg-8">
              <ol class="breadcrumb">
                  <li><a href="">Главная</a></li>
                  <li class="active">Категории</li>
              </ol>
          </div>
      </div>
      <hr>
      <?php //конфликт ?>
        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                      {{--<div class="item">
                          <div class="name">
                              <a href=""></a>
                          </div>
                      </div>--}}
                  </div>
              </div>
          </div>
            <div class="narrow">
                <div class="box">
                    <div class="cp_edit">
                      @if($category !== null)
                      <form class="form-horizontal" action="{{route('admin.category.update', $category)}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" name="category" value="{{$category->id}}">
                    @else
                      <form action="{{route('admin.category.store')}}" method="post" enctype="multipart/form-data">
                        @endif
                        {{ csrf_field() }}
                        <div class="narrow_title">
                                <h2>Редактирование категории: <span id="edit-title">{{$category->title or ''}}</span></h2>
                        </div>
                          <div class="perent">
                              <div class="left">
                                  <label for="" class="item_name">Добавить:</label>
                              </div>
                              <style>
                                  .cp_edit .button-box{display:table}
                                  .cp_edit .button-box .c_action{padding:10px;height:42px;border:1px solid #269ffd;margin-right:15px}
                                  .cp_edit .button-box .c_action.back{background-color:#cee9ff}
                                  .dependent{display:none;position:relative;background:#8F8F8F;margin:20px;color:#fff;padding:7px;text-align:center}
                                  .white_back .perent:after{position: absolute;top: 0;left: 0;bottom: 0;right: 0;content: '';    background-color: rgba(255,255,255,.8);}
                              </style>

                              <div class="center button-box type1">
                                  <label class="c_action @if($category !== null && $category->parent_id == null) back @endif" ><input type="radio"  id="idsection" name="type" value="0" hidden="" @if($category !== null && $category->parent_id == null) checked @endif required=""><span></span><span class="radio-name">Новую категорию</span></label>
                                  <label class="c_action @if($category !== null && $category->parent_id !== null) back @endif"><input type="radio" id="cat" name="type" value="1" hidden="" required="" @if($category !== null && $category->parent_id !== null) checked @endif><span></span><span class="radio-name">В уже имеющуюся</span></label>
                              </div>
                              <script type="text/javascript">
                              $(document).ready(function(){
                                $('#idsection').click(function(){
                                  $('input[name="section2"]').prop('required', true);
                                });

                                $('#cat').click(function(){
                                  $('input[name="section2"]').prop('required', false);
                                });
                              });
                              </script>
                              <script>
                                  $(document).ready(function() {
                                      $('input[type="radio"]').click(function() {
                                          if($(this).attr('id') == 'cat') {$('#select_cat_block').show();}
                                          else {$('#select_cat_block').hide();}
                                      });
                                  });
                                  $(document).ready(function() {
                                      $('input[type="radio"]').click(function() {
                                          if($(this).attr('id') == 'cat') {$('#id-white_back').addClass("white_back");}
                                          else {$('#id-white_back').removeClass("white_back");}
                                      });
                                  });
                                  $(document).ready(function () {
                                      $('input[type="radio"]').click(function () {
                                          $('input:not(:checked)').parent().removeClass("back");
                                          $('input:checked').parent().addClass("back");
                                      });
                                  });
                              </script>
                          </div>

                          <script>
                              $(document).ready(function() {
                                  $('.type1 input[type="radio"]').click(function() {
                                      if($(this).attr('id') == 'idsection') {$('#select_section_block').show();}
                                      else {$('#select_section_block').hide();}
                                  });
                              });
                          </script>
                          <div id="select_section_block" class="perent select_section_block" style="display: @if($category !== null && $category->parent_id == null) block @else none @endif">
                              <div class="left">
                                  <label for="" class="item_name">Выберите раздел:</label>
                              </div>
                              <div class="center button-box type2">
                                  <label class="c_action @if($category !== null && $category->type == 0) back @endif"><input type="radio" name="section2" value="0" hidden="" required="" @if($category !== null && $category->type == 0) checked @endif><span></span><span class="radio-name">Товары</span></label>
                                  <label class="c_action @if($category !== null && $category->type == 1) back @endif"><input type="radio" name="section2" value="1" hidden="" required="" @if($category !== null && $category->type == 1) checked @endif><span></span><span class="radio-name">Услуги</span></label>
                                  <!-- <label class="c_action @if($category !== null && $category->type == 3) back @endif"><input type="radio" name="section2" value="3" hidden="" required="" @if($category !== null && $category->type == 3) checked @endif><span></span><span class="radio-name">Материалы</span></label> -->
                                  @if($type == 1)
                                  <label class="c_action @if($category !== null && $category->type == 4) back @endif"><input type="radio" name="section2" value="4" hidden="" required="" @if($category !== null && $category->type == 4) checked @endif><span></span><span class="radio-name">Строительство</span></label>
                                  <label class="c_action @if($category !== null && $category->type == 5) back @endif"><input type="radio" name="section2" value="5" hidden="" required="" @if($category !== null && $category->type == 5) checked @endif><span></span><span class="radio-name">Бани</span></label>
                                  <label class="c_action @if($category !== null && $category->type == 6) back @endif"><input type="radio" name="section2" value="6" hidden="" required="" @if($category !== null && $category->type == 6) checked @endif><span></span><span class="radio-name">Проекты</span></label>
                                  @elseif($type ==3)
                                  <label class="c_action @if($category !== null && $category->type == 7) back @endif"><input type="radio" name="section2" value="7" hidden="" required="" @if($category !== null && $category->type == 7) checked @endif><span></span><span class="radio-name">Изготовление</span></label>
                                  @endif
                              </div>
                          </div>
                          <script type="text/javascript">
                            $('input[name="section2"]').click(function(){
                              var val = $(this).val();
                              $.get('/admin/category/type/{{$category->id}}/'+val, function(data){});
                            });
                          </script>
                          <?php //dd($category); ?>
                          @if($categories !== null)
                          <div id="select_cat_block" class="perent select_cat_block" style="display: @if($category !== null && $category->parent_id !== null) block @else none @endif">
                              <div class="left">
                                  <label for="" class="item_name" style="">Выберите категорию:</label>
                              </div>
                              <div class="center">
                                  <select id="select_cat" name="subcategory" class="form-control" required="">
                                      <option value="0" selected  disabled>Выберите категорию</option>
                                      @forelse($categories as $cat)
                                          <option value="{{$cat->id or ''}}" @if($category !== null && $category->id == $cat->id) disabled @endif @if($category !== null && $cat->id == $category->parent_id) selected="" @endif>{{$cat->title or ''}}</option>
                                      @empty
                                      @endforelse
                                      <option value="1">Название категории</option>
                                      <option value="2">Название длинной категории</option>
                                  </select>
                              </div>
                              <div class="right">

                              </div>
                          </div>
                          <script type="text/javascript">
                            $('#select_cat').on('change', function(){
                              var val = $(this).val();
                              $.get('/admin/category/subcategory/{{$category->id}}/'+val, function(data){});
                            });
                          </script>
                          @endif


                          <script>
                              $(document).ready(function(){
                                  $('#select_cat').on('change', function() {
                                      if ( this.value > '0') {$("#id-white_back").removeClass("white_back");}
                                      else {$("#id-white_back").addClass("white_back");}
                                  });
                              });
                          </script>
                        {{--<div class="perent">
                            <div class="left">
                                <label for="" class="name_item">Выберите раздел:</label>
                            </div>
                            <div class="center button-box">
                              @if($category !== null)
                                <label><input type="radio" name="type" value="0" class="choose_section" @if($category->type == 0) checked @endif hidden required><span></span><span class="radio-name">Раздел "Товары"</span></label>
                                <label><input type="radio" name="type" value="1" class="choose_section" @if($category->type==1) checked @endif hidden required><span></span><span class="radio-name">Раздел "Услуги"</span></label>
                                @else
                                <label><input type="radio" name="type" value="0" class="choose_section"  hidden required><span></span><span class="radio-name">Раздел "Товары"</span></label>
                                <label><input type="radio" name="type" value="1" class="choose_section" hidden required><span></span><span class="radio-name">Раздел "Услуги"</span></label>
                                @endif
                            </div>
                        </div>--}}
                        <?php //dd($category); ?>
                        <?php //dd($categories); ?>
                      <div id="id-white_back" @if($category == null)class="white_back" @endif>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Название категории:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="title" class="" placeholder="Введите название категории" value="{{$category->title or ''}}" required="">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>

                        <script type="text/javascript">
                        $('input[name="title"]').on('input',function(){
                          var title = $(this).val();
                          $('#edit-title').text(title);
                        });
                        $('input[name="title"]')
                        .on({
                          blur: function() {
                            $.get('/admin/category/title/{{$category->id}}/'+this.value);
                          }
                        });
                        </script>
                          <div class="perent">
                              <div class="left">
                                  <label for="" class="item_name">Стоимость от:</label>
                              </div>
                              <div class="center">
                                  <input placeholder="0" type="text" name="price" data-mask="000 000 000" data-mask-reverse="true" data-mask-maxlength="false" value="{{$category->price or ''}}"/>
                              </div>
                              <div class="right">
                                  <div class="help"></div>
                              </div>
                          </div>
                          <script type="text/javascript">
                              $('input[name="price"]')
                                  .on({
                                      blur: function() {
                                          $.get('/admin/category/price/{{$category->id}}/'+this.value);
                                      }
                                  });
                          </script>
                          <div class="perent">
                              <div class="left">
                                  <label for="" class="seo_name">SEO</label>
                              </div>
                              <div class="center">
                                  <label class="switch">
                                      <input type="checkbox" id="watch" name="checkseo" value="1" @if($category !== null) @if($category->checkseo == 1)checked @endif @endif>
                                      <span class="slider round"></span>
                                  </label>
                              </div>
                          </div>
                          <div class="seo_group" id="switch_seo" style="display:none">
                              <div class="item_line">
                                  <div class="left">
                                      <label for="" class="item_name" style="padding-top: 4px">Название на странице категории:</label>
                                  </div>
                                  <div class="center">
                                      <input type="text" name="seoname" class="" placeholder="Название на странице категории" value="{{$category->seoname or ''}}">
                                  </div>
                                  <div class="right">
                                      <div class="help"></div>
                                  </div>
                              </div>
                              <script type="text/javascript">
                              $('input[name="seoname"]')
                              .on({
                                blur: function() {
                                  $.get('/admin/category/seoname/{{$category->id}}/'+this.value);
                                }
                              });
                              </script>
                              <div class="item_line">
                                  <div class="left">
                                      <label for="" class="item_name">Мета-тег Title:</label>
                                  </div>
                                  <div class="center">
                                      <input type="text" name="seotitle" class="" placeholder="" value="{{$category->seotitle or ''}}">
                                  </div>
                                  <div class="right">
                                      <div class="help"></div>
                                  </div>
                              </div>
                              <script type="text/javascript">
                              $('input[name="seotitle"]')
                              .on({
                                blur: function() {
                                  $.get('/admin/category/seotitle/{{$category->id}}/'+this.value);
                                }
                              });
                              </script>
                              <div class="item_line">
                                  <div class="left">
                                      <label for="" class="item_name">Мета-тег Description:</label>
                                  </div>
                                  <div class="center">
                                      <input type="text" name="seodescription" class="" placeholder="" value="{{$category->seodescription or ''}}">
                                  </div>
                                  <div class="right">
                                      <div class="help"></div>
                                  </div>
                              </div>
                              <script type="text/javascript">
                              $('input[name="seodescription"]')
                              .on({
                                blur: function() {
                                  $.get('/admin/category/seodescription/{{$category->id}}/'+this.value);
                                }
                              });
                              </script>
                              <div class="item_line">
                                  <div class="left">
                                      <label for="" class="item_name">Мета-тег Keywords:</label>
                                  </div>
                                  <div class="center">
                                      <input type="text" name="seokeywords" class="" placeholder="" value="{{$category->seokeywords or ''}}">
                                  </div>
                                  <div class="right">
                                      <div class="help"></div>
                                  </div>
                              </div>
                              <script type="text/javascript">
                              $('input[name="seokeywords"]')
                              .on({
                                blur: function() {
                                  $.get('/admin/category/seokeywords/{{$category->id}}/'+this.value);
                                }
                              });
                              </script>
                          </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Оффер категории:</label>
                            </div>
                            <div class="center">
                                <textarea  name="offer" id="offer" class="" placeholder="Введите оффер категории" >{{$category->offer or ''}}</textarea>
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <script>
	                        var myOffer;
                            ClassicEditor
                            .create( document.querySelector( '#offer' ) )
                            .then( offer => {myOffer = offer;})
                            .catch( error => {});
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Описание категории:</label>
                            </div>
                            <div class="center">
                                <div rows="30" name="text" id="text" >{!! $category->description or '' !!}</div>
                            </div>
                        </div>
                        <script>
	                    var myEditor;
                            ClassicEditor
                                .create( document.querySelector( '#text' ) )
                                .then( editor => {
                                myEditor = editor;
                                    } )
                                .catch( err => {
                                } );
                        </script>
                                <div class="perent">
                                    <div class="left">
                                        <label for="" class="item_name" style="line-height: 14px;">Обложка:</label>
                                    </div>
                                    <div class="center">
                                      <div class="file-upload">
                                          <label>
                                              <input type="file" name="cover" id="fileCover" />
                                              <span>Выбрать файл</span>
                                          </label>
                                      </div>
                                      @if($category->cover !== null)
                                      <div class="image_group" id="cover">
                                          <div class="image">
                                              <img src="{{asset('storage/app')}}/{{$category->cover or ''}}" alt="">
                                          </div>
                                      </div>
                                      @endif
                                      <script type="text/javascript">
                                      $('#fileCover').on('change',function() {
                                        $('#cover').find('.image').remove();
                                        //alert('asd');
                                        var i = 1;
                                        var files = $('#fileCover').prop('files');
                                        $.each(files, function (index, value) {
                                          var file_data = value;
                                          var form_data = new FormData();
                                          form_data.append('file', file_data);
                                          form_data.append('id', {{$category->id}});
                                          $.ajax({
                                                      url: '/admin/image/category/cover/upload',
                                                      dataType: 'text',
                                                      cache: false,
                                                      contentType: false,
                                                      processData: false,
                                                      data: form_data,
                                                      type: 'post',
                                                      success: function(data){
                                                        data = $.parseJSON(data);
                                                          $('#cover').append('<div class="image"><img src="{{asset('storage/app')}}/'+data.image+'" alt=""></div>');

                                                      }
                                           });
                                           i++;
                                        });

                                      });

                                      </script>
                                    </div>
                                </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Фотография:</label>
                            </div>
                            <div class="center">
                                <div class="file-upload">
                                    <label>
                                        <input id="sortpicture" type="file" name="sortpic" />
                                        <span>Выбрать файл</span>
                                    </label>
                                </div>
                                <input type="text" id="filename" class="filename" disabled style="border: none;font-size: 12px;">
                                <div class="imgs">
                                @if($category !== null)
                                <div id="list">
                                <div id="response"> </div>
                                <div id="sort">
                                @forelse($category->imageses as $image)
                                <div class="image_group" id="arrayorder_{{$image->id}}" style="width:auto;float:left;">
                                    <div class="image">
                                        <img src="{{asset('storage/app')}}/{{$image->images or ''}}" alt="">
                                        <a  class="delete" id="send{{$image->id}}"></a>
                                    </div>
                                </div>

                                <script>

                                ajaxForm();
                                function ajaxForm(){
                                  $('#send{{$image->id}}').click(function(){
                                    var data = new FormData($("#form")[0]);
                                      $.ajax({
                                          type: 'GET',
                                          url: "{{route('admin.image.category.destroy')}}?id={{$image->id}}",
                                          data: data,
                                          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                          contentType: false,
                                          processData : false,
                                          success: function(data) {
                                              $('#arrayorder_{{$image->id}}').html('');
                                          }
                                      });
                                      return false;
                                  });
                                }

                        </script>
                                @empty
                                @endforelse
                              </div>
                            </div>
                                @endif
                              </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('#sortpicture').on('change',function() {
                          //alert('asd');
                          var i = 1;
                          var files = $('#sortpicture').prop('files');
                          $.each(files, function (index, value) {
                            var file_data = value;
                            var form_data = new FormData();
                            form_data.append('file', file_data);
                            form_data.append('id', {{$category->articul}});
                            $.ajax({
                                        url: '/admin/image/category/upload',
                                        dataType: 'text',
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'post',
                                        success: function(data){
                                          data = $.parseJSON(data);
                                            $('.imgs').append('<div id="list"><div id="response"> </div><div id="sort"><div class="image_group" id="arrayorder_'+i+'" style="width:auto;float:left;">              <div class="image">                                                    <img src="{{asset('storage/app')}}/'+data.image+'" alt="">                                                    <a  class="delete" id="send'+i+'"></a>                                             </div>                                            </div></div></div>');
                                            $('#send'+i).click(function(){

                                              $.get('{{route('admin.image.goods.destroy')}}?id='+data.id,function(data){$('#arrayorder_'+i).remove();});
                                            });
                                        }
                             });
                             i++;
                          });

                        });

                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Видео с YouTube:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="youtube" class="" placeholder="Например: http://www.youtube.com/watch?v=9X-8JiOhzX4" value="{{$category->youtube or ''}}">

                                <div class="youtube">
                                  @if($category !== null && $category->youtube !== null)
                                <?php $iframe = ''; ?>
                                <iframe width="560" height="315" src="{{$category->youtube or ''}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="margin-top: 15px;"></iframe>
                                @endif
                                </div>

                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('input[name="youtube"]')
                        .on({
                          blur: function() {
                            $('.youtube').html('<iframe width="560" height="315" src="'+this.value+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="margin-top: 15px;"></iframe>');
                            $.get('/admin/category/youtube/{{$category->id}}/youtube?url='+this.value.substr(8));
                          }
                        });
                        </script>
                        <div class="perent" style="margin-top: 40px;">
                            <div class="left">&nbsp</div>
                            <div class="center">
                              <input type="button" class="item_button" value="Сохранить и вернуться">
                            </div>
                            <div class="right"></div>
                        </div>
                      </div>
                      </form>
                      <script type="text/javascript">
                      $('.item_button').click(function(){
                        var text = myEditor.getData();
                        var offer = myOffer.getData();
                        $.post('/admin/category/update/description', {description:text, offer:offer, id:{{$category->id}}}, function(data){
                          history.pushState(null, null, '/admin/category/');
                          $('.admin-content').html(data);
                        });
                      });
                      </script>
                    </div>
                </div>
                <style>
                                        .mini_box{
                                            width: 49%;
                                            padding: 20px;
                                            background: #fff;
                                            float: left;
                                            margin-bottom: 20px;
                                            box-shadow: 0 1px 15px rgba(0,0,0,.04), 0 1px 6px rgba(0,0,0,.04);
                                        }
                                        .mini_box:nth-child(odd){
                                            margin-right: 2%;
                                        }
                                        .mini_cp_edit .perent .left {
                                            width: 30%;
                                        }
                                        .mini_cp_edit .center {
                                            width: 70%;
                                        }
                                    </style>



            </div>
        </div>
        <script type="text/javascript">
        $('#refresh').click(function(){
          $.get('{{route('admin.goods.index')}}', function(data){
            $('.admin-content').html(data);
          });
        });
        $('.item_button').click(function(){
          alert('asd');
          //var text = CKEDITOR.instances['texts'].getData();
          //$.post('admin/category/update/description', {description:text, id:{{$category->id}}}, function(data){
          //  $('.admin-content').html(data);
          });
        });
        </script>
        <script>
            $(document).ready(function () {
                $(".file-upload input[type=file]").change(function () {
                    var filename = $(this).val().replace(/.*\\/, "");
                    $("#filename").val(filename);
                });
            });
        </script>
        <script>
            $(document).ready(function() {
                $('input[type="checkbox"]').click(function() {
                    if($(this).attr('id') == 'watch') {
                      var val = 0;
                        if (this.checked) {$('#switch_seo').show(); val = 1;}
                        else {$('#switch_seo').hide(); val = 0;}
                        $.get('/admin/category/checkseo/{{$category->id}}/'+val);
                    }
                });
            });
        </script>
        @if($category !== null)
            @if($category->checkseo == 1)
                <script>
                    $(document).ready(function () {
                        $('#switch_seo').show();
                    });
                </script>
            @endif
        @endif
        <script type="text/javascript">
        $(document).ready(function(){
        $(function() {
        $("#list #sort").sortable({ opacity: 0.8, cursor: 'move', update: function() {

        var order = $(this).sortable("serialize") + '&update=update';


        $.get("{{route('admin.images.sort')}}", order, function(theResponse){
        $("#response").html(theResponse);
        $("#response").slideDown('slow');
        slideout();
        });
        }
        });
        });
        });

        </script>
      <script type="text/javascript" src="/public/admin/js/jquery.mask.js"></script>
