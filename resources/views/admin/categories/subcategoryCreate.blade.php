@include('admin.admin.include.mainHeader')
<div class="admin">
@include('admin.admin.include.header')
@include('admin.admin.include.lmenu')

    <div class="admin-content">
      <div class="row">
          <div class="col-lg-4">
              <h2>Управление категориями</h2>
          </div>
          <div class="col-lg-8">
              <ol class="breadcrumb">
                  <li><a href="">Главная</a></li>
                  <li class="active">Категории</li>
              </ol>
          </div>
      </div>
      <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                      {{--<div class="item">
                          <div class="name">
                              <a href=""></a>
                          </div>
                      </div>--}}
                  </div>
              </div>
          </div>
            <div class="narrow">
                <div class="box">
                    <div class="cp_edit">
                      @if($subcategory !== null)
                      <?php //dd($subcategory); ?>
                      <form class="form-horizontal" action="{{route('admin.subcategory.update', $subcategory->id)}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" name="subcategory" value="{{$subcategory->id}}">
                    @else
                      <form action="{{route('admin.subcategory.store')}}" method="post" enctype="multipart/form-data">
                        @endif
                        {{ csrf_field() }}
                        <div class="narrow_title">
                            @if($subcategory == null)
                                <h2>Добавление категории</h2>
                            @else
                                <h2>Редактирование категории: {{$subcategory->title or ''}}</h2>
                            @endif
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="name_item">Выберите категорию:</label>
                            </div>
                            <div class="center">
                              <select class="form-control" name="subcategory" required>
                                  @forelse($categories as $c)
                                  @if($subcategory !== null && count($subcategory->categor)>0)
                                  <option value="{{$c->id}}" @if($c->id == $subcategory->categor->id)selected="" @endif>{{$c->title}}</option>
                                  @else
                                  <option value="{{$c->id}}">{{$c->title}}</option>
                                  @endif
                                  @empty

                                  @endforelse
                              </select>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="seo_name">SEO</label>
                            </div>
                            <div class="center">
                                <label class="switch">
                                    <input type="checkbox" id="watch" name="checkseo" value="1" @if($subcategory !== null) @if($subcategory->checkseo == 1)checked @endif @endif>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="seo_group" id="switch_seo" style="display:none">
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name" style="padding-top: 4px">Название на странице категории:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seoname" class="" placeholder="Название на странице категории" value="{{$subcategory->seoname or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Title:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seotitle" class="" placeholder="" value="{{$subcategory->seotitle or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <div class="item_line">
                              <div class="left">
                                  <label for="" class="item_name">Мета-тег Description:</label>
                              </div>
                                <div class="center">
                                    <input type="text" name="seodescription" class="" placeholder="" value="{{$subcategory->seodescription or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Keywords:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seokeywords" class="" placeholder="" value="{{$subcategory->seokeywords or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Название категории:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="title" class="" placeholder="Введите название категории" value="{{$subcategory->title or ''}}" required="">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Оффер категории:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="offer" class="" placeholder="Введите оффер категории" value="{{$subcategory->offer or ''}}" required="">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Описание категории:</label>
                            </div>
                            <div class="center">
                                <textarea rows="10" name="text">{{$subcategory->description or ''}}</textarea>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Фотография:</label>
                            </div>
                            <div class="center">
                                <div class="file-upload">
                                    <label>
                                        <input type="file" name="file">
                                        <span>Выбрать файл</span>
                                    </label>
                                </div>
                                <input type="text" id="filename" class="filename" disabled style="border: none;font-size: 12px;">
                                @if($subcategory !== null)
                                @if($subcategory->images !== null)
                                <div class="image_group" id="image{{$subcategory->id}}">
                                    <div class="image">
                                        <img src="{{asset('storage/app')}}/{{$subcategory->images->images or ''}}" alt="">
                                        <a  class="delete" id="send"></a>
                                    </div>
                                </div>

                                <script>

                                ajaxForm();
                                function ajaxForm(){
                                  $('#send').click(function(){
                                    var data = new FormData($("#form")[0]);
                                      $.ajax({
                                          type: 'GET',
                                          url: "{{route('admin.image.subcategory.destroy')}}?id={{$subcategory->images->id}}",
                                          data: data,
                                          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                          contentType: false,
                                          processData : false,
                                          success: function(data) {
                                              $('#image{{$subcategory->id}}').html('');
                                          }
                                      });
                                      return false;
                                  });
                                }

                        </script>
                                @endif
                                @endif
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Видео с YouTube:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="youtube" class="" placeholder="Например: http://www.youtube.com/watch?v=9X-8JiOhzX4" value="{{$subcategory->youtube or ''}}">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <div class="perent" style="margin-top: 40px;">
                            <div class="left">&nbsp</div>
                            <div class="center">
                              @if($subcategory !== null)
                              <input type="submit" class="item_button" value="Обновить">
                              @else
                              <input type="submit" class="item_button" value="Добавить">
                              @endif
                            </div>
                            <div class="right"></div>
                        </div>
                      </form>
                    </div>
                </div>



            </div>
        </div>
        <script>
            $(document).ready(function () {
                $(".file-upload input[type=file]").change(function () {
                    var filename = $(this).val().replace(/.*\\/, "");
                    $("#filename").val(filename);
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $('input[type="checkbox"]').click(function () {
                    if ($(this).attr('id') == 'watch') {
                        if (this.checked) {
                            $('#switch_seo').show();
                        } else {
                            $('#switch_seo').hide();
                        }
                    }
                });
            });
        </script>
        @if($subcategory !== null)
            @if($subcategory->checkseo == 1)
                <script>
                    $(document).ready(function () {
                        $('#switch_seo').show();
                    });
                </script>
            @endif
        @endif
    </div>
</div>
