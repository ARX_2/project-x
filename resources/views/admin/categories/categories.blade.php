
@forelse($categories as $category)
<?php //if($category->images !== null){dd($category);} ?>

<div class="item" id="arrayorder_{{$category->id}}">
    <div class="image">
        <a href=""><img src="{{asset('storage/app')}}/{{Resize::ResizeImage($category->images, '80x80')}}" alt="" ></a>
    </div>
    <div class="info">
        <div class="block">
            <div class="name"><a href="">{{$category->title}}</a></div>
        </div>
        <div class="additionally">
            <div class="type">Раздел:
              <span>
                @if($category->type == 0)
                Товары
                  @elseif($category->type == 1)
                  Услуги
                  @elseif($category->type == 4)
                  Строительство
                  @elseif($category->type == 5)
                  Бани
                  @elseif($category->type == 6)
                  Проекты
                  @elseif($category->type == 7)
                  Изготовление
              @endif</span></div>
            <div class="quantity">Количество товаров в категории:
              @if($category->type == 0)
              <a href="/admin/category/goods/{{$category->id}}"  >
              @elseif($category->type == 1)
              <a href="/admin/category/services/{{$category->id}}">
              @elseif($category->type == 4)
              <a href="/admin/category/stroitelstvo/{{$category->id}}" >
              @elseif($category->type == 5)
              <a href="/admin/category/bani/{{$category->id}}" >
              @elseif($category->type == 6)
              <a href="/admin/category/project/{{$category->id}}">
              @elseif($category->type == 7)
              <a href="/admin/category/izgotovleniye/{{$category->id}}">
              @endif
                {{count($category->goods)}} </a>шт. и общее <span>
                  @if($category->type == 0)
                  <a href="/admin/category/goods/all/{{$category->id}}"  >
                  @elseif($category->type == 1)
                  <a href="/admin/category/services/all/{{$category->id}}">
                  @elseif($category->type == 4)
                  <a href="/admin/category/stroitelstvo/all/{{$category->id}}" >
                  @elseif($category->type == 5)
                  <a href="/admin/category/bani/all/{{$category->id}}" >
                  @elseif($category->type == 6)
                  <a href="/admin/category/project/all/{{$category->id}}">
                  @elseif($category->type == 7)
                  <a href="/admin/category/izgotovleniye/all/{{$category->id}}">
                  @endif
                <?php $i =0 ?>
                @forelse($category->subcategoriesAll as $sub) <?php $i = $i + count($sub->goods);?>
                @empty
                @endforelse
                {{$i + count($category->goods)}}</a></span> шт.</div>

            @if(count($category->subcategoriesAll) > 0)

            <div class="quantity_cat">Подкатегорий: <span><a href="/admin/category/subcategory/{{$category->id}}" class="collapsed" id="category-id-{{$category->id}}" data-toggle="collapse" data-target="#collapse-category-id-{{$category->id}}" aria-expanded="false" aria-controls="collapse-category-id-{{$category->id}}">{{count($category->subcategoriesAll)}}</a></span> шт. - <span><a href="javascript:void(0);" class="collapsed" id="category-id-{{$category->id}}" data-toggle="collapse" data-target="#collapse-category-id-{{$category->id}}" aria-expanded="false" aria-controls="collapse-category-id-{{$category->id}}">показать</a></span></div>
            @endif
        </div>
    </div>
    <div class="status tz-gallery">
        <span class="sign eae @if($category->published ==1)active @endif eaecat" title="Показать / скрыть категорию"><a href="{{route('admin.category.update.id', ['published',$category->id,(int)$category->published])}}"></a></span>
        <span class="sign text  @if($category->offer != null && $category->description == null || $category->offer == null && $category->description != null) one @endif
        @if($category->offer != null && $category->description != null) all @endif">
            <span class="helperstatus"><span class="helperstatusname">Оффер:</span> <span class="limittext">{{$category->offer}}</span><span class="helperstatusname">Описание:</span> <span class="limittext">{!! $category->description !!}</span></span>
        </span>
        <span class="sign search @if($category->seotitle != null && $category->seodescription == null || $category->seotitle == null && $category->seodescription != null) one @endif
        @if($category->seotitle != null && $category->seodescription != null) all @endif">
        <span class="helperstatus"><span class="helperstatusname">Мета&nbsp;заголовок:</span> {{$category->seotitle}}<br><span class="helperstatusname">Мета&nbsp;описание:</span> {{$category->seodescription}}</span></span>
        @if($category->cover != null)<a href="/storage/app/{{$category->cover}}" class="lightbox"><span class="sign cover"></span></a>@endif
        @if($category->youtube != null)<span class="sign youtube"></span>@endif
        <span class="sign star @if($category->main == 1) activeStar @endif" title="Популярная категория. Если активно - выводится на главную страницу сайта"><a href="{{route('admin.category.update.id', ['main',$category->id,(int)$category->main])}}"></a></span>
        @if($category->published == 0 && $category->deleted == 0)<a href="{{route('admin.category.update.id', ['deleted',$category->id,(int)$category->deleted])}}" class="link_delete"><i class="fa fa-times" aria-hidden="true"></i></a>@endif
    </div>
    @if($category->protected !== 1 || $category->protected == 1 && Auth::user()->userRolle !== 6)
    <div class="action">
        <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
        <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
            <!-- <li><a href="{{route('admin.category.hide')}}?id={{$category->id}}">@if($category->published == 1)Скрыть @else Показать @endif</a></li> -->

            <li><a href="{{route('admin.category.edit', [$category->id])}}" class="link_edit">Редактировать</a></li>

            @if($category->main == 1)
            <!-- <li><a href="{{route('admin.category.main', ['id'=>$category->id])}}">Снять с главной</a></li> -->
            @else
            <!-- <li><a href="{{route('admin.category.main', ['id'=>$category->id])}}">На главную</a></li> -->
            @endif
        </ul>
    </div>
    @endif
</div>

<div id="collapse-category-id-{{$category->id}}"  class="sub_item collapse-lvl-{{$category->id}} collapse" aria-labelledby="category-id-{{$category->id}}" data-parent="#accordion">
  <div id="list{{$category->id}}">
  <div id="response{{$category->id}}"> </div>
  <div id="sort{{$category->id}}">
  @forelse($category->subcategoriesAll as $sub)
    <div class="item" id="arrayorder_{{$sub->id}}">
        <div class="arrows-v"><a href=""></a></div>
        <div class="image">
            <a href=""><img src="{{asset('storage/app')}}/{{Resize::ResizeImage($sub->images, '80x80')}}" alt=""></a>
        </div>
        <div class="info">
            <div class="block">
                <div class="name"><a href="">{{$sub->title or ''}}</a></div>
            </div>
            <div class="additionally">
                <div class="quantity">Количество товаров в категории: <span>
                  @if($category->type == 0)
                  <a href="/admin/subcategory/goods/{{$sub->id}}"  >
                  @elseif($category->type == 1)
                  <a href="/admin/subcategory/services/{{$sub->id}}">
                  @elseif($category->type == 4)
                  <a href="/admin/subcategory/stroitelstvo/{{$sub->id}}" >
                  @elseif($category->type == 5)
                  <a href="/admin/subcategory/bani/{{$sub->id}}" >
                  @elseif($category->type == 6)
                  <a href="/admin/subcategory/project/{{$sub->id}}">
                  @elseif($category->type == 7)
                  <a href="/admin/subcategory/izgotovleniye/{{$sub->id}}">
                  @endif
                {{count($sub->goods)}}</a></span> шт.</div>
            </div>
        </div>
        <div class="status tz-gallery">
          <span class="sign eae @if($sub->published ==1)active @endif" title="Показать / скрыть категорию"><a href="{{route('admin.category.hide')}}?id={{$sub->id}}"></a></span>
            <span class="sign text  @if($sub->offer != null && $sub->description == null || $sub->offer == null && $sub->description != null) one @endif
            @if($sub->offer != null && $sub->description != null) all @endif">
            <span class="helperstatus"><span class="helperstatusname">Оффер:</span> <span class="limittext">{{$sub->offer}}</span><span class="helperstatusname">Описание:</span> <span class="limittext">{!! $sub->description !!}</span></span>
            <a href="{{route('admin.category.update.id', ['deleted',$category->id,(int)$sub->deleted])}}" class="link_delete_sub"><i class="fa fa-times @if($sub->deleted == 0)notred @endif" aria-hidden="true" @if($sub->deleted == 1)style="color:red"@endif></i></a>
        </span>
            <span class="sign search @if($sub->seotitle != null && $sub->seodescription == null || $sub->seotitle == null && $sub->seodescription != null) one @endif
            @if($sub->seotitle != null && $sub->seodescription != null) all @endif">
        <span class="helperstatus"><span class="helperstatusname">Мета&nbsp;заголовок:</span> {{$sub->seotitle}}<br><span class="helperstatusname">Мета&nbsp;описание:</span> {{$sub->seodescription}}</span></span>
            @if($sub->cover != null)<a href="/storage/app/{{$sub->cover}}" class="lightbox"><span class="sign cover"></span></a>@endif
            @if($sub->youtube != null)<span class="sign youtube"></span>@endif
        </div>
        @if($sub->protected !== 1 || $sub->protected == 1 && Auth::user()->userRolle !== 6)
        <div class="action">
            <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
            <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                <li><a href="{{route('admin.category.hide')}}?id={{$sub->id}}">@if($sub->published == 1)Скрыть @else Показать @endif</a></li>
                <li><a href="{{route('admin.category.edit', $sub)}}" class="link_edit">Редактировать</a></li>
            </ul>
        </div>
        @endif
    </div>

    @empty
    @endforelse
  </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        function slideout(){
            setTimeout(function(){
                $("#response{{$category->id}}").slideUp("slow", function () {
                });

            }, 2000);}

        $("#response{{$category->id}}").hide();
        $(function() {
            $("#list{{$category->id}} #sort{{$category->id}}").sortable({ opacity: 0.8, cursor: 'move', update: function() {

                    var order = $(this).sortable("serialize") + '&update=update';
                    $.get("{{route('admin.dragAndDrop.category')}}", order, function(theResponse){
                        $("#response{{$category->id}}").html(theResponse);
                        $("#response{{$category->id}}").slideDown('slow');
                        slideout();
                    });
                }
            });
        });
    });

</script>

@empty
@endforelse

<script type="text/javascript">
  $('.eae').click(function(){
    var link = $(this).find('a').attr('href');
    var checkClass = $(this).hasClass('eaecat');
    var count_active = $('#count_active').html();
    count_active = Number.parseInt(count_active);
    var count_hidden = $('#count_hidden').html();
    count_hidden = Number.parseInt(count_hidden);
    var check = $('#link_active').hasClass('active');
    if(checkClass == true){
      $(this).parent('.status').parent('.item').remove();
      if(check == true){
        count_active = count_active-1;
        count_hidden = count_hidden+1;
      }
      else{
        count_active = count_active+1;
        count_hidden = count_hidden-1;
      }
    }
    else{
      if($(this).hasClass('active') == true){
        $(this).removeClass('active');
        count_active = count_active-1;
        count_hidden = count_hidden+1;
      }
      else{
        $(this).addClass('active');
        count_active = count_active+1;
        if(count_hidden >0){
          count_hidden = count_hidden-1;
        }
      }

    }


    $('#count_active').text(count_active);
    $('#count_hidden').text(count_hidden);

    $.get(link, function(data){});

  });
  $('.link_delete').click(function(){
    var link = $(this).attr('href');
    $(this).parent('.status').parent('.item').remove();
    var count_hidden = $('#count_hidden').html();
    count_hidden = Number.parseInt(count_hidden);


    var count_deleted = $('#count_deleted').html();
    count_deleted = Number.parseInt(count_deleted);
      count_hidden = count_hidden-1;
      count_deleted = count_deleted+1;
    $('#count_hidden').text(count_hidden);
    $('#count_deleted').text(count_deleted);

    $.get(link, function(data){});
    return false;
  });
  $('.link_delete_sub').click(function(){
    var link = $(this).attr('href');
    var checkClass = $(this).find('.fa-times').hasClass('notred');
    if(checkClass == true){
      $(this).find('.fa-times').removeClass('notred');
      $(this).find('.fa-times').attr('style', 'color:red');
    }
    else{
      $(this).find('.fa-times').addClass('notred');
      $(this).find('.fa-times').attr('style', '');
    }
    $.get(link, function(data){});
    return false;
  });

  $('.star').click(function(){
    var link = $(this).find('a').attr('href');
    var check =  $(this).hasClass('activeStar');
    if(check == true){
      $(this).removeClass('activeStar');
    }
    else{
      $(this).addClass('activeStar');
    }
    $.get(link, function(data){});

  });
</script>
<link rel="stylesheet" href="/public/site/open-image/open-image.css">
<script src="/public/site/open-image/open-image.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>
