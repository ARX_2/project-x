<?php //dd($categories); ?>
@forelse($categories as $subcategory)
<?php //dd($subcategory); ?>
<div class="item" id="arrayorder_{{$subcategory->id}}">
    <div class="image">
        <a href=""><img src="{{asset('storage/app')}}/{{$subcategory->images->images or '/public/none-img.png'}}" alt=""></a>
    </div>
    <div class="info">
        <div class="block">
            <div class="name"><a href="">{{$subcategory->title}}</a></div>
        </div>
        <div class="additionally">
            <div class="type">Раздел: <span>@if($subcategory->type == 0)Услуги @else Товары @endif</span></div>
            <div class="quantity">Количество товаров в категории: <span><a href="/admin/subcategory/goods/{{$subcategory->id}}">{{count($subcategory->goods)}}</a></span> шт.</div>
        </div>
    </div>
    <div class="status">
        <span class="sign eae @if($subcategory->published ==1)active @endif"></span>
        @if($subcategory->checkseo == 1)
        <span class="sign search @if($subcategory->published == 1) active @endif"></span>
        @endif
        <span class="sign @if($subcategory->main == 1)star @endif"></span>
    </div>
    <div class="action">
        <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
        <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
            <li><a href="{{route('admin.category.hide')}}?id={{$subcategory->id}}">@if($subcategory->published == 1)Скрыть @else Показать @endif</a></li>
            <li><a href="{{route('admin.category.edit', [$subcategory->id])}}">Редактировать</a></li>
            @if($subcategory->main == 1)
            <li><a href="{{route('admin.category.main', ['id'=>$subcategory->id])}}">Снять с главной</a></li>
            @else
            <li><a href="{{route('admin.category.main', ['id'=>$subcategory->id])}}">На главную</a></li>
            @endif
        </ul>
    </div>
</div>

@empty
@endforelse
