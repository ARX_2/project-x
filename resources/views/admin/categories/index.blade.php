
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление категориями</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="">Главная</a></li>
                    <li class="active">Категории</li>
                </ol>
            </div>
        </div>
        <hr>
        <div class="c_p_company">
          <div class="wide">
              <div class="box" style="position: fixed;">
                  <div class="wide_title">
                      <h2>Правая штука</h2>
                  </div>
                  <div class="undefined_item">
                      <div class="item">
                          <div class="name">
                              <a href="/admin/admin/company/upload?type=4">Описание на первой странице товаров</a>
                          </div>
                      </div>
                      <div class="item">
                          <div class="name">
                              <a href="" data-toggle="modal" data-target="#excelModalLong">Загрузка / выгрузка товаров и категорий</a>
                          </div>
                      </div>
                      <div class="item">
                          <div class="name">
                              <a href="" data-toggle="modal" data-target="#editsection">Переименовать раздел</a>
                          </div>
                      </div>
                      <div class="item">
                          <div class="name">
                              <span class="button-edit"><a href="{{$urlCreate or '/admin/category/create'}}">Добавить</a></span>
                          </div>
                      </div>
                      <div class="item">
                          <div class="name">
                              <a href="" data-toggle="modal" data-target="#save">Сохраниться</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
            <div class="narrow">
                <div class="box">
                    <div class="production">
                        <div class="narrow_title">
                            <div class="left">
                                <i class="fa fa-refresh" aria-hidden="true" style="float:left; font-size:20px;color:#14aae6;padding: 5px;border-radius: 50px;" id="refresh"></i>
                                <h2 style="float:left;margin-left:10px;margin-top:5px;">Категории</h2>
                                <p class="clearFix"></p>
                                <style media="screen">
                                  #refresh:hover{
                                    background-color: #e7e6e6;
                                  }
                                </style>
                            </div>
                            <div class="right">
                                <span class="button-edit"><a href="{{route('admin.category.create')}}">Добавить</a></span>
                            </div>
                        </div>
                        <div class="sort">
                            <div class="page_cat">
                                <span><a href="{{$urlActive or ''}}" class="active" id="link_active">Активные <span id="count_active">{{$countActive}}</span></a></span>
                                <span> | </span>
                                <span><a href="{{$urlHidden}}" id="link_hidden">Скрытые <span id="count_hidden">{{$countHide}}</span></a></span>
                                <span> | </span>
                                <span><a href="{{route('admin.categories.ShowDeleted')}}" id="link_deleted">Удаленные <span id="count_deleted">{{$countDeleted or ''}}</span></a></span>
                            </div>
                            <div class="sort_block">
                                <div class="category">
                                    <div class="left">
                                        <div class="name">Сортировать по:</div>
                                    </div>
                                    <div class="right">
                                        <select class="sort_select">
                                            <option value="0" selected="" >Все категории</option>
                                            <option value="1">Услуги</option>
                                            <option value="2">Товары</option>
                                            <option value="7">Изготовление</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('.sort_select').on('change', function() {
                          if($(this).val() == '1') {
                            $.get('{{route('admin.sort.category')}}?type=1',function(data){
                              $('.categorySort').html(data);
                            });
                          }
                          else if ($(this).val() == '2') {
                            $.get('{{route('admin.sort.category')}}?type=0',function(data){
                              $('.categorySort').html(data);
                            });
                          }
                          else if ($(this).val() == '0') {
                            $.get('{{route('admin.sort.category')}}?type=3',function(data){
                              $('.categorySort').html(data);
                            });
                          }
                          else if ($(this).val() == '7') {
                            $.get('{{route('admin.sort.category')}}?type=7',function(data){
                              $('.categorySort').html(data);
                            });
                          }
                        });
                        </script>
                        <div class="categorySort">
                          @if(isset($urlSub))
                          @include('admin.categories.subcategories', ['categories' => $categories])
                          @else

                            <div id="list">
                            <div id="response"> </div>
                            <div id="sort">
                          @include('admin.categories.categories', ['categories' => $categories])
                        </div>
                      </div>
                      <script type="text/javascript">
                          $(document).ready(function(){
                              function slideout(){
                                  setTimeout(function(){
                                      $("#response").slideUp("slow", function () {
                                      });
                                  }, 2000);}

                              $("#response").hide();
                              $(function() {
                                  $("#list #sort").sortable({ opacity: 0.8, cursor: 'move', update: function() {

                                          var order = $(this).sortable("serialize") + '&update=update';
                                          $.get("{{route('admin.dragAndDrop.category')}}", order, function(theResponse){
                                              $("#response").html(theResponse);
                                              $("#response").slideDown('slow');
                                              slideout();
                                          });
                                      }
                                  });
                              });
                          });

                      </script>
                          @endif
                        </div>
                    </div>
                </div>
                <!-- <script type="text/javascript">$(document).ready(function () {
                        function slideout() {
                            setTimeout(function () {
                                $("#response").slideUp("slow", function () {
                                });
                            }, 2000);
                        }
                        $("#response").hide();
                        $(function () {
                            $("#list #sort").sortable({
                                opacity: 0.8, cursor: 'move', update: function () {
                                    var order = $(this).sortable("serialize") + '&update=update';
                                    $.get("{{route('admin.dragAndDrop.category')}}", order, function (theResponse) {
                                        $("#response").html(theResponse);
                                        $("#response").slideDown('slow');
                                        slideout();
                                    });
                                }
                            });
                        });
                    });
                </script> -->
            </div>
        </div>
</div>



<div class="modal fade" id="save" tabindex="-1" role="dialog" aria-labelledby="editsection" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content vm-goods">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <div class="modal-body modal_edit">
                <div class="content">
                    <div class="vm-header">
                        <div class="name">Сохранение</div>
                    </div>
                    <div class="vm-body">
                        <div class="vm-edit">

                            <div class="perent">
                                <div class="left"><label for="" class="item_name">Название:</label></div>
                                <div class="center">
                                    <input type="text" name="saveTitle" class="" placeholder="Категории" value="">
                                </div>
                            </div>

                            <div class="perent" style="margin-bottom: 0">
                                <div class="left"><label for="" class="item_name"></label></div>
                                <div class="center">
                                    <input class="button-save" id="newSave" type="submit" value="Сохранить" style="width: 115px;background-color: #269ffd;color: #fff;">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $('#newSave').click(function(){
    var title = $('input[name="saveTitle"]').val();
    $.get('/admin/save/categories/{{Auth::user()->siteID}}?title='+title, function(data){
      alert('Сохранение успешно выполнено!');
    });
  });
</script>

<div class="modal fade" id="excelModalLong" tabindex="-1" role="dialog" aria-labelledby="excelModalLongTitle" aria-hidden="true" style="display: none;">
<div class="modal-dialog" role="document">
    <div class="modal-content vm-goods">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <div class="modal-body">
            <div class="content">

                <div class="vm-goods">
                    <div class="vm-header">
                        <div class="block">
                            <div class="psh">
                                <div class="left">
                                    <div class="name">Управление кампанией с помощью XLSX</div>
                                </div>
                                <div class="right"></div>
                            </div>
                        </div>
                    </div>

                    <div class="vm-body">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Выгрузка товаров</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Загрузка товаров</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Обновить количество товаров</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-4" role="tab">Архив</a>
                            </li>
                        </ul><!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane" id="tabs-1" role="tabpanel">
                                <p>Формат XLSX</p>
                                <a href="#" id="excelExport" class="button-xlsx" style="padding: 3px 10px 5px;display: table; color: #222; background: #ffdb4d; border-radius: 3px;">Выгрузить</a>
                                <i id="excelUrl"> <img src="{{asset('public/img')}}/giphy.gif" alt="" id="loader" style="display:none; width:50px;"> </i>
                            </div>
                            <div class="tab-pane active" id="tabs-2" role="tabpanel">
                                <p>Формат XLSX</p>
                                <form class="" action="{{route('admin.category.uploadExcel')}}" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="type" value="1" id="uploadExcel">
                                <input type="file" name="file" style="margin-bottom: 15px;">
                                <button type="submit" class="button-xlsx" style="border: none;padding: 3px 10px 5px;display: table; color: #222; background: #ffdb4d; border-radius: 3px;">Загрузить</button>
                                </form>
                            </div>
                            <div class="tab-pane" id="tabs-3" role="tabpanel">
                                <p>Формат XLSX</p>
                                <form class="" action="{{route('admin.goods.uploadExcel')}}" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                                <input type="hidden" name="type" value="2" id="updateExcel">
                                <input type="file" name="file" style="margin-bottom: 15px;">
                                <button type="submith" class="button-xlsx" style="border: none;padding: 3px 10px 5px; display: table; color: #222; background: #ffdb4d; border-radius: 3px;">Обновить</button>
                              </form>
                            </div>
                            <div class="tab-pane" id="tabs-4" role="tabpanel">
                                <p>Формат XLSX</p>

                            </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$('#excelExport').click(function(){
  $.get('{{route('admin.category.excelExport')}}', function(data){
    $('#excelUrl').html(data);
  });
});

$('#refresh').click(function(){
  $('#refresh').addClass('rotate');
  $.get('{{$link or ''}}?ajax=true', function(data){
    $('.admin-content').html(data);
  });
});

$('#link_active').click(function(){

  var active_link = $(this).attr('href');
  $.get(active_link+'?ajax=true', function(data){
    $('.admin-content').html(data);
    $('#link_hidden').removeClass('active');
    $('#link_deleted').removeClass('active');
    $('#link_active').addClass('active');
  });
  history.pushState(null, null, active_link);
  return false;
});
$('#link_hidden').click(function(){

  var hidden_link = $(this).attr('href');
  $.get(hidden_link+'?ajax=true', function(data){
    $('.admin-content').html(data);
    $('#link_active').removeClass('active');
    $('#link_deleted').removeClass('active');
    $('#link_hidden').addClass('active');
  });
  history.pushState(null, null, hidden_link);
  return false;
});
$('#link_deleted').click(function(){

  var deleted_link = $(this).attr('href');
  $.get(deleted_link+'?ajax=true', function(data){
    $('.admin-content').html(data);
    $('#link_active').removeClass('active');
    $('#link_hidden').removeClass('active');
    $('#link_deleted').addClass('active');
  });
  history.pushState(null, null, deleted_link);
  return false;
});

$('.button-edit').click(function(){
  var link = $('.button-edit').find('a').attr('href');
    $.get(link+'?ajax=true', function(data){
      $('.admin-content').html(data);
    });
    history.pushState(null, null, link);
    return false;
});

$('.link_edit').click(function(){
  var link = $(this).attr('href');
    $.get(link+'?ajax=true', function(data){
      $('.admin-content').html(data);
    });
    history.pushState(null, null, link);
    return false;
});
</script>
<style media="screen">
  .rotate{
    -webkit-animation-name: cog;
-webkit-animation-duration: 1s;
-webkit-animation-iteration-count: infinite;
-webkit-animation-timing-function: linear;
-moz-animation-name: cog;
-moz-animation-duration: 1s;
-moz-animation-iteration-count: infinite;
-moz-animation-timing-function: linear;
-ms-animation-name: cog;
-ms-animation-duration: 1s;
-ms-animation-iteration-count: infinite;
-ms-animation-timing-function: linear;

animation-name: cog;
animation-duration: 1s;
animation-iteration-count: infinite;
animation-timing-function: linear;
}
@-ms-keyframes cog {
from { -ms-transform: rotate(0deg); }
to { -ms-transform: rotate(20deg); }
}
@-moz-keyframes cog {
from { -moz-transform: rotate(0deg); }
to { -moz-transform: rotate(20deg); }
}
@-webkit-keyframes cog {
from { -webkit-transform: rotate(0deg); }
to { -webkit-transform: rotate(20deg); }
}
@keyframes cog {
from {
transform:rotate(0deg);
}
to {
transform:rotate(360deg);
}
  }
</style>
