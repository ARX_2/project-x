<div class="row">
  <div class="col-lg-4">
    <h2>{{$title}}</h2>
  </div>
  <div class="col-lg-8">
    <ol class="breadcrumb">
      <li>
        <a href="{{route('admin.index')}}">{{$parent}}</a>
      </li>
      <li class="active">{{$active}}</li>
    </ol>
  </div>
</div>
