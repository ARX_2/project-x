@include('admin.admin.include.mainHeader')

    <script src="/public/admin/js/maskedinput.min.js"></script>
    @include('admin.admin.include.header')

    @if($company->type == 2)
    @include('admin.layout.lmenu-salon')
    @else
    @include('admin.admin.include.lmenu')
    @endif
    <div id="app">
<example-component></example-component>
    </div>

    <script type="text/javascript">
      //var link = $('.name_link').attr('href');
      $('.name_link').click(function(){
          var link = $(this).attr('href');
          if(link != null){
            $.get(link+'?ajax=true', function(data){
              $('.admin-content').html(data);
            });
            history.pushState(null, null, link);
            return false;
          }
      });

    </script>

    <div class="admin-content" id="admin-cont">
