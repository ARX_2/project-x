@include('admin.index')

        <div class="row">
            <div class="col-lg-4">
                <h2>Личный кабинет</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="">Главная</a></li>
                    <li class="active">Личный кабинет</li>
                </ol>
            </div>
        </div>
        <hr>
        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="cp_edit">
                        <div class="narrow_title">
                            <div class="left">
                                <h2>Личный кабинет</h2>
                            </div>
                            <div class="right">
                                <span class="button-edit"><a href="{{route('admin.profile.edit')}}">Редактировать</a></span>
                            </div>
                        </div>
                        <style>
                            .profile{padding: 0 10px 10px;width: 100%;display: inline-flex}
                            .profile .image{float: left;width: 200px;min-width:200px;height: 200px}
                            .profile .image img{width: 100%;height: 100%;object-fit: cover}
                            .profile_info{margin-left: 20px;width: 100%;}
                            .title{border-bottom: 1px solid #e6e6e6;margin-bottom: 10px;padding-bottom: 10px;width: 100%}
                            .profile_info .name{font-size: 16px;font-weight: 600;line-height: 18px}
                            .profile_info .info {border-bottom: 1px solid #e6e6e6;margin-bottom: 10px;padding-bottom: 10px;width: 100%}
                            .profile_info .info:last-child{border-bottom: 0}
                        </style>
                        <div class="profile">
                            <div class="image">
                                <img src="/storage/app/{{Auth::user()->image}}" alt="">
                            </div>
                            <div class="profile_info">
                                <div class="title">
                                    <div class="name"><?php echo Auth::user()->name; ?> <?php echo Auth::user()->lastname; ?></div>
                                    <?php //dd(Auth::user()); ?>
                                    <div class="position"><?php echo Auth::user()->job; ?></div>
                                </div>
                                <div class="info">
                                    <div class="item">Компания: {{$company->name or 'Название компании отсутствует'}}</div>
                                </div>
                                <div class="info">
                                    <div class="item">Телефон: <?php echo Auth::user()->tel; ?></div>
                                    <div class="item">Почта: <?php echo Auth::user()->email; ?></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <style>
                    .future_profile{width: 100%;display: inline-flex;margin-bottom: 20px}
                    .future_profile .left{width: 30%;height: 400px;background: #ffffffE6;box-shadow: 0 1px 15px rgba(0,0,0,.04), 0 1px 6px rgba(0,0,0,.04);}
                    .future_profile .right{width: 70%;height: 400px;background: #ffffffE6;margin-left: 20px;box-shadow: 0 1px 15px rgba(0,0,0,.04), 0 1px 6px rgba(0,0,0,.04);}
                    .future_profile .full{width: 1000%;height: 200px;background: #ffffffE6;padding:20px;box-shadow: 0 1px 15px rgba(0,0,0,.04), 0 1px 6px rgba(0,0,0,.04);}
                </style>
                <div class="future_profile"><div class="left"></div><div class="right"></div></div>
                <div class="future_profile"><div class="full">Данный раздел разрабатывается!<br>Если есть вариант реализации - пишите в поддержку<br><br>* полезный функционал и удобство - приоритетные задачи</div></div>
            </div>
        </div>
    </div>
