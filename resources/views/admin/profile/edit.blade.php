@include('admin.index')

        <div class="row">
            <div class="col-lg-4">
                <h2>Личный кабинет</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="">Главная</a></li>
                    <li class="active">Личный кабинет</li>
                </ol>
            </div>
        </div>
        <hr>
        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                </div>
            </div>

            <div class="narrow">
                <div class="box">
                  <form class="" action="{{route('admin.profile.update')}}" method="post">
                    {{csrf_field()}}
                    <div class="cp_edit">
                        <div class="narrow_title">
                            <h2>Редактирование ЛК</h2>
                        </div>
                        <div class="profile">
                            <style>
                                .perent input.user_name{width:48%;float:left;margin-right:2%;padding-right:20px}
                                .perent input.sex{width:30%;margin-right:1%;text-align:center;float:left}
                            </style>
                            <div class="perent">
                                <div class="left"><label for="" class="item_name">Имя и фамилия:</label></div>
                                <div class="center">
                                    <input type="text" name="name" class="user_name" placeholder="Имя" value="<?php echo Auth::user()->name; ?>" required="">
                                    <input type="text" name="lastname" class="user_name" placeholder="Фамилия" value="<?php echo Auth::user()->lastname; ?>" required="">
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left"><label for="" class="item_name">Пол:</label></div>
                                <div class="center">
                                    <label class="radio_item">
                                        <input type="radio" id="user_sex" name="user_sex" class="sex" placeholder="Мужской" @if(Auth::user()->sex == 0) checked="" @endif value="0" required="">
                                        <div class="radio_box">Мужской</div>
                                    </label>
                                    <label class="radio_item">
                                        <input type="radio" id="user_sex" name="user_sex" class="sex" placeholder="Женский" @if(Auth::user()->sex == 1) checked="" @endif value="1" required="">
                                        <div class="radio_box">Женский</div>
                                    </label>
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left"><label for="" class="item_name">Email:</label></div>
                                <div class="center">
                                    <input type="text" name="email" class="" placeholder="" value="<?php echo Auth::user()->email; ?>" required="">
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left"><label for="" class="item_name">Телефон:</label></div>
                                <div class="center">
                                    <input type="text" name="tel" class="" placeholder="" value="<?php echo Auth::user()->tel; ?>" required="">
                                </div>
                            </div>
                            <div class="perent" style="margin-bottom: 0">
                                <div class="left"><label for="" class="item_name"></label></div>
                                <div class="center">
                                    <input class="button-save" type="submit" value="Сохранить" style="width: 115px;background-color: #269ffd;color: #fff;">
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="box">
                    <div class="cp_edit">
                        <div class="narrow_title">
                            <h2>Изменить пароль</h2>
                        </div>
                        <div class="profile">
                          <form class="" action="{{route('admin.profile.password')}}" method="post">
                            {{csrf_field()}}
                            <div class="perent">
                                <div class="left"><label for="" class="item_name">Старый пароль:</label></div>
                                <div class="center">
                                    <input type="text" name="lastpassword" class="" placeholder="" value="" required="">
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left"><label for="" class="item_name">Новый пароль:</label></div>
                                <div class="center">
                                    <input type="text" name="newpassword" class="" placeholder="" value="" required="">
                                </div>
                            </div>
                            <div class="perent">
                                <div class="left"><label for="" class="item_name">Повторите пароль:</label></div>
                                <div class="center">
                                    <input type="text" name="confirmpassword" class="" placeholder="" value="" required="">
                                </div>
                            </div>
                            <div class="perent" style="margin-bottom: 0">
                                <div class="left"><label for="" class="item_name"></label></div>
                                <div class="center">
                                    <input class="button-save" type="submit" value="Сохранить" style="width: 115px;background-color: #269ffd;color: #fff;">
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($error == 1)
    <script type="text/javascript">
      alert('Старый пароль не верен');
    </script>
    @elseif($error == 2)
    <script type="text/javascript">
      alert('Пароли не совпадают');
    </script>
    @elseif($error == 3)
    <script type="text/javascript">
      alert('Новый пароль совпадает с старым паролем');
    </script>
    @endif
