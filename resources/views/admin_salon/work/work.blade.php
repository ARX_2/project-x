@forelse($masters as $master)
<tr>
  <td class="masterWork" colspan="7">{{$master->name or ''}}</td>
</tr>
<tr>
  @for($i =1; $i <= 48; $i++)
  <td class="work" data-toggle="modal" data-target="#addService"><i class="fa fa-plus" aria-hidden="true"></i></td>
  @endfor

</tr>
@empty
@endforelse


<div class="modal fade" id="addService" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document" style="width:40%">
<div class="modal-content">
  <div class="modal-header">
    <h4 class="modal-title" id="exampleModalLabel">Добавление услуги на 11:30</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body" >
    <div >
      <div style="width:100%;">
        <label for="" style="width:100%; font-size:12px;">
          Фамилия Имя Отчество
          <input type="text" name="name" value="" class="form-control" placeholder="Иванов Иван Иванович">
        </label>
      </div>

      <div>

          <label for="" style="width:20%;float:left; font-size:12px;">
            № Тел.
            <input type="text" name="name" value="" class="form-control" placeholder="+79008008080">
          </label>
          <div class="clearFix"></div>


        <div style="border:1px solid #ddd;float:left;width:25%;margin-left:10px;">
          <label for="" style="width:40%;float:left; font-size:12px;margin-left:15px;">
            № Карты
            <input type="text" name="name" value="" class="form-control" placeholder="4242">
          </label>

          <label for="" style="width:30%;float:left; font-size:12px;margin-left:10px;">
            Скидка
            <input type="text" name="name" value="" class="form-control" placeholder="10%">
          </label>
          <div class="clearFix"></div>
        </div>

        <div style="border:1px solid #ddd;float:left;width:27%;margin-left:10px;">
          <label for="" style="width:40%;float:left; font-size:12px;margin-left:10px;">
            День рож.
            <input type="text" name="name" value="" class="form-control" placeholder="30">
          </label>

          <label for="" style="width:42%;float:left; font-size:12px;margin-left:10px;">
            Месяц
            <select name="" class="form-control">
              <option disabled selected>Мес.</option>
              <option value="01">Январь</option>
              <option value="02">Февраль</option>
              <option value="03">Март</option>
              <option value="04">Апрель</option>
              <option value="05">Май</option>
              <option value="06">Июнь</option>
              <option value="07">Июль</option>
              <option value="08">Август</option>
              <option value="09">Сентябрь</option>
              <option value="10">Октябрь</option>
              <option value="11">Нябрь</option>
              <option value="12">Декабрь</option>
            </select>
          </label>
          <div class="clearFix"></div>
          </div>

          <div style="float:left;">
            <button type="button" class="btn" name="button" style="padding: 3px 12px; margin-left:2px;background-color: #5091d2;color: white;">Муж.</button><br>
            <button type="button" class="btn" name="button" style="padding: 3px 12px; margin:2px;background-color: #d2509f;color: white;">Жен.</button>
          </div>

          <div style="float:left; margin-left:10px;">
          <label for=""><input type="checkbox" name="VIP" value="1" style="width:20px; height:20px;"> <span style="margin-bottom:3px;">VIP</span> </label><br>
          <label for=""><input type="checkbox" name="BL" value="1" style="width:20px;height:20px;"> <span style="margin-bottom:3px;">Ч.С.</span>  </label>
          </div>

        <textarea name="comment" class="form-control" style="width:70%; height:10vh;float:left;" placeholder="Комментарий к клиенту"></textarea>
        <select class="form-control" style="width:24%; float:left;margin-left:10px;">
          <option disabled selected>Источник клиента</option>
          <option >Узнал из флаера</option>
          <option >Узнал от сайта</option>
          <option >Узнал из Вконтакте</option>
          <option >Узнал из Инстаграмма</option>
          <option >Посоветовали</option>
          <option >С улицы</option>
        </select>
        <br>
        <select class="form-control" style="width:24%; float:left;margin:10px;">
          <option disabled selected>SMS оповещения</option>
          <option >SMS: разрешены</option>
          <option >SMS: запретить уведомления</option>
          <option >SMS: запретить рекламные рассылки</option>
          <option >SMS: запретить все</option>
        </select>
        <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
      <br>
      <hr>
      <h5>Заказ</h5>
      <div class="">
          <input type="text" name="timestart" class="form-control" value="11:30" style="width:10%;float:left;" readonly>
          <input type="text" name="timework" class="form-control"  style="width:10%;float:left;margin-left:10px;" placeholder="мин.">
          <select class="form-control" name="timestamp" style="width:1%;float:left;" >
            <option value="5">5 мин</option>
            <option value="15">15 мин</option>
            <option value="30">30 мин</option>
            <option value="60">1 час</option>
            <option value="75">1 час 15 мин.</option>
            <option value="90">1 час 30 мин.</option>
            <option value="105">1 час 45 мин.</option>
            <option value="120">2 часа</option>
            <option value="210">2 часа 30 мин.</option>
            <option value="180">3 часа</option>
            <option value="240">4 часа</option>
            <option value="300">5 часов</option>
            <option value="360">6 часов</option>
            <option value="420">7 часов</option>
            <option value="480">8 часов</option>
          </select>

          <label for="" style="width:70%;float:left;margin-left:10px;"><span style="margin-top:5px;">Администратор:</span>
            <select class="form-control" name="timestamp" style="width:60%;display: initial; float:right" >
            @forelse($masters as $master)
            <option value="{{$master->id}}">{{$master->name}} ({{$master->job}})</option>
            @empty
            @endforelse
          </select></label>
          <div class="clearfix"></div>

          <label style="float:left;width:25%">
            <input type="checkbox" name="" value="1" style="width:20px; height:20px;">
            Запись подтверждена
          </label>
          <!-- <label style="float:left;">
            <input type="checkbox" name="" value="1" style="width:20px; height:20px;">
            Клиент пришел
          </label> -->

          <label for="" style="width:70%;float:left;margin-left:10px;"><span style="margin-top:5px;">Мастер:</span>
            <select class="form-control" name="timestamp" style="width:60%;display: initial; float:right" >
            @forelse($masters as $master)
            <option value="{{$master->id}}">{{$master->name}} ({{$master->job}})</option>
            @empty
            @endforelse
          </select></label>
          <div class="clearfix"></div>

          <label style="float:left;width:25%">
            <input type="checkbox" name="" value="1" style="width:20px; height:20px;">
            Запись подтверждена
          </label>
          <textarea name="comment" class="form-control" style="width:72%; height:30px;" placeholder="Примечание к предварительной записи"></textarea>
          <div class="clearfix"></div>

          <div class="dropdown" style="float:left;">
          <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float:left; background-color:#383838; color:white;">
          <i class="fa fa-handshake-o" aria-hidden="true"></i> Услуги
          </button>
          <div class="dropdown-menu dropservices" aria-labelledby="dropdownMenuButton1">
            <div style="padding:15px;" class="prokrutka">
              <input type="text" class="search">
              @forelse($services as $serice)
              <li class="dropli" type="service" id="{{$serice->id}}">{{$serice->title}}</li>
              @empty
              @endforelse
            </div>
          </div>
        </div>
        <div class="dropdown" style="float:left;">
          <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float:left;margin-left:10px;background-color:#383838; color:white;">
            <i class="fa fa-diamond" aria-hidden="true"></i>  Материалы
          </button>
          <div class="dropdown-menu dropmaterials" aria-labelledby="dropdownMenuButton2" >
            <div style="padding:15px;" class="prokrutka">
              <input type="text" class="search">
              @forelse($materials as $material)
              <li class="dropli" type="material" id="{{$material->id}}">{{$material->title}}</li>
              @empty
              @endforelse
            </div>
          </div>
        </div>
        <div class="dropdown" style="float:left;">
          <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float:left;margin-left:10px;background-color:#383838; color:white;">
            <i class="fa fa-cubes" aria-hidden="true"></i> Товары
          </button>
          <div class="dropdown-menu dropgoods" aria-labelledby="dropdownMenuButton3">
            <div style="padding:15px;" class="prokrutka">
              <input type="text" class="search">
              @forelse($goods as $product)
              <li class="dropli" type="product" id="{{$product->id}}">{{$product->title}}</li>
              @empty
              @endforelse
            </div>
          </div>
        </div>
          <div class="clearfix"></div>
                <span style="float:left;font-weight:600;">Название</span>
                <span style="float:right;margin-right:10px;font-weight:600;">Стоимость</span>
                <span style="float:right;margin-right:5px;font-weight:600;">Скидка</span>
                <span style="float:right;margin-right:5px;width:10%;font-weight:600;">Цена</span>
                <span style="float:right;margin-right:5px;font-weight:600;">Кол-во </span>
                <span style="float:right;margin-right:5px;font-weight:600;">Мастер/Личн.продажа</span>
                <span style="float:right;margin-right:5px;font-weight:600;">Длительность</span>
                <br>

                <div class="options">

                </div>


          </div>
          <hr>
          <h4>Оплата</h4>
          <div class="">
            <label for="" style="float:left;"><input type="checkbox" name="" value="1"> Абонементом</label>
            <label for="" style="float:left;margin-left:20px;"><input type="radio" name="" value="1">Наличный расчет</label>
            <span style="float:right;width:25%">К оплате: <b class="amountAll">0</b><b>₽</b> </span>
            <div class="clearfix"></div>
            <label for="" style="float:left;"><input type="checkbox" name="" value="1"> Сертификатом</label>
            <label for="" style="float:left;margin-left:20px;"><input type="radio" name="" value="1">Безналичный расчет</label>
            <span style="float:right;width:25%">Наличными: <input type="text" name="" value="0" style="width:30%;display: inline;" class="form-control"> </span>

            <div class="clearfix"></div>
            <span style="float:right;width:25%">Сдача: <b>0</b><b>₽</b> </span>
            <div class="clearfix"></div>
          </div>
          </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            <button type="button" class="btn btn-primary">Сохранить</button>
          </div>
          </div>
          </div>
          </div>

          <script type="text/javascript">

            $('.dropservices').on('keyup', '.search', function(){
                          _this = this;
                          $.each($(".liservice"), function() {
                              if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                                 $(this).hide();
                              else
                                 $(this).show();
                          });
                      });
          </script>
          <script type="text/javascript">

            $('.dropmaterials').on('keyup', '.search', function(){
                          _this = this;
                          $.each($(".limaterial"), function() {
                              if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                                 $(this).hide();
                              else
                                 $(this).show();
                          });
                      });
          </script>
          <script type="text/javascript">

            $('.dropgoods').on('keyup', '.search', function(){
                          _this = this;
                          $.each($(".liproduct"), function() {
                              if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                                 $(this).hide();
                              else
                                 $(this).show();
                          });
                      });
          </script>
          <script type="text/javascript">
            $('.dropli').click(function(){
              var type = $(this).attr('type');
              var id = $(this).attr('id');
              $.get('/admin-salon/work/'+type+'/'+id, function(data){
                $('.options').append(data);
                $('.amountAll').text($('input[name="amount"]').val());

                $('input[name="percent"]').on('input', function(){
                  var cost = Number.parseInt($(this).closest('.opt').find('input[name="cost"]').val());
                  var pesent = Number.parseInt($(this).val());
                  var amount = cost - (pesent * 0.01 * cost);
                  amount = amount.toFixed();
                  $(this).closest('.opt').find('.amount').text(amount+'.р');
                  $('.amount').text(amount);
                });

                $('input[name="counts"]').on('input', function(){

                  if($(this).val() !== '' && $(this).closest('.opt').find('input[name="hiddenCost"]').val() !== ''){

                  var cost = Number.parseInt($(this).closest('.opt').find('input[name="hiddenCost"]').val());
                  var amountAll = Number.parseInt($('input[name="amount"]').val());
                  var oldCost = $(this).closest('.opt').find('.amount').text();
                  oldCost = Number.parseInt(oldCost)

                  //console.log(amountAll);
                  //$('input[name="amount"]').val(amountAll - cost);
                  var pesent = Number.parseInt($(this).closest('.opt').find('input[name="percent"]').val());

                  var counts = Number.parseInt($(this).val());
                  $(this).closest('.opt').find('input[name="cost"]').val(cost*counts);
                  cost = cost*counts;

                  if($(this).closest('.opt').find('input[name="percent"]').val() !== ''){
                    var amount = cost - (pesent * 0.01 * cost);
                  }
                  else{
                    var amount = cost;
                  }
                  amountAll = amountAll - oldCost;
                  //console.log(amount);
                  amountAll = amountAll + amount;

                  amount = amount.toFixed();
                  amountAll = amountAll.toFixed();
                  $(this).closest('.opt').find('.amount').text(amount+'.р');

                  $('input[name="amount"]').val(amountAll);
                  $('.amountAll').text(amountAll);
                  }
                  else{
                    if($(this).closest('.opt').find('input[name="hiddenCost"]').val() !== ''){
                      var cost = Number.parseInt($(this).closest('.opt').find('input[name="hiddenCost"]').val());
                      var amountAll = Number.parseInt($('input[name="amount"]').val());
                      amountAll = amountAll - cost;
                      $('input[name="amount"]').val(amountAll);
                      $('.amountAll').text(amountAll);
                        $(this).closest('.opt').find('input[name="cost"]').val(cost);
                        $('.amount').text(cost);
                    }

                  }
                });

                $('.fa-times').click(function(){
                  $(this).closest('.opt').remove();
                });
              });
            });
          </script>

          <script type="text/javascript">
          $('select[name="timestamp"]').on('change',function(){
            $('input[name="timework"]').val($(this).val());
          });
            </script>
            <style media="screen">
            .masterWork{
              padding:5px;
              text-align:center;
              background-color:#383838;
              color:white;
              border-radius:0 15px 0 0;
              cursor:default;
            }
            .masterWork:hover{
              background-color:#282828 !important;
            }
            .work{
              text-align:center; border:1px solid #ddd
            }
            .work i{
              color:#45861b;
            }
            </style>
            <style media="screen">
            .prokrutka {
              height: 180px;
              width: 100%; /* ширина нашего блока */
              background: #fff; /* цвет фона, белый */
              overflow-y: scroll; /* прокрутка по вертикали */
            }
            .prokrutka li{
              cursor: default;
              margin: 8px auto;
              font-size: 16px;
              width:100%
            }
            .prokrutka li:hover{
              background-color: #e3e3e3;
            }
            .payment{
              width:75px !important;margin-left:15px!important;cursor:default;
            }
            .dopborder{
              box-shadow: 0.5px 0.5px 5px 0.5px rgb(173,58,40);
              /* border: 1px solid black !important; */
            }
          </style>
          <style>



        ::-webkit-scrollbar {
        width: 3px; height: 3px;
        }


        ::-webkit-scrollbar-track-piece  {
        background-color: #c7c7c7;
        }

        ::-webkit-scrollbar-thumb:vertical {
        height: 50px; background-color: #666; border-radius: 3px;
        }

        ::-webkit-scrollbar-thumb:horizontal {
        height: 50px; background-color: #666; border-radius: 3px;
        }

        ::-webkit-scrollbar-corner{
        background-color: #999;
        }
        .emoji{
        width: 18px !important; height:18px !important;
        }
          </style>
