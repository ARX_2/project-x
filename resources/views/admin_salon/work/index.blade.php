@include('admin.index')
<script type="text/javascript">
  $('#admin-cont').remove();
</script>

    <div class="admin-content">
      <div class="row">
        <div class="col-lg-4">
            <h2>Управление сайтом</h2>
        </div>
        <div class="col-lg-8">
            <ol class="breadcrumb">
                <li><a href="/">Главная</a></li>
                <li class="active">Заказы</li>
            </ol>
        </div>
    </div>
        <hr>
        <style>
            .orders .title{padding-bottom:20px;border-bottom:1px solid #ddd;margin-bottom:20px;display:table;width:100%}
            .orders .title .left{width:50%;float:left}
            .orders .title h2{font-size:16px;font-weight:600;margin:0}
            .orders .title h2 a{color: #666}
            .orders .title h2 span{}
            .orders .title .right{width:50%;float:left;font-size:14px;line-height:14px;color:#666;text-align:right}
            .orders .button-edit{cursor:pointer;z-index:3;width:102px;height:30px;padding:7px 14px;margin-left:25px;font-size:14px;background:#0095eb;color:#fff;border-radius:5px;text-align:center;vertical-align:-webkit-baseline-middle}
            .orders .title .right .button-edit a{color:#fff!important}
            .orders table{width:100%}
            .orders table input{width:100%}
            .orders table a{color:#0095eb}
            .orders table thead{border-bottom:1px solid #ddd}
            .orders table thead th{font-weight:600}
            .orders table tbody tr{border-bottom:1px solid #ddd}
            .orders table td:hover{background:rgba(233,235,238,0.5)}
            .orders .order-number{min-width:70px;width:5%;padding:10px;border-right:1px solid #ddd;vertical-align:top; text-align: center;}
            .orders .order-goods{width:25%;min-width:200px;padding:10px;border-right:1px solid #ddd;vertical-align:top}
            .orders .order-goods .goods{display:table;margin-top:30px}
            .orders .order-goods .open{text-align:center;position:relative;margin-top:20px}
            .orders .order-goods .open:after{font-family:FontAwesome;color:#0095eb;content:"\f103";font-size:13px;display:block;padding-left:5px;font-weight:400}
            .orders .order-goods .left{width:20%;float:left}
            .orders .order-goods .right{width:80%;float:left;padding-left:15px}
            .orders .order-goods .name{margin-bottom:10px;font-weight:600;color:#333;overflow:hidden;max-height:40px}
            .orders .order-goods .price{color:#666}
            .orders .order-goods .price span{font-weight:600;color:#333}
            .orders .order-goods .price span:after{font-family:FontAwesome;color:#333;content:"\f158";font-size:13px;padding-left:5px;font-weight:400}
            .orders .order-user{width:25%;padding:10px;border-right:1px solid #ddd;vertical-align:top}
            .orders .order-user .name{padding-right:25px;margin-bottom:10px}
            .orders .order-user .address{margin-top:10px}
            .orders .order-full-price{vertical-align:top;width:10%;padding:10px;border-right:1px solid #ddd}
            .orders .order-full-price .full-price .sum{font-weight:600;margin-bottom:10px}
            .orders .order-full-price .full-price .sum:after{font-family:FontAwesome;color:#333;content:"\f158";font-size:13px;padding-left:5px;font-weight:400}
            .orders .order-full-price .status-pay{font-size:16px;font-weight:400;text-align:right;float:right}
            .clock:after{font-family:FontAwesome;content:"\f017";color:#e46a76}
            .percent:after{font-family:FontAwesome;content:"\f0d6";color:#f0ad4e}
            .check:after{font-family:FontAwesome;content:"\f00c";color:#00c292}
            .orders .order-status{width:10%;padding:10px;border-right:1px solid #ddd;vertical-align:top}
            .sort_input{border-radius:25px;padding:3px 10px;border:1px solid #ddd;background:#fff;color:#333;font-weight:400}
            .sort_input:focus{outline:0;outline-offset:0}
            .sort_select{border-radius:25px;padding:3px 10px;border:none;background:#e9ebee;color:#333;font-weight:400}
            .sort_select:focus{outline:0;outline-offset:0}
            .orders .select_status{display:inline;padding:3px 10px;border-radius:25px;border:none;font-size:12px}
            .orders .select_status option{background:#fff;color:#666}
            .orders .order-status .button-status{display:inline;padding:3px 10px;border-radius:25px;font-size:12px}
            .orders .order-status .danger{background:#e46a76;color:#fff}
            .orders .order-status .warning{background:#f0ad4e;color:#fff}
            .orders .order-status .success{background:#00c292;color:#fff}
            .orders .order-status .secondary{background:#aaa;color:#fff}
            .orders .order-delivery{width:10%;padding:10px;border-right:1px solid #ddd;vertical-align:top}
            .orders .order-action{vertical-align:top;padding:10px;width:5%;min-width:70px;position:relative}
            .orders .action{text-align:right}
            .orders .action .edit:before{font-family:FontAwesome;color:#666;content:"\f141"}
        </style>
        <?php //dd($offers); ?>
        <div class="c_p_company">
            <div class="narrow" style="width: 100%;padding-right: 0">
                <div class="box">
                    <div class="orders">
                        <div class="title">
                            <div class="left">
                                <h2>Рабочий день</h2>
                            </div>
                            <div class="right">
                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                Рабочие смены
                              </button>
                            </div>
                        </div>

                        <div class="block">
                          <table style="width:30%;border-collapse: separate;">
                            <thead>
                              @forelse($dates as $date)
                              <td class="@if(date("d.m.Y", strtotime($date)) == date('d.m.Y')) chooseDayNow @else chooseDays @endif clickDay" day="{{date("d", strtotime($date))}}" month="{{date("m", strtotime($date))}}">{{Resize::dayMonth($date)}}</td>
                              @empty
                              @endforelse
                            </thead>
                          </table>
                            <table class="dashwork">
                                <thead>
                                    <tr>
                                        <th class="order-number" colspan="4">
                                            9:00
                                        </th>
                                        <th class="order-number" colspan="4">
                                            10:00
                                        </th>
                                        <th class="order-number" colspan="4">
                                            11:00
                                        </th>
                                        <th class="order-number" colspan="4">
                                            12:00
                                        </th>
                                        <th class="order-number" colspan="4">
                                            13:00
                                        </th>
                                        <th class="order-number" colspan="4">
                                            14:00
                                        </th>
                                        <th class="order-number" colspan="4">
                                            15:00
                                        </th>
                                        <th class="order-number" colspan="4">
                                            16:00
                                        </th>
                                        <th class="order-number" colspan="4">
                                            17:00
                                        </th>
                                        <th class="order-number" colspan="4">
                                            18:00
                                        </th>
                                        <th class="order-number" colspan="4">
                                            19:00
                                        </th>
                                        <th class="order-number" colspan="4">
                                            20:00
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>

                                 {{-- @include('admin_salon.work.work', ['masters' => $masters,'goods' => $goods, 'services' => $services, 'materials' => $materials])--}}

                                </tbody>
                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
    <script type="text/javascript">
      $('.clickDay').click(function(){
        if($(this).hasClass('chooseDays')){
          $('.clickDay').removeClass('chooseDayNow');
          $('.clickDay').addClass('chooseDays');
          $(this).removeClass('chooseDays');
          $(this).addClass('chooseDayNow');
          var day = $(this).attr('day');
          var month = $(this).attr('month');
          $.get('/admin-salon/work/days/'+day+'/'+month, function(data){
            $('.dashwork').find('tbody').html(data);;
          });
        }
      });
    </script>
    <style media="screen">
      .chooseDays{
        border-left:1px solid #ddd;
        border-top:1px solid #ddd;
        border-right:1px solid #ddd;
        padding:5px;
        text-align:center;
        background-color:#383838;
        color:white;
        cursor:default;
      }
      .chooseDayNow{
        border-left:1px solid #ddd;
        border-top:1px solid #ddd;
        border-right:1px solid #ddd;
        padding:5px;
        text-align:center;
        cursor:default;
        background-color: #f0f0f0;
      }
      .chooseDays:hover{
        background-color:#282828 !important;
      }
    </style>
    <style media="screen">
      .addMaster{
        text-align:center; border:1px solid black; padding: 30px 0; text-decoration:none; color:black;
      }

    </style>

    <script>
        $(".spoiler-trigger").click(function() {
            $(this).parent().next().collapse('toggle');
        });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $.get('/admin-salon/calendar/now/{{date('d.m.Y')}}', function(data){
          data = $.parseJSON(data);
          $('.dates').html(data.days);
          $('.dinamic').remove();
          $('.month').append(data.month);
          $('.lastdays').attr('id', data.lastdays);
          $('#daylast').text(data.daylast);
          $('#daynext').text(data.daynext);
          $('#monthlast').text(data.monthlast);
          $('#monthnext').text(data.monthnext);
          $('.table').find('tbody').html(data.mastersDays);

          $('.checkday').click(function(){
            var day = $(this).attr('id');
            var master = $(this).attr('name');
            var month = $(this).attr('month');
            if($(this).find('span').hasClass('tdspanback')){
              $(this).find('span').removeClass('tdspanback');
              $.get('/admin-salon/checkday/remove/'+day+'/'+month+'/'+master);
            }
            else{
              $(this).find('span').addClass('tdspanback');
              $.get('/admin-salon/checkday/add/'+day+'/'+month+'/'+master);
            }
          });
        });

        $('.movedays').click(function(){
          var day = $(this).attr('id');
          $.get('/admin-salon/calendar/next/'+day, function(data){
            data = $.parseJSON(data);
            $('.dates').html(data.days);
            $('.dinamic').remove();
            $('.month').append(data.month);
            $('.nextdays').attr('id', data.nextDays);
            $('.lastdays').attr('id', data.lastdays);
            $('#daylast').text(data.daylast);
            $('#daynext').text(data.daynext);
            $('#monthlast').text(data.monthlast);
            $('#monthnext').text(data.monthnext);
            $('.table').find('tbody').html(data.mastersDays);
            $('.checkday').click(function(){
              var day = $(this).attr('id');
              var master = $(this).attr('name');
              var month = $(this).attr('month');
              if($(this).find('span').hasClass('tdspanback')){
                $(this).find('span').removeClass('tdspanback');
                $.get('/admin-salon/checkday/remove/'+day+'/'+month+'/'+master);
              }
              else{
                $(this).find('span').addClass('tdspanback');
                $.get('/admin-salon/checkday/add/'+day+'/'+month+'/'+master);
              }
            });
          });
        });
      });
    </script>


    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width:69%">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Рабочие смены</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
                    <thead>
                        <tr class="month">
                            <th rowspan="2" colspan="3" nowrap="" valign="middle" id="cornerTopLeft">
                                <table >
                                    <thead>
                                        <tr>
                                            <td valign="left">
                                                <button class="btn btn-primary movedays lastdays" id="11.05.2019" style="float: left;margin-right: 12px;">
                                                    <i class="fa fa-arrow-left" progress="true"></i>
                                                </button>
                                            </td>
                                            <td valign="middle">
                                                <strong style="font-size: 20px" ><span id="daylast"></span> <span id="monthlast"></span> – <span id="daynext"></span> <span id="monthnext"></span> 2019г.</strong>
                                            </td>
                                            <td valign="right">
                                                <button class="btn btn-primary movedays nextdays" id="03.06.2019" style="float: right;margin-left: 12px;" progress="true">
                                                    <i class="fa fa-arrow-right" progress="true"></i></button>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </th>

                        </tr>
                        <tr class="dates">

                        </tr>
                    </thead>
                    <tbody>



                    </tbody>
                </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary">Сохранить</button>
      </div>
    </div>
  </div>
</div>

<style media="screen">
  .tdspan{
    padding-bottom: 6px;
    padding-left: 10px;
    padding-right: 10px;
  }
  .tdspanback{
    background-color: #8e1485;
  }
</style>
</body></html>
