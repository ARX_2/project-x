  <label style="float:left;width:25%">
    <input type="checkbox" name="" value="1" style="width:20px; height:20px;">
    Запись подтверждена
  </label>
  <textarea name="comment" class="form-control" style="width:72%; height:30px;" placeholder="Примечание к предварительной записи"></textarea>
  <div class="clearfix"></div>

  <div class="dropdown" style="float:left;">
  <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float:left; background-color:#383838; color:white;">
  <i class="fa fa-handshake-o" aria-hidden="true"></i> Услуги
  </button>
  <div class="dropdown-menu dropservices" aria-labelledby="dropdownMenuButton1">
    <div style="padding:15px;" class="prokrutka">
      <input type="text" class="search">
      @forelse($services as $serice)
      <li class="dropli" type="liservice" id="{{$serice->id}}">{{$serice->title}}</li>
      @empty
      @endforelse
    </div>
  </div>
</div>
<div class="dropdown" style="float:left;">
  <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float:left;margin-left:10px;background-color:#383838; color:white;">
    <i class="fa fa-diamond" aria-hidden="true"></i>  Материалы
  </button>
  <div class="dropdown-menu dropmaterials" aria-labelledby="dropdownMenuButton2" >
    <div style="padding:15px;" class="prokrutka">
      <input type="text" class="search">
      @forelse($materials as $material)
      <li class="dropli" type="limaterial" id="{{$material->id}}">{{$material->title}}</li>
      @empty
      @endforelse
    </div>
  </div>
</div>
<div class="dropdown" style="float:left;">
  <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float:left;margin-left:10px;background-color:#383838; color:white;">
    <i class="fa fa-cubes" aria-hidden="true"></i> Товары
  </button>
  <div class="dropdown-menu dropgoods" aria-labelledby="dropdownMenuButton3">
    <div style="padding:15px;" class="prokrutka">
      <input type="text" class="search">
      @forelse($goods as $product)
      <li class="dropli" type="liproduct" id="{{$product->id}}">{{$product->title}}</li>
      @empty
      @endforelse
    </div>
  </div>
</div>
  <div class="clearfix"></div>
  <table>
    <thead>
      <tr>
        <th>Название</th>
        <th>Длительность</th>
        <th>Мастер/Личн.продажа</th>
        <th>Кол-во</th>
        <th>Цена</th>
        <th>Скидка</th>
        <th>Стоимость</th>
      </tr>
    </thead>
    <tbody>

    </tbody>
  </table>
  </div>

  </div>
  </div>

  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
    <button type="button" class="btn btn-primary">Сохранить</button>
  </div>
  </div>
  </div>
  </div>

  <script type="text/javascript">

    $('.dropservices').on('keyup', '.search', function(){
                  _this = this;
                  $.each($(".liservice"), function() {
                      if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                         $(this).hide();
                      else
                         $(this).show();
                  });
              });
  </script>
  <script type="text/javascript">

    $('.dropmaterials').on('keyup', '.search', function(){
                  _this = this;
                  $.each($(".limaterial"), function() {
                      if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                         $(this).hide();
                      else
                         $(this).show();
                  });
              });
  </script>
  <script type="text/javascript">

    $('.dropgoods').on('keyup', '.search', function(){
                  _this = this;
                  $.each($(".liproduct"), function() {
                      if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                         $(this).hide();
                      else
                         $(this).show();
                  });
              });
  </script>
  <script type="text/javascript">
    $('.dropli').click(function(){
      var type = $(this).attr('type');
      var id = $(this).attr('id');
      $.get('/admin-salon/work/'+type+'/'+id, function(data){

      });
    });
  </script>
  <script type="text/javascript">
  $('select[name="timestamp"]').on('change',function(){
    $('input[name="timework"]').val($(this).val());
  });
    </script>
    <style media="screen">
    .masterWork{
      padding:5px;
      text-align:center;
      background-color:#383838;
      color:white;
      border-radius:0 15px 0 0;
      cursor:default;
    }
    .masterWork:hover{
      background-color:#282828 !important;
    }
    .work{
      text-align:center; border:1px solid #ddd
    }
    .work i{
      color:#45861b;
    }
    </style>
    <style media="screen">
    .prokrutka {
      height: 180px;
      width: 100%; /* ширина нашего блока */
      background: #fff; /* цвет фона, белый */
      overflow-y: scroll; /* прокрутка по вертикали */
    }
    .prokrutka li{
      cursor: default;
      margin: 8px auto;
      font-size: 16px;
      width:100%
    }
    .prokrutka li:hover{
      background-color: #e3e3e3;
    }
    .payment{
      width:75px !important;margin-left:15px!important;cursor:default;
    }
    .dopborder{
      box-shadow: 0.5px 0.5px 5px 0.5px rgb(173,58,40);
      /* border: 1px solid black !important; */
    }
  </style>
  <style>



::-webkit-scrollbar {
width: 3px; height: 3px;
}


::-webkit-scrollbar-track-piece  {
background-color: #c7c7c7;
}

::-webkit-scrollbar-thumb:vertical {
height: 50px; background-color: #666; border-radius: 3px;
}

::-webkit-scrollbar-thumb:horizontal {
height: 50px; background-color: #666; border-radius: 3px;
}

::-webkit-scrollbar-corner{
background-color: #999;
}
.emoji{
width: 18px !important; height:18px !important;
}
  </style>
