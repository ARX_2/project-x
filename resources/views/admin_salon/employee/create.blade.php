@extends('admin.layout.'.$siteType)

@section('content')
<div class="admin-content">
  @component('admin.components.breadcrumb')
  @slot('title') Добавление сотрудника @endslot
  @slot('parent') Главная @endslot
  @slot('active') Сотрудники @endslot
  @endcomponent
  <hr>

  <form class="form-horizontal" action="{{route('admin.employee.store')}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    @include('admin.employee.partials.form')

    <input type="hidden" name="created_by" value="{{Auth::id()}}">

  </form>

</div>
@endsection
