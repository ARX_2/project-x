<div class="modal fade" id="exampleModalLong{{$u->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width:50%">
            <div class="modal-content vm-goods">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="content">
                        <style>
                            .vm-header .name{font-size:16px;font-weight:600;padding-bottom:20px;border-bottom:1px solid #ddd;margin-bottom:20px}
                            .modal_edit .perent{width:100%;display:table;margin-bottom:20px}
                            .modal_edit .perent .left{float:left;width:30%;padding-right:20px}
                            .modal_edit .perent .item_name{line-height:32px;margin-bottom: 0}
                            .modal_edit .perent .center{width:70%;float:left}
                            .modal_edit .perent input{background-color:#fff;width:100%;height:32px;border:1px solid #ccc;font-size:14px;font-weight:400;border-radius:3px;color:#222;padding-left:5px}
                            .modal_edit .perent input.user_name{width:48%;float:left;margin-right:2%;padding-right:20px}
                            .modal_edit .perent input.sex{width:30%;margin-right:1%;text-align:center;float:left}
                            .radio_item{width:35%;float:left;margin-right:1%;margin-bottom: 0}
                            .radio_box{background-color:#fff;height:32px;border:1px solid #ccc;font-size:14px;text-align:center;line-height:32px;vertical-align:middle;font-weight:400;border-radius:3px;color:#222;padding-left:5px}
                            .radio_item input{display:none}
                            .radio_item input.sex:checked + .radio_box{background-color:#2196F3;color:#fff}
                            .modal_edit .perent .type{display:table;width:100%;margin-bottom:10px}
                            .modal_edit .perent .type .left{width:5%;float:left;padding:0}
                            .modal_edit .perent .type .left input{width:14px;height:14px}
                            .modal_edit .perent .type .right{width:95%;padding-left:15px;float:left}
                            .modal_edit .perent .type .right .position{font-size:14px;line-height: 16px;font-weight:600;color:#0095eb}
                            .modal_edit .perent .type .right .info{color:#888;line-height:15px;font-size:13px;margin-top:3px;font-weight:400}
                        </style>
                        <form class="" action="{{route('admin-salon.employee.update', $u)}}" method="post" enctype="multipart/form-data">
                          <input type="hidden" name="_method" value="put">
                              {{ csrf_field() }}
                        <div class="vm-goods modal_edit">
                            <div class="vm-header">
                                <div class="name">Редактирование сотрудника: {{$u->name or ''}}</div>
                            </div>
                            @if(Auth::user()->userRolle <= 2)
                            <div class="vm-body">
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Имя и фамилия:</label></div>
                                    <div class="center">
                                        <input type="text" name="name" class="user_name" placeholder="Имя" value="{{$u->name or 'Не заполнено'}}" required="">
                                        <input type="text" name="lastname" class="user_name" placeholder="Фамилия" value="{{$u->lastname or ''}}" required="">
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Пол:</label></div>
                                    <div class="center">
                                        <label class="radio_item">
                                            <input type="radio" id="user_sex" name="user_sex" class="sex" placeholder="Мужской" value="0" @if($u->sex == 0) checked @endif>
                                            <div class="radio_box">Мужской</div>
                                        </label>
                                        <label class="radio_item">
                                            <input type="radio" id="user_sex" name="user_sex" class="sex" placeholder="Женский" value="1" @if($u->sex == 1) checked @endif>
                                            <div class="radio_box">Женский</div>
                                        </label>
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Email:</label></div>
                                    <div class="center">
                                        <input type="text" name="email" class="" placeholder="email@company.ru" value="{{$u->email or 'Не заполнен'}}" required="">
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Телефон:</label></div>
                                    <div class="center">
                                        <input type="text" name="tel" class="" placeholder="+7 (___) ___-__-__" value="{{$u->tel or 'Не заполнен'}}" required="">
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Уровень полномочий:</label></div>
                                    <div class="center">
                                        <div class="row_item">
                                            @forelse($allRolles as $rol)
                                            @if($rol->id !== 1)
                                              <label class="type">
                                                  <div class="left"><input type="radio" name="userRolle" value="{{$rol->id}}" @if($u->userRolle == $rol->id) checked @endif></div>
                                                  <div class="right">
                                                      <div class="position">{{$rol->title or ''}}</div>
                                                      <div class="info">{{$rol->description or ''}}</div>
                                                  </div>
                                              </label>
                                              @endif
                                              @empty
                                              @endforelse
                                            @if($u->userRolle == 1)
                                                <label class="type">
                                                    <div class="left"><input type="radio" name="userRolle" value="1" @if($u->userRolle == 1) checked @endif></div>
                                                    <div class="right">
                                                        <div class="position">Разрабочик</div>
                                                        <div class="info">Режим бога, управление всеми сайтами</div>
                                                    </div>
                                                </label>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Должность:</label></div>
                                    <div class="center"><input type="text" name="job" class="" placeholder="Например: менеджер" value="{{$u->job or 'Не заполнено'}}"></div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Фото:</label></div>
                                    <div class="center"><input type="file" name="image"></div>
                                </div>
                                <div class="perent">
                                    <div class="left"><label for="" class="item_name">Работа:</label></div>
                                    <div class="center">
                                      <label for="" style="font-size:12px;" >Дата принятия на работу<br>
                                        <input type="date" name="datestart" value="{{date('Y-m-d', strtotime($u->datestart))}}" style="width:130px;" >
                                      </label>

                                        <label for="" style="margin-left:10px;font-size:12px;">Статус<br>
                                          <select class="form-control" name="status">
                                            <option value="1" @if($u !== null && $u->status == 1) selected @endif>Работает</option>
                                            <option value="2" @if($u !== null && $u->status == 2) selected @endif>Уволен</option>
                                          </select>
                                        </label>

                                        <label for="" style="margin-left:10px;font-size:12px;">Дата увольнения<br>
                                          <input type="date" name="datefinish" placeholder="Например: менеджер" style="width:130px;" @if($u->status == 1) readonly @endif value="{{date('Y-m-d', strtotime($u->datefinish))}}">
                                        </label>

                                        <label for="" style="margin-left:10px;font-size:12px;">Дата зарплаты с<br>
                                          <input type="date" name="payday" placeholder="Например: менеджер" style="width:130px;" value="{{date('Y-m-d', strtotime($u->payday))}}">
                                        </label>
                                      </div>
                                </div>
                                <table>
                                  <tr>
                                    <td><input type="checkbox" name="check_master" value="1" style="width:20px;float:left;" @if($u->users_salon !== null && $u->users_salon->check_master == 1) checked @endif></td>
                                    <td><span style="float:left;margin:10px 5px;"><b>Мастер</b> получает от оказанных услуг и от личных продаж</span></td>
                                  </tr>
                                </table>
                                <table  class="master_check" @if($u->users_salon !== null && $u->users_salon->check_master !== 1) style="display:none;" @endif>

                                  <tr>
                                    <td>От оказаных услуг</td>
                                    <td><input type="text" name="render_service" value="{{$u->users_salon->render_service or ''}}" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                  </tr>

                                  <tr>
                                    <td>От личных продаж товаров</td>
                                    <td><input type="text" name="sale_goods" value="{{$u->users_salon->sale_goods or ''}}" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                    <td>От личных продаж материалов</td>
                                    <td><input type="text" name="sale_materials" value="{{$u->users_salon->sale_materials or ''}}" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                    <td><input type="checkbox" name="take_discount" value="1" style="width:20px;float:left;width:20px;" @if($u->users_salon !== null && $u->users_salon->take_discount == 1) checked @endif></td>
                                    <td>Учитывать скидку при рачсете ЗП</td>
                                  </tr>

                                  <tr>
                                    <td>От личных продаж абониментов</td>
                                    <td><input type="text" name="sale_aboniments" value="{{$u->users_salon->sale_aboniments or ''}}" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                    <td>От личных продаж сертификатов</td>
                                    <td><input type="text" name="sale_sertificats" value="{{$u->users_salon->sale_sertificats or ''}}" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                    <td><input type="checkbox" name="pay_from_aboniments" value="1" style="width:20px;float:left;width:20px;" @if($u->users_salon !== null && $u->users_salon->pay_from_aboniments == 1) checked @endif></td>
                                    <td>Выплачивать за усгули, оказанные по абонименту</td>
                                  </tr>
                                </table>

                                <table>
                                  <tr>
                                    <td><input type="checkbox" name="check_administrator" value="1" style="width:20px;float:left;" @if($u->users_salon !== null && $u->users_salon->check_administrator == 1) checked @endif></td>
                                    <td><span style="float:left;margin:10px 5px;"><b>Администратор/продовец</b> получает от всех продаж и улсуг в свою смену</span></td>
                                  </tr>
                                </table>
                                <table class="check_administrator"  @if($u->users_salon !== null && $u->users_salon->check_administrator !== 1) style="display:none" @endif>

                                  <tr>
                                    <td>От всех услуг</td>
                                    <td><input type="text" name="all_services" value="{{$u->users_salon->all_services or ''}}" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                    <td>От всех продаж соляриев</td>
                                    <td><input type="text" name="all_sale_angelfish" value="{{$u->users_salon->all_sale_angelfish or ''}}" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                    <td><input type="checkbox" name="show_in_chart" value="{{$u->users_salon->show_in_chart or ''}}" style="width:20px;float:left;width:20px;" @if($u->users_salon !== null && $u->users_salon->show_in_chart == 1) checked @endif></td>
                                    <td>Показывать в расписании рабочего дня</td>
                                  </tr>

                                  <tr>
                                    <td>От всех продаж товаров</td>
                                    <td><input type="text" name="all_sale_goods" value="{{$u->users_salon->all_sale_goods or ''}}" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                    <td>От личных продаж материалов</td>
                                    <td><input type="text" name="all_sale_materials" value="{{$u->users_salon->all_sale_materials or ''}}" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                    <td><input type="checkbox" name="take_discount_admin" value="1" style="width:20px;float:left;width:20px;" @if($u->users_salon !== null && $u->users_salon->take_discount_admin == 1) checked @endif></td>
                                    <td>Учитывать скидку клиенту при расчете ЗП</td>
                                  </tr>

                                  <tr>
                                    <td>От всех продаж абонементов</td>
                                    <td><input type="text" name="pay_from_aboniments_admin" value="{{$u->users_salon->pay_from_aboniments_admin or ''}}" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                    <td>От всех продаж сертификатов</td>
                                    <td><input type="text" name="all_sale_sertificats" value="{{$u->users_salon->all_sale_sertificats or ''}}" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                    <td><input type="checkbox" name="pay_from_aboniments_admin" value="1" style="width:20px;float:left;width:20px;" @if($u->users_salon !== null && $u->users_salon->pay_from_aboniments_admin == 1) checked @endif></td>
                                    <td>Выплачивать за услуги, оказанные по абонементу</td>
                                  </tr>
                                </table>

                                <table>
                                  <tr>
                                    <td><input type="checkbox" name="check_payday" value="1" style="width:20px;float:left;" @if($u->users_salon !== null && $u->users_salon->check_payday == 1) checked @endif></td>
                                    <td><b>Получает оклад</b></td>
                                  </tr>
                                </table>

                                <table class="check_payday" @if($u->users_salon !== null && $u->users_salon->check_payday !== 1) style="display:none;" @endif>
                                  <tr>
                                    <td>
                                      <select class="" name="type_payday">
                                        <option value="1" @if($u->users_salon !== null && $u->users_salon->type_payday == 1) selected @endif>Каждую смену</option>
                                        <option value="2" @if($u->users_salon !== null && $u->users_salon->type_payday == 2) selected @endif>Один раз в месяц</option>
                                        <option value="3" @if($u->users_salon !== null && $u->users_salon->type_payday == 3) selected @endif>Два раз в месяц</option>
                                      </select>
                                    </td>
                                    <td>
                                      <input type="text" name="count1" value="{{$u->users_salon->count1 or ''}}" placeholder="р.">
                                    </td>
                                    <td  class="payday1" @if($u->users_salon !== null && $u->users_salon->type_payday == 1) style="display:none;" @endif>
                                      <select class="" name="payday1">
                                        <option value="1" @if($u->users_salon !== null && $u->users_salon->payday1 == 1) selected @endif>1 числа</option>
                                            <option value="2" @if($u->users_salon !== null && $u->users_salon->payday1 == 2) selected @endif>2 числа</option>
                                            <option value="3" @if($u->users_salon !== null && $u->users_salon->payday1 == 3) selected @endif>3 числа</option>
                                            <option value="4" @if($u->users_salon !== null && $u->users_salon->payday1 == 4) selected @endif>4 числа</option>
                                            <option value="5" @if($u->users_salon !== null && $u->users_salon->payday1 == 5) selected @endif>5 числа</option>
                                            <option value="6" @if($u->users_salon !== null && $u->users_salon->payday1 == 6) selected @endif>6 числа</option>
                                            <option value="7" @if($u->users_salon !== null && $u->users_salon->payday1 == 7) selected @endif>7 числа</option>
                                            <option value="8" @if($u->users_salon !== null && $u->users_salon->payday1 == 8) selected @endif>8 числа</option>
                                            <option value="9" @if($u->users_salon !== null && $u->users_salon->payday1 == 9) selected @endif>9 числа</option>
                                            <option value="10" @if($u->users_salon !== null && $u->users_salon->payday1 == 10) selected @endif>10 числа</option>
                                            <option value="11" @if($u->users_salon !== null && $u->users_salon->payday1 == 11) selected @endif>11 числа</option>
                                            <option value="12" @if($u->users_salon !== null && $u->users_salon->payday1 == 12) selected @endif>12 числа</option>
                                            <option value="13" @if($u->users_salon !== null && $u->users_salon->payday1 == 13) selected @endif>13 числа</option>
                                            <option value="14" @if($u->users_salon !== null && $u->users_salon->payday1 == 14) selected @endif>14 числа</option>
                                            <option value="15" @if($u->users_salon !== null && $u->users_salon->payday1 == 15) selected @endif>15 числа</option>
                                            <option value="16" @if($u->users_salon !== null && $u->users_salon->payday1 == 16) selected @endif>16 числа</option>
                                            <option value="17" @if($u->users_salon !== null && $u->users_salon->payday1 == 17) selected @endif>17 числа</option>
                                            <option value="18" @if($u->users_salon !== null && $u->users_salon->payday1 == 18) selected @endif>18 числа</option>
                                            <option value="19" @if($u->users_salon !== null && $u->users_salon->payday1 == 19) selected @endif>19 числа</option>
                                            <option value="20" @if($u->users_salon !== null && $u->users_salon->payday1 == 20) selected @endif>20 числа</option>
                                            <option value="21" @if($u->users_salon !== null && $u->users_salon->payday1 == 21) selected @endif>21 числа</option>
                                            <option value="22" @if($u->users_salon !== null && $u->users_salon->payday1 == 22) selected @endif>22 числа</option>
                                            <option value="23" @if($u->users_salon !== null && $u->users_salon->payday1 == 23) selected @endif>23 числа</option>
                                            <option value="24" @if($u->users_salon !== null && $u->users_salon->payday1 == 24) selected @endif>24 числа</option>
                                            <option value="25" @if($u->users_salon !== null && $u->users_salon->payday1 == 25) selected @endif>25 числа</option>
                                            <option value="26" @if($u->users_salon !== null && $u->users_salon->payday1 == 26) selected @endif>26 числа</option>
                                            <option value="27" @if($u->users_salon !== null && $u->users_salon->payday1 == 27) selected @endif>27 числа</option>
                                            <option value="28" @if($u->users_salon !== null && $u->users_salon->payday1 == 28) selected @endif>28 числа</option>
                                            <option value="29" @if($u->users_salon !== null && $u->users_salon->payday1 == 29) selected @endif>29 числа</option>
                                            <option value="30" @if($u->users_salon !== null && $u->users_salon->payday1 == 30) selected @endif>30 или последний день месяца</option>
                                            <option value="31" @if($u->users_salon !== null && $u->users_salon->payday1 == 31) selected @endif>31 или последний день месяца</option>
                                      </select>
                                    </td>
                                    <tr>
                                      <td></td>
                                      <td class="count2" @if($u->users_salon !== null && $u->users_salon->type_payday !== 3) style="display:none;" @endif>
                                        <input type="text" name="count2" value="{{$u->users_salon->count2 or ''}}" placeholder="р.">
                                      </td>
                                      <td class="payday2" @if($u->users_salon !== null && $u->users_salon->type_payday !== 3) style="display:none;" @endif>
                                        <select name="payday2">
                                          <option value="1">1 числа</option>
                                          <option value="2" @if($u->users_salon !== null && $u->users_salon->payday2 == 2) selected @endif>2 числа</option>
                                          <option value="3" @if($u->users_salon !== null && $u->users_salon->payday2 == 3) selected @endif>3 числа</option>
                                          <option value="4" @if($u->users_salon !== null && $u->users_salon->payday2 == 4) selected @endif>4 числа</option>
                                          <option value="5" @if($u->users_salon !== null && $u->users_salon->payday2 == 5) selected @endif>5 числа</option>
                                          <option value="6" @if($u->users_salon !== null && $u->users_salon->payday2 == 6) selected @endif>6 числа</option>
                                          <option value="7" @if($u->users_salon !== null && $u->users_salon->payday2 == 7) selected @endif>7 числа</option>
                                          <option value="8" @if($u->users_salon !== null && $u->users_salon->payday2 == 8) selected @endif>8 числа</option>
                                          <option value="9" @if($u->users_salon !== null && $u->users_salon->payday2 == 9) selected @endif>9 числа</option>
                                          <option value="10" @if($u->users_salon !== null && $u->users_salon->payday2 == 10) selected @endif>10 числа</option>
                                          <option value="11" @if($u->users_salon !== null && $u->users_salon->payday2 == 11) selected @endif>11 числа</option>
                                          <option value="12" @if($u->users_salon !== null && $u->users_salon->payday2 == 12) selected @endif>12 числа</option>
                                          <option value="13" @if($u->users_salon !== null && $u->users_salon->payday2 == 13) selected @endif>13 числа</option>
                                          <option value="14" @if($u->users_salon !== null && $u->users_salon->payday2 == 14) selected @endif>14 числа</option>
                                          <option value="15" @if($u->users_salon !== null && $u->users_salon->payday2 == 15) selected @endif>15 числа</option>
                                          <option value="16" @if($u->users_salon !== null && $u->users_salon->payday2 == 16) selected @endif>16 числа</option>
                                          <option value="17" @if($u->users_salon !== null && $u->users_salon->payday2 == 17) selected @endif>17 числа</option>
                                          <option value="18" @if($u->users_salon !== null && $u->users_salon->payday2 == 18) selected @endif>18 числа</option>
                                          <option value="19" @if($u->users_salon !== null && $u->users_salon->payday2 == 19) selected @endif>19 числа</option>
                                          <option value="20" @if($u->users_salon !== null && $u->users_salon->payday2 == 20) selected @endif>20 числа</option>
                                          <option value="21" @if($u->users_salon !== null && $u->users_salon->payday2 == 21) selected @endif>21 числа</option>
                                          <option value="22" @if($u->users_salon !== null && $u->users_salon->payday2 == 22) selected @endif>22 числа</option>
                                          <option value="23" @if($u->users_salon !== null && $u->users_salon->payday2 == 23) selected @endif>23 числа</option>
                                          <option value="24" @if($u->users_salon !== null && $u->users_salon->payday2 == 24) selected @endif>24 числа</option>
                                          <option value="25" @if($u->users_salon !== null && $u->users_salon->payday2 == 25) selected @endif>25 числа</option>
                                          <option value="26" @if($u->users_salon !== null && $u->users_salon->payday2 == 26) selected @endif>26 числа</option>
                                          <option value="27" @if($u->users_salon !== null && $u->users_salon->payday2 == 27) selected @endif>27 числа</option>
                                          <option value="28" @if($u->users_salon !== null && $u->users_salon->payday2 == 28) selected @endif>28 числа</option>
                                          <option value="29" @if($u->users_salon !== null && $u->users_salon->payday2 == 29) selected @endif>29 числа</option>
                                          <option value="30" @if($u->users_salon !== null && $u->users_salon->payday2 == 30) selected @endif>30 или последний день месяца</option>
                                          <option value="31" @if($u->users_salon !== null && $u->users_salon->payday2 == 31) selected @endif>31 или последний день месяца</option>
                                        </select>
                                      </td>
                                    </tr>
                                  </tr>
                                </table>


                                <div class="perent" style="margin-bottom: 0">
                                    <div class="left"><label for="" class="item_name"></label></div>
                                    <div class="center">
                                        <input class="button-save" type="submit" value="Обновить" style="width: 115px;background-color: #269ffd;color: #fff;">
                                    </div>
                                </div>
                            </div>
                            @else
                            Доступ закрыт
                            @endif
                        </div>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
