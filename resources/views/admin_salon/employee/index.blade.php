@include('admin.index')
<script type="text/javascript">
  $('#admin-cont').remove();
</script>
<div class="admin">


    <div class="admin-content">
        <div class="row">
            <div class="col-lg-4">
                <h2>Управление сотрудниками</h2>
            </div>
            <div class="col-lg-8">
                <ol class="breadcrumb">
                    <li><a href="{{route('admin.index')}}">Главная</a></li>
                    <li class="active">Сотрудники</li>
                </ol>
            </div>
        </div>
        <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="narrow_title" style="padding: 10px 0 20px;width: 50%;margin: 0 auto 20px;">
                        <div class="left">
                            <h2>Сотрудники</h2>
                        </div>
                        <div class="right">
                            <span class="button-edit"><span data-toggle="modal" data-target="#adduser">Добавить</span></span>
                        </div>
                    </div>
                    <style>
                        .users{width:50%;margin:10px auto}
                        .users .user-lvl{padding-bottom:20px;border-bottom:1px solid #ddd;margin-bottom:20px}
                        .users .user-lvl .title{font-size: 16px;font-weight: 600;margin-bottom: 10px;}
                        .users .item{display:inline-flex;padding:10px 0;width:100%;position:relative;}
                        .users .item:hover{background:#f7f7f7}
                        .users .image{float:left;width:60px;height:60px;margin-right:15px}
                        .users .image img{width:100%;height:100%;object-fit:cover;border-radius:50%}
                        .users .info {position: relative}
                        .users .info .name{font-size:14px;line-height: 16px;font-weight:600;color:#333}
                        .users .position{color:#999;font-size:14px;line-height: 16px;}
                        .users .more{font-size:14px;line-height: 16px;color: #2986c7;position: absolute;bottom: 0;left: 0}
                        .users .personal{color:#666;font-size:12px;line-height: 14px;margin-top: 5px}
                        .users .personal span{color:#888;font-weight:600}
                        .users .edit:before{position:absolute;right:15px;top:5px;font-family:FontAwesome;content:"\f141"}
                    </style>
                    <div class="users">
                        <form name="home" action="" method="POST">
                            <!-- Каждый уроверь разделяется через user-lvl
                            Последовательность такая (сверху вниз):
                            1. Менеджеры
                            2. Редакторы
                            3. Админинстраторы
                            4. Владелец
                            5. Разработчик
                            -->
                            @forelse($userRolles as $userRolle)
                            <div class="user-lvl">
                                <div class="user_list">
                                @forelse($userRolle->users as $u)
                                    <div class="item">
                                        <div class="image">
                                            <img src="{{asset('storage/app')}}/{{$u->image or '/public/no-image-80x80.png'}}" alt="">
                                        </div>
                                        <div class="info">
                                            <div class="basic" style="position: relative;height: 60px">
                                                <div class="name">{{$u->name or 'Имя Фамилия'}}</div>
                                                <div class="position">@if($u->userRolle == 1) Разработчик @elseif($u->userRolle == 2) Администратор @elseif($u->userRolle == 3) Редактор @elseif($u->userRolle == 4) Менеджер @elseif($u->userRolle == 5) Мастер @endif</div>
                                                <a href="javascript:void(0);" class="more collapsed" id="user{{$u->id}}" data-toggle="collapse" data-target="#c_user{{$u->id}}" aria-expanded="false" aria-controls="collapseTwo">
                                                    Информация
                                                </a>
                                            </div>
                                            <div id="c_user{{$u->id}}" class="advanced collapse" aria-labelledby="user{{$u->id}}" data-parent="#accordion">
                                                <div class="personal">Телефон: <span>{{$u->tel}}</span></div>
                                                <div class="personal">Почта: <span>{{$u->email}}</span></div>
                                                <div class="personal">Должность: <span>{{$u->job}}</span></div>
                                            </div>
                                        </div>
                                        <a href="#" class="dropdown-toggle edit" data-toggle="dropdown"></a>
                                        <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                                            <li><a href="" data-toggle="modal" data-target="#exampleModalLong{{$u->id}}">Редактировать</a></li>
                                            @if($u->userRolle <= 2)<li><a href="{{route('admin.employee.delete', ['id' => $u->id])}}">Удалить</a></li>@endif
                                        </ul>
                                    </div>
                                    @include('admin_salon.employee.partials.modal', ['u' => $u, 'allRolles' => $allRolles])
                                    @empty
                                @endforelse
                                </div>
                            </div>
                            @empty
                            @endforelse




                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <style media="screen">
      td{
        padding: 5px;
      }
    </style>

    <div class="modal fade" id="adduser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document" style="width:50%">
                <div class="modal-content vm-goods">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="modal-body">
                        <div class="content">

                            <form class="" action="{{route('admin-salon.employee.store')}}" method="post" enctype="multipart/form-data">
                                  {{ csrf_field() }}
                            <div class="vm-goods modal_edit">
                                <div class="vm-header">
                                    <div class="name">Добавление сотрудника</div>
                                </div>

                                @if(Auth::user()->userRolle <= 3)
                                <div class="vm-body">
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Имя и фамилия:</label></div>
                                        <div class="center">
                                            <input type="text" name="name" class="user_name" placeholder="Имя" value="" required="">
                                            <input type="text" name="lastname" class="user_name" placeholder="Фамилия" value="" required="">
                                        </div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Пол:</label></div>
                                        <div class="center">
                                            <label class="radio_item">
                                                <input type="radio" id="user_sex" name="user_sex" class="sex" placeholder="Мужской" value="0" required="">
                                                <div class="radio_box">Мужской</div>
                                            </label>
                                            <label class="radio_item">
                                                <input type="radio" id="user_sex" name="user_sex" class="sex" placeholder="Женский" value="1" required="">
                                                <div class="radio_box">Женский</div>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Email:</label></div>
                                        <div class="center">
                                            <input type="text" name="email" class="" placeholder="email@company.ru" value="" required="">
                                        </div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Телефон:</label></div>
                                        <div class="center">
                                            <input type="text" name="tel" class="" placeholder="+7 (___) ___-__-__" value="" required="">
                                        </div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Пароль:</label></div>
                                        <div class="center">
                                            <input type="text" name="password" class="" placeholder="" value="" required="">
                                        </div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Уровень полномочий:</label></div>
                                        <div class="center">
                                            <div class="row_item">
                                              @forelse($allRolles as $rolle)
                                              @if($rolle->id !== 1)
                                                <label class="type">
                                                    <div class="left"><input type="radio" name="userRolle" value="{{$rolle->id}}"></div>
                                                    <div class="right">
                                                        <div class="position">{{$rolle->title or ''}}</div>
                                                        <div class="info">{{$rolle->description or ''}}</div>
                                                    </div>
                                                </label>
                                                @endif
                                                @empty
                                                @endforelse
                                                @if(Auth::user()->userRolle == 1)
                                                <label class="type">
                                                    <div class="left"><input type="radio" name="userRolle" value="1"></div>
                                                    <div class="right">
                                                        <div class="position">Разрабочик</div>
                                                        <div class="info">Режим бога, управление всеми сайтами</div>
                                                    </div>
                                                </label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Должность:</label></div>
                                        <div class="center"><input type="text" name="job" class="" placeholder="Например: менеджер" value=""></div>
                                    </div>
                                    <div class="perent">
                                        <div class="left"><label for="" class="item_name">Фото:</label></div>
                                        <div class="center"><input type="file" name="image" ></div>
                                    </div>

                                    <table>
                                      <tr>
                                        <td><input type="checkbox" name="check_master" value="1" style="width:20px;float:left;" ></td>
                                        <td><span style="float:left;margin:10px 5px;"><b>Мастер</b> получает от оказанных услуг и от личных продаж</span></td>
                                      </tr>
                                    </table>
                                    <table  class="master_check" @ style="display:none;" >

                                      <tr>
                                        <td>От оказаных услуг</td>
                                        <td><input type="text" name="render_service" value="" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                      </tr>

                                      <tr>
                                        <td>От личных продаж товаров</td>
                                        <td><input type="text" name="sale_goods" value="" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                        <td>От личных продаж материалов</td>
                                        <td><input type="text" name="sale_materials" value="" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                        <td><input type="checkbox" name="take_discount" value="1" style="width:20px;float:left;width:20px;" ></td>
                                        <td>Учитывать скидку при рачсете ЗП</td>
                                      </tr>

                                      <tr>
                                        <td>От личных продаж абониментов</td>
                                        <td><input type="text" name="sale_aboniments" value="" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                        <td>От личных продаж сертификатов</td>
                                        <td><input type="text" name="sale_sertificats" value="" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                        <td><input type="checkbox" name="pay_from_aboniments" value="1" style="width:20px;float:left;width:20px;" ></td>
                                        <td>Выплачивать за усгули, оказанные по абонименту</td>
                                      </tr>
                                    </table>

                                    <table>
                                      <tr>
                                        <td><input type="checkbox" name="check_administrator" value="1" style="width:20px;float:left;"></td>
                                        <td><span style="float:left;margin:10px 5px;"><b>Администратор/продовец</b> получает от всех продаж и улсуг в свою смену</span></td>
                                      </tr>
                                    </table>
                                    <table class="check_administrator"  style="display:none" >

                                      <tr>
                                        <td>От всех услуг</td>
                                        <td><input type="text" name="all_services" value="" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                        <td>От всех продаж соляриев</td>
                                        <td><input type="text" name="all_sale_angelfish" value="" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                        <td><input type="checkbox" name="show_in_chart" value="" style="width:20px;float:left;width:20px;"></td>
                                        <td>Показывать в расписании рабочего дня</td>
                                      </tr>

                                      <tr>
                                        <td>От всех продаж товаров</td>
                                        <td><input type="text" name="all_sale_goods" value="" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                        <td>От личных продаж материалов</td>
                                        <td><input type="text" name="all_sale_materials" value="" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                        <td><input type="checkbox" name="take_discount_admin" value="1" style="width:20px;float:left;width:20px;" ></td>
                                        <td>Учитывать скидку клиенту при расчете ЗП</td>
                                      </tr>

                                      <tr>
                                        <td>От всех продаж абонементов</td>
                                        <td><input type="text" name="pay_from_aboniments_admin" value="" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                        <td>От всех продаж сертификатов</td>
                                        <td><input type="text" name="all_sale_sertificats" value="" placeholder="%" style="width:20px;float:left;width:50px;"></td>
                                        <td><input type="checkbox" name="pay_from_aboniments_admin" value="1" style="width:20px;float:left;width:20px;" ></td>
                                        <td>Выплачивать за услуги, оказанные по абонементу</td>
                                      </tr>
                                    </table>

                                    <table>
                                      <tr>
                                        <td><input type="checkbox" name="check_payday" value="1" style="width:20px;float:left;" ></td>
                                        <td><b>Получает оклад</b></td>
                                      </tr>
                                    </table>

                                    <table class="check_payday" style="display:none;" >
                                      <tr>
                                        <td>
                                          <select class="" name="type_payday">
                                            <option value="1" >Каждую смену</option>
                                            <option value="2" >Один раз в месяц</option>
                                            <option value="3">Два раз в месяц</option>
                                          </select>
                                        </td>
                                        <td>
                                          <input type="text" name="count1" value="" placeholder="р.">
                                        </td>
                                        <td  class="payday1" style="display:none;">
                                          <select class="" name="payday1">
                                            <option value="1">1 числа</option>
                                            <option value="2">2 числа</option>
                                            <option value="3">3 числа</option>
                                            <option value="4">4 числа</option>
                                            <option value="5">5 числа</option>
                                            <option value="6">6 числа</option>
                                            <option value="7">7 числа</option>
                                            <option value="8">8 числа</option>
                                            <option value="9">9 числа</option>
                                            <option value="10">10 числа</option>
                                            <option value="11">11 числа</option>
                                            <option value="12">12 числа</option>
                                            <option value="13">13 числа</option>
                                            <option value="14">14 числа</option>
                                            <option value="15">15 числа</option>
                                            <option value="16">16 числа</option>
                                            <option value="17">17 числа</option>
                                            <option value="18">18 числа</option>
                                            <option value="19">19 числа</option>
                                            <option value="20">20 числа</option>
                                            <option value="21">21 числа</option>
                                            <option value="22">22 числа</option>
                                            <option value="23">23 числа</option>
                                            <option value="24">24 числа</option>
                                            <option value="25">25 числа</option>
                                            <option value="26">26 числа</option>
                                            <option value="27">27 числа</option>
                                            <option value="28">28 числа</option>
                                            <option value="29">29 числа</option>
                                            <option value="30">30 или последний день месяца</option>
                                            <option value="31">31 или последний день месяца</option>
                                          </select>
                                        </td>
                                        <tr>
                                          <td></td>
                                          <td class="count2" style="display:none;" >
                                            <input type="text" name="count2" value="" placeholder="р.">
                                          </td>
                                          <td class="payday2" style="display:none;" >
                                            <select name="payday2">
                                            <option value="1">1 числа</option>
                                            <option value="2">2 числа</option>
                                            <option value="3">3 числа</option>
                                            <option value="4">4 числа</option>
                                            <option value="5">5 числа</option>
                                            <option value="6">6 числа</option>
                                            <option value="7">7 числа</option>
                                            <option value="8">8 числа</option>
                                            <option value="9">9 числа</option>
                                            <option value="10">10 числа</option>
                                            <option value="11">11 числа</option>
                                            <option value="12">12 числа</option>
                                            <option value="13">13 числа</option>
                                            <option value="14">14 числа</option>
                                            <option value="15">15 числа</option>
                                            <option value="16">16 числа</option>
                                            <option value="17">17 числа</option>
                                            <option value="18">18 числа</option>
                                            <option value="19">19 числа</option>
                                            <option value="20">20 числа</option>
                                            <option value="21">21 числа</option>
                                            <option value="22">22 числа</option>
                                            <option value="23">23 числа</option>
                                            <option value="24">24 числа</option>
                                            <option value="25">25 числа</option>
                                            <option value="26">26 числа</option>
                                            <option value="27">27 числа</option>
                                            <option value="28">28 числа</option>
                                            <option value="29">29 числа</option>
                                            <option value="30">30 или последний день месяца</option>
                                            <option value="31">31 или последний день месяца</option>
                                            </select>
                                          </td>
                                        </tr>
                                      </tr>
                                    </table>

                                    <div class="perent" style="margin-bottom: 0">
                                        <div class="left"><label for="" class="item_name"></label></div>
                                        <div class="center">
                                            <input class="button-save" type="submit" value="Добавить сотрудника" style="width: 175px;background-color: #269ffd;color: #fff;">
                                        </div>
                                    </div>
                                </div>
                                @else
                                Доступ закрыт
                                @endif
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
          $('input[name="check_master"]').on('click', function(){
            if($(this).is(':checked')){
              $(this).closest('.vm-body').find('.master_check').show();
            }
            else{
              $(this).closest('.vm-body').find('.master_check').hide();
            }
          });

          $('input[name="check_administrator"]').on('click', function(){
            if($(this).is(':checked')){
              $(this).closest('.vm-body').find('.check_administrator').show();
            }
            else{
              $(this).closest('.vm-body').find('.check_administrator').hide();
            }
          });

          $('input[name="check_payday"]').on('click', function(){
            if($(this).is(':checked')){
              $(this).closest('.vm-body').find('.check_payday').show();
            }
            else{
              $(this).closest('.vm-body').find('.check_payday').hide();
            }
          });
          $('select[name="type_payday"]').on('change', function(){
            if($(this).val() == 1){
              $(this).closest('.check_payday').find('.payday1').hide();
              $(this).closest('.check_payday').find('.count2').hide();
              $(this).closest('.check_payday').find('.payday2').hide();
            }
            else if($(this).val() == 2){
              $(this).closest('.check_payday').find('.payday1').show();
              $(this).closest('.check_payday').find('.count2').hide();
              $(this).closest('.check_payday').find('.payday2').hide();
            }
            else if($(this).val() == 3){
              $(this).closest('.check_payday').find('.payday1').show();
              $(this).closest('.check_payday').find('.count2').show();
              $(this).closest('.check_payday').find('.payday2').show();
            }
          });
          $('select[name="status"]').on('change', function(){
            console.log($(this).val());
            if($(this).val() == 1){
              $(this).closest('.perent').find('input[name="datefinish"]').prop('readonly', true);
              $(this).closest('.perent').find('input[name="datefinish"]').val('');
            }
            else{
              $(this).closest('.perent').find('input[name="datefinish"]').prop('readonly', false);
            }
          });
        </script>
