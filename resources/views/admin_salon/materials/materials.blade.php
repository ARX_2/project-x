
@forelse($product as $g)
<?php //dd($g); ?>
<div class="item" id="{{$g->id}}">
    <div class="image">
        <img src="{{asset('storage/app')}}/{{$g->images->images or '/public/no-image-80x80.png'}}" alt="">
    </div>
    <div class="info_goods">
        <div class="block">
            <div class="name"><a href=""data-toggle="modal" data-target="#exampleModalLong">{{$g->title}}</a></div>
            <div class="price" style="width:20%">Цена: <span>{{$g->coast}}</span> руб. <b>{{$g->type_costs->short_title or ''}}</b>.<br>
              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#charge">-</button>
              <button type="button" class="btn btn-light" data-toggle="modal" data-target="#inventry">{{$g->counts or ''}}</button>
              <button type="button" class="btn btn-success"  data-toggle="modal" data-target="#exampleModal">+</button>
              <input type="hidden"  class="warningCounts" id="{{$g->warningCount or ''}}">
            </div>

        </div>
        <div class="additionally">
            <div class="p_category">Раздел: <span>{{$g->categories->title or 'не выбран'}}</span></div>
            <div class="date">Добавлено: <span>{{Resize::date($g->created_at)}}</span><span> | </span> Обновлено: <span>{{Resize::date($g->updated_at)}}</span></div>
        </div>
    </div>
    <div class="status" style="width:15%">
      @if($g->Goods_salon !== null)
       <b>Вознаграждение</b> мастеру/админу<br>

       {{$g->Goods_salon->master_sale_count or ''}}
       @if($g->Goods_salon !== null && $g->Goods_salon->master_sale_type == 2)
       %
       @elseif($g->Goods_salon !== null && $g->Goods_salon->master_sale_type == 3)
       р.
       @elseif($g->Goods_salon !== null && $g->Goods_salon->master_sale_type == 1)
       Стандартная оплата
       @else
       Не выплачивать
       @endif
        | {{$g->Goods_salon->administrator_sale_count or ''}}
        @if($g->Goods_salon !== null && $g->Goods_salon->administrator_sale_type == 2)
        %
        @elseif($g->Goods_salon !== null &&$g->Goods_salon->administrator_sale_type == 3)
        р.
        @elseif($g->Goods_salon !== null &&$g->Goods_salon->administrator_sale_type == 3)
        Стандартная оплата
        @else
        Не выплачивать
        @endif
        @endif
    </div>
    <div class="status" style="width:10%">
        <span class="sign eae @if($g->published == 1)active @endif"><a href="/admin-salon/goods/published/{{$g->id}}/{{$g->published}}" class="link_published"></a></span>
        <span class="sign search @if($g->checkseo == 1)active @endif"></span>
        <span class="sign star @if($g->main == 1)activeStar @endif"><a href="{{route('admin.goods.update.id', ['main',$g->id,(int)$g->main])}}"></a></span>
    </div>
    <div class="action">
        <a href="#" class="dropdown-toggle edit" data-toggle="dropdown" ></a>
        <ul class="dropdown-menu dropdown-item-edit" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
            <!-- <li><a href="{{route('admin.goods.update.id', ['published',$g->id,$g->published])}}" class="link_published">@if($g->published == 1)Скрыть @else Показать @endif</a></li> -->
            {{--<li><a href="{{route('admin.goods.destroy.goods', ['id'=>$g->id, 'deleted' => $g->deleted])}}">@if($g->deleted == 0 || $g->deleted == null)Удалить @else Восстановить @endif</a></li>--}}
            <li><a href="{{route('admin-salon.materials.edit', [$g->id])}}?id={{$g->id}}" class="link_edit">Редактировать</a></li>
            {{--<li><a href="{{route('admin.goods.update.id', ['main',$g->id,(int)$g->main])}}">@if($g->main == 0)На главную @else Снять с главной @endif</a></li>--}}
        </ul>
    </div>
</div>

@empty
@endforelse

@include('admin_salon.goods.newCount')
@include('admin_salon.goods.inventory')
@include('admin_salon.goods.charge')

<script type="text/javascript">

  $('.eae').click(function(){
    var link = $(this).find('a').attr('href');
    $(this).parent('.status').parent('.item').remove();
    var count_active = $('#count_active').html();
    count_active = Number.parseInt(count_active);


    var count_hidden = $('#count_hidden').html();
    count_hidden = Number.parseInt(count_hidden);
    var check = $('#link_active').hasClass('active');
    if(check == true){
      count_active = count_active-1;
      count_hidden = count_hidden+1;
    }
    else{
      count_active = count_active+1;
      count_hidden = count_hidden-1;
    }

    $('#count_active').text(count_active);
    $('#count_hidden').text(count_hidden);

    $.get(link, function(data){});

  });

  $('.star').click(function(){
    var link = $(this).find('a').attr('href');
    var check =  $(this).hasClass('activeStar');
    if(check == true){
      $(this).removeClass('activeStar');
    }
    else{
      $(this).addClass('activeStar');
    }
    $.get(link, function(data){});

  });
</script>
<script type="text/javascript">
  $('.btn-primary').click(function(){
    $('input').val('');
    $('select').val('');
    $('textarea').val('');
    $('#primeCost1').find('b').text('-');
    $('#primeCost2').find('b').text('-');
  });

</script>
<div class="modal fade" id="supplier" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Добавление поставщика</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<div>
  <label for=""><span class="label-supplier">Наименование:</span> <input type="text" name="supplierTitle" value="" class="form-supplier"></label><br>
  <label for=""><span class="label-supplier">Телефон:</span> <input type="text" name="supplierTel" value="" class="form-supplier"></label><br>
  <label for=""><span class="label-supplier">E-mail:</span> <input type="text" name="supplierEmail" value="" class="form-supplier"></label><br>
  <label for=""><span class="label-supplier">Контакт:</span> <input type="text" name="supplierName" value="" class="form-supplier"></label><br>
  <label for=""><span class="label-supplier">Адрес:</span> <input type="text" name="supplierAddress" value="" class="form-supplier"></label><br>
  <label for=""><span class="label-supplier">ИНН:</span> <input type="text" name="supplierINN" value="" class="form-supplier"></label><br>
  <label for=""><span class="label-supplier">КПП:</span> <input type="text" name="supplierKPP" value="" class="form-supplier"></label><br>
  <label for=""><span class="label-supplier">Комментарий:</span> <textarea name="supplierComment" rows="8" cols="80" class="form-supplier"></textarea></label>

</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
<button type="button" class="btn btn-primary" id="newSupplier">Сохранить</button>
</div>
</div>
</div>
</div>
<script type="text/javascript">
$('#newSupplier').click(function(){
  var title = $('input[name="supplierTitle"]').val();
  var tel = $('input[name="supplierTel"]').val();
  var email = $('input[name="supplierEmail"]').val();
  var name = $('input[name="supplierName"]').val();
  var address = $('input[name="supplierAddress"]').val();
  var inn = $('input[name="supplierINN"]').val();
  var kpp = $('input[name="supplierKPP"]').val();
  var comment = $('textarea[name="supplierComment"]').val();

  $.post('admin-salon/supplier/create', {title:title, tel:tel, email:email, name:name, inn:inn, kpp:kpp, comment:comment}, function(data){
    $('#suppliers').html(data);
    alert('Поставщик успешно добавлен!');
  });
});
</script>
