


      <div class="row">
        <div class="col-lg-4">
            <h2>Управление услугами</h2>
        </div>
        <div class="col-lg-8">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.index')}}">Главная</a></li>
                <li class="active">{{$company->info->section0 or 'Товары'}}</li>
            </ol>
        </div>
    </div>
    <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        {{--<div class="item">
                            <div class="name">
                                <a href=""></a>
                            </div>
                        </div>--}}

                    </div>
                </div>
            </div>
            <div class="narrow">
                <div class="box">
                    <div class="cp_edit">
                        <div class="narrow_title">
                            @if($product == null)
                            <h2>Добавление услуги</h2>
                            @else
                            <h2>Редактирование услуги: <span id="edit-title">{{$product->title}}</span></h2>
                            @endif

                        </div>
                        @if($product == null)
                        <form class="" action="{{route('admin.services.store')}}" method="post" enctype="multipart/form-data">
                        @else
                        <form  action="{{route('admin.service.update.id', ['update',$product->id,0])}}" method="post"  enctype="multipart/form-data">
                          <input type="hidden" name="_method" value="put">
                          <input type="hidden" name="id" value="{{$product->id}}">
                        @endif

                        {{ csrf_field() }}
                        <input type="hidden" name="slug" class="form-control" placeholder="Автоматическая генерация" value="{{$goods->slug or ""}}" readonly>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Выберите категорию:</label>
                            </div>
                            <div class="center">
                              <select class="form-control" name="subcategory" required>
                                  <option disabled selected>Выберите категорию</option>
                                  @forelse($categories as $c)
                                  <option value="{{$c->id or ''}}" @if($product !== null && $product->subcategory !== null && $c->id == $product->subcategory->id)selected="" @endif style="font-weight:600;">{{$c->title or ''}}</option>
                                  @forelse($c->subcategoriesAll as $sub)
                                  @if($product !== null && $product->subcategory !== null)
                                  <option value="{{$sub->id}}" @if($sub->id == $product->subcategory->id)selected="" @endif >&nbsp;&nbsp;&nbsp;{{$sub->title}}</option>
                                  @else
                                  <option value="{{$sub->id}}"  >&nbsp;&nbsp;&nbsp;{{$sub->title}}</option>
                                  @endif
                                  @empty
                                  @endforelse
                                  @empty
                                  <option value="">Нет категорий</option>
                                  @endforelse
                              </select>

                            </div>
                        </div>
                        <script type="text/javascript">
                        $('select[name="subcategory"]').on('change',function(){
                          $.get('/admin-salon/service/category/{{$product->id}}/'+$(this).val(), function(data){});
                        });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="seo_name">SEO</label>
                            </div>
                            <div class="center">
                                <label class="switch">
                                    <input type="checkbox" id="watch" name="checkseo" value="1" @if($product !== null)@if($product->checkseo == 1)checked @endif @endif>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                        <div class="seo_group" id="switch_seo" style="display:none">
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name" style="padding-top: 4px">Название на странице товара или услуги:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seoname" class="" placeholder="Название на странице категории" value="{{$product->seoname or ''}}" >
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('input[name="seoname"]')
                            .on({
                              blur: function() {
                                $.get('/admin-salon/services/seoname/{{$product->id}}/'+this.value);
                              }
                            });
                            </script>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Title:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seotitle" class="" placeholder="" value="{{$product->seotitle or ''}}" >
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('input[name="seotitle"]')
                            .on({
                              blur: function() {
                                $.get('/admin-salon/services/seotitle/{{$product->id}}/'+this.value);
                              }
                            });
                            </script>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Description:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seodescription" class="" placeholder="" value="{{$product->seodescription or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('input[name="seodescription"]')
                            .on({
                              blur: function() {
                                $.get('/admin-salon/services/seodescription/{{$product->id}}/'+this.value);
                              }
                            });
                            </script>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Keywords:</label>
                                </div>
                                <div class="center">
                                  <input type="text" name="seokeywords" class="" placeholder="" value="{{$product->seokeywords or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('input[name="seokeywords"]')
                            .on({
                              blur: function() {
                                $.get('/admin-salon/services/seokeywords/{{$product->id}}/'+this.value);
                              }
                            });
                            </script>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Название товара или услуги:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="title" class="" placeholder="Введите название товара или услуги" value="{{$product->title or ''}}" required="">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('input[name="title"]').on('input',function(){
                          var title = $(this).val();
                          $('#edit-title').text(title);
                        });
                        $('input[name="title"]')
                        .on({
                          blur: function() {
                            $.get('/admin-salon/services/title/{{$product->id}}/'+this.value);
                          }
                        });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Описание:</label>
                            </div>
                            <div class="center">
                                <textarea rows="10" name="short_description" id="short_description">{!! $product->short_description or '' !!}</textarea>
                                <script>
                                    CKEDITOR.replace('short_description');
                                </script>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Стоимость:</label>
                            </div>
                            <style>

                            </style>
                            <div class="center price">
                                <input type="text" name="coast" class=" a-price" placeholder="0" value="{{$product->coast or ''}}" data-mask="000 000 000" data-mask-reverse="true" data-mask-maxlength="false">
                                <div class="input-part">
                                        <span>
                                            <span class="number-format-font">₽</span>
                                        </span>
                                </div>

                            </div>
                        </div>
                        <script type="text/javascript">
                        $('input[name="coast"]')
                        .on({
                          blur: function() {
                            $.get('/admin-salon/services/coast/{{$product->id}}/'+this.value);
                          }
                        });
                        </script>

                        <div class="materieals">
                          <?php $i = 1;?>
                          @forelse($product->service_goods as $sg)
                          <div class="perent mat" >
                              <div class="left">
                                  <label for="" class="item_name">Материал:</label>
                              </div>
                              <style>

                              </style>
                              <div class="center price">
                                <div class="dropdown">
                                  <button class="btn btn-outline-secondary dropdown-toggle" style="float:left;" type="button" id="dropdownMenuButton{{$i}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{$sg->products->title or ''}}
                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$i}}">
                                    <div style="padding:15px;" class="prokrutka">
                                      <input type="text" class="search">
                                      @forelse($materials as $material)
                                      <li class="material" id="{{$material->id}}">{{$material->title}}</li>
                                      <?php $i++; ?>
                                      @empty
                                      @endforelse
                                    </div>
                                  </div>
                                  <?php  ?>
                                  <div class="dops" style="float:right; margin-right:270px;">
                                    <div class="input-part payment @if($sg->payment == 1) dopborder @endif">
                                          <span id="{{$sg->id}}">Доп. Опл.</span>
                                        </div>
                                        <?php //dd($sg); ?>
                                    <div class="input-part" style="margin-left:10px; width:100px;">
                                              <input type="text" name="counts" placeholder="количество"  class="form-control" value="{{$sg->counts}}">
                                    </div>

                                    <div class="input-part" id="addsupplier" data-toggle="modal" data-target="#supplier">
                                        <i class="fa fa-trash-o" aria-hidden="true" style="font-size:25px;margin:3px auto;"><a ></a></i>
                                    </div>

                                  </div>
                                </div>

                              </div>
                          </div>
                          @empty
                          @endforelse
                          <div class="perent mat" >
                              <div class="left">
                                  <label for="" class="item_name">Материал:</label>
                              </div>
                              <style>

                              </style>
                              <div class="center price">
                                <div class="dropdown makeNew">
                                  <button class="btn btn-outline-secondary dropdown-toggle" style="float:left;" type="button" id="dropdownMenuButton{{$i+1}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Выберите материал
                                  </button>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$i+1}}">
                                    <div style="padding:15px;" class="prokrutka">
                                      <input type="text" class="search">
                                      @forelse($materials as $material)
                                      <li class="material" id="{{$material->id}}">{{$material->title}}</li>
                                      @empty
                                      @endforelse
                                    </div>
                                  </div>
                                  <div class="dops" style="display:none;float:right;margin-right:270px;">
                                    <div class="input-part payment">
                                          <span id="0">Доп. Опл.</span>
                                        </div>
                                    <div class="input-part" style="margin-left:10px; width:100px;">
                                              <input type="text" name="counts" placeholder="количество" value="" class="form-control" value="">
                                    </div>
                                    <div class="input-part" id="addsupplier" data-toggle="modal" data-target="#supplier">
                                        <i class="fa fa-trash-o" aria-hidden="true" style="font-size:25px;margin:3px auto;"><a ></a></i>
                                    </div>
                                  </div>
                                </div>

                              </div>
                          </div>
                        </div>
                        <style media="screen">
                        .prokrutka {
                          height: 180px;
                          width: 100%; /* ширина нашего блока */
                          background: #fff; /* цвет фона, белый */
                          overflow-y: scroll; /* прокрутка по вертикали */
                        }
                        .prokrutka li{
                          cursor: default;
                          margin: 8px auto;
                          font-size: 16px;
                          width:100%
                        }
                        .prokrutka li:hover{
                          background-color: #e3e3e3;
                        }
                        .payment{
                          width:75px !important;margin-left:15px!important;cursor:default;
                        }
                        .dopborder{
                          box-shadow: 0.5px 0.5px 5px 0.5px rgb(173,58,40);
                          /* border: 1px solid black !important; */
                        }
                      </style>
                        <script type="text/javascript">

                          $('.materieals').on('keyup', '.search', function(){
                                        _this = this;
                                        $.each($(".material"), function() {
                                            if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                                               $(this).hide();
                                            else
                                               $(this).show();
                                        });
                                    });
                        </script>
                        <script type="text/javascript">
                          $(document).ready(function(){
                            var i = {{$i+2}};
                            //console.log($('.material').click(function(){}));
                            $('.materieals').on('click', '.material', function(){
                              //console.log('asd');
                              if($(this).closest('.dropdown').hasClass('makeNew') == true){
                                 $(this).closest('.perent').clone().appendTo('.materieals').find('button').text('Выберите материал').attr('id', 'dropdownMenuButton'+i).find('.dropdown-menu').attr('aria-labelledby', 'dropdownMenuButton'+i);
                                 $(this).closest('.dropdown').removeClass('makeNew').find('.dops').show();

                              }

                                i++;

                              var title = $(this).html();
                              $(this).closest('.dropdown').find('button').text(title);
                              //var mat = $(this).closest('.perent').clone();
                              //$('.materieals').append(mat);
                              var $this =  $(this).closest('.dropdown').find('span');
                              var id = $(this).closest('.dropdown').find('span').attr('id');
                              $.get('/admin-salon/services/material/'+id+'/{{$product->id}}/'+$(this).attr('id'), function(data){
                                $this.attr('id', data);

                              });

                            });

                            });
                        </script>
                        <script type="text/javascript">
                        $('.materieals').on('click', '.payment',function(){
                        var checkclass =  $(this).hasClass('dopborder');
                        if(checkclass == true){
                          $(this).removeClass('dopborder');
                        }
                        else{
                          $(this).addClass('dopborder');
                        }
                        var id = $(this).closest('.dropdown').find('span').attr('id');
                        $.get('/admin-salon/services/payment/{{$product->id}}/'+id, function(data){});
                        });

                        $('.materieals').on('click', '.fa-trash-o',function(){
                          var length = $('.mat').length;
                          if(length > 0){
                            var id = $(this).closest('.dropdown').find('span').attr('id');
                            $(this).closest('.mat').remove();
                            $.get('/admin-salon/services/material/delete/'+id, function(data){});
                          }



                        });

                        $('input[name="counts"]')
                        .on({
                          blur: function() {
                              var id = $(this).closest('.dropdown').find('span').attr('id');
                            $.get('/admin-salon/services/material/counts/'+id+'/'+this.value);
                          }
                        });
                        </script>
                        <div class="options">
                          @forelse($product->options as $option)
                          <div class="perent opt" >
                              <div class="left">
                                  <label for="" class="item_name">Опция:</label>
                              </div>
                              <style>

                              </style>
                              <div class="center price">

                                  <button class="btn btn-outline-primary" style="float:left; display:none;" type="button"  >
                                    Добавить опцию
                                  </button>

                                  <div class="dops" >
                                    <div class="input-part" style="width:70%">
                                          <input type="text" name="titleOpt" placeholder="Название опции" class="form-control" value="{{$option->title or ''}}">
                                        </div>
                                    <div class="input-part" style="margin-left:10px; width:100px;">
                                              <input type="text" name="costOpt" placeholder="р." class="form-control" value="{{$option->cost or ''}}">
                                    </div>
                                    <div class="input-part"  data-toggle="modal" data-target="#supplier">
                                        <i class="fa fa-trash-o delOpt" aria-hidden="true" style="font-size:25px;margin:3px auto;" id="{{$option->id}}"><a ></a></i>
                                    </div>
                                  </div>

                              </div>
                          </div>
                          @empty
                          @endforelse

                        <div class="perent opt" >
                            <div class="left">
                                <label for="" class="item_name">Опция:</label>
                            </div>
                            <style>

                            </style>
                            <div class="center price">

                                <button class="btn btn-outline-primary" style="float:left;" type="button"  >
                                  Добавить опцию
                                </button>

                                <div class="dops" style="display:none;">
                                  <div class="input-part" style="width:70%">
                                        <input type="text" name="titleOpt" placeholder="Название опции" class="form-control" value="">
                                      </div>
                                  <div class="input-part" style="margin-left:10px; width:100px;">
                                            <input type="text" name="costOpt" placeholder="р." class="form-control" value="">
                                  </div>
                                  <div class="input-part"  data-toggle="modal" data-target="#supplier">
                                      <i class="fa fa-trash-o delOpt" aria-hidden="true" style="font-size:25px;margin:3px auto;"><a ></a></i>
                                  </div>
                                </div>

                            </div>
                        </div>

                        </div>
                        <script type="text/javascript">
                          $('.options').on('click', '.btn', function(){

                               $(this).closest('.perent').clone().appendTo('.options');
                               $(this).closest('.price').find('.dops').show();
                               $(this).hide();
                               var $this =  $(this).closest('.price').find('.fa-trash-o');
                               $.get('/admin-salon/services/option/{{$product->id}}/', function(data){
                                 $this.attr('id', data);
                               });
                          });
                          $('input[name="titleOpt"]')
                          .on({
                            blur: function() {
                                var id = $(this).closest('.price').find('.fa-trash-o').attr('id');
                              $.get('/admin-salon/services/option/title/'+id+'/'+this.value);
                            }
                          });
                          $('input[name="costOpt"]')
                          .on({
                            blur: function() {
                                var id = $(this).closest('.price').find('.fa-trash-o').attr('id');
                              $.get('/admin-salon/services/option/cost/'+id+'/'+this.value);
                            }
                          });
                          $('.delOpt').click(function(){
                            var id = $(this).attr('id');
                            $(this).closest('.opt').remove();
                            $.get('/admin-salon/services/option/delete/'+id);
                          });
                        </script>

                        <div class="perent opt" >
                            <div class="left">
                                <label for="" class="item_name">Продолжительность:</label>
                            </div>
                            <style>

                            </style>
                            <div class="center price">

                                <input type="text" name="time" placeholder="мин." class="form-control" value="{{$product->goods_salon->timework or ''}}" style="width:10%; float:left;">

                                <div class="dops" style="float:left;">
                                  <div class="input-part" style="width:70%">
                                        <select name="selectTime" class="form-control">
                                          <option value="5">5 мин</option>
                                          <option value="15">15 мин</option>
                                          <option value="30">30 мин</option>
                                          <option value="60">1 час</option>
                                          <option value="75">1 час 15 мин.</option>
                                          <option value="90">1 час 30 мин.</option>
                                          <option value="105">1 час 45 мин.</option>
                                          <option value="120">2 часа</option>
                                          <option value="210">2 часа 30 мин.</option>
                                          <option value="180">3 часа</option>
                                          <option value="240">4 часа</option>
                                          <option value="300">5 часов</option>
                                          <option value="360">6 часов</option>
                                          <option value="420">7 часов</option>
                                          <option value="480">8 часов</option>
                                        </select>
                                      </div>

                                </div>

                            </div>
                        </div>
                        <script type="text/javascript">
                          $('select[name="selectTime"]').on('change', function(){
                            console.log($(this).val());
                            $('input[name="time"]').val($(this).val());
                            $.get('admin-salon/subservices/time/{{$product->goods_salon->id}}/'+$(this).val());
                          });
                          $('input[name="time"]')
                          .on({
                            blur: function() {
                                $.get('admin-salon/subservices/time/{{$product->goods_salon->id}}/'+this.value);
                            }
                          });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Фотографии:</label>
                            </div>
                            <div class="center">
                                <div class="file-upload">
                                    <label>
                                        <input id="sortpicture" type="file" name="sortpic[]" multiple />
                                        <span>Выбрать файл</span>
                                    </label>
                                </div>
                                <input type="text" id="filename" class="filename" disabled style="border: none;font-size: 12px;">
                                <div class="imgs">
                                @if($product !== null)
                                @forelse($product->imageses as $image)
                                <div class="image_group" id="image{{$image->id}}" style="width:auto; float:left;">
                                    <div class="image">
                                        <img src="{{asset('storage/app')}}/{{$image->images or ''}}" alt="">
                                        <a  class="delete" id="send{{$image->id}}"></a>
                                    </div>
                                </div>
                                <script>

                                ajaxForm();
                                function ajaxForm(){
                                  $('#send{{$image->id}}').click(function(){
                                    var data = new FormData($("#form")[0]);
                                      $.ajax({
                                          type: 'GET',
                                          url: "{{route('admin.image.goods.destroy')}}?id={{$image->id}}",
                                          data: data,
                                          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                          contentType: false,
                                          processData : false,
                                          success: function(data) {
                                              $('#image{{$image->id}}').remove();
                                          }
                                      });
                                      return false;
                                  });
                                }

                        </script>
                                @empty
                                @endforelse
                                <div class="clearfix"></div>
                                @endif
                              </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('#sortpicture').on('change',function() {
                          //alert('asd');
                          var i = 1;
                          var files = $('#sortpicture').prop('files');
                          $.each(files, function (index, value) {
                            var file_data = value;
                            var form_data = new FormData();
                            form_data.append('file', file_data);
                            form_data.append('id', {{$product->id}});
                            $.ajax({
                                        url: '/admin/image/upload',
                                        dataType: 'text',
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'post',
                                        success: function(data){
                                          data = $.parseJSON(data);
                                            $('.imgs').append('<div class="image_group" id="image'+i+'" style="width:auto;float:left;">              <div class="image">                                                    <img src="{{asset('storage/app')}}/'+data.image+'" alt="">                                                    <a  class="delete" id="send'+i+'"></a>                                             </div>                                            </div>');
                                            $('#image'+i).click(function(){

                                              $.get('{{route('admin.image.goods.destroy')}}?id='+data.id,function(data){$('#image'+i).remove();});
                                            });
                                        }
                             });
                             i++;
                          });

                        });

                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Видео с YouTube:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="youtube" class="" placeholder="Например: http://www.youtube.com/watch?v=9X-8JiOhzX4" value="{{$product->youtube or ''}}">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <div class="perent" style="margin-top: 40px;">
                            <div class="left">&nbsp;</div>
                            <div class="center">
                                <input type="button" class="item_button" value="Сохранить и вернуться">
                            </div>
                            <div class="right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>

        <script type="text/javascript">
        $('#refresh').click(function(){
          $.get('{{route('admin.services.index')}}', function(data){
            $('.admin-content').html(data);
          });
        });
        $('.item_button').click(function(){
          var text = CKEDITOR.instances['short_description'].getData();
          $.post('/admin-salon/service/update/description', {short_description:text, id:{{$product->id}}}, function(data){
            $('.admin-content').html(data);
          });
        });
        </script>

        <script>
            $(document).ready( function() {
                $(".file-upload input[type=file]").change(function(){var filename = $(this).val().replace(/.*\\/, "");$("#filename").val(filename);});
            });
        </script>
        <script>
            $(document).ready(function() {
                $('input[type="checkbox"]').click(function() {
                    if($(this).attr('id') == 'watch') {
                      var val = 0;
                        if (this.checked) {$('#switch_seo').show(); val = 1;}
                        else {$('#switch_seo').hide(); val = 0;}
                        $.get('/admin/goods/checkseo/{{$product->id}}/'+val);
                    }
                });
            });
        </script>
        @if($product !== null)
        @if($product->checkseo == 1)
        <script>
            $(document).ready(function() {$('#switch_seo').show();});
        </script>
        @endif
        @endif

        <style>



    ::-webkit-scrollbar {
    width: 3px; height: 3px;
    }


    ::-webkit-scrollbar-track-piece  {
    background-color: #c7c7c7;
    }

    ::-webkit-scrollbar-thumb:vertical {
    height: 50px; background-color: #666; border-radius: 3px;
    }

    ::-webkit-scrollbar-thumb:horizontal {
    height: 50px; background-color: #666; border-radius: 3px;
    }

    ::-webkit-scrollbar-corner{
      background-color: #999;
    }
    .emoji{
      width: 18px !important; height:18px !important;
    }
        </style>
    <script type="text/javascript" src="/public/admin/js/jquery.mask.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
