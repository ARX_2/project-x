<div class="modal fade" id="charge" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 800px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="text" name="goodsid"  style="display:none;" id="chargeid">
        <!--  -->
        <div class="" style="float:left;">
          <label for="" style="font-size:12px;">Текущее количество на складе<br>
  <input type="text" name="oldCounts" value="" class="form-control" readonly></label>
        </div>
        <div style="float:left; font-size:25px; margin:12px 10px;">
          +
        </div>
        <div class="" style="float:left;">
          <label for="" style="font-size:12px;">Списание/расход<br>
  <input type="text" name="newCounts" value="" class="form-control" ></label>
        </div>
        <div style="float:left; font-size:25px; margin:12px 10px;">
          =
        </div>
        <div  style="float:left; margin-left: 5px;">
          <label for="" style="font-size:12px;">Текущее количество<br>
  <input type="text" name="allCounts" value="" class="form-control" readonly></label>
        </div>
        <div class="clearfix"></div>
          <br>
          <label for="" style="width:100%">
            <textarea name="comment" class="form-control" style="height:100px;" placeholder="Комментарий"></textarea>
          </label>
          <input type="hidden" name="onePrice" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary saveCharge">Сохранить</button>
      </div>
    </div>
  </div>
</div>
  <script type="text/javascript">
  $('.btn-danger').click(function(){
    var title = $(this).closest('.item').find('.name').find('a').html();
    var id = $(this).closest('.item').attr('id');
    console.log(id);
    $('.modal-title').text('Списание/расход: '+title);
    $('#chargeid').attr('value', id)
    $.get('admin-salon/goods/counts/'+id, function(data){
      data = $.parseJSON(data);
      $('input[name="oldCounts"]').val(data.allCount);
      $('input[name="onePrice"]').val(data.cost);
    });
  });

  $('#charge').on('input', 'input[name="newCounts"]', function(){
    var val = Number.parseInt($(this).val());
    var oldVal = Number.parseInt($('input[name="oldCounts"]').val());
    var newVal =  oldVal - val ;
    if(newVal == 'NaN'){
      newVal = 0;
    }
    $('#countPrice').text(val);
    $('input[name="allCounts"]').val(newVal);
  });

  $('.saveCharge').click(function(){
    var id = $(this).closest('#charge').find('.modal-body').find('input[name="goodsid"]').val();
    console.log(id);
    var oldCounts = $(this).closest('#charge').find('.modal-body').find('input[name="oldCounts"]').val();
    var newCounts = $(this).closest('#charge').find('.modal-body').find('input[name="newCounts"]').val();
    var allCounts = $(this).closest('#charge').find('.modal-body').find('input[name="allCounts"]').val();
    var comment = $(this).closest('#charge').find('.modal-body').find('textarea[name="comment"]').val();
    var cost = $(this).closest('#charge').find('.modal-body').find('input[name="onePrice"]').val();
    var $this = $(this).closest('#charge').find('.modal-body').find('input[name="oldCounts"]');
    $.post('/admin-salon/invenory/charge', {id:id, oldCounts:oldCounts, newCounts:newCounts,allCounts:allCounts,comment:comment,
      cost:cost}, function(data){
     $this.val(data);
     $('#'+id).find('.btn-light').text(data);
     $('#'+id).find('.btn-light').text(data);
     alert('Спесание успешно');
    });
  });
  </script>
