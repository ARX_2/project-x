<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 800px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="text" name="goodsid"  style="display:none;">
        <!--  -->
        <div class="" style="float:left;">
          <label for="" style="font-size:12px;">Текущее количество на складе<br>
  <input type="text" name="oldCounts" value="" class="form-control" readonly></label>
        </div>
        <div style="float:left; font-size:25px; margin:12px 10px;">
          +
        </div>
        <div class="" style="float:left;">
          <label for="" style="font-size:12px;">Поступило<br>
  <input type="text" name="newCounts" value="" class="form-control" ></label>
        </div>
        <div style="float:left; font-size:25px; margin:12px 10px;">
          =
        </div>
        <div  style="float:left; margin-left: 5px;">
          <label for="" style="font-size:12px;">Новое количество<br>
  <input type="text" name="allCounts" value="" class="form-control" readonly></label>
        </div>
        <div class="clearfix"></div>
        <hr>
        <!--  -->

        <div style="float:left; width:25%; height:100px;">
          <span id="primeCost1">Cебестоимость: <b>-</b>р.</span>
        </div>
        <div  style="float:left; margin-left: 5px; width:42%;">
          <label for="" style="font-size:12px;">Цена за 1шт<br>
          <input type="text" name="onePrice" value="" class="form-control" placeholder="р." style="width:130px; height:26px;text-align:right; padding: 0;"></label>
          <label for="" style="font-size:12px; ">Цена за <span id="countPrice"></span>шт<br>
          <input type="text" name="anyPrice" value="" class="form-control" placeholder="р." style="width:130px; height:26px;text-align:right; padding: 0;"></label>
        </div>
        <div style="float:left;">
          <span id="primeCost2">Себестоимость: <b>-</b>р.</span>
        </div>
        <div  style="float:left; margin-left: 5px; width:42%;">
          <select name="payment" class="form-control" style="width:83%;height:26px; padding: 0;">
            <option disable selected>Выберите поставщика</option>
            <option value="1">Не учитывать оплату</option>
            <option value="2">Изьять наличными из кассы</option>
            <option value="3">Изьять с безналичного расчета</option>
          </select>
        </div>
        <div class="clearfix"></div>

        <hr>
        <!--  -->
          <b>Поставщик: </b><br>
          <div class="clearfix"></div>
          <select name="supplier" id="suppliers" class="form-control" style="width:90%;float:left;height:26px; padding: 0;">
            <option disabled selected>Выберите поставщика</option>
            @forelse($suppliers as $supplier)
            <option value="{{$supplier->id}}">{{$supplier->title}}</option>
            @empty
            @endforelse

          </select>
          <i class="fa fa-plus" aria-hidden="true" style="font-size:25px;margin:2px 15px;float:left;" data-toggle="modal" data-target="#supplier"><a ></a></i>
          <div class="clearfix"></div>

          <br>
          <label for="" style="width:100%">
            Emai:  Тел:  Контактное лицо:  Адрес:  ИНН:  КПП:  Комментарий:
            <textarea name="comment" class="form-control" style="height:100px;"></textarea>
          </label>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" id="save">Сохранить</button>
      </div>
    </div>
  </div>
</div>

  <style media="screen">
    .btn-danger, .btn-light, .btn-success{padding: 3px 8px;}
  </style>
  <script type="text/javascript">
  $('.btn-success').click(function(){
    var title = $(this).closest('.item').find('.name').find('a').html();
    var id = $(this).closest('.item').attr('id');
    console.log(id);
    $('.modal-title').text('Поступление: '+title);
    $('input[name="goodsid"]').val(id);
    $.get('/admin-salon/goods/counts/'+id, function(data){
      data = $.parseJSON(data);
      $('input[name="oldCounts"]').val(data.allCount);
    });
  });
  $('input[name="newCounts"]').on('input', function(){
    var val = Number.parseInt($(this).val());
    var oldVal = Number.parseInt($('input[name="oldCounts"]').val());
    var newVal = val + oldVal;
    if(newVal == 'NaN'){
      newVal = 0;
    }
    $('#countPrice').text(val);
    $('input[name="allCounts"]').val(newVal);
    var onePrice = Number.parseInt($('input[name="onePrice"]').val());
    var primeCost1 = onePrice * oldVal;
    var primeCost2 = primeCost1 + (newVal * onePrice);
    $(this).closest('.modal-body').find('#primeCost1').find('b').text(primeCost1);
    $(this).closest('.modal-body').find('#primeCost2').find('b').text(primeCost2);
  });
  $('input[name="onePrice"]').on('input', function(){
    var val = Number.parseInt($(this).val());
    var oldVal = Number.parseInt($('input[name="oldCounts"]').val());
    var newVal = Number.parseInt($('input[name="newCounts"]').val());
    var primeCost1 = val * oldVal;
    var primeCost2 = primeCost1 + (newVal * val);
    $(this).closest('.modal-body').find('#primeCost1').find('b').text(primeCost1);
    $(this).closest('.modal-body').find('#primeCost2').find('b').text(primeCost2);
    $(this).closest('.modal-body').find('input[name="anyPrice"]').val(val*10);
  });
  $('input[name="anyPrice"]').on('input', function(){
    var val = Number.parseInt($(this).val());
    var diff = Number.parseInt($('input[name="newCounts"]').val());
    $('input[name="onePrice"]').val(val/diff);
    var oldVal = Number.parseInt($('input[name="oldCounts"]').val());
    var primeCost1 = (val/diff) * oldVal;
    var primeCost2 = primeCost1 + (diff * val/diff);
    $(this).closest('.modal-body').find('#primeCost1').find('b').text(primeCost1);
    $(this).closest('.modal-body').find('#primeCost2').find('b').text(primeCost2);
  });

  $('#save').click(function(){
    var id = $('input[name="goodsid"]').val();
    var oldCounts = $('input[name="oldCounts"]').val();
    var newCounts = $('input[name="newCounts"]').val();
    var allCounts = $('input[name="allCounts"]').val();
    var onePrice = $('input[name="onePrice"]').val();
    var payment = $('select[name="payment"]').val();
    var supplier = $('select[name="supplier"]').val();
    var comment = $('textarea[name="comment"]').val();


    $.post('/admin-salon/invenory/entrance', {id:id, oldCounts:oldCounts, newCounts:newCounts,allCounts:allCounts,onePrice:onePrice,
    payment:payment, supplier:supplier, comment:comment}, function(data){
     $('input[name="oldCounts"]').val(data);
     $('#'+id).find('.btn-light').text(data);
     alert('Поступление обновленно');
    });
  });
  </script>
