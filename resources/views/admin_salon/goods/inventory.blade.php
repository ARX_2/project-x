<div class="modal fade" id="inventry" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width: 800px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="text" name="goodsid"  style="display:none;" id="inventoryid">
        <!--  -->
        <div class="" style="float:left;">
          <label for="" style="font-size:12px;">Текущее количество на складе<br>
  <input type="text" name="oldCounts" value="" class="form-control" readonly></label>
        </div>
        <div style="float:left; font-size:25px; margin:12px 10px;">
          +
        </div>
        <div class="" style="float:left;">
          <label for="" style="font-size:12px;">Фактическое количество<br>
  <input type="text" name="newCounts" value="" class="form-control" ></label>
        </div>
        <div style="float:left; font-size:25px; margin:12px 10px;">
          =
        </div>
        <div  style="float:left; margin-left: 5px;">
          <label for="" style="font-size:12px;">Текущее количество<br>
  <input type="text" name="allCounts" value="" class="form-control" readonly></label>
        </div>
        <div class="clearfix"></div>
        <hr>
        <!--  -->

        <div style="float:left; width:25%; height:50px;">
          <span id="primeCost1">Cебестоимость: <b>-</b>р.</span>
        </div>
        <div  style="float:left; margin-left: 5px; width:42%;">
          <input type="hidden" name="onePrice" value="">
        </div>
        <div style="float:right; margin-right:150px;">
          <span id="primeCost2">Себестоимость: <b>-</b>р.</span>
        </div>
        <div class="clearfix"></div>

        <hr>

          <br>
          <label for="" style="width:100%">
            Emai:  Тел:  Контактное лицо:  Адрес:  ИНН:  КПП:  Комментарий:
            <textarea name="comment" class="form-control" style="height:100px;"></textarea>
          </label>
        <label for="" style="width:100%">
          Предупреждать, если осталось менее:
          <input type="text" name="warningCount" value="" class="form-control" style="width:30%"></label>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary save">Сохранить</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$('.btn-light').click(function(){
var title = $(this).closest('.item').find('.name').find('a').html();
var id = $(this).closest('.item').attr('id');
var warningCount = $(this).closest('.price').find('.warningCounts').attr('id');
$('.modal-title').text('Инвентаризация: '+title);
$('#inventoryid').attr('value', id)
$('input[name="warningCount"]').val(warningCount);
$.get('admin-salon/goods/counts/'+id, function(data){
  data = $.parseJSON(data);
  $('input[name="oldCounts"]').val(data.allCount);
  $('input[name="onePrice"]').val(data.cost);
  $('#'+id).find('.btn-light').text(data.allCount);
});
});

$('.save').click(function(){

var id = $('#inventoryid').val();

var oldCounts = $(this).closest('#inventry').find('input[name="oldCounts"]').val();
var newCounts = $(this).closest('#inventry').find('input[name="newCounts"]').val();
var allCounts = $(this).closest('#inventry').find('input[name="allCounts"]').val();
var comment = $(this).closest('#inventry').find('textarea[name="comment"]').val();
var cost = $(this).closest('#inventry').find('input[name="onePrice"]').val();
var warningCount = $(this).closest('#inventry').find('input[name="warningCount"]').val();
var $this = $(this).closest('#inventry').find('input[name="oldCounts"]');
$.post('/admin-salon/invenory/inventory', {id:id, oldCounts:oldCounts, newCounts:newCounts,allCounts:allCounts,comment:comment,
  cost:cost, warningCount:warningCount}, function(data){
 $this.val(data);
 $('#'+id).find('.btn-light').text(data);
 alert('Инвернтаризация обновлена');
});
});

$('#inventry').on('input', 'input[name="newCounts"]', function(){
var val = Number.parseInt($(this).val());
var oldVal = Number.parseInt($('input[name="oldCounts"]').val());
var newVal =  val - oldVal ;
if(newVal == 'NaN'){
  newVal = 0;
}
$('#countPrice').text(val);
$('input[name="allCounts"]').val(newVal);
var onePrice = Number.parseInt($('input[name="onePrice"]').val());
var primeCost1 = onePrice * oldVal;
var primeCost2 =  (val * onePrice) - primeCost1;
$(this).closest('.modal-body').find('#primeCost1').find('b').text(primeCost1);
$(this).closest('.modal-body').find('#primeCost2').find('b').text(primeCost2);
});
</script>
