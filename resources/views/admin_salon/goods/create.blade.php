      <div class="row">
        <div class="col-lg-4">
            <h2>Управление товарами</h2>
        </div>
        <div class="col-lg-8">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.index')}}">Главная</a></li>
                <li class="active">{{$company->info->section0 or 'Товары'}}</li>
            </ol>
        </div>
    </div>
    <hr>

        <div class="c_p_company">
            <div class="wide">
                <div class="box">
                    <div class="wide_title">
                        <h2>Правая штука</h2>
                    </div>
                    <div class="undefined_item">
                        {{--<div class="item">
                            <div class="name">
                                <a href=""></a>
                            </div>
                        </div>--}}

                    </div>
                </div>
            </div>

            <div class="narrow">
                <div class="box">
                    <div class="cp_edit">
                        <div class="narrow_title">
                          <i class="fa fa-chevron-circle-left" aria-hidden="true" style="float:left; font-size:20px;color:#14aae6;padding: 5px;border-radius: 50px;margin-top:5px;" id="refresh"></i>
                          <p class="clearFix"></p>
                          <style media="screen">
                            #refresh:hover{
                              background-color: #e7e6e6;
                            }
                          </style>
                            @if($product == null)
                            <h2 style="float:left;margin-left:10px;">Добавление товара</h2>
                            @else
                            <h2 style="float:left;margin-left:10px;">Редактирование товара: <span id="edit-title">{{$product->title}}</span></h2>
                            @endif

                        </div>
                        @if($product == null)
                        <form class="" action="{{route('admin.goods.store')}}" method="post" enctype="multipart/form-data">
                        @else
                        <form  action="{{route('admin.goods.update.id', ['update',$product->id,0])}}" method="post"  enctype="multipart/form-data">
                          <input type="hidden" name="_method" value="put">
                          <input type="hidden" name="id" value="{{$product->id}}">
                        @endif
                        {{ csrf_field() }}
                        <input type="hidden" name="slug" class="form-control" placeholder="Автоматическая генерация"
               value="{{$goods->slug or ""}}" readonly>
                        {{--<div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Выберите категорию:</label>
                            </div>
                            <div class="center">
                              <select class="form-control" name="subcategory" required>
                                  @forelse($categories as $c)
                                  <option value="{{$c->id or ''}}" @if($product !== null && $product->subcategory !== null && $c->id == $product->subcategory->id) selected="" @endif style="font-weight:600;">{{$c->title or ''}}</option>
                                  @forelse($c->subcategoriesAll as $sub)
                                  @if($product !== null && $product->subcategory !== null )
                                  <option value="{{$sub->id}}" @if($sub->id == $product->subcategory->id)selected="" @endif>{{$sub->title}}</option>
                                  @else
                                  <option value="{{$sub->id}}">&nbsp;&nbsp;&nbsp;{{$sub->title}}</option>
                                  @endif
                                  @empty
                                  @endforelse

                                  @empty
                                  <option value="">Нет категорий</option>
                                  @endforelse
                              </select>
                            </div>
                        </div>--}}
                        <div class="perent">
                            <div class="left">
                                <label for="" class="name_item">Выберите категорию:</label>
                            </div>
                            <div class="center">
                                <style>
                                    .category_selection{width: 100%;    display: inline-flex;}
                                    .category_group{width: 50%;border: 1px solid #ddd;padding-bottom: 10px;    margin-left: -1px;}
                                    .category_group_title{line-height: 13px;font-size: 13px;padding: 10px;color: #888;}
                                    .category_group input[type="radio"] + span{border: 0;margin: 0;width: 1px;height: 1px}
                                    /* input[type="radio"]:checked + span:before {content: "";display: block;background: #fff;border-radius: 100%;width: 8px;height: 8px;margin: 2px;pointer-events: none;} */
                                    .c_action{display:block;width:100%;padding:4px 10px;margin:0;border:0;background-color:#fff;color:#555;text-align:left;line-height:16px;cursor:pointer}
                                    .c_action:hover{background-color:#cee9ff}
                                    .c_action.back{background-color:#269ffd}
                                    .c_action.back span{color:#fff}
                                    .s_action{display:block;width:100%;padding:4px 10px;margin:0;border:0;background-color:#fff;color:#555;text-align:left;line-height:16px;cursor:pointer}
                                    .s_action:hover{background-color:#cee9ff}
                                    .s_action.back{background-color:#269ffd}
                                    .s_action.back span{color:#fff}
                                </style>
                                <div class="category_selection">
                                    <div class="category_group cat">
                                        <div class="category_group_title">Категория</div>
                                        @forelse($categories as $c)
                                        <?php //dd($c); ?>
                                        @if($c->parent_id == null)
                                        <label class="c_action @if($product !== null && $product->subcategory !== null && $c->id == $product->subcategory->id || $product !== null && $product->subcategory !== null && $c->id == $product->subcategory->parent_id) back @endif" id="cat{{$c->id}}"><input type="radio" name="category" hidden="" value="{{$c->id}}" @if($product !== null && $product->subcategory !== null && $c->id == $product->subcategory->id) checked @endif><span></span><span class="radio-name">{{$c->title or 'Название категории'}}</span></label>
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('#cat{{$c->id}}').click(function(){
                                                  //alert('asd');
                                                    @if(count($c->subcategoriesAll)>0)
                                                    $('.s_cat').show();
                                                    @else
                                                    $('.s_cat').hide();
                                                    @endif
                                                    $("input[name='subcategory']").prop("checked", false);
                                                    $('.cat{{$c->id}}').removeClass('back');
                                                    $('.s_action').hide();
                                                    $('.cat{{$c->id}}').show();
                                                    $('#category_group_title').text('{{$c->title or ''}}');
                                                });
                                            });


                                        </script>

                                        @endif
                                        @empty
                                        Сперва нужно добавить категорию
                                        @endforelse

                                    </div>
                                    <?php //dd($product); ?>
                                    <div class="category_group s_cat" style="display: @if($product !== null && $product->subcategory !== null && $product->subcategory->parent_id !== null) block @else none @endif">
                                      <div class="category_group_title" id="category_group_title">Название на нажатую категорию</div>
                                      @forelse($categories as $c)
                                      @forelse($c->subcategoriesAll as $subcategory)

                                        @if($subcategory->published == 1)
                                        <?php //dd($product->subcategory);?>
                                      <label class="s_action cat{{$c->id}} @if($product !== null && $product->subcategory !== null && $subcategory->id == $product->subcategory->id) back @endif" style="display:@if($product !== null && $product->category !== null && $product->subcategory->parent_id == $c->id) block @else none @endif"><?php //dd($subcategory); ?> <input type="radio" name="subcategory" hidden="" class="inputcategory{{$c->id}}" @if($product !== null && $product->subcategory !== null && $subcategory->id == $product->subcategory->id) checked @endif value="{{$subcategory->id}}"><span></span><span class="radio-name">{{$subcategory->title or ''}}</span></label>
                                      @endif

                                      @empty
                                      @endforelse
                                      @empty
                                      @endforelse
                                    </div>
                                </div>
                                <script type="text/javascript">
                                $('.c_action, .s_action').click(function(){
                                  var cat = $(this).find('input').val();
                                  $.get('/admin-salon/goods/category/{{$product->id}}/'+cat,function(data){});
                                  return false;
                                });

                                </script>

                                <script>
                                    $('.cat .c_action').on("click", function(){
                                        $('.c_action').removeClass('back');
                                        $(this).addClass('back');
                                    });
                                    $('.s_cat .s_action').on("click", function(){
                                        $('.s_action').removeClass('back');
                                        $(this).addClass('back');
                                    });
                                </script>

                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="seo_name">SEO</label>
                            </div>
                            <div class="center">
                                <label class="switch">
                                    <input type="checkbox" id="watch" name="checkseo" value="1" @if($product !== null)@if($product->checkseo == 1)checked @endif @endif>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="seo_group" id="switch_seo" style="display:none">
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name" style="padding-top: 4px">Название на странице товара или услуги:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seoname" class="" placeholder="Название на странице категории" value="{{$product->seoname or ''}}" >
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('input[name="seoname"]')
                            .on({
                              blur: function() {
                                $.get('/admin-salon/goods/seoname/{{$product->id}}/'+this.value);
                              }
                            });
                            </script>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Title:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seotitle" class="" placeholder="" value="{{$product->seotitle or ''}}" >
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('input[name="seotitle"]')
                            .on({
                              blur: function() {
                                $.get('/admin-salon/goods/seotitle/{{$product->id}}/'+this.value);
                              }
                            });
                            </script>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Description:</label>
                                </div>
                                <div class="center">
                                    <input type="text" name="seodescription" class="" placeholder="" value="{{$product->seodescription or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('input[name="seodescription"]')
                            .on({
                              blur: function() {
                                $.get('/admin-salon/goods/seodescription/{{$product->id}}/'+this.value);
                              }
                            });
                            </script>
                            <div class="item_line">
                                <div class="left">
                                    <label for="" class="item_name">Мета-тег Keywords:</label>
                                </div>
                                <div class="center">
                                  <input type="text" name="seokeywords" class="" placeholder="" value="{{$product->seokeywords or ''}}">
                                </div>
                                <div class="right">
                                    <div class="help"></div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $('input[name="seokeywords"]')
                            .on({
                              blur: function() {
                                $.get('/admin-salon/goods/seokeywords/{{$product->id}}/'+this.value);
                              }
                            });
                            </script>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;padding-top: 4px;">Название товара или услуги:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="title" class="" placeholder="Введите название товара или услуги" value="{{$product->title or ''}}" required="" id="someInput">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('input[name="title"]').on('input',function(){
                          var title = $(this).val();
                          $('#edit-title').text(title);
                        });
                        $('input[name="title"]')
                        .on({
                          blur: function() {
                            $.get('/admin-salon/goods/title/{{$product->id}}/'+this.value);
                          }
                        });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Описание:</label>
                            </div>
                            <div class="center">
                                <textarea rows="10" name="short_description" id="short_description">{!! $product->short_description or '' !!}</textarea>
                                <script>
                                    CKEDITOR.replace('short_description');
                                </script>
                            </div>
                        </div>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Стоимость:</label>
                            </div>
                            <style>

                            </style>
                            <div class="center price">
                                <input type="text" name="coast" class=" a-price" placeholder="0" value="{{$product->coast or ''}}" data-mask="000 000 000" data-mask-reverse="true" data-mask-maxlength="false">
                                <div class="input-part">
                                  <select class="form-control" name="type_cost" style="width:100px;">

                                    @forelse($type_coasts as $type_coast)
                                    <option value="{{$type_coast->id or ''}}" @if($product !== null && $product->type_cost == $type_coast->id) selected @endif>{{$type_coast->vin_padezh}}</option>
                                    @empty
                                    @endforelse
                                  </select>
                                        <!-- <span>
                                            <span class="number-format-font">₽</span>
                                        </span> -->
                                </div>

                            </div>
                        </div>
                        <script type="text/javascript">
                        $('select[name="type_cost"]').on('change', function(){
                          $.get('admin-salon/goods/type_cost/{{$product->id}}/'+$(this).val());
                        });
                        $('input[name="coast"]')
                        .on({
                          blur: function() {
                            $.get('/admin/goods-salon/coast/{{$product->id}}/'+this.value);
                          }
                        });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Сведенья:</label>
                            </div>
                            <style>

                            </style>
                            <div class="center price">
                                <input type="text" name="articulate" class=" a-price" placeholder="артикул" value="{{$product->articulate or ''}}">
                                <div class="input-part">
                                        <input type="text" name="bar_code" class=" a-price" placeholder="штрих-код" value="{{$product->goods_salon->bar_code or ''}}" >

                                </div>
                                <div class="input-part" style="margin-left:85px;" id="suppliers">
                                          <select name="supplier" class="form-control" style="width:200px;">
                                            <option disabled selected>Выберите поставщика</option>
                                            @forelse($suppliers as $supplier)
                                            <option vlaue="{{$supplier->id}}" @if($product->goods_salon !== null && $product->goods_salon->supplier == $supplier->id) selected @endif>{{$supplier->title or ''}}</option>
                                            @empty
                                            @endforelse
                                          </select>
                                          <!-- <input type="text" name="supplier" class=" a-price" placeholder="поставщик" value="{{$product->goods_salon->supplier or ''}}" > -->
                                </div>
                                <div class="input-part" style="margin-left:165px;" id="addsupplier" data-toggle="modal" data-target="#supplier">
                                    <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size:25px;margin:3px auto;"><a ></a></i>
                                </div>


                            </div>
                        </div>
                        <script type="text/javascript">
                        $('input[name="articulate"]')
                        .on({
                          blur: function() {
                            $.get('/admin-salon/goods/articulate/{{$product->id}}/'+this.value);
                          }
                        });
                        $('input[name="bar_code"]')
                        .on({
                          blur: function() {
                            $.get('/admin-salon/subgoods/bar_code/{{$product->id}}/'+this.value);
                          }
                        });
                        $('select[name="supplier"]').on('change', function(){
                          $.get('/admin-salon/subgoods/supplier/{{$product->id}}/'+$(this).val(), function(data){});
                        });
                        </script>

                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Оплата с продажи мастеру:</label>
                            </div>
                            <style>

                            </style>
                            <div class="center price master_sale">
                              <select id="masterPay" class="form-control" style="width:305px; float:left;">
                                <option disabled selected>Выберите оплату</option>
                                <option value="1" @if($product !== null && $product->goods_salon->master_sale_type == 1) selected @endif>Мастеру с продажи стандартная оплата</option>
                                <option value="2" @if($product !== null && $product->goods_salon->master_sale_type == 2) selected @endif>Мастеру с продажи особый %</option>
                                <option value="3" @if($product !== null && $product->goods_salon->master_sale_type == 3) selected @endif>Мастеру с продажи фикс. сумму</option>
                                <option value="4" @if($product !== null && $product->goods_salon->master_sale_type == 4) selected @endif>Мастеру с продажи не выплачивать</option>
                              </select>
                              <?php //dd($product); ?>
                                <div class="input-part" style="float:left;">
                                        <input type="text" name="masterPayCount" class=" a-price" placeholder="@if($product !== null &&  $product->goods_salon->master_sale_type ==2)% @elseif($product !== null &&  $product->goods_salon->master_sale_type ==3)р. @endif" value="{{$product->goods_salon->master_sale_count or ''}}" @if($product !== null && $product->goods_salon->master_sale_type == 1 || $product !== null && $product->goods_salon->master_sale_type == 4) readonly @endif>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                          $('.master_sale').on('change', 'select',function(){
                            if($(this).val() == 1 || $(this).val() == 4){
                              $('input[name="masterPayCount"]').val('');
                              $('input[name="masterPayCount"]').prop('readonly', true);
                            }
                            else{

                              $('input[name="masterPayCount"]').prop('readonly', false);
                            }
                            if($(this).val() == 2){
                              $('input[name="masterPayCount"]').attr('placeholder', '%');
                            }
                            else if($(this).val() == 3){
                              $('input[name="masterPayCount"]').attr('placeholder', 'р.');
                            }
                            $.get('admin-salon/subgoods/master_sale_type/{{$product->id}}/'+$(this).val());
                          });
                          $('input[name="masterPayCount"]')
                          .on({
                            blur: function() {
                              $.get('/admin-salon/subgoods/master_sale_count/{{$product->id}}/'+this.value);
                            }
                          });
                        </script>

                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Оплата с продажи админ.:</label>
                            </div>
                            <style>

                            </style>
                            <div class="center price">
                              <select id="adminPay" class="form-control" style="width:305px; float:left;">
                                <option disabled selected>Выберите оплату</option>
                                <option value="1" @if($product !== null && $product->goods_salon->administrator_sale_type == 1) selected @endif>Администратору с продажи стандартная оплата</option>
                                <option value="2" @if($product !== null && $product->goods_salon->administrator_sale_type == 2) selected @endif>Администратору с продажи особый %</option>
                                <option value="3" @if($product !== null && $product->goods_salon->administrator_sale_type == 3) selected @endif>Администратору с продажи фикс. сумму</option>
                                <option value="4" @if($product !== null && $product->goods_salon->administrator_sale_type == 4) selected @endif>Администратору с продажи не выплачивать</option>
                              </select>
                                <div class="input-part" style="float:left;">
                                        <input type="text" name="aminPayCount" class=" a-price" placeholder="@if($product !== null &&  $product->goods_salon->administrator_sale_type ==2)% @elseif($product !== null &&  $product->goods_salon->administrator_sale_type ==3)р. @endif" value="{{$product->goods_salon->administrator_sale_count or ''}}" @if($product !== null && $product->goods_salon->administrator_sale_type == 1 || $product !== null && $product->goods_salon->administrator_sale_type == 4) readonly @endif>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                          $('#adminPay').on('change',function(){
                            console.log($(this).val());
                            if($(this).val() == 1 || $(this).val() == 4){
                              $('input[name="aminPayCount"]').val('');
                              $('input[name="aminPayCount"]').prop('readonly', true);

                            }
                            else{
                              $('input[name="aminPayCount"]').prop('readonly', false);
                            }
                            if($(this).val() == 2){
                              $('input[name="aminPayCount"]').attr('placeholder', '%');
                            }
                            else if($(this).val() == 3){
                              $('input[name="aminPayCount"]').attr('placeholder', 'р.');
                            }
                            $.get('admin-salon/subgoods/administrator_sale_type/{{$product->id}}/'+$(this).val());
                          });
                          $('input[name="aminPayCount"]')
                          .on({
                            blur: function() {
                              $.get('/admin-salon/subgoods/administrator_sale_count/{{$product->id}}/'+this.value);
                            }
                          });
                        </script>
                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name" style="line-height: 14px;">Фотографии:</label>
                            </div>
                            <div class="center">
                                <div class="file-upload">
                                    <label>
                                      <input id="sortpicture" type="file" name="sortpic[]" multiple />

                                        <span>Выбрать файл</span>
                                    </label>
                                </div>
                                <input type="text" id="filename" class="filename" disabled style="border: none;font-size: 12px;" >
                                <div class="imgs">
                                @if($product !== null)
                                @forelse($product->imageses as $image)



                                <div class="image_group" id="image{{$image->id}}" style="width:auto;float:left;">
                                    <div class="image">
                                        <img src="{{asset('storage/app')}}/{{$image->images or ''}}" alt="">
                                        <a  class="delete" id="send{{$image->id}}"></a>
                                    </div>
                                </div>

                                <script>

                                ajaxForm();
                                function ajaxForm(){
                                  $('#send{{$image->id}}').click(function(){
                                    var data = new FormData($("#form")[0]);
                                      $.ajax({
                                          type: 'GET',
                                          url: "{{route('admin.image.goods.destroy')}}?id={{$image->id}}",
                                          data: data,
                                          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                          contentType: false,
                                          processData : false,
                                          success: function(data) {
                                              $('#image{{$image->id}}').remove();
                                          }
                                      });
                                      return false;
                                  });
                                }

                        </script>
                                @empty
                                @endforelse
                                <div class="clearfix"></div>
                                @endif
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        $('#sortpicture').on('change',function() {
                          //alert('asd');
                          var i = 1;
                          var files = $('#sortpicture').prop('files');
                          $.each(files, function (index, value) {
                            var file_data = value;
                            var form_data = new FormData();
                            form_data.append('file', file_data);
                            form_data.append('id', {{$product->id}});
                            $.ajax({
                                        url: '/admin/image/upload',
                                        dataType: 'text',
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        data: form_data,
                                        type: 'post',
                                        success: function(data){
                                          data = $.parseJSON(data);
                                            $('.imgs').append('<div class="image_group" id="image'+i+'" style="width:auto;float:left;">              <div class="image">                                                    <img src="{{asset('storage/app')}}/'+data.image+'" alt="">                                                    <a  class="delete" id="send'+i+'"></a>                                             </div>                                            </div>');
                                            $('#image'+i).click(function(){

                                              $.get('{{route('admin.image.goods.destroy')}}?id='+data.id,function(data){$('#image'+i).remove();});
                                            });
                                        }
                             });
                             i++;
                          });

                        });

                        </script>
                        </form>

                        <div class="perent">
                            <div class="left">
                                <label for="" class="item_name">Видео с YouTube:</label>
                            </div>
                            <div class="center">
                                <input type="text" name="youtube" class="" placeholder="Например: http://www.youtube.com/watch?v=9X-8JiOhzX4" value="{{$product->youtube or ''}}">
                            </div>
                            <div class="right">
                                <div class="help"></div>
                            </div>
                        </div>
                        <div class="perent" style="margin-top: 40px;">
                            <div class="left">&nbsp;</div>
                            <div class="center">
                                <input type="button" class="item_button" value="Сохранить и вернуться">
                            </div>
                            <div class="right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="supplier" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Добавление поставщика</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
          <label for=""><span class="label-supplier">Наименование:</span> <input type="text" name="supplierTitle" value="" class="form-supplier"></label><br>
          <label for=""><span class="label-supplier">Телефон:</span> <input type="text" name="supplierTel" value="" class="form-supplier"></label><br>
          <label for=""><span class="label-supplier">E-mail:</span> <input type="text" name="supplierEmail" value="" class="form-supplier"></label><br>
          <label for=""><span class="label-supplier">Контакт:</span> <input type="text" name="supplierName" value="" class="form-supplier"></label><br>
          <label for=""><span class="label-supplier">Адрес:</span> <input type="text" name="supplierAddress" value="" class="form-supplier"></label><br>
          <label for=""><span class="label-supplier">ИНН:</span> <input type="text" name="supplierINN" value="" class="form-supplier"></label><br>
          <label for=""><span class="label-supplier">КПП:</span> <input type="text" name="supplierKPP" value="" class="form-supplier"></label><br>
          <label for=""><span class="label-supplier">Комментарий:</span> <textarea name="supplierComment" rows="8" cols="80" class="form-supplier"></textarea></label>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary" id="newSupplier">Сохранить</button>
      </div>
    </div>
  </div>
</div>
        <script type="text/javascript">
        $('#newSupplier').click(function(){
          var title = $('input[name="supplierTitle"]').val();
          var tel = $('input[name="supplierTel"]').val();
          var email = $('input[name="supplierEmail"]').val();
          var name = $('input[name="supplierName"]').val();
          var address = $('input[name="supplierAddress"]').val();
          var inn = $('input[name="supplierINN"]').val();
          var kpp = $('input[name="supplierKPP"]').val();
          var comment = $('textarea[name="supplierComment"]').val();

          $.post('admin-salon/supplier/create', {title:title, tel:tel, email:email, name:name, inn:inn, kpp:kpp, comment:comment}, function(data){
            $('#suppliers').html(data);
            alert('Поставщик успешно добавлен!');
          });
        });
        $('#refresh').click(function(){
          $.get('{{route('admin.goods.index')}}', function(data){
            $('.admin-content').html(data);
          });
        });
        $('.item_button').click(function(){
          var text = CKEDITOR.instances['short_description'].getData();
          $.post('/admin-salon/goods/update/description', {short_description:text, id:{{$product->id}}}, function(data){
            $('.admin-content').html(data);
          });
        });
        </script>

        <script>

            $(document).ready( function() {
                $(".file-upload input[type=file]").change(function(){var filename = $(this).val().replace(/.*\\/, "");$("#filename").val(filename);});
            });
        </script>
        <script>
            $(document).ready(function() {
                $('input[type="checkbox"]').click(function() {
                    if($(this).attr('id') == 'watch') {
                      var val = 0;
                        if (this.checked) {$('#switch_seo').show(); val = 1;}
                        else {$('#switch_seo').hide(); val = 0;}
                        $.get('/admin/goods/checkseo/{{$product->id}}/'+val);
                    }
                });
            });
        </script>
        @if($product !== null)
        @if($product->checkseo == 1)
        <script>
            $(document).ready(function() {$('#switch_seo').show();});
        </script>
        @endif
        @endif
        <style media="screen">
          #addsupplier:hover{
            /* background-color: #d5d5d5; */
            box-shadow: 2px 2px 10px 2px #d5d5d5;
          }
          label{
            width:100%;
          }
          .form-supplier{
            background-color: #fff;
width: 30%;
height: 36px;
border: 1px solid #ccc;
font-size: 14px;
font-weight: 400;
border-radius: 5px;
color: #222;
padding-left: 5px;
          }
          .label-supplier{
            width:40%;
          }
        </style>
    <script type="text/javascript" src="/public/admin/js/jquery.mask.js"></script>
