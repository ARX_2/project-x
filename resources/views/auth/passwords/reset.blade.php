<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="" type="/img/png">
    <title>Сброс пароля</title>
    <meta name="robots" content="noindex, nofollow">
    <link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,500,600" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('public/services/services/css/style.css') }}">

</head>
<body>
<style>


    .background {
        width: 99vw;
        height: 100vh;
        background: rgb(171, 231, 219);
        display: flex;
        flex-direction: column;
        margin: 0;
    }
    .cat {
        display: flex;
        flex-direction: row;
        align-items: flex-end;
    }
    .head {
        width: 70px;
        height: 35px;
        position: relative;
        right: -10px;
        background: rgb(117, 82, 97);
        border-radius: 40px 40px 0px 0px;
    }
    .ears::before,
    .ears::after{
        content: "";
        width: 0;
        height: 0;
        position: absolute;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-bottom: 25px solid rgb(117, 82, 97);
    }
    .ears::before {
        top: -15px;
        transform: rotate(-27deg);
        animation: left-ear 5s ease-in-out infinite;
    }
    .ears::after {
        top: -15px;
        right: 10px;
    }
    .eyes::before,
    .eyes::after{
        content: "";
        width: 6px;
        height: 4px;
        position: absolute;
        top: 15px;
        border-bottom-left-radius: 110px;
        border-bottom-right-radius: 110px;
        border: 2px solid black;
        border-top: 0;
    }
    .eyes::before {
        left: 35px;
    }
    .eyes::after {
        left: 15px;
    }
    .nose {
        width: 6px;
        height: 6px;
        position: absolute;
        top: 22px;
        left: 27px;
        border-radius: 100%;
        background: rgb(253, 110, 114);
    }
    .buble {
        width: 2px;
        height: 2px;
        background: radial-gradient(circle, transparent, rgb(179, 190, 184));
        border-radius: 100%;
        position: relative;
        top: 3px;
        right: 5px;
        opacity: 0;
        animation: snore 4s ease-in 1s infinite;
    }
    .buble::before {
        content: "";
        width: 5px;
        height: 5px;
        border-left: 1px solid rgb(206, 239, 221);
        border-radius: 100%;
        position: absolute;
        top: 5px;
        left: 3px;
        animation: snore-buble 4s ease-in 1s infinite;
    }
    .puf {
        width: 20px;
        height: 20px;
        position: relative;
        right: 10px;
    }
    .puf::before,
    .puf::after {
        content:"";
        width: 10px;
        height: 10px;
        position: absolute;
        opacity: 0;
        animation: puf 500ms ease-in 1s infinite;
    }

    .puf::before {
        border-bottom: 1px solid rgb(179, 190, 184);
        border-right: 1px solid rgb(179, 190, 184);
        top: 0;
        left: 0;
    }
    .puf::after {
        border-top: 1px solid rgb(179, 190, 184);
        border-left: 1px solid rgb(179, 190, 184);
        bottom: 0;
        right: 0;
    }
    whiskers {

    }
    .one::before,
    .one::after{
        content: "";
        position: absolute;
        top: 20px;
        width: 30px;
        height: 1px;
        background: black;
    }
    .one::before {
        left: -5px;
        transform: rotate(20deg);

    }
    .one::after {
        left: 35px;
        transform: rotate(-20deg);
        z-index: 2;
    }
    .two::before,
    .two::after{
        content: "";
        position: absolute;
        top: 25px;
        width: 15px;
        height: 1px;
        background: black;
    }
    .two::before {
        left: -5px;
        transform: rotate(8deg);

    }
    .two::after {
        width: 10px;
        left: 45px;
        transform: rotate(-8deg);
        z-index: 2;
    }
    .three::before,
    .three::after{
        content: "";
        position: absolute;
        top: 30px;
        width: 25px;
        height: 1px;
        background: black;
    }
    .three::before {
        left: -5px;
        transform: rotate(-10deg);

    }
    .three::after {
        left: 35px;
        transform: rotate(10deg);
        z-index: 2;
    }
    .body {
        width: 120px;
        height: 60px;
        position: relative;
        right: 10px;
        background: rgb(117, 82, 97);
        border-radius: 60px 55px 0 0;
        animation: breath 4s ease-in infinite;
    }
    .tail {
        width: 13px;
        height: 90px;
        position: relative;
        right: 23px;
        top: 80px;
        border-radius: 10px;
        background: rgb(117, 82, 97);
    }
    .cat-tree {
        display: flex;
        flex-direction: column;
        justyify-content: center;
        align-items: center;
    }
    .tree-base {
        width: 3em;
        height: 12em;
        background: rgb(253, 249, 222);
        position: relative;
    }
    .tree-base::before,
    .tree-base::after{
        content: " ";
        position: absolute;
        top:46%;
        background-repeat: repeat;
        height: 1em;
        width: 12em;
        background-size: 12px 12px;
        background-image: radial-gradient(circle at 5px -6px, transparent 12px, rgb(171, 231, 219) 13px);
    }
    .tree-base::before {
        left: -95px;
        transform: rotate(90deg);
    }
    .tree-base::after{
        transform: rotate(-90deg);
        right: -95px;
    }
    .tree-bed {
        width: 190px;
        height: 13px;
        background: rgb(253, 110, 114);
        border-radius: 10px;
    }
    @keyframes left-ear {
        0%, 40%, 55%, 80%, 90%, 100% {
            top: -15px;
            transform: rotate(-27deg);
        }
        10%, 50%, 60%, 85%, 95% {
            transform: rotate(-45deg);
            top: -11px;
        }
    }
    @keyframes breath{
        0% {
            height: 60px;
        }
        20% {
            height: 65px;
        }
        30% {
            height: 69px;
        }
        40% {
            height: 65px;
        }
        60% {
            height: 60px;
        }
        80% {
            height: 64px;
        }
        100% {
            height: 60px;
        }
    }
    @keyframes snore{
        0% {
            opacity: 0;
        }
        33% {
            opacity: 1;
            height: 15px;
            width: 15px;
        }
        66% {
            opacity: 1;
            height: 5px;
            width: 5px;
        }
        99% {
            opacity: 1;
            height: 25px;
            width: 25px;
        }
    }
    @keyframes snore-buble {
        0% {
            width: 2px;
            height: 2px;
        }
        33% {
            width: 5px;
            height: 5px;
        }
        66% {
            width: 0;
            height: 0;
        }
        99% {
            width: 15px;
            height: 15px;
        }
    }
    @keyframes puf {
        0% {
            opacity: 0;
        }
        100% {
            opacity: 1;
        }
    }
    .cat-box{
        float: left;
        left: 15vw;
        bottom: 0;
        display: table;
        position: absolute;
    }
    form{
        display: table;
        background: #ffffff;
        padding: 40px 20px 40px;
    }
    h4  {
        margin-bottom: 30px;
    }
    .col-md-6{
        margin: 200px 0;
    }
    .col-md-6:first-child{
        height: 200px;
    }
    @media screen and (max-width: 800px){
        .background {
            padding-top: 80px;
            width: 100vw;
        }
        .col-md-6{
            margin: 0;
            width: 100%;
            flex: 0 0 100%;
            max-width: 100%;
        }
        form{
            margin: 0 auto;
        }
        .col-md-6:first-child{
            height: 0;
        }
        .cat-mobile{
            display: none;
        }
    }
</style>

<div class="background">
    <div class="row">
        <div class="col-md-6">

        </div>
        <div class="col-md-6" >

            <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-12 control-label">E-Mail Адрес</label>

                    <div class="col-md-12">
                        <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-12 control-label">Пароль</label>

                    <div class="col-md-12">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="col-md-12 control-label">Подтвердить пароль</label>
                    <div class="col-md-12">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Восстановить пароль
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>


<script src="{{ asset('public/services/services/js/jquery-3.2.1.slim.min.js') }}"></script>
<script src="{{ asset('public/services/services/js/ajax-popper.min.js') }}"></script>
<script src="{{ asset('public/services/services/js/bootstrap.min.js') }}"></script>


</body>
</html>
