<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="" type="/img/png">
    <title>Восстановление пароля</title>
    <meta name="robots" content="noindex, nofollow">
    <link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,500,600" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('public/services/services/css/style.css') }}">

</head>
<body>
<style>

    body{
        padding:0;
        margin:0;
    }
    .vid-container{
        position:relative;
        height:100vh;
        overflow:hidden;
    }
    .bgvid{
        position:absolute;
        left:0;
        top:0;
        height: 100vh;
        width: 100vw;
        object-fit: cover;
    }
    .inner-container{
        width:400px;
        height:320px;
        position:absolute;
        top:calc(50vh - 200px);
        left:calc(50vw - 200px);
        overflow:hidden;
    }
    .bgvid.inner{
        top:calc(-50vh + 200px);
        left:calc(-50vw + 200px);
        filter: url("data:image/svg+xml;utf9,<svg%20version='1.1'%20xmlns='https://www.w3.org/2000/svg'><filter%20id='blur'><feGaussianBlur%20stdDeviation='10'%20/></filter></svg>#blur");
        -webkit-filter:blur(10px);
        -ms-filter: blur(10px);
        -o-filter: blur(10px);
        filter:blur(10px);
    }
    .box{
        position:absolute;
        height:100%;
        width:100%;
        font-family:Helvetica;
        color:#fff;
        background:rgba(0,0,0,0.8);
        padding:30px 0px;
    }
    .box h1{
        text-align:center;
        margin:30px 0;
        font-size:30px;
    }
    .box input{
        display:block;
        width:300px;
        margin:20px auto;
        padding:15px;
        background:rgba(0,0,0,0.2);
        color:#fff;
        border:0;
    }
    .box input:focus,.box input:active,.box button:focus,.box button:active{
        outline:none;
    }
    .box button{
        background:#2ecc71;
        border:0;
        color:#fff;
        padding:10px;
        font-size:20px;
        width:330px;
        margin:20px auto;
        display:block;
        cursor:pointer;
    }
    .box button:active{
        background:#27ae60;
    }
    .box p{
        font-size:14px;
        text-align:center;
    }
    .box p span{
        cursor:pointer;
        color:#666;
    }
    .box .checkbox{
        width: 15px;
        margin: 5px;
        float: left;
    }
</style>
<div class="vid-container">

    <div class="inner-container">

        <div class="box">
            <h1>Восстановить пароль</h1>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <form class="" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" name="email" value="{{ old('email') }}" required placeholder="Введите вашу почту"/>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>


                <button type="submit">
                    Отправить
                </button>
            </form>


        </div>
    </div>
</div>






<script src="{{ asset('public/services/services/js/jquery-3.2.1.slim.min.js') }}"></script>
<script src="{{ asset('public/services/services/js/ajax-popper.min.js') }}"></script>
<script src="{{ asset('public/services/services/js/bootstrap.min.js') }}"></script>


</body>
</html>
