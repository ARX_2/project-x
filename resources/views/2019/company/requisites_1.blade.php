<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/about" itemprop="item"><span itemprop="name">О компании</span></a>
                <meta itemprop="position" content="2" />
            </li>
            <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <span itemprop="name">Реквизиты</span>
                <meta itemprop="position" content="3" />
            </li>
        </ol>
    </nav>
</section>
<style>
    .ncm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
    .ncm .title{font-weight:600;line-height:17px;font-size:17px;padding:15px;border-bottom:1px solid #e6e6e6}
    .ncm ul{display:block;padding:0;margin:0}
    .ncm .i{list-style:none;border-bottom:1px solid #e6e6e6;position:relative}
</style>
<section class="i80">
    <div class="row">
        <div class="col-3">
            <div class="ncm">
                <div class="title">Компания</div>
                <div class="menu-list">
                    <ul>
                        <li class="i"><a href="/about">О компании</a></li>
                        @forelse($sections as $i)
                            @if($i->slug == 'action')<li class="i"><a href="/action">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'documantation')<li class="i"><a href="/about/documents">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'jobs')<li class="i"><a href="/jobs">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'news')<li class="i"><a href="/news">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'blog')<li class="i"><a href="/blog">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'pricelist')<li class="i"><a href="/price-list">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'partners')<li class="i"><a href="/partners">{{$i->title}}</a></li>@endif
                        @empty
                        @endforelse
                        <li class="i"><a href="/about/requisites">Реквизиты</a></li>
                        <li class="i"><a href="/contact">Контакты</a></li>
                    </ul>
                </div>
            </div>
            @if(count($documents) != null)
            <div class="documents">
                <div class="title">Документы</div>
                <div class="documents_list">
                    @forelse($documents as $d)
                        <div class="item">
                            <div class="file file-{{$d->type}}"></div>
                            <div class="info">
                                <div class="name"><a href="/storage/app/{{$d->doc}}/">{{$d->title}}</a></div>
                                <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
            @endif
        </div>
        <style>
            .requisites{padding: 5px 10px;font-size: 15px;}
            .requisites ul{margin: 0;padding: 0;padding-bottom: 5px}
            .requisites li{padding: 0;list-style: none}
            .requisites li span{font-weight: 600}
        </style>
        <div class="col-9">
            <div class="c" style="margin-bottom: 0;">
                <div class="content_title"><h1>Реквизиты</h1>@if(Auth::user() !== null)<div class="in_admin_edit"><a target="_blank" href="/admin/admin/company/requisite">Редактировать</a></div>@endif</div>
                <div class="requisites">
                    <ul>
                        @if($company->info->full_name != null)<li><span>Полное наименование:</span> {{$company->info->full_name}}</li>@endif
                        @if($company->info->short_name != null)<li><span>Сокращенное наименование:</span> {{$company->info->short_name}}</li>@endif
                        @if($company->info->legal_address != null)<li><span>Юридический адрес:</span> {{$company->info->legal_address}}</li>@endif
                        @if($company->info->actual_address != null)<li><span>Фактический адрес:</span> {{$company->info->actual_address}}</li>@endif
                        @if($company->info->general_director != null)<li><span>Генеральный директор:</span> {{$company->info->general_director}}</li>@endif
                        @if($company->info->INN != null)<li><span>ИНН:</span> {{$company->info->INN}}</li>@endif
                        @if($company->info->KPP != null)<li><span>КПП:</span> {{$company->info->KPP}}</li>@endif
                        @if($company->info->OGRN != null)<li><span>ОГРН:</span> {{$company->info->OGRN}}</li>@endif
                        @if($company->info->OKPO != null)<li><span>ОКПО:</span> {{$company->info->OKPO}}</li>@endif
                        @if($company->info->OKBED != null)<li><span>ОКВЭД:</span> {{$company->info->OKBED}}</li>@endif
                        @if($company->info->OKATO != null)<li><span>ОКАТО:</span> {{$company->info->OKATO}}</li>@endif
                        <br>
                        @if($company->info->Acount_chek != null)<li><span>Расчетный счет:</span> {{$company->info->Acount_chek}}</li>@endif
                        @if($company->info->Bank != null)<li><span>Полное название банка:</span> {{$company->info->Bank}}</li>@endif
                        @if($company->info->BIK != null)<li><span>БИК:</span> {{$company->info->BIK}}</li>@endif
                        @if($company->info->Corp_check != null)<li><span>Корреспондентский счет:</span> {{$company->info->Corp_check}}</li>@endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>