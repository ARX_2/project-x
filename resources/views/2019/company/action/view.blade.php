<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/action" itemprop="item"><span itemprop="name">Акции</span></a>
                <meta itemprop="position" content="2" />
            </li>
            <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <span itemprop="name">{{$action->title}}</span>
                <meta itemprop="position" content="3" />
            </li>
        </ol>
    </nav>
</section>
<style>
    .ncm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
    .ncm .title{font-weight:600;line-height:17px;font-size:17px;padding:15px;border-bottom:1px solid #e6e6e6}
    .ncm ul{display:block;padding:0;margin:0}
    .ncm .i{list-style:none;border-bottom:1px solid #e6e6e6;position:relative}
</style>
<section class="i80">
    <div class="row">
        <div class="col-3">
            <div class="ncm">
                <div class="title">Компания</div>
                <div class="menu-list">
                    <ul>
                    <li class="i"><a href="/about">О компании</a></li>
                    @forelse($sections as $i)
                        @if($i->slug == 'action')<li class="i"><a href="/action">{{$i->title}}</a></li>@endif
                        @if($i->slug == 'documantation')<li class="i"><a href="/about/documents">{{$i->title}}</a></li>@endif
                        @if($i->slug == 'jobs')<li class="i"><a href="/jobs">{{$i->title}}</a></li>@endif
                        @if($i->slug == 'news')<li class="i"><a href="/news">{{$i->title}}</a></li>@endif
                        @if($i->slug == 'blog')<li class="i"><a href="/blog">{{$i->title}}</a></li>@endif
                        @if($i->slug == 'pricelist')<li class="i"><a href="/price-list">{{$i->title}}</a></li>@endif
                        @if($i->slug == 'partners')<li class="i"><a href="/partners">{{$i->title}}</a></li>@endif
                    @empty
                    @endforelse
                    <li class="i"><a href="/about/requisites">Реквизиты</a></li>
                    <li class="i"><a href="/contact">Контакты</a></li>
                    </ul>
                </div>
            </div>
            @if(count($documents) != null)
            <div class="documents">
                <div class="title">Документы</div>
                <div class="documents_list">
                    @forelse($documents as $d)
                        <div class="item">
                            <div class="file file-{{$d->type}}"></div>
                            <div class="info">
                                <div class="name"><a href="/storage/app/{{$d->doc}}/">{{$d->title}}</a></div>
                                <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
            @endif
        </div>
        <style>
            .c-n{width:100%;display:-ms-flexbox;display:-webkit-flex;display:-moz-flex;display:-ms-flex;display:flex;-webkit-flex-wrap:wrap;-moz-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
            .c-n .i{width:33.333%;float:left;padding:15px;position:relative}
            .c-n .i:before{position:absolute;content:"";top:50%;margin-top:-35%;right:0;height:70%;width:1px;border-right:1px solid #e6e6e6}
            .c-n .i:last-child:before{border:0}
            .c-n .i:nth-child(3n+3):before{border:0}
            .c-n .i .image{position:relative;width:100%;overflow:hidden;height:14vw;background-repeat:no-repeat;background-size:cover;background-position:center}
            .c-n .i .info{padding:10px 0 0}
            .c-n .i .name{width:100%;font-size:15px;line-height:15px;overflow:hidden;max-height:30px}
            .c-n .i .info_desc{font-size:14px;margin-top:10px;line-height:16px;color:#444}
            .c-n .i .info_date{font-size:14px;line-height:14px;color:#888;margin-top:7px}
            @media (min-width: 1400px) {
                .c-n .i .image{height:168px}
            }
        </style>
        <style>
            .cover-image{background-position:50%;background-size:cover;width:100%;height:326px;position:relative;overflow:hidden}
            .cover-image .blur{width:102%;height:103%;margin:-5px -9px;object-fit:contain;background-position:left;position:absolute;background-repeat:no-repeat;background-size:cover;-webkit-filter:blur(5px);-moz-filter:blur(5px);filter:blur(5px);filter:url(/public/site/img/blur.svg#blur)}
            .cover-image img{width:100%;height:100%;object-fit:contain;position:relative;z-index:1}
        </style>
        <style>
            .c-n{width:100%;display:-ms-flexbox;display:-webkit-flex;display:-moz-flex;display:-ms-flex;display:flex;-webkit-flex-wrap:wrap;-moz-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
            .c-n .i{width:33.333%;float:left;padding:15px;position:relative}
            .c-n .i:before{position:absolute;content:"";top:50%;margin-top:-35%;right:0;height:70%;width:1px;border-right:1px solid #e6e6e6}
            .c-n .i:last-child:before{border:0}
            .c-n .i:nth-child(3n+3):before{border:0}
            .c-n .i .image{position:relative;width:100%;overflow:hidden;height:14vw;background-repeat:no-repeat;background-size:cover;background-position:center}
            .c-n .i .info{padding:10px 0 0}
            .c-n .i .name{width:100%;font-size:15px;line-height:15px;overflow:hidden;max-height:30px}
            .c-n .i .info_desc{font-size:14px;margin-top:10px;line-height:16px;color:#444}
            .c-n .i .info_date{font-size:14px;line-height:14px;color:#888;margin-top:7px}
            @media (min-width: 1400px) {
                .c-n .i .image{height:168px}
            }
        </style>
        <div class="col-9">
            <div class="c" style="margin-bottom: 0">
                <div class="cover-image">
                    <div class="blur" style="background-image: url(/storage/app/{{Resize::ResizeImage($action, '83x33')}});"></div>
                    <img src="/storage/app/{{Resize::ResizeImage($action, '500x325')}}" alt="">
                </div>
            </div>
            <div class="c grey" style="margin-bottom: 0">
                <div class="content_title"><h1>{{$action->title}}</h1><div class="data">id акции: {{$action->id}}, сроки проведения: {{$action->datefrom}} - {{$action->dateto}} </div></div>
            </div>
            <div class="c">
                <div class="description">{{$action->text}}</div>
            </div>
            <div class="c grey">
                <div class="content_title">Другие акции</div>
                <div class="c-n">
                    @forelse($similar as $i)
                    <div class="i">
                        <a href="/action/{{$i->id}}-{{$i->slug}}/">
                            <div class="image" style="background-image: url(/storage/app/{{Resize::ResizeImage($i, '250x165')}});"><div class="img_dark"></div></div>
                            <div class="info">
                                <div class="name"><span>{{$i->title}}</span></div>
                                <div class="info_desc"><p>{{$i->offer}}</p></div>
                                <div class="info_date"><p>{{$i->datefrom}} - {{$i->dateto}}</p></div>
                            </div>
                        </a>
                    </div>
                    @empty
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>