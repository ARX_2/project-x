<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <span itemprop="name">{{$settings->title}}</span>
                <meta itemprop="position" content="2" />
            </li>
        </ol>
    </nav>
</section>
<style>
    .ncm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
    .ncm .title{font-weight:600;line-height:17px;font-size:17px;padding:15px;border-bottom:1px solid #e6e6e6}
    .ncm ul{display:block;padding:0;margin:0}
    .ncm .i{list-style:none;border-bottom:1px solid #e6e6e6;position:relative}
</style>
<section class="i80">
    <div class="row">
        <div class="col-3">
            <div class="ncm">
                <div class="title">Компания</div>
                <div class="menu-list">
                    <ul>
                        <li class="i"><a href="/about">О компании</a></li>
                        @forelse($sections as $i)
                            @if($i->slug == 'action')<li class="i"><a href="/action">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'documantation')<li class="i"><a href="/about/documents">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'jobs')<li class="i"><a href="/jobs">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'news')<li class="i"><a href="/news">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'blog')<li class="i"><a href="/blog">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'pricelist')<li class="i"><a href="/price-list">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'partners')<li class="i"><a href="/partners">{{$i->title}}</a></li>@endif
                        @empty
                        @endforelse
                        <li class="i"><a href="/about/requisites">Реквизиты</a></li>
                        <li class="i"><a href="/contact">Контакты</a></li>
                    </ul>
                </div>
            </div>
            @if(count($documents) != null)
                <div class="documents">
                    <div class="title">Документы</div>
                    <div class="documents_list">
                        @forelse($documents as $d)
                            <div class="item">
                                <div class="file file-{{$d->type}}"></div>
                                <div class="info">
                                    <div class="name"><a href="/storage/app/{{$d->doc}}/">{{$d->title}}</a></div>
                                    <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            @endif
        </div>
        <style>.contact{width:100%;margin-top:10px;padding:0 8px;display:-ms-flexbox;display:-webkit-flex;display:-moz-flex;display:-ms-flex;display:flex;-webkit-flex-wrap:wrap;-moz-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap}.contact .block{width:50%;float:left}.contact .data{display:flex;width:100%;font-size:15px}.contact .phone:before{content:"\f095 "}.contact .email:before{content:"\f0e0";font-size:21px}.contact .address:before{content:"\f041"}.contact .timework:before{content:"\f017"}.contact .i{display:flex;width:100%;margin-bottom:15px}.contact .file{font-family:FontAwesome;font-size:23px;margin-right:10px;vertical-align:middle;background:#269ffd;color:#fff;border-radius:3px;width:35px;height:35px;min-width:35px;text-align:center;display:inline-block}.contact .i .info{display:inline-block;font-size:14px;line-height:16px}.contact .i .info .name{font-weight:600;margin-bottom:3px}.contact .i .info span{display:block;margin-bottom:5px}.contact .i .info span:last-child{margin-bottom:0}.contact .i .info span a{display:block}</style>
        <div class="col-9">
            <div class="c">
                <div class="content_title"><h1>{{$settings->title}}</h1>@if(Auth::user() !== null)<div class="in_admin_edit"><a target="_blank" href="/admin/company">Редактировать</a></div>@endif</div>
                <div class="description">{!! $settings->text !!}</div>
                <div class="contact">
                    <div class="block">
                        <div class="i">
                            <div class="file phone"></div>
                            <div class="info"><div class="name">Телефон:</div>
                                @forelse($tels as $i)
                                <span>{{$i->tel}} @if($i->title != null)- {{$i->title}} @endif</span>
                                @empty
                                @endforelse
                            </div>
                        </div>
                        <div class="i">
                            <div class="file address"></div>
                            <div class="info"><div class="name">Адрес:</div><span>@if($company->cities->gorod)г. {{$company->cities->gorod}}, @endif @if($info->address){{$info->address}}@endif</span></div>
                        </div>

                    </div>
                    <div class="block">
                        <div class="i">
                            <div class="file email"></div>
                            <div class="info"><div class="name">Почта:</div>
                                @forelse($emails as $i)
                                    <span>{{$i->email}} @if($i->title != null)- {{$i->title}} @endif</span>
                                @empty
                                @endforelse
                            </div>
                        </div>
                        <div class="i">
                            <div class="file timework"></div>
                            <div class="info"><div class="name">Время работы:</div><span>{{$info->timeJob}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="map">
                <div class="title"><span>Карта</span></div>
                <div class="back-map big">
                    {!! $info->map !!}
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $('.map_b').click(function(){
            $('.block_map_b').hide();
            $('.map-view').hide();
            $('.mapa').show();
        });
    });
</script>