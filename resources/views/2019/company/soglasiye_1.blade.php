<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <span itemprop="name">Согласие на обработку персональных данных</span>
                <meta itemprop="position" content="2" />
            </li>
        </ol>
    </nav>
</section>
<div class="i80">
    <div class="c" style="padding: 25px;font-size: 14px;color: #444;margin-bottom: 0;" >
        <div class="content_title" style="    padding: 0;"><h1>Согласие на обработку персональных данных</h1></div>
<p>Настоящим в соответствии с Федеральным законом № 152-ФЗ «О персональных данных» от 27.07.2006 года Вы подтверждаете свое согласие на обработку персональных данных: сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование, передачу исключительно в целях регистрации программного обеспечения на Ваше имя, как это описано ниже, блокирование, обезличивание, уничтожение. Мы гарантируем конфиденциальность получаемой нами информации. Обработка персональных данных осуществляется в целях эффективного исполнения заказов, договоров и иных обязательств, принятых в качестве обязательных к исполнению перед Вами.</p>

    <p>Настоящим особо оговаривается, что в случае необходимости предоставления Ваших персональных данных правообладателю, дистрибьютору или реселлеру программного обеспечения в целях регистрации программного обеспечения на Ваше имя, Вы даёте согласие на передачу Ваших персональных данных, при этом мы гарантируем, что правообладатель, дистрибьютор или реселлер программного обеспечения будет осуществлять защиту Ваших персональных данных на условиях аналогичных изложенным в Политике конфиденциальности персональных данных.</p>

    <p>Настоящее согласие распространяется на следующие Ваши персональные данные: фамилия, имя и отчество, адрес электронной почты, почтовый адрес доставки заказов, контактный телефон, платёжные реквизиты.</p>

    <p>Срок действия Вашего согласия является неограниченным, однако, Вы вправе в любой момент отозвать настоящее согласие, путём направления письма на электронную почту {{$emails->email}}. Обращаем Ваше внимание, что отзыв Вашего согласия на обработку персональных данных влечёт за собой удаление Вашей учётной записи с Интернет-сайта {{$canonical}}, а также уничтожение записей, содержащих Ваши персональные данные, в системах обработки персональных данных.</p>
    </div>
</div>