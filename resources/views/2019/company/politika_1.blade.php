<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <span itemprop="name">Политика конфиденциальности</span>
                <meta itemprop="position" content="2" />
            </li>
        </ol>
    </nav>
</section>
<div class="i80">
    <div class="c" style="padding: 25px;font-size: 14px;color: #444;margin-bottom: 0;" >
        <div class="content_title" style="    padding: 0;"><h1>Политика конфиденциальности</h1></div>
        <p>Настоящая Политика конфиденциальности персональных данных (далее — Политика) действует в отношении всех персональных данных, которые мы можем получить от пользователя во время использования пользователями официального сайта {{$canonical}} (далее – Сайт), использования Интернет-сервисов, услуг, покупки программного обеспечения, участия в рекламных и маркетинговых кампаниях или акциях и/или ином взаимодействии с нашей компанией (далее – Услуги).</p>
        <p>Мы не контролируем и не несет ответственность за сайты третьих лиц, на которые пользователь может перейти по ссылкам, доступным на сайте {{$canonical}}. На сайтах третьих лиц может быть собственная политика конфиденциальности и у пользователя могут собираться или запрашиваться иные персональные данные.</p>
        <p>Настоящая Политика объясняет, каким образом Сайт обрабатывает и защищает персональную информацию пользователей.</p>
        <p>Используя Услуги и предоставляя Сайту информацию, необходимую для инициирования дальнейшего взаимодействия, Вы выражаете согласие на ее использование в соответствии с настоящей Политикой.</p>
        <p>Для конкретных Услуг, Сайт может публиковать дополнительные положения, дополняющие настоящую Политику.</p>
        <p>1. Персональные данные пользователей, которые получает и обрабатывает Сайт</p>
        <p>1.1. В рамках настоящей Политики под «персональными данными пользователя» понимаются:</p>
        <p>1.1.1. Персональные данные, которые пользователь предоставляет о себе самостоятельно при регистрации (создании учётной записи) на Сайте или в процессе использования Услуг. Обязательная для предоставления (оказания) Услуг информация явно обозначена. К такой информации отнесены: фамилия, имя и отчество, адрес электронной почты, почтовый адрес доставки заказов, контактный телефон, платёжные реквизиты.</p>
        <p>1.1.2. Иная информация о пользователе, сбор и/или предоставление которой определено Сайтом для предоставления отдельных Услуг дополнительно, о чём явно указано при заказе отдельных Услуг.</p>
        <p>1.2. Персональные данные пользователя, предоставленные Сайту, считаются не достоверными и могут быть блокированы до момента получения от пользователя или его законного представителя согласия на обработку персональных данных пользователя в любой дополнительно обозначенной Сайтом  форме, помимо предусмотренной на Сайте.</p>
        <p>2. Цели обработки персональной информации пользователей</p>
        <p>2.1. Сайт обрабатывает только те персональные данные, которые необходимы для оказания и улучшения качества Услуг.</p>
        <p>2.2. Персональную информацию пользователя Сайта может использовать в следующих целях:</p>
        <p>2.2.1. Идентификация стороны в рамках оказания Услуги.</p>
        <p>2.2.2. Предоставление пользователю персонализированных Услуг.</p>
        <p>2.2.3. Улучшение качества Услуг и разработка новых.</p>
        <p>2.2.4. Проведение статистических и иных исследований на основе обезличенных данных.</p>
        <p>2.2.5. Предоставление персональных данных пользователя правообладателям, дистрибьюторам или реселлерам программного обеспечения в целях регистрации программного обеспечения на имя пользователя или организации, интересы которой представляет пользователь.</p>
        <p>2.2.6. Осуществление и/или исполнение функций, полномочий и обязанностей, возложенных законодательством Российской Федерации на {{$canonical}} .</p>
        <p>3. Передача персональных данных пользователя третьим лицам</p>
        <p>3.1. В отношении персональных данных пользователя сохраняется конфиденциальность, кроме случаев обработки персональных данных, доступ неограниченного круга лиц к которым предоставлен пользователем либо по его просьбе.</p>
        <p>3.2. {{$canonical}} вправе передать персональную информацию пользователя третьим лицам в следующих случаях:</p>
        <p>3.2.1. Пользователь предоставил свое согласие на такие действия, в том числе в целях регистрации программного обеспечения на имя пользователя правообладателями, дистрибьюторами или реселлерами программного обеспечения.</p>
        <p>3.2.2. Передача необходима для достижения целей, осуществления и выполнения функций, полномочий и обязанностей, возложенных законодательством Российской Федерации на Сайт.</p>
        <p>4. Меры, применяемые для защиты персональных данных пользователей</p>
        <p>Сайт принимает необходимые и достаточные организационные и технические меры для защиты персональных данных пользователя от неправомерного или случайного доступа, уничтожения, изменения, блокирования, копирования, распространения, а также от иных неправомерных действий с персональными данными третьих лиц.</p>
        <p>5. Права и обязанности пользователя</p>
        <p>5.1. Сайт предпринимает разумные меры для поддержания точности и актуальности имеющихся у {{$canonical}} персональных данных, а также удаления устаревших и других недостоверных или излишних персональных данных, тем не менее, Пользователь несёт ответственность за предоставление достоверных сведений, а также за обновление предоставленных данных в случае каких-либо изменений.</p>
        <p>5.2. Пользователь может в любой момент изменить (обновить, дополнить) предоставленную им персональную информацию или её часть, а также параметры её конфиденциальности путем обращения по электронной почте {{$emails->email}} .</p>
        <p>5.2.1. Пользователь вправе в любой момент отозвать согласие на обработку Сайтом персональных данных путём направления письма на электронную почту {{$emails->email}} с пометкой «отзыв согласия на обработку персональных данных», при этом что отзыв пользователем согласия на обработку персональных данных влечёт за собой удаление учётной записи пользователя с Сайта, а также уничтожение записей, содержащих персональные данные, в системах обработки персональных данных Сайтом, что может сделать невозможным пользование Интернет-сервисами {{$canonical}} .</p>
        <p>5.3. Пользователь имеет право на получение информации, касающейся обработки его персональных данных в {{$canonical}} для чего вправе направить письмо на электронную почту {{$emails->email}} с пометкой «запрос информации о порядке обработки персональных данных».</p>
        <p>5.4. Для исполнения положений в п. 5.2 и 5.3 настоящей Политики Сайта может потребовать подтвердить личность пользователя, затребовав предоставления такого подтверждения в любой непротиворечащей закону форме.</p>
        <p>6. Обратная связь. Вопросы и предложения</p>
        <p>Все предложения или вопросы по поводу настоящей Политики следует сообщать по адресу электронной почты {{$emails->email}}</p>
    </div>
</div>