<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/about" itemprop="item"><span itemprop="name">О компании</span></a>
                <meta itemprop="position" content="2" />
            </li>
            <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <span itemprop="name">Прайс-лист</span>
                <meta itemprop="position" content="3" />
            </li>
        </ol>
    </nav>
</section>
<style>
    .ncm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
    .ncm .title{font-weight:600;line-height:17px;font-size:17px;padding:15px;border-bottom:1px solid #e6e6e6}
    .ncm ul{display:block;padding:0;margin:0}
    .ncm .i{list-style:none;border-bottom:1px solid #e6e6e6;position:relative}
</style>
<section class="i80">
    <div class="row">
        <div class="col-3">
            <div class="ncm">
                <div class="title">Компания</div>
                <div class="menu-list">
                    <ul>
                        <li class="i"><a href="/about">О компании</a></li>
                        @forelse($sections as $i)
                            @if($i->slug == 'action')<li class="i"><a href="/action">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'documantation')<li class="i"><a href="/about/documents">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'jobs')<li class="i"><a href="/jobs">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'news')<li class="i"><a href="/news">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'blog')<li class="i"><a href="/blog">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'pricelist')<li class="i"><a href="/price-list">{{$i->title}}</a></li>@endif
                            @if($i->slug == 'partners')<li class="i"><a href="/partners">{{$i->title}}</a></li>@endif
                        @empty
                        @endforelse
                        <li class="i"><a href="/about/requisites">Реквизиты</a></li>
                        <li class="i"><a href="/contact">Контакты</a></li>
                    </ul>
                </div>
            </div>
            @if(count($documents) != null)
            <div class="documents">
                <div class="title">Документы</div>
                <div class="documents_list">
                    @forelse($documents as $d)
                        <div class="item">
                            <div class="file file-{{$d->type}}"></div>
                            <div class="info">
                                <div class="name"><a href="/storage/app/{{$d->doc}}/">{{$d->title}}</a></div>
                                <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
            @endif
        </div>
        <style>
            .pricelist .block{font-size: 14px;display:table;width:100%;}
            .pricelist .block .head_list{font-weight:600}
            .pricelist .block .head_list .item{border-bottom:none}
            .pricelist .block .item{width:100%;padding:15px;display:table;position:relative;border-bottom:1px solid #ddd}
            .pricelist .block .item:hover{background:#f3f3f3}
            .pricelist .block .item:last-child{border-bottom:none}
            .pricelist .block .item .name{width:75%;float:left;padding-right:40px}
            .pricelist .block .item .unit{width:10%;float:left;padding-right:10px;text-align:center}
            .pricelist .block .item .price{width:10%;float:left;text-align:center}
            .pricelist .block .item.add{color:#ddd}
            .pricelist .block .item.add .name{color:#0095eb}
        </style>
        <div class="col-9">
            @isset($settings->image_one)
                <div class="c" style="margin-bottom: 0">
                    <div class="cover" style="height:200px;background-image: url('/storage/app/{{Resize::ResizeImage($settings->image_one, "840x200")}}');background-position: 50%;background-size: cover;margin: -5px;"></div>
                </div>
            @endif
            <div class="c grey">
                <div class="content_title"><h1>{{$settings->title}}</h1>@if(Auth::user() !== null)<div class="in_admin_edit"><a target="_blank" href="/admin/admin/company/upload?type=11">Редактировать</a></div>@endif</div>
            </div>
            @forelse($pricelist_categories as $p)
                <div class="c pricelist">
                    <div class="block">
                        <div class="head_list">
                            <div class="item">
                                <div class="name">{{$p->title}}</div>
                                <div class="unit">Ед.</div>
                                <div class="price">Цена</div>
                            </div>
                        </div>
                        @forelse($p->pricelist_item as $i)
                        <div class="item">
                            <div class="name">{{$i->title}}</div>
                            <div class="unit">{{$i->count}}</div>
                            <div class="price">{{$i->coast}}</div>
                        </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            @empty
            @endforelse
            <div class="c">
                <div class="description">{!! $settings->text !!}</div>
            </div>
        </div>
    </div>
</section>