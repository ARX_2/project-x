<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Отзывы к категориям</li>
        </ol>
    </nav>
</section>
<style>
    .acr{width: 100%;display: table}
    .content_title {
        margin: 10px 0 5px;
        font-size: 17px;
        line-height: 17px;
        font-weight: 600;
        padding-left: 10px;
        padding-bottom: 15px;
        border-bottom: 1px solid #e6e6e6;
    }
    .content_title h2 {
        font-size: 17px;
        line-height: 17px;
        font-weight: 600;
        margin: 0;
    }
    .acr .box{background: #fff;
        padding: 5px;}
    .acr .box{width: 100%;margin-bottom: 40px}
    .acr .item{width: 100%;
        display: inline-flex;
        padding: 10px;
        border-bottom: 1px solid #ddd;}
    .acr .item:hover{    background: #f5f5f5;}
    .acr .item:last-child{border-bottom: none;margin-bottom: 0;padding-bottom: 0}
    .acr .item .left{width: 33.333%;display: inline-flex}
    .info{margin-left: 10px;}
    .name{
        font-weight: 600;
        font-size: 15px;
        line-height: 15px;margin-bottom: 5px}
    .desc{font-size: 13px}
    .acr .item .left .image{width: 80px;height: 80px}
    .acr .item .left .image img{width: 100%;height: 100%;object-fit: cover}
    .acr .item .center{width: 33.333%;text-align: center;font-size: 15px;font-weight: 600;line-height: 15px}
    .acr .item .left.full{color: #bbb}
    .acr .item .center.full{color: #bbb}
    .acr .item .right{width: 33.333%;text-align: right}
    .button_reviews{display: table;
        float: right;
        font-size: 15px;
        background: #32a601;
        padding: 5px 15px;
        color: #fff;
        font-weight: 600;}
    .button_reviews.full{background: #bbb;}
</style>
<style>
    .subitem i{    width: 40px;
        font-size: 20px;
        text-align: center;color: #aaa;
        line-height: 40px;}
    .subitem .item .left .image  {
        width: 40px;
        height: 40px;
    }
    .acr .subitem .item{border-bottom: none}
    .acr .subitem .item:last-child{
        padding: 10px;
        border-bottom: 1px solid #ddd;}

</style>
<section class="i80">
    <div class="acr">
        <div class="box">
            <div class="content_title"><h2>Отзывы</h2></div>
            @forelse($category as $c)
                @if($c->section != null)
            <div class="item">

                <div class="left @if(count($c->reviews) > 6) full @endif">
                    <div class="image"><img src="/storage/app/{{Resize::ResizeImage($c->images, '80x80')}}" alt=""></div>
                    <div class="info">
                        <div class="name">{{$c->title}}</div>
                        <div class="desc">Раздел: {{$c->section->title}}</div>

                    </div>
                </div>
                <div class="center @if(count($c->reviews) > 6) full @endif">
                    @if(count($c->reviews) == 0) 0 @endif
                    @if(count($c->reviews) == 1) 1 @endif
                    @if(count($c->reviews) == 2) 2 @endif
                    @if(count($c->reviews) == 3) 3 @endif
                    @if(count($c->reviews) == 4) 4 @endif
                    @if(count($c->reviews) == 5) 5 @endif
                    @if(count($c->reviews) == 6) 6 @endif
                    @if(count($c->reviews) == 7) 7 @endif
                    @if(count($c->reviews) == 8) 8 @endif
                    @if(count($c->reviews) == 9) 9 @endif
                    @if(count($c->reviews) > 9) >9 @endif
                    из 6
                </div>

                <div class="right">
                    @if(count($c->reviews) > 6)
                    @if(Auth::user() !== null)
                        <a href="/{{$c->section->slug}}/{{$c->id}}-{{$c->slug}}" target="_blank"><div class="button_reviews full">Добавить отзыв</div></a>
                    @else
                        <div class="button_reviews full">Добавить отзыв</div>
                    @endif
                    @else
                    <a href="/{{$c->section->slug}}/{{$c->id}}-{{$c->slug}}" target="_blank"><div class="button_reviews"> Добавить отзыв</div></a>
                    @endif
                </div>
            </div>
            @if($c->sub_category_nav != null)
            <div class="subitem">
            @forelse($c->sub_category_nav as $sub)
                <div class="item">
                    <div class="left @if(count($sub->reviews) > 6) full @endif">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        
                        <div class="image">@if(($sub->images) == null)<img src="/storage/app/{{Resize::ResizeImage($sub->images, '80x80')}}" alt=""> @endif</div>
                        <div class="info">
                            <div class="name">{{$sub->title}}</div>
                            <div class="desc">Раздел: {{$c->section->title}}</div>
                        </div>
                    </div>
                    <div class="center @if(count($sub->reviews) > 6) full @endif">
                        @if(count($sub->reviews) == 0) 0 @endif
                        @if(count($sub->reviews) == 1) 1 @endif
                        @if(count($sub->reviews) == 2) 2 @endif
                        @if(count($sub->reviews) == 3) 3 @endif
                        @if(count($sub->reviews) == 4) 4 @endif
                        @if(count($sub->reviews) == 5) 5 @endif
                        @if(count($sub->reviews) == 6) 6 @endif
                        @if(count($sub->reviews) == 7) 7 @endif
                        @if(count($sub->reviews) == 8) 8 @endif
                        @if(count($sub->reviews) == 9) 9 @endif
                        @if(count($sub->reviews) > 9) >9 @endif
                        из 6
                    </div>
                    <div class="right">
                        @if(count($sub->reviews) > 6)
                            @if(Auth::user() !== null)
                                <a href="/{{$c->section->slug}}/{{$sub->id}}-{{$sub->slug}}" target="_blank"><div class="button_reviews full">Добавить отзыв</div></a>
                            @else
                                <div class="button_reviews full">Добавить отзыв</div>
                            @endif
                        @else
                        <a href="/{{$c->section->slug}}/{{$sub->id}}-{{$sub->slug}}" target="_blank"><div class="button_reviews">Добавить отзыв</div></a>
                        @endif
                    </div>
                </div>
            @empty
            @endforelse
            </div>
            @endif
            @endif
            @empty
            @endforelse
        </div>
        
    </div>
    <div class="revinfo">
        Всего отзывов: @if(count($reviews) > 0) {{count($reviews)}} @endif
    </div>
</section>
