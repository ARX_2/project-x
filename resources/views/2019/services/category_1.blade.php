<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/services" itemprop="item"><span itemprop="name">Услуги</span></a>
                <meta itemprop="position" content="2" />
            </li>
            @if($category->bread_cat == null)
                <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{$category->title}}</span>
                    <meta itemprop="position" content="3" />
                </li>
            @else
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/services/{{$category->bread_cat->id}}-{{$category->bread_cat->slug}}" itemprop="item"><span itemprop="name">{{$category->bread_cat->title}}</span></a>
                    <meta itemprop="position" content="3" />
                </li>
                <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{$category->title}}</span>
                    <meta itemprop="position" content="4" />
                </li>
            @endif
        </ol>
    </nav>
</section>
<style>.nsm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}.nsm .title{padding:15px 15px 0;line-height:17px;font-size:17px;margin-bottom:10px;font-weight:600}.nsm ul{list-style:none;padding:0;margin:0}.nsm .parent_cat{position:relative;color:#666;font-weight:600;border-bottom:1px solid #e6e6e6;cursor:pointer}.nsm ul .parent_cat{padding:0 15px}.nsm ul .parent_cat{padding:0 15px}.nsm .parent_cat span{display:table-cell;padding:10px 20px 10px 0;width:100%;color:#007bff}</style>
<section class="i80">
    <div class="page_category_2">
        <div class="row">
            <div class="col-3">
                <div class="nsm">
                    <div class="title">Категории</div>
                    <div class="menu-list">
                        <ul class="menu-content">
                            @forelse($category_nav as $c)
                                <li class="cat-item @if(count($c->sub_category_nav)>0) collapse @else 111 @endif">
                                    <div class="parent_cat @if($c->id == $category->id) open_cat @else @endif" @if(count($c->sub_category_nav)>0)onclick="smFunction('sm-{{$c->id}}')" @else  @endif>
                                        @if(count($c->sub_category_nav) == 0)
                                            <a href="/services/{{$c->id}}-{{$c->slug or ''}}"><span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or ''}}@endif</span></a>
                                        @else
                                            <span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or ''}}@endif</span>
                                        @endif
                                    </div>
                                    @if(count($c->sub_category_nav)>0)

                                        <ul class="sub-menu collapsed @if($c->id == $category_id || $c->open_cat == 1 || $category->parent_id == $c->id)active @endif" id="sm-{{$c->id}}">
                                            @forelse($c->sub_category_nav as $sub)
                                                <li class="sub_cat @if($sub->id == $category->id) open_cat @else @endif"><a href="/services/{{$sub->id}}-{{$sub->slug or ''}}">@if($sub->seoname != null){{$sub->seoname}}@else{{$sub->title or ''}}@endif</a></li>
                                            @empty
                                            @endforelse
                                        </ul>
                                    @endif
                                </li>
                            @empty
                            @endforelse
                        </ul>
                    </div>
                </div>
                @if(count($documents) != null)
                <div class="documents">
                    <div class="title">Документы</div>
                    <div class="documents_list">
                        @forelse($documents as $d)
                            <div class="item">
                                <div class="file file-{{$d->type}}"></div>
                                <div class="info">
                                    <div class="name"><a href="/storage/app/{{$d->doc}}/" target="_blank">{{$d->title}}</a></div>
                                    <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
                @endif
            </div>
            <style>
                .c-c_l{width:100%;display:table;margin-top:10px}
                .c-c_l .i{float:left;width:32.333%;display:block;position:relative;margin:.5%}
                .c-c_l .i a{display:table;color:#fff;width:100%}
                .c-c_l .i .image{width:100%;height:16vw;width:100%;height:15vw;position:relative;overflow:hidden}
                .c-c_l .i .image img{width:100%;height:100%;object-fit:cover;-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-o-transition:all .3s ease;transition:all .3s ease}
                .c-c_l .i .dark{opacity:0;background-color:rgba(0,0,0,.2);-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=0);-webkit-transition:opacity .8s cubic-bezier(.19,1,.22,1);transition:opacity .8s cubic-bezier(.19,1,.22,1);position:absolute;display:block;width:100%;height:100%;z-index:2}
                .c-c_l .i:hover .dark{opacity:1}
                .c-c_l .i:hover img{-webkit-transform:scale(1.1);-ms-transform:scale(1.1);transform:scale(1.1)}
                .c-c_l .i .info{padding:0;position:absolute;bottom:10px;right:20px;left:20px;z-index:3}
                .c-c_l .i .name{margin-bottom:10px;width:100%;font-size:17px;line-height:18px;overflow:hidden;font-weight:600}
                .c-c_l .i .name span{background:#269ffd;padding:0 .5em .15em .1em;line-height:1.4}
                @media (min-width: 1400px) {
                    .c-c_l .i .image{height:176px}
                }
                @media screen and (max-width: 992px) {
                    .c-c_l .i .image{height:20.7vw}
                }
                @media screen and (max-width: 575px) {
                    .c-c_l .i{width:49%}
                    .c-c_l .i .image{height:31vw}
                }
                @media screen and (max-width: 480px) {
                    .c-c_l .i{width:99%}
                    .c-c_l .i .image{height:63vw}
                    .c-c_l .i .name{font-size:18px}
                }
            </style>
            <div class="col-9">
                @if($category->cover !== null)
                <div class="c" style="margin-bottom: 0">
                    <div class="cover" style="background-image: url('/storage/app/{{Resize::ResizeImage($category->cover, '847x200')}}')"></div>
                </div>
                @elseif($category->images !== null)
                <div class="c" style="margin-bottom: 0">
                    <div class="cover" style="background-image: url('/storage/app/{{Resize::ResizeImage($category->images, '847x200')}}')"></div>
                </div>
                @endif
                <div class="c">
                    <div class="content_title"><h1>{{$category->title}}</h1>@if(Auth::user() !== null)<div class="data"><a target="_blank" href="/admin/category/{{$category->id}}/edit">Редактировать</a></div>@endif</div>
                    @if($category->offer != null)<div class="description"><p>{!! $category->offer !!}</p></div>@endif
                    @if(count($sub_category) != null)
                    <div class="c-c_l">
                        @forelse($sub_category as $c)
                            <div class="i">
                                <a href="/services/{{$c->id}}-{{$c->slug or 'error-slug'}}">
                                    <div class="image">
                                        <div class="dark"></div>
                                        <img src="/storage/app/{{Resize::ResizeImage($c->images, '270x176')}}" alt="{{$c->title or 'Ошибка: alt'}}"></div>
                                    <div class="info">
                                        <div class="name"><span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or 'Ошибка: Название категории'}}@endif</span></div>
                                    </div>
                                </a>
                            </div>
                        @empty
                        @endforelse
                    </div>
                    @endif
                </div>
                @if(count($goods) != null)
                <div class="c grey">
                    <div class="content_title"><h2>Услуги</h2></div>
                    <div class="c-g">
                        @if($settings->size == 1)<div class="c-g_l c-g_l-xl">@else <div class="c-g_l c-g_l-md"> @endif
                        @forelse($goods as $g)
                            <div class="item">
                                <a href="/services/{{$g->product_url_cat->id}}-{{$g->product_url_cat->slug}}/{{$g->id}}-{{$g->slug}}">
                                    <div class="image"><div class="img_dark"></div>
                                        @if($settings->size == 1)
                                            <img id="ImageGoods{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '400x270')}}" alt="{{$g->title or ''}}">
                                        @else
                                            <img id="ImageGoods{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '185x185')}}" alt="{{$g->title or ''}}">
                                        @endif
                                    </div>
                                    <div class="info">
                                        <div class="name"><span>{{$g->title or ''}}</span></div>
                                        <div class="prices">
                                            <div class="minimum">Цена:</div>
                                            <div class="price-content">@if($g->coast != null){{$g->coast or ''}} р. @else Цена не указана @endif</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @empty
                        @endforelse
                        </div>
                    </div>
                </div>
                @endif
                {{ $goods->links() }}
                @if($category->description != null)<div class="c"><div class="description">{!! $category->description !!}</div></div>@endif
@if(count($category->video) != null)
<script type="text/javascript">
$(document).ready(function(){
$('a.lightbox').divbox({caption: false});
$('a.lightbox2').divbox({caption: false, width: 600, height: 400});
$('a.iframe').divbox({ width: 200, height: 200 , caption: false});
$('a.ajax').divbox({ type: 'ajax', width: 200, height: 200 , caption: false,api:{
afterLoad: function(o){
$(o).find('a.close').click(function(){
o.closed();
return false;
});

$(o).find('a.resize').click(function(){
o.resize(200,50);
return false;
});
}
}});
});
</script>
<div class="c grey video">
<div class="content_title"><h2 style="font-size:17px;line-height:17px;font-weight:600;margin:0">Видео</h2></div>
<div class="c-o">
<div class="c-o_l c-o_l-xxl">
@forelse($category->video as $v)
<div class="item ytp">
<a href="https://www.youtube.com/embed/{{$v->youtube}}?autoplay=1" class="lightbox2">
    <div class="image" style="background-image: url(http://img.youtube.com/vi/{{$v->youtube}}/hqdefault.jpg);">
        <button class="ytp-button">
            <svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z" fill="#212121" fill-opacity="0.8"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg>
        </button>
    </div>
</a>
<div class="info">
    <div class="name" style="font-weight: 600"><h3>{{$v->title}}</h3></div>
    <div class="info_desc"><p>{{$v->description}}</p></div>
</div>
</div>
@empty
@endforelse
</div>
</div>
</div>
@endif
<style>
    .reviews{min-width:100%;display:table}
    .reviews .i{padding:15px;width: 100%;background:#fff;border:1px solid #e6e6e6;margin:.25em 0;display:inline-block}
    .reviews .i.open .reviews_button{display:none}
    .reviews .i .write_rev{display:none}
    .reviews .i.open .write_rev{display:block}
    .reviews .i.open{padding:15px!important;border:1px solid #e6e6e6!important}
    .rating{color:#fdcc63;font-size:12px;line-height:12px;margin-bottom:6px}
    .reviews .user{display:flex}
    .reviews .user .user_image{width:50px;height:50px;border-radius:50%;overflow:hidden;min-width:50px}
    .reviews .user img{width:100%;height:100%;object-fit:cover}
    .user_name{width:100%;padding-left:7px;margin:auto}
    .reviews .user .name{font-weight:600;font-size:15px;line-height:17px}
    .reviews .user .date{color:#888;font-size:14px}
    .i_desc{font-size:15px;line-height:22px;max-height:85px;position:relative;overflow:hidden}
    .toggle-btn{font-size:15px;line-height:17px;margin-top:10px;display:table}
    .text-open{overflow:visible;height:auto;max-height:none}
    .text-open + .toggle-btn{display:none}
    .i_gallery{display:table;width:100%;margin-top:10px}
    .i_gallery .image{width:31.333%;height:50px;margin:1%;float:left}
    .reviews .i_gallery img{width:100%;height:100%;object-fit:cover}
    @media (max-width: 778px) {
        .reviews .i{width:49%}
        .i_gallery .image{height:7.5vh;width:25%}
    }
    @media (max-width: 575px) {
        .reviews .i{width:99%}
        .i_gallery .image{height:6.5vh;width:20%;margin:.5%}
    }
    .masonry{-moz-column-count:3;-webkit-column-count:3;column-count:3;-webkit-column-gap:.5em;-moz-column-gap:.5em;column-gap:.5em;padding:0;font-size:.85em}
    @media only screen and (max-width: 320px) {
        .masonry{-moz-column-count:1;-webkit-column-count:1;column-count:1}
    }
    @media only screen and (min-width: 321px) and (max-width: 768px) {
        .masonry{-moz-column-count:2;-webkit-column-count:2;column-count:2}
    }
    @media only screen and (min-width: 769px) {
        .masonry{-moz-column-count:3;-webkit-column-count:3;column-count:3}
    }
    .reviews_button{display:inline-block;vertical-align:top;padding:15px;font-size:13px;line-height:14px;width:100%;text-align:center;text-transform:uppercase;position:relative;color:#fdcc63!important;font-weight:600;border:1px solid #f7e8a8}
    .reviews_input{width:100%;border:1px solid #e6e6e6;padding:7px 15px;margin-bottom:5px}
    .reviews_input_file{margin-bottom:15px}
    .reviews_textarea{text-align:left;-webkit-box-sizing:border-box;box-sizing:border-box;overflow:hidden;border:1px solid #e6e6e6;height: 80px;padding:15px;margin-bottom: 10px;width:100%;resize:none;outline:none;font:14px/17px Helvetica,Arial,sans-serif}
    .reviews_textarea_button{position:relative;outline:none;border:none;padding:7px 20px;min-width:150px;min-height:32px;border-radius:3px;background-color:#007bab;font-size:14px;line-height:16px;cursor:pointer;color:#fff}
</style>
<div class="c grey" style="background: none">
    <div class="content_title" style="background: #fff"><h2>Отзывы наших заказчиков:</h2></div>
    <div class="reviews">
        <div class="masonry">
            <div class="i" style="padding: 0;border:none" id="ddd3">
                <a href="javascript:void(0);" class="reviews_button" onclick="reviewsFunction()">Оставить отзыв</a>
                <!--                    <div class="fdf" id="ddd3"></div>-->
                <div class="write_rev">
                  <form action="/feedback/store/{{$category->id or ''}}" method="post" enctype="multipart/form-data">
                      {{csrf_field()}}
                  <input type="hidden" name="type" value="category">
                  <input type="text" name="name" class="reviews_input" placeholder="Имя Фамилия" value="">
                  <label for="file">Ваша фотография:</label>
                  <input type="file" name="ava" class="reviews_input_file" placeholder="Текст отзыва..." value="" style="border: 0;padding-left: 0;width: 100%">
                  <textarea name="description" id="" class="reviews_textarea" placeholder="Текст отзыва..."></textarea>
                  <div id="example3"></div>
                        <div class="submitReview">
                            <button class="reviews_textarea_button" style="background-color:#ddd" disabled>Отправить</button>
                        </div>
                  </form>
                </div>
            </div>
            @forelse($reviews as $r)
                <div class="i">
                    <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                    <div class="user">
                        <div class="user_image"><img src="/storage/app/{{Resize::ResizeImage($r, '45x45')}}" alt=""></div>
                        <div class="user_name">
                            <div class="name">{{$r->name}}</div>
                            <div class="date">Добавлено: <span>{{Resize::noTimeDate($r->created_at)}}</span></div>
                        </div>
                    </div>
                    <div class="i_desc">{!! $r->description !!}</div>
                    <a href="" class="toggle-btn">Читать полностью</a>
                    @if($r->imageses != null)
                        <div class="i_gallery tz-gallery">
                            @forelse($r->imageses as $img)
                                <div class="image">
                                    <a href="/storage/app/{{$img->images or ''}}" class="lightbox">
                                        <img src="/storage/app/{{Resize::ResizeImage($img, '75x50')}}" alt="">
                                    </a>
                                </div>
                            @empty
                            @endforelse
                        </div>
                    @endif
                </div>
            @empty
            @endforelse
        </div>
    </div>
</div>
<script>$(".toggle-btn").click(function(e) {e.preventDefault();$(this).prev().toggleClass("text-open")})</script>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
      var verifyCallback = function(response) {
        $('.submitReview').html('<input type="submit" value="Отправить" class="reviews_textarea_button">');
      };
      var onloadCallback = function() {
        grecaptcha.render('example3', {
          'sitekey' : '{{Resize::recaptcha()}}',
          'callback' : verifyCallback,
        });
      };
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>