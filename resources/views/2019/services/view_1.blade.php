<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/services" itemprop="item"><span itemprop="name">Услуги</span></a>
                <meta itemprop="position" content="2" />
            </li>
            @if($category_parent->bread_cat == null)
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/services/{{$category_parent->id}}-{{$category_parent->slug}}" itemprop="item"><span itemprop="name">{{$category_parent->title}}</span></a>
                    <meta itemprop="position" content="3" />
                </li>
                <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{$product->title or ''}}</span>
                    <meta itemprop="position" content="4" />
                </li>
            @else
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/services/{{$category_parent->bread_cat->id}}-{{$category_parent->bread_cat->slug}}" itemprop="item"><span itemprop="name">{{$category_parent->bread_cat->title}}</span></a>
                    <meta itemprop="position" content="3" />
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/services/{{$category_parent->id}}-{{$category_parent->slug}}" itemprop="item"><span itemprop="name">{{$category_parent->title}}</span></a>
                    <meta itemprop="position" content="4" />
                </li>
                <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{$product->title or ''}}</span>
                    <meta itemprop="position" content="5" />
                </li>
            @endif
        </ol>
    </nav>
</section>
<style>.nsm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}.nsm .title{padding:15px 15px 0;line-height:17px;font-size:17px;margin-bottom:10px;font-weight:600}.nsm ul{list-style:none;padding:0;margin:0}.nsm .parent_cat{position:relative;color:#666;font-weight:600;border-bottom:1px solid #e6e6e6;cursor:pointer}.nsm ul .parent_cat{padding:0 15px}.nsm ul .parent_cat{padding:0 15px}.nsm .parent_cat span{display:table-cell;padding:10px 20px 10px 0;width:100%;color:#007bff}</style>
<section class="i80">
    <div class="page_category_2">
        <div class="row">
            <div class="col-3">
                <div class="nsm">
                    <div class="title">Категории</div>
                    <div class="menu-list">
                        <ul class="menu-content">
                            @forelse($category_nav as $c)
                                <li class="cat-item @if(count($c->sub_category_nav)>0) collapse @else 111 @endif">
                                    <div class="parent_cat @if($c->id == $services->category) open_cat @else @endif" @if(count($c->sub_category_nav)>0)onclick="smFunction('sm-{{$c->id}}')" @else  @endif>
                                        @if(count($c->sub_category_nav) == 0)
                                            <a href="/services/{{$c->id}}-{{$c->slug or ''}}"><span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or ''}}@endif</span></a>
                                        @else
                                            <span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or ''}}@endif</span>
                                        @endif
                                    </div>
                                    @if(count($c->sub_category_nav)>0)
                                        <ul class="sub-menu collapsed @if($category_parent->id == $c->id || $c->open_cat == 1 || $category_parent->bread_cat != null && $category_parent->bread_cat->id == $c->id)active @endif"  id="sm-{{$c->id}}">
                                            @forelse($c->sub_category_nav as $sub)
                                                <li class="sub_cat @if($sub->id == $services->category) open_cat @else @endif"><a href="/services/{{$sub->id}}-{{$sub->slug or ''}}">@if($sub->seoname != null){{$sub->seoname}}@else{{$sub->title or ''}}@endif</a></li>
                                            @empty
                                            @endforelse
                                        </ul>
                                    @endif
                                </li>
                            @empty
                            @endforelse
                        </ul>
                    </div>
                </div>
                @if(count($documents) != null)
                <div class="documents">
                    <div class="title">Документы</div>
                    <div class="documents_list">
                        @forelse($documents as $d)
                            <div class="item">
                                <div class="file file-{{$d->type}}"></div>
                                <div class="info">
                                    <div class="name"><a href="/storage/app/{{$d->doc}}/">{{$d->title}}</a></div>
                                    <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
                @endif
            </div>
            <style>
                .s-o_f{display:table;width: 100%;background:#fff;margin-bottom:40px;   -webkit-box-shadow: -1px 0 0 0 #e6e6e6 inset, 0 -1px 0 0 #e6e6e6 inset, -1px -1px 0 0 #e6e6e6 inset, -1px 0 0 0 #e6e6e6, 0 -1px 0 0 #e6e6e6;box-shadow: -1px 0 0 0 #e6e6e6 inset, 0 -1px 0 0 #e6e6e6 inset, -1px -1px 0 0 #e6e6e6, -1px 0 0 0 #e6e6e6, 0 -1px 0 0 #e6e6e6;-webkit-box-orient: vertical;-webkit-box-direction: normal;}
                .s-o_f .pheader{padding: 15px 15px 5px;width:100%;display:flex}
                .s-o_f .pheader .title{display:inline-block;width:60%;padding-right:15px}
                .s-o_f .pheader .title h1{font-weight: 600;font-size: 20px;line-height: 20px;margin: 0;}
                .s-o_f .pheader .title .data{font-size:14px;color:#888;line-height: 14px;margin-top: 5px;}
                .s-o_f .pheader .price{display:inline-block;width:40%;font-size:14px;text-align:right}
                .s-o_f .pheader .price span{font-weight:600;font-size:20px;line-height: 20px;color:#222}
                .s-o_f .image_block .image{width:100%;height:346px;padding:10px;border-bottom:1px solid #e6e6e6}
                .s-o_f .first-image{width:100%;height:100%;position:relative;overflow:hidden;display:table}
                .s-o_f .blur{width:102%;height:103%;margin:-5px -9px;object-fit:contain;background-position: left;position:absolute;background-repeat:no-repeat;background-size:cover;-webkit-filter:blur(5px);-moz-filter:blur(5px);filter:blur(5px);filter:url(/public/site/img/blur.svg#blur)}
                .s-o_f .image_block .image img{width:100%;height:100%;object-fit:contain;position:relative;z-index:1}
                .s-o_f .image_block .gallery{width:100%;display:table;padding:7px}
                .s-o_f .image_block .gallery .item{width:12.5%;float:left}
                .s-o_f .image_block .gallery .item .lightbox{height:5.5vw;margin:3px;display:block;background-repeat:no-repeat;background-size:cover;}
                .s-o_f .image_block .gallery .item img{width:100%;height:100%;object-fit:cover}

                @media (min-width: 1400px) {
                    .s-o_f .image_block .gallery .item .lightbox{height: 65px}
                }
                @media (max-width: 992px) {
                    .s-o_f .blur{width: 103%;height: 110%;margin: -10px -10px}
                    .s-o_f .image_block .gallery .item .lightbox{height: 7.5vw}
                }
                @media (max-width: 575px){
                    .s-o_f .pheader{display:table}
                    .s-o_f .pheader .title{width:100%;padding:0}
                    .s-o_f .pheader .price{width:100%;text-align:left}
                    .s-o_f .image_block .image{height:67vw}
                    .s-o_f .image_block .gallery .item {width:16.666%}
                    .s-o_f .image_block .gallery .item .lightbox {height:9.5vw}
                }
            </style>
            <div class="col-9">
                <div class="product">
                    <div class="s-o_f">
                        <div class="pheader">
                            <div class="title"><h1>{{$services->title or ''}}</h1><div class="data">id услуги: {{$services->id or ''}}, опубликовано {{Resize::date($services->created_at)}}</div>@if(Auth::user() !== null)<div class="data"><a target="_blank" href="/admin/services/{{$services->id or ''}}/edit?id={{$services->id or ''}}&&admin=true">Редактировать</a></div>@endif</div>
                            <div class="price">@if($services->coast != null)<span>{{$services->coast or ''}}</span> руб. @else <span style="font-size: 17px;">Цена не указана</span> @endif</div>
                        </div>
                        <div class="pbody">
                            <div class="image_block tz-gallery">
                                <div class="image">
                                    <a class="lightbox first-image" href="/storage/app/{{$services->images->images or ''}}">
                                        <div class="blur" style="background-image: url(/storage/app/{{Resize::ResizeImage($services->images, '800x300')}});"></div>
                                        <img id="ImageProduct{{$services->images->id or ''}}" src="/storage/app/{{Resize::ResizeImage($services->images, '40x30')}}" alt="{{$services->title or ''}}">
                                    </a>
                                </div>
                                <div class="gallery">
                                <?php $i = 0; ?>
                                @forelse($services->imageses as $img)
                                @if($i>0)
                                    <div class="item"><a class="lightbox" href="/storage/app/{{$img->images or ''}}" style="background-image:url(/storage/app/{{Resize::ResizeImage($img, '75x50')}});"></a></div>
                                @endif
                                <?php $i++; ?>
                                @empty
                                @endforelse
                            </div>
                            </div>
                        </div>
                    </div>
                    @if($services->short_description != null)
                    <div class="c">
                        <div class="content_title"><h2>Описание услуги</h2></div>
                        <div class="description">{!! $services->short_description or '' !!}</div>
                    </div>
                    @endif
                    <script type="text/javascript">
                        $(document).ready(function(){$('#ImageProduct{{$services->images->id or ''}}').attr('src', '/storage/app/{{Resize::ResizeImage($services->images, "485x325")}}');});
                    </script>
                    <div class="c no-mobile">
                        <div class="content_title"><span>Контактная информация</span></div>
                        <div class="manager_in_description">
                            <div class="person">
                                <div class="image"><img src="/storage/app/{{Resize::ResizeImage($manager, '55x55')}}" alt="{{$manager->name or 'Имя'}} {{$manager->lastname or 'Фамалия'}}"></div>
                                <div class="info">
                                    <div class="name">{{$manager->name or 'Менеджер'}} {{$manager->lastname or ''}}</div>
                                    <div class="position">{{$manager->job or 'Бесплатный консультант'}}</div>
                                    <div class="data phone"><span>{{$manager->tel or ''}}</span></div>
                                    <div class="data email"><a href="/contact"><span>Отправить сообщение</span></a></div>
                                </div>
                            </div>
                            <div class="person_company">
                                <div class="name">Контакты: {{$company->sites->name or 'Компания'}} - {{$company->cities->gorod or 'Город'}}</div>
                                <div class="address">Адрес: {{$company->address or 'г. Город, ул. Улица 1, оф. 1'}} <span><a href="#" class="open_map">Показать на карте</a></span></div>
                                <div class="data timework">Время работы: @if($manager != null && $manager->timeJob != null){{$manager->timeJob or 'Время работы'}}@else{{$company->timeJob or 'Время работы'}}@endif</div>
                            </div>
                        </div>
                    </div>
                    <style>
                        .portfolio{display: table;margin: 5px;width: 100%}
                        .portfolio .i{width: 24%;float: left;margin: 0.5%;}
                        .portfolio .image{height: 14vh;width: 100%}
                        .portfolio .i img{width: 100%;height: 100%;object-fit: cover}
                        @media (max-width: 778px) {
                            .portfolio .i{width: 32.333%}
                        }
                        @media (max-width: 575px) {
                            .portfolio .i{width: 49%}
                        }
                    </style>
                    <div class="c">
                        <div class="content_title"><h2>Фото наших работ</h2></div>
                        <div class="portfolio">
                        @forelse($portfolio as $p)
                            <div class="i image_modal" slug="{{$p->id}}"><div class="image"><img src="/storage/app/{{Resize::ResizeImage($p->image_one, '190x125')}}" alt=""></div></div>
                        @empty
                        @endforelse
                        </div>
                    </div>
                    <script type="text/javascript">
                        // .image - отвечает за класс на который надо нажимать, чтоб сработал ajax
                        $('.image_modal').click(function(){
                            var id = $(this).attr('slug');
                            $.get('/portfolio/view/image/description/'+id, function(data){
                                $('.modal_portfolio').html(data);
                            });

                            var url = '?object=' + id;
                            history.pushState(null, null, url);

                            @if($favicon->metrika_short != null)
                            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                                m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
                            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
                            ym({{$favicon->metrika_short}}, 'hit', url);
                            @endif
                        });
                    </script>
                    <div class="modal_portfolio"></div>
<style>
    .reviews{min-width:100%;display:table}
    .reviews .i{padding:15px;width: 100%;background:#fff;border:1px solid #e6e6e6;margin:.25em 0;display:inline-block}
    .reviews .i.open .reviews_button{display:none}
    .reviews .i .write_rev{display:none}
    .reviews .i.open .write_rev{display:block}
    .reviews .i.open{padding:15px!important;border:1px solid #e6e6e6!important}
    .rating{color:#fdcc63;font-size:12px;line-height:12px;margin-bottom:6px}
    .reviews .user{display:flex}
    .reviews .user .user_image{width:50px;height:50px;border-radius:50%;overflow:hidden;min-width:50px}
    .reviews .user img{width:100%;height:100%;object-fit:cover}
    .user_name{width:100%;padding-left:7px;margin:auto}
    .reviews .user .name{font-weight:600;font-size:15px;line-height:17px}
    .reviews .user .date{color:#888;font-size:14px}
    .i_desc{font-size:15px;line-height:22px;max-height:85px;position:relative;overflow:hidden}
    .toggle-btn{font-size:15px;line-height:17px;margin-top:10px;display:table}
    .text-open{overflow:visible;height:auto;max-height:none}
    .text-open + .toggle-btn{display:none}
    .i_gallery{display:table;width:100%;margin-top:10px}
    .i_gallery .image{width:31.333%;height:50px;margin:1%;float:left}
    .reviews .i_gallery img{width:100%;height:100%;object-fit:cover}
    @media (max-width: 778px) {
        .reviews .i{width:49%}
        .i_gallery .image{height:7.5vh;width:25%}
    }
    @media (max-width: 575px) {
        .reviews .i{width:99%}
        .i_gallery .image{height:6.5vh;width:20%;margin:.5%}
    }
    .masonry{-moz-column-count:3;-webkit-column-count:3;column-count:3;-webkit-column-gap:.5em;-moz-column-gap:.5em;column-gap:.5em;padding:0;font-size:.85em}
    @media only screen and (max-width: 320px) {
        .masonry{-moz-column-count:1;-webkit-column-count:1;column-count:1}
    }
    @media only screen and (min-width: 321px) and (max-width: 768px) {
        .masonry{-moz-column-count:2;-webkit-column-count:2;column-count:2}
    }
    @media only screen and (min-width: 769px) {
        .masonry{-moz-column-count:3;-webkit-column-count:3;column-count:3}
    }
    .reviews_button{display:inline-block;vertical-align:top;padding:15px;font-size:13px;line-height:14px;width:100%;text-align:center;text-transform:uppercase;position:relative;color:#fdcc63!important;font-weight:600;border:1px solid #f7e8a8}
    .reviews_input{width:100%;border:1px solid #e6e6e6;padding:7px 15px;margin-bottom:5px}
    .reviews_input_file{margin-bottom:15px}
    .reviews_textarea{text-align:left;-webkit-box-sizing:border-box;box-sizing:border-box;overflow:hidden;border:1px solid #e6e6e6;height: 80px;padding:15px;margin-bottom: 10px;width:100%;resize:none;outline:none;font:14px/17px Helvetica,Arial,sans-serif}
    .reviews_textarea_button{position:relative;outline:none;border:none;padding:7px 20px;min-width:150px;min-height:32px;border-radius:3px;background-color:#007bab;font-size:14px;line-height:16px;cursor:pointer;color:#fff}
</style>
<div class="c grey" style="background: none">
<div class="content_title" style="background: #fff"><h2>Отзывы наших заказчиков:</h2></div>
<div class="reviews">
    <div class="masonry">
        <div class="i" style="padding: 0;border:none" id="ddd3">
            <a href="javascript:void(0);" class="reviews_button" onclick="reviewsFunction()">Оставить отзыв</a>
            <!--                    <div class="fdf" id="ddd3"></div>-->
            <div class="write_rev">
                <form action="/feedback/store/{{$services->id or ''}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="type" value="goods">
                    <input type="text" name="name" class="reviews_input" placeholder="Имя Фамилия" value="">
                    <label for="file">Ваша фотография:</label>
                    <input type="file" name="ava" class="reviews_input_file" placeholder="Текст отзыва..." value="" style="border: 0;padding-left: 0;width: 100%">
                    <textarea name="description" id="" class="reviews_textarea" placeholder="Текст отзыва..."></textarea>
                    <div id="example3"></div>
                        <div class="submitReview">
                            <button class="reviews_textarea_button" style="background-color:#ddd" disabled>Отправить</button>
                        </div>
                </form>
            </div>
        </div>
        @forelse($reviews as $r)
            <div class="i">
                <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                <div class="user">
                    <div class="user_image"><img src="/storage/app/{{Resize::ResizeImage($r, '45x45')}}" alt=""></div>
                    <div class="user_name">
                        <div class="name">{{$r->name}}</div>
                        <div class="date">Добавлено: <span>{{Resize::noTimeDate($r->created_at)}}</span></div>
                    </div>
                </div>
                <div class="i_desc">{!! $r->description !!}</div>
                <a href="" class="toggle-btn">Читать полностью</a>
                @if($r->imageses != null)
                    <div class="i_gallery tz-gallery">
                        @forelse($r->imageses as $img)
                            <div class="image">
                                <a href="/storage/app/{{$img->images or ''}}" class="lightbox">
                                    <img src="/storage/app/{{Resize::ResizeImage($img, '75x50')}}" alt="">
                                </a>
                            </div>
                        @empty
                        @endforelse
                    </div>
                @endif
            </div>
        @empty
        @endforelse
    </div>
</div>
</div>
@if(count($services->video) != null)
<script type="text/javascript">
$(document).ready(function(){
$('a.lightbox').divbox({caption: false});
$('a.lightbox2').divbox({caption: false, width: 600, height: 400});
$('a.iframe').divbox({ width: 200, height: 200 , caption: false});
$('a.ajax').divbox({ type: 'ajax', width: 200, height: 200 , caption: false,api:{
afterLoad: function(o){
$(o).find('a.close').click(function(){
    o.closed();
    return false;
});

$(o).find('a.resize').click(function(){
    o.resize(200,50);
    return false;
});
}
}});
});
</script>
<div class="c grey video">
<div class="content_title"><h2>Видео</h2></div>
<div class="c-o">
<div class="c-o_l c-o_l-xxl">
@forelse($services->video as $v)
<div class="item ytp">
<a href="https://www.youtube.com/embed/{{$v->youtube}}?autoplay=1" class="lightbox2">
    <div class="image" style="background-image: url(http://img.youtube.com/vi/{{$v->youtube}}/hqdefault.jpg);">
        <button class="ytp-button">
            <svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z" fill="#212121" fill-opacity="0.8"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg>
        </button>
    </div>
</a>
<div class="info">
    <div class="name" style="font-weight: 600"><h3>{{$v->title}}</h3></div>
    <div class="info_desc"><p>{{$v->description}}</p></div>
</div>
</div>
@empty
@endforelse
</div>
</div>
</div>
@endif
<script>$(".toggle-btn").click(function(e) {e.preventDefault();$(this).prev().toggleClass("text-open")})</script>
                    <div class="c">
                        <div class="content_title"><h3>Часто задаваемые вопросы:</h3></div>
                        <div class="description"></div>
                    </div>
                    <div class="c grey">
                        <div class="content_title"><h2>Услуги похожие на: {{$services->title or 'Ошибка: название услуги'}}</h2></div>
                        <div class="c-g">
                            <div class="c-g_l c-g_l-md">
                                @forelse($similar as $g)
                                    <div class="item">
                                        <a href="/services/{{$category_parent->id}}-{{$category_parent->slug}}/{{$g->id}}-{{$g->slug}}">
                                            <div class="image"><div class="img_dark"></div><img id="ImageSimilar{{$g->id or 'Ошибка: id'}}" src="/storage/app/{{Resize::ResizeImage($g->images, '19x19')}}" alt=""></div>
                                            <div class="info">
                                                <div class="name"><span>{{$g->title or ''}}</span></div>
                                                <div class="prices"><div class="minimum">Цена:</div><div class="price-content">@if($g->coast != null){{$g->coast or ''}} - @else Цена не указана @endif</div></div>
                                            </div>
                                        </a>
                                    </div>
                                    <script type="text/javascript">$(document).ready(function(){$('#ImageSimilar{{$g->id}}').attr('src', '/storage/app/{{Resize::ResizeImage($g->images, "190x190")}}');});</script>
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $('.open_map').click(function(){$.get('{{route('modal.open.map')}}', function(data){$('.modal').html(data);})});
    });
</script>

<link rel="stylesheet" href="https://rawgit.com/LeshikJanz/libraries/master/Bootstrap/baguetteBox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>
<script type="text/javascript">
      var verifyCallback = function(response) {
        $('.submitReview').html('<input type="submit" value="Отправить" class="reviews_textarea_button">');
      };
      var onloadCallback = function() {
        grecaptcha.render('example3', {
          'sitekey' : '{{Resize::recaptcha()}}',
          'callback' : verifyCallback,
        });
      };
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
