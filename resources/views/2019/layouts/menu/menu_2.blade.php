<body>
@if($info->color_menu != null)
<style>
    .ban_2 .title-ie{background-color:#{{$info->color_menu}}80!important;}
    .ban_2 .action_button{background: #{{$info->color_menu}}80!important;border: 1px solid #{{$info->color_menu}}80!important;}
    .ht{border-top: 3px solid #{{$info->color_menu}}!important;}
    .ht .ht_act-co li .fa, .f_2 .phone .img:before, .f_2 .phone .img:before, .f_2 .location .img:before, .ht .ht_act-user .ht-item .fa{color:#{{$info->color_menu}}!important;}
    .documents .file-pdf:before, .documents .file-word:before, .contact .file, .map .back-map .map_b, .ncm .i:hover, .ncm .i a:hover, .map .back-map .map_b,.ncm .i.active, .ncm .i.active a{background: #{{$info->color_menu}}!important;color: #{{$info->color_text_menu}}!important;}
    .p-o_l .info .connection .to-order{background: #{{$info->color_menu}}!important;border: 1px solid #{{$info->color_menu}}!important;color: #{{$info->color_text_menu}}!important;}
    .p-o_l .info .connection .write{border: 1px solid #{{$info->color_menu}}!important;color: #{{$info->color_menu}}!important;}
    .manager .data.email:before, .manager .data.phone:before, .manager .data.local:before, .manager .data.timework:before, .c .manager_in_description .data.phone:before, .c .manager_in_description .data.email:before{color: #{{$info->color_menu}}!important;}
    .hb, .i-c.in-pic .name span, .news .item .info .date, .c-c_l .i .name span, .f_2 .wrapper_block{background-color: #{{$info->color_menu}}!important;color: #{{$info->color_text_menu}};}
    .nsm .parent_cat span, .ncm .i a, .documents .item .info .name a{color: #222222!important;}
    @if($info->image_menu != null)
.hb, .ban_2 .title-ie, .i-c.in-pic .name span, .news .item .info .date, .c-c_l .i .name span, .f_2 .wrapper_block{background-image: url(/storage/app/{{$info->image_menu}})!important;color: #fff;}
    .hb-wrap li:hover{background-color: #{{$info->color_menu}}cc;}
    @else
.hb-wrap li:hover{background-color: #00000026;}
    @endif
.nsm .sub_cat:hover, .nsm .parent_cat:hover{background: #{{$info->color_menu}};}
    .nsm .sub_cat:hover a, .nsm .sub_cat:hover:before, .nsm .parent_cat:hover, .nsm .parent_cat:hover span{color: #{{$info->color_text_menu}}!important;}
    .nsm .open_cat{background: #{{$info->color_menu}};color: #{{$info->color_text_menu}}!important}
    .nsm .parent_cat.open_cat span, .nsm .sub-menu .sub_cat.open_cat a, .nsm .sub-menu li.open_cat:before{color: #{{$info->color_text_menu}}!important;}
    .ht .ht_act-co a span{border-bottom: 1px dashed #fff;}
</style>
@endif

<header>
    <nav class="h_nav">
        <div class="ht">
            <div class="ht-wrap">
                <ul class="ht_act-co">
                    <li><i class="fa fa-map-marker red" aria-hidden="true"></i><span>{{$info->cities->gorod or 'Город'}}</span></li>
                    <li><i class="fa fa-envelope red" aria-hidden="true"></i><span>{{$email->email or 'contact@company.ru'}}</span></li>
                    <li><a href="tel:{{$tel->tel or ''}}"><i class="fa fa-phone red" aria-hidden="true"></i><span>{{$tel->tel or '+7 (900) 000-00-00'}}</span></a></li>
                </ul>
                <div class="ht_act-user">
                    <div id="needcallhead" class="ht-item" style="cursor: pointer;"><i class="fa fa-undo red" aria-hidden="true"></i><span>Заказать звонок</span></div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#logo1').attr('style','background-image: url(/storage/app/{{$info->logo1 or ''}});');
                $('.footer-logo').attr('src','/storage/app/{{$info->logo2 or ''}}');
            });
        </script>
        <div class="hb">
            <div class="hb-wrap" id="id-hb-wrap">
                <div class="hb_logo">
                    <a href="/" class="hb_brand" id="logo1" style="background-image: url(/storage/app/{{$info->logo1 or ''}});"></a>
                </div>
                <div class="icon">
                    <a href="javascript:void(0);" onclick="myFunction()"><div class="bar1"></div><div class="bar2"></div><div class="bar3"></div></a>
                </div>
                <ul class="hb_nav">
                    <li class="dropdown sort-1">
                        <a href="/about" onclick="navFunction('nav-id-1')" class="dropbtn nolink">{{$menu[0]->title or 'Компания'}}
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu" id="nav-id-1">
                            <li class="nav-item"><a class="nav-link" href="/about">О компании</a></li>
                            @forelse($info->sections as $i)
                                @if($i->slug == 'action')<li><a href="/action">{{$i->title}}</a></li>@endif
                                @if($i->slug == 'documantation')<li><a href="/about/documents">{{$i->title}}</a></li>@endif
                                @if($i->slug == 'jobs')<li><a href="/jobs">{{$i->title}}</a></li>@endif
                                @if($i->slug == 'news')<li><a href="/news">{{$i->title}}</a></li>@endif
                                @if($i->slug == 'blog')<li><a href="/blog">{{$i->title}}</a></li>@endif
                                @if($i->slug == 'pricelist')<li><a href="/price-list">{{$i->title}}</a></li>@endif
                                @if($i->slug == 'partners')<li><a href="/partners">{{$i->title}}</a></li>@endif
                            @empty
                            @endforelse
                            <li><a href="/about/requisites">Реквизиты</a></li>
                            <li class="only-m"><a href="/contact">Контакты</a></li>
                        </ul>
                    </li>
                    <?php $i = 1; ?>
                    @forelse($menu as $m)
                        @if($i == 1) <?php $i++; continue; ?> @endif
                        @if($m->slug == 'product' || $m->slug == 'services' || $m->slug == 'stroitelstvo' || $m->slug == 'bani' || $m->slug == 'proyekty' || $m->slug == 'izgotovleniye')
                            <li class="dropdown sort-2">
                                <a href="/{{$m->slug}}" onclick="navFunction('nav-id-{{$i}}')" class="dropbtn nolink">{{$m->title or 'Нет названия'}}
                                    <i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu" id="nav-id-{{$i}}">
                                    @forelse($categories as $category)
                                        @if($category->type == $m->type && $category->parent_id == null)
                                            <li><a href="/{{$m->slug}}/{{$category->id}}-{{$category->slug or ''}}">@if($category->seoname != null){{$category->seoname}}@else{{$category->title or ''}}@endif</a></li>
                                        @endif
                                    @empty
                                    @endforelse
                                </ul>
                            </li>

                        @else
                            @if($m->slug == 'contact')
                                <li class="sort-3"><a href="/{{$m->slug}}">{{$m->title or 'Нет названия'}}</a></li>
                            @else
                                <li class="sort-{{$i}}"><a href="/{{$m->slug}}">{{$m->title or 'Нет названия'}}</a></li>
                            @endif

                        @endif
                    @empty
                    @endforelse
                </ul>
                <ul class="hb_act-co">
                    <li><i class="fa fa-map-marker red" aria-hidden="true"></i><span>{{$info->cities->title or 'Город'}}</span></li>
                    <li><i class="fa fa-envelope red" aria-hidden="true"></i><span>{{$email->email or 'contact@company.ru'}}</span></li>
                    <li><a href="tel:{{$tel->tel or ''}}"><i class="fa fa-phone red" aria-hidden="true"></i><span>{{$tel->tel or '+7 (900) 000-00-00'}}</span></a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>