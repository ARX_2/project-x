<body>
@if($info->color_menu != null)
    <style>
        .ht_act-call .needcall{background:#{{$info->color_menu}}80!important}
        .ht_act-call .needcall:hover{background:#{{$info->color_menu}}CC!important}
        .hb-wrap li a:hover{border-bottom:2px solid #{{$info->color_menu}}!important}
        .ban_2 .title-ie{background-color:#{{$info->color_menu}}80!important;}
        .ban_2 .action_button{background: #{{$info->color_menu}}80!important;border: 1px solid #{{$info->color_menu}}80!important;}
        .ht .ht_act-co li .fa, .f_2 .phone .img:before, .f_2 .phone .img:before, .f_2 .location .img:before {color:#{{$info->color_menu}}!important;}
        .documents .file-pdf:before, .documents .file-word:before, .contact .file, .map .back-map .map_b, .ncm .i:hover, .ncm .i a:hover, .map .back-map .map_b,.ncm .i.active, .ncm .i.active a{background: #{{$info->color_menu}}!important;color: #{{$info->color_text_menu}}!important;}
        .p-o_l .info .connection .to-order{background: #{{$info->color_menu}}!important;border: 1px solid #{{$info->color_menu}}!important;color: #{{$info->color_text_menu}}!important;}
        .p-o_l .info .connection .write{border: 1px solid #{{$info->color_menu}}!important;color: #{{$info->color_menu}}!important;}
        .manager .data.email:before, .manager .data.phone:before, .manager .data.local:before, .manager .data.timework:before, .c .manager_in_description .data.phone:before, .c .manager_in_description .data.email:before{color: #{{$info->color_menu}}!important;}
        .i-c.in-pic .name span, .news .item .info .date, .c-c_l .i .name span, .f_2 .wrapper_block{background-color: #{{$info->color_menu}}!important;color: #{{$info->color_text_menu}};}
        .nsm .parent_cat span, .ncm .i a, .documents .item .info .name a{color: #222222!important;}
        @if($info->image_menu != null)
.hb, .ban_2 .title-ie, .i-c.in-pic .name span, .news .item .info .date, .c-c_l .i .name span, .f_2 .wrapper_block{background-image: url(/storage/app/{{$info->image_menu}})!important;color: #fff;}
        .hb-wrap li:hover{background-color: #{{$info->color_menu}}cc;}
        @else
.hb-wrap li:hover{background-color: #00000026;}
        @endif
.nsm .sub_cat:hover, .nsm .parent_cat:hover{background: #{{$info->color_menu}};}
        .nsm .sub_cat:hover a, .nsm .sub_cat:hover:before, .nsm .parent_cat:hover, .nsm .parent_cat:hover span{color: #{{$info->color_text_menu}}!important;}
        .nsm .open_cat{background: #{{$info->color_menu}};color: #{{$info->color_text_menu}}!important}
        .nsm .parent_cat.open_cat span, .nsm .sub-menu .sub_cat.open_cat a, .nsm .sub-menu li.open_cat:before{color: #{{$info->color_text_menu}}!important;}
        .ht .ht_act-co a span{border-bottom: 1px dashed #fff;}
    </style>
@endif
<style>
.ht.ht-3{border-top:none}
.ht .ht_act-co{margin:8px 0}
.ht .ht_act-co li{border-right:1px solid #666}
.ht .ht_act-co li:first-child{border-left:1px solid #666}
.ht .ht_act-co li span{color:#ddd}
.ht-wrap-3{overflow:hidden;width:100%;margin:0 auto;font-size:13px;color:#656565;padding:0}
.ht-wrap-3 .needcall_b{float:left;background:#f56d53;padding:5px 10px;color:#fff}
.ht .ht-wrap-3 .ht_act-user span{color:#fff}
.hb{background:#fff}
.hb-wrap li a{color:#fff;border-bottom:2px solid #ffffff00}
.hb-wrap li a:hover{border-bottom:2px solid #f56d53}
.hb-wrap li:hover{background-color:#00000000}
.hb-wrap .hb_logo{margin:3.5px 0}
.hb-wrap li a{padding:24px 8px 22px}
.ht_act-call{float:right;display:flex}
.ht_act-call .needcall{padding:7px 15px;color:#fff;background:#f56d5380;font-size:14px;cursor:pointer}
.ht_act-call .needcall span{margin-left:5px}
.ht_act-call .needcall:hover{background:#f56d53CC}
.ht_act-call .icall{padding:4px 15px;font-size:16px;color:#eee;cursor:pointer}
.ht_act-call .icall span{margin-left:5px;border-bottom:1px dashed #bbb}
.ht_act-call .icall:hover{background:#ffffff0D}
.hb-wrap.resp{background-color: #000000CC!important;}
.hb-wrap.resp li a{padding: 15px 15px;}
@media screen and (max-width: 992px) {
.nm{display:none}
.ht{display:block!important}
}
@media screen and (max-width: 778px) {
.icall{display:none!important}
}
@media screen and (max-width: 415px) {
.ht .ht_act-co li{border-right:0;padding:0 0 0 10px}
.nm2{display:none}
}
</style>
<header>
    <nav class="h_nav-3">
        <div class="ht ht-3">
            <div class="ht-wrap ht-wrap-3">
                <ul class="ht_act-co">
                    <li class="nm2"><i class="fa fa-location-arrow" aria-hidden="true"></i><span>{{$info->cities->gorod or 'Город'}}</span></li>
                    <li><i class="fa fa-clock-o red" aria-hidden="true"></i><span>{{$info->timeJob or 'пн-вс, 10:00-17:00'}}</span></li>
                    <li class="nm"><a href="/contact"><i class="fa fa-map-marker red" aria-hidden="true"></i><span>Адрес офиса</span></a></li>
                </ul>
                <div class="ht_act-call">
                    <div class="needcall" id="needcallhead"><i class="fa fa-undo red" aria-hidden="true"></i><span>Обратный звонок</span></div>
                    <div class="icall"><a href="tel:{{$tel->tel or ''}}"><i class="fa fa-phone red" aria-hidden="true"></i><span>{{$tel->tel or '+7 (900) 000-00-00'}}</span></a></div>
                </div>
            </div>
        </div>
        <div class="hb">
            <div class="hb-wrap" id="id-hb-wrap">
                <div class="hb_logo">
                    <a href="/" class="hb_brand" id="logo1" style="background-image: url(/storage/app/{{$info->logo1 or ''}});"></a>
                </div>
                <div class="icon">
                    <a href="javascript:void(0);" onclick="myFunction()"><div class="bar1"></div><div class="bar2"></div><div class="bar3"></div></a>
                </div>
                <ul class="hb_nav">
                    @forelse($categories as $c)
                        <li class="dropdown sort-2"><a href="/izgotovleniye/{{$c->id}}-{{$c->slug}}" class="dropbtn nolink">@if($c->seoname != null){{$c->seoname}}@else{{$c->title or ''}}@endif</a></li>
                    @empty
                    @endforelse
                </ul>
                <ul class="hb_act-co">
                    <li><i class="fa fa-map-marker red" aria-hidden="true"></i><span>{{$info->cities->gorod or 'Город'}}</span></li>
                    <li><i class="fa fa-envelope red" aria-hidden="true"></i><span>{{$email->email or 'contact@company.ru'}}</span></li>
                    <li><a href="tel:{{$tel->tel or ''}}" style="padding:0"><i class="fa fa-phone red" aria-hidden="true"></i><span>{{$tel->tel or '+7 (900) 000-00-00'}}</span></a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>