<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1">
@if($seoTitle != null)<title>{{$seoTitle}}</title>@else<title>Заполните title страницы</title>@endif
@if($seoDescription != null)<meta name="description" content="{{$seoDescription}}"/>@endif
@if($favicon->favicon != null)
<link rel="icon" href="/storage/app/{{$favicon->favicon}}" type="image/png">
@else
<link rel="icon" href="/public/site/img/favicon.png" type="image/png">
@endif
<meta name="csrf-token" content="{{ csrf_token() }}">
@if($canonical != null)
<link rel="canonical" href="{{$canonical}}"/>
@endif
<style>body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#f3f3f3}*,::after,::before{box-sizing:border-box}a{color:#007bff;text-decoration:none;background-color:transparent;-webkit-text-decoration-skip:objects}.row{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin:0 -15px}.col-3{-webkit-box-flex:0;-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%;padding:0 15px}.col-9{-webkit-box-flex:0;-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%;padding:0 15px}@media screen and (max-width: 992px){.col-3{display:none}.col-9{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%;padding:0}}header{width:100%}.i80{width:80%;margin:0 auto 40px;overflow:hidden}@media screen and (min-width: 1400px){.i80{max-width:1140px}}@media screen and (max-width: 1399px){.i80{width:100%;padding:0 20px}}@media screen and (max-width: 992px){.i80{padding:0 10px}}@media screen and (max-width: 490px){.i80{padding:0 5px}}.h_nav{width:100%}.ht{border-top:3px solid #269ffd;background-color:#fff;width:100%}.ht-wrap{overflow:hidden;width:80%;margin:0 auto;font-size:13px;color:#656565;padding:7px 0}.ht .ht_act-co{padding:0;margin:0;display:table;float:left;list-style-type:none}.ht .ht_act-co li{float:left;padding:0 10px;border-right:1px solid #ddd}.ht .ht_act-co li:first-child{border-left:1px solid #ddd}.ht .ht_act-co li span{margin-left:10px;}.ht .ht_act-user{display:table;float:right}.ht .ht_act-user span{margin-left:10px;border-bottom: 1px dashed #000;}.ht .ht_act-user .ht-item{color:#656565;padding:0 10px;border-left:1px solid #ddd}.ht .ht_act-user .ht-item:last-child{border-right:1px solid #ddd}.hb{background-color:#269ffd;width:100%}.hb-wrap{display:table;padding-top:0;padding-bottom:0;width:80%;margin:0 auto}.hb-wrap .hb_logo{margin:5px 0;height:60px;float:left;width:100%;max-width:270px}.hb-wrap .hb_brand{background-size:contain;background-position:left;height:60px;padding:0;width:100%;overflow:hidden;background-repeat:no-repeat;display:block}.hb-wrap li{display:block;position:relative;text-transform:uppercase;font-weight:600;text-decoration:none;font-size:13px}.hb-wrap li a{padding:25.5px 13px;display:block}.hb-wrap li:hover{background-color:#48aefd}.hb-wrap a{color:#fff}.hb-wrap a:hover{color:#fff}.hb-wrap ul{margin:0;padding:0;float:right;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-webkit-justify-content:flex-end;-ms-flex-pack:inherit;justify-content:flex-end;text-align:left}.hb-wrap ul .sort-1{-webkit-box-ordinal-group:1;-webkit-order:1;-ms-flex-order:1;order:1}.hb-wrap ul .sort-2{-webkit-box-ordinal-group:2;-webkit-order:2;-ms-flex-order:2;order:2}.hb-wrap ul .sort-3{-webkit-box-ordinal-group:3;-webkit-order:3;-ms-flex-order:3;order:3}.hb-wrap .only-m{display:none}.hb-wrap .icon{display:none}.hb .hb_act-co{display:none}.hb .bar1,.bar2,.bar3{width:25px;height:3px;background-color:#fff;margin:6px 0;transition:.4s}.hb-wrap .dropdown-menu{display:none}@media (min-width: 1400px){.ht-wrap{max-width:1140px}.hb-wrap{max-width:1140px;padding:0}}@media (max-width: 1399px){.ht-wrap{width:100%;padding:5px 20px}.hb-wrap{width:100%;padding:0 20px}}@media screen and (max-width: 1200px){.hb-wrap .hb_logo{max-width:245px}}@media screen and (min-width: 993px) and (max-width: 1200px){.hb-wrap ul .sort-1{-webkit-box-ordinal-group:3;-webkit-order:3;-ms-flex-order:3;order:3}.hb-wrap ul .sort-3{display:none}.hb-wrap .only-m{display:block}}@media screen and (max-width: 992px){.ht-wrap{width:100%;padding:5px 0}.hb-wrap ul{padding:20px 0}.ht .ht_act-co li:first-child{border-left:0}.ht .ht_act-user .ht-item:last-child{border-right:0}.hb-wrap .hb_logo{max-width:220px}.hb-wrap li{margin:0;display:table;width:100%}.hb-wrap li a{padding:15px 13px}.hb-wrap .hb_nav{display:none;width:100%}.hb-wrap .icon{float:right;display:block;padding:18px 0;display:table}.hb-wrap .icon a{color:#fff}}@media screen and (max-width: 778px){.ht .ht_act-user{display:none}.hb-wrap{padding:0 10px}}@media screen and (max-width: 576px){.ht{display:none}.hb .resp .hb_act-co{display:block;width:100%;color:#fff;padding:10px 0 0;margin:10px 0;border-top:1px solid #fff}.hb .resp .hb_act-co li{text-transform:none;padding:8px 13px}.hb .resp .hb_act-co span{padding-left:10px;font-weight:400;font-size:14px}}</style>
@if(!isset($jq))
<script type="text/javascript" src="/public/site/js/jquery-3.2.1.min.js"></script>
@endif
@if($favicon->verYa !== null)
<meta name="yandex-verification" content="{{$favicon->verYa or ''}}" />
@endif

@if($favicon->verGo !== null)
<meta name="google-site-verification" content="{{$favicon->verGo or ''}}" />
@endif
</head>


