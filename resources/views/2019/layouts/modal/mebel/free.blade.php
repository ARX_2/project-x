<div id="modal_form_needcall" class="modal-ui-wrapper" style="background: rgba(0, 0, 0, 0.565);">
    <div class="modal-ui-modal" style="background: rgb(255, 255, 255);">
        <div class="modal-ui-content">
            <div class="title"><h2>Заполните простую форму!</h2></div>
            <form id="mebelmodalfree" method="post">
            {{ csrf_field() }}
            <div class="ui-b">
                <div class="ui-b-form" style="padding: 20px">
                    <div class="form-group">
                        <div class="label">Контактный телефон:</div>
                        <div class="g-input"><input type="text" name="phone"placeholder="+7 (900) 000-00-00"></div>
                    </div>
                    <div class="form-group">
                        <div class="label">Ваше сообщение:</div>
                        <div class="g-input"><textarea  name="description" maxlength="500" placeholder="Оставьте комментарий или задайте интересующий вопрос"></textarea></div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="form_name" value="{{$blockname}}" readonly>
            <div class="ui-f"><button type="submit">Отправить</button></div>
            </form>
        </div>
        <div id="modal_close_needcall" class="modal-ui-close"><span>×</span></div>
    </div>
</div>
<input style="display: none" type="text" class="id" value="">
<script type="text/javascript">
    $('#mebelmodalfree').on('submit', function(e){
        var urlhost = window.location.href;
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/modal/mebel/free/send',
            data: $('#mebelmodalfree').serialize()+ "&urlhost=" + urlhost,
            success:function(data){
                console.log(data);
            }
        });
    });
    $(document).ready(function() {
        $(document).ready(function() {$('#needcall').click( function(event){event.preventDefault();$('#overlay').fadeIn(50, function(){$('#modal_form_needcall').css('display', 'flex').animate({opacity: 1}, 50);});});
            $('#modal_close_needcall, #overlay').click( function(){$('#modal_form_needcall').animate({opacity: 0}, 100, function(){$(this).css('display', 'none');$('#overlay').fadeOut(100);});});
        });
    });
</script>
