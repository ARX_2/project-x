<footer class="f_2" @if($siteID == 2)style="background-image: url(/public/site/img/back-u1.jpg);background-position: bottom;background-size: cover"@endif>
    <div class="wrapper_block">
        <div class="i80f">
            <div class="media">
                <div class="social_line"></div>
            </div>
        </div>
    </div>
    <div class="i80f">
        <div class="nav">
            <div class="row">
                <div class="col-sm-4 about">
                    <img src="{{$company->information->logo2}}" alt="" class="footer-logo">
                    <div class="phone">
                        <div class="img"></div>
                        <div class="title"><p>Нужна помощь специалиста? Звоните:</p><a href="tel:{{$tel->tel or ''}}">{{$tel->tel or '+7 (900) 000-00-00'}}</a></div>
                    </div>
                    <div class="phone email">
                        <div class="img"></div>
                        <div class="title"><p>Почта для заявок:</p>{{$email->email or 'email@company.ru'}}</div>
                    </div>
                    <div class="location">
                        <div class="img"></div>
                        <div class="title">Контактная информация:<p>г. {{$city->gorod or 'Город'}}@if($company->address != null), {{$company->address or ', ул. Улица 1, оф. 1'}}@endif</p></div>
                    </div>
                </div>
                <div class="col-sm-8 footer-nav">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="name">Компания</div>
                            <ul>
                                <li><a href="/about">О компании</a></li> @forelse($company->sections as $i)@if($i->slug == 'action')
                                    <li><a href="/{{$i->slug}}">{{$i->title}}</a></li>@endif @if($i->slug == 'documantation')
                                    <li><a href="/about/documents">{{$i->title}}</a></li>@endif @if($i->slug == 'jobs')
                                    <li><a href="/{{$i->slug}}">{{$i->title}}</a></li>@endif @if($i->slug == 'news')
                                    <li><a href="/{{$i->slug}}">{{$i->title}}</a></li>@endif @if($i->slug == 'blog')
                                    <li><a href="/{{$i->slug}}">{{$i->title}}</a></li>@endif @if($i->slug == 'pricelist')
                                    <li><a href="/price-list">{{$i->title}}</a></li>@endif @if($i->slug == 'partners')
                                    <li><a href="/{{$i->slug}}">{{$i->title}}</a></li>@endif @empty @endforelse
                                <li><a href="/about/requisites">Реквизиты</a></li>
                                <li class="only-m"><a href="/contact">Контакты</a></li>
                            </ul>
                        </div>

                        @if(isset($menu[0]) && $menu[0]->published == 1)
                        <div class="col-sm-4">
                            <div class="name">{{$menu[0]->title or 'Товары'}}</div>
                            <ul>
                                @forelse($categories as $category)
                                    @if($category->type == $menu[0]->type)
                                        <li><a href="/{{$menu[0]->slug or 'product'}}/{{$category->id}}-{{$category->slug or ''}}">@if($category->seoname != null){{$category->seoname}}@else{{$category->title or ''}}@endif</a></li>
                                    @endif
                                @empty
                                @endforelse
                            </ul>
                        </div>
                        @endif

                        @if(isset($menu[1]) && $menu[1]->published == 1)
                        <div class="col-sm-4">
                            <div class="name">{{$menu[1]->title or 'Услуги'}}</div>
                            <ul>
                                @forelse($categories as $category)
                                    @if($category->type == $menu[1]->type)
                                        <li><a href="/{{$menu[1]->slug or 'services'}}/{{$category->id}}-{{$category->slug or ''}}">@if($category->seoname != null){{$category->seoname}}@else{{$category->title or ''}}@endif</a></li>
                                    @endif
                                @empty
                                @endforelse
                            </ul>
                        </div>
                        @endif
                        @if(isset($menu[0]) && $menu[0]->published == 0 || isset($menu[1]) && $menu[1]->published == 0)
                        <div class="col-sm-4">
                            <div class="name">Самое читаемое</div>
                            <ul>
                            @forelse($goods as $g)
                                <li><a href="@if($g->product_url_cat->type == 0)/product/@elseif($g->product_url_cat->type == 1)/services/@endif{{$g->product_url_cat->id}}-{{$g->product_url_cat->slug}}/{{$g->id}}-{{$g->slug}}">{{$g->title}}</a></li>
                            @empty
                            @endforelse
                            </ul>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copywriting">
        <div class="i80f">
            <div class="block">
                <div class="copy">{{date('Y')}} © "{{$company->name or ''}}" - {{$company->description or ''}}</div>
            </div>
            <div class="block">
                <a href="/login"><div class="copy">Войти</div></a>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript">
    document.getElementById('needcallhead').onclick = function() {$.get('https://{{$company->title or ''}}/take/modal/needcall', function(data){$('.modal').html(data);})};
</script>
<div class="modal"></div>
{!! $company->information->metrika or '' !!}
{!! $company->information->analytics or '' !!}
<script>
    function loadCSS(hf) {var ms=document.createElement("link");ms.rel="stylesheet";ms.href=hf;document.getElementsByTagName("footer")[0].appendChild(ms);}
    loadCSS("/public/site/css/style.css");
    loadCSS("/public/site/font-awesome/css/font-awesome.min.css");
</script>

<script>var scr = {"scripts":[
        {"src" : "/public/site/js/jquery-3.2.1.min.js", "async" : false},
        {"src" : "/public/site/js/youtuber/youtuber.js", "async" : false},
        //{"src" : "/public/site/js/index.js", "async" : false}
        {"src" : "/public/site/js/main.js", "async" : false},
        {"src" : "https://bittersmann.de/lib/backlink/backlink.js", "async" : false}
    ]};!function(t,n,r){"use strict";var c=function(t){if("[object Array]"!==Object.prototype.toString.call(t))return!1;for(var r=0;r<t.length;r++){var c=n.createElement("script"),e=t[r];c.src=e.src,c.async=e.async,n.body.appendChild(c)}return!0};t.addEventListener?t.addEventListener("load",function(){c(r.scripts);},!1):t.attachEvent?t.attachEvent("onload",function(){c(r.scripts)}):t.onload=function(){c(r.scripts)}}(window,document,scr);
</script>
@if($siteID == 83)
<script type='text/javascript'>
(function () {
    window['yandexChatWidgetCallback'] = function() {
        try {
            window.yandexChatWidget = new Ya.ChatWidget({
                guid: '776360e3-d930-4796-a34b-a167f825cca1',
                buttonText: 'Напишите нам, мы в сети!',
                title: 'Чат с Евромебель',
                theme: 'light',
                collapsedDesktop: 'never',
                collapsedTouch: 'always'
            });
        } catch(e) { }
    };
    var n = document.getElementsByTagName('script')[0],
        s = document.createElement('script');
    s.async = true;
    s.charset = 'UTF-8';
    s.src = 'https://chat.s3.yandex.net/widget.js';
    n.parentNode.insertBefore(s, n);
})();
</script>
@endif
@if($siteID == 72)
<script type='text/javascript'>
(function () {
    window['yandexChatWidgetCallback'] = function() {
        try {
            window.yandexChatWidget = new Ya.ChatWidget({
                guid: 'f6d1ae5d-929e-4dbf-ba31-7903cededcc5',
                buttonText: 'Напишите нам, мы в сети!',
                title: 'Чат с Евромебель',
                theme: 'light',
                collapsedDesktop: 'never',
                collapsedTouch: 'always'
            });
        } catch(e) { }
    };
    var n = document.getElementsByTagName('script')[0],
        s = document.createElement('script');
    s.async = true;
    s.charset = 'UTF-8';
    s.src = 'https://chat.s3.yandex.net/widget.js';
    n.parentNode.insertBefore(s, n);
})();
</script>
@endif
@if($siteID == 70)
<script type='text/javascript'>
(function () {
    window['yandexChatWidgetCallback'] = function() {
        try {
            window.yandexChatWidget = new Ya.ChatWidget({
                guid: '74148f2f-a700-4b72-abb3-0c890a25a56b',
                buttonText: 'Напишите нам, мы в сети!',
                title: 'Чат c Евромебель',
                theme: 'light',
                collapsedDesktop: 'never',
                collapsedTouch: 'always'
            });
        } catch(e) { }
    };
    var n = document.getElementsByTagName('script')[0],
        s = document.createElement('script');
    s.async = true;
    s.charset = 'UTF-8';
    s.src = 'https://chat.s3.yandex.net/widget.js';
    n.parentNode.insertBefore(s, n);
})();
</script>
@endif
@if($siteID == 68)
<script type='text/javascript'>
(function () {
    window['yandexChatWidgetCallback'] = function() {
        try {
            window.yandexChatWidget = new Ya.ChatWidget({
                guid: 'f17f5697-1129-46f8-a884-998f3b54a1be',
                buttonText: 'Напишите нам, мы в сети!',
                title: 'Чат с Евромебель',
                theme: 'light',
                collapsedDesktop: 'never',
                collapsedTouch: 'always'
            });
        } catch(e) { }
    };
    var n = document.getElementsByTagName('script')[0],
        s = document.createElement('script');
    s.async = true;
    s.charset = 'UTF-8';
    s.src = 'https://chat.s3.yandex.net/widget.js';
    n.parentNode.insertBefore(s, n);
})();
</script>
@endif
@if($siteID == 64)
<script type='text/javascript'>
(function () {
    window['yandexChatWidgetCallback'] = function() {
        try {
            window.yandexChatWidget = new Ya.ChatWidget({
                guid: 'f9dbaa7c-eb4d-4cb9-96da-d55090dbf432',
                buttonText: 'Напишите нам, мы в сети!',
                title: 'Чат c Евромебель',
                theme: 'light',
                collapsedDesktop: 'never',
                collapsedTouch: 'always'
            });
        } catch(e) { }
    };
    var n = document.getElementsByTagName('script')[0],
        s = document.createElement('script');
    s.async = true;
    s.charset = 'UTF-8';
    s.src = 'https://chat.s3.yandex.net/widget.js';
    n.parentNode.insertBefore(s, n);
})();
</script>
@endif
@if($siteID == 46)
<script type='text/javascript'>
(function () {
    window['yandexChatWidgetCallback'] = function() {
        try {
            window.yandexChatWidget = new Ya.ChatWidget({
                guid: '2d37cccc-5436-4352-9b4d-5134ff7b350c',
                buttonText: 'Напишите мне, я в сети!',
                title: 'Чат с Димой',
                theme: 'light',
                collapsedDesktop: 'never',
                collapsedTouch: 'always'
            });
        } catch(e) { }
    };
    var n = document.getElementsByTagName('script')[0],
        s = document.createElement('script');
    s.async = true;
    s.charset = 'UTF-8';
    s.src = 'https://chat.s3.yandex.net/widget.js';
    n.parentNode.insertBefore(s, n);
})();
</script>
@endif
@if($siteID == 104)
<script type='text/javascript'>
(function () {
    window['yandexChatWidgetCallback'] = function() {
        try {
            window.yandexChatWidget = new Ya.ChatWidget({
                guid: 'a8db7990-22ab-41f7-b2a4-df9ded8bb4b6',
                buttonText: 'Напишите нам, мы в сети!',
                title: 'Чат с Devo',
                theme: 'light',
                collapsedDesktop: 'never',
                collapsedTouch: 'always'
            });
        } catch(e) { }
    };
    var n = document.getElementsByTagName('script')[0],
        s = document.createElement('script');
    s.async = true;
    s.charset = 'UTF-8';
    s.src = 'https://chat.s3.yandex.net/widget.js';
    n.parentNode.insertBefore(s, n);
})();
</script>
@endif
</body>
