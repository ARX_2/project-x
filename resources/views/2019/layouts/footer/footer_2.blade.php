<style>
    .f_3{background-color:#f3f3f3;border-top:1px solid #e6e6e6}
    .f_3 .row{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-webkit-justify-content:flex-end;-ms-flex-pack:inherit;justify-content:flex-end}
    .f_3 .sort-2{-webkit-box-ordinal-group:1;-webkit-order:1;-ms-flex-order:1;order:1}
    .f_3 .sort-1{-webkit-box-ordinal-group:2;-webkit-order:2;-ms-flex-order:2;order:2}
    .f_3 .wrapper_block{background:linear-gradient(-90deg,#269ffd,#269ffd)}
    .f_3 .nav{margin:0 auto;padding:40px 0}
    .f_3 .nav .row{width:100%;margin:0}
    .f_3 .footer-nav{padding:0}
    .f_3 .col-sm-4{-webkit-box-flex:0;-ms-flex:0 0 25%;flex:0 0 25%;max-width:25%}
    .f_3 .col-sm-8{-webkit-box-flex:0;-ms-flex:0 0 75%;flex:0 0 75%;max-width:75%}
    .f_3 .tel{font-size:20px;line-height:20px;padding:5px 0;color:#222;font-weight:700;display:block;margin-bottom:5px}
    .f_3 .tel:hover{text-decoration:none;color:#48a022}
    .f_3 a{text-decoration:none;-webkit-transition:all .2s ease-in-out;-moz-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;-ms-transition:all .2s ease-in-out;transition:all .2s ease-in-out}
    .f_3 .button-arrow{display:inline-block;vertical-align:top;padding:11px;padding-left:42px;padding-right:22px;background:#48a022;background-size:cover;font-size:13px;line-height:13px;position:relative;color:#fff!important;margin-bottom:5px}
    .f_3 .button-arrow:after{position:absolute;left:15px;top:50%;width:15px;height:15px;margin-top:-6px;content:"\f0e2";font-family:FontAwesome;font-size:14px;line-height:14px}
    .f_3 .f_email{display:block;position:relative;margin-bottom:5px}
    .f_3 .f_email:after{position:absolute;left:5px;top:50%;width:13px;height:13px;margin-top:-5px;content:"\f0e0";font-family:FontAwesome;font-size:12px;line-height:12px}
    .f_3 .button_email{margin-left:25px;font-size:13px;line-height:13px;color:#222}
    .f_3 .f_address{display:block;position:relative}
    .f_3 .f_address p{margin-left:25px;font-size:14px;line-height:18px;color:#222}
    .f_3 .f_address:after{position:absolute;left:5px;top:50%;width:16px;height:16px;margin-top:-8px;content:"\f041";font-family:FontAwesome;font-size:16px;line-height:16px}
    .f_3 .f_social{display:inline-flex;margin-top:15px}
    .f_3 .f_social .i{width:25px;max-width:25px;height:25px;text-align:center;line-height:25px;font-size:25px;margin:0 5px}
    .f_3 .f_social .i.vk i{color:#5181b8}
    .f_3 .f_social .i.inst i{color:#c13584}
    .f_3 .f_social .i.ok i{color:#f7931e}
    .f_3 .f_social .i.you i{color:#fe021c}
    .f_3 .cols-text{font-size:13px;margin-bottom:25px}
    .f_3 .cols-text p{margin-bottom:5px}
    .f_3 .f_nav{display:inline-flex;width:100%}
    .f_3 .f_nav .i{width:25%}
    .f_3 .f_nav .i .name{font-size:15px;font-weight:600}
    .f_3 .f_nav .i li{font-size:14px;line-height:16px;margin:7px 0}
    .f_3 .f_nav .i li a{color:#222;border-bottom:1px dashed #222}
    .f_3 .f_nav .i li a:hover{text-decoration:none;list-style-type:none;color:#48a022}
    .f_3 ul{list-style-type:none;padding:0;margin-bottom:0;margin-right:30px}
    .f_3 .f_partner{margin-top:25px;display:table;width:100%}
    .f_3 .f_partner .name{font-size:15px;font-weight:600;margin-bottom:5px}
    .f_3 .f_partner ul{display:table;width:100%}
    .f_3 .f_partner li{float:left;width:25%;font-size:14px;line-height:16px;margin:4px 0}
    .f_3 .f_partner li a{color:#222;border-bottom:1px dashed #222}
    @media (max-width: 962px) {
        .f_3 .sort-1{-webkit-box-ordinal-group:1;-webkit-order:1;-ms-flex-order:1;order:1}
        .f_3 .sort-2{-webkit-box-ordinal-group:2;-webkit-order:2;-ms-flex-order:2;order:2}
        .f_3 .col-sm-4{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}
        .f_3 .col-sm-8{padding-left:15px;margin-top:25px;-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}
        .f_3 .f_nav{display:inline-block}
        .f_3 .f_nav .i{width:33%;float:left}
        .f_3 .f_partner li{width:33%}
    }
    @media (max-width: 480px) {
        .f_3 .f_nav .i{width:50%}
        .f_3 .f_email:after{display:none}
        .f_3 .f_address:after{display:none}
        .f_3 .f_partner li{width:50%}
        .f_3 .f_3 .col-sm-4{text-align:center}
        .f_3 ul{margin-right:0}
    }
</style>


<footer class="f_3">
    <div class="i80f">
        <div class="nav">
            <div class="row">
                <div class="col-sm-8 footer-nav sort-2">
                    <div class="cols-text">
                        <p>© 2020</p>
                        <p>Сайт носит исключительно информационный характер и не является публичной офертой, определяемой положениями ГК РФ. Для получения подробной информации о наличии, видах, характеристиках и стоимости обращайтесь в офис продаж.</p>
                        <p><a href="/soglasiye-na-obrabotku-personalnykh-dannykh"><span style="text-decoration: underline;color: #222;">Согласие&nbsp;на&nbsp;обработку&nbsp;персональных&nbsp;данных</span></a> •&nbsp;<a href="/politika-konfidentsialnosti"><span style="text-decoration: underline;color: #222;">Политика&nbsp;конфиденциальности</span></a></p>
                    </div>
                    <div class="f_nav">
                        <div class="i">
                            <div class="name">Компания</div>
                            <ul>
                                <li><a href="/about">О компании</a></li>
                            @forelse($company->sections as $i)
                                @if($i->slug == 'action')<li><a href="/{{$i->slug}}">{{$i->title}}</a></li>@endif
                                @if($i->slug == 'documantation')<li><a href="/about/documents">{{$i->title}}</a></li>@endif
                                @if($i->slug == 'jobs')<li><a href="/{{$i->slug}}">{{$i->title}}</a></li>@endif
                                @if($i->slug == 'news')<li><a href="/{{$i->slug}}">{{$i->title}}</a></li>@endif
                                @if($i->slug == 'blog')<li><a href="/{{$i->slug}}">{{$i->title}}</a></li>@endif
                                @if($i->slug == 'pricelist')<li><a href="/price-list">{{$i->title}}</a></li>@endif
                                @if($i->slug == 'partners')<li><a href="/{{$i->slug}}">{{$i->title}}</a></li>@endif
                            @empty
                            @endforelse
                                <li><a href="/about/requisites">Реквизиты</a></li>
                                <li class="only-m"><a href="/contact">Контакты</a></li>
                            </ul>
                        </div>
                        <?php $ii = 0 ?>
                        @forelse($mainsection as $m)
                        <?php $ii++ ?>
                        <div class="i">
                            <div class="name">{{$m->title}}</div>
                            <ul>
                                @forelse($m->category as $c)
                                <li><a href="/{{$m->slug}}/{{$c->id}}-{{$c->slug}}">@if($c->seoname != null){{$c->seoname}}@else{{$c->title}}@endif</a></li>
                                @empty
                                @endforelse
                            </ul>
                        </div>
                        @empty
                        @endforelse
                        @if($portfolio != null && $ii < 3)
                        <div class="i">
                            <div class="name">Портфолио</div>
                            <ul>
                                @forelse($portfolio as $c)
                                <li><a href="/portfolio/{{$c->id}}-{{$c->slug}}">@if($c->seoname != null){{$c->seoname}}@else{{$c->title}}@endif</a></li>
                                @empty
                                @endforelse
                            </ul>
                        </div>
                        @endif
                    </div>
                    @if(count($partners) != null)
                    <div class="f_partner">
                        <div class="name">Наши партнеры в других городах</div>
                        <ul>
                            @forelse($partners as $p)
                            <li><a href="{{$p->link}}">@if($p->title != null){{$p->title}}@else{{$p->link}}@endif</a></li>
                            @empty
                            @endforelse
                        </ul>
                    </div>
                    @endif
                </div>
                <div class="col-sm-4 about sort-1">
                    <div class="phone">
                        <div class="title"><a href="tel:{{$tel->tel or ''}}" class="tel">{{$tel->tel or 'Укажите телефон'}}</a></div>

                    </div>
                    <div id="needcallfooter" class="button-arrow open-form">Заказать звонок</div>
                    <div class="f_email">
                        <a href="" class="button_email">{{$email->email or 'Укажите почту'}}</a>
                    </div>
                    <div class="f_address">
                        <p>г. {{$city->gorod or 'Укажите город'}}@if($company->address != null), {{$company->address or ''}} @else , Укажите адрес @endif<a href="/contact">Показать&nbsp;на&nbsp;карте</a></p>
                    </div>
                    <div class="f_social">
                        <div class="i vk"><a href="{{$company->information->vkontakte}}" target="_blank" rel="nofollow"><i class="fa fa-vk" aria-hidden="true"></i></a></div>
                        <div class="i inst"><a href="{{$company->information->instagram}}" target="_blank" rel="nofollow"><i class="fa fa-instagram" aria-hidden="true"></i></a></div>
                        <div class="i ok"><a href="{{$company->information->odnoklassniki}}" target="_blank" rel="nofollow"><i class="fa fa-odnoklassniki-square" aria-hidden="true"></i></a></div>
                        <div class="i you"><a href="{{$company->information->youtube}}" target="_blank" rel="nofollow"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="/public/site/js/divbox.js" type="text/javascript"></script>
<div class="modal"></div>
{!! $company->information->metrika or '' !!}
{!! $company->information->analytics or '' !!}
<script>
    function loadCSS(hf) {var ms=document.createElement("link");ms.rel="stylesheet";ms.href=hf;document.getElementsByTagName("footer")[0].appendChild(ms);}
    loadCSS("/public/site/css/style.css");
    loadCSS("/public/site/js/divbox.css");
    loadCSS("/public/site/font-awesome/css/font-awesome.min.css");
</script>
<script>var scr = {"scripts":[
            // {"src" : "/public/site/js/jquery-3.2.1.min.js", "async" : false},
            // {"src" : "/public/site/js/youtuber/youtuber.js", "async" : false},
            //{"src" : "/public/site/js/index.js", "async" : false}
            {"src" : "/public/site/js/main.js", "async" : false},
            {"src" : "https://bittersmann.de/lib/backlink/backlink.js", "async" : false}
        ]};!function(t,n,r){"use strict";var c=function(t){if("[object Array]"!==Object.prototype.toString.call(t))return!1;for(var r=0;r<t.length;r++){var c=n.createElement("script"),e=t[r];c.src=e.src,c.async=e.async,n.body.appendChild(c)}return!0};t.addEventListener?t.addEventListener("load",function(){c(r.scripts);},!1):t.attachEvent?t.attachEvent("onload",function(){c(r.scripts)}):t.onload=function(){c(r.scripts)}}(window,document,scr);
</script>
@if($company->type == 3)
    <style>
        .zamer5 {
            position: fixed;
            right: 0px;
            bottom: 50px;
            z-index: 99;
        }
        .zamer5 img {
            width: 145px;
            height: auto;
            cursor: pointer;
        }
        @media screen and (max-width: 979px) and (min-width: 460px){
            .zamer5 {
                bottom: 120px;
            }
        }
        @media screen and (max-width: 459px) and (min-width: 200px){
            .h200 {
                display: none!important;
            }
        }
        @media screen and (max-width: 1199px) and (min-width: 980px){
            .zamer5 img {
                width: 120px;
            }
        }
        @media screen and (max-width: 979px) and (min-width: 460px){
            .zamer5 img {
                width: 100px;
            }
        }
    </style>

    <div class="zamer5 zakaz h200" id="needcallvid"><img src="/public/site/img/zamer5.svg"></div>

@endif
<script type="text/javascript">
    $(document).ready(function(){
        $('#needcallhead').click(function(){$.get('{{route('modal.open.izgotovleniye')}}', {block: 1}, function(data){$('.modal').html(data);})});
        $('#needcallfooter').click(function(){$.get('{{route('modal.open.izgotovleniye')}}', {block: 2}, function(data){$('.modal').html(data);})});
        $('#needcallvid').click(function(){$.get('{{route('modal.open.izgotovleniye')}}', {block: 3}, function(data){$('.modal').html(data);})});
        $('#dizain').click(function(){$.get('{{route('modal.open.izgotovleniye')}}', {block: 4}, function(data){$('.modal').html(data);})});
    });
</script>
@if($info->color_menu != null)
<style>
    .c-gd .act .area, .c-gd .act_button:hover{background:#{{$info->color_menu}}!important;}
    section.windows-2 {background: #{{$info->color_menu}} url(/public/site/img/bg-consult-min.png) no-repeat right bottom;}
    .f_3 .button-arrow{background: #{{$info->color_menu}};}
    .windows-6 .button-project{background: #{{$info->color_menu}} url(/public/site/img/head-project.png) no-repeat 25px 10px;}
    .left_links .linkoo:hover{border-color: #{{$info->color_menu}};color: #{{$info->color_menu}};}
    .act .btn_question{background: #{{$info->color_menu}};}
    .f_3 .tel:hover{color:#{{$info->color_menu}}}
</style>
@endif
</body>
