<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
 <url>
  <loc>http://{{$url->title}}/</loc>
 </url>

 @forelse($categories as $category)
 @if($category->type == 0)
 <?php $type = 'product'?>
 @elseif($category->type == 1)
 <?php $type = 'services'?>
  @elseif($category->type == 4)
 <?php $type = 'stroitelstvo'?>
  @elseif($category->type == 5)
 <?php $type = 'bani'?>
  @elseif($category->type == 6)
 <?php $type = 'proyekty'?>
  @elseif($category->type == 7)
 <?php $type = 'izgotovleniye'?>
 @endif
 <url>
  <loc>http://{{$url->title}}/{{$type}}/{{$category->id}}-{{$category->slug}}</loc>
 </url>
 @forelse($category->goods as $goods)
 <url>
  <loc>http://{{$url->title}}/{{$type}}/{{$category->id}}-{{$category->slug}}/{{$goods->id}}-{{$goods->slug}}</loc>
 </url>
 @empty
 @endforelse
@empty
@endforelse

</urlset>
