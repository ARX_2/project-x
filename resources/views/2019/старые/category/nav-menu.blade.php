<section class="i80">
    <div class="page_category_2">
        <div class="row">
            <div class="col-3">
                <div class="nsm">
                    <div class="title">
                        Категории
                    </div>
                    <style>.nsm{background:#fff;font-size:14px;width:100%;padding:15px;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}.nsm .title{line-height:17px;font-size:17px;margin-bottom:10px;font-weight:600}.nsm ul{list-style:none;padding:0;margin:0}.nsm .parent_cat{position:relative;display: table;color:#666;font-weight:600;border-bottom:1px solid #e6e6e6;cursor:pointer}.nsm .parent_cat span{display:table-cell;padding:10px 0;width:100%;color:#007bff}</style>
                    <div class="menu-list">
                        <ul class="menu-content">
                            @forelse($categories as $category)
                            @if($category->type == 0)
                                <?php $type = 'product'; ?>
                            @else
                                <?php $type = 'services'; ?>
                            @endif

                            <li class="cat-item @if(count($category->subcategories)>0)collapse @else @endif">
                                <div class="parent_cat" @if(count($category->subcategories)>0)onclick="smFunction('sm-{{$category->id}}')" @else @endif>
                                    @if(count($category->subcategories)>0)
                                        <span>{{$category->title or ''}}</span>
                                    @else
                                        <a href="/{{$type or 'category'}}/{{$category->id}}-{{$category->slug or ''}}"><span>{{$category->title or ''}}</span></a>
                                    @endif
                                </div>

                                @if(count($category->subcategories)>0)
                                <ul class="sub-menu collapsed" id="sm-{{$category->id}}">
                                    @forelse($category->subcategories as $sub)
                                    <li class="sub_cat"><a href="/{{$type or 'category'}}/{{$sub->id}}-{{$sub->slug or ''}}">{{$sub->title or ''}}</a></li>
                                    @empty
                                    @endforelse
                                </ul>
                                @endif
                            </li>
                            @empty
                            @endforelse
                        </ul>
                    </div>
                </div>
