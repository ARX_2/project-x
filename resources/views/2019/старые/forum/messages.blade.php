<style>
    body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left}
    *,::after,::before{box-sizing:border-box}
    a{color:#007bff;text-decoration:none;background-color:transparent;-webkit-text-decoration-skip:objects}
    header{width:100%}

    .h_nav{width:100%}
    .ht{border-top:3px solid #269ffd;background-color:#fff;width:100%}
    .ht-wrap{overflow:hidden;width:80%;margin:0 auto;font-size:13px;color:#656565;padding:7px 0}
    .ht .ht_act-co{padding:0;margin:0;display:table;float:left;list-style-type:none}
    .ht .ht_act-co li{float:left;padding:0 10px;border-right:1px solid #ddd}
    .ht .ht_act-co li:first-child{border-left:1px solid #ddd}
    .ht .ht_act-co li span{padding-left:10px}
    .ht .ht_act-user{display:table;float:right}
    .ht .ht_act-user span{padding-left:10px}
    .ht .ht_act-user .ht-item{color:#656565;padding:0 10px;border-left:1px solid #ddd}
    .ht .ht_act-user .ht-item:last-child{border-right:1px solid #ddd}
    .hb{background-color:#269ffd;width:100%}

    .hb-wrap{display:table;padding-top:0;padding-bottom:0;width:80%;margin:0 auto}
    .hb-wrap .hb_logo{margin:5px 0;height:60px;float:left;width:100%;max-width:220px}
    .hb-wrap .hb_brand{background-size:contain;background-position:left;height:60px;padding:0;width:100%;overflow:hidden;background-repeat:no-repeat;display:block}
    .hb-wrap li{display:block;position:relative;text-transform:uppercase;font-weight:600;text-decoration:none;font-size:13px}
    .hb-wrap li a{padding:25.5px 13px;display:block}
    .hb-wrap li:hover{background-color:#48aefd}
    .hb-wrap a{color:#fff}
    .hb-wrap a:hover{color:#fff}

    .hb-wrap ul{margin:0;padding:0;float:right;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-webkit-justify-content:flex-end;-ms-flex-pack:inherit;justify-content:flex-end;text-align:left}
    .hb-wrap ul .sort-1{-webkit-box-ordinal-group:1;-webkit-order:1;-ms-flex-order:1;order:1}
    .hb-wrap ul .sort-2{-webkit-box-ordinal-group:2;-webkit-order:2;-ms-flex-order:2;order:2}
    .hb-wrap ul .sort-3{-webkit-box-ordinal-group:3;-webkit-order:3;-ms-flex-order:3;order:3}
    .hb-wrap .only-m{display:none}
    .hb-wrap .icon{display:none}
    .hb .hb_act-co{display:none}
    .hb .bar1,.bar2,.bar3{width:25px;height:3px;background-color:#fff;margin:6px 0;transition:.4s}
    .hb-wrap .dropdown-menu{display:none}
    .hb-wrap li.sort-3:hover{background-color:unset}
    .navbar-search{padding:17px 0 17px 17px}
    input[type="search"]{width:100%;height:36px;border-radius:25px!important;-webkit-border-radius:25px!important;border:0;box-sizing:border-box;-webkit-box-sizing:border-box;font-weight:400;font-size:12px;color:#000;padding-left:12px;background-color:#fff;padding-right:40px}
    .search-button{position:absolute;top:23px;right:4px;cursor:pointer;z-index:3;width:25px;height:25px;font-size:14px;background:#ff5400;color:#fff;border-radius:50px;border:none}

    @media (min-width: 1400px) {
        .ht-wrap{max-width:1140px}
        .hb-wrap{max-width:1140px;padding:0}
    }
    @media (max-width: 1399px) {
        .ht-wrap{width:100%;padding:5px 20px}
        .hb-wrap{width:100%;padding:0 20px}
    }
    @media screen and (max-width: 1200px) {
        .hb-wrap .hb_logo{max-width:220px}
    }
    @media screen and (min-width: 993px) and (max-width: 1200px) {
        .hb-wrap ul .sort-1{-webkit-box-ordinal-group:3;-webkit-order:3;-ms-flex-order:3;order:3}
        .hb-wrap ul .sort-3{display:none}
        .hb-wrap .only-m{display:block}
    }
    @media screen and (max-width: 992px) {
        .hb .resp .hb_act-co{display:block;width:100%;color:#fff;padding:10px 0 0;margin:10px 0;border-top:1px solid #fff}
        .hb .resp .hb_act-co li{text-transform:none;padding:8px 13px}
        .hb .resp .hb_act-co span{padding-left:10px;font-weight:400;font-size:14px}
        .ht{display:none}
        .ht-wrap{width:100%;padding:5px 0}
        .hb-wrap ul{padding:20px 0}
        .ht .ht_act-co li:first-child{border-left:0}
        .ht .ht_act-user .ht-item:last-child{border-right:0}
        .hb-wrap .hb_logo{max-width:220px}
        .hb-wrap li{margin:0;display:table;width:100%}
        .hb-wrap li a{padding:15px 13px}
        .hb-wrap .hb_nav{display:none;width:100%}
        .hb-wrap .icon{float:right;display:block;padding:18px 0;display:table}
        .hb-wrap .icon a{color:#fff}
        .navbar-search {padding: 17px 13px 0;}
        .search-button{right: 20px}
    }
    @media screen and (max-width: 778px) {
        .ht .ht_act-user{display:none}
        .hb-wrap{padding:0 10px}
    }
</style>
<style>
    .i80{width:80%;margin:0 auto 40px;overflow: hidden;}
    @media screen and (min-width: 1400px) {  .i80{max-width:1140px}  }
    @media screen and (max-width: 1399px) {  .i80{width: 100%;padding: 0 20px}  }
    @media screen and (max-width: 992px) {  .i80{padding: 0 10px}  }
    @media screen and (max-width: 490px) {  .i80{padding: 0 5px}  }
</style>
<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="/forum/">Форум</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="/forum/{{$subcategory->category->slug or ''}}">{{$subcategory->category->title or ''}}</a></li>
            <li class="breadcrumb-item">{{$subcategory->title or ''}}</li>
        </ol>
    </nav>
</section>

<style>
    .p_f_v{width:80%;margin:0 auto 40px;padding:0}
    .p_f_v .title h1{font-size:18px;margin-bottom:15px;line-height:18px;padding-left:10px;margin-top:0}
    .p_f_v .pagination .page-item .page-link{padding:5px 10px;font-size:14px}
    @media (min-width: 1400px) {
        .p_f_v{width:1140px}
    }
</style>

<section class="p_f_v">
    <div class="title" style="position: relative;">
        <h1>{{$subcategory->title or 'Название темы'}} <span class="appeal-clk" onclick="openbox('appeal-4'); return false" style="float: right;font-size: 12px;font-weight: 400">Режим Бога</span></h1>
        <div class="date">
            <div id="appeal-4" class="appeal" style="display: none;z-index: 2">
                <ul>
                    <li><a class="dropdown-item" href="{{route('admin.forum.subcategoryEdit')}}?id={{$messages[0]->subcategory->id}}">Редактировать тему</a></li>
                    <li><a class="dropdown-item" href="{{route('admin.forum.subcategory.delete')}}?id={{$messages[0]->subcategory->id}}">Удалить тему</a></li>
                    <li><a class="dropdown-item" href="{{route('admin.forum.subcategory.archive')}}?id={{$messages[0]->subcategory->id}}">В архив</a></li>
                </ul>
            </div>
        </div>
    </div>
    <style>
        .inner{width:100%;display:flex;border:1px solid #ddd;background:#fff;border-top:2px solid #fd8e75;margin-bottom:10px}
        .inner .left{width:15%;padding:15px;max-width:150px;border-right:1px solid #ddd;float:left;text-align:center;display:inline-block}
        .inner .left .image{margin-bottom:5px}
        .inner .left img{max-width:100%;width:80px;height:80px;object-fit:cover;border-radius:50%}
        .inner .left .name{font-size:14px}
        .inner .right{width:100%;float:left;display:inline-block}
        .inner .right .date{padding:10px 15px;border-bottom:1px solid #ddd;font-size:12px;position:relative}
        .inner .right .description{padding:10px 15px;line-height:18px;font-size:15px}
        .appeal-clk{color:#007bff;cursor:pointer}
        .appeal{position:absolute;right:0;top:30px;text-align: right;z-index: 1;padding:5px 15px;background:#fff;border:1px solid #e6e6e6}
        .appeal ul{padding:0;margin:0;list-style-type:none}
        .appeal li{padding:3px 0;font-size:13px;}
    </style>
    <?php $i = 0; ?>
    @forelse($messages as $message)
    @if($subcategory !== 3)
    <?php //dd($message); ?>
    <?php
    $created = \carbon\Carbon::createFromFormat('Y-m-d H:i:s', $message->created_at)->addHours(5)->toDateTimeString();
    $created = date("d.m.Y в H:i", strtotime($created));
    //dd($message);
    ?>
        <div class="inner" @if(Auth::user() !== null && Auth::user()->userRolle == 3) id="inner{{$message->id}}" @endif>
            <div class="left"><div class="image"><img src="{{asset('storage/app')}}/{{Resize::ResizeImage($message->user, '80x80')}}" alt=""></div><div class="name">{{$message->user->name or ''}}</div></div>
            @if(Auth::user() !== null && Auth::user()->userRolle == 3)
            <div class="right">
            <div class="date">{{$created}} <span class="appeal-clk" onclick="openbox('appeal-{{$message->id}}'); return false" style="float: right">Режим царя</span>
                <div id="appeal-{{$message->id}}" class="appeal" style="display: none;">
                    <ul>
                        <li><a class="dropdown-item" href="#" id="del{{$message->id}}">Сжечь рукопись</a></li>
                        <li><a class="dropdown-item" href="#" id="bann{{$message->id}}" >Силой, данной мне сайтом, изгнать в ЧС</a></li>
                    </ul>
                </div>
            </div>
            <div class="description"><p>Через CSS задайте этому блоку display: none;, а при нажатии на кнопку «Открыть» меняйте это свойство на block. Пробуйте, не получится, выкладывайте код, подскажем, поможем найти ошибку…</p></div>
        </div>
        <script type="text/javascript">
          $('#del{{$message->id}}').click(function(){
            $('#inner{{$message->id}}').remove();
            $.get('{{route('admin.forum.message.delete')}}?id={{$message->id}}');
          });

          $('#bann{{$message->id}}').click(function(){
            $('#inner{{$message->id}}').remove();
            $.get('{{route('admin.forum.user.bann')}}?id={{$message->id}}&&user={{$message->user->id or ''}}');
          });
        </script>
            @else
            <div class="right">
                <div class="date">{{$created}}<span class="appeal-clk" onclick="openbox('appeal-{{$message->id}}'); return false" style="float: right">Пожаловаться</span>
                    <div id="appeal-{{$message->id}}" class="appeal" style="display: none;">
                        <ul>
                            <li><a class="dropdown-item" href="/forum/report?message={{$message->id}}&&type=1">Оскарбление/Провакация</a></li>
                            <li><a class="dropdown-item" href="/forum/report?message={{$message->id}}&&type=2">Не соответствует теме</a></li>
                            <li><a class="dropdown-item" href="/forum/report?message={{$message->id}}&&type=3">Спам</a></li>
                            <li><a class="dropdown-item" href="/forum/report?message={{$message->id}}&&type=4">Экстримизм/Насилие</a></li>
                        </ul>
                    </div>
                </div>
                <div class="description">{!! $message->text !!}</div>
            </div>
            @endif

        </div>
        <?php $i++; ?>
    @endif
    @empty
    @endforelse

    <style>
        .add_inner{width:100%;display:table;margin-bottom:20px}
        .tinner{display:table;width:100%}
        .add_inner .left{float:left}
        .add_inner .right{float:right}
        .add_inner .button_inner{cursor:pointer;z-index:3;width:143px;padding:6px 15px;margin-left:25px;font-size:14px;background:#0095eb;color:#fff;border-radius:5px;text-align:center}
        .add_inner .button_inner a{color:#fff}
        .inner textarea{width:100%;margin-top:5px;border:1px solid #d5d5d5}
        .inner .add_button_inner{cursor:pointer;z-index:3;width:102px;height:30px;padding:4px 14px;margin-left:15px;margin-bottom:15px;font-size:14px;background:#0095eb;color:#fff;border-radius:5px;text-align:center}
        .inner .add_button_inner a{color:#fff}
        .btn-add{cursor:pointer;z-index:3;width:110px;padding:8px 15px;margin-left:15px;margin-bottom:15px;font-size:13px;background:#0095eb;color:#fff;border-radius:5px;text-align:center;border:0}
    </style>


        <div class="add_inner">
            <div class="tinner">
                <div class="left">
                    <style>
                        .pagination{margin-top:0;margin-bottom:0;display:-webkit-box;display:-ms-flexbox;display:flex;padding-left:0;list-style:none;border-radius:.25rem}
                        .page-item.disabled .page-link{color:#6c757d;pointer-events:none;cursor:auto;background-color:#fff;border-color:#e6e6e6}
                        .page-item.active .page-link{z-index:1;color:#fff;background-color:#27a0fd!important;border-color:#007bff}
                        .page-link:not(:disabled):not(.disabled){cursor:pointer}
                        .pagination>li>a,.pagination>li>span{padding:6px 12px;color:#0095eb;font-size:15px}
                        .page-link{position:relative;display:block;padding:.5rem .75rem;margin-left:-1px;line-height:1.25;color:#007bff;background-color:#fff;border:1px solid #dee2e6}
                    </style>
                    @include('2019.layouts.paginator', ['paginate' => $messages])
                </div>
                <div class="right">
                    <div class="button_inner" onclick="openbox('box'); return false">Ответить</div>
                </div>
            </div>

            <div id="box" style="display: none;margin-top: 20px;">
                <div class="panel-collapse out collapse" style="">
                    <form class="" action="/forum/subcategory/{{$subcategory->id}}-{{$subcategory->slug}}/message" method="post">
                        <div class="inner">
                            {{ csrf_field() }}
                            <div class="left">
                                <div class="image">

                                </div>
                                <div class="name">Имя</div>
                            </div>
                            <div class="right">
                                <div class="description">
                                    <textarea name="text" id="" cols="" rows="6" required=""></textarea>
                                </div>
                                <button type="submit" class="btn-add">Отправить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            function openbox(id){
                display = document.getElementById(id).style.display;
                if(display=='none'){document.getElementById(id).style.display='block';}
                else{document.getElementById(id).style.display='none';}
            }
        </script>
</section>

<script>
    $(".spoiler-trigger").click(function() {
        $(this).parent().next().collapse('toggle');
    });
</script>
@if($bann !== null)
<script type="text/javascript">
  alert('Вы были забанены и больше не можете писать сообщния и создавать темы');
</script>
@endif
@if($report !== null)
<script type="text/javascript">
  alert('Жалоба успешно отправлена');
</script>
@endif
<style>
    .row {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-right: -15px;
        margin-left: -15px;
    }
</style>
