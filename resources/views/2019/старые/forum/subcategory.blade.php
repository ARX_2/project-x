
<section class="navigate_box">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="/forum">Фор33ум</a></li>
            <li class="breadcrumb-item active" aria-current="page">Добавить тему </li>
        </ol>
    </nav>
</section>

<section class="page_forum_view">
    <div class="title">
        <h1>Добавить тему на форума</h1>
    </div>

        <div class="inner">
            <div class="left">
                <div class="image">
                    <img src="http://masterica.loc/img/c1.jpg" alt="">
                </div>
                <div class="name">{{Auth::user()->name}}</div>
            </div>
            <div class="right">
              <form class="" action="{{route('account.forum.create')}}" method="post">
                {{ csrf_field() }}
                <div class="description">Выберите категорию:<br>
                    <select name="forumcategory" id="" required>
                      <option disabled>Выберите категорию</option>
                      @forelse($categories as $category)
                        <option value="{{$category->id}}">{{$category->title}}</option>
                      @empty
                        <option value="">Нет категорий</option>
                      @endforelse
                    </select>
                </div>
                <div class="description">
                    <input type="text" name="title" placeholder="Введите название темы">
                </div>
                <div class="description">
                    <textarea name="text" id="" cols="" rows="10" placeholder="Введите ваш вопрос"></textarea>
                </div>
                    <button type="submit" class="btn btn-primary" style="margin:12px;">Отправить</button>

                </form>
            </div>
        </div>







</section>

<script src="//code.jquery.com/jquery-2.1.1.min.js"></script>
<script>
    $(".spoiler-trigger").click(function() {
        $(this).parent().next().collapse('toggle');
    });
</script>
<style>
    .row {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-right: -15px;
        margin-left: -15px;
    }
</style>
