
<style>
    body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left}
    *,::after,::before{box-sizing:border-box}
    a{color:#007bff;text-decoration:none;background-color:transparent;-webkit-text-decoration-skip:objects}
    header{width:100%}

    .h_nav{width:100%}
    .ht{border-top:3px solid #269ffd;background-color:#fff;width:100%}
    .ht-wrap{overflow:hidden;width:80%;margin:0 auto;font-size:13px;color:#656565;padding:7px 0}
    .ht .ht_act-co{padding:0;margin:0;display:table;float:left;list-style-type:none}
    .ht .ht_act-co li{float:left;padding:0 10px;border-right:1px solid #ddd}
    .ht .ht_act-co li:first-child{border-left:1px solid #ddd}
    .ht .ht_act-co li span{padding-left:10px}
    .ht .ht_act-user{display:table;float:right}
    .ht .ht_act-user span{padding-left:10px}
    .ht .ht_act-user .ht-item{color:#656565;padding:0 10px;border-left:1px solid #ddd}
    .ht .ht_act-user .ht-item:last-child{border-right:1px solid #ddd}
    .hb{background-color:#269ffd;width:100%}

    .hb-wrap{display:table;padding-top:0;padding-bottom:0;width:80%;margin:0 auto}
    .hb-wrap .hb_logo{margin:5px 0;height:60px;float:left;width:100%;max-width:220px}
    .hb-wrap .hb_brand{background-size:contain;background-position:left;height:60px;padding:0;width:100%;overflow:hidden;background-repeat:no-repeat;display:block}
    .hb-wrap li{display:block;position:relative;text-transform:uppercase;font-weight:600;text-decoration:none;font-size:13px}
    .hb-wrap li a{padding:25.5px 13px;display:block}
    .hb-wrap li:hover{background-color:#48aefd}
    .hb-wrap a{color:#fff}
    .hb-wrap a:hover{color:#fff}

    .hb-wrap ul{margin:0;padding:0;float:right;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:end;-webkit-justify-content:flex-end;-ms-flex-pack:inherit;justify-content:flex-end;text-align:left}
    .hb-wrap ul .sort-1{-webkit-box-ordinal-group:1;-webkit-order:1;-ms-flex-order:1;order:1}
    .hb-wrap ul .sort-2{-webkit-box-ordinal-group:2;-webkit-order:2;-ms-flex-order:2;order:2}
    .hb-wrap ul .sort-3{-webkit-box-ordinal-group:3;-webkit-order:3;-ms-flex-order:3;order:3}
    .hb-wrap .only-m{display:none}
    .hb-wrap .icon{display:none}
    .hb .hb_act-co{display:none}
    .hb .bar1,.bar2,.bar3{width:25px;height:3px;background-color:#fff;margin:6px 0;transition:.4s}
    .hb-wrap .dropdown-menu{display:none}
    .hb-wrap li.sort-3:hover{background-color:unset}
    .navbar-search{padding:17px 0 17px 17px}
    input[type="search"]{width:100%;height:36px;border-radius:25px!important;-webkit-border-radius:25px!important;border:0;box-sizing:border-box;-webkit-box-sizing:border-box;font-weight:400;font-size:12px;color:#000;padding-left:12px;background-color:#fff;padding-right:40px}
    .search-button{position:absolute;top:23px;right:4px;cursor:pointer;z-index:3;width:25px;height:25px;font-size:14px;background:#ff5400;color:#fff;border-radius:50px;border:none}

    @media (min-width: 1400px) {
        .ht-wrap{max-width:1140px}
        .hb-wrap{max-width:1140px;padding:0}
    }
    @media (max-width: 1399px) {
        .ht-wrap{width:100%;padding:5px 20px}
        .hb-wrap{width:100%;padding:0 20px}
    }
    @media screen and (max-width: 1200px) {
        .hb-wrap .hb_logo{max-width:220px}
    }
    @media screen and (min-width: 993px) and (max-width: 1200px) {
        .hb-wrap ul .sort-1{-webkit-box-ordinal-group:3;-webkit-order:3;-ms-flex-order:3;order:3}
        .hb-wrap ul .sort-3{display:none}
        .hb-wrap .only-m{display:block}
    }
    @media screen and (max-width: 992px) {
        .hb .resp .hb_act-co{display:block;width:100%;color:#fff;padding:10px 0 0;margin:10px 0;border-top:1px solid #fff}
        .hb .resp .hb_act-co li{text-transform:none;padding:8px 13px}
        .hb .resp .hb_act-co span{padding-left:10px;font-weight:400;font-size:14px}
        .ht{display:none}
        .ht-wrap{width:100%;padding:5px 0}
        .hb-wrap ul{padding:20px 0}
        .ht .ht_act-co li:first-child{border-left:0}
        .ht .ht_act-user .ht-item:last-child{border-right:0}
        .hb-wrap .hb_logo{max-width:220px}
        .hb-wrap li{margin:0;display:table;width:100%}
        .hb-wrap li a{padding:15px 13px}
        .hb-wrap .hb_nav{display:none;width:100%}
        .hb-wrap .icon{float:right;display:block;padding:18px 0;display:table}
        .hb-wrap .icon a{color:#fff}
        .navbar-search {padding: 17px 13px 0;}
        .search-button{right: 20px}
    }
    @media screen and (max-width: 778px) {
        .ht .ht_act-user{display:none}
        .hb-wrap{padding:0 10px}
    }
</style>
<style>
    .i80{width:80%;margin:0 auto 40px;overflow: hidden;}
    @media screen and (min-width: 1400px) {  .i80{max-width:1140px}  }
    @media screen and (max-width: 1399px) {  .i80{width: 100%;padding: 0 20px}  }
    @media screen and (max-width: 992px) {  .i80{padding: 0 10px}  }
    @media screen and (max-width: 490px) {  .i80{padding: 0 5px}  }
</style>
<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="/forum/">Форум</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$Forumcategory->title or ''}}</li>
        </ol>
    </nav>
</section>

<style>
    .p_f thead{background:#242424}
    .p_f{width:80%;margin:0 auto 40px;padding:0}
    .p_f .title h1{font-size:18px;padding-left: 10px;margin-bottom:15px;line-height:18px;margin-top:0}

    .p_f .desc-wrapper{font-size:15px;line-height:15px;display:block}
    .p_f .desc-wrapper:before{font-family:FontAwesome;content:"\f111 ";vertical-align:middle;font-size:8px;color:#f60;padding-right:8px;line-height:8px}
    .p_f .node-1{width:50%;padding:5px 10px}
    .p_f .node-2{width:15%}
    .p_f .node-3{width:35%;text-align:right;padding:0 10px}
    .p_f .node-stats{display:flex;align-items:center;width:200px;min-width:170px;vertical-align:middle;text-align:center;padding:0}
    .p_f .node-stats>dl.pairs.pairs--rows{width:50%;float:left;margin:0;padding:0 4px;border-right:1px solid #e0e0e0}
    .p_f .node-stats>dl.pairs.pairs--rows:first-child{padding-left:0}
    .p_f .node-stats .pairs{line-height:1.5}
    .p_f .pairs.pairs--rows>dt{font-size:12px;color:#888}
    .p_f .pairs.pairs--rows>dd{font-size:14px;margin:0 auto}
    .p_f .node-stats>dl.pairs.pairs--rows:last-child{padding-right:0;border-right:0}
    .p_f .node-extra{display:flex;vertical-align:middle;width:230px;min-width:230px;padding:0;display:inline-flex;align-items:center;font-size:12px}
    .p_f .lastPostAv{float:left}
    .p_f .avatar.avatar--xs{width:40px;height:40px;font-size:19.2px}
    .p_f .node-extra a:not(:hover){color:#616161}
    .p_f .avatar{vertical-align:top;overflow:hidden;display:inline-flex;align-items:center;justify-content:center;line-height:1;border-radius:100%}
    .p_f .avatar img{object-fit:cover;width:100%;height:100%}
    .p_f .node-extra .uix_nodeExtra__rows{display:flex;padding-right:5px;flex-wrap:wrap;min-width:0;max-width:100%;flex-direction:column;width:100%}
    .p_f .node-extra-row{overflow:hidden;white-space:nowrap;word-wrap:normal;text-overflow:ellipsis;color:#878787;max-width:100%}
    .p_f .node-extra a:not(:hover){color:#616161}
    .node-extra-row .listInline{overflow:hidden;white-space:nowrap;word-wrap:normal;text-overflow:ellipsis}
    .p_f .listInline{list-style:none;margin:0;padding:0}
    .p_f .listInline>li{display:inline;margin:0;padding:0}
    .p_f .u-dt[title]{border:none;text-decoration:none}
    .p_f .pagination .page-item .page-link{padding:5px 10px;font-size:14px}
    .p_f .p_f_header{width:100%}
    .p_f .p_f_header .left{float:left;width:50%}
    .p_f .p_f_header .right{float:right;width:50%;text-align:right}
    .p_f .p_f_header .right .button_fodum_add{cursor:pointer;z-index:3;width:143px;padding:6px 15px;margin-left:25px;font-size:14px;background:#0095eb;color:#fff;border-radius:5px;text-align:center}
    @media (min-width: 1400px) {
        .p_f{width:1140px}
    }
    @media (max-width: 1380px) {
        .p_f{width:100%;padding:0 20px}
    }
    @media (max-width: 769px) {
        .p_f .node-3{display:none}
        .p_f{margin:20px auto;padding:0 10px}
        .p_f .node-1{width:70%}
    }
    @media (max-width: 480px) {
        .p_f .node-1{width:100%}
        .p_f .node-2{display:none}
    }
    .forum{display:table;width:100%}
    .forum table{background:#fff;margin-bottom:15px;width: 100%}
    .f_title{padding:10px;font-size:15px;line-height:15px}
    .f_title a{color:white}
</style>

<section class="p_f">
    <div class="p_f_header">
        <div class="left">
            <div class="title">
                <h1>{{$Forumcategory->title or ''}}</h1>
            </div>
        </div>
        <div class="right">
            @auth
            <a href="/forum/create/" class="button_fodum_add">Добавить</a>
            @else
            <a href="/account" class="button_fodum_add">Добавить</a>
            @endauth
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="forum">
        <table class="table">
            <thead><tr><th class="f_title"><a href="/forum/{{$Forumcategory->slug or ''}}">{{$Forumcategory->title or ''}}</a></th><th></th><th></th></tr></thead>

                        <tbody>

                          @forelse($subcategories as $subcategory)
                          @if($subcategory !== 3)
                          <?php //dd($subcategory); ?>
                          <tr>
                              <td class="node-1"><a href="/forum/{{$Forumcategory->slug}}/{{$subcategory->id}}-{{$subcategory->slug or ''}}"><span class="desc-wrapper">{{$subcategory->title}}</span></a></td>
                              <td class="node-2">
                                  <div class="node-stats">
                                      <dl class="pairs pairs--rows"><dt>Темы</dt><dd>3</dd></dl>
                                      <dl class="pairs pairs--rows"><dt>Сообщения</dt><dd>{{count($subcategory->messages)}}</dd></dl>
                                  </div>
                              </td>
                              <td class="node-3">
                                  <div class="node-extra">
                                      <div class="uix_nodeExtra__rows">
                                          <div class="node-extra-row"><a href="/forum/{{$Forumcategory->slug}}/{{$subcategory->id}}-{{$subcategory->slug or ''}}" class="node-extra-title" title="{{$subcategory->messages[0]->title or ''}}">{{$subcategory->title}}</a></div>
                                          <div class="node-extra-row">
                                              <ul class="listInline listInline--bullet">
                                                  <li class="node-extra-user"><a href="/forum/{{$Forumcategory->slug}}/{{$subcategory->id}}-{{$subcategory->slug or ''}}" class="username">{{$subcategory->messages[0]->user->name or 'Пользователь'}}</a></li>
                                                  <li><time class="node-extra-date u-dt">{{$subcategory->messages[0]->created_at or 'Дата публикации'}}</time></li>
                                              </ul>
                                          </div>
                                      </div>
                                      @if(count($subcategory->messages)> 0)
                                      <div class="lastPostAv">
                                          <a href="/forum/{{$Forumcategory->slug}}/{{$subcategory->id}}-{{$subcategory->slug or ''}}" class="avatar avatar--xs">
                                              <img src="{{asset('storage/app')}}/{{Resize::ResizeImage($subcategory->messages[0]->user, '32x32')}}" alt="" class="avatar-u1-s">
                                          </a>
                                      </div>
                                      @endif
                                  </div>
                              </td>
                          </tr>
                        @endif
                        @empty
                        @endforelse
                        </tbody>
                    </table>

            </div>
        </div>
    </div>
    <style>
        .pagination{margin-top:0;margin-bottom:40px;display:-webkit-box;display:-ms-flexbox;display:flex;padding-left:0;list-style:none;border-radius:.25rem}
        .page-item.disabled .page-link{color:#6c757d;pointer-events:none;cursor:auto;background-color:#fff;border-color:#e6e6e6}
        .page-item.active .page-link{z-index:1;color:#fff;background-color:#27a0fd!important;border-color:#007bff}
        .page-link:not(:disabled):not(.disabled){cursor:pointer}
        .pagination>li>a,.pagination>li>span{padding:6px 12px;color:#0095eb;font-size:15px}
        .page-link{position:relative;display:block;padding:.5rem .75rem;margin-left:-1px;line-height:1.25;color:#007bff;background-color:#fff;border:1px solid #dee2e6}
    </style>
    @include('2019.layouts.paginator', ['paginate' => $subcategories])





</section>
@if($bann !== null)
<script type="text/javascript">
  alert('Вы были забанены и больше не можете писать сообщния и создавать темы');
</script>
@endif
