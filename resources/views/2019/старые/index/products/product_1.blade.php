<section class="background grey" @if($siteID == 2)style="background-image: url(/public/site/img/back-u1.jpg);background-position: top;background-size: 100%;"@endif>
<div class="i80">
<div class="main_page">
<div class="main_title">
<h2>Популярные товары</h2>
</div>
<div class="i-g">
<div class="i-g_l i-g_l-md">
<?php $i = 1 ?>
<?php //dd($goods); ?>
@forelse($goods as $g)
<?php //dd($g); ?>
@if($g->subcategory !== null && $g->main == 1)
<?php //dd($g); ?>
@if($g->subcategory !== null && $g->subcategory->type !== null && $g->subcategory->type == 0  ||$g->subcategory->categor !== null && $g->subcategory->categor->type == 0 && $g->published == 1)
<?php $type = 'product'; ?>
@else
<?php $type = 'services'; ?>
@endif
<?php //dd($g);?>

<div class="item">
<a href="/{{$type or 'category'}}/{{$g->subcategory->categor->id or $g->subcategory->id}}-{{$g->subcategory->categor->slug or $g->subcategory->slug}}/{{$g->id}}-{{$g->slug or ''}}">
<div class="image"><div class="img_dark"></div><img id="ImageProduct{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '273x273')}}" alt=""></div>
<div class="info">
<div class="name"><span>{{$g->title or ''}}</span></div>
<div class="prices"><div class="minimum">Цена:</div><div class="price-content">{{$g->coast or 'Договорная'}} р.</div></div>
</div>
</a>
</div>
<?php $i++; ?>
@if($i > 8)
<?php break; ?>
@endif
@endif
@empty
@endforelse
</div>
</div>
</div>
</div>
</section>
