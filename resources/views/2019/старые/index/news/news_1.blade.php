<section class="background light">
<div class="i80" style="margin-bottom: 20px;">
<div class="main_page">
<div class="main_title">
<h2>Новости</h2>
</div>
<div class="news">
@forelse($actions as $action)
@if($action->main == 1)

<div class="item">
<a href="/news/{{$action->id or ''}}-{{$action->slug or ''}}">
<div class="image"><div class="img_dark"></div><img id="ImageAction{{$action->id}}" src="/storage/app/{{Resize::ResizeImage($action, '105x70')}}" alt=""></div>
<div class="info">
<div class="date"><span>{{Resize::date($action->created_at)}}</span></div>
<div class="name">{{$action->title or ''}}</div>
</div>
</a>
</div>
@endif
@empty
@endforelse
</div>
</div>
</div>
</section>
