<section class="background grey" @if($siteID == 2)style="background-image: url(/public/site/img/back-u1.jpg);background-position: bottom;background-size: 100%"@endif>
<div class="i80">
<div class="main_page">
<div class="main_title">
<h2>Наши работы</h2>
</div>
<div class="portfolio-i">
<?php $i = 1 ?>
@forelse($objects as $object)
@if($object->main == 1)
@isset($object->images[0])
<?php $image = $object->images[0]?>
@else
<?php $image = null?>
@endif
<?php //dd();?>
<div class="item">
<a href="/portfolio/{{$object->category->id or ''}}-{{$object->category->slug or ''}}/{{$object->id}}-{{$object->slug}}">
<div class="image"><div class="img_dark"></div><img id="ImageObject{{$object->id}}" src="/storage/app/{{Resize::ResizeImage($image, '271x203')}}" alt=""></div>
<div class="info">
<div class="name">{{$object->title or ''}}</div>
<div class="description">{{$object->text or ''}}</div>
<div class="date">Добавлено: {{Resize::date($object->created_at)}}</div>
</div>
</a>
</div>
<?php $i++; ?>
@if($i > 4)
<?php break;?>
@endif
@endif
@empty
@endforelse
</div>
</div>
</div>
</section>
