<section class="background light"><div class="i80"><div class="main_page"><div class="main_title"><h2>Каталог</h2></div><div class="i-c in-pic"><div class="item_list cat_block-sm">
<?php $i = 1; ?>
<?php $cc = 1; ?>
@forelse($categories as $c)
@if($c->main == 1)
@if($c->type == 0)
<?php $type = 'product'; ?>
@else
<?php $type = 'services'; ?>
@endif

<div class="item">
<div class="image">
<a href="/{{$type or 'category'}}/{{$c->id}}-{{$c->slug or ''}}" class="img_dark"></a>
<a href="/{{$type or 'category'}}/{{$c->id}}-{{$c->slug or ''}}"><img id="ImageCategory{{$c->id}}" src="/storage/app/{{Resize::ResizeImage($c->images, '370x246')}}" alt=""></a>
</div>
<div class="info">
<div class="name"><a href="/{{$type or 'category'}}/{{$c->id}}-{{$c->slug or ''}}"><span>{{$c->title or ''}}</span></a></div>
<div class="subcat">@forelse($c->subcategories as $sub)<a href="/{{$type or 'category'}}/{{$c->id}}-{{$c->slug or ''}}/{{$sub->id}}-{{$sub->slug or ''}}">{{$sub->title or ''}}</a>@empty @endforelse</div>
</div>
</div>
@if($cc > 6) <?php break; ?> @endif <?php $i++; ?> @endif @empty @endforelse
</div></div></div></div></section>