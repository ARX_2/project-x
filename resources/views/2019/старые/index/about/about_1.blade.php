<section class="background light">
<div class="i80-n-b">
<div class="main_page">
<div class="main_title">
<h2>{{$texts->title or ''}}</h2>
</div>
<div class="about_i">
<div class="about_i-description">
{!! $texts->text or '' !!}
</div>
</div>
</div>
</div>
</section>