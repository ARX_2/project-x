<section class="solo-tizers" @if($siteID == 2)style="background-image: url(/public/site/img/back-u1.jpg);background-position: center;"@endif>
<div class="tizers separately">
<div class="tizers_list">
@forelse($tizers as $tizer)
<div class="item">
<div class="image"><img src="/storage/app/{{Resize::ResizeImage($tizer, '45x45')}}" alt=""></div>
<div class="info">
<div class="name"><span>{{$tizer->title or ''}}</span></div>
<div class="description"><p>{{$tizer->description or ''}}</p></div>
</div>
</div>
@empty
@endforelse
</div>
</div>
</section>
