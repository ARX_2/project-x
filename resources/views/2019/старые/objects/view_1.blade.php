</div>
<div class="col-9  tz-gallery">
    <style>
        .c-o_v{display:table;overflow:hidden;width:100%;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
        .c-o_v a.lightbox{width:100%;height:100%;display:table}
        .c-o_v .image{width:100%;height:325px;position:relative;overflow:hidden;display:block}
        .c-o_v .image img{width:100%;height:100%;object-fit:contain;position:relative;z-index:1}
        .c-o_v .blur{width:102%;height:103%;margin:-5px -9px;object-fit:contain;background-position:left;position:absolute;background-repeat:no-repeat;background-size:cover;-webkit-filter:blur(5px);-moz-filter:blur(5px);filter:blur(5px);filter:url(/public/site/img/blur.svg#blur)}
        @media screen and (max-width: 480px) {
            .c-o_v .image {height: 66vw}
        }
    </style>
    <div class="c-o_v">
        <div class="image">
            <script type="text/javascript">$(document).ready(function () {$('#ImageGoods{{$object->id}}').attr('src', '/storage/app/{{Resize::ResizeImage($object->images[0], "485x325")}}');});</script>
            <a href="/storage/app/{{$object->image}}" class="lightbox" >
                <div class="blur" style="background-image: url(/storage/app/{{Resize::ResizeImage($object->images[0], "485x325")}})"></div>
                <img id="ImageGoods{{$object->id}}"  src="/storage/app/{{Resize::ResizeImage($object->images[0], "485x325")}}" alt="">
            </a>
        </div>
    </div>
    <div class="c" style="margin-bottom: 0">
        <div class="content_title"><h1>{{$object->title or ''}}</h1><div class="data">id объекта: 1001, опубликовано {{Resize::date($object->created_at)}}</div></div>
        <div class="description">{!! $object->text or '' !!}</div>
    </div>
    <div class="c-o">
                    <div class="c-o_l c-o_l-xl">
                      <?php $i = 0; ?>
                      @forelse($object->images as $image)
                        @if($i>0)

                        <div class="item">
                            <a href="/storage/app/{{$image->images or ''}}" class="lightbox">
                                <div class="image" style="background-image: url(/storage/app/{{$image->images or ''}});"><div class="img_dark"></div></div>
                            </a>
                        </div>
                        @endif
                        <?php $i++; ?>
                        @empty
                        @endforelse
                    </div>
                </div>
</div>
</div>
</div>
</section>
<link rel="stylesheet" href="/public/site/open-image/open-image.css">
<script src="/public/site/open-image/open-image.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>
