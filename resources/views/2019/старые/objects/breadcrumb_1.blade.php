<style>.b_nav{background:#f3f3f3}.b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}.b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}.b_nav li a{color:#999}.breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}.breadcrumb-item.active{color:#aaa}.breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}@media (min-width: 1400px){.b_nav .breadcrumb{max-width:1140px}}@media (max-width: 1399px){.b_nav .breadcrumb{width:100%;padding:13px 20px}}@media screen and (max-width: 992px){.b_nav .breadcrumb{padding:13px 10px}}</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            @if(!isset($object))
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Объекты</li>
            @else
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="/portfolio/">Объекты</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$object->title or ''}}</li>
            @endif
        </ol>
    </nav>
</section>
<style>
.ncm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
.ncm .title{font-weight:600;line-height:17px;font-size:17px;padding:15px;border-bottom:1px solid #e6e6e6}
.ncm ul{display:block;padding:0;margin:0}
.ncm .i{list-style:none;border-bottom:1px solid #e6e6e6;position:relative}
</style>
<section class="i80">
    <div class="page_product_2">
        <div class="row">
            <div class="col-3">
                <div class="ncm">
                    <div class="title">Категории</div>
                    <div class="menu-list">
                        <ul>
                            @forelse($categories as $category)
                            <li class="i @if($objects !== null && $objects[0]->parent_id == $category->id) active @endif"><a href="/portfolio/{{$category->id}}-{{$category->slug}}">{{$category->title}}</a></li>
                            @empty
                            @endforelse
                        </ul>
                    </div>
                </div>
                <!-- <div class="documents">
                    <div class="title">Документы</div>
                    <div class="documents_list">
                        <div class="item">
                            <div class="file file-img"></div>
                            <div class="info">
                                <div class="name"><a href="">Выписка ЕГРЮЛ</a></div>
                                <div class="size">70 Кб, сегодня</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="file file-pdf"></div>
                            <div class="info">
                                <div class="name"><a href="">СРО</a></div>
                                <div class="size">121 Кб, сегодня</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="file file-excel"></div>
                            <div class="info">
                                <div class="name"><a href="">Реквизиты</a></div>
                                <div class="size">44 Кб, сегодня</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="file file-word"></div>
                            <div class="info">
                                <div class="name"><a href="">Название документа</a></div>
                                <div class="size">70 Кб, сегодня</div>
                            </div>
                        </div>
                    </div>
                </div> -->
