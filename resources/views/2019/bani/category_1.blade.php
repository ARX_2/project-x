<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/bani" itemprop="item"><span itemprop="name">Бани</span></a>
                <meta itemprop="position" content="2" />
            </li>
            @if($category->bread_cat == null)
                <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{$category->title}}</span>
                    <meta itemprop="position" content="3" />
                </li>
            @else
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/bani/{{$category->bread_cat->id}}-{{$category->bread_cat->slug}}" itemprop="item"><span itemprop="name">{{$category->bread_cat->title}}</span></a>
                    <meta itemprop="position" content="3" />
                </li>
                <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{$category->title}}</span>
                    <meta itemprop="position" content="4" />
                </li>
            @endif
        </ol>
    </nav>
</section>
<style>
    .nsm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}.nsm .title{padding:15px 15px 0;line-height:17px;font-size:17px;margin-bottom:10px;font-weight:600}.nsm ul{list-style:none;padding:0;margin:0}.nsm .parent_cat{position:relative;color:#666;font-weight:600;border-bottom:1px solid #e6e6e6;cursor:pointer}.nsm ul .parent_cat{padding:0 15px}.nsm ul .parent_cat{padding:0 15px}.nsm .parent_cat span{display:table-cell;padding:10px 20px 10px 0;width:100%;color:#007bff}</style>
<section class="i80">
    <div class="page_category_2">
        <div class="row">
            <div class="col-3">
                <div class="nsm">
                    <div class="title">
                        Категории
                    </div>
                    <div class="menu-list">
                        <ul class="menu-content">
                            @forelse($category_nav as $c)
                                <li class="cat-item @if(count($c->sub_category_nav)>0) collapse @else 111 @endif">
                                    <div class="parent_cat @if($c->id == $category->id) open_cat @else @endif" @if(count($c->sub_category_nav)>0)onclick="smFunction('sm-{{$c->id}}')" @else  @endif>
                                        @if(count($c->sub_category_nav) == 0)
                                            <a href="/bani/{{$c->id}}-{{$c->slug or ''}}"><span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or ''}}@endif</span></a>
                                        @else
                                            <span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or ''}}@endif</span>
                                        @endif
                                    </div>
                                    @if(count($c->sub_category_nav)>0)
                                        <ul class="sub-menu collapsed @if($c->id == $category_id || $c->open_cat == 1 || $category->parent_id == $c->id)active @endif" id="sm-{{$c->id}}">
                                            @forelse($c->sub_category_nav as $sub)
                                                <li class="sub_cat @if($sub->id == $category->id) open_cat @else @endif"><a href="/bani/{{$sub->id}}-{{$sub->slug or ''}}">@if($sub->seoname != null){{$sub->seoname}}@else{{$sub->title or ''}}@endif</a></li>
                                            @empty
                                            @endforelse
                                        </ul>
                                    @endif
                                </li>
                            @empty
                            @endforelse
                        </ul>
                    </div>
                </div>
                @if(count($documents) != null)
                    <div class="documents">
                        <div class="title">Документы</div>
                        <div class="documents_list">
                            @forelse($documents as $d)
                                <div class="item">
                                    <div class="file file-{{$d->type}}"></div>
                                    <div class="info">
                                        <div class="name"><a href="/storage/app/{{$d->doc}}/">{{$d->title}}</a></div>
                                        <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                @endif
            </div>
            <style>
                .c-c_l{width:100%;display:table;margin-top:10px}
                .c-c_l .i{float:left;width:32.333%;display:block;position:relative;margin:.5%}
                .c-c_l .i a{display:table;color:#fff;width:100%}
                .c-c_l .i .image{width:100%;height:16vw;width:100%;height:15vw;position:relative;overflow:hidden}
                .c-c_l .i .image img{width:100%;height:100%;object-fit:cover;-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-o-transition:all .3s ease;transition:all .3s ease}
                .c-c_l .i .dark{opacity:0;background-color:rgba(0,0,0,.2);-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=0);-webkit-transition:opacity .8s cubic-bezier(.19,1,.22,1);transition:opacity .8s cubic-bezier(.19,1,.22,1);position:absolute;display:block;width:100%;height:100%;z-index:2}
                .c-c_l .i:hover .dark{opacity:1}
                .c-c_l .i:hover img{-webkit-transform:scale(1.1);-ms-transform:scale(1.1);transform:scale(1.1)}
                .c-c_l .i .info{padding:0;position:absolute;bottom:10px;right:20px;left:20px;z-index:3}
                .c-c_l .i .name{margin-bottom:10px;width:100%;font-size:17px;line-height:18px;overflow:hidden;font-weight:600}
                .c-c_l .i .name span{background:#269ffd;padding:0 .5em .15em .1em;line-height:1.4}
                @media (min-width: 1400px) {
                    .c-c_l .i .image{height:176px}
                }
                @media screen and (max-width: 992px) {
                    .c-c_l .i .image{height:20.7vw}
                }
                @media screen and (max-width: 575px) {
                    .c-c_l .i{width:49%}
                    .c-c_l .i .image{height:31vw}
                }
                @media screen and (max-width: 480px) {
                    .c-c_l .i{width:99%}
                    .c-c_l .i .image{height:63vw}
                    .c-c_l .i .name{font-size:18px}
                }
            </style>
            <div class="col-9">
                @if($category->cover !== null)
                <div class="c" style="margin-bottom: 0">
                    <div class="cover" style="background-image: url('/storage/app/{{Resize::ResizeImage($category->cover, '847x200')}}')"></div>
                </div>
                @elseif($category->images !== null)
                <div class="c" style="margin-bottom: 0">
                    <div class="cover" style="background-image: url('/storage/app/{{Resize::ResizeImage($category->images, '847x200')}}')"></div>
                </div>
                @endif
                <div class="c">
                    <div class="content_title"><h1>{{$category->title}}</h1>@if(Auth::user() !== null)<div class="data"><a target="_blank" href="/admin/category/{{$category->id}}/edit">Редактировать</a></div>@endif</div>
                    @if($category->offer != null)<div class="description"><p>{{$category->offer}}</p></div>@endif
                    @if(count($sub_category) != null)
                        <div class="c-c_l">
                            @forelse($sub_category as $c)
                                <div class="i">
                                    <a href="/bani/{{$c->id}}-{{$c->slug}}">
                                        <div class="image">
                                            <div class="dark"></div>
                                            <img src="/storage/app/{{Resize::ResizeImage($c->images, '270x176')}}" alt="{{$c->title or 'Ошибка: alt'}}"></div>
                                        <div class="info">
                                            <div class="name"><span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or 'Ошибка: Название категории'}}@endif</span></div>
                                        </div>
                                    </a>
                                </div>
                            @empty
                            @endforelse
                        </div>
                    @endif
                </div>
                <div class="c grey" style="background: none">
                    <div class="content_title" style="background: #fff"><h2>Проекты</h2></div>
                    <div class="c-gd">
                        <div class="c-gd_l c-gd_l-xl">
                            @forelse($products_form_cat as $g)
                                <div class="item">
                                    <a href="/bani/{{$g->product_url_cat->id}}-{{$g->product_url_cat->slug}}/{{$g->id}}-{{$g->slug}}">
                                        <div class="image"><div class="img_dark"></div><img id="ImageGoods{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '400x270')}}" alt=""></div>
                                        <div class="info">
                                            <div class="name"><span>{{$g->title or ''}}</span></div>
                                            <div class="i_cat">{{$g->product_url_cat->title}}</div>
                                            <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                                            <div class="prices">@if($g->coast != null){{$g->coast or ''}} - @else Цена не указана @endif</div>
                                        </div>
                                        <div class="act">
                                            <div class="area">@if($g->goods_doma != null) {{$g->goods_doma->area}} @else ... @endif<span>м2</span></div>
                                            <div class="act_button">Смотреть планировку <i class="fa fa-angle-double-right" aria-hidden="true"></i></div>
                                        </div>
                                    </a>
                                </div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                </div>
                {{ $products_form_cat->links() }}
            </div>
        </div>
    </div>
</section>
<style>
    section.windows-2{background:#469f22 url(/public/site/img/bg-consult-doma-min.png) no-repeat right bottom;color:#fff;padding:62px 0;}
    .wrap{max-width:1170px;margin:auto;padding-left:15px;padding-right:15px}
    .consult-text{float:left;width:100%;max-width:815px}
    .line-1{margin-bottom:20px;line-height:20px;font-size:16px}
    .line-2{font-weight:700;font-size:24px;line-height:28px}
    .consult-order .button-arrow-or{margin-top:25px}
    .button-arrow-or{display:inline-block;vertical-align:top;padding:15px;padding-left:45px;font-weight:600;padding-right:20px;background:#eccc3c url(/public/site/img/arrow-cosult.png) no-repeat 15px 14px;font-size:13px;line-height:14px;text-transform:uppercase;position:relative;color:#2b2b2b!important}
    .wrap:after,.clr:after{content:"";display:block;clear:both}
    @media (max-width: 992px) {
        section.windows-2{background:#469f22}
    }
</style>
<section class="windows-2">
    <div class="wrap">
        <div class="consult-text">
            <div class="line-1">
                <p>Вам необходима консультация? <br>Произвести расчет или подобрать оптимальный вариант?</p>
            </div>
            <div class="line-2">
                <p>Наш менеджер с радостью поможет вам с этим!</p>
            </div>
        </div>
        <div class="consult-order">
            <a href="#form-popUp-3" class="button-arrow-or open-form" data-name="Консультация эксперта">Отправить заявку</a>
        </div>
    </div>
</section>
<style>
    .windows-3{padding:40px 20px;font-size:16px;line-height:26px}
    .cc{display:table;width:100%;padding:5px;margin:30px auto}
    .cc h2{font-size:22px;line-height:22px;font-weight:700;margin:0;margin-bottom:20px}
    @media (min-width: 1400px) {
        .windows-3 .cc{width:1140px}
    }
</style>
@if($category->description != null)
    <section class="windows-3"><div class="cc"><div class="description">{!! $category->description !!}</div></div></section>
@endif
<style>
    .windows-4{padding:40px 20px;background:#fff;font-size:16px;line-height:26px}
    @media (min-width: 1400px) {
        .windows-4 .cc{width:1140px}
    }
    @media (max-width: 778px) {
        .windows-4{padding: 20px 5px;}
        .cc{margin: 20px auto;}
    }
</style>
<section class="windows-4">
    <div class="cc">
        <style>
            .portfolio{display:table;margin:5px;width: 100%;}
            .portfolio .i{width:24.333%;float:left;margin:.333%}
            .portfolio .image{height:16vh;width:100%}
            .wc .portfolio .image{height:20vh;width:100%}
            .portfolio .i img{width:100%;height:100%;object-fit:cover}
            @media (min-width: 1400px) {
                .portfolio .image{height: 180px}
            }
            @media (max-width: 778px) {
                .portfolio .i{width:32.333%}
            }
            @media (max-width: 575px) {
                .portfolio .i{width:49%}
            }
        </style>
        <div class="content_title"><h2>Фото наших работ:</h2></div>
        <div class="portfolio">
            @forelse($portfolio as $p)
                <div class="i image_modal" slug="{{$p->id}}"><div class="image"><img src="/storage/app/{{Resize::ResizeImage($p->image_one, '275x180')}}" alt=""></div></div>
            @empty
            @endforelse
        </div>
    </div>
</section>
<script type="text/javascript">
    // .image - отвечает за класс на который надо нажимать, чтоб сработал ajax
    $('.image_modal').click(function(){
        var id = $(this).attr('slug');
        $.get('/portfolio/view/image/description/'+id, function(data){
            $('.modal_portfolio').html(data);
        });

        var url = '?object=' + id;
        history.pushState(null, null, url);

        @if($favicon->metrika_short != null)
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
        ym({{$favicon->metrika_short}}, 'hit', url);
        @endif

    });
</script>
<div class="modal_portfolio"></div>
<style>
    .windows-5{padding:40px 20px;font-size:16px;line-height:26px}
    @media (min-width: 1400px) {
        .windows-5 .cc{width:1140px}
    }
</style>
<section class="windows-5">
    <style>
        .reviews{min-width:100%;display:table}
        .reviews .i{padding:15px;background:#fff;border:1px solid #e6e6e6;width:100%;margin:.25em 0;display:inline-block}
        .reviews .i.open .reviews_button{display:none}
        .reviews .i .write_rev{display:none}
        .reviews .i.open .write_rev{display:block}
        .reviews .i.open{padding:15px!important;border:1px solid #e6e6e6!important}
        .rating{color:#fdcc63;font-size:12px;line-height:12px;margin-bottom:6px}
        .reviews .user{display:flex}
        .reviews .user .user_image{width:50px;height:50px;border-radius:50%;overflow:hidden;min-width:50px}
        .reviews .user img{width:100%;height:100%;object-fit:cover}
        .user_name{width:100%;padding-left:7px;margin:auto}
        .reviews .user .name{font-weight:600;font-size:15px;line-height:17px}
        .reviews .user .date{color:#888;font-size:14px}
        .i_desc{font-size:15px;line-height:22px;max-height:85px;position:relative;overflow:hidden}
        .toggle-btn{font-size:15px;line-height:17px;margin-top:10px;display:table}
        .text-open{overflow:visible;height:auto;max-height:none}
        .text-open + .toggle-btn{display:none}
        .i_gallery{display:table;width:100%;margin-top:10px}
        .i_gallery .image{width:31.333%;height:50px;margin:1%;float:left}
        .reviews .i_gallery img{width:100%;height:100%;object-fit:cover}
        @media (max-width: 778px) {
            .windows-5{padding: 20px 5px;}
            .i_gallery .image{height:7.5vh;width:25%}
        }
        @media (max-width: 575px) {

            .reviews .i{width:99%}
            .i_gallery .image{height:6.5vh;width:20%;margin:.5%}
        }
        .masonry{-moz-column-count:3;-webkit-column-count:3;column-count:3;-webkit-column-gap:.5em;-moz-column-gap:.5em;column-gap:.5em;padding:0;font-size:.85em}
        @media only screen and (max-width: 575px) {
            .masonry{-moz-column-count:1;-webkit-column-count:1;column-count:1}
        }
        @media only screen and (min-width: 576px) and (max-width: 768px) {
            .masonry{-moz-column-count:2;-webkit-column-count:2;column-count:2}
        }
        @media only screen and (min-width: 769px) {
            .masonry{-moz-column-count:3;-webkit-column-count:3;column-count:3}
        }
        .reviews_button{display:inline-block;vertical-align:top;padding:15px;font-size:13px;line-height:14px;width:100%;text-align:center;text-transform:uppercase;position:relative;color:#fdcc63!important;font-weight:600;border:1px solid #f7e8a8}
        .reviews_input{width:100%;border:1px solid #e6e6e6;padding:7px 15px;margin-bottom:5px}
        .reviews_input_file{margin-bottom:15px}
        .reviews_textarea{text-align:left;-webkit-box-sizing:border-box;box-sizing:border-box;overflow:hidden;border:1px solid #e6e6e6;height: 80px;padding:15px;margin-bottom: 10px;width:100%;resize:none;outline:none;font:14px/17px Helvetica,Arial,sans-serif}
        .reviews_textarea_button{position:relative;outline:none;border:none;padding:7px 20px;min-width:150px;min-height:32px;border-radius:3px;background-color:#007bab;font-size:14px;line-height:16px;cursor:pointer;color:#fff}
    </style>
    <div class="cc">
        <div class="content_title"><h2>Отзывы наших заказчиков:</h2></div>
        <div class="reviews ">
            <div class="masonry">
                <div class="i" style="padding: 0;border:none" id="ddd3">
                    <a href="javascript:void(0);" class="reviews_button" onclick="reviewsFunction()">Оставить отзыв</a>
                    <!--                    <div class="fdf" id="ddd3"></div>-->
                    <div class="write_rev">
                      <form action="/feedback/store/{{$category->id or ''}}" method="post" enctype="multipart/form-data">
                          {{csrf_field()}}
                      <input type="hidden" name="type" value="category">
                      <input type="text" name="name" class="reviews_input" placeholder="Имя Фамилия" value="">
                      <label for="file">Ваша фотография:</label>
                      <input type="file" name="ava" class="reviews_input_file" placeholder="Текст отзыва..." value="" style="border: 0;padding-left: 0;width: 100%">
                      <textarea name="description" id="" class="reviews_textarea" placeholder="Текст отзыва..."></textarea>
                      <div id="example3"></div>
                        <div class="submitReview">
                            <button class="reviews_textarea_button" style="background-color:#ddd" disabled>Отправить</button>
                        </div>
                      </form>
                    </div>
                </div>
                <script>
                    $(".toggle-btn").click(function(e) {e.preventDefault();$(this).prev().toggleClass("text-open")})
                </script>
                @forelse($reviews as $r)
                    <div class="i">
                        <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                        <div class="user">
                            <div class="user_image"><img src="/storage/app/{{Resize::ResizeImage($r, '45x45')}}" alt=""></div>
                            <div class="user_name">
                                <div class="name">{{$r->name}}</div>
                                <div class="date">Добавлено: <span>{{Resize::date($r->created_at)}}</span></div>
                            </div>
                        </div>
                        <div class="i_desc">{!! $r->description !!}</div>
                        <a href="" class="toggle-btn">Читать полностью</a>
                        @if($r->imageses != null)
                            <div class="i_gallery tz-gallery">
                                @forelse($r->imageses as $img)
                                    <div class="image">
                                        <a href="/storage/app/{{$img->images or ''}}" class="lightbox">
                                            <img src="/storage/app/{{Resize::ResizeImage($img, '75x50')}}" alt="">
                                        </a>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                        @endif
                    </div>
                @empty
                @endforelse
            </div>
        </div>
    </div>
</section>
<script>
    $(".toggle-btn").click(function(e) {e.preventDefault();$(this).prev().toggleClass("text-open")})
</script>
<style>
    .windows-6{padding:80px 20px 40px;font-size:16px;line-height:26px;color:#fff;background:url(/public/site/img/bg-klient-doma-min.jpg) no-repeat center top}
    .windows-6 ::placeholder{color:#fff;opacity:1}
    .windows-6 :-ms-input-placeholder{color:#fff}
    .windows-6 ::-ms-input-placeholder{color:#fff}
    @media (min-width: 1400px) {
        .windows-6 .cc{width:1140px}
    }
    .windows-6 .name-client-form{width:30%;float:left}
    .windows-6 .title-client-form{font-size:32px;line-height:34px;font-weight:700;margin-bottom:35px}
    .windows-6 .desc-client-form{color:#9b9c9d;line-height:20px}
    .windows-6 div.wpcf7 .screen-reader-response{position:absolute;overflow:hidden;clip:rect(1px,1px,1px,1px);height:1px;width:1px;margin:0;padding:0;border:0}
    .windows-6 .client-from-body{max-width:860px;width:70%;float:left}
    .windows-6 .inputs .input:first-child{margin-left:0}
    .windows-6 input,.windows-6 textarea,.windows-6 button{outline:none;border:none;background:transparent}
    .windows-6 .inputs .input{width:30%;float:left;margin-right:0;margin-left:25px;margin-bottom:25px}
    .windows-6 .input{margin-right:3%}
    .windows-6 .wpcf7-form-control-wrap{position:relative}
    .windows-6 .inputs .input input{width:210px}
    .windows-6 .input input{color:#fff;padding:15px 30px;width:100%;border-bottom:2px solid #fff}
    .windows-6 .inputs:after{content:"";display:block;clear:both}
    .windows-6 .textarea{margin-bottom:25px;margin-right:64px}
    .windows-6 .textarea textarea{color:#000;padding:15px 30px;width:100%;border:2px solid #fff;height:195px}
    .windows-6 .wpcf7-form-control-wrap{position:relative}
    section.windows-6 .textarea textarea{color:#fff}
    .windows-6 button,.windows-6 html input[type="button"],.windows-6 input[type="reset"],.windows-6 input[type="submit"]{-webkit-appearance:button;cursor:pointer}
    .windows-6 .button-project{display:inline-block;vertical-align:top;padding:15px;padding-left:60px;padding-right:20px;background:#48a022 url(/public/site/img/head-project.png) no-repeat 25px 10px;font-size:13px;line-height:14px;text-transform:uppercase;font-weight:600;position:relative;color:#fff!important}
    .windows-6 .cc:after,.windows-6 .clr:after{content:"";display:block;clear:both}
    @media (max-width: 778px) {
        .windows-6{background-size: cover;padding: 40px 20px 40px;}
        .windows-6 .name-client-form {
            width: 100%;
            text-align: center;    margin-bottom: 20px;
        }
        .windows-6 .title-client-form br {
            display: none;
        }
        .windows-6 .desc-client-form br{display: none;}
        .windows-6 .client-from-body{width: 100%;text-align: center}

        .windows-6 .inputs .input{margin-left: 0;width: 100%}
        .windows-6 .inputs .input input {
            width: 100%;
            text-align: center;
        }
        .windows-6 .textarea{margin-right: 0;margin-top: 10px;}
        section.windows-6 .textarea textarea{text-align: center}
    }
</style>
<section class="windows-6">
    <div class="cc">
        <div class="name-client-form">
            <div class="title-client-form">Заказать <br>строительство</div>
            <div class="desc-client-form">Заполните форму и наш <br>менеджер вам перезвонит</div>
        </div>
        <div role="form" class="wpcf7" id="wpcf7-f13614-p11936-o1" lang="ru-RU" dir="ltr">
            <div class="screen-reader-response"></div>
            <form action="/brevno/#wpcf7-f13614-p11936-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                <div class="client-from-body">
                    <div class="inputs">
                        <div class="input"><span class="wpcf7-form-control-wrap text-325"><input type="text" name="text-325" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Имя"></span></div>
                        <div class="input"><span class="wpcf7-form-control-wrap tel-440"><input type="tel" name="tel-440" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Телефон*"></span></div>
                        <div class="input"><span class="wpcf7-form-control-wrap email-484"><input type="email" name="email-484" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email" aria-invalid="false" placeholder="E-mail"></span></div>
                        <p></p></div>
                    <div class="textarea"><span class="wpcf7-form-control-wrap textarea-542"><textarea name="textarea-542" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Сообщение"></textarea></span></div>
                    <p><span id="hp5d1c50f34f8e2" class="wpcf7-form-control-wrap honeypot-531-wrap" style="display:none !important; visibility:hidden !important;"><label for="namesstyle" class="hp-message">Please leave this field empty.</label><input id="namesstyle" class="wpcf7-form-control wpcf7-text deds" type="text" name="honeypot-531" value="" size="40" tabindex="-1" autocomplete="nope"></span></p>
                    <div class="input-submit"><input type="submit" value="Отправить данные" class="wpcf7-form-control wpcf7-submit button-project"><span class="ajax-loader"></span></div>
                    <p></p></div>
                <div class="wpcf7-response-output wpcf7-display-none"></div></form></div>	</div>
</section>
<link rel="stylesheet" href="/public/site/open-image/open-image.css">
<script src="/public/site/open-image/open-image.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>
<script type="text/javascript">
      var verifyCallback = function(response) {
        $('.submitReview').html('<input type="submit" value="Отправить" class="reviews_textarea_button">');
      };
      var onloadCallback = function() {
        grecaptcha.render('example3', {
          'sitekey' : '{{Resize::recaptcha()}}',
          'callback' : verifyCallback,
        });
      };
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
