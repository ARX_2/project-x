<section class="background grey">
    <div class="i80">
        <div class="main_page">
            <div class="main_title">
                <h2>Популярные интерьеры</h2>
            </div>
            <style>
                .social{position:absolute;top:25px;left:0;z-index:101;font-size:14px}
                .like{background:#d02223;color:#fff;padding:2px 10px;display:table;cursor: pointer}
                .sale{background:#f7d700;color:#222;font-weight:600;display:table;padding:2px 10px;display:table}
                .like:before{content:"\f08a";font-family:FontAwesome;margin-right:6px}
                .like.active:before{content:"\f004";font-family:FontAwesome;margin-right:6px}
                .like:hover:before{content:"\f004";font-family:FontAwesome;margin-right:6px}
                .new{position:absolute;right:0;top:25px;background:#32a601;padding:2px 10px;color:#fff;font-weight:600;font-size:14px;z-index:101}
                .rur:after {    content: "\f158";
                    font-family: FontAwesome;
                    display: inline-block;
                    font-weight: 400;
                    font-style: normal;
                    font-size: 14px;
                    line-height: 1;
                    margin-left: 3px;}
                .newcoast{margin-left: 10px;}
                .oldcoast{    text-decoration: line-through;
                    color: #888;}
                .pogon_metr{font-weight: 400;
                    line-height: 12px;
                    font-size: 12px;
                    color: #888;
                    margin-left: 7px;}
            </style>
            <div class="c grey" style="background: none;">
                <div class="c-gd">
                    <div class="c-gd_l c-gd_l-sm">
                        @forelse($goods as $g)
                            <div class="item">
                                <div class="social">
                                    <div class="like"><span class="count">{{$g->like}}</span></div>
                                    @if($g->type_cost == 2 && $g->percent != null)<div class="sale">-{{$g->percent}}%</div>@endif
                                </div>
                                @if($g->new != null)<div class="new">Новинка</div>@endif
                                <a href="/izgotovleniye/{{$g->product_url_cat->id}}-{{$g->product_url_cat->slug}}/{{$g->id}}-{{$g->slug}}">
                                    <div class="image"><div class="img_dark"></div><img id="ImageGoods{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '400x270')}}" alt=""></div>
                                    <div class="info">
                                        <div class="name"><span>{{$g->title or ''}}</span></div>
                                        <div class="i_cat">@if($g->product_url_cat->seoname != null){{$g->product_url_cat->seoname}}@else{{$g->product_url_cat->title}}@endif</div>
                                        <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                                        <div class="prices">
                                            @if($g->type_cost == 0 || $g->coast == null || $g->coast == 0)
                                                @if($g->coast != null)<span class="rur">{{$g->coast or ''}}</span> @else Цена не указана @endif
                                            @elseif($g->type_cost == 1 && $g->coast != null)
                                                @if($g->coast != null && $g->new_cost == null)от <span class="rur">{{$g->coast or ''}}</span>
                                                @elseif($g->coast != null && $g->new_cost != null){{$g->coast or ''}} — <span class="rur">{{$g->new_cost or ''}}</span>
                                                @endif
                                            @elseif($g->type_cost == 2 && $g->coast != null)
                                                <span class="oldcoast">{{$g->coast or ''}}</span>
                                                <span class="rur newcoast">{{$g->new_cost or ''}}</span>
                                            @endif
                                            @if($g->unit == 1)<span class="pogon_metr">(за погонный метр)</span>@endif
                                        </div>
                                    </div>
                                    <div class="act">
                                        <div class="area">Заказать</div>
                                        <div class="act_button">Смотреть интерьер <i class="fa fa-angle-double-right" aria-hidden="true"></i></div>
                                    </div>
                                </a>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>