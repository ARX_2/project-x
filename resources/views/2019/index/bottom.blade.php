<section class="background light">
    <div class="i80" style="margin-bottom: 20px;">
        <div class="main_page">
            <div class="main_title">
                <h2>Новости</h2>
            </div>
            <div class="news">
                @forelse($news as $n)
                    <div class="item">
                        <a href="/news/{{$n->id}}-{{$n->slug}}">
                            <div class="image"><div class="img_dark"></div><img id="ImageAction{{$n->id}}" src="/storage/app/{{Resize::ResizeImage($n, '105x70')}}" alt=""></div>
                            <div class="info">
                                <div class="date"><span>{{Resize::date($n->created_at)}}</span></div>
                                <div class="name">{{$n->title or ''}}</div>
                            </div>
                        </a>
                    </div>
                @empty
                @endforelse
            </div>
        </div>
    </div>
</section>


@if(count($portfolio) != null)
    <section class="background grey" @if($siteID == 2)style="background-image: url(/public/site/img/back-u1.jpg);background-position: bottom;background-size: 100%"@endif>
        <div class="i80">
            <div class="main_page">
                <div class="main_title">
                    <h2>Наши работы</h2>
                </div>
                <div class="portfolio-i">
                    @forelse($portfolio as $p)
                        <div class="item image_modal" slug="{{$p->id}}">
                            <div class="image"><div class="img_dark"></div><img id="ImageObject{{$p->id}}" src="/storage/app/{{Resize::ResizeImage($p->image_one, '305x184')}}" alt=""></div>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        // .image - отвечает за класс на который надо нажимать, чтоб сработал ajax
        $('.image_modal').click(function(){
            var id = $(this).attr('slug');
            $.get('/portfolio/view/image/description/'+id, function(data){
                $('.modal_portfolio').html(data);
            });

            var url = '?object=' + id;
            history.pushState(null, null, url);

            @if($favicon->metrika_short != null)
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
            ym({{$favicon->metrika_short}}, 'hit', url);
            @endif
        });
    </script>
    <div class="modal_portfolio"></div>
@endif
<section class="background light">
    <div class="i80-n-b">
        <div class="main_page">
            <div class="main_title">
                <h2>{{$settings->title or 'Название главной страницы'}}</h2>@if(Auth::user() !== null)<div class="in_admin_edit"><a target="_blank" href="/admin/admin/company/upload?type=1">Редактировать</a></div>@endif
            </div>
            <div class="about_i">
                <div class="about_i-description">
                    {!! $settings->text or 'Описание на главной странице' !!}
                </div>
            </div>
        </div>
    </div>
</section>
<link rel="stylesheet" href="/public/site/open-image/open-image.css">
<script src="/public/site/open-image/open-image.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>
@if($siteID == 80)
    <script type='text/javascript'>
        (function () {
            window['yandexChatWidgetCallback'] = function() {
                try {
                    window.yandexChatWidget = new Ya.ChatWidget({
                        guid: '4559d84a-4cd2-4cd1-bffd-e3b4c7298d3f',
                        buttonText: 'Напишите нам, мы в сети!',
                        title: 'Чат',
                        theme: 'light',
                        collapsedDesktop: 'never',
                        collapsedTouch: 'always',
                        top: '300px',
                    });
                } catch(e) { }
            };
            var n = document.getElementsByTagName('script')[0],
                s = document.createElement('script');
            s.async = true;
            s.charset = 'UTF-8';
            s.src = 'https://chat.s3.yandex.net/widget.js';
            n.parentNode.insertBefore(s, n);
        })();
    </script>
@endif