
<style>.ban_2 h1{font-size:40px;font-family:"Exo 2",sans-serif;font-weight:600;text-align:left;margin:0}.ban_2 .title-ie{position:relative;padding:0 .2em .15em 0;line-height:1.4;background-color:rgba(38,159,253,0.8)}.ban_2 .d_flex{display:flex}.ban_2{background:#04091e;position:relative;z-index:1}.ban_2 .position{width:100%;bottom:0;left:0;position:absolute}.ban_2 .booking_table{position:relative;overflow:hidden;width:100%;min-height:30vw;height:calc(100vh - 230px)}.ban_2 .overlay{position:absolute;left:0;right:0;top:0;height:125%;bottom:0;z-index:-1}.ban_2{color:#fff}.ban_2 h6{font-size:14px;line-height:30px;text-transform:uppercase;letter-spacing:1.4px;font-weight:400}.ban_2 .ban_desc{font-weight:300;width:65%;text-align:left;font-size:14px;line-height:24px;margin:10px 0}.ban_2 .action{display:table;margin-top:20px}.ban_2 .action_button{background:rgba(38,159,253,0.8);border:1px solid #269ffd;color:#fff;display:table;padding:5px 15px;font-size:18px;text-align:center;border-radius:2px}.ban_2 .b-cover{width:100%;background-size:cover;background-color:#000;background-repeat:no-repeat;background-position:center center;text-align:center;vertical-align:middle;position:relative;background-attachment:fixed;overflow:hidden}.ban_2 .b-cover__carrier{height:100%;width:100%;background-size:cover;background-repeat:no-repeat;background-position:center center;text-align:center;vertical-align:middle;position:relative;background-attachment:fixed;transform:translate3d(0px,0px,0px)}.ban_2 .b-cover__carrier.loaded{opacity:1;transition:opacity 700ms ease 0}.ban_2 .parallax-overlay{top:0;left:0;width:100%;height:100%;background-repeat:repeat;position:absolute}.ban_2 .b-cover__filter{width:100%;height:100%;position:absolute;top:0;left:0}.ban_2.dark .b-cover__filter{background-image:-webkit-linear-gradient(top,rgba(33,33,33,0.2),rgba(33,33,33,0.95))}.ban_2.light .b-cover__filter{background-image:-webkit-linear-gradient(top,rgba(255,255,255,0.45),rgba(255,255,255,0.965))}.ban_2 .t_container{position:absolute;width:100%;height:100%;top:0}.i80b{width:80%;margin:0 auto}.tizers{display:none}.align-items-center{-webkit-box-align:center!important;-ms-flex-align:center!important;align-items:center!important}.solo-tizers{width:100%;background-color:#f3f3f3;padding:40px 0}@media (min-width: 1400px){.i80b{max-width:1140px}}@media (max-width: 1366px){.ban_2 .booking_table{height:calc(100vh - 115px)}.ban_2 .b-cover{background-attachment: inherit;}.i80b{width:95%;margin:0 auto}}@media (max-width: 1024px){.ban_2 h1{font-size:35px}.ban_2 .action_button{font-size:17px}.ban_2 .ban_desc{width:85%}}@media (max-width: 962px){.ban_2 h1{font-size:32px}.ban_2 .action_button{font-size:16px}}@media (max-width: 768px){.ban_2 h1{font-size:29px}.ban_2 h1 br{display:none}.tizers-no-mobile{display:none}}@media (max-width: 480px){.ban_2 h1{font-size:26px}.h_2 .navbar-brand{max-width:215px}.ban_2 .ban_desc{width:100%;max-height:89px;overflow:hidden}}@media (max-height: 480px){.ban_2 .ban_desc{display:none}}
</style>
@if($banner != null)
<section class="ban_2">
    <div class="booking_table b-cover" id="banner" style="background-image: url(/storage/app/{{Resize::ResizeImage($banner, '1520x755')}});">
        <div class="b-cover__carrier loaded" id="Imagebanner" style="background-attachment: scroll"></div>
    </div>
    <div class="parallax-overlay parallax-overlay-pattern"></div>
    <div class="b-cover__filter"></div>
    <div class="t_container d_flex align-items-center">
        <div class="i80b">
            <div class="banner_content text-center">
                <h1 class="text-left"><span class="title-ie">{{$banner->title or ''}}</span></h1>
                @if($banner->text != null)<div class="ban_desc mobile-none"><span>{{$banner->text or ''}}</span></div>@endif
                @if(count($url_request) != null)
                <div class="popular">Популярные запросы:
                @forelse($url_request as $ur)
                    @if($ur->link != null)
                    <a href="{{$ur->link}}" target="_blank">@if($ur->title != null){{$ur->title}}@else{{$ur->link}}@endif</a>
                    @endif
                @empty
                @endforelse
                </div>
                @endif
                @if($banner->url_link != null)
                <div class="action">
                    <a href="{{$banner->url_link}}">
                        <span class="action_button">@if($banner->name_link != null){{$banner->name_link}}@else{{$banner->url_link}}@endif</span>
                    </a>
                </div>
                @endif
            </div>
        </div>
    </div>
    @if($visual->menu == 2 || $visual->menu == 3)
    <div class="tizers">
        <div class="i80-n-b">
            <div class="tizers_list">
                @forelse($tizers as $t)
                <div class="item">
                    <div class="image"><img src="/storage/app/{{Resize::ResizeImage($t, '45x45')}}" alt=""></div>
                    <div class="info">
                        <div class="name"><span>{{$t->title or ''}}</span></div>
                        <div class="description"><p>{{$t->description or ''}}</p></div>
                    </div>
                </div>
                @empty
                @endforelse
            </div>
        </div>
    </div>
    @endif
</section>
@endif
@if($visual->menu == null || $visual->menu == 1)
<section class="solo-tizers" @if($siteID == 2)style="background-image: url(/public/site/img/back-u1.jpg);background-position: center;"@endif>
    <div class="tizers separately">
        <div class="tizers_list">
        @forelse($tizers as $t)
            <div class="item">
                <div class="image"><img src="/storage/app/{{Resize::ResizeImage($t, '45x45')}}" alt=""></div>
                <div class="info">
                    <div class="name"><span>{{$t->title or ''}}</span></div>
                    <div class="description"><p>{{$t->description or ''}}</p></div>
                </div>
            </div>
        @empty
        @endforelse
        </div>
    </div>
</section>
@endif
<section class="background light">
    <div class="i80">
        <div class="main_page">
            <div class="main_title">
                <h2>Каталог</h2>
            </div>
            <div class="i-c in-pic">
                <div class="item_list cat_block-sm">
                @forelse($category as $c)
                    <div class="item">
                        <div class="image">
                            <a href="/{{$c->section->slug or ''}}/{{$c->id}}-{{$c->slug}}" class="img_dark"></a>
                            <a href="/{{$c->section->slug or ''}}/{{$c->id}}-{{$c->slug}}">
                            <img id="ImageCategory{{$c->id}}" src="/storage/app/{{Resize::ResizeImage($c->images, '370x246')}}" alt=""></a>
                        </div>
                        <div class="info">
                            <div class="name"><a href="/{{$c->section->slug or ''}}/{{$c->id}}-{{$c->slug}}"><span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or ''}}@endif</span></a></div>
                            <div class="subcat">
                                @forelse($c->sub_category as $s)
                                    <a href="/{{$c->section->slug or ''}}/{{$s->id}}-{{$s->slug}}">@if($s->seoname != null){{$s->seoname}}@else{{$s->title or ''}}@endif</a>

                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                @empty
                @endforelse
                </div>
            </div>
        </div>
    </div>
</section>
