<section class="background grey" @if($siteID == 2)style="background-image: url(/public/site/img/back-u1.jpg);background-position: top;background-size: 100%;"@endif>
    <div class="i80">
        <div class="main_page">
            <div class="main_title">
                <h2>Популярные товары</h2>
            </div>
            <div class="i-g">
                <div class="i-g_l i-g_l-md">
                    @forelse($goods as $g)
                        <div class="item">
                            <a href="/{{$g->goods_category->section->slug or ''}}/{{$g->goods_category->id}}-{{$g->goods_category->slug}}/{{$g->id}}-{{$g->slug}}">
                                <div class="image"><div class="img_dark"></div><img id="ImageProduct{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '273x273')}}" alt=""></div>
                                <div class="info">
                                    <div class="name"><span>{{$g->title or 'Ошибка - название товара'}}</span></div>
                                    <div class="prices"><div class="minimum">Цена:</div><div class="price-content">@if($g->coast != null){{$g->coast or ''}} р. @else Цена не указана @endif</div></div>
                                </div>
                            </a>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>