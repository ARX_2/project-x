<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/portfolio/" itemprop="item"><span itemprop="name">Портфолио</span></a>
                <meta itemprop="position" content="2" />
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/portfolio/{{$category->id}}-{{$category->slug}}/" itemprop="item"><span itemprop="name">{{$category->title}}</span></a>
                <meta itemprop="position" content="3" />
            </li>
            <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <span itemprop="name">{{$portfolio->title}}</span>
                <meta itemprop="position" content="4" />
            </li>
        </ol>
    </nav>
</section>
<style>.nsm{background:#fff;font-size:14px;width:100%;padding:15px;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}.nsm .title{line-height:17px;font-size:17px;margin-bottom:10px;font-weight:600}.nsm ul{list-style:none;padding:0;margin:0}.nsm .parent_cat{position:relative;color:#666;font-weight:600;border-bottom:1px solid #e6e6e6;cursor:pointer}.nsm .parent_cat span{display:table-cell;padding:10px 20px 10px 0;width:100%;color:#007bff}</style>
<section class="i80">
    <div class="page_category_2">
        <div class="row">
            <div class="col-3">
                <style>
                    .ncm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
                    .ncm .title{font-weight:600;line-height:17px;font-size:17px;padding:15px;border-bottom:1px solid #e6e6e6}
                    .ncm ul{display:block;padding:0;margin:0}
                    .ncm .i{list-style:none;border-bottom:1px solid #e6e6e6;position:relative}
                </style>
                <div class="ncm">
                    <div class="title">Портфолио</div>
                    <div class="menu-list">
                        <ul>
                        @forelse($category_nav as $i)
                            <li class="i @if($category->id == $i->id) active @endif"><a href="/portfolio/{{$i->id}}-{{$i->slug}}">{{$i->title}}</a></li>
                        @empty
                        @endforelse
                        </ul>
                    </div>
                </div>
                @if(count($documents) != null)
                <div class="documents">
                    <div class="title">Документы</div>
                    <div class="documents_list">
                        @forelse($documents as $d)
                            <div class="item">
                                <div class="file file-{{$d->type}}"></div>
                                <div class="info">
                                    <div class="name"><a href="/storage/app/{{$d->doc}}/">{{$d->title}}</a></div>
                                    <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
                @endif
            </div>
            <div class="col-9 tz-gallery">
                <style>
                    .c-o_v{display:table;overflow:hidden;width:100%;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
                    .c-o_v a.lightbox{width:100%;height:100%;display:table}
                    .c-o_v .image{width:100%;height:325px;position:relative;overflow:hidden;display:block}
                    .c-o_v .image img{width:100%;height:100%;object-fit:contain;position:relative;z-index:1}
                    .c-o_v .blur{width:102%;height:103%;margin:-5px -9px;object-fit:contain;background-position:left;position:absolute;background-repeat:no-repeat;background-size:cover;-webkit-filter:blur(5px);-moz-filter:blur(5px);filter:blur(5px);filter:url(/frontend/img/blur.svg#blur)}
                    @media screen and (max-width: 480px) {
                        .c-o_v .image {height: 66vw}
                    }
                </style>
                <div class="c-o_v">
                    <div class="image">
                        <a href="@isset($portfolio->images[0]->images)/storage/app/{{$portfolio->images[0]->images}} @else /storage/app/public/no-image-170x170.jpg @endif" class="lightbox">
                            <div class="blur" style="background-image: url(@isset($portfolio->images[0]->images)/storage/app/{{Resize::ResizeImage($portfolio->images[0], "86x33")}} @else /storage/app/public/no-image-170x170.jpg @endif);"></div>
                            <img src="@isset($portfolio->images[0]->images)/storage/app/{{Resize::ResizeImage($portfolio->images[0], "600x330")}} @else /storage/app/public/no-image-170x170.jpg @endif" alt="">
                        </a>
                    </div>
                </div>
                <div class="c" style="margin-bottom: 0">
                    <div class="content_title"><h1>{{$portfolio->title}}</h1><div class="data">id портфолио: {{$portfolio->id}}, @if($portfolio->created_at != null) добавлено: {{$portfolio->created_at}} @else обновлено: {{$portfolio->updated_at}} @endif</div></div>
                    <div class="description">{!! $portfolio->text !!}
                    </div>
                </div>
                <div class="c-o">
                    <div class="c-o_l c-o_l-xl">
                        <?php $i = 0; ?>
                        @forelse($portfolio->images as $image)
                        @if($i>0)
                        <div class="item">
                            <a href="/storage/app/{{$image->images or ''}}" class="lightbox">
                                <div class="image" style="background-image: url(/storage/app/{{$image->images or ''}});"><div class="img_dark"></div></div>
                            </a>
                        </div>
                        @endif
                        <?php $i++; ?>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<link rel="stylesheet" href="https://rawgit.com/LeshikJanz/libraries/master/Bootstrap/baguetteBox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>