<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <span itemprop="name">Портфолио</span>
                <meta itemprop="position" content="2" />
            </li>
        </ol>
    </nav>
</section>
<style>.nsm{background:#fff;font-size:14px;width:100%;padding:15px;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}.nsm .title{line-height:17px;font-size:17px;margin-bottom:10px;font-weight:600}.nsm ul{list-style:none;padding:0;margin:0}.nsm .parent_cat{position:relative;color:#666;font-weight:600;border-bottom:1px solid #e6e6e6;cursor:pointer}.nsm .parent_cat span{display:table-cell;padding:10px 20px 10px 0;width:100%;color:#007bff}</style>
<section class="i80">
    <div class="page_category_2">
        <div class="row">
            <div class="col-3">
                <style>
                    .ncm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
                    .ncm .title{font-weight:600;line-height:17px;font-size:17px;padding:15px;border-bottom:1px solid #e6e6e6}
                    .ncm ul{display:block;padding:0;margin:0}
                    .ncm .i{list-style:none;border-bottom:1px solid #e6e6e6;position:relative}
                </style>
                <div class="ncm">
                    <div class="title">Портфолио</div>
                    <div class="menu-list">
                        <ul>
                        @forelse($category_nav as $i)
                        <li class="i"><a href="/portfolio/{{$i->id}}-{{$i->slug}}">@if($i->seoname != null){{$i->seoname}}@else{{$i->title or ''}}@endif</a></li>
                        @empty
                        @endforelse
                        </ul>
                    </div>
                </div>
                @if(count($documents) != null)
                <div class="documents">
                    <div class="title">Документы</div>
                    <div class="documents_list">
                        @forelse($documents as $d)
                            <div class="item">
                                <div class="file file-{{$d->type}}"></div>
                                <div class="info">
                                    <div class="name"><a href="/storage/app/{{$d->doc}}/" target="_blank">{{$d->title}}</a></div>
                                    <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
                @endif
            </div>
            <style>
                .c-c_l{width:100%;display:table;margin-top:10px}
                .c-c_l .i{float:left;width:32.333%;display:block;position:relative;margin:.5%}
                .c-c_l .i a{display:table;color:#fff;width:100%}
                .c-c_l .i .image{width:100%;height:16vw;width:100%;height:15vw;position:relative;overflow:hidden}
                .c-c_l .i .image:after{;background: url(https://vk.com/images/album_top_shadow.png)}
                .c-c_l .i .image img{width:100%;height:100%;object-fit:cover;-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-o-transition:all .3s ease;transition:all .3s ease}
                .c-c_l .i .dark{opacity:0;background-color:rgba(0,0,0,.2);-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=0);-webkit-transition:opacity .8s cubic-bezier(.19,1,.22,1);transition:opacity .8s cubic-bezier(.19,1,.22,1);position:absolute;display:block;width:100%;height:100%;z-index:2}
                .c-c_l .i:hover .dark{opacity:1}
                .c-c_l .i:hover img{-webkit-transform:scale(1.1);-ms-transform:scale(1.1);transform:scale(1.1)}
                .c-c_l .i .info-p{position: absolute;bottom: 0;width: 100%;left: 0;padding: 10px 20px;z-index: 3;background: url(https://vk.com/images/album_top_shadow.png);}
                .c-c_l .i .name-p{width:100%;font-size:15px;line-height:18px;overflow:hidden;font-weight:600;display: table;}
                .c-c_l .i .name-p span{line-height:1.4;background-color:inherit!important}
                .c-c_l .i .name-p .qty{float: right;}
                @media (min-width: 1400px) {
                    .c-c_l .i .image{height:176px}
                }
                @media screen and (max-width: 992px) {
                    .c-c_l .i .image{height:20.7vw}
                }
                @media screen and (max-width: 575px) {
                    .c-c_l .i{width:49%}
                    .c-c_l .i .image{height:31vw}
                }
                @media screen and (max-width: 480px) {
                    .c-c_l .i{width:99%}
                    .c-c_l .i .image{height:63vw}
                    .c-c_l .i .name-p{font-size:18px}
                }
            </style>
            <div class="col-9">
                @isset($settings->section_image_one)
                    <div class="c" style="margin-bottom: 0">
                        <div class="cover" style="height:200px;background-image: url('/storage/app/{{Resize::ResizeImage($settings->section_image_one, "840x200")}}');background-position: 50%;background-size: cover;margin: -5px;"></div>
                    </div>
                @endif
                <div class="c">
                    <div class="content_title"><h1>{{$settings->title or 'Наши работы'}}</h1>@if(Auth::user() !== null)<div class="in_admin_edit"><a target="_blank" href="/admin/admin/company/upload?type=6">Редактировать</a></div>@endif</div>
                    <div class="description"><p>{{$settings->offer}}</p></div>
                    <div class="c-c_l">
                        @forelse($category_nav as $i)

                        <div class="i">
                            <a href="/portfolio/{{$i->id}}-{{$i->slug}}">
                                <div class="image"><div class="dark"></div><img src="/storage/app/{{Resize::ResizeImage($i->images, '300x200')}}" alt=""></div>
                                <div class="info-p"><div class="name-p"><span>@if($i->seoname != null){{$i->seoname}}@else{{$i->title or ''}}@endif</span><span class="qty">25</span></div></div>
                            </a>
                        </div>
                        @empty
                        @endforelse
                    </div>
                </div>
                    <style>
                        .portfolio{display:table;width: 100%;}
                        .portfolio .i{width:24.333%;float:left;margin:.333%}
                        .portfolio .image{height:16vh;width:100%}
                        .portfolio .i img{width:100%;height:100%;object-fit:cover}
                        @media (max-width: 778px) {
                            .portfolio .i{width:32.333%}
                        }
                        @media (max-width: 575px) {
                            .portfolio .i{width:49%}
                        }
                    </style>
                    <div class="c">
                        <div class="content_title"><h2>Все фотографии:</h2></div>
                        <div class="portfolio">
                            @forelse($portfolio as $p)
                            @forelse($p->imageses as $img)
                            <div class="i image_modal" slug="{{$p->id}}"><img src="/storage/app/{{Resize::ResizeImage($img, '225x150')}}" alt="{{$img->images}}"></div>
                            @empty
                            @endforelse
                            @empty
                            @endforelse
                        </div>
                    </div>
                    {{ $portfolio->links() }}
                <div class="c">
                    <div class="description">{!! $settings->text !!}</div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };
    var object = getUrlParameter('object');
    if (object > 0) {
        var id = object;
        $.get('/portfolio/view/image/description/'+id, function(data){$('.modal_portfolio').html(data);});
    }
</script>
<script type="text/javascript">
// .image - отвечает за класс на который надо нажимать, чтоб сработал ajax
  $('.image_modal').click(function(){
    var id = $(this).attr('slug');
    $.get('/portfolio/view/image/description/'+id, function(data){
      $('.modal_portfolio').html(data);
    });
    
    var url = '?object=' + id;
    history.pushState(null, null, url);
    
    @if($favicon->metrika_short != null)
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
    ym({{$favicon->metrika_short}}, 'hit', url);
    @endif
  });
</script>
<div class="modal_portfolio"></div>

<link rel="stylesheet" href="/public/site/open-image/open-image.css">
<script src="/public/site/open-image/open-image.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>
