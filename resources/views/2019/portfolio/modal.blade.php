<style>
    .modal-portfolio.modal-ui-modal{width: 650px;}
    .ui-image{width:100%;height:50vh;position:relative;overflow:hidden;display:table;border:1px solid #e6e6e6}
    .ui-image img{width:100%;height:100%;object-fit:contain;position:relative;z-index:1}
    .ui-image .blur{width:100%;height:100%;position:absolute}
    .ui-image-g{width:100%;display:table;margin-top:20px}
    .masonry{-moz-column-count:2;-webkit-column-count:2;column-count:2;-webkit-column-gap:.5em;-moz-column-gap:.5em;column-gap:.5em;padding:0;font-size:.85em}
    .ui-image-g .i{width:100%;background:#fff;margin:.15em 0;display: table-header-group;}
    .ui-image-g .i img{width:100%;height:100%}
    .ui-i-img img{width:100%;height:100%;object-fit:cover}
    .ui-offer{display:flex;padding-top:20px;border-top:1px solid #ddd;width:100%;margin-top: 20px;}
    .ui-offer .image{width:150px;height:150px;background-color:#ddd;border-radius:50%;}
    .ui-offer .image img{width:100%;height:100%;object-fit:cover;border-radius:50%}
    .ui-p-portfolio{padding: 0;border-top:none;margin-top: 20px}
    .ui-user{display:inline-block}
    .ui-offer-text{display:inline-block;width:100%;padding-left:20px;margin:auto}
    .ui-offer-text h3{margin:0;font-size:18px}
    .ui-offer-text p{font-size:14px;font-size:15px;line-height:18px}
    .ui-offer-input{width:50%;border:1px solid #ddd;padding:9px 15px 8px;margin-bottom:5px}
    .ui-offer-button{display:inline-block;vertical-align:top;padding:11px;padding-left:42px;padding-right:22px;background:#32a601;background-size:cover;font-size:13px;line-height:13px;position:relative;color:#fff!important;border:none}
    .ui-offer-button:after{position:absolute;left:15px;top:50%;width:15px;height:15px;margin-top:-6px;content:"\f095";font-family:FontAwesome;font-size:14px;line-height:14px}
    .ui-user-name{display:table;text-align:center;width:100%;font-size:14px;margin-top:5px;color:#666;line-height:18px}
    .ui-user-name span{display:block;font-size:12px}
</style>

<div id="modal_form" class="modal-ui-wrapper" style="background: rgba(0, 0, 0, 0.565);">
    <div class="modal-portfolio modal-ui-modal" style="background: rgb(255, 255, 255);padding: 0;z-index: 10001">
        <div class="modal-ui-content tz-gallery" style="padding: 20px">
            <div class="title"><h2>{{$portfolio->title}}</h2>
                @if(Auth::user() !== null)<div class="data" style="font-size: 13px"><a target="_blank" href="/admin/object/{{$portfolio->id}}/edit?id={{$portfolio->id}}">Редактировать</a></div>@endif</div>
            <a class="lightbox" id="lightbox_main_a" href="/storage/app/{{$portfolio->imageses[0]->images}}">
                <div class="ui-image">
                    <div class="blur" style="background:#e6e6e6"></div>
                    <img id="lightbox_main_img" src="/storage/app/{{$portfolio->imageses[0]->images}}" alt="">
                </div>
            </a>
            <div class="ui-p-portfolio">
                {!! $portfolio->offer !!}
            </div>
            <div class="ui-image-g">
                <div class="masonry">
                <?php $i = 0 ?>
                  @forelse($portfolio->imageses as $image)
                  @if($i>0)
                  <div class="i"><a class="lightbox" href="/storage/app/{{$image->images}}"><img src="/storage/app/{{$image->images}}" alt=""></a></div>
                  @endif
                  <?php $i++; ?>
                  @empty
                  @endforelse
                </div>
            </div>
            @if($portfolio->text != null)
            <div class="ui-p-portfolio" style="border-top: 1px solid #ddd;padding-top: 20px;">
                {!! $portfolio->text !!}
            </div>
            @endif
            <div class="ui-offer">
                <div class="ui-user">
                    <div class="image"><img src="/storage/app/{{Resize::ResizeImage($manager, '150x150')}}" alt=""></div>
                    <div class="ui-user-name">{{$manager->name or ''}}<span>{{$manager->job or 'Бесплатный консультант'}}</span></div>
                </div>
                <div class="ui-offer-text">
                    <h3>Понравился проект?</h3>
                    <p>Оставьте свой номер телефона и мы вам перезвоним в рабочее время!</p>
                    <input type="text" class="ui-offer-input" placeholder="+7 (___) ___-__-__">
                    <button class="ui-offer-button">Перезвоните мне</button>
                </div>
            </div>
        </div>
        <div id="modal_close" class="modal-ui-close"><span>×</span></div>
    </div>
    <div id="overlay" class="overlay" style="position: absolute;width: 100%;height: 100%;z-index: 101;
"></div>
</div>
<script type="text/javascript">
        $('#modal_close, #overlay').click( function(){
            $('#modal_form')
                .animate({opacity: 0}, 100,
                    function(){
                        $(this).css('display', 'none');
                        $('#overlay').fadeOut(100);
                    }
                );
                history.replaceState({}, null, location.pathname);
        });
</script>
<script>baguetteBox.run('.tz-gallery');</script>
