<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/portfolio/" itemprop="item"><span itemprop="name">Портфолио</span></a>
                <meta itemprop="position" content="2" />
            </li>
            <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                @if($category->seoname != null)<span itemprop="name">{{$category->seoname}}</span>@else<span itemprop="name">{{$category->title or ''}}</span>@endif
                <meta itemprop="position" content="3" />
            </li>
        </ol>
    </nav>
</section>
<style>.nsm{background:#fff;font-size:14px;width:100%;padding:15px;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}.nsm .title{line-height:17px;font-size:17px;margin-bottom:10px;font-weight:600}.nsm ul{list-style:none;padding:0;margin:0}.nsm .parent_cat{position:relative;color:#666;font-weight:600;border-bottom:1px solid #e6e6e6;cursor:pointer}.nsm .parent_cat span{display:table-cell;padding:10px 20px 10px 0;width:100%;color:#007bff}</style>
<section class="i80">
    <div class="page_category_2">
        <div class="row">
            <div class="col-3">
                <style>
                    .ncm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
                    .ncm .title{font-weight:600;line-height:17px;font-size:17px;padding:15px;border-bottom:1px solid #e6e6e6}
                    .ncm ul{display:block;padding:0;margin:0}
                    .ncm .i{list-style:none;border-bottom:1px solid #e6e6e6;position:relative}
                </style>
                <div class="ncm">
                    <div class="title">Портфолио</div>
                    <div class="menu-list">
                        <ul>
                        @forelse($category_nav as $i)
                        <li class="i @if($category->id == $i->id) active @endif"><a href="/portfolio/{{$i->id}}-{{$i->slug}}">@if($i->seoname != null){{$i->seoname}}@else{{$i->title or ''}}@endif</a></li>
                        @empty
                        @endforelse
                        </ul>
                    </div>
                </div>
                @if(count($documents) != null)
                <div class="documents">
                    <div class="title">Документы</div>
                    <div class="documents_list">
                        @forelse($documents as $d)
                            <div class="item">
                                <div class="file file-{{$d->type}}"></div>
                                <div class="info">
                                    <div class="name"><a href="/storage/app/{{$d->doc}}/" target="_blank">{{$d->title}}</a></div>
                                    <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
                @endif
            </div>
            <style>
                .portfolio{display:table;width: 100%;}
                .portfolio .i{width:24.333%;float:left;margin:.333%}
                .portfolio .image{height:16vh;width:100%}
                .portfolio .i img{width:100%;height:100%;object-fit:cover}
                @media (max-width: 778px) {
                    .portfolio .i{width:32.333%}
                }
                @media (max-width: 575px) {
                    .portfolio .i{width:49%}
                }
            </style>
            <div class="col-9">
                <div class="c">
                    <div class="content_title"><h1>Наши работы: @if($category->seoname != null){{$category->seoname}}@else{{$category->title or ''}}@endif</h1></div>
                    <div class="portfolio">
                        @forelse($portfolio as $p)
                            @forelse($p->imageses as $img)
                                <div class="i image_modal" slug="{{$p->id}}"><img src="/storage/app/{{Resize::ResizeImage($img, '225x150')}}" alt="{{$img->images}}"></div>
                            @empty
                            @endforelse
                        @empty
                        @endforelse
                    </div>
                </div>
                {{ $portfolio->links() }}
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    // .image - отвечает за класс на который надо нажимать, чтоб сработал ajax
    $('.image_modal').click(function(){
        var id = $(this).attr('slug');
        $.get('/portfolio/view/image/description/'+id, function(data){
            $('.modal_portfolio').html(data);
        });

        var url = '?object=' + id;
        history.pushState(null, null, url);

        @if($favicon->metrika_short != null)
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
        ym({{$favicon->metrika_short}}, 'hit', url);
        @endif

    });
</script>
<div class="modal_portfolio"></div>
<link rel="stylesheet" href="/public/site/open-image/open-image.css">
<script src="/public/site/open-image/open-image.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>