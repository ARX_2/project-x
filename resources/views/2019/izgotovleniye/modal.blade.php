<div id="modal_form" class="modal-ui-wrapper" style="background: rgba(0, 0, 0, 0.565);">
    <form action="{{route('modal.send.izgotovleniye')}}" method="post">
        {{ csrf_field() }}
        <input type="text" name="form_name" value="Форма «Бесплатный вызов замерщика»" readonly>
    <div class="modal-ui-modal" style="background: rgb(255, 255, 255);">
        <div class="modal-ui-content">
            <div class="title"><h2>Отправка заказа</h2></div>
            <div class="ui-p">
                <div class="ui-b-form">
                    <div class="form-group">
                        <div class="label">E-mail:</div>
                        <div class="g-input"><input class="" type="text" name="email" placeholder="Ваш email"></div>
                    </div>
                    <div class="form-group">
                        <div class="label">Контактный телефон:</div>
                        <div class="g-input"><input type="text" name="phone" placeholder="+7 (900) 000-00-00"></div>
                    </div>
                    <div class="form-group">
                        <div class="label">Ваше сообщение:</div>
                        <div class="g-input"><textarea maxlength="500" name="description" placeholder="Оставьте комментарий к заказу или задайте интересующий вопрос" id="message"></textarea></div>
                    </div>
                   {{-- @if($settings != null)<input type="text" name="settings" value="{{$settings}}" readonly>@endif
                    @if($category != null)<input type="text" name="category" value="{{$category}}" readonly>@endif
                    @if($product != null)<input type="text" name="product" value="{{$product}}" readonly>@endif
                    @if($host != null)<input type="text" name="host" value="{{$host}}" readonly>@endif--}}
                </div>
            </div>
            <div class="ui-f"><button id="sendOrder">Подтвердить отправку</button></div>
        </div>
        <div id="modal_close" class="modal-ui-close"><span>×</span></div>
    </div>
    </form>
</div>
<div id="overlay"></div>

<style media="screen">
    .plus, .minus{
        cursor: default;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {
        $('#toorder').click(function (event) {
            event.preventDefault();
            $('#overlay').fadeIn(50,
                function () {
                    $('#modal_form')
                        .css('display', 'flex')
                        .animate({opacity: 1}, 50);
                });
        });

        $('#modal_close, #overlay').click(function () {
            $('#modal_form')
                .animate({opacity: 0}, 100,
                    function () {
                        $(this).css('display', 'none');
                        $('#overlay').fadeOut(100);
                    }
                );
        });
    });
</script>