<style>.b_nav{background:#f3f3f3}.b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}.b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}.b_nav li a{color:#999}.breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}.breadcrumb-item.active{color:#aaa}.breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}@media (min-width: 1400px){.b_nav .breadcrumb{max-width:1140px}}@media (max-width: 1399px){.b_nav .breadcrumb{width:100%;padding:13px 20px}}@media screen and (max-width: 992px){.b_nav .breadcrumb{padding:13px 10px}}</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">О компании</li>
        </ol>
    </nav>
</section>
<style>.ncm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}.ncm .title{font-weight:600;line-height:17px;font-size:17px;padding:15px;border-bottom:1px solid #e6e6e6}.ncm ul{display:block;padding:0;margin:0}.ncm .i{list-style:none;border-bottom:1px solid #e6e6e6;position:relative}</style>
<section class="i80">
    <div class="row">
        <div class="col-3">
            <div class="ncm">
                <div class="title">Компания</div>
                <div class="menu-list">
                    <ul>
                        <li class="i active"><a href="/about/">О компании</a></li>
                        @forelse($sections as $section)
                        <li class="i"><a href="/{{$section->slug or ''}}/">{{$section->title or ''}}</a></li>
                        @empty
                        @endforelse
                        <li class="i"><a href="/contact/">Контакты</a></li>
                    </ul>
                </div>
            </div>
            <div class="documents">
                <div class="title">Документы</div>
            </div>
        </div>
        <?php //dd($text !== null);?>
        <div class="col-9">
            <!-- @if($text !== null && count($text->images)>0)
            <div class="c" style="margin-bottom: 0"><div class="cover" style="height:200px;background-image: url('/storage/app/{{Resize::ResizeImage($text->images[0], '845x200')}}');background-position: 50%;background-size: cover;margin: -5px;"></div></div>
            @endif -->
            <div class="c" style="margin-bottom: 0;">
                <div class="content_title"><h1>{{$text->title or 'Название компании'}}</h1></div>
                <div class="description">{!! $text->text or '' !!}</div>
            </div>
        </div>
    </div>
</section>
