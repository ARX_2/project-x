</div>
<div class="col-9">
    <div class="c grey">
        <div class="content_title">Популярные проекты</div>
        <div class="c-o">
            <div class="c-o_l c-o_l-xl">
                @forelse($objects as $object)

                    @if($object->type == 2)
                <div class="item">
                    <a href="/portfolio/{{$object->category->slug or 'category'}}/{{$object->id or ''}}-{{$object->slug or ''}}">
                        <div class="image" style="background-image: url(/storage/app/{{Resize::ResizeImage($object->images[0], '265x176')}});"><div class="img_dark"></div></div>
                        <div class="info">
                            <div class="name"><span>{{$object->title or ''}}</span></div>
                            <div class="info_desc"><p>{{$object->text or ''}}</p></div>
                            <div class="info_date"><p>Добавлено: {{Resize::date($object->reated_at)}}</p></div>
                        </div>
                    </a>
                </div>
                    @endif
                @empty
                @endforelse
            </div>
        </div>
    </div>
    <div class="c grey">
        <div class="content_title">Наши работы</div>
        <div class="c-o">
            <div class="c-o_l c-o_l-xl">
                @forelse($objects as $object)
                    @if($object->type == 1)
                    <?php //dd($object); ?>
                <div class="item">
                    <a href="/portfolio/{{$object->category->slug or 'category'}}/{{$object->id or ''}}-{{$object->slug or ''}}">
                        <div class="image" style="background-image: url(/storage/app/{{Resize::ResizeImage($object->images[0], '265x176')}});"><div class="img_dark"></div></div>
                        <div class="info">
                            <div class="name"><span>{{$object->title or ''}}</span></div>
                            <div class="info_desc"><p>{{$object->text or ''}}</p></div>
                            <div class="info_date"><p>Добавлено: {{Resize::date($object->created_at)}}</p></div>
                        </div>
                    </a>
                </div>
                    @endif
                @empty
                @endforelse
            </div>
        </div>
    </div>
    <div class="c">
        <div class="content_title"><h1>{{$description->title or ''}}</h1></div>
        <div class="description"><p>{!! $description->text or '' !!}</div>
    </div>
</div>
</div>
</div>
</section>
