<style>.b_nav{background:#f3f3f3}.b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}.b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}.b_nav li a{color:#999}.breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}.breadcrumb-item.active{color:#aaa}.breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}@media (min-width: 1400px){.b_nav .breadcrumb{max-width:1140px}}@media (max-width: 1399px){.b_nav .breadcrumb{width:100%;padding:13px 20px}}@media screen and (max-width: 992px){.b_nav .breadcrumb{padding:13px 10px}}</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <?php //dd($categoryCache);?>
            @if($breadcrumb == 1)
               <?php //dd($category);?>
                @if($category !== null && $category->parent_id == null)

                 <li class="breadcrumb-item" ><a @if($category->type == 0)href="/product" @else href="/services" @endif>{{$section or $title}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$category->title or ''}}</li>
                @elseif($category !== null && $category->parent_id !== null)
                <li class="breadcrumb-item"><a @if($category->type == 0)href="/product" @else href="/services" @endif>{{$section or $title}}</a></li>
                <li class="breadcrumb-item"><a @if($category->type == 0)href="/product/{{$category->categor->id}}-{{$category->categor->slug}}" @else href="/services/{{$category->categor->id}}-{{$category->categor->slug}}" @endif>{{$category->categor->title or $title}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$category->title or ''}}</li>
                @else
                 <li class="breadcrumb-item active" aria-current="page">{{$section or $title}}</li>
                @endif
                @elseif($breadcrumb == 2)

                    <li class="breadcrumb-item"><a href="/category/{{$category->id or ''}}-{{$category->slug or ''}}">{{$category->title or ''}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$subcategory->title or ''}}</li>
                @else
                    @if($product->subcategory->categor !== null)
                    <li class="breadcrumb-item"><a href="/{{$type or 'category'}}/{{$product->subcategory->categor->id or ''}}-{{$product->subcategory->categor->slug or ''}}">{{$product->subcategory->categor->title or ''}}</a>
                    </li>
                    @endif
                    <li class="breadcrumb-item"><a href="/{{$type or 'category'}}/{{$product->subcategory->categor->id or ''}}-{{$product->subcategory->categor->slug or ''}}/{{$product->subcategory->id or ''}}-{{$product->subcategory->slug or ''}}">{{$product->subcategory->title or ''}}</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">{{$product->title or ''}}</li>
                @endif
        </ol>
    </nav>
</section>
