</div>
<?php //dd($content);?>
<div class="col-9">
    <style>
        .c-c_l{width:100%;display:table;margin-top:10px}
        .c-c_l .i{float:left;width:32.333%;display:block;position:relative;margin:.5%}
        .c-c_l .i a{display:table;color:#fff;width:100%}
        .c-c_l .i .image{width:100%;height:16vw;width:100%;height:15vw;position:relative;overflow:hidden}
        .c-c_l .i .image img{width:100%;height:100%;object-fit:cover;-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-o-transition:all .3s ease;transition:all .3s ease}
        .c-c_l .i .dark{opacity:0;background-color:rgba(0,0,0,.2);-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=0);-webkit-transition:opacity .8s cubic-bezier(.19,1,.22,1);transition:opacity .8s cubic-bezier(.19,1,.22,1);position:absolute;display:block;width:100%;height:100%;z-index:2}
        .c-c_l .i:hover .dark{opacity:1}
        .c-c_l .i:hover img{-webkit-transform:scale(1.1);-ms-transform:scale(1.1);transform:scale(1.1)}
        .c-c_l .i .info{padding:0;position:absolute;bottom:10px;right:20px;left:20px;z-index:3}
        .c-c_l .i .name{margin-bottom:10px;width:100%;font-size:17px;line-height:18px;overflow:hidden;font-weight:600}
        .c-c_l .i .name span{background:#269ffd;padding:0 .5em .15em .1em;line-height:1.4}
        @media (min-width: 1400px) {
            .c-c_l .i .image{height:176px}
        }
        @media screen and (max-width: 992px) {
            .c-c_l .i .image{height:20.7vw}
        }
        @media screen and (max-width: 575px) {
            .c-c_l .i{width:49%}
            .c-c_l .i .image{height:31vw}
        }
        @media screen and (max-width: 480px) {
            .c-c_l .i{width:99%}
            .c-c_l .i .image{height:63vw}
            .c-c_l .i .name{font-size:18px}
        }
    </style>
    
    <?php //dd($subcategoryCache->images !== null);?>
    @if($content == 2 && $categoryCache->images !== null)
    <?php //dd($categoryCache->images); ?>
        <div class="c" style="margin-bottom: 0">
            <div class="cover" style="height:200px;background-image: url('/storage/app/{{Resize::ResizeImage($categoryCache->images, '847x200')}}');background-position: 50%;background-size: cover;margin: -5px;"></div>
        </div>
    @elseif($content == null)
    @if($subcategoryCache->images !== null)
    <div class="c" style="margin-bottom: 0">
        <div class="cover" style="height:200px;background-image: url('/storage/app/{{Resize::ResizeImage($subcategoryCache->images, '847x200')}}');background-position: 50%;background-size: cover;margin: -5px;"></div>
    </div>
    @endif
    @else
    @if(count($categories)>0 && $categories[0]->images !== null)
    <div class="c" style="margin-bottom: 0">
        <div class="cover" style="height:200px;background-image: url('/storage/app/{{Resize::ResizeImage($categories[0]->images, '847x200')}}');background-position: 50%;background-size: cover;margin: -5px;"></div>
    </div>
    @endif
    @endif

        @if($content == null)
            <div class="c">
                <div class="content_title"><h1>{{$subcategoryCache->title or ''}}</h1></div>
                @if($subcategoryCache->offer != null)
                <div class="description"><p>{{$subcategoryCache->offer or ''}}</p></div>
                @endif
            </div>
        @elseif($content == 2)
        <?php //dd($content); ?>
            <div class="c">
                <div class="content_title"><h1>{{$categoryCache->title or ''}}</h1></div>
                @if($categoryCache->offer != null)
                <div class="description"><p>{{$categoryCache->offer or ''}}</p></div>
                @endif
                @if(count($categoryCache->subcategories)>0)
                    <div class="c-c_l">
                        @forelse($categoryCache->subcategories as $subcategory)
                            <!-- <script type="text/javascript">$(document).ready(function () {$('#ImageCategory{{$subcategory->id}}').attr('src', '/storage/app/{{Resize::ResizeImage($subcategory->images, "283x187")}}');});</script> -->
                            <div class="i">
                                <a href="/product/{{$subcategory->id}}-{{$subcategory->slug or ''}}">
                                    <div class="image">
                                        <div class="dark"></div>
                                        <img id="ImageCategory{{$subcategory->id}}" src="/storage/app/{{Resize::ResizeImage($subcategory->images, '283x187')}}" alt=""></div>
                                    <div class="info">
                                        <div class="name"><span>{{$subcategory->title or ''}}</span></div>
                                    </div>
                                </a>
                            </div>
                        @empty
                        @endforelse
                    </div>
                @endif
            </div>
        @else

            <div class="c">
                    <div class="content_title"><h1>{{$section or $title}}</h1></div>
                    <div class="description"><p>{{$texts->offer or ''}}</p></div>
                    <div class="c-c_l">
                        @forelse($categories as $subcategory)
                            <!-- <script type="text/javascript">$(document).ready(function () {$('#ImageCategory{{$subcategory->id}}').attr('src', '/storage/app/{{Resize::ResizeImage($subcategory->images, "283x187")}}');});</script> -->
                            <div class="i">
                                <a href="/product/{{$subcategory->id}}-{{$subcategory->slug or ''}}">
                                    <div class="image">
                                        <div class="dark"></div>
                                        <img id="ImageCategory{{$subcategory->id}}" src="/storage/app/{{Resize::ResizeImage($subcategory->images, '283x187')}}" alt=""></div>
                                    <div class="info">
                                        <div class="name"><span>{{$subcategory->title or ''}}</span></div>
                                    </div>
                                </a>
                            </div>
                        @empty
                        @endforelse
                    </div>

            </div>
        @endif

    <div class="c grey">
        <div class="content_title"><h2>{{$title or 'Товары'}}</h2></div>
        <div class="c-g">
            <div class="c-g_l c-g_l-md">
                @if($content == null)
                <?php //dd($subcategoryCache);?>
                @if($subcategoryCache->categor->type == 0)
                            @forelse($subcategoryCache->goods as $g)

                        <!-- <script type="text/javascript">$(document).ready(function () {$('#ImageGoods{{$g->id}}').attr('src', '/storage/app/{{Resize::ResizeImage($g->images, "200x200")}}');});</script> -->
                            <div class="item">
                                <a href="/product/{{$subcategoryCache->id}}-{{$subcategoryCache->slug or ''}}/{{$g->id}}-{{$g->slug or ''}}">
                                    <div class="image"><div class="img_dark"></div><img id="ImageGoods{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '200x200')}}" alt=""></div>
                                    <div class="info">
                                        <div class="name"><span>{{$g->title or ''}}</span></div>
                                        <div class="prices">
                                            <div class="minimum">Цена:</div>
                                            <div class="price-content">{{$g->coast}} р.</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                    @empty
                    @endforelse

                        @else

                            @forelse($subcategoryCache->services as $g)
                        <!-- <script type="text/javascript">$(document).ready(function () {$('#ImageGoods{{$g->id}}').attr('src', '/storage/app/{{Resize::ResizeImage($g->images, "200x200")}}');});</script> -->
                            <div class="item">
                                <a href="/product/{{$subcategoryCache->id}}-{{$subcategoryCache->slug or ''}}/{{$g->id}}-{{$g->slug or ''}}">
                                    <div class="image"><div class="img_dark"></div><img id="ImageGoods{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '200x200')}}" alt=""></div>
                                    <div class="info">
                                        <div class="name"><span>{{$g->title or ''}}</span></div>
                                        <div class="prices">
                                            <div class="minimum">Цена:</div>
                                            <div class="price-content">{{$g->coast}} р.</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                    @empty
                    @endforelse
                        @endif





                @elseif($content == 2)

                    <?php $i = 1; //dd($categoryCache);?>

                    @if($categoryCache->type == 0)
                    @forelse($categoryCache->goods as $g)


                            <!-- <script type="text/javascript">$(document).ready(function () {$('#ImageGoods{{$g->id}}').attr('src', '/storage/app/{{Resize::ResizeImage($g->images, "200x200")}}');});</script> -->
                            <div class="item">
                                <a href="/product/{{$categoryCache->id}}-{{$categoryCache->slug or ''}}/{{$g->id}}-{{$g->slug or ''}}">
                                    <div class="image"><div class="img_dark"></div><img id="ImageGoods{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '200x200')}}" alt=""></div>
                                    <div class="info">
                                        <div class="name"><span>{{$g->title or ''}}</span></div>
                                        <div class="prices">
                                            <div class="minimum">Цена:</div>
                                            <div class="price-content">{{$g->coast}} р.</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @if($i >20)
                                <?php break; ?>
                            @endif
                            <?php $i++; ?>
                        @empty
                        @endforelse
                    @else
                    @forelse($categoryCache->services as $g)

                            <!-- <script type="text/javascript">$(document).ready(function () {$('#ImageGoods{{$g->id}}').attr('src', '/storage/app/{{Resize::ResizeImage($g->images, "200x200")}}');});</script> -->
                            <div class="item">
                                <a href="/product/{{$categoryCache->id}}-{{$categoryCache->slug or ''}}/{{$g->id}}-{{$g->slug or ''}}">
                                    <div class="image"><div class="img_dark"></div><img id="ImageGoods{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '200x200')}}" alt=""></div>
                                    <div class="info">
                                        <div class="name"><span>{{$g->title or ''}}</span></div>
                                        <div class="prices">
                                            <div class="minimum">Цена:</div>
                                            <div class="price-content">{{$g->coast}} р.</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @if($i >20)
                                <?php break; ?>
                            @endif
                            <?php $i++; ?>
                        @empty
                        @endforelse
                    @endif

                    @forelse($categoryCache->subcategories as $sub)
                    <?php //dd($sub);?>
                        @forelse($sub->goods as $g)
                            <?php //dd($g);?>
                            <!-- <script type="text/javascript">$(document).ready(function () {$('#ImageGoods{{$g->id}}').attr('src', '/storage/app/{{Resize::ResizeImage($g->images, "200x200")}}');});</script> -->
                            <div class="item">
                                <a href="/product/{{$sub->id}}-{{$sub->slug or ''}}/{{$g->id}}-{{$g->slug or ''}}">
                                    <div class="image"><div class="img_dark"></div><img id="ImageGoods{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '200x200')}}" alt=""></div>
                                    <div class="info">
                                        <div class="name"><span>{{$g->title or ''}}</span></div>
                                        <div class="prices">
                                            <div class="minimum">Цена:</div>
                                            <div class="price-content">{{$g->coast}} р.</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @if($i >20)
                                <?php break; ?>
                            @endif
                            <?php $i++; ?>
                        @empty
                        @endforelse
                        @if($i >20)
                            <?php break; ?>
                        @endif
                        <?php $i++; ?>
                    @empty
                    @endforelse
                @else
                        <?php //dd($category);?>
                            @forelse($goods as $g)
                                @if($g->subcategory->categor !== null)
                                <?php $category = $g->subcategory->categor; ?>
                                @else
                                <?php $category = $g->subcategory; ?>
                                @endif
                                <?php //dd($g); ?>
                                <!-- <script type="text/javascript">$(document).ready(function () {$('#ImageGoods{{$g->id or ''}}').attr('src', '/storage/app/{{Resize::ResizeImage($g->images, "200x200")}}');});</script> -->

                                <div class="item">
                                    <a href="/product/{{$g->subcategory->id or ''}}-{{$g->subcategory->slug or ''}}/{{$g->id or ''}}-{{$g->slug or ''}}">
                                        <div class="image"><div class="img_dark"></div><img id="ImageGoods{{$g->id or ''}}" src="/storage/app/{{Resize::ResizeImage($g->images, '200x200')}}" alt=""></div>
                                        <div class="info">
                                            <div class="name"><span>{{$g->title or ''}}</span></div>
                                            <div class="prices">
                                                <div class="minimum">Цена:</div>
                                                <div class="price-content"{{$g->coast}} р.</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                </div>
                            @empty
                            @endforelse

                            <!--@include('2019.layouts.paginator', ['paginate' => $goods])-->
                            <?php //dd($goods);?>
                @endif
            </div>
        </div>
    </div>
    <?php //dd($content);?>
    @if($content == 2)
    <div class="c">
        <div class="description">
            {!! $categoryCache->description or '' !!}
        </div>
    </div>
    @elseif($content == 1)
    <div class="c">
        <div class="description">
            {!! $texts->text or '' !!}
        </div>
    </div>
    @else
    <div class="c">
        <div class="description">

            {!! $subcategoryCache->description or '' !!}

        </div>
    </div>
    @endif
</div>
</div>
</div>
</section>
<script type="text/javascript">
    $('#sm-{{$cat}}').addClass('active');
</script>
<?php //dd($subcategoryCache);?>
