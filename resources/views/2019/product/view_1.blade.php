<style>
.b_nav{background:#f3f3f3}
.b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
.b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
.b_nav li a{color:#999}
.breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
.breadcrumb-item.active{color:#aaa}
.breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
@media (min-width: 1400px) {
    .b_nav .breadcrumb{max-width:1140px}
}
@media (max-width: 1399px) {
    .b_nav .breadcrumb{width:100%;padding:13px 20px}
}
@media screen and (max-width: 992px) {
    .b_nav .breadcrumb{padding:13px 10px}
}
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/product" itemprop="item"><span itemprop="name">Товары</span></a>
                <meta itemprop="position" content="2" />
            </li>
            @if($category_parent->bread_cat == null)
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/product/{{$category_parent->id}}-{{$category_parent->slug}}" itemprop="item"><span itemprop="name">{{$category_parent->title}}</span></a>
                    <meta itemprop="position" content="3" />
                </li>
                <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{$product->title or ''}}</span>
                    <meta itemprop="position" content="4" />
                </li>
            @else
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/product/{{$category_parent->bread_cat->id}}-{{$category_parent->bread_cat->slug}}" itemprop="item"><span itemprop="name">{{$category_parent->bread_cat->title}}</span></a>
                    <meta itemprop="position" content="3" />
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/product/{{$category_parent->id}}-{{$category_parent->slug}}" itemprop="item"><span itemprop="name">{{$category_parent->title}}</span></a>
                    <meta itemprop="position" content="4" />
                </li>
                <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{$product->title or ''}}</span>
                    <meta itemprop="position" content="5" />
                </li>
            @endif
        </ol>
    </nav>
</section>
<style>
.p-o_l{width:100%;display:flex;margin-bottom:40px}
.p-o_l .info .title h1{font-weight:600;font-size:20px;line-height:20px;margin:0}
.p-o_l .img_b{width:40%;display:block;float:left;background:#fff;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
.p-o_l .img_b .image{width:100%;height:28.5vw;padding:10px;border-bottom:1px solid #e6e6e6}
.p-o_l .img_dark{opacity:0;background-color:rgba(0,0,0,.2);-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=0);-webkit-transition:opacity .8s cubic-bezier(.19,1,.22,1);transition:opacity .8s cubic-bezier(.19,1,.22,1);position:absolute;display:block;width:100%;height:100%;z-index:2}
.p-o_l .lightbox:hover .img_dark{opacity: 1}
.p-o_l .img_b .first-image{width:100%;height:100%;position:relative;overflow:hidden;display:table}
.p-o_l .img_b .image img{width:100%;height:100%;object-fit:cover}
.p-o_l .img_b .gallery{width:100%;display:table;padding:7px}
.p-o_l .img_b .gallery .item{width:20%;float:left}
.p-o_l .img_b .gallery .item .lightbox{height:5vw;margin:3px;display:block;background-repeat:no-repeat;background-size:cover}
.p-o_l .info{width:60%;float:left;background:#fff;position:relative;padding-bottom:65px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}
.p-o_l .info .title{padding:10px 15px;width:100%;border-bottom:1px solid #e6e6e6}
.p-o_l .info .title .data{font-size:14px;line-height:14px;margin-top: 5px;color:#888}
.p-o_l .info .price{width:100%;padding:20px 15px;display:table;border-bottom:1px solid #e6e6e6;font-size:14px}
.p-o_l .info .price span{font-weight:600;font-size:20px;line-height: 20px;color:#222}
.p-o_l .info .additionally{padding:15px;width:100%}
.p-o_l .info .additionally .item{margin-bottom:15px;color:#888;font-size:14px;line-height:16px}
.p-o_l .info .additionally .item span{font-weight:600;color:#222}
.p-o_l .info .connection{position:absolute;bottom:0;padding:15px;display:flex}
.p-o_l .info .connection .to-order{background:#269ffd;border:1px solid #269ffd;color:#fff;padding:9px 8px;font-size:18px;    line-height: 18px;width:125px;text-align:center;border-radius:3px}
.p-o_l .info .connection .write{background:#fff;border:1px solid #269ffd;color:#269ffd;padding:9px 8px;font-size:18px;    line-height: 18px;width:160px;text-align:center;border-radius:3px}
@media (min-width: 1400px) {
    .p-o_l .img_b .image{height:337px}
    .p-o_l .img_b .gallery .item .lightbox{height:58px}
}
@media (max-width: 992px) {
    .p-o_l .img_b .image {height: 39vw;}
    .p-o_l .img_b .gallery .item .lightbox{height: 7vw;}
}
@media (max-width: 575px) {
    .p-o_l{display: table}
    .p-o_l .img_b{width: 100%}
    .p-o_l .img_b .image {height: 97vw;}
    .p-o_l .img_b .gallery .item{width: 16.666%;}
    .p-o_l .img_b .gallery .item .lightbox{height: 14vw;}
    .p-o_l .info{width: 100%}
    .p-o_l .info .title{padding: 20px 15px}
    .p-o_l .info .connection{width: 100%}
    .p-o_l .info .connection a{width: 50%}
    .p-o_l .info .connection .to-order{width: 100%;padding: 5px 8px;font-size: 20px;}
    .p-o_l .info .connection .write{padding: 5px 8px;width: 100%;font-size: 20px;}
}
.p-o_l .connection button{padding: 0;border: 0;background: none;}
.p-o_l .info .additionally .item.switch-on{margin-bottom:10px}
.switch-list{list-style-type:none;margin:0;display:table;padding:0;width:100%;margin-bottom:10px}
.switch-list .switch{display:block;float:left;margin-right:5px;margin-bottom: 5px}
.switch-list .tooltip{position:relative;opacity:1;border:1px solid #ddd}
.switch-list .tooltip.active{border:1px solid #269ffd}
.switch-list .button-text{background:#fff;padding:7px 10px;cursor:pointer;border:none}
.switch-list .button-text:hover{background:#eee}
.switch-list .tooltip.active .button-text{background:#f0f8ff;font-weight:600}
.switch-list .button-text span{font-size:14px;line-height:14px;display:block}
</style>
<section class="i80">
    <div class="page_product_2">
        <div class="row">
            <div class="col-9">
                <div class="product">
                    <div class="p-o_l">
                        <div class="img_b tz-gallery">
                            <div class="image"><a class="lightbox first-image" href="/storage/app/{{$product->images->images or ''}}"><div class="img_dark"></div><img src="/storage/app/{{Resize::ResizeImage($product->images, '315x315')}}" alt=""></a></div>
                            <div class="gallery">
                                <?php $i = 0; ?>
                                @forelse($product->imageses as $img)
                                @if($i>0)
                                <div class="item"><a class="lightbox" href="/storage/app/{{$img->images or ''}}" style="background-image:url(/storage/app/{{Resize::ResizeImage($img, '50x50')}});"></a></div>
                                @endif
                                <?php $i++; ?>
                                @empty
                                @endforelse
                            </div>
                        </div>
                        <div class="info">
                            <div class="title"><h1>{{$product->title or ''}}</h1><div class="data">id товара: {{$product->id or ''}}, опубликовано {{Resize::date($product->created_at)}}</div>@if(Auth::user() !== null)<div class="data"><a target="_blank" href="/admin/goods/{{$product->id or ''}}/edit?id={{$product->id or ''}}&&admin=true">Редактировать</a></div>@endif
                            </div>
                            <div class="price">@if($product->coast != null)<span>{{$product->coast or ''}}</span> руб. @else <span style="font-size: 17px;">Цена не указана</span> @endif</div>
                            <div class="additionally">
                                <div class="item">Минимальный заказ: <span>1 шт.</span></div>
                                <div class="item">Оплата: <span>Наличный расчет / Безналичный расчет</span></div>
                                <div class="item">Доставка: <span>Доставка автопарком компании</span></div>
                            </div>
                            <div class="connection">
                                <button style="margin-right: 15px"><div class="to-order" id="toorder">Заказать</div></button>
                                <button><div class="write" id="needcall">Перезвонить?</div></button>
                            </div>
                        </div>
                    </div>
                    @if($product->short_description != null)
                    <div class="c">
                        <div class="content_title"><h2>Описание товара</h2></div>
                        <div class="description">{!! $product->short_description or '' !!}</div>
                    </div>
                    @endif
                    <style>
                        .portfolio{display: table;margin: 5px;width: 100%}
                        .portfolio .i{width: 24%;float: left;margin: 0.5%;}
                        .portfolio .image{height: 14vh;width: 100%}
                        .portfolio .i img{width: 100%;height: 100%;object-fit: cover}
                        @media (max-width: 778px) {
                            .portfolio .i{width: 32.333%}
                        }
                        @media (max-width: 575px) {
                            .portfolio .i{width: 49%}
                        }
                    </style>
                    <div class="c">
                        <div class="content_title"><h2>Фото наших работ</h2></div>
                        <div class="portfolio">
                            @forelse($portfolio as $p)
                                <div class="i image_modal" slug="{{$p->id}}"><div class="image"><img src="/storage/app/{{Resize::ResizeImage($p->image_one, '190x125')}}" alt=""></div></div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                    <script type="text/javascript">
                        // .image - отвечает за класс на который надо нажимать, чтоб сработал ajax
                        $('.image_modal').click(function(){
                            var id = $(this).attr('slug');
                            $.get('/portfolio/view/image/description/'+id, function(data){
                                $('.modal_portfolio').html(data);
                            });

                            var url = '?object=' + id;
                            history.pushState(null, null, url);

                            @if($favicon->metrika_short != null)
                            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                                m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
                            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
                            ym({{$favicon->metrika_short}}, 'hit', url);
                            @endif
                        });
                    </script>
                    <div class="modal_portfolio"></div>
                    <style>
                        .reviews{min-width:100%;display:table}
                        .reviews .i{padding:15px;width: 100%;background:#fff;border:1px solid #e6e6e6;margin:.25em 0;display:inline-block}
                        .reviews .i.open .reviews_button{display:none}
                        .reviews .i .write_rev{display:none}
                        .reviews .i.open .write_rev{display:block}
                        .reviews .i.open{padding:15px!important;border:1px solid #e6e6e6!important}
                        .rating{color:#fdcc63;font-size:12px;line-height:12px;margin-bottom:6px}
                        .reviews .user{display:flex}
                        .reviews .user .user_image{width:50px;height:50px;border-radius:50%;overflow:hidden;min-width:50px}
                        .reviews .user img{width:100%;height:100%;object-fit:cover}
                        .user_name{width:100%;padding-left:7px;margin:auto}
                        .reviews .user .name{font-weight:600;font-size:15px;line-height:17px}
                        .reviews .user .date{color:#888;font-size:14px}
                        .i_desc{font-size:15px;line-height:22px;max-height:85px;position:relative;overflow:hidden}
                        .toggle-btn{font-size:15px;line-height:17px;margin-top:10px;display:table}
                        .text-open{overflow:visible;height:auto;max-height:none}
                        .text-open + .toggle-btn{display:none}
                        .i_gallery{display:table;width:100%;margin-top:10px}
                        .i_gallery .image{width:31.333%;height:50px;margin:1%;float:left}
                        .reviews .i_gallery img{width:100%;height:100%;object-fit:cover}
                        @media (max-width: 778px) {
                            .reviews .i{width:49%}
                            .i_gallery .image{height:7.5vh;width:25%}
                        }
                        @media (max-width: 575px) {
                            .reviews .i{width:99%}
                            .i_gallery .image{height:6.5vh;width:20%;margin:.5%}
                        }
                        .masonry{-moz-column-count:3;-webkit-column-count:3;column-count:3;-webkit-column-gap:.5em;-moz-column-gap:.5em;column-gap:.5em;padding:0;font-size:.85em}
                        @media only screen and (max-width: 320px) {
                            .masonry{-moz-column-count:1;-webkit-column-count:1;column-count:1}
                        }
                        @media only screen and (min-width: 321px) and (max-width: 768px) {
                            .masonry{-moz-column-count:2;-webkit-column-count:2;column-count:2}
                        }
                        @media only screen and (min-width: 769px) {
                            .masonry{-moz-column-count:3;-webkit-column-count:3;column-count:3}
                        }
                        .reviews_button{display:inline-block;vertical-align:top;padding:15px;font-size:13px;line-height:14px;width:100%;text-align:center;text-transform:uppercase;position:relative;color:#fdcc63!important;font-weight:600;border:1px solid #f7e8a8}
                        .reviews_input{width:100%;border:1px solid #e6e6e6;padding:7px 15px;margin-bottom:5px}
                        .reviews_input_file{margin-bottom:15px}
                        .reviews_textarea{text-align:left;-webkit-box-sizing:border-box;box-sizing:border-box;overflow:hidden;border:1px solid #e6e6e6;height: 80px;padding:15px;margin-bottom: 10px;width:100%;resize:none;outline:none;font:14px/17px Helvetica,Arial,sans-serif}
                        .reviews_textarea_button{position:relative;outline:none;border:none;padding:7px 20px;min-width:150px;min-height:32px;border-radius:3px;background-color:#007bab;font-size:14px;line-height:16px;cursor:pointer;color:#fff}
                    </style>
<div class="c grey" style="background: none">
<div class="content_title" style="background: #fff"><h2>Отзывы наших заказчиков:</h2></div>
<div class="reviews">
    <div class="masonry">
        <div class="i" style="padding: 0;border:none" id="ddd3">
            <a href="javascript:void(0);" class="reviews_button" onclick="reviewsFunction()">Оставить отзыв</a>
            <!--                    <div class="fdf" id="ddd3"></div>-->
            <div class="write_rev">
                <form action="/feedback/store/{{$product->id or ''}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="type" value="goods">
                    <input type="text" name="name" class="reviews_input" placeholder="Имя Фамилия" value="">
                    <label for="file">Ваша фотография:</label>
                    <input type="file" name="ava" class="reviews_input_file" placeholder="Текст отзыва..." value="" style="border: 0;padding-left: 0;width: 100%">
                    <textarea name="description" id="" class="reviews_textarea" placeholder="Текст отзыва..."></textarea>
                    <div id="example3"></div>
                        <div class="submitReview">
                            <button class="reviews_textarea_button" style="background-color:#ddd" disabled>Отправить</button>
                        </div>
                </form>
            </div>
        </div>
        @forelse($reviews as $r)
            <div class="i">
                <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                <div class="user">
                    <div class="user_image"><img src="/storage/app/{{Resize::ResizeImage($r, '45x45')}}" alt=""></div>
                    <div class="user_name">
                        <div class="name">{{$r->name}}</div>
                        <div class="date">Добавлено: <span>{{Resize::noTimeDate($r->created_at)}}</span></div>
                    </div>
                </div>
                <div class="i_desc">{!! $r->description !!}</div>
                <a href="" class="toggle-btn">Читать полностью</a>
                @if($r->imageses != null)
                    <div class="i_gallery tz-gallery">
                        @forelse($r->imageses as $img)
                            <div class="image">
                                <a href="/storage/app/{{$img->images or ''}}" class="lightbox">
                                    <img src="/storage/app/{{Resize::ResizeImage($img, '75x50')}}" alt="">
                                </a>
                            </div>
                        @empty
                        @endforelse
                    </div>
                @endif
            </div>
        @empty
        @endforelse
    </div>
</div>
</div>
<script>
$(".toggle-btn").click(function(e) {e.preventDefault();$(this).prev().toggleClass("text-open")})
</script>
                    <div class="c">
                        <div class="content_title"><h3>Часто задаваемые вопросы:</h3></div>
                        <div class="description"></div>
                    </div>
                    <div class="c grey">
                        <div class="content_title"><h2>Товары похожие на: {{$product->title or 'Ошибка: название услуги'}}</h2></div>
                        <div class="c-g">
                            <div class="c-g_l c-g_l-md">
                                @forelse($similar as $g)
                                <div class="item">
                                    <a href="/product/{{$category_parent->id}}-{{$category_parent->slug}}/{{$g->id}}-{{$g->slug}}">
                                        <div class="image"><div class="img_dark"></div><img id="ImageSimilar{{$g->id or 'Ошибка: id'}}" src="/storage/app/{{Resize::ResizeImage($g->images, '19x19')}}" alt=""></div>
                                        <div class="info">
                                            <div class="name"><span>{{$g->title or ''}}</span></div>
                                            <div class="prices"><div class="minimum">Цена:</div><div class="price-content">@if($g->coast != null){{$g->coast or ''}} - @else Цена не указана @endif</div></div>
                                        </div>
                                    </a>
                                </div>
                                <script type="text/javascript">$(document).ready(function(){$('#ImageSimilar{{$g->id}}').attr('src', '/storage/app/{{Resize::ResizeImage($g->images, "190x190")}}');});</script>
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="manager">
                    <div class="title">Контактная информация</div>
                    <div class="block">
                        <div class="person">
                            <div class="image" style="background-image: url(/storage/app/{{Resize::ResizeImage($manager, '55x55')}});"></div>
                            <div class="info"><div class="name">{{$manager->name or 'Имя'}} {{$manager->lastname or 'Фамалия'}}</div><div class="position">{{$manager->job or 'Должность'}}</div></div>
                        </div>
                        <div class="data phone"><span>{{$manager->tel or '+7 (900) 000-00-00'}}</span></div>
                        <div class="data email"><a target="_blank" href="/contact"><span>Отправить сообщение</span></a></div>
                        <div class="data local"><div class="address"><span>г. {{$company->address or 'Заполните адрес'}}</span><br><a class="open_map">Показать на карте</a></div></div>
                        <div class="data timework"><span>Время работы: {{$manager->timeJob or 'не указано'}}</span></div>
                    </div>
                </div>
                <div class="map">
                    <div class="title"><span>Карта</span></div>
                    <a class="open_map"><div class="back-map"><div class="block_map_b"><button class="map_b">Показать карту</button></div><div class="map-view" style="background-image: url(/public/site/img/map-load.jpg);"></div></div></a>
                </div>
                @if(count($documents) != null)
                <div class="documents">
                    <div class="title">Документы</div>
                    <div class="documents_list">
                        @forelse($documents as $d)
                            <div class="item">
                                <div class="file file-{{$d->type}}"></div>
                                <div class="info">
                                    <div class="name"><a href="/storage/app/{{$d->doc}}/">{{$d->title}}</a></div>
                                    <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
<link rel="stylesheet" href="/public/site/open-image/open-image.css">
<script src="/public/site/open-image/open-image.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.open_map').click(function(){$.get('{{route('modal.open.map')}}', function(data){$('.modal').html(data);})});
    });
</script>
<script type="text/javascript">
    document.getElementById('toorder').onclick = function() {$.get('{{route('take.modal.product', ['id' => $product->id])}}', function(data){$('.modal').html(data);})};
    document.getElementById('needcall').onclick = function() {$.get('{{route('take.modal.needcall', ['id' => $product->id])}}', function(data){$('.modal').html(data);})};
</script>
<style media="screen">
  .button-feedback{
    position: relative;
overflow: hidden;
width: 180px;
height: 30px;
background: #2196f3;
border: none;
border-radius: 3px;
padding: 4px;
color: #fff;
text-align: center;
  }
  .form-control{
    background-color: #fff;
width: 100%;

border: 1px solid #ccc;
font-size: 14px;
font-weight: 400;
border-radius: 5px;
color: #222;
padding-left: 5px;
  }
</style>
<script type="text/javascript">
      var verifyCallback = function(response) {
        $('.submitReview').html('<input type="submit" value="Отправить" class="reviews_textarea_button">');
      };
      var onloadCallback = function() {
        grecaptcha.render('example3', {
          'sitekey' : '{{Resize::recaptcha()}}',
          'callback' : verifyCallback,
        });
      };
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>