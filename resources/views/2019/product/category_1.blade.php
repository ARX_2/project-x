<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/product" itemprop="item"><span itemprop="name">Товары</span></a>
                <meta itemprop="position" content="2" />
            </li>
            @if($category->bread_cat == null)
                <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{$category->title}}</span>
                    <meta itemprop="position" content="3" />
                </li>
            @else
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/product/{{$category->bread_cat->id}}-{{$category->bread_cat->slug}}" itemprop="item"><span itemprop="name">{{$category->bread_cat->title}}</span></a>
                    <meta itemprop="position" content="3" />
                </li>
                <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{$category->title}}</span>
                    <meta itemprop="position" content="4" />
                </li>
            @endif
        </ol>
    </nav>
</section>
<style>
    .nsm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}.nsm .title{padding:15px 15px 0;line-height:17px;font-size:17px;margin-bottom:10px;font-weight:600}.nsm ul{list-style:none;padding:0;margin:0}.nsm .parent_cat{position:relative;color:#666;font-weight:600;border-bottom:1px solid #e6e6e6;cursor:pointer}.nsm ul .parent_cat{padding:0 15px}.nsm ul .parent_cat{padding:0 15px}.nsm .parent_cat span{display:table-cell;padding:10px 20px 10px 0;width:100%;color:#007bff}</style>
<section class="i80">
    <div class="page_category_2">
        <div class="row">
            <div class="col-3">
                <div class="nsm">
                    <div class="title">Категории</div>
                    <div class="menu-list">
                        <ul class="menu-content">
                            @forelse($category_nav as $c)
                                <li class="cat-item @if(count($c->sub_category_nav)>0) collapse @else 111 @endif">
                                    <div class="parent_cat @if($c->id == $category->id) open_cat @else @endif" @if(count($c->sub_category_nav)>0)onclick="smFunction('sm-{{$c->id}}')" @else  @endif>
                                        @if(count($c->sub_category_nav) == 0)
                                            <a href="/product/{{$c->id}}-{{$c->slug or ''}}"><span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or ''}}@endif</span></a>
                                        @else
                                            <span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or ''}}@endif</span>
                                        @endif
                                    </div>
                                    @if(count($c->sub_category_nav)>0)
                                        <ul class="sub-menu collapsed @if($c->id == $category_id || $c->open_cat == 1 || $category->parent_id == $c->id)active @endif" id="sm-{{$c->id}}">
                                            @forelse($c->sub_category_nav as $sub)
                                                <li class="sub_cat @if($sub->id == $category->id) open_cat @else @endif"><a href="/product/{{$sub->id}}-{{$sub->slug or ''}}">@if($sub->seoname != null){{$sub->seoname}}@else{{$sub->title or ''}}@endif</a></li>
                                            @empty
                                            @endforelse
                                        </ul>
                                    @endif
                                </li>
                            @empty
                            @endforelse
                        </ul>
                    </div>
                </div>
                @if(count($documents) != null)
                <div class="documents">
                    <div class="title">Документы</div>
                    <div class="documents_list">
                        @forelse($documents as $d)
                            <div class="item">
                                <div class="file file-{{$d->type}}"></div>
                                <div class="info">
                                    <div class="name"><a href="/storage/app/{{$d->doc}}/" target="_blank">{{$d->title}}</a></div>
                                    <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
                @endif
            </div>
            <style>
                .c-c_l{width:100%;display:table;margin-top:10px}
                .c-c_l .i{float:left;width:32.333%;display:block;position:relative;margin:.5%}
                .c-c_l .i a{display:table;color:#fff;width:100%}
                .c-c_l .i .image{width:100%;height:16vw;width:100%;height:15vw;position:relative;overflow:hidden}
                .c-c_l .i .image img{width:100%;height:100%;object-fit:cover;-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-o-transition:all .3s ease;transition:all .3s ease}
                .c-c_l .i .dark{opacity:0;background-color:rgba(0,0,0,.2);-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=0);-webkit-transition:opacity .8s cubic-bezier(.19,1,.22,1);transition:opacity .8s cubic-bezier(.19,1,.22,1);position:absolute;display:block;width:100%;height:100%;z-index:2}
                .c-c_l .i:hover .dark{opacity:1}
                .c-c_l .i:hover img{-webkit-transform:scale(1.1);-ms-transform:scale(1.1);transform:scale(1.1)}
                .c-c_l .i .info{padding:0;position:absolute;bottom:10px;right:20px;left:20px;z-index:3}
                .c-c_l .i .name{margin-bottom:10px;width:100%;font-size:17px;line-height:18px;overflow:hidden;font-weight:600}
                .c-c_l .i .name span{background:#269ffd;padding:0 .5em .15em .1em;line-height:1.4}
                @media (min-width: 1400px) {
                    .c-c_l .i .image{height:176px}
                }
                @media screen and (max-width: 992px) {
                    .c-c_l .i .image{height:20.7vw}
                }
                @media screen and (max-width: 575px) {
                    .c-c_l .i{width:49%}
                    .c-c_l .i .image{height:31vw}
                }
                @media screen and (max-width: 480px) {
                    .c-c_l .i{width:99%}
                    .c-c_l .i .image{height:63vw}
                    .c-c_l .i .name{font-size:18px}
                }
            </style>
            <div class="col-9">
                @if($category->cover !== null)
                <div class="c" style="margin-bottom: 0">
                    <div class="cover" style="background-image: url('/storage/app/{{Resize::ResizeImage($category->cover, '847x200')}}')"></div>
                </div>
                @elseif($category->images !== null)
                <div class="c" style="margin-bottom: 0">
                    <div class="cover" style="background-image: url('/storage/app/{{Resize::ResizeImage($category->images, '847x200')}}')"></div>
                </div>
                @endif
                <div class="c">
                    <div class="content_title"><h1>{{$category->title}}</h1>@if(Auth::user() !== null)<div class="data"><a target="_blank" href="/admin/category/{{$category->id}}/edit">Редактировать</a></div>@endif</div>
                    @if($category->offer != null)<div class="description"><p>{!! $category->offer !!}</p></div>@endif
                    @if(count($sub_category) != null)
                    <div class="c-c_l">
                        @forelse($sub_category as $c)
                            <div class="i">
                                <a href="/product/{{$c->id}}-{{$c->slug}}">
                                    <div class="image">
                                        <div class="dark"></div>
                                        <img src="/storage/app/{{Resize::ResizeImage($c->images, '270x176')}}" alt="{{$c->title or 'Ошибка: alt'}}"></div>
                                    <div class="info">
                                        <div class="name"><span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or 'Ошибка: Название категории'}}@endif</span></div>
                                    </div>
                                </a>
                            </div>
                        @empty
                        @endforelse
                    </div>
                    @endif
                </div>
                @if(count($goods) != null)
                <div class="c grey">
                    <div class="content_title"><h2>Товары</h2></div>
                    <div class="c-g">
                        @if($settings !== null && $settings->size == 1)<div class="c-g_l c-g_l-xl">@else <div class="c-g_l c-g_l-md"> @endif
                        @forelse($goods as $p)
                            <div class="item">
                                <a href="/product/{{$p->product_url_scat->id}}-{{$p->product_url_scat->slug}}/{{$p->id}}-{{$p->slug}}">
                                    <div class="image"><div class="img_dark"></div>
                                        <img id="ImageGoods{{$p->id}}" src="/storage/app/{{Resize::ResizeImage($p->images, '185x185')}}" alt="">
                                    </div>
                                    <div class="info">
                                        <div class="name"><span>{{$p->title or ''}}</span></div>
                                        <div class="prices">
                                            <div class="minimum">Цена:</div>
                                            <div class="price-content">@if($p->coast != null){{$p->coast or ''}} р. @else Цена не указана @endif</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @empty
                        @endforelse
                        </div>
                    </div>
                </div>
                @endif
                {{ $goods->links() }}
                @if($category->description != null)<div class="c"><div class="description">{!! $category->description !!}</div></div>@endif
<script type="text/javascript">
$(document).ready(function(){
$('a.lightbox').divbox({caption: false});
$('a.lightbox2').divbox({caption: false, width: 600, height: 400});
$('a.iframe').divbox({ width: 200, height: 200 , caption: false});
$('a.ajax').divbox({ type: 'ajax', width: 200, height: 200 , caption: false,api:{
afterLoad: function(o){
$(o).find('a.close').click(function(){
o.closed();
return false;
});

$(o).find('a.resize').click(function(){
o.resize(200,50);
return false;
});
}
}});
});
</script>
@if(count($category->video) != null)
<div class="c grey video">
    <div class="content_title"><h2 style="font-size:17px;line-height:17px;font-weight:600;margin:0">Видео</h2></div>
    <div class="c-o">
        <div class="c-o_l c-o_l-xxl">
            @forelse($category->video as $v)
            <div class="item ytp">
                <a href="https://www.youtube.com/embed/{{$v->youtube}}?autoplay=1" class="lightbox2">
                    <div class="image" style="background-image: url(http://img.youtube.com/vi/{{$v->youtube}}/hqdefault.jpg);">
                        <button class="ytp-button">
                            <svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z" fill="#212121" fill-opacity="0.8"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg>
                        </button>
                    </div>
                </a>
                <div class="info">
                    <div class="name" style="font-weight: 600"><h3>{{$v->title}}</h3></div>
                    <div class="info_desc"><p>{{$v->description}}</p></div>
                </div>
            </div>
            @empty
            @endforelse
        </div>
    </div>
</div>
@endif
</div>
</div>
</div>
</section>
