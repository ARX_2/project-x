<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <span itemprop="name">Товары</span>
                <meta itemprop="position" content="2" />
            </li>
        </ol>
    </nav>
</section>
<style>.nsm{background:#fff;font-size:14px;width:100%;margin-bottom:40px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}.nsm .title{padding:15px 15px 0;line-height:17px;font-size:17px;margin-bottom:10px;font-weight:600}.nsm ul{list-style:none;padding:0;margin:0}.nsm .parent_cat{position:relative;color:#666;font-weight:600;border-bottom:1px solid #e6e6e6;cursor:pointer}.nsm ul .parent_cat{padding:0 15px}.nsm ul .parent_cat{padding:0 15px}.nsm .parent_cat span{display:table-cell;padding:10px 20px 10px 0;width:100%;color:#007bff}</style>
<section class="i80">
<div class="page_category_2">
<div class="row">
<div class="col-3">
    <div class="nsm">
        <div class="title">Категории</div>
        <div class="menu-list">
            <ul class="menu-content">
                @forelse($category_nav as $c)
                    <li class="cat-item @if(count($c->sub_category_nav)>0) collapse @else 111 @endif">
                        <div class="parent_cat" @if(count($c->sub_category_nav)>0)onclick="smFunction('sm-{{$c->id}}')" @else  @endif>
                            @if(count($c->sub_category_nav) == 0)
                                <a href="/product/{{$c->id}}-{{$c->slug or ''}}"><span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or ''}}@endif</span></a>
                            @else
                                <span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or ''}}@endif</span>
                            @endif
                        </div>
                        @if(count($c->sub_category_nav)>0)
                            <ul class="sub-menu collapsed @if($c->open_cat == 1)active @endif" id="sm-{{$c->id}}">
                                @forelse($c->sub_category_nav as $sub)
                                    <li class="sub_cat"><a href="/product/{{$sub->id}}-{{$sub->slug or ''}}">@if($sub->seoname != null){{$sub->seoname}}@else{{$sub->title or ''}}@endif</a></li>
                                @empty
                                @endforelse
                            </ul>
                        @endif
                    </li>
                @empty
                @endforelse
            </ul>
        </div>
    </div>
    @if(count($documents) != null)
    <div class="documents">
        <div class="title">Документы</div>
        <div class="documents_list">
            @forelse($documents as $d)
                <div class="item">
                    <div class="file file-{{$d->type}}"></div>
                    <div class="info">
                        <div class="name"><a href="/storage/app/{{$d->doc}}/" target="_blank">{{$d->title}}</a></div>
                        <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                    </div>
                </div>
            @empty
            @endforelse
        </div>
    </div>
    @endif
</div>
<style>
.c-c_l{width:100%;display:table;margin-top:10px}
.c-c_l .i{float:left;width:32.333%;display:block;position:relative;margin:.5%}
.c-c_l .i a{display:table;color:#fff;width:100%}
.c-c_l .i .image{width:100%;height:16vw;width:100%;height:15vw;position:relative;overflow:hidden}
.c-c_l .i .image img{width:100%;height:100%;object-fit:cover;-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-o-transition:all .3s ease;transition:all .3s ease}
.c-c_l .i .dark{opacity:0;background-color:rgba(0,0,0,.2);-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=0);-webkit-transition:opacity .8s cubic-bezier(.19,1,.22,1);transition:opacity .8s cubic-bezier(.19,1,.22,1);position:absolute;display:block;width:100%;height:100%;z-index:2}
.c-c_l .i:hover .dark{opacity:1}
.c-c_l .i:hover img{-webkit-transform:scale(1.1);-ms-transform:scale(1.1);transform:scale(1.1)}
.c-c_l .i .info{padding:0;position:absolute;bottom:10px;right:20px;left:20px;z-index:3}
.c-c_l .i .name{margin-bottom:10px;width:100%;font-size:17px;line-height:18px;overflow:hidden;font-weight:600}
.c-c_l .i .name span{background:#269ffd;padding:0 .5em .15em .1em;line-height:1.4}
@media (min-width: 1400px) {
    .c-c_l .i .image{height:176px}
}
@media screen and (max-width: 992px) {
    .c-c_l .i .image{height:20.7vw}
}
@media screen and (max-width: 575px) {
    .c-c_l .i{width:49%}
    .c-c_l .i .image{height:31vw}
}
@media screen and (max-width: 480px) {
    .c-c_l .i{width:99%}
    .c-c_l .i .image{height:63vw}
    .c-c_l .i .name{font-size:18px}
}
</style>
<div class="col-9">
    @isset($settings->section_image_one)
    <div class="c" style="margin-bottom: 0">
        <div class="cover" style="height:200px;background-image: url('/storage/app/{{Resize::ResizeImage($settings->section_image_one, "840x200")}}');background-position: 50%;background-size: cover;margin: -5px;"></div>
    </div>
    @endif
    <div class="c">
        <div class="content_title"><h1>{{$settings->title or 'Товары'}}</h1>@if(Auth::user() !== null)<div class="in_admin_edit"><a target="_blank" href="/admin/admin/company/upload?type=4">Редактировать</a></div>@endif</div>
        <div class="description"><p>{!! $settings->offer !!}</p></div>
        <div class="c-c_l">
            @forelse($category_nav as $c)
                <div class="i">
                    <a href="/product/{{$c->id}}-{{$c->slug}}">
                        <div class="image">
                            <div class="dark"></div>
                            <img src="/storage/app/{{Resize::ResizeImage($c->images, '270x176')}}" alt="{{$c->title or 'Ошибка: alt'}}"></div>
                        <div class="info">
                            <div class="name"><span>@if($c->seoname != null){{$c->seoname}}@else{{$c->title or 'Ошибка: Название категории'}}@endif</span></div>
                        </div>
                    </a>
                </div>
            @empty
            @endforelse
        </div>
    </div>
    @if(count($goods) != null)
    <div class="c grey">
        <div class="content_title"><h2>Товары</h2></div>
        <div class="c-g">
            @if($settings->size == 1)<div class="c-g_l c-g_l-xl">@else <div class="c-g_l c-g_l-md"> @endif
            @forelse($goods as $g)
                <div class="item">
                    <a href="/product/{{$g->product_url_cat->id}}-{{$g->product_url_cat->slug}}/{{$g->id}}-{{$g->slug}}">
                        <div class="image"><div class="img_dark"></div>
                            @if($settings->size == 1)
                            <img id="ImageGoods{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '400x270')}}" alt="{{$g->title or ''}}">
                            @else
                            <img id="ImageGoods{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '185x185')}}" alt="{{$g->title or ''}}">
                            @endif
                        </div>
                        <div class="info">
                            <div class="name"><span>{{$g->title or ''}}</span></div>
                            <div class="prices">
                                <div class="minimum">Цена:</div>
                                <div class="price-content">@if($g->coast != null){{$g->coast or ''}} р. @else Цена не указана @endif</div>
                            </div>
                        </div>
                    </a>
                </div>
            @empty
            @endforelse
            </div>
        </div>
    </div>
    @endif
    {{ $goods->links() }}
    @if($settings->text != null)<div class="c"><div class="description">{!! $settings->text !!}</div></div>@endif
</div>
</div>
</div>
</section>
