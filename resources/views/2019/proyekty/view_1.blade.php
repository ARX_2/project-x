<style>
    .b_nav{background:#f3f3f3}
    .b_nav .breadcrumb{background-color:#f3f3f3;margin:0 auto;width:80%}
    .b_nav li{font-size:14px;max-width:33.333%;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;line-height:14px}
    .b_nav li a{color:#999}
    .breadcrumb{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:13px 10px;margin-bottom:1rem;list-style:none;background-color:#e9ecef;border-radius:.25rem}
    .breadcrumb-item.active{color:#aaa}
    .breadcrumb-item+.breadcrumb-item::before{display:inline-block;padding-right:.5rem;padding-left:.5rem;color:#bbb;content:"/"}
    @media (min-width: 1400px) {
        .b_nav .breadcrumb{max-width:1140px}
    }
    @media (max-width: 1399px) {
        .b_nav .breadcrumb{width:100%;padding:13px 20px}
    }
    @media screen and (max-width: 992px) {
        .b_nav .breadcrumb{padding:13px 10px}
    }
</style>
<section class="b_nav">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span></a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                <a href="/proyekty" itemprop="item"><span itemprop="name">Проекты</span></a>
                <meta itemprop="position" content="2" />
            </li>
            @if($category_parent->bread_cat == null)
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/proyekty/{{$category_parent->id}}-{{$category_parent->slug}}" itemprop="item"><span itemprop="name">{{$category_parent->title}}</span></a>
                    <meta itemprop="position" content="3" />
                </li>
                <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{$product->title or ''}}</span>
                    <meta itemprop="position" content="4" />
                </li>
            @else
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/proyekty/{{$category_parent->bread_cat->id}}-{{$category_parent->bread_cat->slug}}" itemprop="item"><span itemprop="name">{{$category_parent->bread_cat->title}}</span></a>
                    <meta itemprop="position" content="3" />
                </li>
                <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="/proyekty/{{$category_parent->id}}-{{$category_parent->slug}}" itemprop="item"><span itemprop="name">{{$category_parent->title}}</span></a>
                    <meta itemprop="position" content="4" />
                </li>
                <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <span itemprop="name">{{$product->title or ''}}</span>
                    <meta itemprop="position" content="5" />
                </li>
            @endif
        </ol>
    </nav>
</section>
<section class="i80">
    <div class="page_product_2">
        <div class="row">
            <div class="col-9">
                <div class="product">
                    <style>
                        .s-o_f{display:table;width: 100%;background:#fff;margin-bottom:40px;   -webkit-box-shadow: -1px 0 0 0 #e6e6e6 inset, 0 -1px 0 0 #e6e6e6 inset, -1px -1px 0 0 #e6e6e6 inset, -1px 0 0 0 #e6e6e6, 0 -1px 0 0 #e6e6e6;box-shadow: -1px 0 0 0 #e6e6e6 inset, 0 -1px 0 0 #e6e6e6 inset, -1px -1px 0 0 #e6e6e6, -1px 0 0 0 #e6e6e6, 0 -1px 0 0 #e6e6e6;-webkit-box-orient: vertical;-webkit-box-direction: normal;}
                        .s-o_f .pheader{padding: 15px 15px 5px;width:100%;display:flex}
                        .s-o_f .pheader .title{display:inline-block;width:60%;padding-right:15px}
                        .s-o_f .pheader .title h1{font-weight: 600;font-size: 20px;line-height: 20px;margin: 0;}
                        .s-o_f .pheader .title .data{font-size:14px;color:#888;line-height: 14px;margin-top: 5px;}
                        .s-o_f .pheader .price{display:inline-block;width:40%;font-size:14px;text-align:right}
                        .s-o_f .pheader .price span{font-weight:600;font-size:20px;line-height: 20px;color:#222}
                        .s-o_f .image_block .image{width:100%;height:346px;padding:10px;border-bottom:1px solid #e6e6e6}
                        .s-o_f .first-image{width:100%;height:100%;position:relative;overflow:hidden;display:table}
                        .s-o_f .blur{width:102%;height:103%;margin:-5px -9px;object-fit:contain;background-position: left;position:absolute;background-repeat:no-repeat;background-size:cover;-webkit-filter:blur(5px);-moz-filter:blur(5px);filter:blur(5px);filter:url(/public/site/img/blur.svg#blur)}
                        .s-o_f .image_block .image img{width:100%;height:100%;object-fit:contain;position:relative;z-index:1}
                        .s-o_f .image_block .gallery{width:100%;display:table;padding:7px}
                        .s-o_f .image_block .gallery .item{width:12.5%;float:left}
                        .s-o_f .image_block .gallery .item .lightbox{height:5.5vw;margin:3px;display:block;background-repeat:no-repeat;background-size:cover;}
                        .s-o_f .image_block .gallery .item img{width:100%;height:100%;object-fit:cover}

                        @media (min-width: 1400px) {
                            .s-o_f .image_block .gallery .item .lightbox{height: 65px}
                        }
                        @media (max-width: 992px) {
                            .s-o_f .blur{width: 103%;height: 110%;margin: -10px -10px}
                            .s-o_f .image_block .gallery .item .lightbox{height: 7.5vw}
                        }
                        @media (max-width: 575px){
                            .s-o_f .pheader{display:table}
                            .s-o_f .pheader .title{width:100%;padding:0}
                            .s-o_f .pheader .price{width:100%;text-align:left}
                            .s-o_f .image_block .image{height:67vw}
                            .s-o_f .image_block .gallery .item {width:16.666%}
                            .s-o_f .image_block .gallery .item .lightbox {height:9.5vw}
                        }
                    </style>
                    <div class="s-o_f">
                        <div class="pheader">
                            <div class="title"><h1>{{$product->title or ''}}</h1><div class="data">id проекта: {{$product->id or ''}}, опубликовано {{Resize::date($product->created_at)}}</div>@if(Auth::user() !== null)<div class="data"><a target="_blank" href="/admin/proyekty/{{$product->id or ''}}/edit?id={{$product->id or ''}}">Редактировать</a></div>@endif</div>
                            <div class="price">@if($product->coast != null)<span>{{$product->coast or ''}}</span> руб. @else <span style="font-size: 17px;">Цена не указана</span> @endif</div>
                        </div>
                        <div class="pbody">
                            <div class="image_block tz-gallery">
                                <div class="image">
                                    <a class="lightbox first-image" href="/storage/app/{{$product->images->images or ''}}">
                                        <div class="blur" style="background-image: url(/storage/app/{{Resize::ResizeImage($product->images, '315x315')}});"></div>
                                        <img id="ImageProduct" src="/storage/app/{{Resize::ResizeImage($product->images, '485x315')}}" alt="КУАУТЛ">
                                    </a>
                                </div>
                                <div class="gallery">
                                    <?php $i = 0; ?>
                                    @forelse($product->imageses as $img)
                                        @if($i >= 1)
                                            <div class="item"><a class="lightbox" href="/storage/app/{{$img->images or ''}}" style="background-image:url(/storage/app/{{Resize::ResizeImage($img, '75x50')}});"></a></div>
                                        @endif
                                        <?php $i++; ?>
                                    @empty
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="c">
                        <style>
                            .b_act{margin: 10px 15px;display: flex}
                            .b_act .btn_question{background: #269ffd;border: 1px solid #e6e6e6;color: #fff;padding: 9px 18px;font-size: 18px;line-height: 18px;text-align: center;border-radius: 3px;min-width: 260px;}
                            .b_act .act_info{font-size: 14px;line-height: 16px;margin: 3px 0 3px 25px;color: #888;}
                        </style>
                        <div class="b_act">
                            <div class="btn_question" id="needcall">Задать вопрос эксперту</div>
                            <div class="act_info">Оформите заявку на сайте, мы свяжемся с вами в ближайшее время и ответим на все интересующие вопросы</div>
                        </div>
                    </div>
                    <div class="c">
                        <div class="content_title">
                            <span>Характеристики</span>
                        </div>
                        <div class="description">
                            <table style="width: 100%">
                                <tbody>
                                <tr><td>Общая площадь</td><td>{{$product->goods_doma->area or ''}} м2</td></tr>
                                <tr><td>Этажность</td><td>{{$product->goods_doma->floors or ''}}</td></tr>
                                <tr><td>Спальни</td><td>{{$product->goods_doma->bedroom or ''}}</td></tr>
                                <tr><td>Габариты</td><td>{{$product->goods_doma->dimensions or ''}}</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <style>
                        .packs{width:100%;display:-ms-flexbox;display:-webkit-flex;display:-moz-flex;display:-ms-flex;display:flex;-webkit-flex-wrap:wrap;-moz-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;padding:0;margin:0;list-style-type:none}
                        .packs li{padding:10px;width:33.333%;display:flex;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal;background:#fff}
                        .packs li .name{width:100%;font-size:15px;line-height:17px;position:relative;padding-left:15px}
                        .packs li .name:before{content:"";width:8px;height:8px;background:#469f22;display:inline-block;vertical-align:top;position:absolute;left:0;top:5px;-webkit-border-radius:10px;-moz-border-radius:10px;border-radius:10px}
                        .packs li .value{text-align:right;width:100%;font-size:15px;font-weight:600;color:#469f22}
                    </style>
                    <div class="c grey" style="background: none">
                        <div class="content_title" style="background: #fff">
                            <h2>Комплектации и цены</h2>
                        </div>
                        <ul class="packs">
                            @if($product->goods_doma != null && count($product->goods_doma->area)>0)
                                <li><span class="name">Брус</span><span class="value">{{number_format($goods_doma_price->brus * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Профилированный брус</span><span class="value">{{number_format($goods_doma_price->prof_brus * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Оцилиндрованное бревно</span><span class="value">{{number_format($goods_doma_price->ocilindr_brevno * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Рубленое бревно</span><span class="value">{{number_format($goods_doma_price->rubl_brevno * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Клееный брус</span><span class="value">{{number_format($goods_doma_price->kleen_brus * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Канадская технология</span><span class="value">{{number_format($goods_doma_price->kanad_tech * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Немецкая технология</span><span class="value">{{number_format($goods_doma_price->nem_tech * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Пеноблок</span><span class="value">{{number_format($goods_doma_price->penoblock * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Керамзитоблок</span><span class="value">{{number_format($goods_doma_price->keramzitblock * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Газоблок</span><span class="value">{{number_format($goods_doma_price->gazoblock * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">SIP-панел</span><span class="value">{{number_format($goods_doma_price->sip_panel * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Двойной брус</span><span class="value">{{number_format($goods_doma_price->dvoinoy_brus * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Лафет</span><span class="value">{{number_format($goods_doma_price->lafet * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Кирпич</span><span class="value">{{number_format($goods_doma_price->kirpich * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Каркас</span><span class="value">{{number_format($goods_doma_price->karkas * $product->goods_doma->area)}} -</span></li>
                                <li><span class="name">Арболит</span><span class="value">{{number_format($goods_doma_price->arbolit * $product->goods_doma->area)}} -</span></li>
                            @else
                                <li><span class="name">Брус</span><span class="value">{{$goods_doma_price->brus}} руб./м2</span></li>
                                <li><span class="name">Профилированный брус</span><span class="value">{{$goods_doma_price->prof_brus}} руб./м2</span></li>
                                <li><span class="name">Оцилиндрованное бревно</span><span class="value">{{$goods_doma_price->ocilindr_brevno}} руб./м2</span></li>
                                <li><span class="name">Рубленое бревно</span><span class="value">{{$goods_doma_price->rubl_brevno}} руб./м2</span></li>
                                <li><span class="name">Клееный брус</span><span class="value">{{$goods_doma_price->kleen_brus}} руб./м2</span></li>
                                <li><span class="name">Канадская технология</span><span class="value">{{$goods_doma_price->kanad_tech}} руб./м2</span></li>
                                <li><span class="name">Немецкая технология</span><span class="value">{{$goods_doma_price->nem_tech}} руб./м2</span></li>
                                <li><span class="name">Пеноблок</span><span class="value">{{$goods_doma_price->penoblock}} руб./м2</span></li>
                                <li><span class="name">Керамзитоблок</span><span class="value">{{$goods_doma_price->keramzitblock}} руб./м2</span></li>
                                <li><span class="name">Газоблок</span><span class="value">{{$goods_doma_price->gazoblock}} руб./м2</span></li>
                                <li><span class="name">SIP-панел</span><span class="value">{{$goods_doma_price->sip_panel}} руб./м2</span></li>
                                <li><span class="name">Двойной брус</span><span class="value">{{$goods_doma_price->dvoinoy_brus}} руб./м2</span></li>
                                <li><span class="name">Лафет</span><span class="value">{{$goods_doma_price->lafet}} руб./м2</span></li>
                                <li><span class="name">Кирпич</span><span class="value">{{$goods_doma_price->kirpich}} руб./м2</span></li>
                                <li><span class="name">Каркас</span><span class="value">{{$goods_doma_price->karkas}} руб./м2</span></li>
                                <li><span class="name">Арболит</span><span class="value">{{$goods_doma_price->arbolit}} руб./м2</span></li>
                            @endif

                        </ul>
                    </div>
                    <div class="c">
                        <div class="content_title"><span>Описание товара</span></div>
                        <div class="description">{!! $product->short_description or 'Описание отсутствует' !!}</div>
                    </div>
                    <style>
                        .c.only-mobile{display:none}
                        .c .m_only_m{width:100%;display:flex;padding:10px}
                        .c .m_only_m .info_l_company{width:100%}
                        .person{display:flex}
                        .c .m_only_m .info_l_company .image{float:left;width:55px;min-width:55px;height:55px}
                        .c .m_only_m .info_l_company .image img{width:55px;height:55px;object-fit:cover;border-radius:50%}
                        .info_person{width:100%;margin:auto 10px}
                        .info_person .name{font-weight:600;line-height:18px;font-size:16px}
                        .info_person .position{color:#888;font-size:15px;line-height:17px}
                        .c .m_only_m .info_l_company .info{display:inline-block;width:100%;margin-top:20px}
                        .c .m_only_m .info_l_company .info .name{line-height:18px;margin-bottom:5px;font-weight:600;font-size:16px}
                        .c .m_only_m .info_l_company .info .position{line-height:18px;font-size:15px;margin-bottom:10px}
                        .c .m_only_m .info_r_company{display:inline-block;width:60%}
                        .c .m_only_m .data{width:49%;font-size:15px;text-align:center;float:left;display:block;border:1px solid #e6e6e6;margin:.5%;padding:5px;background:#f3f3f3}
                        .c .m_only_m .data.phone:before{font-family:FontAwesome;font-size:15px;margin-right:10px;vertical-align:middle;color:#269ffd;content:"\f095";width:20px;min-width:20px;text-align:center;display:inline-block}
                        .c .m_only_m .data.email:before{font-family:FontAwesome;font-size:15px;margin-right:10px;vertical-align:middle;color:#269ffd;content:"\f0e0";width:20px;min-width:20px;text-align:center;display:inline-block}
                        .c .m_only_m .data.address:before{font-family:FontAwesome;font-size:15px;margin-right:10px;vertical-align:middle;color:#269ffd;content:"\f041";width:20px;min-width:20px;text-align:center;display:inline-block}
                        .c .m_only_m .data.timework:before{font-family:FontAwesome;font-size:15px;margin-right:10px;vertical-align:middle;color:#269ffd;content:"\f017";width:20px;min-width:20px;text-align:center;display:inline-block}
                        @media (max-width: 992px) {
                            .c.only-mobile{display:block}
                        }
                        @media (max-width: 575px) {
                            .c .m_only_m .data{width:100%;margin:.5% 0}
                        }
                    </style>
                    <div class="c only-mobile">
                        <div class="content_title"><span>Контактная информация</span></div>
                        <div class="m_only_m">
                            <div class="info_l_company">
                                <div class="person">
                                    <div class="image"><img src="/storage/app/{{Resize::ResizeImage($manager, '55x55')}}" alt="{{$manager->name or 'Имя'}} {{$manager->lastname or 'Фамалия'}}"></div>
                                    <div class="info_person">
                                        <div class="name">{{$manager->name or 'Имя'}} {{$manager->lastname or 'Фамилия'}}</div>
                                        <div class="position">{{$manager->job or 'Должность'}}</div>
                                    </div>
                                </div>

                                <div class="info">
                                    <a href="tel:{{$manager->tel or '+7 (900) 000-00-00'}}"><div class="data phone"><span>{{$manager->tel or '+7 (900) 000-00-00'}}</span></div></a>
                                    <div class="data email"><span>{{$manager->email or ''}}</span></div>
                                    <div class="data address">{{$company->address or 'г. Город, ул. Улица 1, оф. 1'}}</div>
                                    <div class="data timework">@if($manager != null && $manager->timeJob != null){{$manager->timeJob or 'Время работы'}}@else{{$company->timeJob or 'Время работы'}}@endif</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style>
                        .portfolio{display:table;width: 100%;}
                        .portfolio .i{width:24.333%;float:left;margin:.333%}
                        .portfolio .image{height:16vh;width:100%}
                        .portfolio .i img{width:100%;height:100%;object-fit:cover}
                        @media (max-width: 778px) {
                            .portfolio .i{width:32.333%}
                        }
                        @media (max-width: 575px) {
                            .portfolio .i{width:49%}
                        }
                    </style>
                    <div class="c">
                        <div class="content_title"><h2>Фото наших работ:</h2></div>
                        <div class="portfolio">
                            @forelse($portfolio as $p)
                                <div class="i image_modal" slug="{{$p->id}}"><div class="image"><img src="/storage/app/{{Resize::ResizeImage($p->image_one, '190x125')}}" alt=""></div></div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                    <script type="text/javascript">
                        // .image - отвечает за класс на который надо нажимать, чтоб сработал ajax
                        $('.image_modal').click(function(){
                            var id = $(this).attr('slug');
                            $.get('/portfolio/view/image/description/'+id, function(data){
                                $('.modal_portfolio').html(data);
                            });

                            var url = '?object=' + id;
                            history.pushState(null, null, url);

                            @if($favicon->metrika_short != null)
                            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                                m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
                            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
                            ym({{$favicon->metrika_short}}, 'hit', url);
                            @endif
                        });
                    </script>
                    <div class="modal_portfolio"></div>
                    <style>
                        .reviews{min-width:100%;display:table}
                        .reviews .i{padding:15px;background:#fff;border:1px solid #e6e6e6;width:100%;margin:.25em 0;display:inline-block}
                        .reviews .i.open .reviews_button{display:none}
                        .reviews .i .write_rev{display:none}
                        .reviews .i.open .write_rev{display:block}
                        .reviews .i.open{padding:15px!important;border:1px solid #e6e6e6!important}
                        .rating{color:#fdcc63;font-size:12px;line-height:12px;margin-bottom:6px}
                        .reviews .user{display:flex}
                        .reviews .user .user_image{width:50px;height:50px;border-radius:50%;overflow:hidden;min-width:50px}
                        .reviews .user img{width:100%;height:100%;object-fit:cover}
                        .user_name{width:100%;padding-left:7px;margin:auto}
                        .reviews .user .name{font-weight:600;font-size:15px;line-height:17px}
                        .reviews .user .date{color:#888;font-size:14px}
                        .i_desc{font-size:15px;line-height:22px;max-height:85px;position:relative;overflow:hidden}
                        .toggle-btn{font-size:15px;line-height:17px;margin-top:10px;display:table}
                        .text-open{overflow:visible;height:auto;max-height:none}
                        .text-open + .toggle-btn{display:none}
                        .i_gallery{display:table;width:100%;margin-top:10px}
                        .i_gallery .image{width:31.333%;height:50px;margin:1%;float:left}
                        .reviews .i_gallery img{width:100%;height:100%;object-fit:cover}
                        @media (max-width: 778px) {
                            .reviews .i{width:49%}
                            .i_gallery .image{height:7.5vh;width:25%}
                        }
                        @media (max-width: 575px) {
                            .reviews .i{width:99%}
                            .i_gallery .image{height:6.5vh;width:20%;margin:.5%}
                        }
                        .masonry{-moz-column-count:3;-webkit-column-count:3;column-count:3;-webkit-column-gap:.5em;-moz-column-gap:.5em;column-gap:.5em;padding:0;font-size:.85em}
                        @media only screen and (max-width: 320px) {
                            .masonry{-moz-column-count:1;-webkit-column-count:1;column-count:1}
                        }
                        @media only screen and (min-width: 321px) and (max-width: 768px) {
                            .masonry{-moz-column-count:2;-webkit-column-count:2;column-count:2}
                        }
                        @media only screen and (min-width: 769px) {
                            .masonry{-moz-column-count:3;-webkit-column-count:3;column-count:3}
                        }
                        .reviews_button{display:inline-block;vertical-align:top;padding:15px;font-size:13px;line-height:14px;width:100%;text-align:center;text-transform:uppercase;position:relative;color:#fdcc63!important;font-weight:600;border:1px solid #f7e8a8}
                        .reviews_input{width:100%;border:1px solid #e6e6e6;padding:7px 15px;margin-bottom:5px}
                        .reviews_input_file{margin-bottom:15px}
                        .reviews_textarea{text-align:left;-webkit-box-sizing:border-box;box-sizing:border-box;overflow:hidden;border:1px solid #e6e6e6;height: 80px;padding:15px;margin-bottom: 10px;width:100%;resize:none;outline:none;font:14px/17px Helvetica,Arial,sans-serif}
                        .reviews_textarea_button{position:relative;outline:none;border:none;padding:7px 20px;min-width:150px;min-height:32px;border-radius:3px;background-color:#007bab;font-size:14px;line-height:16px;cursor:pointer;color:#fff}
                    </style>
                    <div class="c grey" style="background: none">
                        <div class="content_title" style="background: #fff"><h2>Отзывы наших заказчиков:</h2></div>
                        <div class="reviews ">
                            <div class="masonry">
                                <div class="i" style="padding: 0;border:none" id="ddd3">
                                    <a href="javascript:void(0);" class="reviews_button" onclick="reviewsFunction()">Оставить отзыв</a>
                                    <!--                    <div class="fdf" id="ddd3"></div>-->
                                    <div class="write_rev">
                                      <form action="/feedback/store/{{$product->id or ''}}" method="post" enctype="multipart/form-data">
                          {{csrf_field()}}
                      <input type="hidden" name="type" value="goods">
                      <input type="text" name="name" class="reviews_input" placeholder="Имя Фамилия" value="">
                      <label for="file">Ваша фотография:</label>
                      <input type="file" name="ava" class="reviews_input_file" placeholder="Текст отзыва..." value="" style="border: 0;padding-left: 0;width: 100%">
                      <textarea name="description" id="" class="reviews_textarea" placeholder="Текст отзыва..."></textarea>
                      <div id="example3"></div>
                        <div class="submitReview">
                            <button class="reviews_textarea_button" style="background-color:#ddd" disabled>Отправить</button>
                        </div>
                      </form>
                                    </div>
                                </div>
                                <script>
                                    $(".toggle-btn").click(function(e) {e.preventDefault();$(this).prev().toggleClass("text-open")})
                                </script>
                                @forelse($reviews as $r)
                                    <div class="i">
                                        <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                                        <div class="user">
                                            <div class="user_image"><img src="/storage/app/{{Resize::ResizeImage($r, '45x45')}}" alt=""></div>
                                            <div class="user_name">
                                                <div class="name">{{$r->name}}</div>
                                                <div class="date">Добавлено: <span>{{Resize::date($r->updated_at)}}</span></div>
                                            </div>
                                        </div>
                                        <div class="i_desc">{!! $r->description !!}</div>
                                        <a href="" class="toggle-btn">Читать полностью</a>
                                        @if($r->imageses != null)
                                            <div class="i_gallery tz-gallery">
                                                @forelse($r->imageses as $img)
                                                    <div class="image">
                                                        <a href="/storage/app/{{$img->images}}" class="lightbox">
                                                            <img src="/storage/app/{{Resize::ResizeImage($img, '75x50')}}" alt="">
                                                        </a>
                                                    </div>
                                                @empty
                                                @endforelse
                                            </div>
                                        @endif
                                    </div>
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <script>
                        $(".toggle-btn").click(function(e) {e.preventDefault();$(this).prev().toggleClass("text-open")})
                    </script>
                    <div class="c">
                        <div class="content_title"><h3>Часто задаваемые вопросы:</h3></div>
                        <div class="description"></div>
                    </div>
                    <div class="c grey" style="background: none">
                        <div class="content_title" style="background: #fff"><h2>Проекты похожие на: {{$product->title or ''}}</h2></div>
                        <div class="c-gd">
                            <div class="c-gd_l c-gd_l-md">
                                @forelse($similar as $g)
                                    <div class="item">
                                        <a href="/proyekty/{{$category_parent->id}}-{{$category_parent->slug}}/{{$g->id}}-{{$g->slug}}">
                                            <div class="image"><div class="img_dark"></div><img src="/storage/app/{{Resize::ResizeImage($g->images, '190x190')}}" alt=""></div>
                                            <div class="info">
                                                <div class="name"><span>{{$g->title or ''}}</span></div>
                                                <div class="i_cat">{{$g->product_url_cat->title}}</div>
                                                <div class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                                                <div class="prices">@if($g->coast != null){{$g->coast or ''}} - @else Цена не указана @endif</div>
                                            </div>
                                            <div class="act">
                                                <div class="area">@if($g->goods_doma != null) {{$g->goods_doma->area}} @else ... @endif<span>м2</span></div>
                                                <div class="act_button">Смотреть <i class="fa fa-angle-double-right" aria-hidden="true"></i></div>
                                            </div>
                                        </a>
                                    </div>
                                @empty
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="manager">
                    <div class="title">Контактная информация</div>
                    <div class="block">
                        <div class="person">
                            <div class="image" style="background-image: url(/storage/app/{{Resize::ResizeImage($manager, '55x55')}});"></div>
                            <div class="info"><div class="name">{{$manager->name or 'Имя'}} {{$manager->lastname or 'Фамалия'}}</div><div class="position">{{$manager->job or 'Должность'}}</div></div>
                        </div>
                        <div class="data phone"><span>{{$manager->tel or '+7 (900) 000-00-00'}}</span></div>
                        <div class="data email"><a target="_blank" href="/contact"><span>Отправить сообщение</span></a></div>
                        <div class="data local"><div class="address"><span>г. {{$company->address or 'Заполните адрес'}}</span><br><a class="open_map">Показать на карте</a></div></div>
                        <div class="data timework"><span>Время работы: {{$manager->timeJob or 'не указано'}}</span></div>
                    </div>
                </div>
                <div class="map">
                    <div class="title"><span>Карта</span></div>
                    <a class="open_map"><div class="back-map"><div class="block_map_b"><button class="map_b">Показать карту</button></div><div class="map-view" style="background-image: url(/public/site/img/map-load.jpg);"></div></div></a>
                </div>
                @if(count($documents) != null)
                    <div class="documents">
                        <div class="title">Документы</div>
                        <div class="documents_list">
                            @forelse($documents as $d)
                                <div class="item">
                                    <div class="file file-{{$d->type}}"></div>
                                    <div class="info">
                                        <div class="name"><a href="/storage/app/{{$d->doc}}/">{{$d->title}}</a></div>
                                        <div class="size">{{$d->size}} Мб, @if($d->updated_at == null) добавлено: {{$d->created_at}} @else обновлено: {{$d->updated_at}} @endif</div>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
<link rel="stylesheet" href="/public/site/open-image/open-image.css">
<script src="/public/site/open-image/open-image.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>
<div class="modal"></div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.open_map').click(function(){$.get('{{route('modal.open.map')}}', function(data){$('.modal').html(data);})});
    });
</script>
<script type="text/javascript">
    document.getElementById('toorder').onclick = function() {$.get('{{route('take.modal.product', ['id' => $product->id])}}', function(data){$('.modal').html(data);})};
    document.getElementById('needcall').onclick = function() {$.get('{{route('take.modal.needcall', ['id' => $product->id])}}', function(data){$('.modal').html(data);})};
</script>
<script type="text/javascript">
      var verifyCallback = function(response) {
        $('.submitReview').html('<input type="submit" value="Отправить" class="reviews_textarea_button">');
      };
      var onloadCallback = function() {
        grecaptcha.render('example3', {
          'sitekey' : '{{Resize::recaptcha()}}',
          'callback' : verifyCallback,
        });
      };
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>