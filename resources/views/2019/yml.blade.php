
<yml_catalog date="{{date('Y-m-d H:i')}}">
<shop>
<name>{{$title->name}}</name>
<company>{{$title->name}}</company>
<url>http://{{$title->title}}/</url>
<currencies>
<currency id="RUR" rate="1" plus="0"/>
</currencies>
<categories>
    @forelse($categories as $category)
<category id="{{$category->id}}" @if($category->parent_id !== null) parentId="{{$category->parent_id}}" @endif>{{$category->title}}</category>
    @empty
    @endforelse
</categories>
<local_delivery_cost>0</local_delivery_cost>
<offers>
    <?php $i=1000?>
    @forelse($categories as $category)
    @forelse($category->goods as $goods)
    @if($category->type == 0)
 <?php $type = 'product'?>
 @elseif($category->type == 1)
 <?php $type = 'services'?>
  @elseif($category->type == 4)
 <?php $type = 'stroitelstvo'?>
  @elseif($category->type == 5)
 <?php $type = 'bani'?>
  @elseif($category->type == 6)
 <?php $type = 'proyekty'?>
  @elseif($category->type == 7)
 <?php $type = 'izgotovleniye'?>
 @endif
<offer id="{{$goods->id}}" available="true">
    <url>http://{{$title->title}}/{{$type}}/{{$category->id}}-{{$category->slug}}/{{$goods->id}}-{{$goods->slug}}</url>
    <price>{{$i}}</price>
    <currencyId>RUB</currencyId>
    <categoryId>{{$goods->category}}</categoryId>
    <delivery>true</delivery>
    <delivery-options>{{$goods->delivery}}</delivery-options>
    <name>{{$goods->title}} </name>
    <description>{{$goods->short_description}}</description>
    <country_of_origin>Россия</country_of_origin>
</offer>
<?php $i++;?>
    @empty
    @endforelse
    @empty
    @endforelse

</offers>
</shop>
</yml_catalog>