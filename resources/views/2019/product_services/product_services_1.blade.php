</div>
<style>
    .s-o_f{display:table;width: 100%;background:#fff;margin-bottom:40px;   -webkit-box-shadow: -1px 0 0 0 #e6e6e6 inset, 0 -1px 0 0 #e6e6e6 inset, -1px -1px 0 0 #e6e6e6 inset, -1px 0 0 0 #e6e6e6, 0 -1px 0 0 #e6e6e6;box-shadow: -1px 0 0 0 #e6e6e6 inset, 0 -1px 0 0 #e6e6e6 inset, -1px -1px 0 0 #e6e6e6, -1px 0 0 0 #e6e6e6, 0 -1px 0 0 #e6e6e6;-webkit-box-orient: vertical;-webkit-box-direction: normal;}
    .s-o_f .pheader{padding: 15px 15px 5px;width:100%;display:flex}
    .s-o_f .pheader .title{display:inline-block;width:60%;padding-right:15px}
    .s-o_f .pheader .title h1{font-weight: 600;font-size: 20px;line-height: 20px;margin: 0;}
    .s-o_f .pheader .title .data{font-size:14px;color:#888;line-height: 14px;margin-top: 5px;}
    .s-o_f .pheader .price{display:inline-block;width:40%;font-size:14px;text-align:right}
    .s-o_f .pheader .price span{font-weight:600;font-size:20px;line-height: 20px;color:#222}
    .s-o_f .image_block .image{width:100%;height:346px;padding:10px;border-bottom:1px solid #e6e6e6}
    .s-o_f .first-image{width:100%;height:100%;position:relative;overflow:hidden;display:table}
    .s-o_f .blur{width:102%;height:103%;margin:-5px -9px;object-fit:contain;background-position: left;position:absolute;background-repeat:no-repeat;background-size:cover;-webkit-filter:blur(5px);-moz-filter:blur(5px);filter:blur(5px);filter:url(/public/site/img/blur.svg#blur)}
    .s-o_f .image_block .image img{width:100%;height:100%;object-fit:contain;position:relative;z-index:1}
    .s-o_f .image_block .gallery{width:100%;display:table;padding:7px}
    .s-o_f .image_block .gallery .item{width:12.5%;float:left}
    .s-o_f .image_block .gallery .item .lightbox{height:5.5vw;margin:3px;display:block;background-repeat:no-repeat;background-size:cover;}
    .s-o_f .image_block .gallery .item img{width:100%;height:100%;object-fit:cover}

    @media (min-width: 1400px) {
        .s-o_f .image_block .gallery .item .lightbox{height: 65px}
    }
    @media (max-width: 992px) {
        .s-o_f .blur{width: 103%;height: 110%;margin: -10px -10px}
        .s-o_f .image_block .gallery .item .lightbox{height: 7.5vw}
    }
    @media (max-width: 575px){
        .s-o_f .pheader{display:table}
        .s-o_f .pheader .title{width:100%;padding:0}
        .s-o_f .pheader .price{width:100%;text-align:left}
        .s-o_f .image_block .image{height:67vw}
        .s-o_f .image_block .gallery .item {width:16.666%}
        .s-o_f .image_block .gallery .item .lightbox {height:9.5vw}
    }
</style>
<div class="col-9">
    <div class="product">
        <div class="s-o_f">
            <div class="pheader">
                <div class="title"><h1>{{$product->title or ''}}</h1><div class="data">id услуги: {{$product->id or ''}}, опубликовано {{Resize::date($product->created_at)}} в 22:02</div></div>
                <div class="price">от<span> {{$product->coast or ''}} </span>руб.</div>
            </div>
            <?php //dd($product->images); ?>
            <div class="pbody">
                <div class="image_block tz-gallery">
                    <div class="image"><a class="lightbox first-image" href="/storage/app/{{$product->images->images or ''}}"><div class="blur" style="background-image: url(/storage/app/{{$product->images->images or ''}});"></div><img id="ImageProduct{{$product->images->id or ''}}" src="/storage/app/{{Resize::ResizeImage($product->images, '40x30')}}" alt="{{$product->title or ''}}"></a></div>
                    <div class="gallery">
                        <?php $i = 0; ?>
                        @forelse($product->imageses as $img)
                            @if($i>0)
                        <div class="item"><a class="lightbox" href="/storage/app/{{$img->images or ''}}" style="background-image:url(/storage/app/{{$img->images or ''}});"></a></div>
                                @endif
                            @empty
                            @endforelse
                    </div>
                </div>
            </div>
        </div>
        <div class="c">
            <div class="content_title">
                <h2>Описание услуги</h2>
            </div>
            <div class="description">
                {!! $product->short_description or '' !!}
            </div>
        </div>
        <script type="text/javascript">$(document).ready(function(){$('#ImageProduct{{$product->images->id or ''}}').attr('src', '/storage/app/{{Resize::ResizeImage($product->images, "485x325")}}');});</script>
