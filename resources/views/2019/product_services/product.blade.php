<style>.b_nav li{max-width:25%}.p-o_l{width:100%;display:flex;margin-bottom:40px}.p-o_l .info .title h1{font-weight:600;font-size:20px;line-height:20px;margin:0}.p-o_l .img_b{width:40%;display:block;float:left;background:#fff;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}.p-o_l .img_b .image{width:100%;height:28.5vw;padding:10px;border-bottom:1px solid #e6e6e6}.p-o_l .img_dark{opacity:0;background-color:rgba(0,0,0,.2);-ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=0);-webkit-transition:opacity .8s cubic-bezier(.19,1,.22,1);transition:opacity .8s cubic-bezier(.19,1,.22,1);position:absolute;display:block;width:100%;height:100%;z-index:2}.p-o_l .lightbox:hover .img_dark{opacity:1}.p-o_l .img_b .first-image{width:100%;height:100%;position:relative;overflow:hidden;display:table}.p-o_l .img_b .image img{width:100%;height:100%;object-fit:cover}.p-o_l .img_b .gallery{width:100%;display:table;padding:7px}.p-o_l .img_b .gallery .item{width:20%;float:left}.p-o_l .img_b .gallery .item .lightbox{height:5vw;margin:3px;display:block;background-repeat:no-repeat;background-size:cover}.p-o_l .info{width:60%;float:left;background:#fff;position:relative;padding-bottom:65px;-webkit-box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6 inset,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;box-shadow:-1px 0 0 0 #e6e6e6 inset,0 -1px 0 0 #e6e6e6 inset,-1px -1px 0 0 #e6e6e6,-1px 0 0 0 #e6e6e6,0 -1px 0 0 #e6e6e6;-webkit-box-orient:vertical;-webkit-box-direction:normal}.p-o_l .info .title{padding:10px 15px;width:100%;border-bottom:1px solid #e6e6e6}.p-o_l .info .title .data{font-size:14px;line-height:14px;margin-top:5px;color:#888}.p-o_l .info .price{width:100%;padding:20px 15px;display:table;border-bottom:1px solid #e6e6e6;font-size:14px}.p-o_l .info .price span{font-weight:600;font-size:20px;line-height:20px;color:#222}.p-o_l .info .additionally{padding:15px;width:100%}.p-o_l .info .additionally .item{margin-bottom:15px;color:#888;font-size:14px;line-height:16px}.p-o_l .info .additionally .item span{font-weight:600;color:#222}.p-o_l .info .connection{position:absolute;bottom:0;padding:15px;display:flex}.p-o_l .info .connection .to-order{background:#269ffd;border:1px solid #269ffd;color:#fff;padding:9px 8px;font-size:18px;line-height:18px;width:125px;text-align:center;border-radius:3px}.p-o_l .info .connection .write{background:#fff;border:1px solid #269ffd;color:#269ffd;padding:9px 8px;font-size:18px;line-height:18px;width:160px;text-align:center;border-radius:3px}@media (min-width: 1400px){.p-o_l .img_b .image{height:337px}.p-o_l .img_b .gallery .item .lightbox{height:58px}}@media (max-width: 992px){.p-o_l .img_b .image{height:39vw}.p-o_l .img_b .gallery .item .lightbox{height:7vw}}@media (max-width: 575px){.p-o_l{display:table}.p-o_l .img_b{width:100%}.p-o_l .img_b .image{height:97vw}.p-o_l .img_b .gallery .item{width:16.666%}.p-o_l .img_b .gallery .item .lightbox{height:14vw}.p-o_l .info{width:100%}.p-o_l .info .title{padding:20px 15px}.p-o_l .info .connection{width:100%}.p-o_l .info .connection a{width:50%}.p-o_l .info .connection .to-order{width:100%;padding:5px 8px;font-size:20px}.p-o_l .info .connection .write{padding:5px 8px;width:100%;font-size:20px}}.p-o_l .connection button{padding:0;border:0;background:none}.p-o_l .info .additionally .item.switch-on{margin-bottom:10px}.switch-list{list-style-type:none;margin:0;display:table;padding:0;width:100%;margin-bottom:10px}.switch-list .switch{display:block;float:left;margin-right:5px;margin-bottom:5px}.switch-list .tooltip{position:relative;opacity:1;border:1px solid #ddd}.switch-list .tooltip.active{border:1px solid #269ffd}.switch-list .button-text{background:#fff;padding:7px 10px;cursor:pointer;border:none}.switch-list .button-text:hover{background:#eee}.switch-list .tooltip.active .button-text{background:#f0f8ff;font-weight:600}.switch-list .button-text span{font-size:14px;line-height:14px;display:block}</style>

<section class="i80">
    <div class="page_product_2">
        <div class="row">
            <div class="col-9">
                <div class="p-o_l">
                    <div class="img_b tz-gallery">
                        <div class="image"><a class="lightbox first-image" href="/storage/app/{{$product->images->images or ''}}"><div class="img_dark"></div><img src="/storage/app/{{Resize::ResizeImage($product->images, '319x316')}}" alt=""></a></div>
                        <div class="gallery">
                            <?php $i = 0; ?>

                            @forelse($product->imageses as $img)
                                <div class="item"><a class="lightbox" href="/storage/app/{{$img->images or ''}}" style="background-image:url(/storage/app/{{Resize::ResizeImage($img, '59x58')}});"></a></div>
                                @if($i >10)
                                    <?php break; ?>
                                @endif
                            @empty
                            @endforelse

                        </div>
                    </div>
                    <div class="info">
                        <div class="title">
                            <h1>{{$product->title or ''}}</h1>
                            <div class="data">id товара: {{$product->id}}, опубликовано {{Resize::date($product->created_at)}}</div>
                        </div>
                        <div class="price">Цена: <span> {{$product->coast or ''}}</span> руб.</div>
                        <div class="additionally">

                            <div class="item">Минимальный заказ: <span>1 шт.</span></div>
                            <div class="item">Оплата: <span>Наличный расчет / Безналичный расчет</span></div>
                            <div class="item">Доставка: <span>Доставка автопарком компании</span></div>
                        </div>
                        <div class="connection">
                            <button style="margin-right: 15px"><div class="to-order" id="toorder">Заказать</div></button>
                            <button><div class="write" id="needcall">Перезвонить?</div></button>
                        </div>
                    </div>
                </div>
                <div class="c">
                    <div class="content_title"> 
                        <h2>Описание товара</h2>
                    </div>
                    <div class="description">
                        {!! $product->short_description or '' !!}
                    </div>
                </div>

                <script type="text/javascript">
                    document.getElementById('toorder').onclick = function() {
                        $.get('{{route('take.modal.product', ['id' => $product->id])}}', function(data){
                            $('.modal').html(data);
                        })
                    }

                    document.getElementById('needcall').onclick = function() {
                        $.get('{{route('take.modal.needcall', ['id' => $product->id])}}', function(data){
                            $('.modal').html(data);
                        })
                    }
                </script>

