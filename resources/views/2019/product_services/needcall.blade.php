<div id="modal_form_needcall" class="modal-ui-wrapper" style="background: rgba(0, 0, 0, 0.565);">
    <div class="modal-ui-modal" style="background: rgb(255, 255, 255);">
        <div class="modal-ui-content">
            <div class="title"><h2>Перезвоним!</h2></div>
            <div class="ui-b">
                <div class="ui-b-form" style="padding: 20px">
                    <div class="form-group">
                        <div class="label">Контактный телефон:</div>
                        <div class="g-input"><input type="text" placeholder="+7 (900) 000-00-00" name="tel"></div>
                    </div>
                    <div class="form-group">
                        <div class="label">Ваше сообщение:</div>
                        <div class="g-input"><textarea maxlength="500" placeholder="Оставьте комментарий к заказу или задайте интересующий вопрос" id="message"></textarea></div>
                    </div>
                </div>
            </div>
            <div class="ui-f"><button id="sendOrder">Отправить</button></div>
        </div>
        <div id="modal_close_needcall" class="modal-ui-close"><span>×</span></div>
    </div>
</div>
<input style="display: none" type="text" class="id" value="{{$id}}">
<script type="text/javascript">
$('#sendOrder').click(function(){
    var tel = $('input[name="tel"]').val();
    var message = $('#message').val();
    var id = $('.id').val();
    $.post('{{route('create.order')}}', {tel:tel, message:message, product:id}, function(data){
      alert('Ваше сообщение успешно отправлено! Ождайте обратного звонка.')
    });
});
$(document).ready(function() {
    $(document).ready(function() {$('#needcall').click( function(event){event.preventDefault();$('#overlay').fadeIn(50, function(){$('#modal_form_needcall').css('display', 'flex').animate({opacity: 1}, 50);});});
        $('#modal_close_needcall, #overlay').click( function(){$('#modal_form_needcall').animate({opacity: 0}, 100, function(){$(this).css('display', 'none');$('#overlay').fadeOut(100);});});
    });
});
</script>
