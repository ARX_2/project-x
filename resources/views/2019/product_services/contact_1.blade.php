<div class="c no-mobile">
    <div class="content_title"><span>Контактная информация</span></div>
    <div class="manager_in_description">
        <div class="person">
            <div class="image"><img src="/storage/app/{{Resize::ResizeImage($userOnSite, '55x55')}}" alt="{{$userOnSite->name or 'Имя'}} {{$userOnSite->lastname or 'Фамалия'}}"></div>
            <div class="info">
                <div class="name">{{$userOnSite->name or 'Имя Фамилия'}}</div>
                <div class="position">{{$userOnSite->job or 'Должность'}}</div>
                <div class="data phone"><span>{{$userOnSite->tel or '+7 (900) 000-00-00'}}</span></div>
                <div class="data email"><a href="/contact/"><span>Отправить сообщение</span></a></div>
            </div>
        </div>
        <div class="person_company">
            <div class="name">Контакты: {{$company->title or 'Название компании'}} - {{$company->city or 'Город'}}</div>
            <div class="address">Адрес: {{$company->address or 'г. Город, ул. Улица 1, оф. 1'}} <span><a href="/contact/">Показать на карте</a></span></div>
            <div class="data timework">Время работы: {{$userOnSite->timeJob or 'пн-пт с 8:00 до 18:00'}}</div>
        </div>
    </div>
</div>
