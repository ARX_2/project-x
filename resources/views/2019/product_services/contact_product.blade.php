
<div class="col-3">
    <div class="manager">
        <div class="title">Контактная информация</div>
        <div class="block">
            <div class="person">
                <div class="image" style="background-image: url(/storage/app/{{Resize::ResizeImage($userOnSite, '55x55')}});"></div>
                <div class="info"><div class="name">{{$userOnSite->name or 'Имя'}} {{$userOnSite->lastname or 'Фамилия'}}</div><div class="position">{{$userOnSite->job or 'Должность'}}</div></div>
            </div>
            <?php //dd($company);?>
            <div class="data phone"><span>{{$userOnSite->tel or '+7 (900) 000-00-00'}}</span></div>
            <div class="data email"><a href="/contact"><span>Отправить сообщение</span></a></div>
            <div class="data local"><div class="address"><span>{{$company->address or 'г. Город, ул. Улица 1, оф. 1'}}</span><br><a id="openmap" class="open_cart">Показать на карте</a></div></div>
            <div class="data timework"><span>Время работы: {{$userOnSite->timeJob or 'пн-пт с 8:00 до 18:00'}}</span></div>
        </div>
    </div>
    <div class="map">
        <div class="title"><span>Карта</span></div>
        <a id="openmap2"><div class="back-map"><div class="block_map_b"><button class="map_b">Показать карту</button></div><div class="map-view" style="background-image: url(/public/site/img/map-load.jpg);"></div></div></a>
    </div>
    <div class="documents">
        <div class="title">Документы</div>
    </div>
</div>
</div>
</div>
</section>

<link rel="stylesheet" href="/public/site/open-image/open-image.css">
<script src="/public/site/open-image/open-image.js"></script>
<script>baguetteBox.run('.tz-gallery');</script>
<div class="modal"></div>

<script type="text/javascript">
document.getElementById('openmap').onclick = function() {
    $.get('{{route('take.modal.map')}}', function(data){
        $('.modal').html(data);
    })
};
document.getElementById('openmap2').onclick = function() {
    $.get('{{route('take.modal.map')}}', function(data){
        $('.modal').html(data);
    })
};
</script>
