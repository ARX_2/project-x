<?php //dd($product); ?>

<div id="modal_form" class="modal-ui-wrapper" style="background: rgba(0, 0, 0, 0.565);">
    <div class="modal-ui-modal" style="background: rgb(255, 255, 255);">
        <div class="modal-ui-content">
            <div class="title"><h2>Отправка заказа</h2></div>
            <div class="ui-p">
                <div class="p-image"><img src="/storage/app/{{$product->images->images or ''}}" alt=""></div>
                <div class="p-info">
                    <div class="p-info-t"><div class="p-name">{{$product->title or ''}}</div></div>
                </div>
            </div>
            <div class="ui-p">
                <div class="p-info-f">
                    <div class="i ui-m-b">
                        <div class="name">Цена:</div>
                        <div class="price"><b>{{$product->coast or ''}}</b> руб.</div>
                    </div>
                    <div class="i">
                        <div class="name">Количество:</div>
                        <div class="qty-ui">
                            <input class="qty-input" type="text" name="count" value="1" id="count">
                            <div class="minus">-</div>
                            <div class="plus">+</div>
                        </div>
                        <div class="price ui-m-n">шт. х <b>{{$product->coast or ''}}</b> руб.</div>
                    </div>
                    <div class="i">
                        <div class="name">Итого:</div>
                        <div class="qty-price"><b id="sum">{{str_replace(' ', '', $product->coast)}}</b> руб.</div>
                        <input type="hidden" class="sum" value="{{str_replace(' ', '', $product->coast)}}">
                        <input type="hidden" class="id" value="{{$product->id}}">
                    </div>
                </div>
            </div>
            <div class="ui-b">
                <div class="title">Контактная информация</div>
                <div class="ui-b-form">
                    <div class="form-group">
                        <div class="label">E-mail:</div>
                        <div class="g-input"><input class="" type="text" name="email" placeholder="Ваш email"></div>
                    </div>
                    <div class="form-group">
                        <div class="label">Контактный телефон:</div>
                        <div class="g-input"><input type="text" name="tel" placeholder="+7 (900) 000-00-00"></div>
                    </div>
                    <div class="form-group">
                        <div class="label">Ваше сообщение:</div>
                        <div class="g-input"><textarea maxlength="500" name="message" placeholder="Оставьте комментарий к заказу или задайте интересующий вопрос" id="message"></textarea></div>
                    </div>
                </div>
            </div>
            <div class="ui-f"><button id="sendOrder">Подтвердить отправку</button></div>
        </div>
        <div id="modal_close" class="modal-ui-close"><span>×</span></div>
    </div>
</div>
<div id="overlay"></div>

<style media="screen">
.plus, .minus{
  cursor: default;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
    $('#toorder').click( function(event){
    event.preventDefault();
    $('#overlay').fadeIn(50,
    function(){
    $('#modal_form')
    .css('display', 'flex')
    .animate({opacity: 1}, 50);
    });
});

$('#modal_close, #overlay').click( function(){
    $('#modal_form')
    .animate({opacity: 0}, 100,
    function(){
    $(this).css('display', 'none');
    $('#overlay').fadeOut(100);
    }
    );
});

$('.minus').click(function () {
    var $input = $(this).parent().find('#count');
    var count = parseInt($input.val()) - 1;
    var countif = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;

    $input.val(count);
    $input.change();
    var sum = $('.sum').val();
    @if($product->coast !== null)
    if(countif > 0){
      $('#sum').text(parseInt(sum) - {{str_replace(' ', '', $product->coast)}});
      $('.sum').val(parseInt(sum) - {{str_replace(' ', '', $product->coast)}});
    }
    @endif
    return false;
});
$('.plus').click(function () {
    var $input = $(this).parent().find('#count');
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    var sum = $('.sum').val();
    @if($product->coast !== null)
    $('#sum').text(parseInt(sum) + {{str_replace(' ', '', $product->coast)}});
    $('.sum').val(parseInt(sum) + {{str_replace(' ', '', $product->coast)}});
    @endif
    return false;
});

$('#sendOrder').click(function(){
    var count = $('#count').val();
    var email = $('input[name="email"]').val();
    var tel = $('input[name="tel"]').val();
    var message = $('#message').val();
    var id = $('.id').val();
    $.post('{{route('create.order')}}', {count:count, email:email, tel:tel, message:message, product:id}, function(data){
      alert('Ваш заказ успешно принят в обработку')
    });
});

});


</script>
