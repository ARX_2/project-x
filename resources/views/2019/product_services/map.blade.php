<div id="modal_form_openmap" class="modal-ui-wrapper" style="background: rgba(0, 0, 0, 0.565);">
    <div class="modal-ui-modal" style="background: rgb(255, 255, 255);">
        <div class="modal-ui-content">
            <div class="title"><h2>Карта</h2></div>
            <div class="ui-b">
                <div class="ui-b-form">{!! $info->map !!}</div>
            </div>
        </div>
        <div id="modal_close_openmap" class="modal-ui-close"><span>×</span></div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {
    $(document).ready(function() {$('#openmap').click( function(event){event.preventDefault();$('#overlay').fadeIn(50, function(){$('#modal_form_openmap').css('display', 'flex').animate({opacity: 1}, 50);});});
        $('#modal_close_openmap, #overlay').click( function(){$('#modal_form_openmap').animate({opacity: 0}, 100, function(){$(this).css('display', 'none');$('#overlay').fadeOut(100);});});
    });
});
</script>