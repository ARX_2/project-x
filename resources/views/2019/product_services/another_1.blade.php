<div class="c grey">
    <div class="content_title"><h2>Товары похожие на: {{$subcategory->title or ''}}</h2></div>
    <div class="c-g">
        <div class="c-g_l c-g_l-md">
            @forelse($subcategory->goods as $g)
            <!-- <script type="text/javascript">$(document).ready(function(){$('#ImageAnother{{$g->id}}').attr('src', '/storage/app/{{Resize::ResizeImage($g->images, "196x196")}}');});</script> -->
            <div class="item">
                <a href="/product/{{$subcategory->id}}-{{$subcategory->slug or ''}}/{{$g->id}}-{{$g->slug or ''}}">
                    <div class="image"><div class="img_dark"></div><img id="ImageAnother{{$g->id}}" src="/storage/app/{{Resize::ResizeImage($g->images, '196x196')}}" alt=""></div>
                    <div class="info">
                        <div class="name"><span>{{$g->title or ''}}</span></div>
                        <div class="prices">
                            <div class="minimum">Цена:</div>
                            <div class="price-content">от {{$g->coast or ''}} р.</div>
                        </div>
                    </div>
                </a>
            </div>
            @empty
            @endforelse
        </div>
    </div>
</div>
</div>
