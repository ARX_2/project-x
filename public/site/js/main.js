function myFunction() {
    var t = document.getElementById("id-hb-wrap");
    "hb-wrap" === t.className ? t.className += " resp change" : t.className = "hb-wrap";
    for (var s = document.querySelectorAll(".resp .nolink"), e = 0; e < s.length; e++) s[e].addEventListener("click", function (t) {
        t.preventDefault()
    }, !1)
}

function navFunction(t) {
    for (var s = document.querySelectorAll(".resp .dropdown-menu"), e = 0; e < s.length; e++) {
        var n = s[e];
        s[e].id === t ? n.classList.toggle("show") : n.classList.remove("show")
    }
}

function smFunction(t) {
    for (var s = document.querySelectorAll(".collapsed"), e = 0; e < s.length; e++) {
        var n = s[e];
        s[e].id === t && n.classList.toggle("active")
    }
}
function reviewsFunction() {
    var t = document.getElementById("ddd3");
    "i" === t.className ? t.className += " open" : t.className = "i";
}

var XMC = function (t) {
    this.bodyID = t.bodyID, this.body = null, this.backgroundLayerID = t.backgroundLayerID, this.backgroundLayer = null, this.selector = t.selector, this.selectorValue = t.selectorValue, this.btnCloseId = t.btnId, this.btnClose = null, "styleBg" in t && (this.styleBg = t.styleBg), "styleBody" in t && (this.styleBody = t.styleBody), "btnStyle" in t && (this.styleBtn = t.btnStyle), "content" in t ? this.content = t.content : console.error("content not found"), "classListBg" in t && (this.classListBg = t.classListBg), "classListBody" in t && (this.classListBody = t.classListBody), "classListBtn" in t && (this.classListBtn = t.classListBtn), this.delegateClick()
};
XMC.prototype.initBackground = function () {
    return null === this.backgroundLayer && (this.backgroundLayer = document.createElement("div"), this.backgroundLayer.id = this.backgroundLayerID, document.body.appendChild(this.backgroundLayer), this.btnClose = document.createElement("button"), this.btnClose.id = this.btnCloseId, this.btnClose.innerText = "×", null !== this.styleBg && this.bgStyle(), null !== this.classListBg && this.setClasses(this.classListBg, this.backgroundLayer), this.classListBtn && this.setClasses(this.classListBtn, this.btnClose), null !== this.styleBtn && this.btnStyle()), this.backgroundLayer.style.display = "flex", this
}, XMC.prototype.bgStyle = function () {
    var t = Object.keys(this.styleBg), s = this;
    t.map(function (t) {
        s.backgroundLayer.style[t] = s.styleBg[t]
    })
}, XMC.prototype.btnStyle = function () {
    var t = Object.keys(this.styleBtn), s = this;
    t.map(function (t) {
        s.btnClose.style[t] = s.styleBtn[t]
    })
}, XMC.prototype.initTarget = function () {
    return null === this.body && (this.body = document.createElement("div"), this.body.id = this.bodyID, this.backgroundLayer.appendChild(this.body), this.body.innerHTML = this.content, null !== this.styleBody && this.bodyStyle(), this.classListBody && this.setClasses(this.classListBody, this.body), this.body.appendChild(this.btnClose)), this.body.style.display = "flex", this
}, XMC.prototype.bodyStyle = function () {
    var t = Object.keys(this.styleBody), s = this;
    t.map(function (t) {
        s.body.style[t] = s.styleBody[t]
    })
}, XMC.prototype.show = function () {
    this.initBackground(), this.initTarget()
}, XMC.prototype.close = function () {
    this.backgroundLayer.style.display = "none", this.body.style.display = "none"
}, XMC.prototype.delegateClick = function () {
    var t = this;
    window.addEventListener("click", function () {
        event.target.hasAttribute(t.selector) && event.target.getAttribute(t.selector) === t.selectorValue && (t.show(), t.delegateClose())
    }, t, !1)
}, XMC.prototype.delegateClose = function () {
    if (null !== this.btnClose) {
        var t = this.btnClose, s = this;
        t.addEventListener("click", function () {
            s.close()
        }, s)
    }
}, XMC.prototype.setClasses = function (t, s) {
    t.map(function (t) {
        s.classList.add(t)
    })
}, new XMC({
    bodyID: "body",
    backgroundLayerID: "wrapper",
    selector: "data-type",
    selectorValue: "openModalForm",
    btnId: "mfClose",
    content: '<div class="ytp-modal-content"><iframe src="https://www.youtube.com/embed/H9wsCOHJCAc?autoplay=1" style="width:100%;height:100%;border:0;margin:0;overflow:auto"></iframe></div>',
    classListBg: ["ytp-modal-wrapper"],
    classListBody: ["ytp-modal"],
    classListBtn: ["ytp-close"],
    styleBg: {background: "#00000090"},
    styleBody: {background: "#ffffff"},
    btnStyle: {color: "#ffffff"}
});


var XMC2 = function (t) {
    this.bodyID = t.bodyID, this.body = null, this.backgroundLayerID = t.backgroundLayerID, this.backgroundLayer = null, this.selector = t.selector, this.selectorValue = t.selectorValue, this.btnCloseId = t.btnId, this.btnClose = null, "styleBg" in t && (this.styleBg = t.styleBg), "styleBody" in t && (this.styleBody = t.styleBody), "btnStyle" in t && (this.styleBtn = t.btnStyle), "content" in t ? this.content = t.content : console.error("content not found"), "classListBg" in t && (this.classListBg = t.classListBg), "classListBody" in t && (this.classListBody = t.classListBody), "classListBtn" in t && (this.classListBtn = t.classListBtn), this.delegateClick()
};
XMC2.prototype.initBackground = function () {
    return null === this.backgroundLayer && (this.backgroundLayer = document.createElement("div"), this.backgroundLayer.id = this.backgroundLayerID, document.body.appendChild(this.backgroundLayer), this.btnClose = document.createElement("div"), this.btnClose.id = this.btnCloseId, this.btnClose.innerHTML = "<span>×</span>", null !== this.styleBg && this.bgStyle(), null !== this.classListBg && this.setClasses(this.classListBg, this.backgroundLayer), this.classListBtn && this.setClasses(this.classListBtn, this.btnClose), null !== this.styleBtn && this.btnStyle()), this.backgroundLayer.style.display = "flex", this
}, XMC2.prototype.bgStyle = function () {
    var t = Object.keys(this.styleBg), s = this;
    t.map(function (t) {
        s.backgroundLayer.style[t] = s.styleBg[t]
    })
}, XMC2.prototype.btnStyle = function () {
    var t = Object.keys(this.styleBtn), s = this;
    t.map(function (t) {
        s.btnClose.style[t] = s.styleBtn[t]
    })
}, XMC2.prototype.initTarget = function () {
    return null === this.body && (this.body = document.createElement("div"), this.body.id = this.bodyID, this.backgroundLayer.appendChild(this.body), this.body.innerHTML = this.content, null !== this.styleBody && this.bodyStyle(), this.classListBody && this.setClasses(this.classListBody, this.body), this.body.appendChild(this.btnClose)), this.body.style.display = "table", this
}, XMC2.prototype.bodyStyle = function () {
    var t = Object.keys(this.styleBody), s = this;
    t.map(function (t) {
        s.body.style[t] = s.styleBody[t]
    })
}, XMC2.prototype.show = function () {
    this.initBackground(), this.initTarget()
}, XMC2.prototype.close = function () {
    this.backgroundLayer.style.display = "none", this.body.style.display = "none"
}, XMC2.prototype.delegateClick = function () {
    var t = this;
    window.addEventListener("click", function () {
        event.target.hasAttribute(t.selector) && event.target.getAttribute(t.selector) === t.selectorValue && (t.show(), t.delegateClose())
    }, t, !1)
}, XMC2.prototype.delegateClose = function () {
    if (null !== this.btnClose) {
        var t = this.btnClose, s = this;
        t.addEventListener("click", function () {
            s.close()
        }, s)
    }
}, XMC2.prototype.setClasses = function (t, s) {
    t.map(function (t) {
        s.classList.add(t)
    })
}, new XMC2({
    bodyID: "body",
    backgroundLayerID: "wrapper",
    selector: "data-type",
    selectorValue: "openModalForm2",
    btnId: "mfClose",
    content: '<div class="modal-ui-content">\n' +
    '            <div class="title"><h2>Отправка заказа</h2></div>\n' +
    '            <form action="/create/order/" method="post">\n'+
    '            <div class="ui-p">\n' +
    '                <div class="p-image"><img src="/frontend/img/dev-315x315.jpg" alt=""></div>\n' +
    '                <div class="p-info">\n' +
    '                    <div class="p-info-t"><div class="p-name">Длинное название товара</div></div>\n' +
    '                    <div class="p-info-b">\n' +
    '                        <div class="p-switch">Цвет: <span>Красный</span></div>\n' +
    '                        <div class="p-switch">Комплект: <span>Комплект 1</span></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="ui-p">\n' +
    '                <div class="p-info-f">\n' +
    '                    <div class="i ui-m-b"><div class="name">Цена:</div><div class="price"><b>32000.00</b> руб.</div></div><div class="i"><div class="name">Количество:</div><div class="qty-ui"><input class="qty-input" type="text" name="count" value="2"><div class="minus">-</div><div class="plus">+</div></div> <div class="price ui-m-n">шт. х <b>32000.00</b> руб.</div></div><div class="i"><div class="name">Итого:</div><div class="qty-price"><b>64000.00</b> руб.</div></div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="ui-b">\n' +
    '                <div class="title">В компанию: "Название компании"</div>\n' +
    '                <div class="ui-b-form">\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="label">E-mail:</div>\n' +
    '                        <div class="g-input"><input class="" type="text" name="email" placeholder="Ваш email"></div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="label">Контактный телефон:</div>\n' +
    '                        <div class="g-input"><input type="text" name="tel" placeholder="+7 (900) 000-00-00"></div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="label">Ваше сообщение:</div>\n' +
    '                        <div class="g-input"><textarea maxlength="500" name="message" placeholder="Оставьте комментарий к заказу или задайте интересующий вопрос"></textarea></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="ui-f"><button>Подтвердить отправку</button></div>\n' +
    '            </form>\n'+
    '        </div>',
    classListBg: ["modal-ui-wrapper"],
    classListBody: ["modal-ui-modal"],
    classListBtn: ["modal-ui-close"],
    styleBg: {background: "#00000090"},
    styleBody: {background: "#ffffff"},
    btnStyle: {}
});




var XMC3 = function (t) {
    this.bodyID = t.bodyID, this.body = null, this.backgroundLayerID = t.backgroundLayerID, this.backgroundLayer = null, this.selector = t.selector, this.selectorValue = t.selectorValue, this.btnCloseId = t.btnId, this.btnClose = null, "styleBg" in t && (this.styleBg = t.styleBg), "styleBody" in t && (this.styleBody = t.styleBody), "btnStyle" in t && (this.styleBtn = t.btnStyle), "content" in t ? this.content = t.content : console.error("content not found"), "classListBg" in t && (this.classListBg = t.classListBg), "classListBody" in t && (this.classListBody = t.classListBody), "classListBtn" in t && (this.classListBtn = t.classListBtn), this.delegateClick()
};
XMC3.prototype.initBackground = function () {
    return null === this.backgroundLayer && (this.backgroundLayer = document.createElement("div"), this.backgroundLayer.id = this.backgroundLayerID, document.body.appendChild(this.backgroundLayer), this.btnClose = document.createElement("div"), this.btnClose.id = this.btnCloseId, this.btnClose.innerHTML = "<span>×</span>", null !== this.styleBg && this.bgStyle(), null !== this.classListBg && this.setClasses(this.classListBg, this.backgroundLayer), this.classListBtn && this.setClasses(this.classListBtn, this.btnClose), null !== this.styleBtn && this.btnStyle()), this.backgroundLayer.style.display = "flex", this
}, XMC3.prototype.bgStyle = function () {
    var t = Object.keys(this.styleBg), s = this;
    t.map(function (t) {
        s.backgroundLayer.style[t] = s.styleBg[t]
    })
}, XMC3.prototype.btnStyle = function () {
    var t = Object.keys(this.styleBtn), s = this;
    t.map(function (t) {
        s.btnClose.style[t] = s.styleBtn[t]
    })
}, XMC3.prototype.initTarget = function () {
    return null === this.body && (this.body = document.createElement("div"), this.body.id = this.bodyID, this.backgroundLayer.appendChild(this.body), this.body.innerHTML = this.content, null !== this.styleBody && this.bodyStyle(), this.classListBody && this.setClasses(this.classListBody, this.body), this.body.appendChild(this.btnClose)), this.body.style.display = "table", this
}, XMC3.prototype.bodyStyle = function () {
    var t = Object.keys(this.styleBody), s = this;
    t.map(function (t) {
        s.body.style[t] = s.styleBody[t]
    })
}, XMC3.prototype.show = function () {
    this.initBackground(), this.initTarget()
}, XMC3.prototype.close = function () {
    this.backgroundLayer.style.display = "none", this.body.style.display = "none"
}, XMC3.prototype.delegateClick = function () {
    var t = this;
    window.addEventListener("click", function () {
        event.target.hasAttribute(t.selector) && event.target.getAttribute(t.selector) === t.selectorValue && (t.show(), t.delegateClose())
    }, t, !1)
}, XMC3.prototype.delegateClose = function () {
    if (null !== this.btnClose) {
        var t = this.btnClose, s = this;
        t.addEventListener("click", function () {
            s.close()
        }, s)
    }
}, XMC3.prototype.setClasses = function (t, s) {
    t.map(function (t) {
        s.classList.add(t)
    })
}, new XMC3({
    bodyID: "body",
    backgroundLayerID: "wrapper",
    selector: "data-type",
    selectorValue: "openModalForm3",
    btnId: "mfClose",
    content: '<div class="modal-ui-content">\n' +
    '            <div class="title"><h2>Перезвоним!</h2></div>\n' +
    '            <div class="ui-b">\n' +
    '                <div class="ui-b-form" style="padding: 20px">\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="label">Контактный телефон:</div>\n' +
    '                        <div class="g-input"><input type="text" placeholder="+7 (900) 000-00-00"></div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <div class="label">Ваше сообщение:</div>\n' +
    '                        <div class="g-input"><textarea maxlength="500" placeholder="Оставьте комментарий к заказу или задайте интересующий вопрос"></textarea></div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="ui-f"><button>Отправить</button></div>\n' +
    '        </div>',
    classListBg: ["modal-ui-wrapper"],
    classListBody: ["modal-ui-modal"],
    classListBtn: ["modal-ui-close"],
    styleBg: {background: "#00000090"},
    styleBody: {background: "#ffffff"},
    btnStyle: {}
});
